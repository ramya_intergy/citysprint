
package au.com.intergy.invoice.webservice.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "generateBatch", namespace = "http://webservice.invoice.intergy.com.au/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generateBatch", namespace = "http://webservice.invoice.intergy.com.au/", propOrder = {
    "userId",
    "runId",
    "description",
    "xmlContent"
})
public class GenerateBatch {

    @XmlElement(name = "userId", namespace = "")
    private String userId;
    @XmlElement(name = "runId", namespace = "")
    private String runId;
    @XmlElement(name = "description", namespace = "")
    private String description;
    @XmlElement(name = "xmlContent", namespace = "")
    private String xmlContent;

    /**
     * 
     * @return
     *     returns String
     */
    public String getUserId() {
        return this.userId;
    }

    /**
     * 
     * @param userId
     *     the value for the userId property
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 
     * @return
     *     returns String
     */
    public String getRunId() {
        return this.runId;
    }

    /**
     * 
     * @param runId
     *     the value for the runId property
     */
    public void setRunId(String runId) {
        this.runId = runId;
    }

    /**
     * 
     * @return
     *     returns String
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * 
     * @param description
     *     the value for the description property
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     returns String
     */
    public String getXmlContent() {
        return this.xmlContent;
    }

    /**
     * 
     * @param xmlContent
     *     the value for the xmlContent property
     */
    public void setXmlContent(String xmlContent) {
        this.xmlContent = xmlContent;
    }

}
