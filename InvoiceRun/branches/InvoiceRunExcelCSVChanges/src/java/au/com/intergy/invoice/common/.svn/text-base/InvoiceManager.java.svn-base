package au.com.intergy.invoice.common;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

/**
 * 
 * @author Chi Bui
 */
public class InvoiceManager {
	
	/**
	 *  Change status of the given batch groups to INIT,
	 *  their customer batches' status to INIT and in turn their invoices' status to INIT.
	 *  
	 *  To resume a batch group, see resumeBatchGroups() methods
	 */
	public static void initialiseBatchGroups(String[] batchGroupIds, Map<Long, String> printerNames) 
	throws DBAccessException {
		Connection conn = null;
		try {
			conn = DatabaseConnector.getConnection();
			conn.setAutoCommit(false); 
			
			BatchGroupDataAccess.updateStatusAndPrinter(conn, BatchGroup.Status.INIT.name(), 
					batchGroupIds, printerNames);
			initialiseCustomerBatchesByBatchGroups(conn, batchGroupIds);
			conn.commit();
		} catch (Exception e) {
			if (conn != null)
				try {conn.rollback(); } catch (SQLException se) {}
		} finally {
			try {DatabaseConnector.returnConnection(conn);} catch (Exception dae) {}
		}	
	}
	
	/** Initialise customer batches and their invoices */
	private static void initialiseCustomerBatchesByBatchGroups(Connection conn, String[] batchGroupIds) 
	throws DBAccessException {
		CustomerBatchDataAccess.updateStatusByBatchGroupIds(
				conn, batchGroupIds, CustomerBatch.Status.INIT.name());
		
		initialiseInvoicesByBatchGroups(conn, batchGroupIds);
	}
	
	/** Initialise all invoices - change status to INIT */
	private static void initialiseInvoicesByBatchGroups(Connection conn, String[] batchGroupIds) 
	throws DBAccessException {
		InvoiceDataAccess.updateStatusByBatchGroups(conn, batchGroupIds, Statement.Status.INIT.name());
	}
	
	
	/**
	 *  Change status of the given batch groups to STARTED,
	 *  their customer batches' status to INIT and in turn their invoices' status to INIT.
	 *  
	 *  To resume a batch group, see resumeBatchGroups() methods
	 */
	public static void startBatchGroups(String[] batchGroupIds, Map<Long, String> printerNames) 
	throws DBAccessException {
		Connection conn = null;
		try {
			conn = DatabaseConnector.getConnection();
			conn.setAutoCommit(false); 
			
			BatchGroupDataAccess.updateStatusAndPrinter(conn, BatchGroup.Status.STARTED.name(), 
					batchGroupIds, printerNames);
			startCustomerBatchesByBatchGroups(conn, batchGroupIds);
			conn.commit();
		} catch (Exception e) {
			if (conn != null)
				try {conn.rollback(); } catch (SQLException se) {}
		} finally {
			try {DatabaseConnector.returnConnection(conn);} catch (Exception dae) {}
		}	
	}
	
	/** Start customer batches and their invoices */
	private static void startCustomerBatchesByBatchGroups(Connection conn, String[] batchGroupIds) 
	throws DBAccessException {
		CustomerBatchDataAccess.updateStatusByBatchGroupIds(
				conn, batchGroupIds, CustomerBatch.Status.PROCESSING.name());
		
		startInvoicesByBatchGroups(conn, batchGroupIds);
	}
	
	/** Start all invoices - change invoice status to PROCESSING */
	private static void startInvoicesByBatchGroups(Connection conn, String[] batchGroupIds) 
	throws DBAccessException {
		InvoiceDataAccess.updateStatusByBatchGroups(conn, batchGroupIds, Statement.Status.PROCESSING.name());
	}
	
	
	
	/**
	 *  Change status of the given batch groups to STARTED.
	 *  Leave invoice statuses as they are.
	 *  
	 */
	public static void resumeBatchGroups(String[] batchGroupIds, Map<Long, String> printerNames) 
	throws DBAccessException {
		Connection conn = null;
		try {
			conn = DatabaseConnector.getConnection();
			conn.setAutoCommit(false); 
			
			BatchGroupDataAccess.updateStatusAndPrinter(conn, BatchGroup.Status.STARTED.name(), 
					batchGroupIds, printerNames);
			
			CustomerBatchDataAccess.updateStatusByBatchGroupIdsAndCurrentStatus(conn, batchGroupIds,
					CustomerBatch.Status.PAUSED.name(), CustomerBatch.Status.PROCESSING.name());
			conn.commit();
		} catch (Exception e) {
			if (conn != null)
				try {conn.rollback(); } catch (SQLException se) {}
		} finally {
			try {DatabaseConnector.returnConnection(conn);} catch (Exception dae) {}
		}	
	}
	
	/**
	 * Updates batch group status, customer batch status to CANCELLED
	 */
	public static void cancelBatchGroups(String[] batchGroupIds, Map<Long, String> printerNames) 
	throws DBAccessException {
		Connection conn = null;
		try {
			conn = DatabaseConnector.getConnection();
			conn.setAutoCommit(false); 
			
			
			BatchGroupDataAccess.updateStatusAndPrinter(conn, BatchGroup.Status.CANCELLED.name(), 
					batchGroupIds, printerNames);
			
			cancelCustomerBatchesByBatchGroups(conn, batchGroupIds);
			conn.commit();
		} catch (Exception e) {
			if (conn != null)
				try {conn.rollback(); } catch (SQLException se) {}
		} finally {
			try {DatabaseConnector.returnConnection(conn);} catch (Exception dae) {}
		}	
	}
	
	private static void cancelCustomerBatchesByBatchGroups(Connection conn, String[] batchGroupIds) 
	throws Exception {
		CustomerBatchDataAccess.updateStatusByBatchGroupIds(conn,
				batchGroupIds, CustomerBatch.Status.CANCELLED.name());
		cancelInvoicesByBatchGroups(conn, batchGroupIds);
	}
	
	/** Cancel all invoices */
	private static void cancelInvoicesByBatchGroups(Connection conn, String[] batchGroupIds) 
	throws DBAccessException {
		InvoiceDataAccess.updateStatusByBatchGroups(conn, batchGroupIds, Statement.Status.CANCELLED.name());
	}
	
	/**
	 *  Log an error message.
	 *  Fails invoice, customer batch and batch group.
	 */
	public static void failInvoice(long invoiceId, String errorMessage, String brand, String custNo) 
	throws DBAccessException {
		InvoiceDataAccess.updateStatus(invoiceId, Statement.Status.FAILED.name());
		Statement stmt = InvoiceDataAccess.findById(invoiceId);
		ErrorDataAccess.addError(InvoiceError.ErrorType.INVOICE.name(), invoiceId, errorMessage,
				stmt.getBrand(), stmt.getAccountNumber());
		failCustomerBatchByInvoice(invoiceId);
		ErrorNofifier.notifyError();
	}
	
	/**
	 *  Log an error message.
	 *  Fails customer batch and its batch group.
	 */
	public static void failCustomerBatch(long custBatchId, String errorMessage, String brand, String custKey) 
	throws DBAccessException {
		CustomerBatchDataAccess.updateToFail(custBatchId, 
				CustomerBatch.Status.FAILED.name());
		ErrorDataAccess.addError(InvoiceError.ErrorType.CUSTBATCH.name(), custBatchId, errorMessage,
				brand, custKey);
		failBatchGroupByCustBatchId(custBatchId);
		ErrorNofifier.notifyError();
	}
	
	private static void failCustomerBatchByInvoice(long invoiceId) 
	throws DBAccessException {
		Statement stmt = InvoiceDataAccess.findById(invoiceId);
		CustomerBatchDataAccess.updateToFail(stmt.getCustBatchId(), 
				CustomerBatch.Status.FAILED.name());
		failBatchGroupByCustBatchId(stmt.getCustBatchId());
	}
	
	private static void failBatchGroupByCustBatchId(long custBatchId)
	throws DBAccessException {
		BatchGroupDataAccess.updateStatusByCustBatchId(custBatchId, BatchGroup.Status.FAILED.name());
	}
}
