package au.com.intergy.invoice.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A Batch contains one or more BatchGroup.
 * A BatchGroup consists of one or more CustomerBatches.
 * 
 * @author Chi Bui
 *
 */
public class BatchGroup implements Serializable {
	private final static long serialVersionUID = 142275542244L;
	private long id;
	
	private long batchId;
	/** Id of the invoice run passed from CityTrak */
	private String runId;
	/** Id of the user who invokes the invoice run */
	private String userId;
	/** Describes parameters used to generate the batch or the invoice run */
	private String description;
	
	private String brand;
	private TransportMethod transportMethod;
	private String outputFileFormat;
	
	private Date submittedDate;
	private int custBatchCount;
	private int invoiceCount;
	private String status;	
	private String selectedPrinter;
        private String scheduleOutputFormat;


	private List<CustomerBatch> custBatches = new ArrayList<CustomerBatch>();
	
	public enum Status { INIT, STARTED, PAUSED, CANCELLED, COMPLETED, FAILED};
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getBatchId() {
		return batchId;
	}
	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public TransportMethod getTransportMethod() {
		return transportMethod;
	}
	public void setTransportMethod(TransportMethod transportMethod) {
		this.transportMethod = transportMethod;
	}
	public String getOutputFileFormat() {
		return outputFileFormat;
	}
	public void setOutputFileFormat(String outputFileFormat) {
		this.outputFileFormat = outputFileFormat;
	}
	
	public Date getSubmittedDate() {
		return submittedDate;
	}
	
	public void setSubmittedDate(Date submittedDate) {
		this.submittedDate = submittedDate;
	}
	
	public int getCustBatchCount() {
		return (!custBatches.isEmpty())?custBatches.size():custBatchCount;
	}
	
	public void setCustBatchCount() {
		custBatchCount = custBatches.size();
	}
	
	public void setCustBatchCount(int custBatchCount) {
		this.custBatchCount = custBatchCount;
	}
	
	public int getInvoiceCount() {		
		return invoiceCount;
	}

        public String getScheduleOutputFormat() {
            return scheduleOutputFormat;
        }

        public void setScheduleOutputFormat(String scheduleOutputFormat) {
            this.scheduleOutputFormat = scheduleOutputFormat;
        }
	/**
	 * @return Number of invoices including any statement.
	 */
	public static int getInvoiceCount(List<CustomerBatch> custBatches) {

		int tempCount = 0;

		for (CustomerBatch custBatch : custBatches) {
			tempCount += custBatch.getStatement() == null ? 0 : 1;
			tempCount += custBatch.getInvoices().size();
		}
		return tempCount;
	}
	

	public void setInvoiceCount(int invoiceCount) {
		this.invoiceCount = invoiceCount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSelectedPrinter() {
		return selectedPrinter;
	}
	public void setSelectedPrinter(String selectedPrinter) {
		this.selectedPrinter = selectedPrinter;
	}
	
	public String getRunId() {
		return runId;
	}
	public void setRunId(String runId) {
		this.runId = runId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<CustomerBatch> getCustBatches() {
		return custBatches;
	}
	public void setCustBatches(List<CustomerBatch> custBatches) {
		this.custBatches = custBatches;
	}
	
	/** 
	 * An utility method returns a list of BatchGroup based on the given list
	 * of customer batches. 
	 * 
	 * It is assumed that all given customer batches belong to a Batch, i.e. created
	 * from 1 web service call.
	 */
	public static List<BatchGroup> getBatchGroups(List<CustomerBatch> custBatches) {
		List<BatchGroup> batchGroupsResult = new ArrayList<BatchGroup>();
		
		//key = batch group ID, value = list of cust batches in the same batch group
		Map<String, List<CustomerBatch>> custBatchesMap = new HashMap<String, List<CustomerBatch>>();
		
		//group customer batches by batch group ID
		for (CustomerBatch custBatch : custBatches) {
		
			String groupKey = custBatch.getGroupKey();
			if (!custBatchesMap.keySet().contains(custBatch.getGroupKey())) {
				custBatchesMap.put(groupKey, new ArrayList<CustomerBatch>());
			}
			custBatchesMap.get(groupKey).add(custBatch);
		}
		
		for (String groupKey : custBatchesMap.keySet()) {
			BatchGroup group = new BatchGroup();
			group.setCustBatches(custBatchesMap.get(groupKey));
			
			//set brand, transport method, output format
			CustomerBatch custBatch = custBatchesMap.get(groupKey).get(0);
			group.setBrand(custBatch.getBrand());
			group.setTransportMethod(custBatch.getTransportMethod());
			group.setOutputFileFormat(custBatch.getOutputFileFormat());
			group.setStatus(Status.INIT.name());
			group.setInvoiceCount(getInvoiceCount(custBatchesMap.get(groupKey)));
                        group.setScheduleOutputFormat(custBatch.getScheduleOutputFormat());
			batchGroupsResult.add(group);
		}
		return batchGroupsResult;
	}
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation 
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "]";
	
	    StringBuilder retValue = new StringBuilder();
	    
	    retValue.append("BatchGroup ( ")
	        .append(super.toString()).append(TAB)
	        .append("[id = ").append(this.id).append(TAB)
	        .append("[batchId = ").append(this.batchId).append(TAB)
	        .append("[runId = ").append(this.runId).append(TAB)
	        .append("[userId = ").append(this.userId).append(TAB)
	        .append("[description = ").append(this.description).append(TAB)
	        .append("[brand = ").append(this.brand).append(TAB)
	        .append("[transportMethod = ").append(this.transportMethod).append(TAB)
	        .append("[outputFileFormat = ").append(this.outputFileFormat).append(TAB)
	        .append("[submittedDate = ").append(this.submittedDate).append(TAB)
	        .append("[custBatchCount = ").append(this.custBatchCount).append(TAB)
	        .append("[invoiceCount = ").append(this.invoiceCount).append(TAB)
	        .append("[status = ").append(this.status).append(TAB)
	        .append("[selectedPrinter = ").append(this.selectedPrinter).append(TAB)
	        .append("[custBatches = ").append(this.custBatches).append(TAB)
	        .append(" )");
	    
	    return retValue.toString();
	}
	
	
}
