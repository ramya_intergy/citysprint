package au.com.intergy.invoice.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import au.com.intergy.invoice.tool.InvoiceFileArchiver;

public class BatchGroupDataAccess {
	private static final Logger logger = new Logger();

	public static void create(Connection conn, BatchGroup batchGroup) 
	throws DBAccessException {

		PreparedStatement stmt = null;
		try {
			String query = "INSERT INTO batchgroup "+
			"(BatchID, Brand_Group,  OutputFormat, TransportMethod, "+
			"Status, CustomerBatchCount, InvoiceCount, PrinterSelected, DateSubmitted, ScheduleOutputFormat)"+
			"  VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			stmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

			stmt.setLong(1, batchGroup.getBatchId());
			stmt.setString(2, batchGroup.getBrand());
			stmt.setString(3, batchGroup.getOutputFileFormat());
			stmt.setString(4, batchGroup.getTransportMethod().name());
			stmt.setString(5, batchGroup.getStatus());
			stmt.setInt(6, batchGroup.getCustBatchCount());
			stmt.setInt(7, batchGroup.getInvoiceCount());
			stmt.setString(8, batchGroup.getSelectedPrinter());
			java.sql.Timestamp time = 
				batchGroup.getSubmittedDate() == null? null: new 
						java.sql.Timestamp(batchGroup.getSubmittedDate().getTime());
			stmt.setTimestamp(9, time);
                        stmt.setString(10, batchGroup.getScheduleOutputFormat());

			int result = stmt.executeUpdate();
			if (result == 0)
				throw new DBAccessException("Unable to save BatchGroup to DB: " + batchGroup.toString());
			
            ResultSet keys = stmt.getGeneratedKeys();
			
            long batchGroupId = 0;
			if (keys.next()) 
				batchGroupId = keys.getLong(1);
			
			if (batchGroupId == 0)
				throw new DBAccessException("Unable to retrieve BatchGroup ID for batch " + batchGroup.getBatchId());
			
			for (CustomerBatch custBatch : batchGroup.getCustBatches()) {
				custBatch.setBatchGroupId(batchGroupId);
				CustomerBatchDataAccess.create(conn, custBatch);
			}
			keys.close();
		} catch (Exception e) {
			logger.log("Unable to create BatchGroup " + e.toString());
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			if (stmt != null)
				try { stmt.close();} catch (SQLException se) {}
		}
	}
	
	/**
	 * Only update BatchGroup if its current status is STARTED
	 * @param newStatus
	 * @param custBatchStatus e.g. SUCCESS
	 * @param batchGroupId
	 * @throws DBAccessException
	 */
	public static void updateStatusBasedOnCustBatch(String newStatus, String custBatchStatus, long batchGroupId) 
	throws DBAccessException {
		Connection conn = null;
		try {
			conn = DatabaseConnector.getConnection();
			List<CustomerBatch> custBatches = CustomerBatchDataAccess.findCustomerBatchesByBatchGroupId(conn, batchGroupId);
			
			boolean updateBatchGroup = true;
			for (CustomerBatch custBatch : custBatches) {
				if (!custBatch.getStatus().name().equals(custBatchStatus)) {
					updateBatchGroup = false;
					break;
				}
			}
			
			if (updateBatchGroup) {
				String query = "UPDATE batchgroup SET Status = ? " +
				" WHERE ID = ?  AND Status = ?";

				PreparedStatement prStmt = conn.prepareStatement(query);
				prStmt.setString(1, newStatus);
				prStmt.setLong(2, batchGroupId);
			    prStmt.setString(3, BatchGroup.Status.STARTED.name());
				
				prStmt.executeUpdate();
				prStmt.close();
			}
		} catch (Exception e) {
			logger.log("ERROR: Unable to update status for batch groups with error " + e.toString());
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			try {DatabaseConnector.returnConnection(conn); } catch (Exception e) {}
		}
	}
	
	public static void updateStatus(String newStatus, String[] batchGroupIds) 
	throws DBAccessException {
		Connection conn = null;
		try {
			String query = "UPDATE batchgroup SET Status = '" +newStatus +
			"' WHERE ID in (";

		
			String batchGroupIdString = "";
			
			for (int i = 0; i< batchGroupIds.length; i++) {
				batchGroupIdString = batchGroupIdString.concat(batchGroupIds[i]);
				if (i != batchGroupIds.length - 1)
					batchGroupIdString = batchGroupIdString.concat(", ");
			}

			query = query.concat(batchGroupIdString).concat(")");

			conn = DatabaseConnector.getConnection();
			Statement stmt = conn.createStatement();

			stmt.executeUpdate(query);
			stmt.close();
		} catch (Exception e) {
			logger.log("ERROR: Unable to update status for batch groups with error " + e.toString());
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			try {DatabaseConnector.returnConnection(conn); } catch (Exception e) {}
		}
	}
	
	public static void updateStatusAndPrinter(Connection conn, String newStatus, String[] batchGroupIds, Map<Long, String> newPrinters) 
	throws DBAccessException {
		try {
			for (int i = 0; i< batchGroupIds.length; i++) {
				Long id =  Long.parseLong(batchGroupIds[i]);
				conn = updateStatusAndPrinter(conn, id, newStatus, newPrinters.get(id));
			}
		} catch (Exception e) {
			logger.log("ERROR: Unable to update printer for batch groups with error " + e.toString());
			throw new DBAccessException(e.getMessage());
		} 
	}
	
	public static void updateStatusAndPrinter(String newStatus, String[] batchGroupIds, Map<Long, String> newPrinters) 
	throws DBAccessException {
		Connection conn = null;
		try {
			conn = DatabaseConnector.getConnection();
			conn.setAutoCommit(false);
			
			for (int i = 0; i< batchGroupIds.length; i++) {
				Long id =  Long.parseLong(batchGroupIds[i]);
				conn = updateStatusAndPrinter(conn, id, newStatus, newPrinters.get(id));
			}

			conn.commit();
		} catch (Exception e) {
			try { 
				if (conn != null)
					conn.rollback(); 
			} catch (SQLException sqle) {}
			logger.log("ERROR: Unable to update printer for batch groups with error " + e.toString());
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			try {DatabaseConnector.returnConnection(conn); } catch (Exception e) {}
		}
	}
	
	public static void updateStatusByCustBatchId(long custBatchId, String newStatus) 
	throws DBAccessException {
		Connection conn = null;
		try {
			String query = "UPDATE batchgroup SET Status = '" +newStatus +
			"' WHERE ID IN ( select BatchGroupID from customerbatchdetails where BatchID_CustKey = " +
			custBatchId + ")";

			conn = DatabaseConnector.getConnection();
			Statement stmt = conn.createStatement();

			stmt.executeUpdate(query);
			stmt.close();
		} catch (Exception e) {
			logger.log("ERROR: Unable to update status for batch group with error " + e.toString());
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			try {DatabaseConnector.returnConnection(conn); } catch (Exception e) {}
		}
	}
	
	private static Connection updateStatusAndPrinter(Connection conn, Long id, String status, String newPrinter) 
	throws SQLException {
		String query = null;
		
		if (newPrinter == null)
			query = "UPDATE batchgroup SET Status = ? WHERE ID = ?";
		else 
			query = "UPDATE batchgroup SET Status = ? , PrinterSelected = ? WHERE ID = ?";
		
		PreparedStatement prStmt = conn.prepareStatement(query);
		prStmt.setString(1, status);
		
		if (newPrinter == null) {
			prStmt.setLong(2, id);
		} else {
			prStmt.setString(2, newPrinter);
			prStmt.setLong(3, id);
		}
		
		prStmt.executeUpdate();
		prStmt.close();
		return conn;
	}
	
	public static List<BatchGroup> findBatchGroupByIds(String[] batchGroupIds) 	
	throws DBAccessException {
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet result = null;
		try {
			String query = "select * from batchgroup bg, batch b WHERE bg.BatchID = b.BatchID AND ID in (";
		
			String batchGroupIdString = "";
			
			for (int i = 0; i< batchGroupIds.length; i++) {
				batchGroupIdString = batchGroupIdString.concat(batchGroupIds[i]);
				if (i != batchGroupIds.length - 1)
					batchGroupIdString = batchGroupIdString.concat(", ");
			}

			query = query.concat(batchGroupIdString).concat(")");

			conn = DatabaseConnector.getConnection();
			stmt = conn.createStatement();

			result = stmt.executeQuery(query);
			
			return createBatchGroups(result);
		} catch (Exception e) {
			logger.log("ERROR: Unable to update status for batch groups with error " + e.toString());
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			try {
				if (stmt != null) 
					stmt.close();
				if (result != null)
					result.close();
				DatabaseConnector.returnConnection(conn); 
			} catch (Exception e) {}
		}
	}
	
	public static List<BatchGroup> findNonCompletedBatchGroups() 	
	throws DBAccessException {

		List<BatchGroup> groups = new ArrayList<BatchGroup>();
		Connection conn = null;
		
		try {
			String query = "select * from batchgroup bg, batch b " +
					"WHERE bg.BatchID = b.BatchID AND Status != 'COMPLETED' " +
					"AND Status != 'CANCELLED' ORDER BY bg.BatchID DESC";

			conn = DatabaseConnector.getConnection();
			Statement stmt = conn.createStatement();

			ResultSet result = stmt.executeQuery(query);
			
			while (result.next()) {
				BatchGroup group = new BatchGroup();
				group.setBatchId(result.getLong("BatchID"));
				group.setRunId(result.getString("RunID"));
				group.setUserId(result.getString("UserID"));
				group.setId(result.getLong("ID"));
				group.setBrand(result.getString("Brand_Group"));
				group.setOutputFileFormat( result.getString("OutputFormat"));
				group.setTransportMethod(TransportMethod.valueOf(result.getString("TransportMethod")));
				group.setStatus(result.getString("Status"));
				group.setInvoiceCount(result.getInt("InvoiceCount"));
				group.setCustBatchCount(result.getInt("CustomerBatchCount"));
				group.setSelectedPrinter(result.getString("PrinterSelected"));
				
				java.sql.Timestamp submittedTime = result.getTimestamp("DateSubmitted");
				if (submittedTime != null)
					group.setSubmittedDate(new java.util.Date(result.getTimestamp("DateSubmitted").getTime()));
				group.setScheduleOutputFormat(result.getString("ScheduleOutputFormat"));
				groups.add(group);
			}
           
			result.close();
			stmt.close();
		} catch (Exception e) {
			logger.log("Unable to retrieve incompleted batch groups: " + e.toString());
			throw new DBAccessException("Unable to retrieve incompleted batch groups: " + e.getMessage());
		} finally {
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }	
		}
		
		return groups;
	}
	
	public static BatchGroup findById(long batchGroupID)
	throws DBAccessException {
		Connection conn = null;
		PreparedStatement stmt = null;
		BatchGroup bGroup = null;
		
		try {
			String query = "select * from batchgroup WHERE ID = ? ";


			conn = DatabaseConnector.getConnection();
			stmt = conn.prepareStatement(query);

			stmt.setLong(1, batchGroupID);
			
			ResultSet result = stmt.executeQuery();

			if (result.next()) {
				bGroup = new BatchGroup();
				bGroup.setStatus( result.getString("Status"));
				bGroup.setId(result.getLong("ID"));
				bGroup.setSelectedPrinter(result.getString("PrinterSelected"));
				bGroup.setBrand(result.getString("Brand_Group"));
			}
		} catch (Exception e) {
			logger.log("ERROR: Unable to update status for batch groups with error " + e.toString());
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			try {
				if (stmt != null) 
					stmt.close();
				DatabaseConnector.returnConnection(conn); 
			} catch (Exception e) {}
		}
		
		return bGroup;
	}
	public static boolean isBatchGroupStarted(long batchGroupID)
	throws DBAccessException {

		Connection conn = null;
		PreparedStatement stmt = null;
		String status = null;
		try {
			String query = "select Status from batchgroup WHERE ID = ? ";


			conn = DatabaseConnector.getConnection();
			stmt = conn.prepareStatement(query);

			stmt.setLong(1, batchGroupID);
			
			ResultSet result = stmt.executeQuery(query);

			if (result.next()) {
				status = result.getString("Status");
			}
			
		} catch (Exception e) {
			logger.log("ERROR: Unable to update status for batch groups with error " + e.toString());
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			try {
				if (stmt != null) 
					stmt.close();
				DatabaseConnector.returnConnection(conn); 
			} catch (Exception e) {}
		}
		
		return BatchGroup.Status.STARTED.equals(status);
	}
	
	public static List<BatchGroup> findCompletedBatchGroups(java.util.Date date) 	
	throws DBAccessException {

		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			String query = "select * from batchgroup bg, batch b " +
					" WHERE bg.Status IN('COMPLETED','CANCELLED') "+
					" AND bg.BatchID = b.BatchID AND bg.DateSubmitted > ? ORDER BY bg.BatchID DESC";

			conn = DatabaseConnector.getConnection();
			stmt = conn.prepareStatement(query);

			stmt.setTimestamp(1, new Timestamp(date.getTime()));
			ResultSet result = stmt.executeQuery();
			
			return createBatchGroups(result);
		} catch (Exception e) {
			logger.log("Unable to retrieve completed batch groups: " + e.toString());
			throw new DBAccessException("Unable to retrieve completed batch groups: " + e.getMessage());
		} finally {
			try { 
				if (stmt != null)
					stmt.close();
				DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }	
		}
	}
	
	/** @see InvoiceFileArchiver */
	public static List<BatchGroup> findCompletedAndCancelledByDates(java.util.Date startDate, java.util.Date endDate) 	
	throws DBAccessException {

		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			String query = "select * from batchgroup bg, batch b " +
					" WHERE b.BatchID = bg.BatchID AND Status IN('COMPLETED','CANCELLED') "+
					" AND DateSubmitted >= ? AND DateSubmitted <= ? ORDER BY bg.BatchID ASC";

			conn = DatabaseConnector.getConnection();
			stmt = conn.prepareStatement(query);

			stmt.setTimestamp(1, new Timestamp(startDate.getTime()));
			stmt.setTimestamp(2, new Timestamp(endDate.getTime()));
			ResultSet result = stmt.executeQuery();
			
			return createDeepBatchGroups(result);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBAccessException("Unable to retrieve completed batch groups: " + e.getMessage());
		} finally {
			try { 
				if (stmt != null)
					stmt.close();
				DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }	
		}
	}
	
	/** Creates and returns a list of BatchGroup based on the given ResultSet */
	private static List<BatchGroup> createBatchGroups(ResultSet result) 
	throws SQLException {
		List<BatchGroup> groups = new ArrayList<BatchGroup>();
		
		while (result.next()) {
			BatchGroup group = new BatchGroup();
			group.setBatchId(result.getLong("BatchID"));
			group.setRunId(result.getString("RunID"));
			group.setUserId(result.getString("UserID"));
			group.setDescription(result.getString("Description"));
			group.setId(result.getLong("ID"));
			group.setBrand(result.getString("Brand_Group"));
			group.setOutputFileFormat( result.getString("OutputFormat"));
			group.setTransportMethod(TransportMethod.valueOf(result.getString("TransportMethod")));
			group.setStatus(result.getString("Status"));
			group.setInvoiceCount(result.getInt("InvoiceCount"));
			group.setCustBatchCount(result.getInt("CustomerBatchCount"));
			group.setSelectedPrinter(result.getString("PrinterSelected"));
			
			java.sql.Timestamp submittedTime = result.getTimestamp("DateSubmitted");
			if (submittedTime != null)
				group.setSubmittedDate(new java.util.Date(result.getTimestamp("DateSubmitted").getTime()));
			group.setScheduleOutputFormat(result.getString("ScheduleOutputFormat"));
			groups.add(group);
		}
		
		return groups;
	}
	
	/** Creates and returns a list of BatchGroup based on the given ResultSet */
	private static List<BatchGroup> createDeepBatchGroups(ResultSet result) 
	throws Exception {
		List<BatchGroup> groups = createBatchGroups(result);
		
		for (BatchGroup group: groups) {
			group.setCustBatches(CustomerBatchDataAccess.findDeepCustomerBatchesByBatchGroupId(group.getId()));
		}
		return groups;
	}
	
}
