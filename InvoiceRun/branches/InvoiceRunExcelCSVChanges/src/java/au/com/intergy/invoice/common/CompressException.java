package au.com.intergy.invoice.common;

public class CompressException extends Exception {

	public CompressException(String message) {
		super(message);
	}
}
