package au.com.intergy.invoice.common;


import java.io.ByteArrayOutputStream; 
import java.io.UnsupportedEncodingException;

import java.util.zip.DataFormatException; 

import java.util.zip.Deflater; 

import java.util.zip.Inflater; 

 

public class CompressUncompressStrings { 

 

    public static void main(String[] args) { 

    	try {
 

        String myString = "This is my String.09o<invoicelist>"; 

        String compressedMyString = compressString(myString); 

 

        System.out.println(); 

        System.out.println("-------------------------------------------------------------------"); 

        System.out.println("myByte has been compressed to compressedMyString whose value is : " 

                                                                                + compressedMyString); 

        System.out.println("Now the String has been Compressed and placed in compressedMyString"); 

        System.out.println("Lets Uncompress the Compressed String back to the Original myByte"); 

        System.out.println("--------------------------------------------------------------------"); 

        System.out.println(); 

        String deCompressedMyString = deCompressString(compressedMyString); 

 

        System.out.println("compressedMyString has been decompressed to  : "+ deCompressedMyString); 

 
    	} catch (UnsupportedEncodingException uex) {
    		uex.printStackTrace();
    	}
    } 

 

    public static String compressString(String myString) throws UnsupportedEncodingException{ 

        byte[] myByte = myString.getBytes(); 

        // Compression level of best compression 
        Deflater compressor = new Deflater(); 
        compressor.setLevel(Deflater.BEST_COMPRESSION); 
 
        // Give the Compressor the input data to compress 
        compressor.setInput(myByte); 
        compressor.finish(); 
 
        // Create an expandable byte array to hold the compressed data. 
        // It is not necessary that the compressed data will be smaller than the 
        // uncompressed data. 
        ByteArrayOutputStream bos = new ByteArrayOutputStream(myByte.length); 
 
        // Compress the data 
        byte[] buf = new byte[1024]; 
        while (!compressor.finished()) { 
            int count = compressor.deflate(buf); 
            bos.write(buf, 0, count); 
        } 
 
        try { 

            bos.close(); 

        } catch (Exception e) { 

            e.printStackTrace(); 

        } 

 

        // Get the compressed data 

        byte[] compressedMyByte = bos.toByteArray(); 

        String compressedMyString = new String(compressedMyByte); 

 

        return compressedMyString; 

    } 

 

    public static String deCompressString(String compressedMyString) throws UnsupportedEncodingException { 
        Inflater decompressor = new Inflater(); 

        byte[] buf = new byte[1024]; 

        byte[] compressedMyByte = compressedMyString.getBytes(); 

        decompressor.setInput(compressedMyByte); 

        // Create an expandable byte array to hold the decompressed data 
        ByteArrayOutputStream bos = new ByteArrayOutputStream(compressedMyByte.length); 

        // Decompress the data 

        buf = new byte[1024]; 

        while (!decompressor.finished()) { 
            try { 
                int count = decompressor.inflate(buf); 
                bos.write(buf, 0, count); 
            } catch (DataFormatException e) { 
            	e.printStackTrace();
            	break;
            } 
        } 
        try { 
            bos.close(); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 

 

        // Get the decompressed data 

        byte[] decompressedMyByte = bos.toByteArray(); 
        String decompressedMyString = new String(decompressedMyByte); 

 

        return decompressedMyString; 
    } 

}  
