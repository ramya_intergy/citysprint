package au.com.intergy.invoice.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import au.com.intergy.invoice.common.CustomerBatch.Status;

/**
 * 
 * @author Chi Bui
 *
 */
public class CustomerBatchDataAccess {
	public static void create(Connection conn, CustomerBatch custBatch) 
	throws DBAccessException {

		try {
			String query = "INSERT INTO customerbatchdetails " +
			"(BatchGroupID, BatchID_CustKey, CustomerKey, TransportMethod, OutputFormat, Brand_Group, CreditController, "+
			" CreditControllerEmail, CreditControllerPhone, Status, TransportAddress, TransportUsername, TransportPassword, EmailTemplate, "
                                + " CreditControllerTitle, CreditControllerOffice, CreditControllerFax, ScheduleOutputFormat)"+
			"  VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			PreparedStatement stmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

			stmt.setLong(1, custBatch.getBatchGroupId());
			stmt.setLong(2, custBatch.getCustomerBatchId());
			stmt.setString(3, custBatch.getCustomerKey());
			stmt.setString(4, custBatch.getTransportMethod().name());
			stmt.setString(5, custBatch.getOutputFileFormat());
			stmt.setString(6, custBatch.getBrand());
			stmt.setString(7, custBatch.getCreditControllerName());
			stmt.setString(8, custBatch.getCreditControllerEmail());
			stmt.setString(9, custBatch.getCreditControllerPhone());
			stmt.setString(10, custBatch.getStatus().name());
			stmt.setString(11, custBatch.getTransportAddress());
			stmt.setString(12, custBatch.getTransportUsername());
			stmt.setString(13, custBatch.getTransportPassword());
			
                        stmt.setString(14, custBatch.getTransportEmailTemplate());
                        
                        stmt.setString(15, custBatch.getCreditControllerTitle());
                        stmt.setString(16, custBatch.getCreditControllerOffice());
                        stmt.setString(17, custBatch.getCreditControllerFaxNo());
                        stmt.setString(18, custBatch.getScheduleOutputFormat());
                        System.out.println("############# custBatch.getCreditControllerTitle() ==" + custBatch.getCreditControllerTitle()
                                + ", custBatch.getCreditControllerOffice() ==" + custBatch.getCreditControllerOffice()
                                +", custBatch.getCreditControllerFaxNo()==" + custBatch.getCreditControllerFaxNo());
                        
			int result = stmt.executeUpdate();
			if (result == 0)
				throw new DBAccessException("Unable to save to DB: " + custBatch.toString());
			
			
			ResultSet keys = stmt.getGeneratedKeys();
			long custBatchId = 0;
			if (keys.next()) 
				custBatchId = keys.getLong(1);
			
			if (custBatchId == 0)
				throw new DBAccessException("Unable to retrieve cust batch ID for " + custBatch.toString());
			
			//save statement
			if (custBatch.getStatement() != null) {
				custBatch.getStatement().setCustBatchId(custBatchId);
				InvoiceDataAccess.createStatement(conn, custBatch.getStatement());
			}
			
			for (Invoice invoice : custBatch.getInvoices()) {
				invoice.setCustBatchId(custBatchId);
				InvoiceDataAccess.createStatement(conn, invoice);
			}
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBAccessException("Unable to create cust batch" + custBatch.toString() + e.getMessage());
		} 
	}
	
	public static void create(CustomerBatch custBatch) 
	throws DBAccessException {
		Connection conn = null;

		try {
			create(DatabaseConnector.getConnection(), custBatch);
		} catch (Exception e) {
			throw new DBAccessException(e.getMessage());
		} finally {
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }	
		}
	}

	public static CustomerBatch findById(long custBatchId) 
	throws DBAccessException {
		CustomerBatch custBatchResult = null;

		PreparedStatement stmt = null;
		
		Connection conn = null;
		try {
			
			ResultSet rstCustBatches  = null;
			conn = DatabaseConnector.getConnection();
			
			String sqlEmail = "SELECT * FROM customerbatchdetails WHERE BatchID_CustKey = ?";
			
			stmt = conn.prepareStatement(sqlEmail);
			stmt.setLong(1, custBatchId);
			
			rstCustBatches = stmt.executeQuery();

			//assume the DB has no more than 1 row with the provided cust batch ID
			if (rstCustBatches.next())
			{
				custBatchResult  = new CustomerBatch();
				custBatchResult.setCustomerKey(rstCustBatches.getString("CustomerKey"));
				custBatchResult.setBatchGroupId(rstCustBatches.getLong("BatchGroupID"));
				custBatchResult.setCustomerBatchId(custBatchId);
				custBatchResult.setStatus(Status.valueOf(rstCustBatches.getString("Status")));
				custBatchResult.setBrand(rstCustBatches.getString("Brand_Group"));
				custBatchResult.setTransportMethod(
						TransportMethod.valueOf(rstCustBatches.getString("TransportMethod")));
				custBatchResult.setOutputFileFormat(rstCustBatches.getString("OutputFormat"));
				custBatchResult.setCreditControllerName(rstCustBatches.getString("CreditController"));
				custBatchResult.setCreditControllerEmail(rstCustBatches.getString("CreditControllerEmail"));
				custBatchResult.setCreditControllerPhone(rstCustBatches.getString("CreditControllerPhone"));
                                
                                custBatchResult.setCreditControllerTitle(rstCustBatches.getString("CreditControllerTitle"));
				custBatchResult.setCreditControllerOffice(rstCustBatches.getString("CreditControllerOffice"));
				custBatchResult.setCreditControllerFaxNo(rstCustBatches.getString("CreditControllerFax"));
                                
				custBatchResult.setTransportAddress(rstCustBatches.getString("TransportAddress"));
			
				custBatchResult.setErrorMessage(rstCustBatches.getString("ErrorMessage"));
				
				java.sql.Time errorTime = rstCustBatches.getTime("ErrorSentDateTime");
				custBatchResult.setErrorSentDateTime( errorTime == null? null : new Date(errorTime.getTime()));
                                custBatchResult.setScheduleOutputFormat(rstCustBatches.getString("ScheduleOutputFormat"));
			}

			stmt.close();
		} catch (SQLException sqle) {
			throw new DBAccessException(sqle.getMessage());
		} finally {
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }	
		}
		
		return custBatchResult;
	}
	
	public static void updateStatus(long custBatchId, String newStatus) 
	throws DBAccessException{
		
		Connection conn = null;

		try {
			String query = "UPDATE customerbatchdetails SET Status = ? WHERE BatchID_CustKey = ?";

			conn = DatabaseConnector.getConnection();
			PreparedStatement stmt = conn.prepareStatement(query);

			stmt.setString(1, newStatus);
			stmt.setLong(2, custBatchId);

			stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			//TODO log stacktrace
			throw new DBAccessException(e.getMessage());
		} finally {
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }	
		}
	}
		
	public static void updateStatusByBatchGroupIds(Connection conn, String[] batchGroupIDs, String newStatus) 
	throws DBAccessException {
		Statement stmt = null;
		
		try {
			StringBuilder sbuilder = new StringBuilder();
			sbuilder.append("UPDATE customerbatchdetails SET Status = '");
			sbuilder.append(newStatus);
			sbuilder.append("' WHERE BatchGroupID IN (");
			
			for (int i = 0; i < batchGroupIDs.length; i++) {
				sbuilder.append(batchGroupIDs[i]);
				if (i < batchGroupIDs.length - 1) 
					sbuilder.append(", ");
			}
			sbuilder.append(" ) ");
			
			stmt = conn.createStatement();

			stmt.executeUpdate(sbuilder.toString());

		} catch (Exception e) {
			throw new DBAccessException(e.getMessage());
		} finally {
			if (stmt != null)
				try {stmt.close();} catch (SQLException se) {}
		}
	}
	
	public static void updateStatusByBatchGroupIds(String[] batchGroupIDs, String newStatus) 
	throws DBAccessException {
		Connection conn = null;

		try {
			StringBuilder sbuilder = new StringBuilder();
			sbuilder.append("UPDATE customerbatchdetails SET Status = '");
			sbuilder.append(newStatus);
			sbuilder.append("' WHERE BatchGroupID IN (");
			
			for (int i = 0; i < batchGroupIDs.length; i++) {
				sbuilder.append(batchGroupIDs[i]);
				if (i < batchGroupIDs.length - 1) 
					sbuilder.append(", ");
			}
			sbuilder.append(" ) ");
			
			conn = DatabaseConnector.getConnection();
			Statement stmt = conn.createStatement();

			stmt.executeUpdate(sbuilder.toString());

			stmt.close();
		} catch (Exception e) {
			throw new DBAccessException(e.getMessage());
		} finally {
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }	
		}
	}
	
	/**
	 * 
	 * @param currentStatus Only update cust batch with this current status e.g. PROCESSING
	 * @throws DBAccessException
	 */
	public static void updateStatusByBatchGroupIdsAndCurrentStatus(String[] batchGroupIDs, 
			 String currentStatus, String newStatus) 
	throws DBAccessException {
		Connection conn = null;

		try {
			StringBuilder sbuilder = new StringBuilder();
			sbuilder.append("UPDATE customerbatchdetails SET Status = '");
			sbuilder.append(newStatus);
			sbuilder.append("' WHERE BatchGroupID IN (");
			
			for (int i = 0; i < batchGroupIDs.length; i++) {
				sbuilder.append(batchGroupIDs[i]);
				if (i < batchGroupIDs.length - 1) 
					sbuilder.append(", ");
			}
			sbuilder.append(" ) AND Status = '");
			sbuilder.append(currentStatus + "'");
			
			conn = DatabaseConnector.getConnection();
			Statement stmt = conn.createStatement();

			stmt.executeUpdate(sbuilder.toString());

			stmt.close();
		} catch (Exception e) {
			//TODO log stacktrace
			throw new DBAccessException(e.getMessage());
		} finally {
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }	
		}
	}
	
	public static void updateStatusByBatchGroupIdsAndCurrentStatus(Connection conn, 
			String[] batchGroupIDs, String currentStatus, String newStatus) 
	throws DBAccessException {
		Statement stmt = null;
		
		try {
			StringBuilder sbuilder = new StringBuilder();
			sbuilder.append("UPDATE customerbatchdetails SET Status = '");
			sbuilder.append(newStatus);
			sbuilder.append("' WHERE BatchGroupID IN (");
			
			for (int i = 0; i < batchGroupIDs.length; i++) {
				sbuilder.append(batchGroupIDs[i]);
				if (i < batchGroupIDs.length - 1) 
					sbuilder.append(", ");
			}
			sbuilder.append(" ) AND Status = '");
			sbuilder.append(currentStatus + "'");

			stmt = conn.createStatement();
			stmt.executeUpdate(sbuilder.toString());
		} catch (Exception e) {
			throw new DBAccessException(e.getMessage());
		} finally {
			if (stmt != null)
				try {stmt.close();} catch (SQLException se) {}
		}
	}
	
	
	
	public static void updateGeneratedFlag(long custBatchID) 
	throws DBAccessException{
		
		Connection conn = null;

		try {
			String query = "UPDATE customerbatchdetails SET Generated = true " +
					"WHERE BatchID_CustKey = " + custBatchID;

			conn = DatabaseConnector.getConnection();
			Statement stmt = conn.createStatement();

			stmt.executeUpdate(query);
			stmt.close();
		} catch (Exception e) {
			throw new DBAccessException(e.getMessage());
		} finally {
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }	
		}
	}
	
	public static void updateToFail(long custBatchId, String newStatus) 
	throws DBAccessException{
		
		Connection conn = null;

		try {
			String query = "UPDATE customerbatchdetails "+
			" SET Status = ?" +
			" WHERE BatchID_CustKey = ?";


			conn = DatabaseConnector.getConnection();
			PreparedStatement stmt = conn.prepareStatement(query);

			stmt.setString(1, newStatus);
			stmt.setLong(2, custBatchId);

			stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			throw new DBAccessException(e.getMessage());
		} finally {
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }	
		}
	}
	

	/**
	 * Returns customer batches that status = PROCESSING, transport method = EMAIL
	 * and Generated flag = true.
	 * @param resultLimit If resultLimit < 1, there will be no limit.
	 * @return
	 * @throws DBAccessException
	 */
	public static Set<CustomerBatch> findNotSentEmailCustomerBatches(int resultLimit) 
	throws DBAccessException { 
		
		Set<CustomerBatch> custBatchesResult = new LinkedHashSet<CustomerBatch>();

		PreparedStatement stmt = null;
		boolean hasLimit = resultLimit > 0;
		Connection conn = null;
		
		try {
			
			ResultSet rstCustBatches  = null;
			CustomerBatch custBatch = null;
			conn = DatabaseConnector.getConnection();
			
			String sqlEmail = "SELECT * FROM customerbatchdetails " +
			" WHERE TransportMethod = ? AND Status=? AND Generated = true ORDER BY BatchID_CustKey ASC " +
			(hasLimit?" LIMIT ?" : "");
			
			stmt = conn.prepareStatement(sqlEmail);

			stmt.setString(1, TransportMethod.EMAIL.name());
			stmt.setString(2, Status.PROCESSING.name());
			if (hasLimit)
				stmt.setInt(3, resultLimit);
			
			rstCustBatches = stmt.executeQuery();

			while(rstCustBatches.next())
			{
				custBatch = new CustomerBatch();
				custBatch.setCustomerBatchId(rstCustBatches.getLong("BatchID_CustKey"));
				custBatch.setCustomerKey(rstCustBatches.getString("CustomerKey"));
				custBatch.setBatchGroupId(rstCustBatches.getLong("BatchGroupID"));
				custBatch.setBrand(rstCustBatches.getString("Brand_Group"));
				custBatch.setCreditControllerName(rstCustBatches.getString("CreditController"));
				custBatch.setCreditControllerEmail(rstCustBatches.getString("CreditControllerEmail"));
				custBatch.setCreditControllerPhone(rstCustBatches.getString("CreditControllerPhone"));
				
                                custBatch.setCreditControllerTitle(rstCustBatches.getString("CreditControllerTitle"));
                                custBatch.setCreditControllerOffice(rstCustBatches.getString("CreditControllerOffice"));
                                custBatch.setCreditControllerFaxNo(rstCustBatches.getString("CreditControllerFax"));
                                
                                custBatch.setTransportAddress(rstCustBatches.getString("TransportAddress"));
				custBatch.setOutputFileFormat(rstCustBatches.getString("OutputFormat"));
				custBatch.setScheduleOutputFormat(rstCustBatches.getString("ScheduleOutputFormat"));
                                custBatch.setTransportEmailTemplate(rstCustBatches.getString("EmailTemplate"));
                                
				custBatchesResult.add(custBatch);
			}

			stmt.close();
		} catch (SQLException sqle) {
                    sqle.printStackTrace();
			throw new DBAccessException(sqle.getMessage());
		} finally {
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }	
		}
		
		return custBatchesResult;
	}
	
	
	/**
	 * Returns customer batches that have Status = PROCESSING, transport method = PRINT
	 * and Generated flag = true.
	 * @param resultLimit If resultLimit < 1, there will be no limit.
	 * @return CustomerBatches and their statement and invoices
	 * @throws DBAccessException
	 */
	public static Set<CustomerBatch> findNotPrintedCustomerBatches(int resultLimit) 
	throws DBAccessException { 
		
		Set<CustomerBatch> custBatchesResult = new LinkedHashSet<CustomerBatch>();

		PreparedStatement stmt = null;
		boolean hasLimit = resultLimit > 0;
		Connection conn= null;
		
		try {
			
			ResultSet rstCustBatches  = null;
			CustomerBatch custBatch = null;
			conn = DatabaseConnector.getConnection();
			
			String sqlCustBatch = "SELECT * FROM customerbatchdetails " +
			" WHERE TransportMethod = ? AND Status=? AND Generated = true ORDER BY BatchID_CustKey " +
			(hasLimit?" LIMIT ?" : "");
			
			stmt = conn.prepareStatement(sqlCustBatch);

			stmt.setString(1, TransportMethod.PRINT.name());
			stmt.setString(2, Status.PROCESSING.name());
			if (hasLimit)
				stmt.setInt(3, resultLimit);
			
			rstCustBatches = stmt.executeQuery();

			while(rstCustBatches.next())
			{
				custBatch = new CustomerBatch();
				custBatch.setCustomerBatchId(rstCustBatches.getLong("BatchID_CustKey"));
				custBatch.setCustomerKey(rstCustBatches.getString("CustomerKey"));
				custBatch.setBatchGroupId(rstCustBatches.getLong("BatchGroupID"));
				custBatch.setBrand(rstCustBatches.getString("Brand_Group"));
				custBatch.setStatus(Status.valueOf(rstCustBatches.getString("Status")) );
				custBatch.setCreditControllerName(rstCustBatches.getString("CreditController"));
				custBatch.setCreditControllerEmail(rstCustBatches.getString("CreditControllerEmail"));
				custBatch.setCreditControllerPhone(rstCustBatches.getString("CreditControllerPhone"));
                                
                                custBatch.setCreditControllerTitle(rstCustBatches.getString("CreditControllerTitle"));
				custBatch.setCreditControllerOffice(rstCustBatches.getString("CreditControllerOffice"));
				custBatch.setCreditControllerFaxNo(rstCustBatches.getString("CreditControllerFax"));
                                
				custBatch.setTransportAddress(rstCustBatches.getString("TransportAddress"));
				custBatch.setOutputFileFormat(rstCustBatches.getString("OutputFormat"));
                                custBatch.setScheduleOutputFormat(rstCustBatches.getString("ScheduleOutputFormat"));
				
				custBatchesResult.add(custBatch);
				Set<au.com.intergy.invoice.common.Statement> statements = 
					InvoiceDataAccess.findByCustBatchIDForPrinting(conn, custBatch.getCustomerBatchId());
				for (au.com.intergy.invoice.common.Statement statement : statements) {
					statement.setCustBatch(custBatch);
					if (statement instanceof Invoice) {
						custBatch.addInvoice((Invoice) statement);
					} else
						custBatch.setStatement(statement);
				}
			}
			stmt.close();
		} catch (SQLException sqle) {
			throw new DBAccessException(sqle.getMessage());
		} finally {
			try {DatabaseConnector.returnConnection(conn);} catch (SQLException sqle) {}
		}
		
		return custBatchesResult;
	}
	
	/**
	 * @return Customer batches with status = INIT and TransportMethod = 'Email'
	 * @throws DBAccessException
	 */
	public static Set<CustomerBatch> findNotSentEmailCustomerBatches() 
	throws DBAccessException {
		return findNotSentEmailCustomerBatches(-1);
	}
	
	/**
	 * Returns customer batches that have Status = PROCESSING, transport method = FTP
	 * and Generated flag = true.
	 * @param resultLimit If resultLimit < 1, there will be no limit.
	 * @return CustomerBatches and their statement and invoices
	 * @throws DBAccessException
	 */
	public static Set<CustomerBatch> findNotTransferredCustomerBatches(int resultLimit) 
	throws DBAccessException {
		return findNotDeliveredCustomerBatches(TransportMethod.FTP.name(), resultLimit);
	}

        /**
	 * Returns customer batches that have Status = PROCESSING, transport method = FTPS
	 * and Generated flag = true.
	 * @param resultLimit If resultLimit < 1, there will be no limit.
	 * @return CustomerBatches and their statement and invoices
	 * @throws DBAccessException
	 */
	public static Set<CustomerBatch> findNotTransferredFTPSCustomerBatches(int resultLimit)
	throws DBAccessException {
		return findNotDeliveredCustomerBatches(TransportMethod.FTPS.name(), resultLimit);
	}
        
	/**
	 * Returns customer batches that have Status = PROCESSING and Generated flag = true
	 * for the provided transport method.
	 * 
	 * @param resultLimit If resultLimit < 1, there will be no limit.
	 * @return CustomerBatches and their statement and invoices
	 * @throws DBAccessException
	 */
	private static Set<CustomerBatch> findNotDeliveredCustomerBatches(String transportMethod, int resultLimit) 
	throws DBAccessException { 
		
		Set<CustomerBatch> custBatchesResult = new LinkedHashSet<CustomerBatch>();

		PreparedStatement stmt = null;
		boolean hasLimit = resultLimit > 0;
		Connection conn= null;
		
		try {
			
			ResultSet rstCustBatches  = null;
			CustomerBatch custBatch = null;
			conn = DatabaseConnector.getConnection();
			
			String sqlCustBatch = "SELECT * FROM customerbatchdetails " +
			" WHERE TransportMethod = ? AND Status=? AND Generated = true ORDER BY BatchID_CustKey " +
			(hasLimit?" LIMIT ?" : "");
			
			stmt = conn.prepareStatement(sqlCustBatch);

			stmt.setString(1, transportMethod);
			stmt.setString(2, Status.PROCESSING.name());
			if (hasLimit)
				stmt.setInt(3, resultLimit);
			
			rstCustBatches = stmt.executeQuery();

			while(rstCustBatches.next())
			{
				custBatch = new CustomerBatch();
				custBatch.setCustomerBatchId(rstCustBatches.getLong("BatchID_CustKey"));
				custBatch.setCustomerKey(rstCustBatches.getString("CustomerKey"));
				custBatch.setBatchGroupId(rstCustBatches.getLong("BatchGroupID"));
				custBatch.setBrand(rstCustBatches.getString("Brand_Group"));
				custBatch.setStatus(Status.valueOf(rstCustBatches.getString("Status")) );
				custBatch.setCreditControllerName(rstCustBatches.getString("CreditController"));
				custBatch.setCreditControllerEmail(rstCustBatches.getString("CreditControllerEmail"));
				custBatch.setCreditControllerPhone(rstCustBatches.getString("CreditControllerPhone"));
                                
                                custBatch.setCreditControllerTitle(rstCustBatches.getString("CreditControllerTitle"));
				custBatch.setCreditControllerOffice(rstCustBatches.getString("CreditControllerOffice"));
				custBatch.setCreditControllerFaxNo(rstCustBatches.getString("CreditControllerFax"));
                                
				custBatch.setOutputFileFormat(rstCustBatches.getString("OutputFormat"));
				custBatch.setTransportAddress(rstCustBatches.getString("TransportAddress"));
				custBatch.setTransportUsername(rstCustBatches.getString("TransportUsername"));
				custBatch.setTransportPassword(rstCustBatches.getString("TransportPassword"));
				custBatch.setScheduleOutputFormat(rstCustBatches.getString("ScheduleOutputFormat"));

				custBatchesResult.add(custBatch);
				Set<au.com.intergy.invoice.common.Statement> statements = 
					InvoiceDataAccess.findByCustBatchIDForPrinting(conn, custBatch.getCustomerBatchId());
				for (au.com.intergy.invoice.common.Statement statement : statements) {
					if (statement instanceof Invoice) {
						custBatch.addInvoice((Invoice) statement);
					} else
						custBatch.setStatement(statement);
				}
			}
			stmt.close();
		} catch (SQLException sqle) {
			throw new DBAccessException(sqle.getMessage());
		} finally {
			try {DatabaseConnector.returnConnection(conn);} catch (SQLException sqle) {}
		}
		
		return custBatchesResult;
	}
	
	public static List<CustomerBatch> findCustomerBatchesByBatchGroupId(Connection conn, Long batchGroupId) 
	throws Exception {
		List<CustomerBatch> custBatchesResult = new ArrayList<CustomerBatch>();
		
		Statement stmt = null;
		ResultSet rstEmail  = null;
		CustomerBatch custBatch = null;
		
		stmt = conn.createStatement();
		String sqlEmail = "SELECT * FROM customerbatchdetails " +
		                  "WHERE BatchGroupID='"+batchGroupId+"'" ;
		
		rstEmail = stmt.executeQuery(sqlEmail);
		
		while(rstEmail.next())
		{
			custBatch = new CustomerBatch();
			custBatch.setCustomerBatchId(rstEmail.getLong("BatchID_CustKey"));
			custBatch.setCustomerKey(rstEmail.getString("CustomerKey"));
			custBatch.setBrand(rstEmail.getString("Brand_Group"));
			custBatch.setStatus(Status.valueOf(rstEmail.getString("Status")));
			custBatch.setCreditControllerName(rstEmail.getString("CreditController"));
			custBatch.setCreditControllerEmail(rstEmail.getString("CreditControllerEmail"));
			custBatch.setCreditControllerPhone(rstEmail.getString("CreditControllerPhone"));
                        
                        custBatch.setCreditControllerTitle(rstEmail.getString("CreditControllerTitle"));
			custBatch.setCreditControllerOffice(rstEmail.getString("CreditControllerOffice"));
			custBatch.setCreditControllerFaxNo(rstEmail.getString("CreditControllerFax"));
                                
			custBatchesResult.add(custBatch);
		}
		
		stmt.close();
		return custBatchesResult;
	}
	
	/** 
	 * 
	 * @return Customer batches with invoices. Those invoice objects do not
	 * have XML body.
	 * 
	 * XML body is used in generating files only.
	 */
	public static List<CustomerBatch> findDeepCustomerBatchesByBatchGroupId(Long batchGroupId) 
	throws DBAccessException {
		Connection conn = null;
		List<CustomerBatch> custBatchesResult = new ArrayList<CustomerBatch>();
		
		try {
			conn = DatabaseConnector.getConnection();
			custBatchesResult = findCustomerBatchesByBatchGroupId(conn, batchGroupId);
			
			for (CustomerBatch custBatch : custBatchesResult) {
				Set<au.com.intergy.invoice.common.Statement> invAndStatements = 
					InvoiceDataAccess.findShallowByCustBatchID(custBatch.getCustomerBatchId());
				for (au.com.intergy.invoice.common.Statement stmt : invAndStatements) {
					if (stmt instanceof Invoice) {
						custBatch.addInvoice((Invoice) stmt);
					} else
						custBatch.setStatement(stmt);
				}
			}
			
		} catch (Exception e) {
            throw new DBAccessException(e.toString());
		} finally {
			try {DatabaseConnector.returnConnection(conn);} catch (SQLException se) {}
		}
		
		return custBatchesResult;
	}
}
