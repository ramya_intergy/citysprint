package au.com.intergy.invoice.common;

public class DBAccessException extends Exception {

	private static final long serialVersionUID = 14417554784756L;
	
	public DBAccessException(String message) {
		super(message);
	}
}
