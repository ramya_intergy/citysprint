package au.com.intergy.invoice.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	public static String getCurrentDateTime (String datePattern) {
		Date now = new Date();
		
		SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
		return formatter.format(now);
	}
	
	public static String toStringDate(String datePattern, Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
		return formatter.format(date);
	}
	
	public static Calendar floorCalendar(Calendar cal) {
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}
	
	public static Date toDate(String datePattern, String strDate) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
		return formatter.parse(strDate);
	}
}
