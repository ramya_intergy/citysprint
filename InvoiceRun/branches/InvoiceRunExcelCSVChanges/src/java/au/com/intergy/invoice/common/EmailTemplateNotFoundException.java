package au.com.intergy.invoice.common;

public class EmailTemplateNotFoundException extends Exception {

	private static final long serialVersionUID = 14451755458756L;
	
	public EmailTemplateNotFoundException(String message) {
		super(message);
	}
}
