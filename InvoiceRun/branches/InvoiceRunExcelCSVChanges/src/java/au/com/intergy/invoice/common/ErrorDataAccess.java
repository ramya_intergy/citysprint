package au.com.intergy.invoice.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ErrorDataAccess {

	public static void addError(String type, long referenceId, String message, String brand, String custNo) 
	throws DBAccessException {
		Connection conn = null;
		
		try {
			conn = DatabaseConnector.getConnection();
			String query = "INSERT INTO errorlog (Type, ReferenceID, Message, Brand, CustomerNumber)  VALUES (?, ?, ?, ?, ?)";
			
			PreparedStatement stmt = conn.prepareStatement(query);

			stmt.setString(1, type);
			stmt.setLong(2, referenceId);
			//300 is the allocated length for Message field
			stmt.setString(3, message.length()<=300?message:message.substring(0, 299));
			stmt.setString(4, brand);
			stmt.setString(5, custNo);
			stmt.executeUpdate();			
			
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
			try { 
				if (conn != null)
					conn.rollback(); 
			} catch (SQLException sqle) { }		
			throw new DBAccessException("ErrorDataAccess: Unable to log an error : " + e.getMessage());
		} finally {
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException sqle) { }			
		}
	}
	
	public static List<InvoiceError> getNotSentErrors() 
	throws DBAccessException {
		Connection conn = null;
		List<InvoiceError> errors = new ArrayList<InvoiceError>();

		try {
			conn = DatabaseConnector.getConnection();
			String query = "SELECT * from errorlog where SentDateTime is null";
			
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery(query);			
			
			while(result.next()) {
				InvoiceError error = new InvoiceError();
				
				error.setId(result.getLong("ID"));
				error.setReferenceId(result.getLong("ReferenceID"));
				error.setType(result.getString("Type"));
				error.setMessage(result.getString("Message"));
				error.setBrand(result.getString("Brand"));
				error.setAccountNumber(result.getString("CustomerNumber"));
				error.setCreatedDate(new java.util.Date(result.getTimestamp("DateCreated").getTime()));
				errors.add(error);
			}
			
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
			try { 
				if (conn != null)
					conn.rollback(); 
				} catch (SQLException sqle) { }		
			throw new DBAccessException("ErrorDataAccess: Unable to retrieve not sent errors : " + e.getMessage());
		} finally {
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException sqle) { }			
		}
		
		return errors;
	}
	
	public static void updateSentDateTime(long errorId) 
	throws DBAccessException {
		Connection conn = null;

		try {
			conn = DatabaseConnector.getConnection();
			String query = "UPDATE errorlog SET SentDateTime = ? WHERE ID = ?";
			
			PreparedStatement stmt = conn.prepareStatement(query);

			stmt.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
			stmt.setLong(2, errorId);
			stmt.executeUpdate();			
			
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
			try { 
				if (conn != null)
					conn.rollback(); 
			} catch (SQLException sqle) { }		
			throw new DBAccessException("ErrorDataAccess: Unable to update error sent date time : " + e.getMessage());
		} finally {
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException sqle) { }			
		}
	}
	
}
