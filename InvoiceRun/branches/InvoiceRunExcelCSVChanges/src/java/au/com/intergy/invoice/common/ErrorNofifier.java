package au.com.intergy.invoice.common;

import java.util.Properties;
import java.util.List;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *  Assuming error does not occurs often.
 *
 */
public class ErrorNofifier {
	private static Logger logger = new Logger();
	private final static Configuration config = Configuration.getInstance();
	private final static String MAIL_SERVER = config.getString("mail.smtp.host");
	private final static String MAIL_SERVER_CONFIG_KEY = "mail.smtp.host";
	private static final Properties MAIL_PROPERTIES;
	
	private static final String ERROR_EMAIL_SUBJECT = config.getString("error.notify.subject", "Invoice Run Error");
	private static final String ERROR_EMAIL_FROMADDRESS = config.getString("error.notify.fromaddress");
	private static final List<String> ERROR_EMAIL_TOADDRESSES = config.getListOfString("error.notify.toaddresses");
	
	static {
		MAIL_PROPERTIES = new Properties();
		MAIL_PROPERTIES.put(MAIL_SERVER_CONFIG_KEY, MAIL_SERVER);
	}
	
	public static void addSystemError(String message) {
		try {
			ErrorDataAccess.addError(InvoiceError.ErrorType.SYS_ERROR.name(), 0, message, null, null);
		} catch (DBAccessException bde) {
			logger.log("Error: Unable to add a system error " + bde.toString());
		}
	}
	
	public static void notifyError() {
		try {
			List<InvoiceError> notSentErrors = ErrorDataAccess.getNotSentErrors();
			String emailBody = null;

			for (InvoiceError error: notSentErrors) {
				switch(InvoiceError.ErrorType.valueOf(error.getType())) {
					case INVOICE:
						emailBody = "Unable to process invoice " + error.getReferenceId();
						break;
					case CUSTBATCH:
						emailBody = "Unable to process customer batch " + error.getReferenceId();
						break;
					case SYS_ERROR:
						emailBody = "A system error has occurred.";
						break;
				}
				
				emailBody = emailBody.concat(System.getProperty("line.separator"));
				emailBody = emailBody.concat("Created date: " + DateUtil.toStringDate("dd-MMM-yyyy HH:mm:ss z", error.getCreatedDate()));
				
				emailBody = emailBody.concat(System.getProperty("line.separator"));
				emailBody = emailBody.concat("Brand code: " + (error.getBrand() == null?"N/A":error.getBrand()));
				
				emailBody = emailBody.concat(System.getProperty("line.separator"));
				emailBody = emailBody.concat("Customer/account number: " +
						(error.getAccountNumber() == null?"N/A":error.getAccountNumber()));
				
				emailBody = emailBody.concat(System.getProperty("line.separator"));
				emailBody = emailBody.concat("Message: " + error.getMessage());
				
				sendEmail(emailBody);
				ErrorDataAccess.updateSentDateTime(error.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.log("ERROR: ErrorNotifier: Unable to notify error: " + e.toString());
		}
	}
	
	private static void sendEmail(String content) 
	throws NoSuchProviderException, MessagingException {
		Session mailSession = Session.getDefaultInstance(MAIL_PROPERTIES, null);

		MimeMessage message = new MimeMessage(mailSession);
		message.setSubject(ERROR_EMAIL_SUBJECT);
		message.setContent(content, "text/plain");

		InternetAddress addressFrom = new InternetAddress(ERROR_EMAIL_FROMADDRESS);
		message.setFrom(addressFrom);

		//set to addresses
		InternetAddress[] toInternetAddresses = new InternetAddress[ERROR_EMAIL_TOADDRESSES.size()]; 
		int count = 0;
		for (String toAddress : ERROR_EMAIL_TOADDRESSES) {
			toInternetAddresses[count] = new InternetAddress(toAddress);
			count++;
		}
		message.setRecipients(Message.RecipientType.TO, toInternetAddresses);

		Transport.send(message);
	}
}
