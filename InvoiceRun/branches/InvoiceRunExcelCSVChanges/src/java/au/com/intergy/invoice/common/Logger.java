package au.com.intergy.invoice.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class Logger {

	private static final Configuration conf = Configuration.getInstance();
	private static final File logDir = new File(conf.getString("log.file.dir"));
	private static final File file = new File(logDir,"error_log.txt");
	private final String NEW_LINE = System.getProperty("line.separator");
	private final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	public void log(String aContents) 
	{
		try {

			Writer output;
			if (!file.exists())
			{
				 output = new BufferedWriter(new FileWriter(file));
			}
			else 
			{
				output = new BufferedWriter(new FileWriter(file,true));
			}
			
			GregorianCalendar cal = new GregorianCalendar(TimeZone.getTimeZone("Pacific/Sydney"));

			//FileWriter always assumes default encoding is OK!
			output.write( format.format(cal.getTime()) + " " + aContents + NEW_LINE);
			output.close();
		} 
		catch (Exception e) {
			throw new Error("Unable to create file \n " + e.toString());
		}
	}
}
