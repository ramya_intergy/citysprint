package au.com.intergy.invoice.common;

import java.io.FilenameFilter;

public class StylesheetFilenameFilter implements FilenameFilter {
		
	public boolean accept ( java.io.File f , String fileName)   {  
		return fileName.toLowerCase().endsWith ( ".xslt" ); 
	}  
}
