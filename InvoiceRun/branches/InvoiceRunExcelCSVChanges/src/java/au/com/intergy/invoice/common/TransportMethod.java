package au.com.intergy.invoice.common;

public enum TransportMethod {
	EMAIL, PRINT, FTP, FTPS
}
