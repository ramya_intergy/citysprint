package au.com.intergy.invoice.delivery;

import java.util.ArrayList;
import java.util.List;

/**
 * Encapsulates data related to an email sent.
 * 
 * @author Chi Bui
 *
 */
public class Email {

	private String fromAddress;
	private String password;
	private List<String> toAddresses;
	private List<String> ccAddresses;
	private List<String> bccAddresses;
	private String subject;
	private String textBody;
	private List<String> attachements;
	
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getFromAddress() {
		return fromAddress;
	}
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}
	
	public List<String> getToAddresses() {
		return toAddresses;
	}
	
	public void setToAddress(String toAddress) {
		if (toAddresses == null)
			toAddresses = new ArrayList<String>();
		toAddresses.add(toAddress);
	}
	
	public void setToAddresses(List<String> toAddresses) {
		this.toAddresses = toAddresses;
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getTextBody() {
		return textBody;
	}
	
	public String getTextBody(String note) {
		return note == null ? textBody : textBody.concat(note);
	}
	
	public void setTextBody(String textBody) {
		this.textBody = textBody;
	}
	
	public List<String> getAttachements() {
		return attachements;
	}
	public void setAttachements(List<String> attachements) {
		this.attachements = attachements;
	}
	
	
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation 
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "\n";
	
	    StringBuilder retValue = new StringBuilder();
	    
	    retValue.append("Email [ ")
	        .append("fromAddress = ").append(this.fromAddress).append(TAB)
	        .append("toAddresses = ").append(this.toAddresses).append(TAB)
	        .append("ccAddresses = ").append(this.ccAddresses).append(TAB)
	        .append("bccAddresses = ").append(this.bccAddresses).append(TAB)
	        .append("subject = ").append(this.subject).append(TAB)
	        .append("textBody = ").append(this.textBody).append(TAB)
	        .append("attachements = ").append(this.attachements).append(TAB)
	        .append("]");
	    
	    return retValue.toString();
	}
}
