package au.com.intergy.invoice.delivery;

import au.com.intergy.invoice.common.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import au.com.intergy.invoice.common.CustomerBatch.Status;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Sends emails for all customer batches with transport method = 'Email' and
 * have files generated.
 *
 * @author Chi Bui
 */
public class EmailSender {

    private static Logger logger = new Logger();
    private final static Configuration config = Configuration.getInstance();
    private final static String MAIL_SERVER = config.getString("mail.smtp.host");
    private final static String MAIL_SERVER_CONFIG_KEY = "mail.smtp.host";
    /**
     * ATTACHMENT_SIZE is in Bytes, configured in KB, default value = 1MB
     */
    private final static long ATTACHMENT_SIZE = config.getLong("email.attachment.size", new Long(2)) * 1000;
    private final static long ATTACHMENT_FILE_SIZE = config.getLong("email.attachment.file.size", new Long(4)) * 1000;
    private final static String EMAIL_TEMPLATE_DIR = config.getString("email.template.dir");
    private final static String EMAIL_FROM_ADDRESS = config.getString("email.fromaddress");
    private final static boolean TEST_SEND_EMAIL = config.getBoolean("email.test", false);
    private final static long EMAIL_SENDER_SLEEP = config.getLong("email.sender.sleep", new Long(2000));

    public static void main(String[] args) throws InterruptedException {
        EmailSender sender = new EmailSender();

        System.out.println("Running EmailSender ...");
        try {
            while (true) {
                sender.sendEmails();
                Thread.sleep(EMAIL_SENDER_SLEEP);
            }
        } catch (DBAccessException dae) {
            ErrorNofifier.addSystemError("Terminating Invoice Run's EmailSender " + dae.toString());
            logger.log("ERROR: EmailSender is quiting ..." + dae.toString());
            dae.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Send emails for outstanding customer batches with transport method =
     * 'Email'
     */
    public void sendEmails()
            throws DBAccessException {
        Set<CustomerBatch> custBatches = CustomerBatchDataAccess.findNotSentEmailCustomerBatches(10);

        for (CustomerBatch custBatch : custBatches) {

            BatchGroup group = BatchGroupDataAccess.findById(custBatch.getBatchGroupId());
            //only send email for the customer batch if the batch group is started (i.e. processing), 
            //not paused or cancelled
            if (BatchGroup.Status.STARTED.name().equals(group.getStatus())) {
                long custBatchId = custBatch.getCustomerBatchId();

                System.out.println("Processing customer batch " + custBatchId);
                try {
                    sendEmail(createEmail(custBatch));
                    CustomerBatchDataAccess.updateStatus(custBatch.getCustomerBatchId(), Status.SUCCESS.name());
                    BatchGroupDataAccess.updateStatusBasedOnCustBatch(BatchGroup.Status.COMPLETED.name(),
                            Status.SUCCESS.name(), custBatch.getBatchGroupId());
                    /**
                     * Adnan, need to add successful completion email here.
                     */
                } catch (FileNotGeneratedException fnge) {
                    //do not fail customer batch with no file generated
                    logger.log("WARNING: Customer batch " + custBatch.getCustomerBatchId() + " does not have all files generated.");
                } catch (Exception e) {
                    InvoiceManager.failCustomerBatch(custBatch.getCustomerBatchId(),
                            "Unable to send email with error " + e.toString(), custBatch.getBrand(), custBatch.getCustomerKey());
                    e.printStackTrace();
                    logger.log("ERROR: Can't send email for customer batch " + custBatch.getCustomerBatchId());
                    logger.log(e.toString());
                    /**
                     * Adnan, need to add Failed batches email here.
                     */
                }
            }
        }
    }

    /**
     * @return An Email for each customer batch.
     * @see au.com.intergy.invoice.delivery.Email
     */
    public Email createEmail(CustomerBatch custBatch)
            throws EmailTemplateNotFoundException, DBAccessException {

        Email emailResult = new Email();

        String emailSubject = "Invoice(s)";

        try {
            switch (Brand.valueOf(custBatch.getBrand())) {
                case CS:
                    emailSubject = "Invoices from CitySprint";
                    break;
                case CSSS:
                    emailSubject = "Invoice(s) from CitySprint Share Services";
                    break;
                case W1:
                    emailSubject = "Invoice(s) from WestOne Cars";
                    break;
                case DUH:
                    emailSubject = "Invoice(s) from Drive U Home";
                    break;
            }
        } catch (IllegalArgumentException ise) {
            //might caused by the brand does not exist
        }

        emailResult.setFromAddress(EMAIL_FROM_ADDRESS);
        emailResult.setSubject(emailSubject);

        //emailResult.setPassword(EMAIL_PASSWORD);
        if (TEST_SEND_EMAIL) {
            emailResult.setToAddresses(config.getListOfString("email.test.toaddresses"));
        } else {
//			emailResult.setToAddress(custBatch.getTransportAddress());
            System.out.println();
            String transportAddresses = custBatch.getTransportAddress().replaceAll(";", ",");
            String[] addresses = transportAddresses.split(",");
            for (String address : addresses) {
                emailResult.setToAddress(address);
            }
        }
        emailResult.setAttachements(InvoiceDataAccess.getAttachmentFileNames(custBatch.getCustomerBatchId()));

        double invTotal = InvoiceDataAccess.getInvoicTotalById(custBatch.getCustomerBatchId());
        logger.log("######### custBatch.getCustomerBatchId() == " + custBatch.getCustomerBatchId() 
                + ", Inv Total == " + invTotal);
        Invoice accountDetails = InvoiceDataAccess.getInvoicAccountDetailsById(custBatch.getCustomerBatchId());

        emailResult.setTextBody(
                prepareEmailTextBody(custBatch.getBrand(), custBatch.getCreditControllerName(),
                custBatch.getCreditControllerEmail(), custBatch.getCreditControllerPhone(), custBatch.getCustomerKey(), custBatch.getTransportEmailTemplate(),
                custBatch.getCreditControllerTitle(), custBatch.getCreditControllerOffice(),
                custBatch.getCreditControllerFaxNo(), invTotal, accountDetails));
        
        
        if (custBatch.getTransportEmailTemplate() != null && !custBatch.getTransportEmailTemplate().trim().equals("")){
            emailResult.setSubject(getEmailSubject(custBatch.getTransportEmailTemplate(), custBatch.getCustomerKey()));
        }
        
        return emailResult;
    }
    
    private String getEmailSubject(String emailTemplate, String accountNumber){
    
        String subject = null;
        
         if (emailTemplate != null && !emailTemplate.trim().equals("")) {
                String emailTemplateLower = emailTemplate.toLowerCase();
                
                if (emailTemplateLower.contains("initial_letter"))
                    subject = "CitySprint Overdue Account " + accountNumber;
                
                else if (emailTemplateLower.contains("reminder_letter"))
                    subject = "Urgent Payment Reminder Account " + accountNumber;
                
                else if (emailTemplateLower.contains("urgent_letter"))
                    subject = "Final Reminder Account " + accountNumber;
            }
        
        return subject;
    }

    /**
     * @param ccName Credit Controller Name
     */
    private String prepareEmailTextBody(String brand, String ccName, String ccEmail, String ccPhone, String custKey, String emailTemplate,
            String ccTitle, String ccOffice, String ccFaxNo, double invTotal, Invoice accountDetails)
            throws EmailTemplateNotFoundException {
        //retrieve email template
        String templateFileName = "Email_NA.html";

        try {
            if (emailTemplate != null && !emailTemplate.trim().equals("")) {
                templateFileName = emailTemplate;
            } else {
                switch (Brand.valueOf(brand)) {
                    case CS:
                        templateFileName = "Email_CISP.html";
                        break;
                    case CSSS:
                        templateFileName = "Email_CSSS.html";
                        break;
                    case W1:
                        templateFileName = "Email_WO.html";
                        break;
                    case DUH:
                        templateFileName = "Email_DUH.html";
                        break;
                    case BUR:
                        templateFileName = "Email_BUR.html";
                        break;
                }
            }
        } catch (IllegalArgumentException iae) {
        }

        try {
            String template = FileUtil.readFile(EMAIL_TEMPLATE_DIR + "/" + templateFileName, "\n");

            logger.log("######### template before replacement == " + template );
            
            template = StringUtils.replaceAll(template, "\\$\\{custKey\\}", custKey == null ? "" : custKey);
            template = StringUtils.replaceAll(template, "\\$\\{creditcontrollername\\}", ccName == null ? "" : ccName);
            template = StringUtils.replaceAll(template, "\\$\\{creditcontrolleremail\\}", ccEmail == null ? "" : ccEmail);
            template = StringUtils.replaceAll(template, "\\$\\{creditcontrollerphone\\}", ccPhone == null ? "" : ccPhone);

            template = StringUtils.replaceAll(template, "\\$\\{creditcontrollertitle\\}", ccTitle == null ? "" : ccTitle);
            template = StringUtils.replaceAll(template, "\\$\\{creditcontrolleroffice\\}", ccOffice == null ? "" : ccOffice);
            template = StringUtils.replaceAll(template, "\\$\\{creditcontrollerfaxno\\}", ccFaxNo == null ? "" : ccFaxNo);

            template = StringUtils.replaceAll(template, "\\$\\{invtotal\\}", "" + String.format( "%.2f", invTotal ));
            StringBuffer accountBuffer = new StringBuffer("");
            if (accountDetails != null) {
//                if (accountDetails.getAccountName() != null && !accountDetails.getAccountName().trim().equals("")) {
////                                template = StringUtils.replaceAll(template, "\\$\\{accName\\}", ""+ accountDetails.getAccountName());
//                    accountBuffer.append(accountDetails.getAccountName());
//                    accountBuffer.append("<br>");
//                }
                if (accountDetails.getAccountAddr1() != null && !accountDetails.getAccountAddr1().trim().equals("")) {
//                                template = StringUtils.replaceAll(template, "\\$\\{accAddr1\\}", ""+ accountDetails.getAccountAddr1());
                    accountBuffer.append(accountDetails.getAccountAddr1());
                    accountBuffer.append("<br>");
                }

                if (accountDetails.getAccountAddr2() != null && !accountDetails.getAccountAddr2().trim().equals("")) {
//                                template = StringUtils.replaceAll(template, "\\$\\{accAddr2\\}", ""+ accountDetails.getAccountAddr2());
                    accountBuffer.append(accountDetails.getAccountAddr2());
                    accountBuffer.append("<br>");
                }

                if (accountDetails.getAccountAddr3() != null && !accountDetails.getAccountAddr3().trim().equals("")) {
//                                template = StringUtils.replaceAll(template, "\\$\\{accAddr3\\}", ""+ accountDetails.getAccountAddr3());
                    accountBuffer.append(accountDetails.getAccountAddr3());
                    accountBuffer.append("<br>");
                }

                if (accountDetails.getAccountAddr4() != null && !accountDetails.getAccountAddr4().trim().equals("")) {
//                                template = StringUtils.replaceAll(template, "\\$\\{accAddr4\\}", ""+ accountDetails.getAccountAddr4());
                    accountBuffer.append(accountDetails.getAccountAddr4());
                    accountBuffer.append("<br>");
                }

                if (accountDetails.getAccountAddr5() != null && !accountDetails.getAccountAddr5().trim().equals("")) {
//                                template = StringUtils.replaceAll(template, "\\$\\{accAddr5\\}", ""+ accountDetails.getAccountAddr5());
                    accountBuffer.append(accountDetails.getAccountAddr5());
                    accountBuffer.append("<br>");
                }

                if (accountDetails.getAccountAddr6() != null && !accountDetails.getAccountAddr6().trim().equals("")) {
//                                template = StringUtils.replaceAll(template, "\\$\\{accAddr6\\}", ""+ accountDetails.getAccountAddr6());
                    accountBuffer.append(accountDetails.getAccountAddr6());
                    accountBuffer.append("<br>");
                }

//                if (accountDetails.getAccountAddr7() != null && !accountDetails.getAccountAddr7().trim().equals("")) {
////                                template = StringUtils.replaceAll(template, "\\$\\{accAddr7\\}", ""+ accountDetails.getAccountAddr7());
//                    accountBuffer.append(accountDetails.getAccountAddr7());
////                                accountBuffer.append("<br>");
//                }

            }

            template = StringUtils.replaceAll(template, "\\$\\{accountAddress\\}", "" + accountBuffer);

            Date today = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 7);

//	        Date nextWeek = calendar.getTime();

            //
            // We want the message to be is Locale.US
            //
            Locale.setDefault(Locale.US);

            //
            // Format a date, the time value is included
            //
            String message = MessageFormat.format("{0,date} ", today);
//	        System.out.println(message);
            //template now is complete
            template = StringUtils.replaceAll(template, "\\$\\{currentDate\\}", "" + message);

            logger.log("######### template AFTER replacement == " + template );
            return template;

        } catch (IOException ioe) {
            throw new EmailTemplateNotFoundException("Unable to read email template "
                    + EMAIL_TEMPLATE_DIR + "/" + templateFileName);
        }
    }

    /**
     * For each Email object, multiple actual emails may be sent if the total
     * files size is bigger than the MAX_FILE_SIZE.
     */
    public void sendEmail(Email email)
            throws MessagingException, FileTooBigException, FileNotGeneratedException {
        List<List<String>> attachmentParts = getEmailAttachmentsSize(email.getAttachements());
        int totalAttachmentFiles = email.getAttachements().size();
        String attachmentNote = " of " + totalAttachmentFiles + " total files.";
        int firstFileIndex = 1;
        int partCount = 0;
        String subject = email.getSubject();

        for (List<String> part : attachmentParts) {
            String emailNote = null;

            if (attachmentParts.size() > 1) {
                emailNote = "Note: contains file(s) " + firstFileIndex
                        + " to " + (firstFileIndex + part.size() - 1) + attachmentNote;
                firstFileIndex += part.size();

                email.setSubject(subject.concat(" - Part " + (++partCount)));
            }

            // set message body part
            Multipart bodyAndAttachments = new MimeMultipart();
            //text
            BodyPart textBody = new MimeBodyPart();

            textBody.setContent(email.getTextBody(emailNote), "text/html");
            bodyAndAttachments.addBodyPart(textBody);

            System.out.println("Sending email from " + email.getFromAddress() + " to " + email.getToAddresses());
            sendEmail(getMessage(email), bodyAndAttachments, part);
            System.out.println("Sent email " + email.getFromAddress()
                    + " with attachment size " + part.size());
        }
    }

    /**
     * @return Mail Message with no body content and attachment.
     */
    private Message getMessage(Email email) throws MessagingException {
        Properties props = new Properties();
        props.put(MAIL_SERVER_CONFIG_KEY, MAIL_SERVER);
        //props.put("mail.smtp.auth", "true"); 


        // create some properties and get the default Session
        //new IntergyAuthenticator(email.getFromAddress(), email.getPassword())
        Session session = Session.getDefaultInstance(props, null);

        // create a message
        Message msg = new MimeMessage(session);

        // set the from address
        InternetAddress addressFrom = new InternetAddress(email.getFromAddress());
        msg.setFrom(addressFrom);

        // set the to addresses
        InternetAddress[] toInternetAddresses = new InternetAddress[email.getToAddresses().size()];
        int count = 0;
        for (String toAddress : email.getToAddresses()) {
            toInternetAddresses[count] = new InternetAddress(toAddress);
            count++;
        }
        msg.setRecipients(Message.RecipientType.TO, toInternetAddresses);

        // Setting the Subject and Content Type
        msg.setSubject(email.getSubject());

        return msg;
    }

    private void sendEmail(Message msg, Multipart body, List<String> fileNames)
            throws MessagingException, FileNotGeneratedException {

        for (String fileName : fileNames) {
            if (fileName == null) {
                throw new FileNotGeneratedException();
            }
            BodyPart attachment = new MimeBodyPart();
            DataSource source = new FileDataSource(fileName);
            attachment.setDataHandler(new DataHandler(source));

            //
            int beginIdx = fileName.indexOf("INV_");
            if (beginIdx == -1) {
                beginIdx = fileName.indexOf("STMT_");
            }
            if (beginIdx == -1) {
                beginIdx = fileName.indexOf("SCH_");
            }
            attachment.setFileName(fileName.substring(beginIdx));
            body.addBodyPart(attachment);
        }

        msg.setContent(body);
        Transport.send(msg);
        //Transport tran = new Transport();
    }

    /**
     * Group attachment files in to parts with size < MAX_FILE_SIZE.
     *
     * @throws FileTooBigException If detects a file with size > MAX_FILE_SIZE.
     */
    List<List<String>> getEmailAttachmentsSize(List<String> fileNames)
            throws FileTooBigException {
        List<List<String>> attachmentParts = new ArrayList<List<String>>();

        List<String> part = new ArrayList<String>();
        attachmentParts.add(part);

        long tempTotalFileSize = 0;

        for (String fileName : fileNames) {
            File file = new File(fileName);

            if (file.length() > ATTACHMENT_FILE_SIZE) {
                throw new FileTooBigException("File " + file.getAbsolutePath()
                        + ": " + file.length() + " bytes is bigger than config file size " + ATTACHMENT_FILE_SIZE);
            }

            if (tempTotalFileSize == 0 || ATTACHMENT_SIZE - tempTotalFileSize > file.length()) {
                tempTotalFileSize += file.length();
            } else {
                tempTotalFileSize = 0L;
                part = new ArrayList<String>();
                attachmentParts.add(part);
            }

            part.add(fileName);
        }

        return attachmentParts;
    }
}
