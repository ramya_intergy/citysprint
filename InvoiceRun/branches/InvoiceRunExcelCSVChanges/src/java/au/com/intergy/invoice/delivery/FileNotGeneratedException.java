package au.com.intergy.invoice.delivery;

public class FileNotGeneratedException extends Exception {
	private static final long serialVersionUID = 14441755458756L;

	public FileNotGeneratedException() {
		this("File name is null.");
	}
	
	public FileNotGeneratedException(String message) {
		super(message);
	}
}
