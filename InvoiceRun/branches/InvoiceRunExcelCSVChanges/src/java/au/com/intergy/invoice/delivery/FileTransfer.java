package au.com.intergy.invoice.delivery;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;

import java.security.NoSuchAlgorithmException;

import au.com.intergy.invoice.common.BatchGroup;
import au.com.intergy.invoice.common.BatchGroupDataAccess;
import au.com.intergy.invoice.common.Configuration;
import au.com.intergy.invoice.common.CustomerBatch;
import au.com.intergy.invoice.common.CustomerBatchDataAccess;
import au.com.intergy.invoice.common.DBAccessException;
import au.com.intergy.invoice.common.ErrorNofifier;
import au.com.intergy.invoice.common.InvoiceDataAccess;
import au.com.intergy.invoice.common.InvoiceManager;
import au.com.intergy.invoice.common.Logger;
import au.com.intergy.invoice.common.CustomerBatch.Status;
import java.io.PrintWriter;
import org.apache.commons.net.PrintCommandListener;

/**
 * This class continuously checks if it needs to transfer files
 * for outstanding customer batches.
 * 
 * Files are transferred using the FTP protocol.
 * 
 * @author Chi Bui
 *
 */
public class FileTransfer {
	private static Logger logger = new Logger();
	private final static Configuration config = Configuration.getInstance();
	private final static long FILE_TRANSFER_SLEEP = config.getLong("file.transfer.sleep", new Long(2000));

	public static void main(String[] args) throws InterruptedException {
		FileTransfer ftpSender = new FileTransfer();

		System.out.println("Running File Transfer ...");
	           try {
                while (true) {
                    ftpSender.send();//Normal FTP
                    ftpSender.sendFTPS();//FTPS
                    Thread.sleep(FILE_TRANSFER_SLEEP);
                }
            } catch (DBAccessException dae) {
                ErrorNofifier.addSystemError("Terminating Invoice Run's FTP " + dae.toString());
                logger.log("ERROR: FTP is quiting ..." + dae.toString());
                dae.printStackTrace();
                System.exit(-1);
            }
	}

	/**
	 * Transfer files for each customer batch,
	 * then update its status and update batch group's status.
	 * 
	 * @throws DBAccessException
	 */
	private void send() throws DBAccessException {
		Set<CustomerBatch> custBatches = CustomerBatchDataAccess.findNotTransferredCustomerBatches(10);
		//System.out.println("Size:"+custBatches.size());
		for (CustomerBatch custBatch : custBatches) {
			try {
				transferFiles(custBatch.getTransportAddress(), 
						custBatch.getTransportUsername(), custBatch.getTransportPassword(), 
						InvoiceDataAccess.getAttachmentFileNames(custBatch.getCustomerBatchId()) );
				CustomerBatchDataAccess.updateStatus(custBatch.getCustomerBatchId(), Status.SUCCESS.name());
				System.out.println("After status update...");
				BatchGroupDataAccess.updateStatusBasedOnCustBatch(BatchGroup.Status.COMPLETED.name(),
						Status.SUCCESS.name(), custBatch.getBatchGroupId());
				System.out.println("Transfer files successfully for customer batch " + 
						custBatch.getCustomerBatchId());
			} catch (Exception e) {
				InvoiceManager.failCustomerBatch(custBatch.getCustomerBatchId(), 
					"Unable to transfer file with error " + e.getMessage(), custBatch.getBrand(), custBatch.getCustomerKey());
				e.printStackTrace();
				logger.log("ERROR: Can't transfer files for customer batch " + custBatch.getCustomerBatchId());
				logger.log(e.toString());
			}
		}
	}

        /**
	 * Transfer files via FTPS for each customer batch,
	 * then update its status and update batch group's status.
	 *
	 * @throws DBAccessException
	 */
	private void sendFTPS() throws DBAccessException {
		Set<CustomerBatch> custBatches = CustomerBatchDataAccess.findNotTransferredFTPSCustomerBatches(10);
		//System.out.println("Size:"+custBatches.size());
		for (CustomerBatch custBatch : custBatches) {
			try {
				transferFilesFTPS(custBatch.getTransportAddress(),
						custBatch.getTransportUsername(), custBatch.getTransportPassword(),
						InvoiceDataAccess.getAttachmentFileNames(custBatch.getCustomerBatchId()) );
				CustomerBatchDataAccess.updateStatus(custBatch.getCustomerBatchId(), Status.SUCCESS.name());
				System.out.println("After status update in sendFTPS...");
				BatchGroupDataAccess.updateStatusBasedOnCustBatch(BatchGroup.Status.COMPLETED.name(),
						Status.SUCCESS.name(), custBatch.getBatchGroupId());
				System.out.println("Transfer files successfully for customer batch " +
						custBatch.getCustomerBatchId());
			} catch (Exception e) {
				InvoiceManager.failCustomerBatch(custBatch.getCustomerBatchId(),
					"Unable to transfer file via FTPS with error " + e.getMessage(), custBatch.getBrand(), custBatch.getCustomerKey());
				e.printStackTrace();
				logger.log("ERROR: Can't transfer files via FTPS for customer batch " + custBatch.getCustomerBatchId());
				logger.log(e.toString());
			}
		}
	}

	private void transferFiles(String ftpHost, String ftpUserName, 
			String ftpPassword, List<String> fileNames) throws  FileTransferException {

		FTPClient ftp = new FTPClient();

		//Connect to the server
		boolean isConnected = true;
		try {
			System.out.println("Ftp connect...");
			ftp.connect(ftpHost);
			//15/01/2009 Ramya added ftp.enterLocalPassiveMode(); to fix storeFile hanging issue
			//Ref:http://roshantitus.blogspot.com/2006_11_01_archive.html
			ftp.enterLocalPassiveMode();

		} catch (IOException ioe) {
			isConnected = false;
                        System.out.println("In connect:"+ioe.toString());
		}
		if (!isConnected) {
			throw new FileTransferException ("Unable to connect to " + ftpHost);
		}

		// verify success
		int reply = ftp.getReplyCode();    
		if(!FTPReply.isPositiveCompletion(reply)) {
			try {
				ftp.disconnect();
			} catch (Exception e) {
				System.err.println("Unable to disconnect from FTP server " +
						"after server refused connection. " + e.toString());
			}
			throw new FileTransferException ("FTP server  " + ftpHost + " refused connection.");
	 }
	   //System.out.println("Ftp logon...");
	 //Log on
		boolean isLoggedOn;
		try {
			isLoggedOn = ftp.login(ftpUserName, ftpPassword);
		} catch (IOException ioe) {
			isLoggedOn = false;
		}
        //System.out.println("Ftp logon..."+isLoggedOn);
		if (!isLoggedOn) {
			throw new FileTransferException ("Unable to login to FTP server " +
					"using username "+ftpUserName+" " +
					"and password "+ftpPassword);
		}

		//System.out.println(ftp.getReplyString());
		//System.out.println("Remote system is " + ftp.getSystemName());

		//set file type to either ASCII or Binary
		//ftp.setFileType(FTP.ASCII_FILE_TYPE);
		try {
			ftp.setFileType(FTP.BINARY_FILE_TYPE);
		} catch (IOException ioe) {
			throw new FileTransferException("Unable to set file type to BINARY for host "  + ftpHost);
		}
        //System.out.println("FTP Set File type" + fileNames.size());

		boolean isStoredFile=false;
		//Transfer all files
		for (String fileName : fileNames) {
			File file = new File(fileName);
			boolean isTransferred = true;

			try {
				System.out.println("FTP before storefile" + fileName);
				ftp.setDefaultTimeout(100000);
				isStoredFile=ftp.storeFile(file.getName(), new FileInputStream(file));
				//System.out.println("FTP after storefile" + isStoredFile);
			} catch (IOException ioe) { 
				isTransferred = false;
                                System.out.println("In store file:"+ioe.toString());
			}

			//check result
			if (!isTransferred) {
				throw new FileTransferException ("Unable to store file " + fileName + " in host " + ftpHost);
			}
		}

	    //System.out.println("FTP after transfer");
		//Disconnect from the remote FTP host
		try {
			ftp.disconnect();
		} catch (IOException exc) {
			logger.log("FileTransfer: WARNING: Unable to disconnect from FTP server " + ftpHost + ". "+ exc.toString());
		}

	}

        private void transferFilesFTPS(String ftpsHost, String ftpsUserName,
			String ftpsPassword, List<String> fileNames) throws  FileTransferException, NoSuchAlgorithmException {

                String protocol = "TLSv1";
		FTPSClient ftps = new FTPSClient(protocol, true);
                ftps.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
                
		//Connect to the server
		boolean isConnected = true;
                String sMessage = "";
		try {
			System.out.println("Ftps connect...implicit true");
                        logger.log("ftpsHost:"+ftpsHost);
                        logger.log("ftpsUserName:"+ftpsUserName);
                        logger.log("ftpsPassword:"+ftpsPassword);
                        logger.log("implicit true protocol:"+protocol);

			ftps.connect(ftpsHost, 990);
                        logger.log("after connect");
			//15/01/2009 Ramya added ftp.enterLocalPassiveMode(); to fix storeFile hanging issue
			//Ref:http://roshantitus.blogspot.com/2006_11_01_archive.html
			ftps.enterLocalPassiveMode();

		} catch (IOException ioe) {
			isConnected = false;
                        sMessage = ioe.getMessage();
                        logger.log("FTPS Exception message: "+ sMessage);
                        ioe.printStackTrace();
		}
		if (!isConnected) {
			throw new FileTransferException ("Unable to connect to " + ftpsHost + "Exception: "+ sMessage);
		}

		// verify success
		int reply = ftps.getReplyCode();
		if(!FTPReply.isPositiveCompletion(reply)) {
			try {
				ftps.disconnect();
			} catch (Exception e) {
				System.err.println("Unable to disconnect from FTPS server " +
						"after server refused connection. " + e.toString());
			}
			throw new FileTransferException ("FTPS server  " + ftpsHost + " refused connection.");
	 }
	   //System.out.println("Ftp logon...");
	 //Log on
		boolean isLoggedOn;
		try {
			isLoggedOn = ftps.login(ftpsUserName, ftpsPassword);
		} catch (IOException ioe) {
			isLoggedOn = false;
		}
        //System.out.println("Ftp logon..."+isLoggedOn);
		if (!isLoggedOn) {
			throw new FileTransferException ("Unable to login to FTPS server " +
					"using username "+ftpsUserName+" " +
					"and password "+ftpsPassword);
		}

		//System.out.println(ftp.getReplyString());
		//System.out.println("Remote system is " + ftp.getSystemName());

		//set file type to either ASCII or Binary
		//ftp.setFileType(FTP.ASCII_FILE_TYPE);
		try {
			ftps.setFileType(FTP.BINARY_FILE_TYPE);
		} catch (IOException ioe) {
			throw new FileTransferException("Unable to set file type to BINARY for host "  + ftpsHost);
		}
        //System.out.println("FTP Set File type" + fileNames.size());

		boolean isStoredFile=false;
		//Transfer all files
		for (String fileName : fileNames) {
			File file = new File(fileName);
			boolean isTransferred = true;

			try {
				//System.out.println("FTP before storefile" + fileName);
				ftps.setDefaultTimeout(100000);
				isStoredFile=ftps.storeFile(file.getName(), new FileInputStream(file));
				//System.out.println("FTP after storefile" + isStoredFile);
			} catch (IOException ioe) {
				isTransferred = false;
			}

			//check result
			if (!isTransferred) {
				throw new FileTransferException ("Unable to store file " + fileName + " in host " + ftpsHost);
			}
		}

	    //System.out.println("FTP after transfer");
		//Disconnect from the remote FTP host
		try {
			ftps.disconnect();
		} catch (IOException exc) {
			logger.log("FileTransfer: WARNING: Unable to disconnect from FTPS server " + ftpsHost + ". "+ exc.toString());
		}

	}

}
