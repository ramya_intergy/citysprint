package au.com.intergy.invoice.delivery;

public class FileTransferException extends Exception {

	public FileTransferException(String message) {
		super(message);
	}
}
