package au.com.intergy.invoice.filegenerator;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Result; 
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

import au.com.intergy.invoice.common.Configuration;
import au.com.intergy.invoice.common.DatabaseConnector;
import au.com.intergy.invoice.common.FileUtil;
import au.com.intergy.invoice.common.Logger;
import au.com.intergy.invoice.webservice.XMLParser;

public class PDFGeneratorBatchProcess {
	private final static Logger logger = new Logger();
	private final static Configuration conf = Configuration.getInstance();
	
	int xmlLineCount=0;
	String[] xmlData;
	int tempXmlLineCount=0;
	int batchid=0;
	public static void main(String[] args){
		PDFGeneratorBatchProcess pdf = new PDFGeneratorBatchProcess();

		try {
			String file = FileUtil.readFile(conf.getString("xml.file"), " ");
			XMLParser.parse(file);
			pdf.generatePDFs();
		} catch(Exception e) {
			logger.log(e.getMessage().toString());
		} 
	}

	public String generatePDFs() {
		Connection conDB = null;
		Connection conUpdate = null;
		String sXML="";
		String LOGPath="";
		String XSLTPath="";
		String PDFLocation="";
		String fullXSLTFile="";
		String fullPDFPath="";
		String fullTIFFPath="";
		String XSLTFile="";
		String generatedFileDir ="";
		String FontConfig="";
		String pdfFilename="";
		String styleSheet="";
		String xml="";
		String OutputFormat="";
		String tempLocation="";
		String filename="";
		int id=0;
		String status="";

		final String NEW_LINE = System.getProperty("line.separator");
		StringBuilder sContents= new StringBuilder();
		java.util.Date d = new java.util.Date();
		String timeStamp = d.toString();

		try {

			PDFGeneratorBatchProcess pdf = new PDFGeneratorBatchProcess();

			conDB = DatabaseConnector.getConnection();
			conUpdate =  DatabaseConnector.getConnection();

			Statement stmUpdateInvoice;

			Statement stmSelect;
			ResultSet rstRead;
			Statement stmUpdate;

			String sSQL="";
			String sUpdateInvoice="";

			stmSelect= conDB.createStatement();

			sSQL="Select ID,InvoiceNumber,XSLTName,XMLBody,InvoiceDetails.TransportMethod," +
					"InvoiceDetails.OutputFormat,InvoiceDetails.Brand_Group,InvoiceDetails.BatchID_CustKey from InvoiceDetails,CustomerBatchDetails where InvoiceDetails.BatchID_CustKey=CustomerBatchDetails.BatchID_CustKey and FileGeneratedDateTime is null and InvoiceDetails.Status is null";

			rstRead=stmSelect.executeQuery(sSQL);	
			String BatchID_Custkey="";
			StringBuffer sb = new StringBuffer();
			ByteArrayInputStream bais;
			while(rstRead.next())
			{
				bais = new ByteArrayInputStream(rstRead.getBytes("XMLBody"));
				styleSheet=rstRead.getString("XSLTName");
				id=rstRead.getInt("ID");
				OutputFormat=rstRead.getString("OutputFormat");
				BatchID_Custkey=rstRead.getString("BatchID_CustKey");


				SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd hh-mm-ss");
				java.util.Date currentTime_1 = new  java.util.Date();
				String dateString = formatter.format(currentTime_1);

				if (rstRead.getString("InvoiceNumber")==null)
				{
					pdfFilename="STMT_" + dateString;
				}
				else
				{
					pdfFilename="INV_" + rstRead.getString("InvoiceNumber") + "_"+dateString;
				}

				XSLTFile =  conf.getString("XSLT_Dev");
				
				
				generatedFileDir = conf.getString("generated.file.dir");

				fullXSLTFile=XSLTFile.concat(styleSheet);

				File xsltfile=null;
				File pdffile=null;
				File tiffFile=null;
				File xmlFile=null;

				if (OutputFormat.equals("PDF"))
				{
					if(pdfFilename.contains(".pdf"))
						fullPDFPath=generatedFileDir.concat(pdfFilename);
					else
					{
						if(pdfFilename.length()>0)
						{
							pdfFilename=pdfFilename.concat(".pdf");
							fullPDFPath=generatedFileDir.concat(pdfFilename);
						}
					}

					filename=pdfFilename;
					tempLocation=fullPDFPath;
					xsltfile = new File(fullXSLTFile);
					pdffile = new File(fullPDFPath);
					xmlFile= new File(sb.toString());

				}
				else
				{
					if(pdfFilename.contains(".tiff"))
						fullTIFFPath=generatedFileDir.concat(pdfFilename);
					else
					{
						if(pdfFilename.length()>0)
						{
							pdfFilename = pdfFilename.concat(".tiff");
							fullTIFFPath = generatedFileDir.concat(pdfFilename);
						}
					}
					filename=pdfFilename;
					tempLocation=fullTIFFPath;    
					xsltfile = new File(fullXSLTFile);
					tiffFile = new File(fullTIFFPath);
					xmlFile= new File(sb.toString());
				}

				if(xsltfile.exists() && pdfFilename.length()>0)
				{
					FopFactory fopFactory = FopFactory.newInstance();

					FOUserAgent foUserAgent = fopFactory.newFOUserAgent();

					Fop fop=null;
					OutputStream out=null;

					try 
					{
						// Construct fop with desired output format

						if (OutputFormat.equals("PDF"))
						{
							out = new java.io.FileOutputStream(pdffile);
							out = new java.io.BufferedOutputStream(out);
							fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);
						}
						else
						{
							out = new java.io.FileOutputStream(tiffFile);
							out = new java.io.BufferedOutputStream(out);
							fop = fopFactory.newFop(MimeConstants.MIME_TIFF, foUserAgent, out);
						}

						// Setup XSLT
						TransformerFactory factory = TransformerFactory.newInstance();
						Transformer transformer = factory.newTransformer(new StreamSource(xsltfile));

						// Set the value of a <param> in the stylesheet
						transformer.setParameter("versionParam", "2.0");

						// Setup input for XSLT transformation
						Source src = new StreamSource(bais);
						//Source src = new StreamSource(xml);

						// Resulting SAX events (the generated FO) must be piped through to FOP
						Result res = new SAXResult(fop.getDefaultHandler());

						// Start XSLT transformation and FOP processing
						transformer.transform(src, res);
						//transformer.transform((Source)xml,res);

					}
					catch(Exception ee)
					{
						status=ee.toString();
					}
					finally 
					{
						out.close();
						logger.log("ER000^SUCCESS");
						status="ER000^SUCCESS";
					}

				} else {
					logger.log(sContents.toString());

					if(!xsltfile.exists())
						status= "ER001^XSLT File does not exists";
					else if(pdfFilename.equals(""))
						status= "ER002^PDF Filename is empty";
					else 
						status= "ER003^XML String is empty";

				}

				SimpleDateFormat formatter1 = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
				java.util.Date currentTime_2 = new  java.util.Date();
				String filegeneratedDT = formatter1.format(currentTime_2);


				if (status.equals("ER000^SUCCESS"))
				{
					sUpdateInvoice="update InvoiceDetails set status='S',TempLocation='"+ tempLocation  +"',FileName='"+ filename +"', FileGeneratedDateTime='"+ filegeneratedDT +"' where ID="+id;
				}
				else
				{    
					sUpdateInvoice="update InvoiceDetails set status='F',FileGeneratedDateTime='"+ filegeneratedDT +"',ErrorMessage='"+ status +"' where ID="+id;
				}    
				stmUpdateInvoice= conUpdate.createStatement();
				stmUpdateInvoice.executeUpdate(sUpdateInvoice);


			}

			// Update the PDF generation status/error in CustomerBatchDetails table
			conDB.close();
			conDB=DatabaseConnector.getConnection();
			stmSelect.close();
			rstRead.close();
			int nullCount=0;
			int failCount=0;

			stmSelect= conDB.createStatement();
			sSQL="Select count(*) as countofNull from InvoiceDetails where status is null and BatchID_CustKey='"+ BatchID_Custkey +"'";
			rstRead=stmSelect.executeQuery(sSQL);	


			while(rstRead.next()) {
				nullCount=rstRead.getInt("countofNull");
			}

			stmSelect.close();
			rstRead.close();

			stmSelect= conDB.createStatement();
			sSQL="Select count(*) as failCount from InvoiceDetails where status='F' and BatchID_CustKey='"+ BatchID_Custkey +"'";
			rstRead=stmSelect.executeQuery(sSQL);	


			while(rstRead.next())
			{
				failCount=rstRead.getInt("failCount");
			}

			if(nullCount>0 || failCount>0)
			{
				sUpdateInvoice="update CustomerBatchDetails set status='F',ErrorMessage='Error' where BatchID_CustKey='"+ BatchID_Custkey +"'";
			}
			else
			{
				sUpdateInvoice="update CustomerBatchDetails set status='S' where BatchID_CustKey='"+ BatchID_Custkey +"'";  
			} 
			stmUpdate= conUpdate.createStatement();
			stmUpdate.executeUpdate(sUpdateInvoice);

		}    
		catch(Exception ex)
		{
			sContents.append(ex.getMessage().toString());
			sContents.append(timeStamp);
			sContents.append(NEW_LINE);
			sContents.append(fullXSLTFile);
			sContents.append(NEW_LINE);
			sContents.append(fullPDFPath);

			status= "ER999^Errors occured during processing:" + ex.getMessage().toString();
		} finally {
			try {
				DatabaseConnector.returnConnection(conDB);
				DatabaseConnector.returnConnection(conUpdate);

			} catch (SQLException sqle) {} 
		}
		
		return status;
	}
}
