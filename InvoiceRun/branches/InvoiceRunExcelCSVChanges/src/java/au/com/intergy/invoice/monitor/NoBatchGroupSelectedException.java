package au.com.intergy.invoice.monitor;

public class NoBatchGroupSelectedException extends Exception {

	public NoBatchGroupSelectedException() {
		super();
	}
	
	public NoBatchGroupSelectedException(String message) {
		super(message);
	}
}
