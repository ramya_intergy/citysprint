package au.com.intergy.invoice.monitor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import au.com.intergy.invoice.common.Configuration;
import au.com.intergy.invoice.common.FileUtil;
import au.com.intergy.invoice.common.StylesheetFilenameFilter;

/**
 * 
 * @author Chi Bui
 */
public class StylesheetManager extends HttpServlet {

	private final static Configuration config = Configuration.getInstance();
	private final static String XSLT_DIR = config.getString("xslt.dir");
	
	/** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    	
    		processRequest(request, response);
    	
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    		processRequest(request, response);
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
    	HttpSession session = request.getSession();
    	session.setAttribute("currentStylesheets", FileUtil.getStylesheetNamesAsList(XSLT_DIR));
    	
		try {	
			if (ServletFileUpload.isMultipartContent(request)) {

				//clear error message
				request.getSession().removeAttribute("errorMsg");
				
				FileItemFactory factory = new DiskFileItemFactory();
				// Create a new file upload handler
				ServletFileUpload upload = new ServletFileUpload(factory);

				// Parse the request
				List items = upload.parseRequest(request);
				Iterator iter = items.iterator();
				try {
					while (iter.hasNext()) {
						FileItem item = (FileItem) iter.next();

						if (!item.isFormField()) {
							processUploadedFile(item);
						}
					}
				} catch (FileNotFoundException fnfe) {
					request.getSession().setAttribute("errorMsg", fnfe.getMessage());
				}
				
				response.sendRedirect("./stylesheet?action=viewStylesheet");
			} else {
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/view/stylesheets.jsp");
				dispatcher.forward(request, response);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    private void processUploadedFile(FileItem item) 
    throws Exception {
        String fileName = item.getName();

        if (fileName != null) {
        	fileName = FilenameUtils.getName(fileName);
        	File uploadedFile = new File(XSLT_DIR + "/" + fileName);
        	item.write(uploadedFile);
        }
    }
}
