package au.com.intergy.invoice.monitor.webhandler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import au.com.intergy.invoice.common.BatchGroup;
import au.com.intergy.invoice.common.BatchGroupDataAccess;
import au.com.intergy.invoice.common.Configuration;
import au.com.intergy.invoice.common.DateUtil;

public abstract class AbstractBatchGroupHander implements Handler {

	protected final Configuration conf = Configuration.getInstance();
	protected final int pastDays = conf.getInt("view.pastdays", 2);
	
	public Map<String, Object> handle(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
                au.com.intergy.invoice.common.Logger log = new au.com.intergy.invoice.common.Logger();
                log.log("In abstruct handler");
		Map<String, Object> result = new HashMap<String, Object>();

		result.put(VIEW_NAME, "batches.jsp");

		List<BatchGroup> groups = BatchGroupDataAccess.findNonCompletedBatchGroups();
		HttpSession session = request.getSession();
		session.setAttribute("batchGroups", groups);
		
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.DAY_OF_MONTH, pastDays*(-1));
		cal = DateUtil.floorCalendar(cal);
		session.setAttribute("completedBatchGroups", BatchGroupDataAccess.findCompletedBatchGroups(cal.getTime()));
                //Uncomment below line while deploying to UAT / LIVE for developement this is not required
		session.setAttribute("printers", getAvailablePrinters());
		session.setAttribute("errorMessage", request.getAttribute("errorMessage"));
		return result;
	}
	
	protected String[] getSelectedBatchGroups(HttpServletRequest request) {
		return request.getParameterValues("selectBatchGroup");	
	}

	protected List<String> getAvailablePrinters() {
		PrintService[] services = PrintServiceLookup.lookupPrintServices( null, null ); 
		

		if (services == null) 
			return new ArrayList<String>();

		List<String> printerNames = new ArrayList<String>(services.length);
		for (int i = 0; i < services.length; i++) {
			printerNames.add(services[i].getName());
		}
		
		return printerNames;
	}
	
	protected Map<Long, String> getNewPrinterNames(HttpServletRequest request, List<BatchGroup> bgroups) {
		Map<Long, String> updatedPrinters = new HashMap<Long, String>();
		String currentPrinter = null;
		String newPrinter = null;
		for (BatchGroup group : bgroups) {
			currentPrinter = group.getSelectedPrinter();
			newPrinter = request.getParameter("printer_" + group.getId());
			if (newPrinter != null && !newPrinter.equals(currentPrinter)) {
				updatedPrinters.put(group.getId(), newPrinter);
			}
		}
		return updatedPrinters;
	}
}
