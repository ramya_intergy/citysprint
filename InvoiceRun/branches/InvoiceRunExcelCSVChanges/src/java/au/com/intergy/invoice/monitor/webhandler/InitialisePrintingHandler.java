package au.com.intergy.invoice.monitor.webhandler;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import au.com.intergy.invoice.common.BatchGroup;
import au.com.intergy.invoice.common.BatchGroupDataAccess;
import au.com.intergy.invoice.common.DBAccessException;
import au.com.intergy.invoice.common.InvoiceManager;

public class InitialisePrintingHandler extends AbstractBatchGroupHander {

	public String getName() {
		return Handler.INIT_PRINT_ACTION;
	}

	public Map<String, Object> handle(HttpServletRequest request, HttpServletResponse response) 
	throws Exception {

		String[] selectedBGroups = getSelectedBatchGroups(request);

		if (selectedBGroups == null || selectedBGroups.length <= 0) {
			request.setAttribute("errorMessage", "Please select a batch to initialise printing.");
		} else {
			
			List<BatchGroup> bgroups = BatchGroupDataAccess.findBatchGroupByIds(selectedBGroups);

			try {
				InvoiceManager.initialiseBatchGroups(selectedBGroups, getNewPrinterNames(request, bgroups));
			} catch (DBAccessException dae) {
				request.setAttribute("errorMessage", "Unable to intialise batch groups.");
			}
		}

		return super.handle(request, response);
	}

}
