package au.com.intergy.invoice.tool;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.intergy.invoice.common.BatchGroup;
import au.com.intergy.invoice.common.BatchGroupDataAccess;
import au.com.intergy.invoice.common.CustomerBatch;
import au.com.intergy.invoice.common.DateUtil;
import au.com.intergy.invoice.common.Invoice;

public class InvoiceFileArchiver {

	private static String TEMPORARY_DIR;
	
	public static void main(String[] args) {
		if (args.length < 8) {
			System.out.println(getUsage());
			System.exit(-1);
		}
		try {
			Map<String, String> arguments = new HashMap<String, String>();
			arguments.put(args[0], args[1]);
			arguments.put(args[2], args[3]);
			arguments.put(args[4], args[5]);
			arguments.put(args[6], args[7]);
			
			TEMPORARY_DIR = arguments.get("-tempdir");
			if (TEMPORARY_DIR == null || TEMPORARY_DIR.isEmpty())
				throw new Exception("Please provide temporary directory to archive files.");
            String archivedFileName = arguments.get("-nm");
			Date startDate = DateUtil.toDate("dd-MM-yyyy", arguments.get("-sd"));
			Date endDate = DateUtil.toDate("dd-MM-yyyy", arguments.get("-ed"));
			
			startDate = floorDate(startDate);
			endDate = ceilDate(endDate);
			
			List<BatchGroup> batchgroups = 
				BatchGroupDataAccess.findCompletedAndCancelledByDates(startDate, endDate);
			
			
			for (BatchGroup group : batchgroups) {
				System.out.println("INFO: Archiving files for batch " + 
						group.getBatchId() + ", batch group " + group.getId() + " to " + TEMPORARY_DIR );
				List<CustomerBatch> custBatches = group.getCustBatches();
				
				for (CustomerBatch custBatch: custBatches) {
					//copy Statement file to the temporary directory
					if (custBatch.getStatement() != null)
						copyFile(custBatch.getStatement().getGeneratedFileName(), TEMPORARY_DIR);

					//copy invoice files to the temporary directory
					for (Invoice inv : custBatch.getInvoices()) {
						copyFile(inv.getGeneratedFileName(), TEMPORARY_DIR);
					}
				}
			}
			
			//zip up the directory file 
			createArchive(archivedFileName, TEMPORARY_DIR);
			
			//TODO
			//Set Generated flag of all customer batches of those b.groups to 'false'
			//set filename and generated date time for all invoices and statements to null
	
			//TODO remove files in temp folder and generated folder
		} catch (Exception pe) {
			System.out.println(pe.getMessage());
			System.out.println(getUsage());
			System.exit(-1);
		}
	}
	
	private static void copyFile(String filePath, String newLocation) throws IOException {
		System.out.println("INFO: Executing " + "cp " + filePath + " " + newLocation);
		Process p = Runtime.getRuntime().exec("cp " + filePath + " " + newLocation);
	}
	
	private static void createArchive(String fileName, String fileDir) throws IOException {
		if (!fileName.endsWith(".tgz"))
			fileName = fileName.concat(".tgz");
		System.out.println("INFO: Executing " + "cd " + fileDir);
		Process changeDirProc = Runtime.getRuntime().exec("cd " + fileDir);
		Process tarProc = Runtime.getRuntime().exec("tar -czvf " + fileName + " *.*");
	}
	
	
	private static String getUsage() {
		return "Usage: -sd <start date> -ed <end date> -tempdir <temporary directory> -n <name of zip file>\n " +
	"Dates should be in dd-MM-yyyy format.";
	}
	
	/** 
	 * 
	 * @return The floored date of the given date. If the given date is 30-12-2008 07:12:14 33,
	 * the return date will be 30-12-2008 00:00:00 0
	 */
	public static Date floorDate(Date date) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	
	public static Date ceilDate(Date date) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 11);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 59);
		return cal.getTime();
	}
}
