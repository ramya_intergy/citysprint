package au.com.intergy.invoice.webservice;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import au.com.intergy.invoice.common.BatchDataAccess;
import au.com.intergy.invoice.common.BatchGroup;
import au.com.intergy.invoice.common.Configuration;
import au.com.intergy.invoice.common.CustomerBatch;
import au.com.intergy.invoice.common.FileUtil;
import au.com.intergy.invoice.common.Logger;


/**
 *  Exposes invoice web services.
 */
@WebService(name="InvoiceWS", serviceName="InvoiceWebServices")
public class InvoiceServices {
	private final static Logger logger = new Logger();
	private final static Configuration conf = Configuration.getInstance();
	private final String ERROR_EMPTY_INPUT = "ER003^XML String is empty";
	private final String ERROR_EXCEPTION = "ER999^Exception during processing";
	private final String SUCCESS_MESSAGE = "ER000^SUCCESS";
	
	/**
	 * Web service operation
	 */
	
	@WebMethod
	public String[] getStylesheets() {
		return FileUtil.getStylesheetNamesAsArray(conf.getString("xslt.dir"));
	}
	
	private String createBatch(String userId, String runId, String description, String xmlBody) {
		try
		{
                    logger.log("Entering WS." );
			if (xmlBody == null) {
				return ERROR_EMPTY_INPUT;
			}
			logger.log("B4 calling parse method." );
			List<CustomerBatch> custBatches = XMLParser.parse(xmlBody);
                        logger.log("B4 calling the create method." );
			long batchId = BatchDataAccess.create(userId, runId, description, BatchGroup.getBatchGroups(custBatches), new Date());

			logger.log("Batch with ID " + batchId + " is created." );
			return  SUCCESS_MESSAGE +" for BatchID " +  batchId;
		} catch (InvoiceParseException ipe) {
			logger.log("WARNING: (Invoice WS) Unable to parse input XML " + ipe.getMessage());
			return ERROR_EXCEPTION + " Unable to parse the input XML.";
		} catch (InvalidInvoiceDataException iid) {
			logger.log("WARNING: (Invoice WS) Invalid data " + iid.getMessage());
			return ERROR_EXCEPTION + " " + iid.getMessage();
		} catch (EmptyCustomerBatchException ecbe) {
			logger.log("WARNING: (Invoice WS) Invalid data " + ecbe.getMessage());
			return ERROR_EXCEPTION + " Customer Batch needs to contain at least 1 statement or 1 invoice.";
		} catch (InvalidOutputFileFormatException ioffe) { 
			logger.log("WARNING: (Invoice WS) Invalid data " + ioffe.getMessage());
			return ERROR_EXCEPTION + " " + ioffe.getMessage();
		} catch(Exception ex) {
			logger.log(ex.getMessage());
                        ex.printStackTrace();
			return ERROR_EXCEPTION + " Internal error.";
		}
	}
	
	/*@WebParam(name = "userId") String userId
	@WebMethod
	public String generateBatch(@WebParam(name = "userId") String userId, @WebParam(name = "runId") String runId,  
			@WebParam(name = "xmlContent") String xmlContent, @WebParam(name = "description") String description)  
	throws FileNotFoundException, IOException {
		return createBatch(xmlBody);
	}
	*/
	
	@WebMethod
	public String GetXML(@WebParam(name = "xmlBody") String xmlBody)  
	throws FileNotFoundException, IOException {
            logger.log("In the GetXML Method." );
		return createBatch("", "", "", xmlBody);
	}
	
	@WebMethod
	public String generateBatch(@WebParam(name = "userId") String userId, @WebParam(name = "runId") String runId,  
			@WebParam(name = "description") String description, @WebParam(name = "xmlContent") String xmlContent)  
	throws FileNotFoundException, IOException {
            logger.log("In the GenerateBatch Method." );
		return createBatch(userId, runId, description, xmlContent);
	}
}
