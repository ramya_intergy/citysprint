package au.com.intergy.invoice.webservice;

public class InvalidInvoiceDataException extends Exception {
	private static final long serialVersionUID = 44784573993L;
        
        private String missingTagName;
        private String missingTagMsg;

    public String getMissingTagMsg() {
        return missingTagMsg;
    }

    public void setMissingTagMsg(String missingTagMsg) {
        this.missingTagMsg = missingTagMsg;
    }

    public String getMissingTagName() {
        return missingTagName;
    }

    public void setMissingTagName(String missingTagName) {
        this.missingTagName = missingTagName;
    }
        
	public InvalidInvoiceDataException(String message) {
		super(message);
	}
}
