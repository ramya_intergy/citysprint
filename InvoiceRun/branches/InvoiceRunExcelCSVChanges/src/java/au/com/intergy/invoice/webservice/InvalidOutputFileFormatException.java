package au.com.intergy.invoice.webservice;

public class InvalidOutputFileFormatException extends Exception {
	private static final long serialVersionUID = 54784573993L;
	public InvalidOutputFileFormatException(String message) {
		super(message);
	}
}
