package au.com.intergy.invoice.webservice;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import au.com.intergy.invoice.common.BatchDataAccess;
import au.com.intergy.invoice.common.BatchGroup;
import au.com.intergy.invoice.common.Configuration;
import au.com.intergy.invoice.common.CustomerBatch;
import au.com.intergy.invoice.common.FileUtil;
import au.com.intergy.invoice.webservice.XMLParser;

import junit.framework.TestCase;

public class XMLParserTest extends TestCase {

	public void testXMLParsing() {
		Configuration conf = Configuration.getInstance();
		List<CustomerBatch> custBatchesResult = new ArrayList<CustomerBatch>();
		
		try {
			String file = FileUtil.readFile(conf.getString("xml.file"), " ");

			List<CustomerBatch> custBatches =  XMLParser.parse(file);
			System.out.println("Contains number of cust batch  "+  custBatches.size());
			
			//System.out.println(custBatches);			
			//System.out.println(BatchGroup.getBatchGroups(custBatches));
			int count = 0;
			int invTotal = 0;
			for (CustomerBatch custBatch : custBatches) {
				count++;
				int statementCount = custBatch.getStatement() == null? 0 : 1;
				invTotal +=statementCount;
				invTotal += custBatch.getInvoices().size();
			}
			long batchId = BatchDataAccess.create(BatchGroup.getBatchGroups(custBatches), new Date());
			System.out.println(batchId);
		
			System.out.println("Contains total number of invoices/statements: " + invTotal);
		} catch (IOException ioe) {
			ioe.printStackTrace();
			fail("Can't read file");
		} catch (Exception ipe) {
			ipe.printStackTrace();
			fail("Can't read file");
		}
		
		
	}
}
