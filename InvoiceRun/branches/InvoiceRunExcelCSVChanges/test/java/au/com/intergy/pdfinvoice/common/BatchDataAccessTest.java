package au.com.intergy.pdfinvoice.common;

import au.com.intergy.invoice.common.BatchDataAccess;
import junit.framework.TestCase;

public class BatchDataAccessTest extends TestCase {

	public void testCreateBatch() {
		
		try {
			long id = BatchDataAccess.create();

			System.out.println(id);
		} catch (Exception e) {
			fail("Unable to create batch." + e.toString());
		}
	}
}
