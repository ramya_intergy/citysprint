package au.com.intergy.pdfinvoice.common;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import au.com.intergy.invoice.common.BatchGroup;
import au.com.intergy.invoice.common.BatchGroupDataAccess;
import au.com.intergy.invoice.common.DBAccessException;
import au.com.intergy.invoice.common.DateUtil;

import junit.framework.TestCase;

public class BatchGroupDataAccessTest extends TestCase {

	/*
	public void testFindAllBatchGroups() {
		try {
			List<BatchGroup> groups = BatchGroupDataAccess.findNonCompletedBatchGroups();
			
			System.out.println(groups);
		} catch (Exception e) {
			fail("Unable to retrieve batch groups" + e.toString());
		}
	}
	*/
	public void testFindCompletedBatchGroups() {
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.DAY_OF_MONTH, 2*(-1));
		cal = DateUtil.floorCalendar(cal);
	
		try {
			List<BatchGroup> groups = BatchGroupDataAccess.findCompletedBatchGroups(cal.getTime());

			System.out.println(groups);
		} catch (DBAccessException dbae) {
			fail();
		}
	}
}
