package au.com.intergy.pdfinvoice.common;

import java.util.List;

import au.com.intergy.invoice.common.Configuration;
import junit.framework.TestCase;

public class ConfigurationTest extends TestCase {

	public void testGetListOfString() {
		Configuration conf = Configuration.getInstance();
		
		List<String> toAddresses = conf.getListOfString("email.test.toaddresses");
		if (toAddresses.size() != 2)
			fail("There should be 2 addresses.");
		System.out.println(toAddresses);
	}
}
