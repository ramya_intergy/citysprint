package au.com.intergy.pdfinvoice.common;

import junit.framework.TestCase;
import au.com.intergy.invoice.common.Brand;
import au.com.intergy.invoice.common.CustomerBatch;
import au.com.intergy.invoice.common.CustomerBatchDataAccess;
import au.com.intergy.invoice.common.TransportMethod;
import au.com.intergy.invoice.common.CustomerBatch.Status;

public class DatabaseConnectorTest extends TestCase {

	public void testCreateCustomerBatch() {

		CustomerBatch custBatch = getDummyCustomerBatch();
		try {
			CustomerBatchDataAccess.create(custBatch);
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Cant save customer batch");
		}
	}

	private CustomerBatch getDummyCustomerBatch() {
		CustomerBatch custBatch = new CustomerBatch();

		custBatch.setBatchGroupId(new Long(1));
		custBatch.setCustomerBatchId(1);
		custBatch.setBrand(Brand.CS.name());
		custBatch.setTransportMethod(TransportMethod.EMAIL);
		custBatch.setOutputFileFormat("PDF");
		custBatch.setCreditControllerName("Carbon Paper");
		custBatch.setCreditControllerEmail("chib@intergy.com.au");
		custBatch.setStatus(Status.INIT);


		return custBatch;
	}

}
