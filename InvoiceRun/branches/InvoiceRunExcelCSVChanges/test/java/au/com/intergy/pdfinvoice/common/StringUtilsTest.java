package au.com.intergy.pdfinvoice.common;

import java.util.Date;

import junit.framework.TestCase;
import au.com.intergy.invoice.common.Configuration;
import au.com.intergy.invoice.common.FileUtil;
import au.com.intergy.invoice.common.StringUtils;


public class StringUtilsTest extends TestCase {


	public void testCompress() {
		Configuration conf = Configuration.getInstance();
		
		try {
			Date date1 = new Date();
			String originalStr =  FileUtil.readFile(conf.getString("xml.file"), " ");
			
			String compressedStr = StringUtils.compressToString("test", originalStr);
			Date date2 = new Date();
			System.out.println("It takes " +  (date2.getTime()- date1.getTime()) + "ms to compress " + originalStr.length());
			
			String decompressedStr = StringUtils.decompress(compressedStr);
			Date date3 = new Date();
			System.out.println("It takes " + (date3.getTime()- date2.getTime()) + "ms to decompress.");
			assertEquals(originalStr, decompressedStr);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}	
	}
}
