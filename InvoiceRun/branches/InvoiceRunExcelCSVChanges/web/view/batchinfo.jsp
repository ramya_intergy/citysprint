<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

    <head>
        <title>Invoice Run Batch Info</title>
        <link rel="stylesheet" href="./css/invoice.css">
    </head>

    <body class="box">

        <c:if test="${not empty param.batchId}">
            <table width="100%" class="box">
                <tr  >
                    <th valign="top" align="left" width="150">Batch ID:</th>
                    <td valign="top" ><c:out value="${param.batchId}" /></td>
                </tr>
                <tr >
                    <th valign="top" align="left" width="150">Start Account No:</th>
                    <td valign="top" ><c:out value="${requestScope.batchDetails.startAccountNo}"/></td>
                </tr>
                <tr >
                    <th valign="top" align="left" width="150">End Account No:</th>
                    <td valign="top" ><c:out value="${requestScope.batchDetails.endAccountNo}"/></td>
                </tr>
                <tr >
                    <th valign="top" align="left" width="150">No. of Invoices:</th>
                    <td valign="top" ><c:out value="${requestScope.batchDetails.noOfInvoicesInBatch}"/></td>
                </tr>

            </table>

            <table width="100%" class="box">
                <tr  width="100%" class="boxheading2">

                    <th align="left" width="15%">Cust Batch ID</th>
                    <th align="left" width="15%">Customer Key</th>
                    <th align="left" width="15%">Transport Method</th>
                    <th align="left" width="35%">Transport Address</th>

                    <th align="left" width="10%">Output Format</th>
                    <th align="left" width="10%">Brand</th>
                    
                </tr>

                <c:forEach var="details" items="${requestScope.batchDetails.customerBatches}"
                           varStatus="status">                         
                    <tr>                                                                 
                        <td ><c:out value="${details.customerBatchId}"/></td>
                        <td ><c:out value="${details.customerKey}"/></td> 
                        <td ><c:out value="${details.transportMethod}"/></td>
                        <td ><c:out value="${details.transportAddress}"/></td>
                        <td ><c:out value="${details.outputFileFormat}"/></td> 
                        <td ><c:out value="${details.brand}"/></td>
                           
                    </tr>                                                       
                </c:forEach>                                                                    
            </table>                                                          



        </c:if>

    </body>
</html>