<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
  <title>Invoice Run Description</title>
  <link rel="stylesheet" href="./css/invoice.css">
</head>

<body style="background-color:	#EFEFEF;">

<c:if test="${not empty param.runId}">
<table width="100%" height="15%" class="box">
  <tr  height="20">
    <th valign="top" align="left" width="100">Run ID</th>
    <td valign="top" ><c:out value="${param.runId}" /></td>
  </tr>
  <tr>
    <th valign="top" align="left" width="100">Description</th>
    <td valign="top" ><c:out value="${requestScope.batchDetails.description}"/></td>
  </tr>

</table>

<table width="100%" class="box">
  <tr  width="100%" class="boxheading2">
      
    <th align="left" width="20%">Batches ID</th>
    <th align="left" width="20%">Start Account No</th>
    <th align="left" width="20%">End Account No</th>
    <th align="left" width="20%">No. of Invoices</th>
    <th align="left" width="20%">Customer Batch Count</th>

  </tr>
  <c:forEach var="details" items="${requestScope.batchDetails.customerBatches}" varStatus="status">
  <tr>
    <td ><c:out value="${details.batchId}"/></td>
    <td ><c:out value="${details.startAccountNo}"/></td>
    <td ><c:out value="${details.endAccountNo}"/></td>
    <td ><c:out value="${details.noOfInvoicesInBatch}"/></td>
    <td ><c:out value="${details.customerBatchCounts}"/></td>

  </tr>
</c:forEach>
</table>    
    
    
</c:if>

</body>
</html>