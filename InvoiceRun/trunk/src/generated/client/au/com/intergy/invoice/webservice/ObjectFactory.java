
package au.com.intergy.invoice.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the au.com.intergy.invoice.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetStylesheetsResponse_QNAME = new QName("http://webservice.invoice.intergy.com.au/", "getStylesheetsResponse");
    private final static QName _GenerateBatch_QNAME = new QName("http://webservice.invoice.intergy.com.au/", "generateBatch");
    private final static QName _GetStylesheets_QNAME = new QName("http://webservice.invoice.intergy.com.au/", "getStylesheets");
    private final static QName _IOException_QNAME = new QName("http://webservice.invoice.intergy.com.au/", "IOException");
    private final static QName _GenerateBatchResponse_QNAME = new QName("http://webservice.invoice.intergy.com.au/", "generateBatchResponse");
    private final static QName _GetXMLResponse_QNAME = new QName("http://webservice.invoice.intergy.com.au/", "GetXMLResponse");
    private final static QName _GetXML_QNAME = new QName("http://webservice.invoice.intergy.com.au/", "GetXML");
    private final static QName _FileNotFoundException_QNAME = new QName("http://webservice.invoice.intergy.com.au/", "FileNotFoundException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: au.com.intergy.invoice.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IOException }
     * 
     */
    public IOException createIOException() {
        return new IOException();
    }

    /**
     * Create an instance of {@link GenerateBatch }
     * 
     */
    public GenerateBatch createGenerateBatch() {
        return new GenerateBatch();
    }

    /**
     * Create an instance of {@link GenerateBatchResponse }
     * 
     */
    public GenerateBatchResponse createGenerateBatchResponse() {
        return new GenerateBatchResponse();
    }

    /**
     * Create an instance of {@link GetStylesheetsResponse }
     * 
     */
    public GetStylesheetsResponse createGetStylesheetsResponse() {
        return new GetStylesheetsResponse();
    }

    /**
     * Create an instance of {@link GetXMLResponse }
     * 
     */
    public GetXMLResponse createGetXMLResponse() {
        return new GetXMLResponse();
    }

    /**
     * Create an instance of {@link GetStylesheets }
     * 
     */
    public GetStylesheets createGetStylesheets() {
        return new GetStylesheets();
    }

    /**
     * Create an instance of {@link GetXML }
     * 
     */
    public GetXML createGetXML() {
        return new GetXML();
    }

    /**
     * Create an instance of {@link FileNotFoundException }
     * 
     */
    public FileNotFoundException createFileNotFoundException() {
        return new FileNotFoundException();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStylesheetsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.invoice.intergy.com.au/", name = "getStylesheetsResponse")
    public JAXBElement<GetStylesheetsResponse> createGetStylesheetsResponse(GetStylesheetsResponse value) {
        return new JAXBElement<GetStylesheetsResponse>(_GetStylesheetsResponse_QNAME, GetStylesheetsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateBatch }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.invoice.intergy.com.au/", name = "generateBatch")
    public JAXBElement<GenerateBatch> createGenerateBatch(GenerateBatch value) {
        return new JAXBElement<GenerateBatch>(_GenerateBatch_QNAME, GenerateBatch.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStylesheets }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.invoice.intergy.com.au/", name = "getStylesheets")
    public JAXBElement<GetStylesheets> createGetStylesheets(GetStylesheets value) {
        return new JAXBElement<GetStylesheets>(_GetStylesheets_QNAME, GetStylesheets.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IOException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.invoice.intergy.com.au/", name = "IOException")
    public JAXBElement<IOException> createIOException(IOException value) {
        return new JAXBElement<IOException>(_IOException_QNAME, IOException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateBatchResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.invoice.intergy.com.au/", name = "generateBatchResponse")
    public JAXBElement<GenerateBatchResponse> createGenerateBatchResponse(GenerateBatchResponse value) {
        return new JAXBElement<GenerateBatchResponse>(_GenerateBatchResponse_QNAME, GenerateBatchResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetXMLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.invoice.intergy.com.au/", name = "GetXMLResponse")
    public JAXBElement<GetXMLResponse> createGetXMLResponse(GetXMLResponse value) {
        return new JAXBElement<GetXMLResponse>(_GetXMLResponse_QNAME, GetXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.invoice.intergy.com.au/", name = "GetXML")
    public JAXBElement<GetXML> createGetXML(GetXML value) {
        return new JAXBElement<GetXML>(_GetXML_QNAME, GetXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FileNotFoundException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.invoice.intergy.com.au/", name = "FileNotFoundException")
    public JAXBElement<FileNotFoundException> createFileNotFoundException(FileNotFoundException value) {
        return new JAXBElement<FileNotFoundException>(_FileNotFoundException_QNAME, FileNotFoundException.class, null, value);
    }

}
