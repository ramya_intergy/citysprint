
package au.com.intergy.invoice.webservice.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getStylesheets", namespace = "http://webservice.invoice.intergy.com.au/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStylesheets", namespace = "http://webservice.invoice.intergy.com.au/")
public class GetStylesheets {


}
