
package au.com.intergy.invoice.webservice.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "GetXML", namespace = "http://webservice.invoice.intergy.com.au/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetXML", namespace = "http://webservice.invoice.intergy.com.au/")
public class GetXML {

    @XmlElement(name = "xmlBody", namespace = "")
    private String xmlBody;

    /**
     * 
     * @return
     *     returns String
     */
    public String getXmlBody() {
        return this.xmlBody;
    }

    /**
     * 
     * @param xmlBody
     *     the value for the xmlBody property
     */
    public void setXmlBody(String xmlBody) {
        this.xmlBody = xmlBody;
    }

}
