package au.com.intergy.invoice.common;

import java.util.ArrayList;
import java.util.List;

public class Statement {

	public final static int STATEMENT_SEQUENCE_NUMBER = 1;
	
	protected long id;
	protected long custBatchId;
	protected String xmlBody;
	
	private String xsltFileName;
	private String generatedFileName;
	
	private String outputFormat;
	private String brand;
	private String printFlag;
	private boolean lastPrintItem;
	private String accountNumber;
        
        private String accountName;
        private String accountAddr1;
        private String accountAddr2;
        private String accountAddr3;
        private String accountAddr4;
        private String accountAddr5;
        private String accountAddr6;
        private String accountAddr7;

	private String creditControllerName = "";
	private String creditControllerEmail = "";
	private String creditControllerPhone = "";

        private String creditControllerTitle = "";
        private String creditControllerOffice = "";
        private String creditControllerFaxNo = "";
//    private String invoiceTotal;
//        private String creditControllerTitle;

        public String getAccountAddr1() {
            return accountAddr1;
        }

        public void setAccountAddr1(String accountAddr1) {
            this.accountAddr1 = accountAddr1;
        }

        public String getAccountAddr2() {
            return accountAddr2;
        }

        public void setAccountAddr2(String accountAddr2) {
            this.accountAddr2 = accountAddr2;
        }

        public String getAccountAddr3() {
            return accountAddr3;
        }

        public void setAccountAddr3(String accountAddr3) {
            this.accountAddr3 = accountAddr3;
        }

        public String getAccountAddr4() {
            return accountAddr4;
        }

        public void setAccountAddr4(String accountAddr4) {
            this.accountAddr4 = accountAddr4;
        }
        
         public String getAccountAddr5() {
            return accountAddr5;
        }

        public void setAccountAddr5(String accountAddr4) {
            this.accountAddr5 = accountAddr4;
        }

        public String getAccountAddr6() {
            return accountAddr6;
        }

        public void setAccountAddr6(String accountAddr6) {
            this.accountAddr6 = accountAddr6;
        }

        public String getAccountAddr7() {
            return accountAddr7;
        }

        public void setAccountAddr7(String accountAddr7) {
            this.accountAddr7 = accountAddr7;
        }
        
        public String getAccountName() {
            return accountName;
        }

        public void setAccountName(String accountName) {
            this.accountName = accountName;
        }
    
        public String getCreditControllerFaxNo() {
            return creditControllerFaxNo;
        }

        public void setCreditControllerFaxNo(String creditControllerFaxNo) {
            this.creditControllerFaxNo = creditControllerFaxNo;
        }

        public String getCreditControllerOffice() {
            return creditControllerOffice;
        }

        public void setCreditControllerOffice(String creditControllerOffice) {
            this.creditControllerOffice = creditControllerOffice;
        }

        public String getCreditControllerTitle() {
            return creditControllerTitle;
        }

        public void setCreditControllerTitle(String creditControllerTitle) {
            this.creditControllerTitle = creditControllerTitle;
        }
    
	private String status;
	private String readyStatus;
	
	/**
	 * Y for Sent/Printed
	 * N for otherwise
	 */
	private String sendStatus;

	private CustomerBatch custBatch;
	
	/**
	 * FAILED if file cannot be generated
	 *
	 */
	public enum Status {INIT, PROCESSING, SUCCESS, FAILED, CANCELLED};
	
	public Statement() {
		status = Status.INIT.name();
		readyStatus = "N";
		sendStatus = "N";
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id  = id;
	}
	
	public String getReadyStatus() {
		return readyStatus;
	}
	
	public void setReadyStatus(String readyStatus) {
		this.readyStatus = readyStatus;
	}
	
	public String getSendStatus() {
		return sendStatus;
	}
	
	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}
	
	private List<String> messages = new ArrayList<String>();
	
	public long getCustBatchId() {
		return custBatchId;
	}
	public void setCustBatchId(long custBatchId) {
		this.custBatchId = custBatchId;
	}
	public String getXmlBody() {
		return xmlBody;
	}
	public void setXmlBody(String xmlBody) {
		this.xmlBody = xmlBody;
	}

	public String getXsltFileName() {
		return xsltFileName;
	}
	public void setXsltFileName(String xsltFileName) {
		this.xsltFileName = xsltFileName;
	}
	public String getGeneratedFileName() {
		return generatedFileName;
	}
	public void setGeneratedFileName(String generatedFileName) {
		this.generatedFileName = generatedFileName;
	}
	public int getSequenceNumber() {
		return STATEMENT_SEQUENCE_NUMBER;
	}
	
	public String getOutputFormat() {
		return outputFormat;
	}
	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getPrintFlag() {
		return printFlag;
	}
	public void setPrintFlag(String printFlag) {
		this.printFlag = printFlag;
	}
	public boolean isLastPrintItem() {
		return lastPrintItem;
	}
	public void setLastPrintItem(boolean lastPrintItem) {
		this.lastPrintItem = lastPrintItem;
	}
	
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getCreditControllerName() {
		return creditControllerName;
	}
	public void setCreditControllerName(String creditControllerName) {
		this.creditControllerName = creditControllerName;
	}
	public String getCreditControllerEmail() {
		return creditControllerEmail;
	}
	public void setCreditControllerEmail(String creditControllerEmail) {
		this.creditControllerEmail = creditControllerEmail;
	}
	public String getCreditControllerPhone() {
		return creditControllerPhone;
	}
	public void setCreditControllerPhone(String creditControllerPhone) {
		this.creditControllerPhone = creditControllerPhone;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public List<String> getMessages() {
		return messages;
	}
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	
	public CustomerBatch getCustBatch() {
		return custBatch;
	}
	public void setCustBatch(CustomerBatch custBatch) {
		this.custBatch = custBatch;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Statement) {
			return this.id == ((Statement) obj).id;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return ((int)id) * 577;
	}
	
	
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation 
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "],";
	
	    StringBuilder retValue = new StringBuilder();
	    
	    retValue.append("\n\tStatement [ ")
	        .append(super.toString()).append(TAB)
	        .append("[custBatchId = ").append(this.custBatchId).append(TAB)
	        .append("[xmlBody = ").append(this.xmlBody).append(TAB)
	        .append("[xsltFileName = ").append(this.xsltFileName).append(TAB)
	        .append("[generatedFileName = ").append(this.generatedFileName).append(TAB)
	        .append("[outputFormat = ").append(this.outputFormat).append(TAB)
	        .append("[brand = ").append(this.brand).append(TAB)
	        .append("[printFlag = ").append(this.printFlag).append(TAB)
	        .append("[lastPrintItem = ").append(this.lastPrintItem).append(TAB)
	        .append("[accountNumber = ").append(this.accountNumber).append(TAB)
	        .append("[creditControllerName = ").append(this.creditControllerName).append(TAB)
	        .append("[creditControllerEmail = ").append(this.creditControllerEmail).append(TAB)
	        .append("[creditControllerPhone = ").append(this.creditControllerPhone).append(TAB)
                 .append("[creditControllerTitle = ").append(this.creditControllerTitle).append(TAB)
                     .append("[creditControllerOffice = ").append(this.creditControllerOffice).append(TAB)
                     .append("[creditControllerFaxNo = ").append(this.creditControllerFaxNo).append(TAB)
	        .append("[messages = ").append(this.messages).append(TAB)
	        .append(" ]");
	    
	    return retValue.toString();
	}
}
