package au.com.intergy.invoice.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represents a batch of customer invoices received in 1 Web Services call.
 * 
 * @author Chi Bui
 */
public class Batch {

	private long id;
	private Date createdTime;
	List<BatchGroup> batchGroups = new ArrayList<BatchGroup>();
	
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public Date getCreatedTime() {
		return createdTime;
	}
	
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	
	public List<BatchGroup> getBatchGroups() {
		return batchGroups;
	}

	public void setCustBatches(List<BatchGroup> batchGroups) {
		this.batchGroups = batchGroups;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation 
	 * of this object.
	 */
	public String toString()
	{
		final String TAB = "],";
	
	    StringBuilder retValue = new StringBuilder();
	    
	    retValue.append("Batch ( ")
	        .append(super.toString()).append(TAB)
	        .append("id = ").append(this.id).append(TAB)
	        .append("createdTime = ").append(this.createdTime).append(TAB)
	        .append("custBatches = ").append(this.batchGroups).append(TAB)
	        .append(" )");
	    
	    return retValue.toString();
	}
}
