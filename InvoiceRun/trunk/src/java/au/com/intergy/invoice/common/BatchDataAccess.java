package au.com.intergy.invoice.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class BatchDataAccess {
	
	public synchronized static long create(String userId, String runId, String description,
			List<BatchGroup> batchGroups, java.util.Date batchSubmittedDate) 
	throws DBAccessException {
		Connection conn = null;
		long batchIdResult = 0;
		Statement stmt = null;
		
		try {
			conn = DatabaseConnector.getConnection();
			String query = "INSERT INTO batch (UserID, RunID, Description) VALUES ('" + userId +"', '" +
			runId + "' ,'" + description +"')";
			stmt = conn.createStatement();
			conn.setAutoCommit(false);
			stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);

			ResultSet keys = stmt.getGeneratedKeys();
			
			if (keys.next()) 
				batchIdResult = keys.getLong(1);
			
			for (BatchGroup group : batchGroups) {
				group.setBatchId(batchIdResult);
				group.setSubmittedDate(batchSubmittedDate);
				BatchGroupDataAccess.create(conn, group);
			}

			conn.commit();
		} catch (Exception e) {
			e.printStackTrace();
			try { 
				if (conn != null) 
					conn.rollback(); 
			} catch (SQLException sqle) { }		
			throw new DBAccessException("Unable to create Batch: " + e.getMessage());
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				DatabaseConnector.returnConnection(conn); 
			} catch (SQLException sqle) { }			
		}
		
		return batchIdResult;
		
	}
	
	/** 
     * @return ID of the newly created batch.
	 */
	public synchronized static long create(List<BatchGroup> batchGroups, java.util.Date batchSubmittedDate) 
	throws DBAccessException {
		Connection conn = null;
		long batchIdResult = 0;
		Statement stmt = null;
		
		try {
			conn = DatabaseConnector.getConnection();
			
			stmt = conn.createStatement();
			conn.setAutoCommit(false);
			stmt.executeUpdate("INSERT INTO batch VALUES ()", Statement.RETURN_GENERATED_KEYS);

			ResultSet keys = stmt.getGeneratedKeys();
			
			if (keys.next()) 
				batchIdResult = keys.getLong(1);
			
			for (BatchGroup group : batchGroups) {
				group.setBatchId(batchIdResult);
				group.setSubmittedDate(batchSubmittedDate);
				BatchGroupDataAccess.create(conn, group);
			}

			conn.commit();
		} catch (Exception e) {
			e.printStackTrace();
			try { 
				if (conn != null) 
					conn.rollback(); 
			} catch (SQLException sqle) { }		
			throw new DBAccessException("Unable to create Batch: " + e.getMessage());
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				DatabaseConnector.returnConnection(conn); 
			} catch (SQLException sqle) { }			
		}
		
		return batchIdResult;
		
	}
	
	
	public synchronized static long create() 
	throws DBAccessException {
		Connection conn = null;
		int result = 0;
		Statement stmt = null;
		
		try {
			conn = DatabaseConnector.getConnection();
			
			stmt = conn.createStatement();
			conn.setAutoCommit(false);
			stmt.executeUpdate("INSERT INTO batch VALUES ()", Statement.RETURN_GENERATED_KEYS);

			ResultSet keys = stmt.getGeneratedKeys();
			
			if (keys.next()) 
				System.out.println(keys.getString(1));
			
			conn.commit();
		} catch (Exception e) {
			throw new DBAccessException(e.getMessage());
		} finally {
			
			try { 
				if (stmt != null)
					stmt.close();
				DatabaseConnector.returnConnection(conn); 
			} catch (SQLException e) { }			
		}
		
		return result;
	}	
	
	public static BatchesDetails findDescriptionByBatchId(String batchID, String runId)
	throws DBAccessException {
		Connection conn = null;
		PreparedStatement stmt = null;
		String despRet = null;
		BatchesDetails batchDetails = new BatchesDetails();
                batchDetails.setBatchId(batchID);
		try {
			String query = "select * from batch WHERE BatchID = ? ";


			conn = DatabaseConnector.getConnection();
			stmt = conn.prepareStatement(query);

			stmt.setString(1, batchID);
			
			ResultSet result = stmt.executeQuery();
                        
			if (result.next()) {
				despRet = result.getString("Description");
                                batchDetails.setDescription(despRet);
			}
                        
                        DatabaseConnector.closeDBResources(stmt, result);
                        
//                        query = "select  * from customerbatchdetails  where BatchGroupID in (select id from batchgroup where batchid = ? ) ";
//                        query = "SELECT batchid, customerbatchcount, invoicecount  "
                        query = "SELECT batchid, sum(invoicecount) noOfInvoices , sum(customerBatchCount) custBatchCounts "
                                + "FROM  batchgroup b "
                                + "WHERE    batchid in (select batchid from batch where runid = ?) group by batchid ";

			stmt = conn.prepareStatement(query);

//			stmt.setString(1, batchID);
			stmt.setString(1, runId);
                        
                       
			result = stmt.executeQuery();
                        
                        List<BatchInvoicesInfo> customerBatches = new ArrayList<BatchInvoicesInfo>();
                                
                        while (result.next()){
                            BatchInvoicesInfo custBatch = new BatchInvoicesInfo();
                            custBatch.setBatchId(result.getString("batchid"));
                           
                            custBatch.setNoOfInvoicesInBatch(result.getInt("noOfInvoices"));
                            custBatch.setCustomerBatchCounts(result.getInt("custBatchCounts"));
                            
                            findBatchInfo1ByBatchId(custBatch.getBatchId(), custBatch);
                            
                            customerBatches.add(custBatch);
                        }
                        
                        batchDetails.setCustomerBatches(customerBatches);
                        
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			try {
				if (stmt != null) 
					stmt.close();
				DatabaseConnector.returnConnection(conn); 
			} catch (Exception e) {}
		}
		
		return batchDetails;
	}
        
        
        public static BatchInvoicesInfo findBatchInfoByBatchId(String batchID)
	throws DBAccessException {
		Connection conn = null;
		PreparedStatement stmt = null;
		String despRet = null;
		BatchInvoicesInfo batchDetails = new BatchInvoicesInfo();
                batchDetails.setBatchId(batchID);
		try {
			String query = "select sum(InvoiceCount) no_of_invoices from batchgroup where batchId = ? ";


			conn = DatabaseConnector.getConnection();
			stmt = conn.prepareStatement(query);

			stmt.setString(1, batchID);
			
			ResultSet result = stmt.executeQuery();
                        
			if (result.next()) {
				despRet = result.getString("no_of_invoices");
                                try{
                                batchDetails.setNoOfInvoicesInBatch(Long.valueOf(despRet));
                                }catch (NumberFormatException nbef){
                                    nbef.printStackTrace();
                                }
			}
                        
                        DatabaseConnector.closeDBResources(stmt, result);
                        
//                        query = "select id, CustomerNumber from invoicedetails where BatchID_CustKey = ? and id = (select min(id) from invoicedetails where BatchID_CustKey = ?) "
//                                 + " union all " 
//                                 + " select id, CustomerNumber from invoicedetails where BatchID_CustKey = ? and id = (select max(id) from invoicedetails where BatchID_CustKey = ?) " ;

//                       query = "SELECT id, customernumber FROM invoicedetails WHERE  batchid_custkey in (SELECT batchid_custkey FROM customerbatchdetails WHERE batchgroupid IN (SELECT id FROM batchgroup WHERE batchid = ?)) "
//                                + " AND id = (SELECT min(id) FROM invoicedetails WHERE batchid_custkey in (SELECT batchid_custkey FROM customerbatchdetails WHERE batchgroupid IN (SELECT id FROM batchgroup WHERE batchid = ?))) "
//                                + " UNION ALL "
//                                + " SELECT id, customernumber FROM invoicedetails WHERE  batchid_custkey in (SELECT batchid_custkey FROM customerbatchdetails WHERE batchgroupid IN (SELECT id FROM batchgroup WHERE batchid = ?)) "
//                                + " AND id = (SELECT max(id) FROM invoicedetails WHERE batchid_custkey in (SELECT batchid_custkey FROM customerbatchdetails WHERE batchgroupid IN (SELECT id FROM batchgroup WHERE batchid = ?))) ";
                        
                       
                        query = "select id, customernumber from invoicedetails where id = ( SELECT min(invoicedetails.id) FROM invoicedetails , customerbatchdetails, batchgroup WHERE invoicedetails.batchid_custkey = customerbatchdetails.BatchID_CustKey and batchid = ? and batchgroup.id = batchgroupid) "
  
                               + " UNION ALL " +

                                 " select id, customernumber from invoicedetails where id = ( SELECT max(invoicedetails.id) FROM invoicedetails , customerbatchdetails, batchgroup WHERE invoicedetails.batchid_custkey = customerbatchdetails.BatchID_CustKey and batchid = ? and batchgroup.id = batchgroupid) " ;

                        
			stmt = conn.prepareStatement(query);

			stmt.setString(1, batchID);
                        stmt.setString(2, batchID);
//                        stmt.setString(3, batchID);
//                        stmt.setString(4, batchID);
			
			result = stmt.executeQuery();
                        
                        int startId = -1;
                        int endId = -1;
                        String startAccNo = "";
                        String endAccNo = "";
                        int counter = 1;
                        while (result.next()){
                            if (counter++ == 1){
                                startId = result.getInt("id");
                                startAccNo = result.getString("CustomerNumber");
                            }else {
                                endId = result.getInt("id");
                                endAccNo = result.getString("CustomerNumber");
                            }
                        }
                        
                        if (startId < endId) {
                            batchDetails.setStartAccountNo(startAccNo);
                            batchDetails.setEndAccountNo(endAccNo);
                        } else {
                            batchDetails.setStartAccountNo(endAccNo);
                            batchDetails.setEndAccountNo(startAccNo);
                        }
                        
                        query = "select  * from customerbatchdetails  where BatchGroupID in (select id from batchgroup where batchid = ? ) ";
                        
                        DatabaseConnector.closeDBResources(stmt, result);
                        
                        stmt = conn.prepareStatement(query);
                        stmt.setString(1, batchID);
                        
                        result = stmt.executeQuery();
                        List<CustomerBatch> customerBatches = new ArrayList<CustomerBatch>();
                                
                        while (result.next()){
                            CustomerBatch custBatch = new CustomerBatch();
                           custBatch.setCustomerKey(result.getString("CustomerKey"));
				custBatch.setBatchGroupId(result.getLong("BatchGroupID"));
				custBatch.setCustomerBatchId(result.getLong("BatchID_CustKey"));
				custBatch.setStatus(CustomerBatch.Status.valueOf(result.getString("Status")));
				custBatch.setBrand(result.getString("Brand_Group"));
				custBatch.setTransportMethod(
						TransportMethod.valueOf(result.getString("TransportMethod")));
				custBatch.setOutputFileFormat(result.getString("OutputFormat"));
				custBatch.setCreditControllerName(result.getString("CreditController"));
				custBatch.setCreditControllerEmail(result.getString("CreditControllerEmail"));
				custBatch.setCreditControllerPhone(result.getString("CreditControllerPhone"));
                                
                                custBatch.setCreditControllerTitle(result.getString("CreditControllerTitle"));
				custBatch.setCreditControllerOffice(result.getString("CreditControllerOffice"));
				custBatch.setCreditControllerFaxNo(result.getString("CreditControllerFax"));
                                
				custBatch.setTransportAddress(result.getString("TransportAddress"));
                                custBatch.setScheduleOutputFormat(result.getString("ScheduleOutputFormat"));
                            
                            
                            customerBatches.add(custBatch);
                        }
                        
                        batchDetails.setCustomerBatches(customerBatches);
                        
                        
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			try {
				if (stmt != null) 
					stmt.close();
				DatabaseConnector.returnConnection(conn); 
			} catch (Exception e) {}
		}
		
		return batchDetails;
	}
        
        private static void findBatchInfo1ByBatchId(String batchID, BatchInvoicesInfo batchDetails)
	throws DBAccessException {
		Connection conn = null;
		PreparedStatement stmt = null;
		String despRet = null;
//		BatchInvoicesInfo batchDetails = new BatchInvoicesInfo();
//                batchDetails.setBatchId(batchID);
		try {
//			String query = "select sum(InvoiceCount) no_of_invoices from batchgroup where batchId = ? ";


			conn = DatabaseConnector.getConnection();
			
                        
//                        query = "select id, CustomerNumber from invoicedetails where BatchID_CustKey = ? and id = (select min(id) from invoicedetails where BatchID_CustKey = ?) "
//                                 + " union all " 
//                                 + " select id, CustomerNumber from invoicedetails where BatchID_CustKey = ? and id = (select max(id) from invoicedetails where BatchID_CustKey = ?) " ;

//                       String query = "SELECT id, customernumber FROM invoicedetails WHERE  batchid_custkey in (SELECT batchid_custkey FROM customerbatchdetails WHERE batchgroupid IN (SELECT id FROM batchgroup WHERE batchid = ?)) "
//                                + " AND id = (SELECT min(id) FROM invoicedetails WHERE batchid_custkey in (SELECT batchid_custkey FROM customerbatchdetails WHERE batchgroupid IN (SELECT id FROM batchgroup WHERE batchid = ?))) "
//                                + " UNION ALL "
//                                + " SELECT id, customernumber FROM invoicedetails WHERE  batchid_custkey in (SELECT batchid_custkey FROM customerbatchdetails WHERE batchgroupid IN (SELECT id FROM batchgroup WHERE batchid = ?)) "
//                                + " AND id = (SELECT max(id) FROM invoicedetails WHERE batchid_custkey in (SELECT batchid_custkey FROM customerbatchdetails WHERE batchgroupid IN (SELECT id FROM batchgroup WHERE batchid = ?))) ";
                        
                        String query = "select id, customernumber from invoicedetails where id = ( SELECT min(invoicedetails.id) FROM invoicedetails , customerbatchdetails, batchgroup WHERE invoicedetails.batchid_custkey = customerbatchdetails.BatchID_CustKey and batchid = ? and batchgroup.id = batchgroupid)"
  
                                        + " UNION ALL " +

                                        "select id, customernumber from invoicedetails where id = ( SELECT max(invoicedetails.id) FROM invoicedetails , customerbatchdetails, batchgroup WHERE invoicedetails.batchid_custkey = customerbatchdetails.BatchID_CustKey and batchid = ? and batchgroup.id = batchgroupid) ";

                        stmt = conn.prepareStatement(query);

			stmt.setString(1, batchID);
                        stmt.setString(2, batchID);
//                        stmt.setString(3, batchID);
//                        stmt.setString(4, batchID);
			
			ResultSet result = stmt.executeQuery();
                        
                        int startId = -1;
                        int endId = -1;
                        String startAccNo = "";
                        String endAccNo = "";
                        int counter = 1;
                        while (result.next()){
                            if (counter++ == 1){
                                startId = result.getInt("id");
                                startAccNo = result.getString("CustomerNumber");
                            }else {
                                endId = result.getInt("id");
                                endAccNo = result.getString("CustomerNumber");
                            }
                        }
                        
                        if (startId < endId) {
                            batchDetails.setStartAccountNo(startAccNo);
                            batchDetails.setEndAccountNo(endAccNo);
                        } else {
                            batchDetails.setStartAccountNo(endAccNo);
                            batchDetails.setEndAccountNo(startAccNo);
                        }
                        
                        
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			try {
				if (stmt != null) 
					stmt.close();
				DatabaseConnector.returnConnection(conn); 
			} catch (Exception e) {}
		}
		
		
	}
}
