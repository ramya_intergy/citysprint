/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.intergy.invoice.common;

import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;

/**
 *
 * @author adnann
 */
public class BatchInvoicesInfo implements Serializable{
    private String batchId;
    private String startAccountNo;
    private String endAccountNo;
    private long noOfInvoicesInBatch;
    private long customerBatchCounts;

    public long getCustomerBatchCounts() {
        return customerBatchCounts;
    }

    public void setCustomerBatchCounts(long customerBatchCounts) {
        this.customerBatchCounts = customerBatchCounts;
    }
    
    private List<CustomerBatch> customerBatches = new ArrayList<CustomerBatch>();

    public List<CustomerBatch> getCustomerBatches() {
        return customerBatches;
    }

    public void setCustomerBatches(List<CustomerBatch> customerBatches) {
        this.customerBatches = customerBatches;
    }
    
    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getEndAccountNo() {
        return endAccountNo;
    }

    public void setEndAccountNo(String endAccountNo) {
        this.endAccountNo = endAccountNo;
    }

    public long getNoOfInvoicesInBatch() {
        return noOfInvoicesInBatch;
    }

    public void setNoOfInvoicesInBatch(long noOfInvoicesInBatch) {
        this.noOfInvoicesInBatch = noOfInvoicesInBatch;
    }

    public String getStartAccountNo() {
        return startAccountNo;
    }

    public void setStartAccountNo(String startAccountNo) {
        this.startAccountNo = startAccountNo;
    }
    
}
