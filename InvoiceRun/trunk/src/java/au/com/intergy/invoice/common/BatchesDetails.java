/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.intergy.invoice.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author adnann
 */
public class BatchesDetails implements Serializable{
    private String batchId;
    private String description ;

    
    private List<BatchInvoicesInfo> customerBatches = new ArrayList<BatchInvoicesInfo>();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public List<BatchInvoicesInfo> getCustomerBatches() {
        return customerBatches;
    }

    public void setCustomerBatches(List<BatchInvoicesInfo> customerBatches) {
        this.customerBatches = customerBatches;
    }
}
