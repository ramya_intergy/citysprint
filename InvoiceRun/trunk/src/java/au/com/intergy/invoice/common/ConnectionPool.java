package au.com.intergy.invoice.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

class ConnectionPool { 
	private final static Configuration config = Configuration.getInstance();
	private static ConnectionPool instance;
	private ConnectionPool() {}
	
	public final static String DRIVER = config.getString("db.driver", "com.mysql.jdbc.Driver");
	public final static String URL = config.getString("db.url", "jdbc:mysql://localhost:3306/invoicerun");
	public final static String USER = config.getString("db.user");
	public final static String PASSWORD = config.getString("db.password");
	
	static ConnectionPool getInstance() {
		if (instance == null)
			instance = new ConnectionPool();

		System.out.println("DB details: " + DRIVER + " " + URL);
		System.out.println("User " + USER);
		return instance;
	}
	
	Queue<Connection> connections = new ArrayBlockingQueue<Connection>(200);
	
	Connection getConnection() throws SQLException {
		if (connections.isEmpty()) {
			return createConnection();
		}
		return connections.poll();	
	}
	
	void returnConnection(Connection connection) {
		connections.offer(connection);
	}
	
	private Connection createConnection() throws SQLException {
		Connection conDB = null;

		try
		{
			Class.forName(DRIVER).newInstance();
			conDB  = DriverManager.getConnection(URL+ "user="+ USER+ "&password=" +PASSWORD);
		} catch (Exception e) {
			throw new SQLException("Error opening database connection" +
					e.toString());
		}

		return conDB;
	}	
}
