package au.com.intergy.invoice.common;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * A customer batch consists of all invoices related to 1 customer received from
 * 1 web service call.
 *
 * @author Chi Bui
 */
public class CustomerBatch {

    /**
     * Id of the web service call
     */
    private Long batchGroupId;
    private long customerBatchId;
    /**
     * Brand, group, or company name
     */
    private String brand;
    /**
     * Customer key or account number
     */
    private String customerKey;
    private String creditControllerName;
    private String creditControllerEmail;
    private String creditControllerPhone;
    private String creditControllerTitle;
    private String creditControllerOffice;
    private String creditControllerFaxNo;
    private String invoiceTotal = "0.0";
//        private String creditControllerTitle;

    public String getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(String invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }

    public String getCreditControllerFaxNo() {
        return creditControllerFaxNo;
    }

    public void setCreditControllerFaxNo(String creditControllerFaxNo) {
        this.creditControllerFaxNo = creditControllerFaxNo;
    }

    public String getCreditControllerOffice() {
        return creditControllerOffice;
    }

    public void setCreditControllerOffice(String creditControllerOffice) {
        this.creditControllerOffice = creditControllerOffice;
    }

    public String getCreditControllerTitle() {
        return creditControllerTitle;
    }

    public void setCreditControllerTitle(String creditControllerTitle) {
        this.creditControllerTitle = creditControllerTitle;
    }

//    public String getInvoiceTotal() {
//        return invoiceTotal;
//    }
//
//    public void setInvoiceTotal(String invoiceTotal) {
//        this.invoiceTotal = invoiceTotal;
//    }

    public enum Status {

        INIT, PROCESSING, PAUSED, SUCCESS, CANCELLED, FAILED
    }
    private Status status;
    private TransportMethod transportMethod;
    private String outputFileFormat;
    private String transportAddress;
    private String transportUsername;
    private String transportPassword;
    private String xsltFileName;
    private Statement statement;
    private Set<Invoice> invoices = new LinkedHashSet<Invoice>();
    private String transportEmailTemplate;

    private String scheduleOutputFormat = "";

    //SFTP Changes
    private int transportPort;

    public String getTransportKeyfile() {
        return transportKeyfile;
    }

    public void setTransportKeyfile(String transportKeyfile) {
        this.transportKeyfile = transportKeyfile;
    }

    public int getTransportPort() {
        return transportPort;
    }

    public void setTransportPort(int transportPort) {
        this.transportPort = transportPort;
    }
    private String transportKeyfile;


    public String getTransportEmailTemplate() {
        return transportEmailTemplate;
    }

    public void setTransportEmailTemplate(String transportEmailTemplate) {
        this.transportEmailTemplate = transportEmailTemplate;
    }

    public void setCustomerKey(String custKey) {
        this.customerKey = custKey;
    }

    public String getCustomerKey() {
        return this.customerKey;
    }

    public void setXsltFileName(String xsltFileName) {
        this.xsltFileName = xsltFileName;
    }

    public String getXsltFileName() {
        return xsltFileName;
    }

    public String getScheduleOutputFormat() {
        return scheduleOutputFormat;
    }

    public void setScheduleOutputFormat(String scheduleOutputFormat) {
        this.scheduleOutputFormat = scheduleOutputFormat;
    }

     /**
     * @return Email address or FTP address
     */
    public String getTransportAddress() {
        return transportAddress;
    }

    public void setTransportAddress(String transportAddress) {
        this.transportAddress = transportAddress;
    }

    public String getTransportUsername() {
        return transportUsername;
    }

    public void setTransportUsername(String transportUsername) {
        this.transportUsername = transportUsername;
    }

    public String getTransportPassword() {
        return transportPassword;
    }

    public void setTransportPassword(String transportPassword) {
        this.transportPassword = transportPassword;
    }

    public String getOutputFileFormat() {
        return outputFileFormat;
    }

    public void setOutputFileFormat(String outputFormat) {
        this.outputFileFormat = outputFormat;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Date getErrorSentDateTime() {
        return errorSentDateTime;
    }

    public void setErrorSentDateTime(Date errorSentDateTime) {
        this.errorSentDateTime = errorSentDateTime;
    }
    private String errorMessage;
    private Date errorSentDateTime;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public TransportMethod getTransportMethod() {
        return transportMethod;
    }

    public void setTransportMethod(TransportMethod transportMethod) {
        this.transportMethod = transportMethod;
    }

    public Long getBatchGroupId() {
        return batchGroupId;
    }

    public void setBatchGroupId(Long batchGroupId) {
        this.batchGroupId = batchGroupId;
    }

    public long getCustomerBatchId() {
        return customerBatchId;
    }

    public void setCustomerBatchId(long customerBatchId) {
        this.customerBatchId = customerBatchId;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCreditControllerName() {
        return creditControllerName;
    }

    public void setCreditControllerName(String creditControllerName) {
        this.creditControllerName = creditControllerName;
    }

    public String getCreditControllerEmail() {
        return creditControllerEmail;
    }

    public void setCreditControllerEmail(String creditControllerEmail) {
        this.creditControllerEmail = creditControllerEmail;
    }

    public String getCreditControllerPhone() {
        return creditControllerPhone;
    }

    public void setCreditControllerPhone(String creditControllerPhone) {
        this.creditControllerPhone = creditControllerPhone;
    }

    public Statement getStatement() {
        return statement;
    }

    /**
     * Makes the given Statement the statement of the customer batch.
     *
     * Also set other details such as brand, xslt file, output file, ccName,
     * ccEmail and ccPhone.
     */
    public void setStatement(Statement statement) {
        this.statement = setStatementInfo(statement);
        this.creditControllerName = statement.getCreditControllerName();
        this.creditControllerEmail = statement.getCreditControllerEmail();
        this.creditControllerPhone = statement.getCreditControllerPhone();
        
         this.creditControllerTitle = statement.getCreditControllerTitle();
        this.creditControllerOffice = statement.getCreditControllerOffice();
        this.creditControllerFaxNo = statement.getCreditControllerFaxNo();
    }

    public Set<Invoice> getInvoices() {
        return invoices;
    }

    /**
     * Set invoices to the list, update invoice details and customer batch
     * details such as creditor name and email.
     *
     * @param invoices
     */
    public void setInvoices(Set<Invoice> invoices) {
        this.invoices = invoices;

        for (Invoice invoice : invoices) {
            setStatementInfo(invoice);
            if (this.creditControllerName == null) {
                this.creditControllerName = invoice.getCreditControllerName();
            }
            if (this.creditControllerEmail == null) {
                this.creditControllerEmail = invoice.getCreditControllerEmail();
            }
            if (this.creditControllerPhone == null) {
                this.creditControllerPhone = invoice.getCreditControllerPhone();
            }
            
            if (this.creditControllerTitle == null){
                this.creditControllerTitle = invoice.getCreditControllerTitle();
            }
            
            if (this.creditControllerOffice == null){
                this.creditControllerOffice = invoice.getCreditControllerOffice();
            }
            
            if (this.creditControllerFaxNo == null){
                this.creditControllerFaxNo = invoice.getCreditControllerFaxNo();
            }
            
            this.invoiceTotal = invoice.getInvoiceTotal();
            invoice.setScheduleOutputFormat(this.scheduleOutputFormat);
        }
    }

    public void addInvoice(Invoice inv) {
        invoices.add(inv);
    }

    private Statement setStatementInfo(Statement statement) {
        statement.setOutputFormat(this.outputFileFormat);
        statement.setBrand(this.brand);
        statement.setXsltFileName(this.xsltFileName);
        statement.setCustBatch(this);
        return statement;
    }

    /**
     * Returns a combination of output file format + transport method + brand
     * name
     */
    public String getGroupKey() {
        return outputFileFormat.concat(transportMethod.name()).concat(brand).toUpperCase();
    }

    /**
     * @return true if the customer batch does not have any statement or
     * invoices
     */
    public boolean isEmpty() {
        return (statement == null && invoices.isEmpty());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CustomerBatch) {
            return this.customerBatchId == ((CustomerBatch) obj).customerBatchId;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return ((int) customerBatchId) * 977;
    }

    /**
     * Constructs a
     * <code>String</code> with all attributes in name = value format.
     *
     * @return a
     * <code>String</code> representation of this object.
     */
    public String toString() {
        final String TAB = "\n";

        StringBuilder retValue = new StringBuilder();

        retValue.append("CustomerBatch ( ").append(super.toString()).append(TAB).append("batchId = ").append(this.batchGroupId).append(TAB).append("customerBatchId = ").append(this.customerBatchId).append(TAB).append("brand = ").append(this.brand).append(TAB).append("customerKey = ").append(this.customerKey).append(TAB).append("creditControllerName = ").append(this.creditControllerName).append(TAB).append("creditControllerEmail = ").append(this.creditControllerEmail).append(TAB).append("creditControllerPhone = ").append(this.creditControllerPhone).append(TAB).append("status = ").append(this.status).append(TAB).append("transportMethod = ").append(this.transportMethod).append(TAB).append("outputFormat = ").append(this.outputFileFormat).append(TAB).append("transportAddress = ").append(this.transportAddress).append(TAB).append("transportUsername = ").append(this.transportUsername).append(TAB).append("transportPassword = ").append(this.transportPassword).append(TAB).append("xsltFileName = ").append(this.xsltFileName).append(TAB).append("statement = ").append(this.statement).append(TAB) //.append("invoices = ").append(this.invoices).append(TAB)
                .append("errorMessage = ").append(this.errorMessage).append(TAB).append("errorSentDateTime = ").append(this.errorSentDateTime).append(TAB).append(" )");

        return retValue.toString();
    }
}
