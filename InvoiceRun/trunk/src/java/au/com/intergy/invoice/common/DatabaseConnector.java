package au.com.intergy.invoice.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * An utility class that manage database connections.
 * 
 * @author Chi B.
 *
 */
public class DatabaseConnector {

	//private static final ConnectionPool pool = ConnectionPool.getInstance();
	public static Connection getConnection() throws SQLException
	{
		//return pool.getConnection();
		Connection conDB = null;

		try
		{
			Class.forName(ConnectionPool.DRIVER).newInstance();
			conDB  = DriverManager.getConnection(ConnectionPool.URL+
					"user="+ ConnectionPool.USER+ "&password=" + ConnectionPool.PASSWORD);
		} catch (Exception e) {
			throw new SQLException("Error opening database connection" +
					e.toString());
		}

		return conDB;
	}
	
	public static void returnConnection(Connection conn) throws SQLException
	{
		if (conn == null)
			return;
		try {
			conn.close();
		} catch (SQLException sqle) { }
			
		//conn.setAutoCommit(true);
		//pool.returnConnection(conn);
	}
        
        public static void closeDBResources(java.sql.Statement stmt, ResultSet rs) 
	{
		
			
		try {
                    if (stmt != null)
			stmt.close();
		} catch (SQLException sqle) { }
                
		try {
                    if (rs != null)
			rs.close();
		} catch (SQLException sqle) { }
		
	}
}
