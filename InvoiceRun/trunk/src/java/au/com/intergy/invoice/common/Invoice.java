package au.com.intergy.invoice.common;

public class Invoice extends Statement {

    private int sequenceNumber;
    private String invoiceNumber;
    private String scheduleOutputFormat = "";
    private String scheduleFileName = "";

    public String getScheduleFileName() {
        return scheduleFileName;
    }

    public void setScheduleFileName(String scheduleFileName) {
        this.scheduleFileName = scheduleFileName;
    }

    public String getScheduleOutputFormat() {
        return scheduleOutputFormat;
    }

    public void setScheduleOutputFormat(String scheduleOutputFormat) {
        this.scheduleOutputFormat = scheduleOutputFormat;
    }
        
        private String invoiceTotal = "0.0";

        public String getInvoiceTotal() {
            return invoiceTotal;
        }

        public void setInvoiceTotal(String invoiceTotal) {
            this.invoiceTotal = invoiceTotal;
        }

   
	public Invoice() {
		super();
	}
	
	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Invoice) {
			Invoice inv = (Invoice) obj;
			return (this.id == inv.id  && this.sequenceNumber == inv.sequenceNumber);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return ((int)id) * 577 + sequenceNumber*77;
	}
	
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation 
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "],";
	
	    StringBuilder retValue = new StringBuilder();
	    
	    retValue.append("Invoice ( ")
	        .append(super.toString()).append(TAB)
	        .append("[sequenceNumber = ").append(this.sequenceNumber).append(TAB)
	        .append(" )");
	    
	    return retValue.toString();
	}	
}
