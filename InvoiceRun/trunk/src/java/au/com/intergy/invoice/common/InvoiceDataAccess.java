package au.com.intergy.invoice.common;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class InvoiceDataAccess {
    private static Logger logger = new Logger();
	
	public static void createStatement(Connection conn, Statement stmt) 
	throws DBAccessException{

		if (stmt instanceof Invoice) {
			createInvoice(conn, (Invoice) stmt);
		} else {

			//else, we save a statement
			try {
				String query = "INSERT INTO invoicedetails "+
				"(BatchID_CustKey, Brand_Group, XMLBody, BodySize, CustomerNumber, XSLTName, "+
				" OutputFormat, TransportMethod, AcceptedDateTime, Ready, SequenceID, DetailsType, Status, Sent"
                                        + ", AccountName, AccountAdd1, AccountAdd2, AccountAdd3, AccountAdd4, AccountAdd5, AccountAdd6, AccountAdd7)"+
				"  VALUES(?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?, ?, ?, ?, ?, ?, ?, ?)";

				PreparedStatement prStmt = conn.prepareStatement(query);

				prStmt.setLong(1, stmt.getCustBatchId());
				prStmt.setString(2, stmt.getBrand());
				prStmt.setBytes(3, StringUtils.compressToBytes("stmt", stmt.getXmlBody()) );
				prStmt.setLong(4, stmt.getXmlBody().length());
				prStmt.setString(5, stmt.getAccountNumber());
				prStmt.setString(6, stmt.getXsltFileName());
				prStmt.setString(7, stmt.getOutputFormat());
				prStmt.setString(8, stmt.getCustBatch().getTransportMethod().name());
				prStmt.setTimestamp(9, new java.sql.Timestamp(System.currentTimeMillis()));
				prStmt.setString(10, stmt.getReadyStatus());
				prStmt.setString(11, String.valueOf(stmt.getSequenceNumber()) );
				prStmt.setString(12, "S");            //'S' stands for Statement
				prStmt.setString(13, stmt.getStatus());
				prStmt.setString(14, stmt.getSendStatus());
                                
                                prStmt.setString(15, stmt.getAccountName());
                                prStmt.setString(16, stmt.getAccountAddr1());
                                prStmt.setString(17, stmt.getAccountAddr2());
                                prStmt.setString(18, stmt.getAccountAddr3());
                                prStmt.setString(19, stmt.getAccountAddr4());
                                prStmt.setString(20, stmt.getAccountAddr5());
                                prStmt.setString(21, stmt.getAccountAddr6());
                                prStmt.setString(22, stmt.getAccountAddr7());

                                int result = prStmt.executeUpdate();

				if (result == 0)
					throw new DBAccessException("Unable to save a Statement to DB: " + stmt.toString());
				prStmt.close();
			} catch (Exception e) {
				e.printStackTrace();
				throw new DBAccessException(e.getMessage());
			} 
		}
	}
	
	public static void createInvoice(Connection conn, Invoice inv) 
	throws DBAccessException {
		try {
			String query = "INSERT INTO invoicedetails "+
			"(BatchID_CustKey, Brand_Group, XMLBody, BodySize, CustomerNumber, InvoiceNumber, XSLTName, "+
			" OutputFormat, TransportMethod, AcceptedDateTime, Ready, SequenceID, DetailsType, Status, Sent, invTotal"
                                 + ", AccountName, AccountAdd1, AccountAdd2, AccountAdd3, AccountAdd4, AccountAdd5, AccountAdd6, AccountAdd7, ScheduleOutputFormat)"+
			"  VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			PreparedStatement prStmt = conn.prepareStatement(query);

			prStmt.setLong(1, inv.getCustBatchId());
			prStmt.setString(2, inv.getBrand());
			prStmt.setBytes(3,StringUtils.compressToBytes(inv.getInvoiceNumber(), inv.getXmlBody())  );
			prStmt.setLong(4,  inv.getXmlBody().length());
			prStmt.setString(5, inv.getAccountNumber());
			prStmt.setString(6, inv.getInvoiceNumber());
			prStmt.setString(7, inv.getXsltFileName());
			prStmt.setString(8, inv.getOutputFormat());
			prStmt.setString(9, inv.getCustBatch().getTransportMethod().name());
			prStmt.setTimestamp(10, new java.sql.Timestamp(System.currentTimeMillis()));
			prStmt.setString(11, inv.getReadyStatus());
			prStmt.setString(12, String.valueOf(inv.getSequenceNumber()) );
			prStmt.setString(13, "I");
			prStmt.setString(14, inv.getStatus());
			prStmt.setString(15, inv.getSendStatus());
                        
                        prStmt.setString(16, inv.getInvoiceTotal());
			
                        prStmt.setString(17, inv.getAccountName());
                        prStmt.setString(18, inv.getAccountAddr1());
                        prStmt.setString(19, inv.getAccountAddr2());
                        prStmt.setString(20, inv.getAccountAddr3());
                        prStmt.setString(21, inv.getAccountAddr4());
                        prStmt.setString(22, inv.getAccountAddr5());
                        prStmt.setString(23, inv.getAccountAddr6());
                        prStmt.setString(24, inv.getAccountAddr7());

                        prStmt.setString(25, inv.getScheduleOutputFormat());
                                
			int result = prStmt.executeUpdate();
			
			if (result == 0)
				throw new DBAccessException("Unable to save an Invoice to DB: " + inv.toString());
			
			prStmt.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} 
	}
	
	public static List<String> getAttachmentFileNames(Long custBatchId) 
	throws DBAccessException {
		List<String> resultFilenames = new ArrayList<String>();

		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ResultSet rstFilenames  = null;
			conn = DatabaseConnector.getConnection();
			String sqlEmail = "SELECT FileName FROM invoicedetails WHERE BatchID_CustKey=?";
			stmt = conn.prepareStatement(sqlEmail);

			stmt.setLong(1, custBatchId);
			
			rstFilenames = stmt.executeQuery();

			while(rstFilenames.next()) {
				resultFilenames.add(rstFilenames.getString("FileName"));
			}
                        logger.log("resultFilenames.size"+ resultFilenames.size());
                        //Check Schedule Files
                        sqlEmail = "SELECT ScheduleFileName FROM invoicedetails WHERE BatchID_CustKey=? AND ScheduleFileName is NOT NULL";
			stmt = conn.prepareStatement(sqlEmail);

			stmt.setLong(1, custBatchId);

			rstFilenames = stmt.executeQuery();

			while(rstFilenames.next()) {
				resultFilenames.add(rstFilenames.getString("ScheduleFileName"));
			}
                        logger.log("resultFilenames.size after"+ resultFilenames.size());
		} catch (SQLException sqle) {
			throw new DBAccessException(sqle.getMessage());
		} finally {
			try { 
				if (stmt != null)
					stmt.close();
				DatabaseConnector.returnConnection(conn); 
			} catch (SQLException e) { }	
		}
		
		return resultFilenames;
	}
	
        public static double getInvoicTotalById(Long id) 
	throws DBAccessException {

		Double bodyResult = 0.00d;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ResultSet rsXmlBody  = null;
			conn = DatabaseConnector.getConnection();
			String sqlEmail = "SELECT sum(invTotal) as invTotal FROM invoicedetails where BatchID_CustKey=? and DetailsType = 'I' ";
			stmt = conn.prepareStatement(sqlEmail);

			stmt.setLong(1, id);
			
			rsXmlBody = stmt.executeQuery();

			if (rsXmlBody.next()) {
				bodyResult = rsXmlBody.getDouble("invTotal");
			}

		} catch (SQLException sqle) {
			throw new DBAccessException(sqle.getMessage());
		} finally {
			try { 
				if (stmt != null)
					stmt.close();
				DatabaseConnector.returnConnection(conn); 
			} catch (SQLException e) { }	
		}
		
		return bodyResult;
	}
        
        public static Invoice getInvoicAccountDetailsById(Long id) 
	throws DBAccessException {

//		Double bodyResult = 0.0d;
		PreparedStatement stmt = null;
		Connection conn = null;

                Invoice invoiceObj = new Invoice();
                invoiceObj.setId(id);
                
		try {
			ResultSet rsXmlBody  = null;
			conn = DatabaseConnector.getConnection();
			String sqlEmail = "Select distinct AccountName, AccountAdd1, AccountAdd2, AccountAdd3, AccountAdd4, AccountAdd5, AccountAdd6, AccountAdd7 FROM invoicedetails where BatchID_CustKey=? and DetailsType = 'I' ";
			stmt = conn.prepareStatement(sqlEmail);

			stmt.setLong(1, id);
			
			rsXmlBody = stmt.executeQuery();

			if (rsXmlBody.next()) {
//				bodyResult = rsXmlBody.getDouble("invTotal");
                            invoiceObj.setAccountName(rsXmlBody.getString("AccountName"));
                            
                            invoiceObj.setAccountAddr1(rsXmlBody.getString("AccountAdd1"));
                            invoiceObj.setAccountAddr2(rsXmlBody.getString("AccountAdd2"));
                            invoiceObj.setAccountAddr3(rsXmlBody.getString("AccountAdd3"));
                            invoiceObj.setAccountAddr4(rsXmlBody.getString("AccountAdd4"));
                            invoiceObj.setAccountAddr5(rsXmlBody.getString("AccountAdd5"));
                            invoiceObj.setAccountAddr6(rsXmlBody.getString("AccountAdd6"));
                            invoiceObj.setAccountAddr7(rsXmlBody.getString("AccountAdd7"));
			}

		} catch (SQLException sqle) {
			throw new DBAccessException(sqle.getMessage());
		} finally {
			try { 
				if (stmt != null)
					stmt.close();
				DatabaseConnector.returnConnection(conn); 
			} catch (SQLException e) { }	
		}
		
		return invoiceObj;
	}
        
	public static String getXMLBodyById(Long id) 
	throws DBAccessException, IOException {
                
		String bodyResult = null;
		PreparedStatement stmt = null;
		Connection conn = null;

		try {
			ResultSet rsXmlBody  = null;
			conn = DatabaseConnector.getConnection();
			String sqlEmail = "SELECT XMLBody FROM invoicedetails WHERE ID=?";
			stmt = conn.prepareStatement(sqlEmail);

			stmt.setLong(1, id);
			
			rsXmlBody = stmt.executeQuery();

			if (rsXmlBody.next()) {
				bodyResult = StringUtils.decompress(rsXmlBody.getBytes("XMLBody"));
                               
			}

		} catch (SQLException sqle) {
			throw new DBAccessException(sqle.getMessage());
		} finally {
			try { 
				if (stmt != null)
					stmt.close();
				DatabaseConnector.returnConnection(conn); 
			} catch (SQLException e) { }	
		}
		
		return bodyResult;
	}
	
	/**
	 * @return invoices with no generated files. If the no of PROCESSING invoices < limit,
	 * the remaining invoices will be INIT.
	 */
	public static Set<Statement> getInvoicesWithNoGeneratedFiles(int limit) 
	throws DBAccessException, IOException {
		PreparedStatement stmt = null;
		Connection conn = null;
		Set<Statement> invoicesResult = getStartedInvoicesWithNoGeneratedFiles(limit);

		//if no of PROCESSING invoices < limit, get some INIT ones
		if (invoicesResult.size() < limit) {
			limit = limit - invoicesResult.size();
		} else {
			return invoicesResult;
		}
		
		try {
			ResultSet rsInvoices  = null;
			conn = DatabaseConnector.getConnection();
			String sqlEmail = "SELECT * FROM invoicedetails " +
					"WHERE FileGeneratedDateTime is NULL AND Status = 'INIT' " +
					"ORDER BY BatchID_CustKey ASC LIMIT ?";
			stmt = conn.prepareStatement(sqlEmail);

			stmt.setInt(1, limit);
			
			rsInvoices = stmt.executeQuery();

			while (rsInvoices.next()) {
				String detailType = rsInvoices.getString("DetailsType");

				if ("I".equalsIgnoreCase(detailType)) {
					Invoice inv = new Invoice();
					inv.setSequenceNumber(rsInvoices.getInt("SequenceID"));
					
					inv.setId(rsInvoices.getLong("ID"));
					inv.setBrand(rsInvoices.getString("Brand_Group"));
					inv.setXmlBody(StringUtils.decompress(rsInvoices.getBytes("XMLBody")));
					inv.setAccountNumber(rsInvoices.getString("CustomerNumber"));
                                        
                                        inv.setAccountName(rsInvoices.getString("AccountName"));
                                        inv.setAccountAddr1(rsInvoices.getString("AccountAdd1"));
                                        inv.setAccountAddr2(rsInvoices.getString("AccountAdd2"));
                                        inv.setAccountAddr3(rsInvoices.getString("AccountAdd3"));
                                        inv.setAccountAddr4(rsInvoices.getString("AccountAdd4"));
                                        inv.setAccountAddr5(rsInvoices.getString("AccountAdd5"));
                                        inv.setAccountAddr6(rsInvoices.getString("AccountAdd6"));
                                        inv.setAccountAddr7(rsInvoices.getString("AccountAdd7"));
                                        
					inv.setInvoiceNumber(rsInvoices.getString("InvoiceNumber"));
					inv.setXsltFileName(rsInvoices.getString("XSLTName"));
					inv.setGeneratedFileName(rsInvoices.getString("FileName"));
					inv.setOutputFormat(rsInvoices.getString("OutputFormat"));
					inv.setCustBatchId(rsInvoices.getLong("BatchID_CustKey"));

                                        inv.setScheduleOutputFormat(rsInvoices.getString("ScheduleOutputFormat"));
                                        inv.setScheduleFileName(rsInvoices.getString("ScheduleFileName"));
                                        
					invoicesResult.add(inv);
				} else {
					//this is a statement
					Statement statement = new Statement();
					
					statement.setId(rsInvoices.getLong("ID"));
					statement.setBrand(rsInvoices.getString("Brand_Group"));
					statement.setXmlBody(StringUtils.decompress(rsInvoices.getBytes("XMLBody")) );
					statement.setAccountNumber(rsInvoices.getString("CustomerNumber"));
                                        
                                        statement.setAccountName(rsInvoices.getString("AccountName"));
                                        statement.setAccountAddr1(rsInvoices.getString("AccountAdd1"));
                                        statement.setAccountAddr2(rsInvoices.getString("AccountAdd2"));
                                        statement.setAccountAddr3(rsInvoices.getString("AccountAdd3"));
                                        statement.setAccountAddr4(rsInvoices.getString("AccountAdd4"));
                                        statement.setAccountAddr5(rsInvoices.getString("AccountAdd5"));
                                        statement.setAccountAddr6(rsInvoices.getString("AccountAdd6"));
                                        statement.setAccountAddr7(rsInvoices.getString("AccountAdd7"));
                                        
					statement.setXsltFileName(rsInvoices.getString("XSLTName"));
					statement.setGeneratedFileName(rsInvoices.getString("FileName"));
					statement.setOutputFormat(rsInvoices.getString("OutputFormat"));
					statement.setCustBatchId(rsInvoices.getLong("BatchID_CustKey"));
					invoicesResult.add(statement);
				}
			}
		} catch (SQLException sqle) {
			throw new DBAccessException(sqle.getMessage());
		} finally {
			if (stmt != null)
				try {stmt.close();} catch (SQLException se) {}
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }	
		}
		return invoicesResult;
	}
	
	/**
	 * @return PROCESSING invoices with no files generated.
	 */
	private static Set<Statement> getStartedInvoicesWithNoGeneratedFiles(int limit) 
	throws DBAccessException, IOException {
		PreparedStatement stmt = null;
		Connection conn = null;
		Set<Statement> invoicesResult = new LinkedHashSet<Statement>();

		try {
			ResultSet rsInvoices  = null;
			conn = DatabaseConnector.getConnection();
			String sqlEmail = "SELECT * FROM invoicedetails " +
					" WHERE FileGeneratedDateTime is NULL AND Status = 'PROCESSING' " +
					" ORDER BY BatchID_CustKey ASC LIMIT ?";
			stmt = conn.prepareStatement(sqlEmail);

			stmt.setInt(1, limit);
			
			rsInvoices = stmt.executeQuery();

			while (rsInvoices.next()) {
				String detailType = rsInvoices.getString("DetailsType");

				if ("I".equalsIgnoreCase(detailType)) {
					Invoice inv = new Invoice();
					inv.setSequenceNumber(rsInvoices.getInt("SequenceID"));
					
					inv.setId(rsInvoices.getLong("ID"));
					inv.setBrand(rsInvoices.getString("Brand_Group"));
					inv.setXmlBody(StringUtils.decompress(rsInvoices.getBytes("XMLBody")));
					inv.setAccountNumber(rsInvoices.getString("CustomerNumber"));
                                        
                                        inv.setAccountName(rsInvoices.getString("AccountName"));
                                        inv.setAccountAddr1(rsInvoices.getString("AccountAdd1"));
                                        inv.setAccountAddr2(rsInvoices.getString("AccountAdd2"));
                                        inv.setAccountAddr3(rsInvoices.getString("AccountAdd3"));
                                        inv.setAccountAddr4(rsInvoices.getString("AccountAdd4"));
                                        inv.setAccountAddr5(rsInvoices.getString("AccountAdd5"));
                                        inv.setAccountAddr6(rsInvoices.getString("AccountAdd6"));
                                        inv.setAccountAddr7(rsInvoices.getString("AccountAdd7"));
                                        
					inv.setInvoiceNumber(rsInvoices.getString("InvoiceNumber"));
					inv.setXsltFileName(rsInvoices.getString("XSLTName"));
					inv.setGeneratedFileName(rsInvoices.getString("FileName"));
					inv.setOutputFormat(rsInvoices.getString("OutputFormat"));
					inv.setCustBatchId(rsInvoices.getLong("BatchID_CustKey"));
					invoicesResult.add(inv);
				} else {
					//this is a statement
					Statement statement = new Statement();
					
					statement.setId(rsInvoices.getLong("ID"));
					statement.setBrand(rsInvoices.getString("Brand_Group"));
					statement.setXmlBody(StringUtils.decompress (rsInvoices.getBytes("XMLBody")));
					statement.setAccountNumber(rsInvoices.getString("CustomerNumber"));
                                        
                                        statement.setAccountName(rsInvoices.getString("AccountName"));
                                        statement.setAccountAddr1(rsInvoices.getString("AccountAdd1"));
                                        statement.setAccountAddr2(rsInvoices.getString("AccountAdd2"));
                                        statement.setAccountAddr3(rsInvoices.getString("AccountAdd3"));
                                        statement.setAccountAddr4(rsInvoices.getString("AccountAdd4"));
                                        statement.setAccountAddr5(rsInvoices.getString("AccountAdd5"));
                                        statement.setAccountAddr6(rsInvoices.getString("AccountAdd6"));
                                        statement.setAccountAddr7(rsInvoices.getString("AccountAdd7"));
                                        
					statement.setXsltFileName(rsInvoices.getString("XSLTName"));
					statement.setGeneratedFileName(rsInvoices.getString("FileName"));
					statement.setOutputFormat(rsInvoices.getString("OutputFormat"));
					statement.setCustBatchId(rsInvoices.getLong("BatchID_CustKey"));
					invoicesResult.add(statement);
				}
			}
		} catch (SQLException sqle) {
			throw new DBAccessException(sqle.getMessage());
		} finally {
			if (stmt != null)
				try {stmt.close();} catch (SQLException se) {}
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }	
		}
		return invoicesResult;
	}
	/**
	 * Update the file path/name and the generated time.
	 * @param generatedFilePath absolute path and name of the generated file.
	 */
	public static void updateInvoiceFileGeneratedDetails(long id, String generatedFilePath) 
	throws DBAccessException {
		Connection conn = null;
		PreparedStatement prStmt = null;
		
		try {
			String query = "UPDATE invoicedetails SET FileGeneratedDateTime = ? , FileName = ? WHERE ID = ?";

			conn = DatabaseConnector.getConnection();
			prStmt = conn.prepareStatement(query);

			prStmt.setTimestamp(1, new java.sql.Timestamp(System.currentTimeMillis()));
			prStmt.setString(2, generatedFilePath);
			prStmt.setLong(3, id);
			
			prStmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			if (prStmt != null) {
				try {prStmt.close();} catch (SQLException se) {}
			}
			try {DatabaseConnector.returnConnection(conn);} catch (SQLException sqle) {}
		}
	}

        /**
	 * Update the file path/name and the generated time.
	 * @param generatedFilePath absolute path and name of the generated file.
	 */
	public static void updateScheduleFileGeneratedDetails(long id, String generatedFilePath)
	throws DBAccessException {
		Connection conn = null;
		PreparedStatement prStmt = null;

		try {
			String query = "UPDATE invoicedetails SET ScheduleFileGeneratedDateTime = ? , ScheduleFileName = ? WHERE ID = ?";

			conn = DatabaseConnector.getConnection();
			prStmt = conn.prepareStatement(query);

			prStmt.setTimestamp(1, new java.sql.Timestamp(System.currentTimeMillis()));
			prStmt.setString(2, generatedFilePath);
			prStmt.setLong(3, id);

			prStmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			if (prStmt != null) {
				try {prStmt.close();} catch (SQLException se) {}
			}
			try {DatabaseConnector.returnConnection(conn);} catch (SQLException sqle) {}
		}
	}
        
	public static void updateDeliveredDateTime(long id, long deliveredTime) 
	throws DBAccessException {
		Connection conn = null;
		PreparedStatement prStmt = null;
		
		try {
			String query = "UPDATE invoicedetails SET Print_Email_FileDateTime = ? WHERE ID = ?";

			conn = DatabaseConnector.getConnection();
			prStmt = conn.prepareStatement(query);

			prStmt.setTimestamp(1, new java.sql.Timestamp(deliveredTime));
			prStmt.setLong(2, id);
			
			prStmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			if (prStmt != null) {
				try {prStmt.close();} catch (SQLException se) {}
			}
			try {DatabaseConnector.returnConnection(conn);} catch (SQLException sqle) {}
		}
	}
	
	public static void updateStatus(long id, String newStatus) 
	throws DBAccessException {
		Connection conn = null;
		PreparedStatement prStmt = null;
		
		try {
			String query = "UPDATE invoicedetails SET Status = ? WHERE ID = ?";

			conn = DatabaseConnector.getConnection();
			prStmt = conn.prepareStatement(query);

			prStmt.setString(1, newStatus);
			prStmt.setLong(2, id);
			
			prStmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBAccessException(e.getMessage());
		} finally {
			if (prStmt != null) {
				try {prStmt.close();} catch (SQLException se) {}
			}
			try {DatabaseConnector.returnConnection(conn);} catch (SQLException sqle) {}
		}
	}
	
	public static void updateStatusByBatchGroups(Connection conn, String[] batchGroupIds, String newStatus) 
	throws DBAccessException {
		java.sql.Statement stmt = null;

		try {
			String query = "UPDATE invoicedetails SET Status = '" + newStatus + "' " + 
			    " WHERE BatchID_CustKey IN (select distinct BatchID_CustKey from customerbatchdetails cb, batchgroup bg " +
			    " WHERE  cb.BatchGroupID = bg.ID AND bg.ID IN (";

			for (int i = 0; i<batchGroupIds.length; i++) {
				if (i == 0)
					query = query.concat(batchGroupIds[i]);
				else {
					query = query.concat(", ").concat(batchGroupIds[i]);
				}
			}
			
			query = query.concat("))");
			stmt = conn.createStatement();			
			stmt.executeUpdate(query);
		} catch (Exception e) {
			throw new DBAccessException(e.getMessage());
		} finally {
			if (stmt != null) {
				try {stmt.close();} catch (SQLException se) {}
			}
		}
	}
	
	/**
	 * Invoice returned only contained info required for printing - such as sequence id, printer name 
	 * and file name
	 * The order of invoices are in sequence id - ascending order
	 */
	public static Set<Statement> findByCustBatchIDForPrinting(Connection conn, long custBatchID) 
	throws DBAccessException {
		PreparedStatement stmt = null;
		Set<Statement> invoicesResult = new LinkedHashSet<Statement>();

		try {
			ResultSet rsInvoices  = null;
			//conn = DatabaseConnector.getConnection();
			String sqlEmail = "SELECT ID, FileName, SequenceID, InvoiceNumber, BatchID_CustKey, DetailsType " +
					" FROM invoicedetails  WHERE BatchID_CustKey = ? ORDER BY SequenceID ASC";

			stmt = conn.prepareStatement(sqlEmail);
			stmt.setLong(1, custBatchID);
			
			rsInvoices = stmt.executeQuery();

			while (rsInvoices.next()) {
				String detailType = rsInvoices.getString("DetailsType");

				if ("I".equalsIgnoreCase(detailType)) {
					Invoice inv = new Invoice();
					inv.setSequenceNumber(rsInvoices.getInt("SequenceID"));
					inv.setId(rsInvoices.getLong("ID"));
					inv.setInvoiceNumber(rsInvoices.getString("InvoiceNumber"));
					inv.setGeneratedFileName(rsInvoices.getString("FileName"));
					inv.setCustBatchId(rsInvoices.getLong("BatchID_CustKey"));
					invoicesResult.add(inv);
				} else {
					Statement statement = new Statement();
					statement.setId(rsInvoices.getLong("ID"));
					statement.setGeneratedFileName(rsInvoices.getString("FileName"));
					statement.setCustBatchId(rsInvoices.getLong("BatchID_CustKey"));
					invoicesResult.add(statement);
				}
			}
			
		} catch (SQLException sqle) {
			throw new DBAccessException(sqle.getMessage());
		} finally {
			if (stmt != null)
				try {stmt.close();} catch (SQLException se) {}
		}
		return invoicesResult;
	}
	
	/**
	 * Note: this method returns both invoices and statements of the 
	 * given customer batch.
	 * Does not return XML body for the invoices/statements.
	 */
	public static Set<Statement> findShallowByCustBatchID(long custBatchID) 
	throws DBAccessException {
		PreparedStatement stmt = null;
		Set<Statement> invoicesResult = new LinkedHashSet<Statement>();
        Connection conn = null;
        
		try {
			ResultSet rsInvoices  = null;
			//conn = DatabaseConnector.getConnection();
			String sqlEmail = "SELECT ID, FileName, SequenceID, InvoiceNumber, BatchID_CustKey, DetailsType " +
					" FROM invoicedetails  WHERE BatchID_CustKey = ? ORDER BY SequenceID ASC";

			conn = DatabaseConnector.getConnection();
			stmt = conn.prepareStatement(sqlEmail);
			stmt.setLong(1, custBatchID);
			
			rsInvoices = stmt.executeQuery();

			while (rsInvoices.next()) {
				String detailType = rsInvoices.getString("DetailsType");

				if ("I".equalsIgnoreCase(detailType)) {
					Invoice inv = new Invoice();
					inv.setSequenceNumber(rsInvoices.getInt("SequenceID"));
					inv.setId(rsInvoices.getLong("ID"));
					inv.setInvoiceNumber(rsInvoices.getString("InvoiceNumber"));
					inv.setGeneratedFileName(rsInvoices.getString("FileName"));
					inv.setCustBatchId(rsInvoices.getLong("BatchID_CustKey"));
					invoicesResult.add(inv);
				} else {
					Statement statement = new Statement();
					statement.setId(rsInvoices.getLong("ID"));
					statement.setGeneratedFileName(rsInvoices.getString("FileName"));
					statement.setCustBatchId(rsInvoices.getLong("BatchID_CustKey"));
					invoicesResult.add(statement);
				}
			}
			
		} catch (SQLException sqle) {
			throw new DBAccessException(sqle.getMessage());
		} finally {
			try {DatabaseConnector.returnConnection(conn);} catch (SQLException se) {}
		}
		return invoicesResult;
	}
	
	/**
	 * @return {@link Statement}  
	 */
	public static Statement findById(long id) 
	throws DBAccessException {
		PreparedStatement stmt = null;
		Connection conn = null;
		Statement invStatement = null;
		
		try {
			ResultSet rsInvoices  = null;
			conn = DatabaseConnector.getConnection();
			String sqlEmail = "SELECT * FROM invoicedetails  WHERE ID = ?";
			
			stmt = conn.prepareStatement(sqlEmail);
			stmt.setLong(1, id);
			
			rsInvoices = stmt.executeQuery();

			while (rsInvoices.next()) {
				String detailType = rsInvoices.getString("DetailsType");

				if ("I".equalsIgnoreCase(detailType)) {
					Invoice inv = new Invoice();
					inv.setSequenceNumber(rsInvoices.getInt("SequenceID"));
					inv.setId(rsInvoices.getLong("ID"));
					inv.setInvoiceNumber(rsInvoices.getString("InvoiceNumber"));
					inv.setGeneratedFileName(rsInvoices.getString("FileName"));
					inv.setCustBatchId(rsInvoices.getLong("BatchID_CustKey"));
					inv.setStatus(rsInvoices.getString("Status"));
					invStatement = inv;
				} else {
					invStatement = new Statement();
					invStatement.setId(rsInvoices.getLong("ID"));
					invStatement.setGeneratedFileName(rsInvoices.getString("FileName"));
					invStatement.setCustBatchId(rsInvoices.getLong("BatchID_CustKey"));
					invStatement.setStatus(rsInvoices.getString("Status"));
				}
			}
			
		} catch (SQLException sqle) {
			throw new DBAccessException(sqle.getMessage());
		} finally {
			if (stmt != null)
				try {stmt.close();} catch (SQLException se) {}
			try {DatabaseConnector.returnConnection(conn);} catch (SQLException sqle) {}
		}
		return invStatement;
	}
	
	/** 
	 *  If all invoices of the same cust batch have been generated, 
	 *  return cust batch id. Else return null.
	 */
	public static boolean allFilesGenerated(long invoiceId) 
	throws DBAccessException{
		
		Connection conn = null;

		try {
			String query = "SELECT ID from invoicedetails WHERE BatchID_CustKey = " +
					" (select BatchID_CustKey from invoicedetails where ID = " + invoiceId +
 					" ) AND FileName is NULL";

			conn = DatabaseConnector.getConnection();
			java.sql.Statement stmt = conn.createStatement();

			ResultSet result = stmt.executeQuery(query);
			
			return !result.next();
			
		} catch (Exception e) {
			throw new DBAccessException(e.getMessage());
		} finally {
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }	
		}
	}

        /**
	 *  Get CustomerKey
	 */
	public static String getCustomerKey(long BatchID_CustKey)
	throws DBAccessException{

		Connection conn = null;

		try {
			String query = "SELECT CustomerKey from customerbatchdetails WHERE BatchID_CustKey = " +
					BatchID_CustKey;

			conn = DatabaseConnector.getConnection();
			java.sql.Statement stmt = conn.createStatement();

			ResultSet result = stmt.executeQuery(query);
			String CustomerKey = "";
			while(result.next()){
                            CustomerKey = result.getString("CustomerKey");
                        }
                        return CustomerKey;

		} catch (Exception e) {
			throw new DBAccessException(e.getMessage());
		} finally {
			try { DatabaseConnector.returnConnection(conn); } catch (SQLException e) { }
		}
	}
}
