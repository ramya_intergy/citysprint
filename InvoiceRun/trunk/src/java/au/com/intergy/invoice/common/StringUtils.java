package au.com.intergy.invoice.common;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class StringUtils {

	public static String replaceAll( String originalString, String regex, String replaceValue) {
		Pattern pattern = Pattern.compile(regex);
		
		Matcher matcher = pattern.matcher(originalString);
		if (matcher.find()) {
			originalString = matcher.replaceAll(replaceValue);
		}
		
		return originalString;
	}
	
	public static boolean isEmpty(String param) {
		return param == null || param.trim().equals("");
	}

	public static byte[] compressToBytes(String name, String uncompressedStr) throws IOException {
		if (uncompressedStr == null)
			return null;

		name = name!=null? name : "noname";
		if (name.length() > 0xFFFF)  {  //make sure name of the zip entry is less than 65535=FFFF

		}
		ByteArrayInputStream bis = null;
		ByteArrayOutputStream baos = null;
		ZipOutputStream zos = null;
		byte[] compressedStr = null;
		try {
			byte[] uncompressedBytes = uncompressedStr.getBytes();
			bis = new ByteArrayInputStream(uncompressedBytes);
			baos = new ByteArrayOutputStream();

			ZipEntry ze = new ZipEntry(name);
			zos = new ZipOutputStream(baos);
			zos.putNextEntry(ze);
			zos.write(uncompressedStr.getBytes());
			zos.closeEntry();
			zos.finish();
			zos.flush();

			compressedStr = baos.toByteArray();

		}  finally {
			if (zos != null)  { try {zos.close();} catch (IOException ioe) { ioe.printStackTrace(); } }
			if (baos != null) { try {baos.close();} catch (IOException ioe) { ioe.printStackTrace();}}
			if (bis != null) try { bis.close(); } catch (IOException ioe) { ioe.printStackTrace();}
					
		}
		return compressedStr;
	}

	public static String compressToString(String name, String uncompressedStr) throws IOException {
		if (uncompressedStr == null)
			return null;

		name = name!=null? name : "noname";
		if (name.length() > 0xFFFF)  {  //make sure name of the zip entry is less than 65535=FFFF

		}
		ByteArrayInputStream bis = null;
		ByteArrayOutputStream baos = null;
		ZipOutputStream zos = null;
		String compressedStr = null;
		try {
			byte[] uncompressedBytes = uncompressedStr.getBytes();
			bis = new ByteArrayInputStream(uncompressedBytes);
			baos = new ByteArrayOutputStream();

			ZipEntry ze = new ZipEntry(name);
			zos = new ZipOutputStream(baos);
			zos.putNextEntry(ze);
			zos.write(uncompressedStr.getBytes());
			zos.closeEntry();
			zos.finish();
			zos.flush();

			compressedStr = new String( baos.toByteArray(), "ISO-8859-1");

		}  finally {
			if (zos != null)  { try {zos.close();} catch (IOException ioe) { ioe.printStackTrace(); } }
			if (baos != null) { try {baos.close();} catch (IOException ioe) { ioe.printStackTrace();}}
			if (bis != null) try { bis.close(); } catch (IOException ioe) { ioe.printStackTrace();}
					
		}
		return compressedStr;
	}
/*
	public static String decompressToString(byte[] compressedStr, String tempFilePath) throws IOException {

		if (compressedStr == null)
			return null;
		
		
		//save to file
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(tempFilePath);
			fos.write(compressedStr);
		} finally {
			if (fos != null) try {fos.close(); } catch (IOException ioe) {}
		}

		//decompress from file
		StringBuffer sb = new StringBuffer();
		ZipInputStream zis = null;
		
		try {
			zis = new ZipInputStream(new FileInputStream(tempFilePath));
			while (zis.available() != 0) {
				ZipEntry entry = zis.getNextEntry();
				int c = -1;
				
				while ((c = zis.read()) != -1) {
					sb.append((char)c);
				}
				zis.closeEntry();
			}
		} finally {
			if (zis != null) try { zis.close(); } catch (IOException ioe) {}
		}
		
		return sb.toString();
	}
	*/
	public static String decompress(String compressedStr) throws IOException {

		if (compressedStr == null)
			return null;
		if (compressedStr.length() == 0)
			return "";
		ByteArrayInputStream bis = null;
		ByteArrayOutputStream baos = null;
		String decompressedStrRet = "";

		try {
			//"ISO-8859-1"
			bis = new ByteArrayInputStream(compressedStr.getBytes("ISO-8859-1"));
			ZipInputStream zis = new ZipInputStream(bis);
			ZipEntry entry;
			baos = new ByteArrayOutputStream();
			
			
			BufferedReader in2 = new BufferedReader( new InputStreamReader( zis));
			
			zis.getNextEntry();
			String s;
			StringBuffer sb = new StringBuffer();
			while ((s = in2.readLine()) != null) {
				sb.append(s);
			}
			decompressedStrRet = sb.toString();
			zis.close();
		
		} finally {
			try {
				if (baos != null)
					baos.close();
			} catch (IOException e) {}
			
			try {
				if (bis != null)
					bis.close();
			} catch (IOException e) {}				
		}
		
		return decompressedStrRet;
	}
	
	public static String decompress(byte[] compressedStr) throws IOException {

		if (compressedStr == null)
			return null;
		if (compressedStr.length == 0)
			return "";
		ByteArrayInputStream bis = null;
		ByteArrayOutputStream baos = null;
		String decompressedStrRet = "";

		try {
			//"ISO-8859-1"
			bis = new ByteArrayInputStream(compressedStr);
			ZipInputStream zis = new ZipInputStream(bis);
			ZipEntry entry;
			baos = new ByteArrayOutputStream();
			
			
			BufferedReader in2 = new BufferedReader( new InputStreamReader( zis));
			
			zis.getNextEntry();
			String s;
			StringBuffer sb = new StringBuffer();
			while ((s = in2.readLine()) != null) {
				sb.append(s);
			}
			decompressedStrRet = sb.toString();
			zis.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if (baos != null)
					baos.close();
			} catch (IOException e) {}
			
			try {
				if (bis != null)
					bis.close();
			} catch (IOException e) {}				
		}
		
		return decompressedStrRet;
	}
	
	/*
	public static void compress(String str, String outputFilePath) throws IOException {
		
		ZipOutputStream zos = null;
		FileInputStream fis = null;
		
		try {
			zos = new ZipOutputStream(new FileOutputStream(
					outputFilePath));
			ZipEntry oZipEntry = new ZipEntry("compressed");
			zos.putNextEntry(oZipEntry);
			zos.write(str.getBytes());
			zos.closeEntry();
			zos.finish();
			zos.flush();
			
			//write to file
			fis = new FileInputStream(outputFilePath);
			int iAvailable = fis.available();
			
			byte[] bytes = new byte[iAvailable];
			fis.read(bytes, 0, iAvailable);
		}  finally {
			if (zos != null) try { zos.close(); } catch (IOException ioe) { }
		}
	}
	*/
	/*
	public static String decompress(String compressedFilePath) throws IOException {
	
		StringBuffer sb = new StringBuffer();
		ZipInputStream zis = null;
		
		try {
			zis = new ZipInputStream(new FileInputStream(compressedFilePath));
					
			while (zis.available() != 0) {
				ZipEntry entry = zis.getNextEntry();
				int c = -1;
				
				while ((c = zis.read()) != -1) {
					sb.append((char)c);
				}
				zis.closeEntry();
			}
		} finally {
			if (zis != null) try { zis.close(); } catch (IOException ioe) {}
		}
		return sb.toString();
	}	
	*/
}
