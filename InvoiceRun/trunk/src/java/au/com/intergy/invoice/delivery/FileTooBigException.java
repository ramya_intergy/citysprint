package au.com.intergy.invoice.delivery;

/**
 * Indicates that a file is too big compared to the configured file size
 * to be attached to an email.
 * 
 * @author Chi Bui
 */
public class FileTooBigException extends Exception {
	private static final long serialVersionUID = 1441755458756L;

	public FileTooBigException(String message) {
		super(message);
	}
}
