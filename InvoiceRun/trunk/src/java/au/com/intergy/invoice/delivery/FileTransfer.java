package au.com.intergy.invoice.delivery;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;

import java.security.NoSuchAlgorithmException;

import au.com.intergy.invoice.common.BatchGroup;
import au.com.intergy.invoice.common.BatchGroupDataAccess;
import au.com.intergy.invoice.common.Configuration;
import au.com.intergy.invoice.common.CustomerBatch;
import au.com.intergy.invoice.common.CustomerBatchDataAccess;
import au.com.intergy.invoice.common.DBAccessException;
import au.com.intergy.invoice.common.ErrorNofifier;
import au.com.intergy.invoice.common.InvoiceDataAccess;
import au.com.intergy.invoice.common.InvoiceManager;
import au.com.intergy.invoice.common.Logger;
import au.com.intergy.invoice.common.CustomerBatch.Status;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import org.apache.commons.net.PrintCommandListener;

//SFTP - Adding commons.vfs2.1 as opposed to commons.vfs2.2
import org.apache.commons.vfs2.Capability;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.FileType;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;

/**
 * This class continuously checks if it needs to transfer files
 * for outstanding customer batches.
 * 
 * Files are transferred using the FTP protocol.
 * 
 * @author Chi Bui
 *
 */
public class FileTransfer {

    private static Logger logger = new Logger();
    private final static Configuration config = Configuration.getInstance();
    private final static long FILE_TRANSFER_SLEEP = config.getLong("file.transfer.sleep", new Long(2000));
    private final static List<String> FILE_TRANSFER_SEMAPHORE_CUSTKEY = config.getListOfString("file.transfer.semaphore.custkey");
    private final static String PRIVATEKEY_FILEPATH = config.getString("file.transfer.privatekey.filepath");

    public static void main(String[] args) throws InterruptedException {
        FileTransfer ftpSender = new FileTransfer();

        System.out.println("Running File Transfer ...");
        try {
            while (true) {
                ftpSender.send();//Normal FTP
                ftpSender.sendFTPS();//FTPS
                ftpSender.sendSFTP();//SFTP
                Thread.sleep(FILE_TRANSFER_SLEEP);
            }
        } catch (DBAccessException dae) {
            ErrorNofifier.addSystemError("Terminating Invoice Run's FTP " + dae.toString());
            logger.log("ERROR: FTP is quiting ..." + dae.toString());
            dae.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Transfer files for each customer batch,
     * then update its status and update batch group's status.
     *
     * @throws DBAccessException
     */
    private void send() throws DBAccessException {
        Set<CustomerBatch> custBatches = CustomerBatchDataAccess.findNotTransferredCustomerBatches(10);
        //System.out.println("Size:"+custBatches.size());
        for (CustomerBatch custBatch : custBatches) {
            try {
                transferFiles(custBatch.getTransportAddress(),
                        custBatch.getTransportUsername(), custBatch.getTransportPassword(),
                        InvoiceDataAccess.getAttachmentFileNames(custBatch.getCustomerBatchId()), custBatch.getCustomerKey());
                CustomerBatchDataAccess.updateStatus(custBatch.getCustomerBatchId(), Status.SUCCESS.name());
                System.out.println("After status update...");
                BatchGroupDataAccess.updateStatusBasedOnCustBatch(BatchGroup.Status.COMPLETED.name(),
                        Status.SUCCESS.name(), custBatch.getBatchGroupId());
                System.out.println("Transfer files successfully for customer batch "
                        + custBatch.getCustomerBatchId());
            } catch (Exception e) {
                InvoiceManager.failCustomerBatch(custBatch.getCustomerBatchId(),
                        "Unable to transfer file with error " + e.getMessage(), custBatch.getBrand(), custBatch.getCustomerKey());
                e.printStackTrace();
                logger.log("ERROR: Can't transfer files for customer batch " + custBatch.getCustomerBatchId());
                logger.log(e.toString());
            }
        }
    }

    /**
     * Transfer files via FTPS for each customer batch,
     * then update its status and update batch group's status.
     *
     * @throws DBAccessException
     */
    private void sendFTPS() throws DBAccessException {
        Set<CustomerBatch> custBatches = CustomerBatchDataAccess.findNotTransferredFTPSCustomerBatches(10);
        //System.out.println("Size:" + custBatches.size());
        for (CustomerBatch custBatch : custBatches) {
            try {
                transferFilesFTPS(custBatch.getTransportAddress(),
                        custBatch.getTransportUsername(), custBatch.getTransportPassword(),
                        InvoiceDataAccess.getAttachmentFileNames(custBatch.getCustomerBatchId()), custBatch.getCustomerKey());
                CustomerBatchDataAccess.updateStatus(custBatch.getCustomerBatchId(), Status.SUCCESS.name());
                System.out.println("After status update in sendFTPS...");
                BatchGroupDataAccess.updateStatusBasedOnCustBatch(BatchGroup.Status.COMPLETED.name(),
                        Status.SUCCESS.name(), custBatch.getBatchGroupId());
                System.out.println("Transfer files successfully for customer batch "
                        + custBatch.getCustomerBatchId());
            } catch (Exception e) {
                InvoiceManager.failCustomerBatch(custBatch.getCustomerBatchId(),
                        "Unable to transfer file via FTPS with error " + e.getMessage(), custBatch.getBrand(), custBatch.getCustomerKey());
                e.printStackTrace();
                logger.log("ERROR: Can't transfer files via FTPS for customer batch " + custBatch.getCustomerBatchId());
                logger.log(e.toString());
            }
        }
    }

    /**
     * Transfer files via FTPS for each customer batch,
     * then update its status and update batch group's status.
     *
     * @throws DBAccessException
     */
    private void sendSFTP() throws DBAccessException {
        Set<CustomerBatch> custBatches = CustomerBatchDataAccess.findNotTransferredSFTPCustomerBatches(10);
        //System.out.println("Size:" + custBatches.size());
        for (CustomerBatch custBatch : custBatches) {
            try {
                transferFilesSFTP(custBatch.getTransportAddress(),
                        custBatch.getTransportUsername(), custBatch.getTransportPassword(),
                        InvoiceDataAccess.getAttachmentFileNames(custBatch.getCustomerBatchId()), custBatch.getCustomerKey(), custBatch.getTransportPort(), custBatch.getTransportKeyfile());
                CustomerBatchDataAccess.updateStatus(custBatch.getCustomerBatchId(), Status.SUCCESS.name());
                System.out.println("After status update in sendSFTP...");
                BatchGroupDataAccess.updateStatusBasedOnCustBatch(BatchGroup.Status.COMPLETED.name(),
                        Status.SUCCESS.name(), custBatch.getBatchGroupId());
                System.out.println("Transfer files successfully for customer batch "
                        + custBatch.getCustomerBatchId());
            } catch (Exception e) {
                InvoiceManager.failCustomerBatch(custBatch.getCustomerBatchId(),
                        "Unable to transfer file via SFTP with error " + e.getMessage(), custBatch.getBrand(), custBatch.getCustomerKey());
                e.printStackTrace();
                logger.log("ERROR: Can't transfer files via SFTP for customer batch " + custBatch.getCustomerBatchId());
                logger.log(e.toString());
            }
        }
    }

    private void transferFiles(String ftpHost, String ftpUserName,
            String ftpPassword, List<String> fileNames, String customerKey) throws FileTransferException, IOException {

        FTPClient ftp = new FTPClient();

        //Connect to the server
        boolean isConnected = true;
        try {
            System.out.println("Ftp connect...");
            ftp.connect(ftpHost);
            //15/01/2009 Ramya added ftp.enterLocalPassiveMode(); to fix storeFile hanging issue
            //Ref:http://roshantitus.blogspot.com/2006_11_01_archive.html
            ftp.enterLocalPassiveMode();

        } catch (IOException ioe) {
            isConnected = false;
        }
        if (!isConnected) {
            throw new FileTransferException("Unable to connect to " + ftpHost);
        }

        // verify success
        int reply = ftp.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            try {
                ftp.disconnect();
            } catch (Exception e) {
                System.err.println("Unable to disconnect from FTP server "
                        + "after server refused connection. " + e.toString());
            }
            throw new FileTransferException("FTP server  " + ftpHost + " refused connection.");
        }
        //System.out.println("Ftp logon...");
        //Log on
        boolean isLoggedOn;
        try {
            isLoggedOn = ftp.login(ftpUserName, ftpPassword);
        } catch (IOException ioe) {
            isLoggedOn = false;
        }
        //System.out.println("Ftp logon..."+isLoggedOn);
        if (!isLoggedOn) {
            throw new FileTransferException("Unable to login to FTP server "
                    + "using username " + ftpUserName + " "
                    + "and password " + ftpPassword);
        }

        //System.out.println(ftp.getReplyString());
        //System.out.println("Remote system is " + ftp.getSystemName());

        //set file type to either ASCII or Binary
        //ftp.setFileType(FTP.ASCII_FILE_TYPE);
        try {
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
        } catch (IOException ioe) {
            throw new FileTransferException("Unable to set file type to BINARY for host " + ftpHost);
        }
        //System.out.println("FTP Set File type" + fileNames.size());

        boolean isStoredFile = false;
        //Transfer all files
        for (String fileName : fileNames) {
            File file = new File(fileName);
            boolean isTransferred = true;

            try {
                //System.out.println("FTP before storefile" + fileName);
                ftp.setDefaultTimeout(100000);

                if (FILE_TRANSFER_SEMAPHORE_CUSTKEY.contains(customerKey)) {
                    isStoredFile = ftp.storeFile(file.getName() + ".tmp", new FileInputStream(file));
                } else {
                    isStoredFile = ftp.storeFile(file.getName(), new FileInputStream(file));
                }
                //System.out.println("FTP after storefile" + isStoredFile);
            } catch (IOException ioe) {
                isTransferred = false;
            }

            //check result
            if (!isTransferred) {
                throw new FileTransferException("Unable to store file " + fileName + " in host " + ftpHost);
            } else {

                if (FILE_TRANSFER_SEMAPHORE_CUSTKEY.contains(customerKey)) {
                    logger.log("Before rename FTP:" + file.getName());
                    ftp.rename(file.getName() + ".tmp", file.getName());
                }
            }
        }

        //System.out.println("FTP after transfer");
        //Disconnect from the remote FTP host
        try {
            ftp.disconnect();
        } catch (IOException exc) {
            logger.log("FileTransfer: WARNING: Unable to disconnect from FTP server " + ftpHost + ". " + exc.toString());
        }

    }

    private void transferFilesFTPS(String ftpsHost, String ftpsUserName,
            String ftpsPassword, List<String> fileNames, String customerKey) throws FileTransferException, NoSuchAlgorithmException, IOException {

        String protocol = "TLSv1";
        FTPSClient ftps = new FTPSClient(protocol, true);
        ftps.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));

        //Connect to the server
        boolean isConnected = true;
        String sMessage = "";
        try {
            System.out.println("Ftps connect...implicit true");
            logger.log("ftpsHost:" + ftpsHost);
            logger.log("ftpsUserName:" + ftpsUserName);
            logger.log("ftpsPassword:" + ftpsPassword);
            logger.log("implicit true protocol:" + protocol);

            ftps.connect(ftpsHost, 990);
            logger.log("after connect");
            //15/01/2009 Ramya added ftp.enterLocalPassiveMode(); to fix storeFile hanging issue
            //Ref:http://roshantitus.blogspot.com/2006_11_01_archive.html
            ftps.enterLocalPassiveMode();

        } catch (IOException ioe) {
            isConnected = false;
            sMessage = ioe.getMessage();
            logger.log("FTPS Exception message: " + sMessage);
            ioe.printStackTrace();
        }
        if (!isConnected) {
            throw new FileTransferException("Unable to connect to " + ftpsHost + "Exception: " + sMessage);
        }

        // verify success
        int reply = ftps.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            try {
                ftps.disconnect();
            } catch (Exception e) {
                System.err.println("Unable to disconnect from FTPS server "
                        + "after server refused connection. " + e.toString());
            }
            throw new FileTransferException("FTPS server  " + ftpsHost + " refused connection.");
        }
        //System.out.println("Ftp logon...");
        //Log on
        boolean isLoggedOn;
        try {
            isLoggedOn = ftps.login(ftpsUserName, ftpsPassword);
        } catch (IOException ioe) {
            isLoggedOn = false;
        }
        //System.out.println("Ftp logon..."+isLoggedOn);
        if (!isLoggedOn) {
            throw new FileTransferException("Unable to login to FTPS server "
                    + "using username " + ftpsUserName + " "
                    + "and password " + ftpsPassword);
        }

        //System.out.println(ftp.getReplyString());
        //System.out.println("Remote system is " + ftp.getSystemName());

        //set file type to either ASCII or Binary
        //ftp.setFileType(FTP.ASCII_FILE_TYPE);
        try {
            ftps.setFileType(FTP.BINARY_FILE_TYPE);
        } catch (IOException ioe) {
            throw new FileTransferException("Unable to set file type to BINARY for host " + ftpsHost);
        }
        //System.out.println("FTP Set File type" + fileNames.size());

        boolean isStoredFile = false;
        //Transfer all files
        for (String fileName : fileNames) {
            File file = new File(fileName);
            boolean isTransferred = true;

            try {
                //System.out.println("FTP before storefile" + fileName);
                ftps.setDefaultTimeout(100000);
                //isStoredFile = ftps.storeFile(file.getName(), new FileInputStream(file));
                if (FILE_TRANSFER_SEMAPHORE_CUSTKEY.contains(customerKey)) {
                    isStoredFile = ftps.storeFile(file.getName() + ".tmp", new FileInputStream(file));
                } else {
                    isStoredFile = ftps.storeFile(file.getName(), new FileInputStream(file));
                }
                //System.out.println("FTP after storefile" + isStoredFile);
            } catch (IOException ioe) {
                isTransferred = false;
            }

            //check result
            if (!isTransferred) {
                throw new FileTransferException("Unable to store file " + fileName + " in host " + ftpsHost);
            } else {
                if (FILE_TRANSFER_SEMAPHORE_CUSTKEY.contains(customerKey)) {
                    logger.log("Before rename of file");
                    ftps.rename(file.getName() + ".tmp", file.getName());
                }
            }
        }

        //System.out.println("FTP after transfer");
        //Disconnect from the remote FTP host
        try {
            ftps.disconnect();
        } catch (IOException exc) {
            logger.log("FileTransfer: WARNING: Unable to disconnect from FTPS server " + ftpsHost + ". " + exc.toString());
        }

    }

    private void transferFilesSFTP(String sftpHost, String sftpUserName,
            String sftpPassword, List<String> fileNames, String customerKey, int port, String keyfile) {


        StandardFileSystemManager manager = new StandardFileSystemManager();

        try {

            boolean isStoredFile = false;
            //Transfer all files
            for (String fileName : fileNames) {
                File file = new File(fileName);
                System.out.println("Filename:" + fileName);
                boolean isTransferred = true;

                if (!file.exists()) {
                    throw new RuntimeException("Error. Local file not found");
                }

                System.out.println("keyfile:" + keyfile);
                System.out.println("port:" + port);
                System.out.println("password:" + sftpPassword);
                //keyfile=(keyfile==null)?"":keyfile;
                if ((keyfile == null) && sftpPassword.length() > 0) {
                    System.out.println("putFile");
                    putFile(sftpHost, sftpUserName, sftpPassword, file.getName(), file.getAbsolutePath(), customerKey, port);
                } else {
                    System.out.println("putFilePublicKeyAuthentication");
                    putFilePublicKeyAuthentication(sftpHost, sftpUserName, sftpPassword, file.getName(), file.getAbsolutePath(), customerKey, port, keyfile);
                }
//                //Initializes the file manager
//                manager.init();
//
//                //Setup our SFTP configuration
//                FileSystemOptions opts = new FileSystemOptions();
//                SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(
//                        opts, "no");
//                SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, true);
//                SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, 100000);
//
//                String sftpUri = "";
//                String sftpRenameUri = "";
//                //Create the SFTP URI using the host name, userid, password,  remote path and file name
//                if(FILE_TRANSFER_SEMAPHORE_CUSTKEY.contains(customerKey)){
//                    sftpRenameUri = "sftp://" + sftpUserName + ":" + sftpPassword + "@" + sftpHost + "/"
//                        +  file.getName();
//
//                    sftpUri = "sftp://" + sftpUserName + ":" + sftpPassword + "@" + sftpHost + "/"
//                        +  (file.getName() + ".tmp");
//                }
//                else
//                    sftpUri = "sftp://" + sftpUserName + ":" + sftpPassword + "@" + sftpHost + "/"
//                        +  file.getName();
//
//                logger.log("sftpUri:"+sftpUri);
//                System.out.println("sftpUri:"+sftpUri);
//                // Create local file object
//                FileObject localFile = manager.resolveFile(file.getAbsolutePath());
//                System.out.println("after localfile:"+localFile.exists());
//                // Create remote file object
//                FileObject remoteFile = manager.resolveFile(sftpUri, opts);
//                System.out.println("remotefile:"+remoteFile.exists());
//                // Copy local file to sftp server
//                remoteFile.copyFrom(localFile, Selectors.SELECT_SELF);
//                System.out.println("remotefile2:"+remoteFile.exists());
//
//                if(remoteFile.exists())
//                {
//                    System.out.println("In remoteFile exists:"+FILE_TRANSFER_SEMAPHORE_CUSTKEY.contains(customerKey));
//                    if(FILE_TRANSFER_SEMAPHORE_CUSTKEY.contains(customerKey))
//                    {
//                        System.out.println("Before rename SFTP"+sftpRenameUri);
//                        try
//                        {
//                            FileObject renameFile = manager.resolveFile(sftpRenameUri, opts);
//                            System.out.println("After rename SFTP file");
//                            if (!renameFile.exists()) {
//                                System.out.println("not rename SFTP file exists");
//                                //renameFile.createFolder();
//                                //manager.close();
//                            }
//
//                            //manager.init();
//                            remoteFile = manager.resolveFile(sftpUri, opts);
//                            System.out.println("remotefile3:"+remoteFile.exists());
//
//                            //if (renameFile.exists() && renameFile.getType() == FileType.FOLDER) {
//                                //renameFile = renameFile.resolveFile(remoteFile.getName().getBaseName());
//                            //}
//                            boolean bcanRename=remoteFile.canRenameTo(renameFile);
//                            System.out.println("renameFile bcanRename:"+bcanRename);
//                            if(bcanRename && remoteFile.exists())
//                            {
//                                System.out.println(remoteFile.getName().getBaseName());
//                                System.out.println(renameFile.getName().getBaseName());
//                                remoteFile.moveTo(renameFile);
//                                System.out.println("after move");
//                            }
//                        }
//                        catch (RuntimeException e) {
//                            System.out.println("Exception:"+e.getMessage());
//                        }
//                    }
                //System.out.println("SFTP File " + localFile + " uploaded successfully");
//                }
            }

        } catch (Exception ex) {
            ex.getMessage();

        } finally {
            manager.close();
        }
    }

    private void putFile(String host, String username, String password, String remotefile, String localfile, String customerKey, int port) {
        JSch jsch = new JSch();
        Session session = null;
        String renameFile = "";
        InputStream localFileInputStream = null;

        try {

            //Create local file object
            try {
                File localFile = new File(localfile);
                System.out.println("after localfile:" + localFile.exists());
                localFileInputStream = new FileInputStream(localFile);
            } catch (java.io.FileNotFoundException fox) {
                System.out.println("localfile not found:" + localfile);
            }


            System.out.println("port putFile:" + port);
            session = jsch.getSession(username, host, port);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(password);
            session.connect();
            System.out.println("after session connect putFile");

            Channel channel = session.openChannel("sftp");
            channel.connect();
            System.out.println("after channel connect putFile");
            ChannelSftp sftpChannel = (ChannelSftp) channel;

            if (FILE_TRANSFER_SEMAPHORE_CUSTKEY.contains(customerKey)) {
                renameFile = remotefile;
                remotefile = remotefile + ".tmp";
                System.out.println("remotefile:" + remotefile);
                System.out.println("renameFile:" + renameFile);
            }

            String currentDirectory=sftpChannel.pwd();
            System.out.println("Current Dir:"+currentDirectory);
            sftpChannel.cd(currentDirectory + "PROCESSING/");
            currentDirectory=sftpChannel.pwd();
            System.out.println("Current Dir after CD:"+currentDirectory);

            if (localFileInputStream == null) {
                System.out.println("No input file to write");
            } else {
                sftpChannel.put(localFileInputStream, renameFile);
                System.out.println("after put");
            }

                    
            try {
                sftpChannel.lstat(renameFile);
            } catch (SftpException e){
                if(e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE){
                // file doesn't exist
                     System.out.println("File doesnot exists:"+renameFile);

                } else {
                // something else went wrong
                    throw e;
                }
            }

            //if(FILE_TRANSFER_SEMAPHORE_CUSTKEY.contains(customerKey))
            //sftpChannel.rename(remotefile, renameFile);
            //System.out.println("after rename");
            sftpChannel.exit();
            session.disconnect();
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        }
    }

    private void putFilePublicKeyAuthentication(String host, String username, String password, String remotefile, String localfile, String customerKey, int port, String keyfile) {
        JSch jsch = new JSch();
        Session session = null;
        String renameFile = "";
        InputStream localFileInputStream = null;
        
        try {

            //Create local file object
            try {
                File localFile = new File(localfile);
                System.out.println("after localfile:" + localFile.exists());
                localFileInputStream = new FileInputStream(localFile);
            } catch (java.io.FileNotFoundException fox) {
                System.out.println("localfile not found:" + localfile);
            }

            jsch.addIdentity(PRIVATEKEY_FILEPATH + "\\" + keyfile);
            session = jsch.getSession(username, host, port);
            session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            System.out.println("after session connect publickey");

            Channel channel = session.openChannel("sftp");
            channel.connect();
            System.out.println("after channel connect");
            ChannelSftp sftpChannel = (ChannelSftp) channel;

            if (FILE_TRANSFER_SEMAPHORE_CUSTKEY.contains(customerKey)) {
                renameFile = remotefile;
                remotefile = remotefile + ".tmp";

            }
            sftpChannel.put(localFileInputStream, remotefile);
            //System.out.println("after put"+sftpChannel.readlink(remotefile));

            if (FILE_TRANSFER_SEMAPHORE_CUSTKEY.contains(customerKey)) {
                sftpChannel.rename(remotefile, renameFile);
            }
            System.out.println("after rename");
            sftpChannel.exit();
            session.disconnect();
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        }
    }
}
