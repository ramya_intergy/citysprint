package au.com.intergy.invoice.delivery;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class IntergyAuthenticator extends Authenticator {
	String username;
	String password;
	
	public  IntergyAuthenticator(String username,String password){
		this.username=username;
		this.password=password;
	}
	
	public PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(username,password);
	}
}
