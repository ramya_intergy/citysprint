package au.com.intergy.invoice.delivery;

import java.io.IOException;
import java.util.Set;

import au.com.intergy.invoice.common.BatchGroup;
import au.com.intergy.invoice.common.BatchGroupDataAccess;
import au.com.intergy.invoice.common.Configuration;
import au.com.intergy.invoice.common.CustomerBatch;
import au.com.intergy.invoice.common.CustomerBatchDataAccess;
import au.com.intergy.invoice.common.DBAccessException;
import au.com.intergy.invoice.common.ErrorNofifier;
import au.com.intergy.invoice.common.Invoice;
import au.com.intergy.invoice.common.InvoiceDataAccess;
import au.com.intergy.invoice.common.InvoiceManager;
import au.com.intergy.invoice.common.Logger;
import au.com.intergy.invoice.common.Statement;
import au.com.intergy.invoice.common.BatchGroup.Status;

public class Printer {
	private static Logger logger = new Logger();
	private final static Configuration config = Configuration.getInstance();
	private final static String PRINT_COMMAND = config.getString("print.command");
	private final static boolean PRINT_STATEMENT_FIRST = config.getBoolean("print.statement.first", true);
	private final static String OPERATING_SYSTEM = config.getString("os");
	private final static String PRINT_OUTPUT_PATH = config.getString("print.output.path");
	private final static int PRINT_SLEEP_SECCONDS = config.getInt("print.sleep.seconds", 0);
	
	public static void main(String[] args) throws InterruptedException {
		System.out.println("Running Invoice Printer ...");
		
		init();
		try {
			Printer printer = new Printer();
			while (true) {
				printer.print();
				Thread.sleep(2000);
			}
		} catch (DBAccessException dae) {
			ErrorNofifier.addSystemError("Terminating Invoice Run's Printer " + dae.toString());
			logger.log("ERROR: Printer is quiting ..." + dae.toString());
			dae.printStackTrace();
			System.exit(-1);
		}
	}
	
	/**
	 * Validates configuration for printing.
	 */
	private static void init() {
		if (OPERATING_SYSTEM == null || OPERATING_SYSTEM.isEmpty()) {
			logger.log("ERROR: Printer is quiting - need to config the operating system 'os'.");
			System.out.println("ERROR: Printer is quiting - need to config the operating system 'os'.");
			System.exit(-1);
		}
		if (PRINT_OUTPUT_PATH == null || PRINT_OUTPUT_PATH.isEmpty()) {
			if ("windows".equalsIgnoreCase(OPERATING_SYSTEM)) {
				logger.log("ERROR: Printer is quiting - need to config 'print.output.path' for Windows OS.");
				System.out.println("ERROR: Printer is quiting - need to config 'print.output.path' for Windows OS.");
				System.exit(-1);
			}
		}
	}
	
	private void print() throws DBAccessException {
		//process 10 customer batches each time
		Set<CustomerBatch> custBatches = CustomerBatchDataAccess.findNotPrintedCustomerBatches(5);
		
		for (CustomerBatch custBatch : custBatches) {
			
			BatchGroup group = BatchGroupDataAccess.findById(custBatch.getBatchGroupId());
			//only print the customer batch if the batch group is started (i.e. processing), not paused or cancel
			if (Status.STARTED.name().equals(group.getStatus())) {
				System.out.println("Printing for customer batch " + custBatch.getCustomerBatchId() + " customer " + custBatch.getCustomerKey());
				if (PRINT_STATEMENT_FIRST) {
					printStatement(group.getSelectedPrinter(), custBatch.getStatement());
					printInvoices(group.getSelectedPrinter(), custBatch.getInvoices());
				} else {
					printInvoices(group.getSelectedPrinter(), custBatch.getInvoices());
					printStatement(group.getSelectedPrinter(), custBatch.getStatement());
				}
				CustomerBatchDataAccess.updateStatus(custBatch.getCustomerBatchId(),
						au.com.intergy.invoice.common.CustomerBatch.Status.SUCCESS.name());
				//complete the batch group if all cust batches have completed successfully
				BatchGroupDataAccess.updateStatusBasedOnCustBatch(Status.COMPLETED.name(), 
						au.com.intergy.invoice.common.CustomerBatch.Status.SUCCESS.name(), custBatch.getBatchGroupId());
				System.out.println("Finish for customer batch " + custBatch.getCustomerBatchId());
			}
		}
	}
	
	private void printInvoices(String printerName, Set<Invoice> invoices) {
		for (Invoice inv : invoices) {
			printStatement(printerName, inv);
		}
	}

	private void printStatement(String printerName, Statement statement) {
		
		try 
		{
			try {
				Thread.sleep(PRINT_SLEEP_SECCONDS * 1000);
			} catch (InterruptedException ie) {}            			
			
			//do not print if there is no statement
			if (statement == null)
				return;
			String command = PRINT_COMMAND.concat(" " + printerName).concat(" " + statement.getGeneratedFileName());
			if ("windows".equalsIgnoreCase(OPERATING_SYSTEM)) {
				command = command + " > " + PRINT_OUTPUT_PATH;
			}
			System.out.println(command);
			Process p = Runtime.getRuntime().exec(command);
			
			try {
				int waitCode = p.waitFor();
				if (waitCode != 0) {
					System.out.println("...unable to wait for the print process");
					logger.log("WARNING: Unable to wait for the print process for statement/inv " 
							+ statement.getId());
				}
			} catch (InterruptedException ie) { }
			
			try {
				InvoiceDataAccess.updateDeliveredDateTime(statement.getId(), System.currentTimeMillis());
			} catch (DBAccessException dba) {
				logger.log("WARNING: Unable to update print time for statement/inv " + statement.getId());
			}			
		} catch (IOException ioe) {
			try {
				InvoiceManager.failInvoice(statement.getId(), "Unable to print, check file " + 
						statement.getGeneratedFileName() + ", " + ioe.toString(), 
						statement.getBrand(), statement.getAccountNumber());
			} catch (DBAccessException dbae) {}
			System.exit(-1);
		}
	}
}
