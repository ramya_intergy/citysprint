package au.com.intergy.invoice.filegenerator;

public class FileGenerationException extends Exception {
	
	public FileGenerationException() {
		
	}
	
	public FileGenerationException(String message) {
		super(message);
	}
}
