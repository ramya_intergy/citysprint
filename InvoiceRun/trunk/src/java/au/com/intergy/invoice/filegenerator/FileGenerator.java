package au.com.intergy.invoice.filegenerator;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

import au.com.intergy.invoice.common.Configuration;
import au.com.intergy.invoice.common.CustomerBatchDataAccess;
import au.com.intergy.invoice.common.DBAccessException;
import au.com.intergy.invoice.common.DateUtil;
import au.com.intergy.invoice.common.ErrorNofifier;
import au.com.intergy.invoice.common.Invoice;
import au.com.intergy.invoice.common.InvoiceDataAccess;
import au.com.intergy.invoice.common.InvoiceManager;
import au.com.intergy.invoice.common.Logger;
import au.com.intergy.invoice.common.Statement;
import com.mapforce.MappingMapToEnvelope;
import com.mapforceNHS.MappingNHSToINVFIL;
import com.mapforceTP.MappingTPToINVFIL;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.http.HttpResponse;

import org.apache.http.client.*;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * This pdf file generator runs forever to pick up 
 * incompleted customer batch with output format = PDF, generates
 * pdf files for them and update their status.
 * 
 * @author Chi B.
 */
public class FileGenerator {

    /** Directory where configuration resides */
    private final static Logger logger = new Logger();
    private final static Configuration conf = Configuration.getInstance();
    private final static int INVOICE_LIMIT = conf.getInt("generator.invoice.limit", 5);
    private final static String GENERATED_FILE_DIR = conf.getString("generated.file.dir");
    private final static String XSLT_FILES_DIR = conf.getString("xslt.dir");
    private final static String GENERATE_TIFF_COMMAND = conf.getString(
            "generator.tiff.file.command").concat(" ").concat(conf.getString("generator.tiff.file.options"));
    private final static String SCHEDULEFILE_WEBSERVICE_URL = conf.getString("generator.schedule.file.webserviceurl");
    private final static String EDI_OUTPUT_NHS = conf.getString("generator.edi.output.nhs");
    private final static String EDI_OUTPUT_TP = conf.getString("generator.edi.output.tp");

    public static void main(String[] args) throws InterruptedException {
        FileGenerator pdf = new FileGenerator();

        logger.log("FILE GENERATOR: Running ...");
        System.out.println("FILE GENERATOR: Running ...");
        while (true) {
            try {
                pdf.generateFiles();
            } catch (DBAccessException dae) {
                ErrorNofifier.addSystemError("Terminating Invoice Run's FileGenerator " + dae.toString());
                logger.log("ERROR: FileGenerator is quiting ..." + dae.toString());
                dae.printStackTrace();
                System.exit(-1);
            } catch (Exception e) {
                ErrorNofifier.addSystemError("Terminating Invoice Run's FileGenerator " + e.toString());
                logger.log("ERROR: FileGenerator is quiting ..." + e.toString());
                e.printStackTrace();
                System.exit(-1);
            }
            Thread.sleep(2000);
        }
    }

    /** All file extensions will be in lower case. */
    private void generateFiles() throws DBAccessException, IOException, TransformerConfigurationException, TransformerException {
        //get some invoices that need to generate files
        Set<Statement> invoices;

        invoices = InvoiceDataAccess.getInvoicesWithNoGeneratedFiles(INVOICE_LIMIT);

        String generatedFileName = null;
        String pdfFileName = null;
        String customerKey = null;
        String scheduleGeneratedFileName = null;
        String scheduleXMLFileName = null;

        for (Statement stmt : invoices) {
            generatedFileName = GENERATED_FILE_DIR.concat("/");
            scheduleGeneratedFileName = GENERATED_FILE_DIR.concat("/");
            customerKey = InvoiceDataAccess.getCustomerKey((stmt instanceof Invoice) ?((Invoice) stmt).getCustBatchId():stmt.getCustBatchId());
            //invoice file name = INV_<invoice number>_<id>_yyyyMMMdd.<output format>
            generatedFileName = generatedFileName.concat((stmt instanceof Invoice) ? "INV_".concat(customerKey).concat("_").concat(((Invoice) stmt).getInvoiceNumber())
                    : "STMT_".concat( customerKey));
           
            generatedFileName = generatedFileName.concat("_" + Long.toString(stmt.getId()));
            generatedFileName =
                    generatedFileName.concat(DateUtil.getCurrentDateTime("_yyyyMMMdd"));
            pdfFileName = generatedFileName.concat(".pdf");
            generatedFileName = generatedFileName.concat("." + stmt.getOutputFormat().toLowerCase());

            if(stmt instanceof Invoice){
                if(((Invoice)stmt).getScheduleOutputFormat()==null)
                    scheduleGeneratedFileName = "";
                else {
                    scheduleGeneratedFileName = scheduleGeneratedFileName.concat((stmt instanceof Invoice) ? "SCH_".concat(customerKey).concat("_").concat(((Invoice) stmt).getInvoiceNumber())
                            : "");
                    scheduleGeneratedFileName = scheduleGeneratedFileName.concat("_" + Long.toString(stmt.getId()));
                    scheduleGeneratedFileName =
                            scheduleGeneratedFileName.concat(DateUtil.getCurrentDateTime("_yyyyMMMdd"));
                    scheduleXMLFileName = scheduleGeneratedFileName.concat(".xml");

                    scheduleGeneratedFileName = scheduleGeneratedFileName.concat("." + ((Invoice) stmt).getScheduleOutputFormat().toLowerCase());
                }
            }

            try {
                //logger.log("output format:"+stmt.getOutputFormat());
                if("INVOIC".equals(stmt.getOutputFormat())){
                    //logger.log("In if");
                    if(EDI_OUTPUT_NHS.equals(stmt.getXsltFileName())){
                        String nhsFileName = generatedFileName.substring(0, generatedFileName.lastIndexOf('.'));
                        nhsFileName = nhsFileName.concat(".edi");
                        generateNHSEDI(nhsFileName, stmt.getXmlBody());
                        generatedFileName = nhsFileName;
                    }
                    else if(EDI_OUTPUT_TP.equals(stmt.getXsltFileName())){
                        String tpFileName = generatedFileName.substring(0, generatedFileName.lastIndexOf('.'));
                        tpFileName = tpFileName.concat(".edi");
                        generateTPEDI(tpFileName, stmt.getXmlBody());
                        generatedFileName = tpFileName;
                    }
                    else
                        generateEDI(generatedFileName, stmt.getXmlBody());
                }
                else if ("XML".equals(stmt.getOutputFormat())) {
                    generateXML(generatedFileName, XSLT_FILES_DIR.concat("/").concat(stmt.getXsltFileName()), stmt.getXmlBody());
                } else {

                    generatePDF(pdfFileName, XSLT_FILES_DIR.concat("/").concat(stmt.getXsltFileName()), stmt.getXmlBody());

                    if ("TIFF".equals(stmt.getOutputFormat())) {
                        Process p = Runtime.getRuntime().exec(
                                GENERATE_TIFF_COMMAND.concat(generatedFileName).concat(" " + pdfFileName));
                    }
                }
                InvoiceDataAccess.updateInvoiceFileGeneratedDetails(stmt.getId(), generatedFileName);
                System.out.println("Generated file " + generatedFileName + " for invoice id " + stmt.getId());

                //Generate schedule file
                if(stmt instanceof Invoice){
                    try
                    {
                        if(((Invoice)stmt).getScheduleOutputFormat()==null)
                        {}
                        else{
                            if(!((Invoice)stmt).getScheduleOutputFormat().equals("")){
                                logger.log("ScheduleOutputFormat:"+((Invoice)stmt).getScheduleOutputFormat());
                                generateScheduleFile(scheduleXMLFileName, scheduleGeneratedFileName, stmt.getXmlBody(), ((Invoice)stmt).getScheduleOutputFormat());

                                InvoiceDataAccess.updateScheduleFileGeneratedDetails(stmt.getId(), scheduleGeneratedFileName);
                                logger.log("Schedule Generated file " + scheduleGeneratedFileName + " for invoice id " + stmt.getId());
                            }
                        }
                    }
                    catch (Exception e){
                        e.printStackTrace();
                        logger.log("Unable to generate Schedule file " + scheduleGeneratedFileName + " for invoice id " + stmt.getId());
                    }
                }
                if (InvoiceDataAccess.allFilesGenerated(stmt.getId())) {
                    CustomerBatchDataAccess.updateGeneratedFlag(stmt.getCustBatchId());
                }
            } catch (Exception e) {
                e.printStackTrace();
                logger.log("Unable to generate file " + generatedFileName + " for invoice/statement " + stmt.getId() + e.toString());
                try {
                    InvoiceManager.failInvoice(stmt.getId(),
                            "Unable to generate file" + generatedFileName, stmt.getBrand(), stmt.getAccountNumber());
                } catch (DBAccessException de) {
                    logger.log("FILE GENERATOR: Unable to update invoice error message " + de.toString());
                }

                if (e instanceof DBAccessException) {
                    throw (DBAccessException) e;
                }

                if (e instanceof TransformerConfigurationException) {
                    throw (TransformerConfigurationException) e;
                }

                if (e instanceof TransformerException) {
                    throw (TransformerException) e;
                }
            }
        }
    }

    /**
     *
     * @param filePath Absolute path to the PDF file.
     */
    void generatePDF(String filePath, String xsltFilePath, String content)
            throws FileGenerationException {
        FopFactory fopFactory = FopFactory.newInstance();
        FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
        Fop fop = null;
        OutputStream out = null;


        File xsltFile = new File(xsltFilePath);
        if (!xsltFile.exists()) {
            throw new FileGenerationException("Unable to generate pdf file " + filePath + " because stylesheet " +
                    xsltFilePath + " does not exist.");
        }
        try {
            out = new java.io.FileOutputStream(new File(filePath));
            out = new java.io.BufferedOutputStream(out);

            fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

            // Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(xsltFile));

            transformer.setParameter("versionParam", "2.0");

            Source src = new StreamSource(new ByteArrayInputStream(content.getBytes()));
            Result res = new SAXResult(fop.getDefaultHandler());

            transformer.transform(src, res);
        } catch (TransformerConfigurationException tce) {
            throw new FileGenerationException("Unable to read " + xsltFilePath + "." + tce.toString());
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileGenerationException("Unable to generate pdf file " + filePath +
                    " with " + xsltFilePath + e.toString());
        } finally {
            try {
                out.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * @author Adnan Nasrullah
     * @param filePath
     * @param contents
     * @throws FileGenerationException 
     */
     void generateEDI (String filePath, String contents) throws FileGenerationException {
        InputStream in = new ByteArrayInputStream(contents.getBytes());
        
        try { // Mapping
                        com.mapforce.MappingConsole mappingConsoleObj = new com.mapforce.MappingConsole();
//			mappingConsoleObj.TraceTargetConsole ttc = new mappingConsoleObj.TraceTargetConsole();


			MappingMapToEnvelope MappingMapToEnvelopeObject = new MappingMapToEnvelope();


			MappingMapToEnvelopeObject.registerTraceTarget(mappingConsoleObj.getTraceTargetConsole());
                        
                        com.altova.io.Output EnvelopeTarget = new com.altova.io.FileOutput(filePath);
                        
                        MappingMapToEnvelopeObject.run(
						in,
						EnvelopeTarget);
			System.out.println("Finished");
		} 
		catch (com.altova.UserException ue) 
		{
			System.out.print("USER EXCEPTION:");
			System.out.println( ue.getMessage() );
			System.exit(1);
		}
		catch (com.altova.AltovaException e)
		{
			System.out.print("ERROR: ");
			System.out.println( e.getMessage() );
			if (e.getInnerException() != null)
			{
				System.out.print("Inner exception: ");
				System.out.println(e.getInnerException().getMessage());
				if (e.getInnerException().getCause() != null)
				{
					System.out.print("Cause: ");
					System.out.println(e.getInnerException().getCause().getMessage());
				}
			}
			System.out.println("\nStack Trace: ");
			e.printStackTrace();
			System.exit(1);
		}
		
		catch (Exception e) {
			System.out.print("ERROR: ");
			System.out.println( e.getMessage() );
			System.out.println("\nStack Trace: ");
			e.printStackTrace();
			System.exit(1);
		}
        
    }

     void generateNHSEDI(String filePath, String contents) throws FileGenerationException {
        InputStream in = new ByteArrayInputStream(contents.getBytes());

        try { // Mapping
            System.out.println("In NHSEDI");

            com.mapforceNHS.MappingConsole mappingConsoleObj = new com.mapforceNHS.MappingConsole();
//            mappingConsoleObj.TraceTargetConsole ttc = new mappingConsoleObj.TraceTargetConsole();

            MappingNHSToINVFIL MappingNHSToINVFILObject = new MappingNHSToINVFIL();
            //MappingNHSToINVFILObject.registerTraceTarget(mappingConsoleObj.getTraceTargetConsole());
            com.altovaNHS.io.Output NHSTarget = new com.altovaNHS.io.FileOutput(filePath);
            MappingNHSToINVFILObject.run(in, NHSTarget);
            System.out.println("NHS Finished");
            
        } catch (com.altova.UserException ue) {
            System.out.print("USER EXCEPTION:");
            System.out.println(ue.getMessage());
            System.exit(1);
        } catch (com.altova.AltovaException e) {
            System.out.print("ERROR: ");
            System.out.println(e.getMessage());
            if (e.getInnerException() != null) {
                System.out.print("Inner exception: ");
                System.out.println(e.getInnerException().getMessage());
                if (e.getInnerException().getCause() != null) {
                    System.out.print("Cause: ");
                    System.out.println(e.getInnerException().getCause().getMessage());
                }
            }
            System.out.println("\nStack Trace: ");
            e.printStackTrace();
            System.exit(1);
        } catch (Exception e) {
            System.out.print("ERROR: ");
            System.out.println(e.getMessage());
            System.out.println("\nStack Trace: ");
            e.printStackTrace();
            System.exit(1);
        }

    }

     void generateTPEDI (String filePath, String contents) throws FileGenerationException {
        InputStream in = new ByteArrayInputStream(contents.getBytes());

         try { // Mapping
             System.out.println("In TPEDI");
             com.mapforceTP.MappingConsole mappingConsoleObj = new com.mapforceTP.MappingConsole();
//			mappingConsoleObj.TraceTargetConsole ttc = new mappingConsoleObj.TraceTargetConsole();


             MappingTPToINVFIL MappingTPToINVFILObject = new MappingTPToINVFIL();


             MappingTPToINVFILObject.registerTraceTarget(mappingConsoleObj.getTraceTargetConsole());

             com.altovaTP.io.Output TPTarget = new com.altovaTP.io.FileOutput(filePath);

             MappingTPToINVFILObject.run(
                     in,
                     TPTarget);
             System.out.println("TP Finished");
         } catch (com.altova.UserException ue) {
             System.out.print("USER EXCEPTION:");
             System.out.println(ue.getMessage());
             System.exit(1);
         } catch (com.altova.AltovaException e) {
             System.out.print("ERROR: ");
             System.out.println(e.getMessage());
             if (e.getInnerException() != null) {
                 System.out.print("Inner exception: ");
                 System.out.println(e.getInnerException().getMessage());
                 if (e.getInnerException().getCause() != null) {
                     System.out.print("Cause: ");
                     System.out.println(e.getInnerException().getCause().getMessage());
                 }
             }
             System.out.println("\nStack Trace: ");
             e.printStackTrace();
             System.exit(1);
         } catch (Exception e) {
             System.out.print("ERROR: ");
             System.out.println(e.getMessage());
             System.out.println("\nStack Trace: ");
             e.printStackTrace();
             System.exit(1);
         }

    }

    void generateXML(String filePath, String xsltFilePath, String content) throws TransformerConfigurationException, TransformerException, FileGenerationException {
        try {
            transform(content, xsltFilePath, filePath);
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
            throw new TransformerConfigurationException("Invalid factory configuration");
        } catch (TransformerException e) {
            e.printStackTrace();
            throw new TransformerException("Error during XML transformation");

        }
    }

    public void transform(String inXML, String inXSL, String outXML) throws TransformerConfigurationException, TransformerException, FileGenerationException {
        File xsltFile = new File(inXSL);
        if (!xsltFile.exists()) {
            throw new FileGenerationException("Unable to generate xml file " + outXML + " because stylesheet " +
                    inXSL + " does not exist.");
        }

        TransformerFactory factory = TransformerFactory.newInstance();
        StreamSource xslStream = new StreamSource(xsltFile);
        Transformer transformer = factory.newTransformer(xslStream);
        StreamSource in = new StreamSource(new ByteArrayInputStream(inXML.getBytes()));
        StreamResult out = new StreamResult(outXML);
        transformer.transform(in, out);
        System.out.println("The generated XML file is:" + outXML);
    }

    void generateScheduleFile (String XMLFileName, String filePath, String contents, String format) throws FileGenerationException {

        OutputStream out = null;
        String sCommandLine = "";
        int iStatus = 0;
        String sLocation = "";
        HttpResponse resOutput = null;
        logger.log("In generateScheduleFile XMLFileName:" + XMLFileName);
        logger.log("filePath:"+filePath);
        try {

            /*out = new java.io.FileOutputStream(new File(XMLFileName));
            out.write(contents.getBytes());

            sCommandLine = "curl -v -X POST -d @" + XMLFileName + " " + SCHEDULEFILE_WEBSERVICE_URL + format.toLowerCase();
            logger.log("############### sCommandLine ==" + sCommandLine);
            Process proc = Runtime.getRuntime().exec(sCommandLine);
            proc.waitFor();
				
            BufferedReader stdInput = new BufferedReader(new
            InputStreamReader(proc.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
            InputStreamReader(proc.getErrorStream()));

            // read the output from the command
            String s = null;
            while ((s = stdInput.readLine()) != null) {
                    logger.log(s);
            }

            // read any errors from the attempted command
            while ((s = stdError.readLine()) != null) {
                logger.log(s);
                if (s.contains("HTTP/1.1")) {
                    logger.log("************status substring:"+s.substring(10, 14));
                    sStatus = s.substring(10, 14);
                    logger.log("indexof:"+s.indexOf("HTTP/1.1"));
                }

                //logger.log("sStatus:" + sStatus);
                if (sStatus.trim().equals("201") && s.contains("Location")) {
                    sLocation = s.substring(12, s.length());
                    logger.log("************sLocation substring:"+sLocation);
                    sLocation = sLocation.trim();
                }
            }*/

            URL url = new URL(SCHEDULEFILE_WEBSERVICE_URL + format.toLowerCase());

            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setRequestMethod("POST");
            httpConn.setRequestProperty("Content-Type", "application/xml; charset=utf-8");
            httpConn.setRequestProperty("Content-Length", Integer.toString(contents.length()));
            httpConn.setDoOutput(true);

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter((httpConn.getOutputStream())));
            writer.write(contents, 0, contents.length());
            writer.flush();
            writer.close();

            iStatus = httpConn.getResponseCode();
            logger.log("iStatus:"+iStatus);
            logger.log("sLocation:"+httpConn.getHeaderField("Location"));
            sLocation = httpConn.getHeaderField("Location");
                
             

            if(sLocation.length() > 0 && (iStatus==201)){
              HttpClient client = new DefaultHttpClient();
              logger.log("Before downloading:"+sLocation);
              HttpGet get = new HttpGet(sLocation);
              resOutput = client.execute(get);
              logger.log("sLocation.length:"+sLocation.length());
             
              InputStream is = resOutput.getEntity().getContent();
              FileOutputStream fos = new FileOutputStream(new File((filePath)));
              int inByte;
              while ((inByte = is.read()) != -1) {
                  fos.write(inByte);
              }
              logger.log("after write");
              is.close();
              fos.close();
            }

        } catch (Exception ex) {
            logger.log("generateScheduleFile exception:"+ex.getMessage());
        }

    }
}
