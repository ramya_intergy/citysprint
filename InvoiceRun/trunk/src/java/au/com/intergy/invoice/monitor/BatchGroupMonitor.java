package au.com.intergy.invoice.monitor;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import au.com.intergy.invoice.common.Logger;
import au.com.intergy.invoice.monitor.webhandler.CancelPrintingHandler;
import au.com.intergy.invoice.monitor.webhandler.DisplayBatchInfoHandler;
import au.com.intergy.invoice.monitor.webhandler.DisplayRunInfoHandler;
import au.com.intergy.invoice.monitor.webhandler.Handler;
import au.com.intergy.invoice.monitor.webhandler.InitialisePrintingHandler;
import au.com.intergy.invoice.monitor.webhandler.PausePrintingHandler;
import au.com.intergy.invoice.monitor.webhandler.ResumePrintingHandler;
import au.com.intergy.invoice.monitor.webhandler.StartPrintingHandler;
import au.com.intergy.invoice.monitor.webhandler.ViewBatchGroupsHandler;

public class BatchGroupMonitor extends HttpServlet {
	    private static Logger logger = new Logger();
	    private Map<String, Handler> actionHandlerMap = new HashMap<String, Handler>();
	    
	    
	    public BatchGroupMonitor() {
	    	//initialise mapping between actions and handlers
	    	if (actionHandlerMap.isEmpty()) {
	    		actionHandlerMap.put(Handler.INIT_PRINT_ACTION, new InitialisePrintingHandler());
	    		actionHandlerMap.put(Handler.START_PRINT_ACTION, new StartPrintingHandler());
	    		actionHandlerMap.put(Handler.PAUSE_PRINT_ACTION, new PausePrintingHandler());
	    		actionHandlerMap.put(Handler.RESUME_PRINT_ACTION, new ResumePrintingHandler());
	    		actionHandlerMap.put(Handler.CANCEL_PRINT_ACTION, new CancelPrintingHandler());
	    		actionHandlerMap.put(Handler.DISPLAY_RUNINFO_ACTION, new DisplayRunInfoHandler());
                        actionHandlerMap.put(Handler.DISPLAY_BATCHINFO_ACTION, new DisplayBatchInfoHandler());                        
	    	}
	    }
	    
	    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
	     * @param request servlet request
	     * @param response servlet response
	     */
	    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
	    	String actionParam = request.getParameter("action");
	    	
	    	Handler handler = actionHandlerMap.get(actionParam);
	    	
	    	if (handler == null)
	    		handler = new ViewBatchGroupsHandler();
	    	
	    	String viewName= "error.jsp";
	    	
	    	try {
	    		Map<String, Object> result = handler.handle(request, response);
	    		viewName = result.get(Handler.VIEW_NAME).toString();
	    		
	    	} catch (Exception e) {
	    		e.printStackTrace();
	    		System.out.println(e.toString());
	    		logger.log(e.toString());
	    	}
	    	
	    	try {	    	
	    		if ( actionParam == null || actionParam.contains("view")) {
	    			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/view/" + viewName);
	    			dispatcher.forward(request, response);
	    		} else {
	    			
	    			response.sendRedirect("./monitor?action=viewBatchGroup");
	    		}
	    	} catch (Exception ioe) {
	    		logger.log("Error in forwarding/redirecting pages" + ioe.toString());
	    	}
	    }
	    
	    /** Handles the HTTP <code>GET</code> method.
	     * @param request servlet request
	     * @param response servlet response
	     */
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	    	
	    		processRequest(request, response);
	    	
	    }

	    /** Handles the HTTP <code>POST</code> method.
	     * @param request servlet request
	     * @param response servlet response
	     */
	    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
	    		processRequest(request, response);
	    }
	    
	    /** Returns a short description of the servlet.
	     */
	    public String getServletInfo() {
	        return "Short description";
	    }
	    // </editor-fold>

	    private void UpdateStartStatus(HttpServletRequest request, HttpServletResponse response,String status)throws ServletException, IOException
	    {
	        Connection conDB = null;
	        Connection conDBInvoice = null;
	        try
	        {
	            String [] sIds;
	            String sql="";
	            String sSQL="";
	            String sId=request.getParameter("sId")==null?"0":request.getParameter("sId").toString();
	              
	            sIds=sId.split(",");
	            Statement stmUpdate;
	            Statement stmUpdateInvoice;
	            //DataSource dataSource=getInvoiceRunbyAdmin();
	            conDB = OpenConnection();
	            conDBInvoice = OpenConnection();
	             ResultSet rstRead;
	             ResultSet rstReadBatch;
	             Statement stmSelect;
	             String tempStatus="";
	            
	            for(int i=0;i<sIds.length;i++)
	            {
	                 stmSelect= conDB.createStatement();
	                 sSQL="select * from BatchStatus where BatchStatusID="+sIds[i]+"";
	                 rstRead=stmSelect.executeQuery(sSQL);	
	                 
	                 
	                 while (rstRead.next())
	                 {
	                    tempStatus=rstRead.getString("status");
	                 }

	                 if (tempStatus.equals("NS") || tempStatus.equals("Resume") || tempStatus.equals("Cancel") || tempStatus.equals("Pause") || tempStatus.equals("Completed") )
	                 {
	                    String sUpdateSql="update BatchStatus set status='"+ status +"' where BatchStatusID ="+sIds[i];
	                    stmUpdate= conDB.createStatement();
	                    stmUpdate.executeUpdate(sUpdateSql);	
	                 }
	                 
	                 
	                 stmSelect= conDBInvoice.createStatement();
	                 sSQL="select outputFormat,BatchID,TransportMethod from BatchStatus where BatchStatusID="+sIds[i]+"";
	                 rstReadBatch=stmSelect.executeQuery(sSQL);	
	                 
	                 while(rstReadBatch.next())
	                 {
	                     String sUpdateInvoice="update InvoiceDetails set Ready='Y' where TransportMethod='"+ rstReadBatch.getString("TransportMethod") +"' and OutputFormat='"+ rstReadBatch.getString("OutputFormat") +"' and BatchID="+ rstReadBatch.getInt("BatchID");
	                     stmUpdateInvoice= conDBInvoice.createStatement();
	                     stmUpdateInvoice.executeUpdate(sUpdateInvoice);
	                 }
	                 
	                 
	                 
	            }
	            conDB.close();
	            conDBInvoice.close();
	            
	            
	        }
	        catch(Exception Ex)
	        {
	            
	        }
	 
	        
	    }
	    
	    
	    private void UpdatePauseStatus(HttpServletRequest request, HttpServletResponse response,String status)throws ServletException, IOException
	    {
	        Connection conDB = null;
	        Connection conDBInvoice = null;
	        try
	        {
	            String [] sIds;
	            String sql="";
	            String sSQL="";
	            String sId=request.getParameter("sId")==null?"0":request.getParameter("sId").toString();
	              
	            sIds=sId.split(",");
	            Statement stmUpdate;
	            Statement stmUpdateInvoice;
	            //DataSource dataSource=getInvoiceRunbyAdmin();
	            conDB = OpenConnection();
	            conDBInvoice = OpenConnection();
	            ResultSet rstRead;
	            ResultSet rstReadBatch;
	            Statement stmSelect;
	            String tempStatus="";
	            
	            for(int i=0;i<sIds.length;i++)
	            {
	                 stmSelect= conDB.createStatement();
	                 sSQL="select * from BatchStatus where BatchStatusID="+sIds[i]+"";
	                 rstRead=stmSelect.executeQuery(sSQL);	
	                 
	                 
	                 while (rstRead.next())
	                 {
	                    tempStatus=rstRead.getString("status");
	                 }

	                 if (tempStatus.equals("Resume") || tempStatus.equals("Cancel"))
	                 {
	                    String sUpdateSql="update BatchStatus set Status='"+ status +"' where BatchStatusID ="+sIds[i];
	                    stmUpdate= conDB.createStatement();
	                    stmUpdate.executeUpdate(sUpdateSql);	
	                 }
	                 
	                 stmSelect= conDBInvoice.createStatement();
	                 sSQL="select OutputFormat,BatchID,TransportMethod from BatchStatus where BatchStatusID="+sIds[i]+"";
	                 rstReadBatch=stmSelect.executeQuery(sSQL);	
	                 
	                 while(rstReadBatch.next())
	                 {
	                     String sUpdateInvoice="update InvoiceDetails set Ready='N' where TransportMethod='"+ rstReadBatch.getString("TransportMethod") +"' and OutputFormat='"+ rstReadBatch.getString("OutputFormat") +"' and BatchID="+ rstReadBatch.getInt("BatchID");
	                     stmUpdateInvoice= conDBInvoice.createStatement();
	                     stmUpdateInvoice.executeUpdate(sUpdateInvoice);
	                 }
	                 
	                 
	            }
	            conDB.close();
	            conDBInvoice.close();
	            
	            
	            
	        }
	        catch(Exception Ex)
	        {
	            
	        }
	 
	        
	    }

	        
	    private void UpdateResumeStatus(HttpServletRequest request, HttpServletResponse response,String status)throws ServletException, IOException
	    {
	        Connection conDB = null;
	        Connection conDBInvoice = null;
	        try
	        {
	            String [] sIds;
	            String sql="";
	            String sSQL="";
	            String sId=request.getParameter("sId")==null?"0":request.getParameter("sId").toString();
	              
	            sIds=sId.split(",");
	            Statement stmUpdate;
	            Statement stmUpdateInvoice;
	            //DataSource dataSource=getInvoiceRunbyAdmin();
	            conDB = OpenConnection();
	            conDBInvoice = OpenConnection();
	            
	             ResultSet rstRead;
	             ResultSet rstReadBatch;
	             
	             Statement stmSelect;
	             String tempStatus="";
	            
	            for(int i=0;i<sIds.length;i++)
	            {
	                 stmSelect= conDB.createStatement();
	                 sSQL="select * from BatchStatus where BatchStatusID="+sIds[i]+"";
	                 rstRead=stmSelect.executeQuery(sSQL);	
	                 
	                     
	                 while (rstRead.next())
	                 {
	                    tempStatus=rstRead.getString("status");
	                 }

	                 if (tempStatus.equals("Pause") || tempStatus.equals("Cancel"))
	                 {
	                    String sUpdateSql="update BatchStatus set status='"+ status +"' where BatchStatusID ="+sIds[i];
	                    stmUpdate= conDB.createStatement();
	                    stmUpdate.executeUpdate(sUpdateSql);	
	                 }

	                 stmSelect= conDBInvoice.createStatement();
	                 sSQL="select outputFormat,BatchID,TransportMethod from BatchStatus where BatchStatusID="+sIds[i]+"";
	                 rstReadBatch=stmSelect.executeQuery(sSQL);	
	                 
	                 while(rstReadBatch.next())
	                 {
	                     String sUpdateInvoice="update InvoiceDetails set ready='Y' where TransportMethod='"+ rstReadBatch.getString("TransportMethod") +"' and outputFormat='"+ rstReadBatch.getString("outputFormat") +"' and BatchID="+ rstReadBatch.getInt("BatchID");
	                     stmUpdateInvoice= conDBInvoice.createStatement();
	                     stmUpdateInvoice.executeUpdate(sUpdateInvoice);
	                 }
	                 
	            }
	             
	            conDB.close();
	            conDBInvoice.close();
	            
	            
	        }
	        catch(Exception Ex)
	        {
	            
	        }
	 
	        
	    }

	            
	    private void UpdateCancelStatus(HttpServletRequest request, HttpServletResponse response,String status)throws ServletException, IOException
	    {
	        Connection conDB = null;
	        Connection conDBInvoice = null;
	        
	        try
	        {
	            String [] sIds;
	            String sql="";
	            String sSQL="";
	            String sId=request.getParameter("sId")==null?"0":request.getParameter("sId").toString();
	              
	            sIds=sId.split(",");
	            Statement stmUpdate;
	            Statement stmUpdateInvoice;
	            //DataSource dataSource=getInvoiceRunbyAdmin();
	            conDB = OpenConnection();
	            conDBInvoice = OpenConnection();
	            ResultSet rstRead;
	            ResultSet rstReadBatch;
	            Statement stmSelect;
	            String tempStatus="";
	            
	            for(int i=0;i<sIds.length;i++)
	            {
	                 stmSelect= conDB.createStatement();
	                 sSQL="select * from BatchStatus where BatchStatusID="+sIds[i]+"";
	                 rstRead=stmSelect.executeQuery(sSQL);	
	                 
	                 
	                 while (rstRead.next())
	                 {
	                    tempStatus=rstRead.getString("status");
	                 }

	                 if (tempStatus.equals("Start") || tempStatus.equals("Pause") || tempStatus.equals("Resume"))
	                 {
	                    String sUpdateSql="update BatchStatus set status='"+ status +"' where BatchStatusID ="+sIds[i];
	                    stmUpdate= conDB.createStatement();
	                    stmUpdate.executeUpdate(sUpdateSql);	
	                 }
	                 
	                 stmSelect= conDBInvoice.createStatement();
	                 sSQL="select outputFormat,BatchID,TransportMethod from BatchStatus where BatchStatusID="+sIds[i]+"";
	                 rstReadBatch=stmSelect.executeQuery(sSQL);	
	                 
	                 while(rstReadBatch.next())
	                 {
	                     String sUpdateInvoice="update InvoiceDetails set ready='N' where TransportMethod='"+ rstReadBatch.getString("TransportMethod") +"' and outputFormat='"+ rstReadBatch.getString("outputFormat") +"' and BatchID="+ rstReadBatch.getInt("BatchID");
	                     stmUpdateInvoice= conDBInvoice.createStatement();
	                     stmUpdateInvoice.executeUpdate(sUpdateInvoice);
	                 }
	            }
	            conDB.close();
	          
	            
	        }
	        catch(Exception Ex)
	        {
	            
	        }
	 
	        
	    }

	    private void loadPage(HttpServletRequest request, HttpServletResponse response)   throws SQLException,ServletException, IOException 
	    {
	         ServletOutputStream strmResponse = response.getOutputStream();
	        try
	        {
	                String sSql="select * from BatchStatus where status not in('Completed')";
	                Connection conDB = null;
	                Connection conDB1=null;
	                Statement stmSelect;
	                Statement stmSelectCount;
	                ResultSet rstRead;
	                ResultSet rstReadCount;
	                ResultSet rstReadTotalCount;
	        
	            //DataSource dataSource=getInvoiceRunbyAdmin();
	            conDB = OpenConnection();
	            conDB1 = OpenConnection();
	            stmSelect= conDB.createStatement();
	            rstRead=stmSelect.executeQuery(sSql);
	           
	        
	            strmResponse.println("<html>");
	            strmResponse.println("<head>");
	            strmResponse.println("<title>Batch Status Monitor</title>");
	            strmResponse.println("<link media=\"screen\" href=\"NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
	            strmResponse.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
	            strmResponse.println("</head>");
	         
	            strmResponse.println("<script language=javascript>");
	            
	            strmResponse.println("function getCheckBoxID()");
	            strmResponse.println("{");
	                strmResponse.println("var sId=\"\";");
	                strmResponse.println("var CInputBooking=document.getElementsByTagName('input');");
	                strmResponse.println("for (var i=0,thisInput; thisInput=CInputBooking[i]; i++) {");
	                strmResponse.println("if(thisInput.type==\"checkbox\")");
	                strmResponse.println("{");
	                strmResponse.println("if(thisInput.checked==true)");
	                strmResponse.println("sId=sId+thisInput.value+\",\";");
	                strmResponse.println("}");
	                strmResponse.println("}");
	                strmResponse.println("return sId");
	            strmResponse.println("}");
	            
	             strmResponse.println("function startAction()");
	             strmResponse.println("{");
	                 strmResponse.println("var sId=getCheckBoxID()");
	                 //strmResponse.println("var pId=getCheckBoxID()");
	                 strmResponse.println("document.BatchStatusMonitor.sId.value=sId");
	                 strmResponse.println("document.BatchStatusMonitor.sAction.value='Start'");
	                 strmResponse.println("document.BatchStatusMonitor.submit();");
	             strmResponse.println("}");
	            
	             strmResponse.println("function pauseAction()");
	             strmResponse.println("{");
	                 strmResponse.println("var sId=getCheckBoxID()");
	                 //strmResponse.println("var pId=getCheckBoxID()");
	                 strmResponse.println("document.BatchStatusMonitor.sId.value=sId");
	                 strmResponse.println("document.BatchStatusMonitor.sAction.value='Pause'");
	                 strmResponse.println("document.BatchStatusMonitor.submit();");
	             strmResponse.println("}");
	             
	             strmResponse.println("function resumeAction()");
	             strmResponse.println("{");
	                 strmResponse.println("var sId=getCheckBoxID()");
	                 //strmResponse.println("var pId=getCheckBoxID()");
	                 strmResponse.println("document.BatchStatusMonitor.sId.value=sId");
	                 strmResponse.println("document.BatchStatusMonitor.sAction.value='Resume'");
	                 strmResponse.println("document.BatchStatusMonitor.submit();");
	             strmResponse.println("}");
	             
	             
	             strmResponse.println("function cancelAction()");
	             strmResponse.println("{");
	                 strmResponse.println("var sId=getCheckBoxID()");
	                 //strmResponse.println("var pId=getCheckBoxID()");
	                 strmResponse.println("document.BatchStatusMonitor.sId.value=sId");
	                 strmResponse.println("document.BatchStatusMonitor.sAction.value='Cancel'");
	                 strmResponse.println("document.BatchStatusMonitor.submit();");
	             strmResponse.println("}");
	             
	             
	            strmResponse.println("</script>");
	            strmResponse.println("</head>");
	            
	            strmResponse.println("<body>");
	            strmResponse.println("<br><br>");
	            strmResponse.println("<table border=\"1\" align=\"center\">");
	            strmResponse.println("<tr><td colspan=10><h5>Current</h5></td></tr>");
	            strmResponse.println("<tr>");
	            strmResponse.println("<td></td>");
	            strmResponse.println("<td>Network Printers</td>");
	            strmResponse.println("<td>Date Time</td>");
	            strmResponse.println("<td>Batch ID</td>");
	            strmResponse.println("<td>Brand/Group</td>");
	            strmResponse.println("<td>Output Format</td>");
	            strmResponse.println("<td>Transport Method</td>");
	            strmResponse.println("<td>Status</td>");
	            strmResponse.println("<td>No of Invoices</td>");
	            strmResponse.println("<td>No of Customer Batches</td>");
	            strmResponse.println("</tr>");
	            int count=0;
	            String SqlTotalCount,SqlCount,SqlCustBatchCount;
	            
	            double percentage=0;
	            while (rstRead.next())
	            {
	                
	                
	                //SqlTotalCount="select count(*) as TotalNumberOfInvoices from InvoiceDetails group by batchid,brand_Group,outputformat having batchid="+ rstRead.getInt("BatchID") +" and brand_group='"+ rstRead.getString("Brand_Group") +"' and outputformat='"+rstRead.getString("OutputFormat")+"';";
	                // changed the display from total count to custmer batch count
	                SqlCustBatchCount="select count(*) as TotalNumberOfInvoices,brand_Group,outputformat,TransportMethod,BatchID from CustomerBatchDetails group by brand_Group,outputformat,TransportMethod,BatchID having BatchID="+ rstRead.getInt("BatchID") ;
	                SqlCount="Select count(*) as NumberofInvoices from InvoiceDetails group by BatchID,Brand_Group,OutputFormat,TransportMethod,Ready,Sent,Status having BatchID="+ rstRead.getInt("BatchID") +" and Brand_Group='"+ rstRead.getString("Brand_Group") +"' and OutputFormat='"+rstRead.getString("OutputFormat")+"' and Sent='Y';";

	                stmSelectCount= conDB1.createStatement();
	                stmSelect=conDB1.createStatement();
	                rstReadCount=stmSelectCount.executeQuery(SqlCount);
	                rstReadTotalCount=stmSelect.executeQuery(SqlCustBatchCount);

	                int TotalCount=0,tempCount=0;

	                while (rstReadCount.next())
	                {
	                    tempCount=rstReadCount.getInt("NumberofInvoices");
	                }
	                rstReadCount.close();
	                while (rstReadTotalCount.next())
	                {
	                    TotalCount=rstReadTotalCount.getInt("TotalNumberOfInvoices");
	                }
	                rstReadTotalCount.close();
	                double temp= (double)tempCount/(double)TotalCount ;
	                percentage= Math.round(temp * (double)100);
	                
	                strmResponse.println("<tr>");
	                strmResponse.println("<td>");
	                strmResponse.println("<input id=\"chkBatchStatusID\" value="+rstRead.getInt("BatchStatusID")+" type=\"checkbox\"></td>");
	                strmResponse.println("<td>");
	                strmResponse.println("<select id=\"selectPrinter\">");
	                strmResponse.println("<option>Printer 1</option>");
	                strmResponse.println("</select>");
	                strmResponse.println("</td>");
	                strmResponse.println("<td>"+rstRead.getDate("DateSubmitted")+ " " + rstRead.getTime("DateSubmitted") +"</td>");
	                strmResponse.println("<td>"+rstRead.getInt("BatchID")+"</td>");
	                strmResponse.println("<td>"+rstRead.getString("Brand_Group")+"</td>");
	                strmResponse.println("<td>"+rstRead.getString("OutputFormat")+"</td>");
	                strmResponse.println("<td>"+rstRead.getString("TransportMethod")+"</td>");
	                strmResponse.println("<td>" + rstRead.getString("status") + " ("+ (int)percentage + "%)</td>");
	                strmResponse.println("<td>"+rstRead.getInt("NumberofInvoices")+"</td>");
	                strmResponse.println("<td>"+TotalCount+"</td>");
	                strmResponse.println("</tr>");
	               
	            }
	           strmResponse.println("</table>");
	           
	           strmResponse.println("<br /><br />");
	           
	           // Code to display Completed records
	           
	           sSql="Select * from BatchGroup where status='Completed'";
	            conDB = OpenConnection();
	            stmSelect= conDB.createStatement();
	            rstRead=stmSelect.executeQuery(sSql);
	           
	            strmResponse.println("<table border=\"1\" align=\"center\">");
	            strmResponse.println("<tr><td colspan=10><h5>Completed</h5></td></tr>");
	            strmResponse.println("<tr>");
	            strmResponse.println("<td></td>");
	            strmResponse.println("<td>Network Printers</td>");
	            strmResponse.println("<td>Date Time</td>");
	            strmResponse.println("<td>Batch ID</td>");
	            strmResponse.println("<td>Brand/Group</td>");
	            strmResponse.println("<td>Output Format</td>");
	            strmResponse.println("<td>Transport Method</td>");
	            strmResponse.println("<td>Status</td>");
	            strmResponse.println("<td>No of Invoices</td>");
	            strmResponse.println("<td>No of Customer Batches</td>");
	            strmResponse.println("</tr>");
	           
	            
	            while (rstRead.next())
	            {
	                
	                //SqlTotalCount="select count(*) as TotalNumberOfInvoices from invoicedetails group by batchid,brand_Group,outputformat having batchid="+ rstRead.getInt("BatchID") +" and brand_group='"+ rstRead.getString("Brand_Group") +"' and outputformat='"+rstRead.getString("OutputFormat")+"';";
	                // changed the display from total count to custmer batch count
	                SqlCustBatchCount="select count(*) as TotalNumberOfInvoices,brand_Group,outputformat,TransportMethod,BatchID from CustomerBatchDetails group by brand_Group,outputformat,TransportMethod,BatchID having BatchID="+ rstRead.getInt("BatchID") ;
	                SqlCount="Select count(*) as NumberofInvoices from InvoiceDetails group by BatchID,Brand_Group,OutputFormat,TransportMethod,Ready,Sent,Status having BatchID="+ rstRead.getInt("BatchID") +" and Brand_Group='"+ rstRead.getString("Brand_Group") +"' and OutputFormat='"+rstRead.getString("OutputFormat")+"' and Sent='Y';";

	                stmSelectCount= conDB1.createStatement();
	                stmSelect=conDB1.createStatement();
	                rstReadCount=stmSelectCount.executeQuery(SqlCount);
	                rstReadTotalCount=stmSelect.executeQuery(SqlCustBatchCount);

	                int TotalCount=0,tempCount=0;

	                while (rstReadCount.next())
	                {
	                    tempCount=rstReadCount.getInt("NumberofInvoices");
	                }
	                rstReadCount.close();
	                while (rstReadTotalCount.next())
	                {
	                    TotalCount=rstReadTotalCount.getInt("TotalNumberOfInvoices");
	                }
	                rstReadTotalCount.close();
	                double temp= (double)tempCount/(double)TotalCount ;
	                percentage= Math.round(temp * (double)100);
	                
	                strmResponse.println("<tr>");
	                strmResponse.println("<td>");
	                strmResponse.println("<input id=\"chkBatchStatusID\" value="+rstRead.getInt("BatchStatusID")+" type=\"checkbox\"></td>");
	                strmResponse.println("<td>");
	                strmResponse.println("<select id=\"selectPrinter\">");
	                strmResponse.println("<option>Printer 1</option>");
	                strmResponse.println("</select>");
	                strmResponse.println("</td>");
	                strmResponse.println("<td>"+rstRead.getDate("DateSubmitted")+" "+ rstRead.getTime("DateSubmitted") +  "</td>");
	                strmResponse.println("<td>"+rstRead.getInt("BatchID")+"</td>");
	                strmResponse.println("<td>"+rstRead.getString("Brand_Group")+"</td>");
	                strmResponse.println("<td>"+rstRead.getString("OutputFormat")+"</td>");
	                strmResponse.println("<td>"+rstRead.getString("TransportMethod")+"</td>");
	                strmResponse.println("<td>" + rstRead.getString("status") + " ("+ (int)percentage + "%)</td>");
	                strmResponse.println("<td>"+rstRead.getInt("NumberofInvoices")+"</td>");
	                strmResponse.println("<td>"+TotalCount+"</td>");
	                strmResponse.println("</tr>");
	               
	            }
	           strmResponse.println("</table>");
	           
	           strmResponse.println("<br /><br />");
	           
	                   
	           strmResponse.println("<table style=\"width:450px\" align=\"center\"><tr><td>");
	           
	           
	            strmResponse.println("<input id=\"btn\" name=\"btnStart\" type=\"button\" onClick=\"javascript:startAction()\" value=\"Start\" />");
	           
	           
	           strmResponse.println("<input id=\"btn\" name=\"btnPause\" type=\"button\"  onClick=\"javascript:pauseAction()\" value=\"Pause\" />");
	           
	           strmResponse.println("<input id=\"btn\" name=\"btnResume\" type=\"button\"  onClick=\"javascript:resumeAction()\"  value=\"Resume\" />");
	           
	           strmResponse.println("<input id=\"btn\" name=\"btnCacel\" type=\"button\"  onClick=\"javascript:cancelAction()\"  value=\"Cancel\" />");
	           
	           strmResponse.println("</td></tr></table>");
	           
	           strmResponse.println("<form name=\"BatchStatusMonitor\" method=\"POST\" action=\"batchStatusMonitor\">");
	           strmResponse.println("<input type=hidden name=\"sId\" value=\"\">");
	           strmResponse.println("<input type=hidden name=\"sAction\" value=\"\">");
	           strmResponse.println("<input type=hidden name=\"sPrinter\" value=\"\">");
	           strmResponse.println("</form>");
	           
	           
	           strmResponse.println("</body>");
	     
	           strmResponse.println("</form>");
	           
	           
	          strmResponse.println("</html>");
	        }
	        catch (SQLException ex)
	        {
	             strmResponse.println(ex.getMessage());
	        }
	        catch (Exception ex)
	        {
	             strmResponse.println(ex.getMessage());
	        }
	    }

	  

	      
	    private boolean checkStatus(HttpServletRequest request, HttpServletResponse response) throws SQLException,ServletException, IOException 
	    {
	        boolean status=false;
	        Connection conDB = null;      
	        
	        try
	        {
	                String [] sIds;
	                String sql="";
	                String sId=request.getParameter("sId")==null?"0":request.getParameter("sId").toString();
	                //DataSource dataSource=getInvoiceRunbyAdmin();
	                conDB = OpenConnection();
	                sIds=sId.split(",");
	          
	                Statement stmUpdate;
	                ResultSet rstRead;
	                Statement stmSelect;
	                String tempStatus="";
	                for(int i=0;i<sIds.length;i++)
	                {
	                     sql="select * from BatchStatus where BatchStatusID="+sIds[i]+"";
	                     conDB = OpenConnection();
	                     stmSelect= conDB.createStatement();
	                     rstRead=stmSelect.executeQuery(sql);	

	                     while (rstRead.next())
	                     {

	                         if(tempStatus =="")
	                         {
	                            tempStatus=rstRead.getString("status");
	                         }


	                         if (rstRead.getString("status").equals(tempStatus))
	                         {
	                             status=false;
	                         }
	                         else
	                         {
	                             status=true;
	                         }
	                     }
	                }
	                
	        }
	        
	        catch(Exception ex)
	        {
	            
	        }
	        
	        finally
	        {
	             conDB.close();
	        }  
	                  
	        return status; 
	    }
	    
	    
	    
	   protected void vShowErrorPage(HttpServletResponse rspOutput,Exception Error)
	        throws ServletException, IOException
	    {
	        rspOutput.setContentType("text/html");
	        ServletOutputStream strmResponse = rspOutput.getOutputStream();
	        strmResponse.println("<HTML><BODY>");
	        strmResponse.println("<FONT COLOR=\"Red\">" +
	                             "<H1>Application Error</H1></FONT><BR>");
	        strmResponse.println(Error.getMessage()+"<BR>");
	        //strmResponse.println(getCustomStackTrace(Error)+"<BR>");
	        strmResponse.println(Error.getClass().toString()+"<BR>");
	        strmResponse.println("</BODY></HTML>");
	        strmResponse.close();        
	    }
	    protected void vShowErrorPage(HttpServletResponse rspOutput,String Error)
	        throws ServletException, IOException
	    {
	        rspOutput.setContentType("text/html");
	        ServletOutputStream strmResponse = rspOutput.getOutputStream();
	        strmResponse.println("<HTML><BODY>");
	        strmResponse.println("<FONT COLOR=\"Red\">" +
	                             "<H1>Application Error</H1></FONT><BR>");
	        strmResponse.println(Error + "<BR>");
	        strmResponse.println("</BODY></HTML>");
	        strmResponse.close();        
	    }

	     public Connection OpenConnection() throws SQLException
	    {

	      Connection conDB = null;
	      
	   
	      try
	      {
	          // Opening connection to database by registering the driver.
	          ResourceBundle bdl = new PropertyResourceBundle(new FileInputStream(getLocalDirName() + "/Config.properties"));
	          
	         Class.forName(bdl.getString("sJDBCDriver")).newInstance();
	         conDB  = DriverManager.getConnection(bdl.getString("sJDBCurl")+"user="+bdl.getString("sMySqlUser")+"&password="+bdl.getString("sMySqlPassword"));

	      }
	      catch (Exception errOpen)
	      {
	          throw new SQLException("<B>Error opening database connection</B><BR>" +
	                          errOpen.toString());
	      }
	        
	      return conDB;
	    }

	    public String getClassName()
	   {
	      String thisClassName;     

	      //Build a string with executing class's name
	      thisClassName = this.getClass().getName();
	      thisClassName = thisClassName.substring(thisClassName.lastIndexOf(".") + 1,thisClassName.length());
	      thisClassName += ".class";  //this is the name of the bytecode file that is executing     

	      return thisClassName;
	   }
	    
	   public  String getLocalDirName()
	   {
	      String localDirName;     

	      //Use that name to get a URL to the directory we are executing in
	      java.net.URL myURL = this.getClass().getResource(getClassName());  //Open a URL to the our .class file     

	      //Clean up the URL and make a String with absolute path name
	      localDirName = myURL.getPath();  //Strip path to URL object out
	      
	      localDirName = myURL.getPath().replaceAll("%20", " ");  //change %20 chars to spaces     

	      //Get the current execution directory
	      localDirName = localDirName.substring(0,localDirName.lastIndexOf("/"));  //clean off the file name     

	      return localDirName;
	   }

}
