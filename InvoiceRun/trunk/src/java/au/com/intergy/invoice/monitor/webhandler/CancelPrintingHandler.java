package au.com.intergy.invoice.monitor.webhandler;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import au.com.intergy.invoice.common.BatchGroup;
import au.com.intergy.invoice.common.BatchGroupDataAccess;
import au.com.intergy.invoice.common.CustomerBatch;
import au.com.intergy.invoice.common.CustomerBatchDataAccess;
import au.com.intergy.invoice.common.DBAccessException;
import au.com.intergy.invoice.common.InvoiceManager;

public class CancelPrintingHandler extends AbstractBatchGroupHander {

	public String getName() {
		return Handler.CANCEL_PRINT_ACTION;
	}
	
	public Map<String, Object> handle(HttpServletRequest request, HttpServletResponse response) 
	throws Exception {

		String[] selectedBGroups = getSelectedBatchGroups(request);
		
		if (selectedBGroups == null || selectedBGroups.length <= 0) {
			request.setAttribute("errorMessage", "Please select a batch to cancel printing.");
		} else {
			List<BatchGroup> bgroups = BatchGroupDataAccess.findBatchGroupByIds(selectedBGroups);
			String validationMsg = validate(bgroups);

			if ( validationMsg != null) {
				request.setAttribute("errorMessage", validationMsg);
			} else {
				InvoiceManager.cancelBatchGroups(selectedBGroups, getNewPrinterNames(request, bgroups));
			}
		}
		
		return super.handle(request, response);
	}
	
	/**
	 * @return null if there is no error.
	 */
	private String validate(List<BatchGroup> bgroups) throws DBAccessException {
		for (BatchGroup group : bgroups) {
			String status = group.getStatus();
			
			if (status.equals(BatchGroup.Status.COMPLETED.name())) {
				return "You cannot cancel a completed batch group, see batch with id " + group.getBatchId();
			}
		}
		return null;
	}
}
