/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.intergy.invoice.monitor.webhandler;

import au.com.intergy.invoice.common.BatchDataAccess;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author adnann
 */
public class DisplayBatchInfoHandler extends AbstractBatchGroupHander {

	public String getName() {
		return Handler.DISPLAY_RUNINFO_ACTION;
	}

	public Map<String, Object> handle(HttpServletRequest request, HttpServletResponse response) 
	throws Exception {
		String batchId = request.getParameter("batchId");
		Map<String, Object> result = new HashMap<String, Object>();

		result.put(VIEW_NAME, "batchinfo.jsp");
		
//		request.setAttribute("description", BatchDataAccess.findDescriptionByBatchId(batchId));
                request.setAttribute("batchDetails", BatchDataAccess.findBatchInfoByBatchId(batchId));
		return result;
	}
}