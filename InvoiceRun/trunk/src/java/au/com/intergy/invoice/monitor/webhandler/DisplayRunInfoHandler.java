package au.com.intergy.invoice.monitor.webhandler;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import au.com.intergy.invoice.common.BatchDataAccess;

public class DisplayRunInfoHandler extends AbstractBatchGroupHander {

	public String getName() {
		return Handler.DISPLAY_RUNINFO_ACTION;
	}

	public Map<String, Object> handle(HttpServletRequest request, HttpServletResponse response) 
	throws Exception {
		String batchId = request.getParameter("batchId");
                String runId = request.getParameter("runId");
		Map<String, Object> result = new HashMap<String, Object>();

		result.put(VIEW_NAME, "runinfo.jsp");
		
//		request.setAttribute("description", BatchDataAccess.findDescriptionByBatchId(batchId));
                request.setAttribute("batchDetails", BatchDataAccess.findDescriptionByBatchId(batchId, runId));
		return result;
	}
}
