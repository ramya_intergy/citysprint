package au.com.intergy.invoice.monitor.webhandler;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ViewBatchGroupsHandler extends AbstractBatchGroupHander {

	public String getName() {
		return Handler.VIEW_BATCHGROUP_ACTION;
	}

	public Map<String, Object> handle(HttpServletRequest request, HttpServletResponse response)
    throws Exception {
		request.setAttribute("errorMessage", request.getSession().getAttribute("errorMessage"));
		return super.handle(request, response);
	}
}
