package au.com.intergy.invoice.printing;

import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.ServiceUI;
import javax.print.SimpleDoc;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;

import jcifs.smb.SmbFile;

public class PrinterDiscovery {

	
	public static String[] getAvailablePrinters() {
		PrintService[] services = PrintServiceLookup.lookupPrintServices( null, null ); 
		String[] printerNames = null;

		if (services == null) 
			return null;

		printerNames = new String[services.length];

		for (int i = 0; i < services.length; i++) {
			printerNames[i] = services[i].getName();
		}
		
		return printerNames;
	}
	
	public static void main(String args[]) {
		
		String filename = "C:/Chi/workspace/Invoice/test/generated/INV_50778_2008_09_04_134013.pdf";
		PrintRequestAttributeSet pras =
			new HashPrintRequestAttributeSet();
		DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
		PrintService printService[] =
			PrintServiceLookup.lookupPrintServices(flavor, pras);
		PrintService defaultService =
			PrintServiceLookup.lookupDefaultPrintService();
		if (defaultService != null)
			System.out.println(defaultService.getName());
		//PrintService service = ServiceUI.printDialog(null, 200, 200,
		//		printService, defaultService, flavor, pras);
		
		PrintService service = defaultService;
		
		if (service != null) {
			try {
				DocPrintJob job = service.createPrintJob();
				FileInputStream fis = new FileInputStream(filename);
				DocAttributeSet das = new HashDocAttributeSet();
				Doc doc = new SimpleDoc(fis, flavor, das);
				job.print(doc, pras);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		/*
		try {
			PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
			pras.add(new Copies(1));

			PrintService pss[] = PrintServiceLookup.lookupPrintServices(null, null);

			if (pss.length == 0) {
				System.out.println("No printer services available.");

				System.exit(-1);
			}
			PrintService ps = null;
			for (PrintService service : pss) {
				System.out.println(service.getName());
				if (service.getName().equals("\\\\BERNIE\\IP")) {
					ps = service;
				}
			}


			if (ps != null ) {
				System.out.println("Printing to " + ps);

				DocPrintJob job = ps.createPrintJob();

				FileInputStream fin = new FileInputStream("C:/Chi/workspace/Invoice/test/generated/INV_50778_2008_09_04_134013.pdf");
				Doc doc = new SimpleDoc(fin, DocFlavor.INPUT_STREAM.POSTSCRIPT, null);

				job.print(doc, pras);

				fin.close();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		
		 /*   
		try {
			//findAllPrinters();
			
			
			
			
			PrintService[] services = PrintServiceLookup.lookupPrintServices( null, null ); 
			
			for (PrintService service : services) {
				System.out.println(service.getName());
				if (service.getName().equals("\\\\BERNIE\\IP")) {
					service.createPrintJob();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}*/
	}
	
	private static void findAllPrinters() throws Exception {
		SmbFile root = new SmbFile("smb://domain;USRASS@domain");
		Hashtable printerHash = new Hashtable();

		searchForPrinters(root, printerHash);

		Enumeration keys = printerHash.keys();
		System.out.println("Number of Printers Found: " +
				printerHash.size());
		SmbFile file = null;
		while (keys.hasMoreElements()) {
			file = ((SmbFile)keys.nextElement());
			System.out.println("UNC: " + file.getUncPath());
		}
	}

	private static void searchForPrinters(SmbFile root, Hashtable
			printers) throws Exception {
		SmbFile[] kids= null;

		try {
			kids = root.listFiles();
		} catch (Exception e) {

		}
		if (kids == null)
			return;

		for (int i = 0; i < kids.length; i++) {
			if (kids[i].getType() == SmbFile.TYPE_WORKGROUP ||
					kids[i].getType() == SmbFile.TYPE_SERVER)
				searchForPrinters(kids[i], printers);
			else if (kids[i].getType() == SmbFile.TYPE_PRINTER) {
				printers.put(kids[i], Boolean.TRUE);
			}
		}
	}

}
