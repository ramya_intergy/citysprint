package au.com.intergy.invoice.webclient;

import au.com.intergy.invoice.common.Configuration;
import au.com.intergy.invoice.common.FileUtil;
import au.com.intergy.invoice.webservice.InvoiceWS;
import au.com.intergy.invoice.webservice.InvoiceWebServices;

/**
 * 
 * @author Chi Bui
 * @since  August 2008
 */
public class InvoiceServicesClient {
	private static final Configuration conf = Configuration.getInstance();

	public static void main(String[] args) {
	
		InvoiceWebServices service = new InvoiceWebServices();
		InvoiceWS port  = service.getInvoiceWSPort();

		try {
			String file = FileUtil.readFile(conf.getString("xml.file"), " ");

			//String result = port.getXML(file);
			String result = port.generateBatch("intergy", "1", "test batch", file);
			System.out.println(result);
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
	}
}
