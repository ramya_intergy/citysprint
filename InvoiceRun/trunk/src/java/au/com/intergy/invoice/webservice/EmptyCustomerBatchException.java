package au.com.intergy.invoice.webservice;

public class EmptyCustomerBatchException extends Exception {
	private static final long serialVersionUID = 24854573993L;
	
	public EmptyCustomerBatchException() {
		super();
	}
	
	public EmptyCustomerBatchException(String message) {
		super(message);
	}
}
