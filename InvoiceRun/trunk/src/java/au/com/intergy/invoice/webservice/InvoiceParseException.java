package au.com.intergy.invoice.webservice;

public class InvoiceParseException extends Exception {
	private static final long serialVersionUID = 4154854573993L;
	public InvoiceParseException(String message) {
		super(message);
	}
}
