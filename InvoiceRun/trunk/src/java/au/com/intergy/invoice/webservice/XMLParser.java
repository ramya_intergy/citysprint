package au.com.intergy.invoice.webservice;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import au.com.intergy.invoice.common.Configuration;
import au.com.intergy.invoice.common.CustomerBatch;
import au.com.intergy.invoice.common.Statement;
import au.com.intergy.invoice.common.StringUtils;
import au.com.intergy.invoice.common.TransportMethod;
import au.com.intergy.invoice.common.Invoice;
import au.com.intergy.invoice.common.CustomerBatch.Status;
import au.com.intergy.invoice.common.Logger;

/**
 * Provides utility methods to parse XML documents specifically for the invoice
 * project.
 *
 */
public class XMLParser {

    private final static String XSLT_FILES_DIR = Configuration.getInstance().getString("xslt.dir");
    private static Logger logger = new Logger();

    public static List<CustomerBatch> parse(String xml)
            throws InvoiceParseException,
            InvalidInvoiceDataException,
            EmptyCustomerBatchException,
            InvalidOutputFileFormatException {
        List<CustomerBatch> custBatchesResult = new ArrayList<CustomerBatch>();
        // part of the original xml content which has not yet been parsed.
        String unprocessedXMLContent = xml;

        try {

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
            Document doc = db.parse(bais);
            doc.getDocumentElement().normalize();

            String xsdName = "";

            NodeList nodeLstxsd = doc.getElementsByTagName("invoicelist");
            Node nodexsd = nodeLstxsd.item(0);
            if (nodexsd.getNodeType() == Node.ELEMENT_NODE) {

                Element elexsd = (Element) nodexsd;
                xsdName = elexsd.getAttribute("xsi:noNamespaceSchemaLocation");
                if (xsdName.isEmpty()) {
                    throw new InvalidInvoiceDataException("No schema location provided.");
                }
            }

            //go through each customer batch
            NodeList nodeLstCustBatchDet = doc.getElementsByTagName("custBatch");
            for (int i = 0; i < nodeLstCustBatchDet.getLength(); i++) {
                Node custBatchNode = nodeLstCustBatchDet.item(i);

                if (custBatchNode.getNodeType() == Node.ELEMENT_NODE) {
                    CustomerBatch custBatch = new CustomerBatch();

                    custBatchesResult.add(custBatch);

                    Element fstElmnt = (Element) custBatchNode;

                    custBatch.setBrand(fstElmnt.getAttribute("company"));
                    custBatch.setCustomerKey(fstElmnt.getAttribute("customerKey"));

                    try {
                        setTransportDetails(fstElmnt, custBatch);
                    } catch (IllegalArgumentException iae) {
                        throw new InvalidInvoiceDataException("Transport method can only be PRINT, EMAIL, FTP , FTPS or SFTP.");
                    }

                    //validate transport details
                    //Ramya 10/06/2018 Ignoring Stylesheet checking if format is INVOIC
                    //To support multiple EDI output's change request
                    if (!"INVOIC".equals(custBatch.getOutputFileFormat())) {
                        if (!new File(XSLT_FILES_DIR + "/" + custBatch.getXsltFileName()).exists()) {
                            throw new InvalidInvoiceDataException("Stylesheet " + custBatch.getXsltFileName()
                                    + " does not exist for customer " + custBatch.getCustomerKey() + ".");
                        }
                    }
                    custBatch.setStatus(Status.INIT);

                    //statement
                    Statement statement = getStatement(fstElmnt);
                    if (statement != null) {
                        custBatch.setStatement(statement);

                        int x = unprocessedXMLContent.indexOf("<statement ");
                        int y = unprocessedXMLContent.indexOf("</statement>");

                        String stmtBody = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
                        stmtBody = stmtBody + "<invoicelist xsi:noNamespaceSchemaLocation=\"" + xsdName + "\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";

                        stmtBody = stmtBody + unprocessedXMLContent.substring(x, y);
                        stmtBody = stmtBody + "</statement>";
                        stmtBody = stmtBody.replace("'", "''") + "</invoicelist>";
                        stmtBody = stmtBody.replace("\t", "");

                        custBatch.getStatement().setXmlBody(stmtBody);
                        //TODO assume the statement appears before all the invoices
                        unprocessedXMLContent = unprocessedXMLContent.substring(y + 1, unprocessedXMLContent.length());

                    }

                    //invoices
                    CustBatchInvoices custBatchInv = getInvoices(fstElmnt, unprocessedXMLContent, xsdName);
                    custBatch.setInvoices(custBatchInv.getInvoices());
                    unprocessedXMLContent = custBatchInv.getUnprocessedXMLContent();

                    //validate transport method
                    if (custBatch.getTransportMethod() == null) {
                        throw new InvalidInvoiceDataException("Transport method cannot be empty.");
                    }

                    if (custBatch.getTransportMethod() == TransportMethod.EMAIL) {
                        if (custBatch.getCreditControllerEmail() == null) {
                            throw new InvalidInvoiceDataException("No credit controller email provided for customer "
                                    + custBatch.getCustomerKey() + ".");

                        }
                        if (StringUtils.isEmpty(custBatch.getTransportAddress())) {
                            throw new InvalidInvoiceDataException("No transport address provided to email customer "
                                    + custBatch.getCustomerKey() + ".");
                        }
                    } else if (custBatch.getTransportMethod() == TransportMethod.FTP || custBatch.getTransportMethod() == TransportMethod.FTPS) {
                        if (StringUtils.isEmpty(custBatch.getTransportAddress())
                                || StringUtils.isEmpty(custBatch.getTransportUsername())
                                || StringUtils.isEmpty(custBatch.getTransportPassword())) {
                            throw new InvalidInvoiceDataException("FTP address, username and password cannot be empty for customer "
                                    + custBatch.getCustomerKey() + ".");
                        }
                    }else if(custBatch.getTransportMethod() == TransportMethod.SFTP){
                        if (StringUtils.isEmpty(custBatch.getTransportAddress())
                                || StringUtils.isEmpty(custBatch.getTransportUsername())
                                && (StringUtils.isEmpty(custBatch.getTransportKeyfile()) && StringUtils.isEmpty(custBatch.getTransportPassword()))) {
                            throw new InvalidInvoiceDataException("SFTP address, username, password / keyfile cannot be empty for customer "
                                    + custBatch.getCustomerKey() + ".");
                        }
                    }

                    //validate output file format
                    if (custBatch.isEmpty()) {
                        throw new EmptyCustomerBatchException();
                    }
                    if (custBatch.getTransportMethod() == TransportMethod.PRINT
                            && !"PDF".equals(custBatch.getOutputFileFormat())) {
                        throw new InvalidOutputFileFormatException("Only PDF format is accepted for PRINT customer batches.");
                    } else if (!"PDF".equals(custBatch.getOutputFileFormat())
                            && !"TIFF".equals(custBatch.getOutputFileFormat())
                            && !"XML".equals(custBatch.getOutputFileFormat())
                            && !"INVOIC".equals(custBatch.getOutputFileFormat())) { //ADNAN, added against Amazon EDI format @06/06/2011
                        throw new InvalidOutputFileFormatException("Invalid output file format " + custBatch.getOutputFileFormat());
                    }
                }
            }
        } catch (ParserConfigurationException pce) {
            throw new InvoiceParseException("Error in parsing " + pce.toString());
        } catch (IOException ioe) {
            throw new InvoiceParseException("Error in parsing " + ioe.toString());
        } catch (SAXException se) {
            throw new InvoiceParseException("Error in parsing " + se.toString());
        }

        return custBatchesResult;
    }

    static void setTransportDetails(Element e, CustomerBatch custBatch) {
        NodeList TpElmntLst = e.getElementsByTagName("TransportDetails");
        Node fstNodeTp = TpElmntLst.item(0);
        Element fstElmntTp = (Element) fstNodeTp;
        logger.log("In set transport details");
        //xslt file name
        NodeList styleSheetNameElmntLst = fstElmntTp.getElementsByTagName("StyleSheet");
        Element styleSheetNameElmnt = (Element) styleSheetNameElmntLst.item(0);
        NodeList styleSheetName = styleSheetNameElmnt.getChildNodes();
        custBatch.setXsltFileName(((Node) styleSheetName.item(0)).getNodeValue());

        //transport method
        NodeList transportMethodElmntLst = fstElmntTp.getElementsByTagName("method");
        Element transportMethodElmnt = (Element) transportMethodElmntLst.item(0);
        NodeList tpMethod = transportMethodElmnt.getChildNodes();
        String transportMethod = ((Node) tpMethod.item(0)) == null ? null : ((Node) tpMethod.item(0)).getNodeValue();
        logger.log("transportmethod:" + transportMethod);
        logger.log("TransportMethod.valueOf(transportMethod):" + TransportMethod.valueOf(transportMethod));
        custBatch.setTransportMethod(
                transportMethod != null ? TransportMethod.valueOf(transportMethod) : null);

        //transport address
        NodeList transportAddElmntLst = fstElmntTp.getElementsByTagName("address");
        Element transportAddElmnt = (Element) transportAddElmntLst.item(0);
        NodeList transportAdd = transportAddElmnt.getChildNodes();

        if (transportAdd.item(0) != null
                && !StringUtils.isEmpty(((Node) transportAdd.item(0)).getNodeValue())) {
            custBatch.setTransportAddress(((Node) transportAdd.item(0)).getNodeValue());
        }

        //transport username
        NodeList transportUNameElmntLst = fstElmntTp.getElementsByTagName("username");
        Element transportUNameElmnt = (Element) transportUNameElmntLst.item(0);
        NodeList transportUName = transportUNameElmnt.getChildNodes();

        if (transportUName.item(0) != null) {
            custBatch.setTransportUsername(((Node) transportUName.item(0)).getNodeValue());
        }

        //transport password
        NodeList transportPWDElmntLst = fstElmntTp.getElementsByTagName("password");
        Element transportPWDElmnt = (Element) transportPWDElmntLst.item(0);
        NodeList transportPWD = transportPWDElmnt.getChildNodes();

        if (transportPWD.item(0) != null) {
            custBatch.setTransportPassword(((Node) transportPWD.item(0)).getNodeValue());
        }

        NodeList outputElmntLst = fstElmntTp.getElementsByTagName("FileFormat");
        Element outputElmnt = (Element) outputElmntLst.item(0);
        NodeList output = outputElmnt.getChildNodes();
        custBatch.setOutputFileFormat(((Node) output.item(0)).getNodeValue().toUpperCase());
        logger.log("In set transport details 2");
        try {
            NodeList schOutputElmntLst = fstElmntTp.getElementsByTagName("ScheduleFileFormat");
            Element schOutputElmnt = (Element) schOutputElmntLst.item(0);
            NodeList schOutput = schOutputElmnt.getChildNodes();
            custBatch.setScheduleOutputFormat(((Node) schOutput.item(0)).getNodeValue().toUpperCase());
        } catch (Exception ex) {
            logger.log("No ScheduleFileFormat");
            logger.log(ex.getMessage());
            ex.printStackTrace();
        }

        //TransportDetails
//                emailTemplate 
        try {
            NodeList emailTempElmntLst = fstElmntTp.getElementsByTagName("emailTemplate");
            Element emailTempElmnt = (Element) emailTempElmntLst.item(0);
            NodeList emailTemplate = emailTempElmnt.getChildNodes();
            custBatch.setTransportEmailTemplate(((Node) emailTemplate.item(0)).getNodeValue());
        } catch (Exception ex) {
            logger.log("No emailTemplate");
            logger.log(ex.getMessage());
            ex.printStackTrace();
        }

        //SFTP Changes
        try {
            NodeList portElmntLst = fstElmntTp.getElementsByTagName("port");
            Element portElmnt = (Element) portElmntLst.item(0);
            NodeList port = portElmnt.getChildNodes();
            custBatch.setTransportPort(Integer.parseInt(((Node) port.item(0)).getNodeValue()));
        } catch (Exception ex) {
            logger.log("No port");
            logger.log(ex.getMessage());
            ex.printStackTrace();
        }


        try {
            NodeList keyfileElmntLst = fstElmntTp.getElementsByTagName("keyfile");
            Element keyfileElmnt = (Element) keyfileElmntLst.item(0);
            NodeList keyfile = keyfileElmnt.getChildNodes();
            custBatch.setTransportKeyfile(((Node) keyfile.item(0)).getNodeValue());
        } catch (Exception ex) {
            logger.log("No keyfile");
            logger.log(ex.getMessage());
            ex.printStackTrace();
        }
        logger.log("Completed setTransportDetails");
    }

    private static Statement getStatement(Element e) throws InvalidInvoiceDataException{
        NodeList stmtElmntLst = e.getElementsByTagName("statement");
        Element stmtInvElmnt = (Element) stmtElmntLst.item(0);

        if (stmtInvElmnt == null) {
            return null;
        }

        Statement stmtResult = new Statement();
        
        try{
            String printFlag = stmtInvElmnt.getAttribute("printFlag");
            stmtResult.setPrintFlag(printFlag == null ? null : printFlag);
        } catch (NullPointerException npe) {
                            throw new InvalidInvoiceDataException(getMissingErrorPage("printFlag", "Didn't get that"));
        }
        try{
            stmtResult.setLastPrintItem(Boolean.valueOf(stmtInvElmnt.getAttribute("lastPrintItem")));
         } catch (NullPointerException npe) {
                            throw new InvalidInvoiceDataException(getMissingErrorPage("lastPrintItem", "Didn't get that"));
        }
        
        try{
                NodeList stmtInvNoElmntLst = stmtInvElmnt.getElementsByTagName("invAccount");
                Node fstNodeStmtInv = stmtInvNoElmntLst.item(0);
           
             try{
                Element fstStmtInvElmnt = (Element) fstNodeStmtInv;
                NodeList stmtAccNoElmntLst = fstStmtInvElmnt.getElementsByTagName("accNo");
                Element stmtAccNoElmnt = (Element) stmtAccNoElmntLst.item(0);
                NodeList stmtCustomerNo = stmtAccNoElmnt.getChildNodes();
                if (stmtCustomerNo.item(0) != null) {
                        stmtResult.setAccountNumber(((Node) stmtCustomerNo.item(0)).getNodeValue());
                }
                 
            } catch (NullPointerException npe) {
                                throw new InvalidInvoiceDataException(getMissingErrorPage("accNo", "Didn't get that"));
            }
         } catch (NullPointerException npe) {
                            throw new InvalidInvoiceDataException(getMissingErrorPage("invAccount", stmtResult.getAccountNumber()));
        }
        
       
        
        //&& !((Node) stmtCustomerNo.item(0)).getNodeValue().trim().isEmpty()
       
        try{

            NodeList stmtInvCCElmntLst = stmtInvElmnt.getElementsByTagName("invCreditcontroller");
            Node fstNodeStmtCC = stmtInvCCElmntLst.item(0);
            Element fstStmtCCElmnt = (Element) fstNodeStmtCC;

            try{
                NodeList stmtCCNameElmntLst = fstStmtCCElmnt.getElementsByTagName("creName");
                Element stmtCCNameElmnt = (Element) stmtCCNameElmntLst.item(0);
                NodeList stmtCCName = stmtCCNameElmnt.getChildNodes();
                if (stmtCCName.item(0) != null) {
                    stmtResult.setCreditControllerName(((Node) stmtCCName.item(0)).getNodeValue());
                }
            } catch (NullPointerException npe) {
                                throw new InvalidInvoiceDataException(getMissingErrorPage("creName", stmtResult.getAccountNumber()));
            }

            try{
                NodeList stmtCCPhoneElmntLst = fstStmtCCElmnt.getElementsByTagName("crePhone");
                Element stmtCCPhoneElmnt = (Element) stmtCCPhoneElmntLst.item(0);
                NodeList stmtCCPhone = stmtCCPhoneElmnt.getChildNodes();
                if (stmtCCPhone.item(0) != null) {
                    stmtResult.setCreditControllerPhone(((Node) stmtCCPhone.item(0)).getNodeValue());
                }
            } catch (NullPointerException npe) {
                    throw new InvalidInvoiceDataException(getMissingErrorPage("crePhone", stmtResult.getAccountNumber()));
            }

            try{
                NodeList stmtCCEmailElmntLst = fstStmtCCElmnt.getElementsByTagName("creEmail");
                Element stmtCCEmailElmnt = (Element) stmtCCEmailElmntLst.item(0);
                NodeList stmtCCEmail = stmtCCEmailElmnt.getChildNodes();
                if (stmtCCEmail.item(0) != null) {
                    stmtResult.setCreditControllerEmail(((Node) stmtCCEmail.item(0)).getNodeValue());
                }
            } catch (NullPointerException npe) {
                    throw new InvalidInvoiceDataException(getMissingErrorPage("creEmail", stmtResult.getAccountNumber()));
            }

            try{
                NodeList stmtCCTitleElmntLst = fstStmtCCElmnt.getElementsByTagName("creTitle");
                Element stmtCCTitleElmnt = (Element) stmtCCTitleElmntLst.item(0);
                NodeList stmtTitleEmail = stmtCCTitleElmnt.getChildNodes();
                if (stmtTitleEmail.item(0) != null) {
                    stmtResult.setCreditControllerTitle(((Node) stmtTitleEmail.item(0)).getNodeValue());
                }

                System.out.println("#################### Title :" + stmtResult.getCreditControllerTitle());
            } catch (NullPointerException npe) {
                    throw new InvalidInvoiceDataException(getMissingErrorPage("creTitle", stmtResult.getAccountNumber()));
            }

            try{
                NodeList stmtCCOfficeElmntLst = fstStmtCCElmnt.getElementsByTagName("creOffice");
                Element stmtCCOfficeElmnt = (Element) stmtCCOfficeElmntLst.item(0);
                NodeList stmtOfficeEmail = stmtCCOfficeElmnt.getChildNodes();
                if (stmtOfficeEmail.item(0) != null) {
                    stmtResult.setCreditControllerOffice(((Node) stmtOfficeEmail.item(0)).getNodeValue());
                }

                System.out.println("#################### Office :" + stmtResult.getCreditControllerOffice());
            } catch (NullPointerException npe) {
                    throw new InvalidInvoiceDataException(getMissingErrorPage("creOffice", stmtResult.getAccountNumber()));
            }

            try{
                NodeList stmtCCFaxElmntLst = fstStmtCCElmnt.getElementsByTagName("creFaxno");
                Element stmtCCFaxElmnt = (Element) stmtCCFaxElmntLst.item(0);
                NodeList stmtCCFax = stmtCCFaxElmnt.getChildNodes();
                if (stmtCCFax.item(0) != null) {
                    stmtResult.setCreditControllerFaxNo(((Node) stmtCCFax.item(0)).getNodeValue());
                }

                System.out.println("#################### Fax No :" + stmtResult.getCreditControllerFaxNo());
            } catch (NullPointerException npe) {
                    throw new InvalidInvoiceDataException(getMissingErrorPage("creFaxno", stmtResult.getAccountNumber()));
            }
        
        } catch (NullPointerException npe) {
                 throw new InvalidInvoiceDataException(getMissingErrorPage("invCreditcontroller", stmtResult.getAccountNumber()));
        }
        return stmtResult;
    }
    private static final String START_TAG_ESCAPE_CHARS = "<";
    private static final String END_TAG_ESCAPE_CHARS = ">";

    private static String getMissingErrorPage(String tagName, String accountNo) {

        StringBuffer missingTagMsg = new StringBuffer("");
        if (tagName != null) {
            missingTagMsg.append(START_TAG_ESCAPE_CHARS);
            missingTagMsg.append(tagName);
            missingTagMsg.append(END_TAG_ESCAPE_CHARS);

            missingTagMsg.append(" is missing or empty, please fix this issue and then try again. Account#: " + accountNo);
        }

        return missingTagMsg.toString();
    }

    private static CustBatchInvoices getInvoices(Element e, String xml, String xsdName)
            throws InvalidInvoiceDataException {
        Set<Invoice> invListResult = new LinkedHashSet<Invoice>();
        try {
            NodeList nodeLst = e.getElementsByTagName("invoice");
            String unprocessedXMLContent = xml;
            try {
                for (int index = 0; index < nodeLst.getLength(); index++) {

                    Node node = nodeLst.item(index);
                    Invoice invoice = new Invoice();

                    invoice.setSequenceNumber(index + 1);
                    invListResult.add(invoice);

                    if (node.getNodeType() == Node.ELEMENT_NODE) {

                        Element eleInvno = (Element) node;
                        try {
                            invoice.setPrintFlag(eleInvno.getAttribute("printFlag"));
                        } catch (NullPointerException npe) {
                            throw new InvalidInvoiceDataException(getMissingErrorPage("printFlag", "Didn't get that"));
                        }

                        try {
                            invoice.setBrand(eleInvno.getAttribute("company"));
                        } catch (NullPointerException npe) {
                            throw new InvalidInvoiceDataException(getMissingErrorPage("company", "Didn't get that"));
                        }

                        try {
                            invoice.setLastPrintItem(Boolean.valueOf(eleInvno.getAttribute("lastPrintItem")));
                        } catch (NullPointerException npe) {
                            throw new InvalidInvoiceDataException(getMissingErrorPage("lastPrintItem", "Didn't get that"));
                        }
                    }

                    Element fstInvElmnt = (Element) node;

                    try {
                        NodeList nodeLstInvtDet = fstInvElmnt.getElementsByTagName("invAccount");
                        Node fstNodeInv = nodeLstInvtDet.item(0);
			Element fstInvAcctElmnt = (Element) fstNodeInv;

                            try{
                                    NodeList CustNumElmntLst = fstInvAcctElmnt.getElementsByTagName("accNo");
                                    Element custNumElmnt = (Element) CustNumElmntLst.item(0);
                                    NodeList CustNum = custNumElmnt.getChildNodes();
                                    if (CustNum.item(0) != null) {
                                        invoice.setAccountNumber(((Node) CustNum.item(0)).getNodeValue());
                                    }
                                }catch(NullPointerException npe){
                                   throw new InvalidInvoiceDataException(  getMissingErrorPage( "accNo", "Didn't get that"));
                                }
                       
                           try{
                                    NodeList CustNameElmntLst = fstInvAcctElmnt.getElementsByTagName("accName");
                                    Element custNameElmnt = (Element) CustNameElmntLst.item(0);
                                    NodeList CustName = custNameElmnt.getChildNodes();
                                    if (CustName.item(0) != null) {
                                        invoice.setAccountName(((Node) CustName.item(0)).getNodeValue());
                                    }
                            }catch(NullPointerException npe){
                                   throw new InvalidInvoiceDataException(  getMissingErrorPage( "accName", invoice.getAccountNumber()));
                            }
                           
                           try{
                        NodeList CustAddElmntLst = fstInvAcctElmnt.getElementsByTagName("accAddr1");
                        Element custAddElmnt = (Element) CustAddElmntLst.item(0);
                        NodeList custAdd = custAddElmnt.getChildNodes();
                        if (custAdd.item(0) != null) {
                            invoice.setAccountAddr1(((Node) custAdd.item(0)).getNodeValue());
                        }   
                        }catch(NullPointerException npe){
                                   throw new InvalidInvoiceDataException(  getMissingErrorPage( "accAddr1", invoice.getAccountNumber()));
                        }
                           
                           try{
                        NodeList CustAdd2ElmntLst = fstInvAcctElmnt.getElementsByTagName("accAddr2");
                        Element custAdd2Elmnt = (Element) CustAdd2ElmntLst.item(0);
                        NodeList custAdd2 = custAdd2Elmnt.getChildNodes();
                        if (custAdd2.item(0) != null) {
                            invoice.setAccountAddr2(((Node) custAdd2.item(0)).getNodeValue());
                        }
                        
                         }catch(NullPointerException npe){
                                   throw new InvalidInvoiceDataException(  getMissingErrorPage( "accAddr2", invoice.getAccountNumber()));
                        }
                        
                           try{
                        NodeList CustAdd3ElmntLst = fstInvAcctElmnt.getElementsByTagName("accAddr3");
                        Element custAddr3Elmnt = (Element) CustAdd3ElmntLst.item(0);
                        NodeList custAddr3 = custAddr3Elmnt.getChildNodes();
                        if (custAddr3.item(0) != null) {
                            invoice.setAccountAddr3(((Node) custAddr3.item(0)).getNodeValue());
                        }

                        }catch(NullPointerException npe){
                                   throw new InvalidInvoiceDataException(  getMissingErrorPage( "accAddr3", invoice.getAccountNumber()));
                        }
                           
                           try{
                        NodeList custAddr4ElmntLst = fstInvAcctElmnt.getElementsByTagName("accAddr4");
                        Element custAddr4Elmnt = (Element) custAddr4ElmntLst.item(0);
                        NodeList custAddr4 = custAddr4Elmnt.getChildNodes();
                        if (custAddr4.item(0) != null) {
                            invoice.setAccountAddr4(((Node) custAddr4.item(0)).getNodeValue());
                        }

                        }catch(NullPointerException npe){
                                   throw new InvalidInvoiceDataException(  getMissingErrorPage( "accAddr4", invoice.getAccountNumber()));
                        }
                           
                           try{
                        NodeList custAddr5ElmntLst = fstInvAcctElmnt.getElementsByTagName("accAddr5");
                        Element custAddr5Elmnt = (Element) custAddr5ElmntLst.item(0);
                        NodeList custAddr5 = custAddr5Elmnt.getChildNodes();
                        if (custAddr5.item(0) != null) {
                            invoice.setAccountAddr5(((Node) custAddr5.item(0)).getNodeValue());
                        }

                        }catch(NullPointerException npe){
                                   throw new InvalidInvoiceDataException(  getMissingErrorPage( "accAddr5", invoice.getAccountNumber()));
                        }
                           
                           try{
                        NodeList custAddr6ElmntLst = fstInvAcctElmnt.getElementsByTagName("accAddr6");
                        Element custAddr6Elmnt = (Element) custAddr6ElmntLst.item(0);
                        NodeList custAddr6 = custAddr6Elmnt.getChildNodes();
                        if (custAddr6.item(0) != null) {
                            invoice.setAccountAddr6(((Node) custAddr6.item(0)).getNodeValue());
                        }
                        
                        }catch(NullPointerException npe){
                                   throw new InvalidInvoiceDataException(  getMissingErrorPage( "accAddr6", invoice.getAccountNumber()));
                        }
                           
                    } catch (NullPointerException npe) {
                        throw new InvalidInvoiceDataException(getMissingErrorPage("invAccount", invoice.getAccountNumber()));
                    }
                    try{
                    NodeList InvCCElmntLst = fstInvElmnt.getElementsByTagName("invCreditcontroller");
                    Node fstNodeInvCC = InvCCElmntLst.item(0);
                    Element fstInvCCElmnt = (Element) fstNodeInvCC;

                    try{
                    NodeList invCCNameElmntLst = fstInvCCElmnt.getElementsByTagName("creName");
                    Element invCCNameElmnt = (Element) invCCNameElmntLst.item(0);
                    NodeList invCCName = invCCNameElmnt.getChildNodes();
                    if (invCCName.item(0) != null && ((Node) invCCName.item(0)).getNodeValue().length() > 1) {
                        invoice.setCreditControllerName(((Node) invCCName.item(0)).getNodeValue());
                    }

                     } catch (NullPointerException npe) {
                        throw new InvalidInvoiceDataException(getMissingErrorPage("creName", invoice.getAccountNumber()));
                    }
                    
                    try{
                    NodeList invCCPhoneElmntLst = fstInvCCElmnt.getElementsByTagName("crePhone");
                    Element invCCPhoneElmnt = (Element) invCCPhoneElmntLst.item(0);
                    NodeList invCCPhone = invCCPhoneElmnt.getChildNodes();
                    if (invCCPhone.item(0) != null && (((Node) invCCPhone.item(0)).getNodeValue().length() > 1)) {
                        invoice.setCreditControllerPhone(((Node) invCCPhone.item(0)).getNodeValue());
                    }

                     } catch (NullPointerException npe) {
                        throw new InvalidInvoiceDataException(getMissingErrorPage("crePhone", invoice.getAccountNumber()));
                    }
                    
                    try{
                    NodeList invCCEmailElmntLst = fstInvCCElmnt.getElementsByTagName("creEmail");
                    Element invCCEmailElmnt = (Element) invCCEmailElmntLst.item(0);
                    NodeList invCCEmail = invCCEmailElmnt.getChildNodes();

                    if (invCCEmail.item(0) != null && ((Node) invCCEmail.item(0)).getNodeValue().length() > 1) {
                        invoice.setCreditControllerEmail(((Node) invCCEmail.item(0)).getNodeValue());
                    }

                    } catch (NullPointerException npe) {
                        throw new InvalidInvoiceDataException(getMissingErrorPage("creEmail", invoice.getAccountNumber()));
                    }

                    try{
                    NodeList invCCTitleElmntLst = fstInvCCElmnt.getElementsByTagName("creTitle");
                    Element invCCTitleElmnt = (Element) invCCTitleElmntLst.item(0);
                    NodeList invCCTitle = invCCTitleElmnt.getChildNodes();
                    if (invCCTitle.item(0) != null && (((Node) invCCTitle.item(0)).getNodeValue().length() > 1)) {
                        invoice.setCreditControllerTitle(((Node) invCCTitle.item(0)).getNodeValue());
                    }

                    System.out.println("#################### Title From Invoice :" + invoice.getCreditControllerTitle());
                    } catch (NullPointerException npe) {
                        throw new InvalidInvoiceDataException(getMissingErrorPage("creTitle", invoice.getAccountNumber()));
                    }
                    
                    try{
                    NodeList invCCOfficeElmntLst = fstInvCCElmnt.getElementsByTagName("creOffice");
                    Element invCCOfficeElmnt = (Element) invCCOfficeElmntLst.item(0);
                    NodeList invCCOffice = invCCOfficeElmnt.getChildNodes();
                    if (invCCOffice.item(0) != null && (((Node) invCCOffice.item(0)).getNodeValue().length() > 1)) {
                        invoice.setCreditControllerOffice(((Node) invCCOffice.item(0)).getNodeValue());
                    }

                    System.out.println("#################### Office From Invoice:" + invoice.getCreditControllerOffice());

                    } catch (NullPointerException npe) {
                        throw new InvalidInvoiceDataException(getMissingErrorPage("creOffice", invoice.getAccountNumber()));
                    }
                    
                    try{
                    NodeList invCCFaxElmntLst = fstInvCCElmnt.getElementsByTagName("creFaxno");
                    Element invCCFaxElmnt = (Element) invCCFaxElmntLst.item(0);
                    NodeList invCCFax = invCCFaxElmnt.getChildNodes();
                    if (invCCFax.item(0) != null && (((Node) invCCFax.item(0)).getNodeValue().length() > 1)) {
                        invoice.setCreditControllerFaxNo(((Node) invCCFax.item(0)).getNodeValue());
                    }

                    System.out.println("#################### FaxNo From Invoice:" + invoice.getCreditControllerFaxNo());

                    } catch (NullPointerException npe) {
                        throw new InvalidInvoiceDataException(getMissingErrorPage("creFaxno", invoice.getAccountNumber()));
                    }
                    
                    }catch(NullPointerException npe){
                                   throw new InvalidInvoiceDataException(  getMissingErrorPage( "invCreditcontroller", invoice.getAccountNumber()));
                        }

                    try{
                    NodeList objCatNodes = fstInvElmnt.getElementsByTagName("invNo");
                    Node objNode = objCatNodes.item(0);
                    NodeList objNodes = objNode.getChildNodes();
                    invoice.setInvoiceNumber(objNodes.item(0).getNodeValue());
                    if (invoice.getInvoiceNumber().isEmpty()) {
                        throw new InvalidInvoiceDataException("No invoice number provided");
                    }

                    }catch(NullPointerException npe){
                                   throw new InvalidInvoiceDataException(  getMissingErrorPage( "invNo", invoice.getAccountNumber()));
                        }
            
                    try{
                    NodeList objInvTotalNodes = fstInvElmnt.getElementsByTagName("invTotal");
                    Node objInvTotalNode = objInvTotalNodes.item(0);
                    NodeList invTotalNodes = objInvTotalNode.getChildNodes();
                    invoice.setInvoiceTotal(invTotalNodes.item(0).getNodeValue());

                    }catch(NullPointerException npe){
                                   throw new InvalidInvoiceDataException(  getMissingErrorPage( "invTotal", invoice.getAccountNumber()));
                    }
                    
                    int x = unprocessedXMLContent.indexOf("<invoice ");
                    int y = unprocessedXMLContent.indexOf("</invoice>");

                    String invoiceBody = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
                    invoiceBody = invoiceBody + "<invoicelist xsi:noNamespaceSchemaLocation=\""
                            + xsdName + "\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
                    invoiceBody = invoiceBody + unprocessedXMLContent.substring(x, y) + "</invoice>";
                    invoiceBody = invoiceBody.replace("'", "''") + "</invoicelist>";
                    invoiceBody = invoiceBody.replace("\t", "");
                    invoice.setXmlBody(invoiceBody);
                    //unprocessedXMLContent becomes smaller
                    unprocessedXMLContent = unprocessedXMLContent.substring(y + 1, unprocessedXMLContent.length());
                }
            } catch (NullPointerException npe) {
                System.out.println("################### XML parsing error inside getInvoices() ###################");

                npe.printStackTrace();
                throw new InvalidInvoiceDataException(npe.getMessage());
            }
            CustBatchInvoices custBatchInv = new CustBatchInvoices();
            try {
                custBatchInv.setInvoices(invListResult);
                custBatchInv.setUnprocessedXMLContent(unprocessedXMLContent);
            } catch (NullPointerException npe) {
                System.out.println("################### XML parsing error inside getInvoices() PART II ###################");

                npe.printStackTrace();
                throw new InvalidInvoiceDataException(npe.getMessage());
            }

            return custBatchInv;

        } catch (InvalidInvoiceDataException ex) {

            throw ex;
        }

    }

    /**
     * Encapsulates a set of invoices for a customer batch and the unprocessed
     * XML content.
     */
    private static class CustBatchInvoices {

        private Set<Invoice> invoices;
        private String unprocessedXMLContent;

        Set<Invoice> getInvoices() {
            return invoices;
        }

        void setInvoices(Set<Invoice> invoices) {
            this.invoices = invoices;
        }

        String getUnprocessedXMLContent() {
            return unprocessedXMLContent;
        }

        void setUnprocessedXMLContent(String unprocessedXMLContent) {
            this.unprocessedXMLContent = unprocessedXMLContent;
        }
    }
}
