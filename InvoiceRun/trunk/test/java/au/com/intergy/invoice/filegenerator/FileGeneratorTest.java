package au.com.intergy.invoice.filegenerator;

import au.com.intergy.invoice.common.Configuration;
import au.com.intergy.invoice.common.FileUtil;
import au.com.intergy.invoice.common.InvoiceDataAccess;
import junit.framework.TestCase;

public class FileGeneratorTest extends TestCase {

	private static final Configuration conf = Configuration.getInstance();
	
	public void testGenerateFile() {
		FileGenerator generator = new FileGenerator();
		
		try {

			//String file = InvoiceDataAccess.getXMLBodyById(new Long(25));
			String file = FileUtil.readFile("C:/Chi/workspace/InvoiceIssues/wr1152/inv34015.txt", " ");
			//System.out.println(file);
			
			if (file == null)
				fail("cant read xmlbody for invoice id 4");
			String xsltDir = "C:/Chi/workspace/InvoiceIssues/wr1152";
			String xsltFilePath = xsltDir.concat("/").concat("citySprint.xslt");
			
			
			String generatedFileDir = "C:/Chi/workspace/InvoiceIssues/wr1152";

			//_13:32:18
			//System.out.println("Current dir "  + System.getProperty("user.dir") );
			generator.generatePDF(generatedFileDir.concat("/").concat("INV_34015.pdf"), 
					xsltFilePath, file);
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unable to generate file " + e.toString());
		}
		
	}
}
