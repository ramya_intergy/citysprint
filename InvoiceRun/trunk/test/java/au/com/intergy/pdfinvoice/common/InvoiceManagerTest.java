package au.com.intergy.pdfinvoice.common;

import java.util.HashMap;
import java.util.Map;

import au.com.intergy.invoice.common.DBAccessException;
import au.com.intergy.invoice.common.InvoiceDataAccess;
import au.com.intergy.invoice.common.InvoiceManager;
import au.com.intergy.invoice.common.Statement;
import junit.framework.TestCase;

public class InvoiceManagerTest extends TestCase {

	public void testStartBatchGroups() {
		Map<Long, String> printerNames = new HashMap<Long, String>();
		String[] ids = {"107"};
		printerNames.put(new Long("107"), "Test Printer");
		try {
			InvoiceManager.startBatchGroups(ids, printerNames);
			
			Statement inv = InvoiceDataAccess.findById(new Long("2683"));
			if (!inv.getStatus().equals("INIT"))
				fail(inv.getStatus());
		} catch (DBAccessException dae) {
			dae.printStackTrace();
			fail("Not yet implemented");
		}
	}

}
