<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<html>
    <head>
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
        <META HTTP-EQUIV="Expires" CONTENT="-1">
        <META HTTP-EQUIV="Refresh" CONTENT="60; URL=monitor">
        <META NAME="version" CONTENT="RC21"> 
        <title>
            Invoice batches
        </title>
        <jsp:useBean id="batchGroups" scope="session" class="java.util.ArrayList" />
        <jsp:useBean id="completedBatchGroups" scope="session" class="java.util.ArrayList" />
        <jsp:useBean id="printers" scope="session" class="java.util.ArrayList" />
        <link rel="stylesheet" href="./css/invoice.css">

        <script language=JavaScript>
            
            function SetAllCheckBoxes(FormName, FieldName, CheckValue)
            {
                //var selectAll = CheckValue;
			
                if(!document.forms[FormName])
                    return;
                var objCheckBoxes = document.forms[FormName].elements[FieldName];
                if(!objCheckBoxes)
                    return;
					
                var countCheckBoxes = objCheckBoxes.length;
                if(!countCheckBoxes)
                    objCheckBoxes.checked = CheckValue;
                else
			
                // set the check value for all check boxes
                    for(var i = 0; i < countCheckBoxes; i++)
                        objCheckBoxes[i].checked = CheckValue;
            }
                        
            function submitForm(actionValue) {
                document.MonitorForm.action.value=actionValue;
                document.MonitorForm.submit();
            }
            function submitCompletedForm(actionValue) {
                document.CompletedMonitorForm.action.value=actionValue;
                document.CompletedMonitorForm.submit();
            }

            function popupRunInfo(mylink, windowname)
            {
    
                if (! window.focus)return true;
                var href;
                if (typeof(mylink) == 'string')
                    href=mylink;
                else
                    href=mylink.href;
                window.open(href, windowname, 'width=' + (screen.width - 10) + ',height='+ (screen.height -10) + ' ,scrollbars=yes,resizable=yes,toolbar=false,menubar=false');
                return false;
                //                window.open('your_url', 'popup_name','height=' + screen.height + ',width=' + screen.width + ',resizable=yes,scrollbars=yes,toolbar=yes,menubar=yes,location=yes')
            }

            function popup(mylink, windowname)
            {
                if (! window.focus)return true;
                var href;
                if (typeof(mylink) == 'string')
                    href=mylink;
                else
                    href=mylink.href;
                //                window.open(href, windowname, 'width=400,height=200,scrollbars=yes');
                window.open(href, windowname, 'width=' + (screen.width - 10) + ',height='+ (screen.height -10) + ' ,scrollbars=yes,resizable=yes,toolbar=false,menubar=false');
                return false;
            }

        </script>

    </head>
    <body>

        <table border="0" cellpadding="0" cellspacing="0" width="100%"> 

            <tr>
                <td align="center" bgcolor="white">
                    <br/>
                    <h3>Invoice Batches Monitor</h3>
                </td>
            </tr>

            <tr>
                <td bgcolor="white" align="right"><a href="./stylesheet">[Stylesheets]</a></td>
            </tr>
            <tr>
                <td bgcolor="white">&nbsp;<font color="red"><c:out value="${sessionScope.errorMessage}"/></font></td>
            </tr>

            <tr>
                <td>
                    <form name="MonitorForm" method="post" action="./monitor">
                        <input type="hidden" name="action" value=""/> 
                        <table width="100%" class="box">
                            <tr>
                                <td class="boxheading1" colspan="13">Current Batch Groups</td>
                            </tr>

                            <tr> 
                                <td colspan="12" align="center">
                                    <input class="btn" type="button" value="Start" name="btnStart" onClick="submitForm('startPrinting')" style="width: 80px"/> &nbsp; 
                                    <input class="btn" type="button" value="Pause" name="btnPause" onClick="submitForm('pausePrinting')" style="width: 80px"/> &nbsp;
                                    <input class="btn" type="button" value="Resume" name="btnResume" onClick="submitForm('resumePrinting')" style="width: 80px"/> &nbsp;
                                    <input class="btn" type="button" value="Cancel" name="btnCancel" onClick="submitForm('cancelPrinting')" style="width: 80px"/> 
                                </td>
                            </tr> 

                            <tr class="boxheading2"> 
                                <td> <input type="checkbox" id="selectAllBatches" name="selectAllBatches" value="-2" onclick="SetAllCheckBoxes('MonitorForm', 'selectBatchGroup', this.checked);"> </td>
                                <td> Select Printer </td>
                                <td> Created At&nbsp;</td>
                                <td> User &nbsp;</td>
                                <td> Run &nbsp;</td>
                                <td> Batch&nbsp;</td>
                                <td> Brand&nbsp;</td>
                                <td> Invoice<br> Format&nbsp;</td>
                                <td> Schedule<br> Format&nbsp;</td>
                                <td> Transport Method&nbsp;</td>
                                <td> Status&nbsp;</td>
                                <td> Customer Batch Count</td>
                                <td> Invoice Count</td>
                            </tr>       

                            <c:forEach var="group" items="${batchGroups}" varStatus="status">
                                <tr> 

                                    <td><input type="checkbox" id="selectBatchGroup" name="selectBatchGroup" value="${group.id}"></td>
                                    <td> 

                                        <c:choose>
                                            <c:when test='${not empty printers}'>
                                                <select name="printer_${group.id}" <c:if test="${'STARTED' eq group.status}">disabled</c:if>>
                                                    <c:forEach var="printer" items="${printers}">
                                                        <option value="${printer}" <c:if test="${printer eq group.selectedPrinter}">selected</c:if>>
                                                            <c:out value='${printer}'/>
                                                        </option>
                                                    </c:forEach>
                                                </select>
                                            </c:when>
                                            <c:otherwise>
                                                <c:out value="No Printers"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td width="100"><fmt:formatDate pattern="dd-MMM-yyyy HH:mm:ss z" value="${group.submittedDate}" /> </td>
                                    <td> <c:out value="${group.userId}"/></td>
                                    <td><a href="./monitor?action=viewRunInfo&runId=<c:out value='${group.runId}'/>&batchId=<c:out value='${group.batchId}'/>" 
                                           onClick="return popupRunInfo(this, 'parameters')">
                                            <c:out value="${group.runId}"/></a></td>
                                    <td>
                                        <a href="./monitor?action=viewBatchInfo&batchId=<c:out value='${group.batchId}'/>" 
                                           onClick="return popup(this, 'parameters')"> 
                                            <c:out value="${group.batchId}"/>
                                        </a>
                                    </td>
                                    <td> <c:out value="${group.brand}"/></td>
                                    <td> <c:out value="${group.outputFileFormat}"/></td>
                                    <td> <c:out value="${group.scheduleOutputFormat}"/></td>
                                    <td width="70"> <c:out value="${group.transportMethod}"/></td>

                                    <c:choose>
                                        <c:when test="${group.status eq 'STARTED'}">
                                            <c:set var="statusClass" value="startstatus" scope="request"/>
                                        </c:when>
                                        <c:when test="${group.status eq 'FAILED'}">
                                            <c:set var="statusClass" value="failedstatus" scope="request"/>
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="statusClass" value="" scope="request"/>
                                        </c:otherwise>
                                    </c:choose>
                                    <td class="<c:out value='${statusClass}'/>"><c:out value="${group.status}"/></td>
                                    <td width="70">  <c:out value="${group.custBatchCount}"/></td>
                                    <td width="70">  <c:out value="${group.invoiceCount}"/></td>
                                </tr>   
                            </c:forEach>

                            <tr> 
                                <td colspan="12" align="center">
                                    <input class="btn" type="button" value="Start" name="btnStart" onClick="submitForm('startPrinting')" style="width: 80px"/> &nbsp; 
                                    <input class="btn" type="button" value="Pause" name="btnPause" onClick="submitForm('pausePrinting')" style="width: 80px"/> &nbsp;
                                    <input class="btn" type="button" value="Resume" name="btnResume" onClick="submitForm('resumePrinting')" style="width: 80px"/> &nbsp;
                                    <input class="btn" type="button" value="Cancel" name="btnCancel" onClick="submitForm('cancelPrinting')" style="width: 80px"/> 
                                </td>
                            </tr> 
                        </table> 
                    </form> 
                </td>
            </tr>

            <!-- Space -->
            <tr>
                <td>&nbsp;</td>
            </tr>


            <tr>
                <td>

                    <table width="100%" class="box">
                        <tr>
                            <td class="boxheading1">Completed Batch Groups</td>
                        </tr>
                        <tr>
                            <td>

                                <c:choose>
                                    <c:when test='${not empty completedBatchGroups}'>

                                        <form name="CompletedMonitorForm" method="post" action="./monitor">
                                            <input type="hidden" name="action" value=""/> 
                                            <table width="100%" class="box">
                                                <!--  
                                                  <tr> 
                                                      <td colspan="10" align="center">
                                                          <input class="btn" type="button" value="Initialise" name="btnInit" onClick="submitCompletedForm('initialisePrinting')" style="width: 80px"/> &nbsp; 
                                                      </td>
                                                  </tr> 
                                                -->
                                                <tr class="boxheading2"> 
                                                    <td> <input type="checkbox" id="selectAllBatches" name="selectAllBatches" value="-2" onclick="SetAllCheckBoxes('CompletedMonitorForm', 'selectBatchGroup', this.checked);"> </td>
                                                    <td> Select Printer </td>
                                                    <td> Created At&nbsp;</td>
                                                    <td> User&nbsp;</td>
                                                    <td> Run&nbsp;</td>
                                                    <td> Batch&nbsp;</td>
                                                    <td> Brand&nbsp;</td>
                                                    <td> Invoice<br>Format&nbsp;</td>
                                                    <td> Schedule<br>Format&nbsp;</td>
                                                    <td> Transport Method&nbsp;</td>
                                                    <td> Status&nbsp;</td>
                                                    <td> Customer Batch Count</td>
                                                    <td> Invoice Count</td>
                                                </tr>       
                                                <c:forEach var="group" items="${completedBatchGroups}" varStatus="status">
                                                    <tr> 

                                                        <td><input type="checkbox" id="selectBatchGroup" name="selectBatchGroup" value="${group.id}"></td>
                                                        <td> 

                                                            <c:choose>
                                                                <c:when test='${not empty printers}'>
                                                                    <select name="printer_${group.id}" <c:if test="${'STARTED' eq group.status}">disabled</c:if>>
                                                                        <c:forEach var="printer" items="${printers}">
                                                                            <option value="${printer}" <c:if test="${printer eq group.selectedPrinter}">selected</c:if>>
                                                                                <c:out value='${printer}'/>
                                                                            </option>
                                                                        </c:forEach>
                                                                    </select>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <c:out value="No Printers"/>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </td>
                                                        <td width="100"> <fmt:formatDate pattern="dd-MMM-yyyy HH:mm:ss z" value="${group.submittedDate}" /> </td>
                                                        <td> <c:out value="${group.userId}"/></td>
                                                        <td><a href="./monitor?action=viewRunInfo&runId=<c:out value='${group.runId}'/>&batchId=<c:out value='${group.batchId}'/>" 
                                                               onClick="return popup(this, 'parameters')">
                                                                <c:out value="${group.runId}"/></a></td>
                                                        <td>
                                                            <a href="./monitor?action=viewBatchInfo&batchId=<c:out value='${group.batchId}'/>" 
                                                               onClick="return popup(this, 'parameters')"> 
                                                                <c:out value="${group.batchId}"/>
                                                            </a>
                                                        </td>
                                                        <td> <c:out value="${group.brand}"/></td>
                                                        <td> <c:out value="${group.outputFileFormat}"/> </td>
                                                        <td> <c:out value="${group.scheduleOutputFormat}"/> </td>
                                                        <td width="70"><c:out value="${group.transportMethod}"/></td>

                                                        <c:choose>
                                                            <c:when test="${group.status eq 'STARTED'}">
                                                                <c:set var="statusClass" value="startstatus" scope="request"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:set var="statusClass" value="" scope="request"/>
                                                            </c:otherwise>
                                                        </c:choose>
                                                        <td class="<c:out value='${statusClass}'/>"><c:out value="${group.status}"/></td>
                                                        <td width="70">  <c:out value="${group.custBatchCount}"/></td>
                                                        <td width="70">  <c:out value="${group.invoiceCount}"/></td>
                                                    </tr>   
                                                </c:forEach>

                                                <tr> 
                                                    <td colspan="10" align="center">
                                                        <input class="btn" type="button" value="Initialise" name="btnInit" onClick="submitCompletedForm('initialisePrinting')" style="width: 80px"/> &nbsp; 
                                                    </td>
                                                </tr> 
                                            </table> 
                                        </form> 


                                    </c:when>
                                    <c:otherwise>
                                        There is no completed batch.
                                    </c:otherwise>
                                </c:choose>


                            </td>
                        </tr>
                    </table>


                    </body>

                    </html>