<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<html>
<head>
  <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
  <META HTTP-EQUIV="Expires" CONTENT="-1">
  <META HTTP-EQUIV="Refresh" CONTENT="30; URL=stylesheet">
  <title>
    Invoice stylesheets
  </title>

<link rel="stylesheet" href="./css/invoice.css">
<script type="text/javascript" language="JavaScript">
function check() {
  var ext = document.UploadForm.newStylesheet.value;
  ext = ext.substring(ext.length-4,ext.length);
  ext = ext.toLowerCase();
  if(ext != 'xslt') {
    alert('Please select a .xslt file instead!');
    return false; }
  else
    return true; }
</script>
<jsp:useBean id="currentStylesheets" scope="session" class="java.util.ArrayList" />
</head>
<body>

<table border="0" cellpadding="0" cellspacing="0" width="100%"> 

<tr>
  <td align="center" bgcolor="white">
   <br/>
   <h3>Invoice Stylesheets</h3>
  </td>
</tr>
<tr>
  <td bgcolor="white" align="right"><a href="./monitor">[Monitor]</a></td>
</tr>
<tr>
  <td bgcolor="white">&nbsp;<font color="red"><c:out value="${sessionScope.errorMsg}"/></font></td>
</tr>

<tr>
  <td>
   <form name="UploadForm" method="post" enctype="multipart/form-data"
 onsubmit="return check();" action="./stylesheet">
   
   <table width="100%" class="box">
     <tr>
      <td class="boxheading1">Upload File</td>
     </tr>
     <tr>

       <td>
         <input type="file" name="newStylesheet" size="80">
       </td>
     </tr>
      <tr>
       <td>
         <input type="submit" class="btn" name="btnUpload" value="Upload"/>
       </td>
     </tr>
  </table> 
  </form> 
    </td>
  </tr>

   <!-- Space -->
   <tr>
     <td>&nbsp;</td>
   </tr>
  
   <tr>
   <td>
   <table width="100%" class="box">
     <tr> 
       <td class="boxheading1">Available stylesheets</td>
     </tr>

     <c:forEach var="stylesheet" items="${currentStylesheets}" varStatus="status">
     <tr>
       <td>
        &nbsp;<c:out value='${stylesheet}'/>
       </td>
     </tr>
     </c:forEach>
   </table>
   </td>
  </tr>
</table>
                         
</body>

</html>