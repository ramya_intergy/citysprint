// InfoBinderInterface.java 
// This file contains generated code and will be overwritten when you rerun code generation.

package com.altovaNHS.typeinfo;

public interface InfoBinderInterface
{
	NamespaceInfo[] getNamespaces();
	TypeInfo[] getTypes();
	MemberInfo[] getMembers();
}