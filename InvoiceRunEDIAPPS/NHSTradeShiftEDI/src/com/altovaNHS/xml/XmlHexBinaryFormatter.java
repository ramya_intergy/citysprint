// XmlHexBinaryFormatter.java
// This file contains generated code and will be overwritten when you rerun code generation.

package com.altovaNHS.xml;

public class XmlHexBinaryFormatter extends XmlFormatter
{
	public String format(byte[] v)
	{
		return com.altovaNHS.HexBinary.encode(v);
	}

	public byte[] parseBinary(String s)
	{
		return com.altovaNHS.HexBinary.decode(s);
	}
}