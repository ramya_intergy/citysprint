// XmlTimeFormatter.java
// This file contains generated code and will be overwritten when you rerun code generation.

package com.altovaNHS.xml;

import com.altovaNHS.types.DateTime;

public class XmlTimeFormatter extends XmlFormatter
{
	public String format(DateTime dt)
	{
		return dt.toTimeString("0.0######");
	}
}
