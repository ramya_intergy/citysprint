// TextIntegerFormatter.java
// This file contains generated code and will be overwritten when you rerun code generation.

package com.altovaTP.text;

import com.altovaTP.CoreTypes;

class TextIntegerFormatter extends TextFormatter
{
	public String format(double v)
	{
		return CoreTypes.castToString((long) v);
	}
}
