////////////////////////////////////////////////////////////////////////
//
// Table.java
//
// This file was generated by MapForce 2018sp1.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the MapForce Documentation for further details.
// http://www.altova.com/mapforce
//
////////////////////////////////////////////////////////////////////////

package com.altovaTP.text.tablelike.csv;

import com.altovaTP.text.tablelike.ISerializer;

public class Table extends com.altovaTP.text.tablelike.Table
{
	private int m_lineEnd = 0;
	
	public Table(com.altovaTP.typeinfo.TypeInfo tableType, int lineEnd)
	{
		super(false);
		this.tableType = tableType;
		this.m_lineEnd = lineEnd;
		init();
	}

	public Format getFormat() { return ((Serializer) m_Serializer).getFormat(); }

	protected ISerializer createSerializer()
	{
		return new Serializer(this,m_lineEnd);
	}

	protected void initHeader(com.altovaTP.text.tablelike.Header header)
	{
		for( int iMember = 0 ; iMember < tableType.getMembers().length ; ++iMember )
		{
			com.altovaTP.typeinfo.MemberInfo member = tableType.getMembers()[iMember];
			header.add(
				new com.altovaTP.text.tablelike.ColumnSpecification(member.getLocalName()));
		}
	}
}
