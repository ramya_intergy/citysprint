// XmlIntegerFormatter.java
// This file contains generated code and will be overwritten when you rerun code generation.

package com.altovaTP.xml;

import com.altovaTP.CoreTypes;

class XmlIntegerFormatter extends XmlFormatter
{
	public String format(double v)
	{
		return CoreTypes.castToString((long) v);
	}
}
