/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package citysprint.co.uk.NDI;

import java.sql.*;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.*;

/**
 *
 * @author ramyas
 */
public class PDFLabelInfo extends CommonClass implements Serializable {

    private String connote = "";
    private String ccompany = "";
    private String caddress1 = "";
    private String caddress2 = "";
    private String caddress3 = "";
    private String ctown = "";
    private String ccounty = "";
    private String cpostcode = "";
    private String ccountry = "";
    private String carrierdesc = "";
    private String carrierloc = "";
    private String dcompany = "";
    private String daddress1 = "";
    private String daddress2 = "";
    private String daddress3 = "";
    private String dtown = "";
    private String dcounty = "";
    private String dpostcode = "";
    private String dcountry = "";
    private String dcontactname = "";
    private String dcontactphone = "";
    private String ddeliverby = "";
    private String ddeliveron = "";
    private String destinationcode = "";
    private String commodity = "";
    private String weight = "";
    private String totalpieces = "";
    private String instructions = "";
    private String routingcode = "";

    public String getCaddress1() {
        return caddress1;
    }

    public void setCaddress1(String caddress1) {
        this.caddress1 = caddress1;
    }

    public String getCaddress2() {
        return caddress2;
    }

    public void setCaddress2(String caddress2) {
        this.caddress2 = caddress2;
    }

    public String getCaddress3() {
        return caddress3;
    }

    public void setCaddress3(String caddress3) {
        this.caddress3 = caddress3;
    }

    public String getCarrierdesc() {
        return carrierdesc;
    }

    public String getCcountry() {
        return ccountry;
    }

    public void setCcountry(String ccountry) {
        this.ccountry = ccountry;
    }

    public String getDcountry() {
        return dcountry;
    }

    public void setDcountry(String dcountry) {
        this.dcountry = dcountry;
    }

    public void setCarrierdesc(String carrierdesc) {
        this.carrierdesc = carrierdesc;
    }

    public String getCarrierloc() {
        return carrierloc;
    }

    public void setCarrierloc(String carrierloc) {
        this.carrierloc = carrierloc;
    }

    public String getCcompany() {
        return ccompany;
    }

    public void setCcompany(String ccompany) {
        this.ccompany = ccompany;
    }

    public String getCcounty() {
        return ccounty;
    }

    public void setCcounty(String ccounty) {
        this.ccounty = ccounty;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getConnote() {
        return connote;
    }

    public void setConnote(String connote) {
        this.connote = connote;
    }

    public String getCpostcode() {
        return cpostcode;
    }

    public void setCpostcode(String cpostcode) {
        this.cpostcode = cpostcode;
    }

    public String getCtown() {
        return ctown;
    }

    public void setCtown(String ctown) {
        this.ctown = ctown;
    }

    public String getDaddress1() {
        return daddress1;
    }

    public void setDaddress1(String daddress1) {
        this.daddress1 = daddress1;
    }

    public String getDaddress2() {
        return daddress2;
    }

    public void setDaddress2(String daddress2) {
        this.daddress2 = daddress2;
    }

    public String getDaddress3() {
        return daddress3;
    }

    public void setDaddress3(String daddress3) {
        this.daddress3 = daddress3;
    }

    public String getDcompany() {
        return dcompany;
    }

    public void setDcompany(String dcompany) {
        this.dcompany = dcompany;
    }

    public String getDcontactname() {
        return dcontactname;
    }

    public void setDcontactname(String dcontactname) {
        this.dcontactname = dcontactname;
    }

    public String getDcontactphone() {
        return dcontactphone;
    }

    public void setDcontactphone(String dcontactphone) {
        this.dcontactphone = dcontactphone;
    }

    public String getDcounty() {
        return dcounty;
    }

    public void setDcounty(String dcounty) {
        this.dcounty = dcounty;
    }

    public String getDdeliverby() {
        return ddeliverby;
    }

    public void setDdeliverby(String ddeliverby) {
        this.ddeliverby = ddeliverby;
    }

    public String getDdeliveron() {
        return ddeliveron;
    }

    public void setDdeliveron(String ddeliveron) {
        this.ddeliveron = ddeliveron;
    }

    public String getDestinationcode() {
        return destinationcode;
    }

    public void setDestinationcode(String destinationcode) {
        this.destinationcode = destinationcode;
    }

    public String getDpostcode() {
        return dpostcode;
    }

    public void setDpostcode(String dpostcode) {
        this.dpostcode = dpostcode;
    }

    public String getDtown() {
        return dtown;
    }

    public void setDtown(String dtown) {
        this.dtown = dtown;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getRoutingcode() {
        return routingcode;
    }

    public void setRoutingcode(String routingcode) {
        this.routingcode = routingcode;
    }

    public String getTotalpieces() {
        return totalpieces;
    }

    public void setTotalpieces(String totalpieces) {
        this.totalpieces = totalpieces;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public JSONArray getJSONAuditDetails(String session_ID, String ndi_jobno, HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        Connection conShowLabel = null;
        Statement stmtShowLabel = null;
        JSONArray objJSONArray = new JSONArray();
        JSONObject objLabelData = new JSONObject();

        String sqlShowLabel = "";

        sqlShowLabel = "select ndi_accno,ndi_jobno,ndi_consignno,ndi_readyon,ndi_readyat,ndi_pcomp,ndi_paddress,ndi_paddress2,ndi_paddress3,ndi_ptown,ndi_pcounty,ndi_ppostcode,ndi_pcountrycode, "
                + "ndi_pcountry,ndi_dcomp,ndi_daddress,ndi_daddress2,ndi_daddress3,ndi_dtown,ndi_dcounty,ndi_dpostcode,ndi_dcountrycode, "
                + "ndi_dcountry,ndi_dinstruct,ndi_dcontact,ndi_dphone,ndi_ddate,ndi_dtime, ndi_scname, ndi_scaddress, ndi_scaddress2, ndi_sctown, ndi_sccounty, ndi_scpostcode,ndi_sccountrycode, "
                + "ndi_carriername, ndi_routingcode, ndi_destination_label, ndi_prodtype, ndi_totpieces,ndi_totweight from ndi_audit where ndi_sessionid = '" + session_ID + "' and ndi_jobno in ( " + ndi_jobno + " )";


        int i = 0;
        try {
            //initialize config values
            initializeConfigValues(req, res);
            System.out.println("################### PDFLabelInfo: sqlShowLabel == " + sqlShowLabel);
            conShowLabel = cOpenConnection();
            stmtShowLabel = conShowLabel.createStatement();
            ResultSet rstRead = stmtShowLabel.executeQuery(sqlShowLabel);
            while (rstRead.next()) {
                this.connote = rstRead.getString("ndi_consignno");

                //Collection
                this.ccompany = rstRead.getString("ndi_pcomp");
                this.caddress1 = rstRead.getString("ndi_paddress");
                this.caddress2 = rstRead.getString("ndi_paddress2");
                this.caddress3 = rstRead.getString("ndi_paddress3");
                this.ctown = rstRead.getString("ndi_ptown");
                this.ccounty = rstRead.getString("ndi_pcounty");
                this.ccountry = rstRead.getString("ndi_pcountry");
                this.cpostcode = rstRead.getString("ndi_ppostcode");

                //Carrier
                this.carrierdesc = rstRead.getString("ndi_carriername");
                this.carrierloc = rstRead.getString("ndi_sctown");

                //Delivery
                this.dcompany = rstRead.getString("ndi_dcomp");
                this.daddress1 = rstRead.getString("ndi_daddress");
                this.daddress2 = rstRead.getString("ndi_daddress2");
                this.daddress3 = rstRead.getString("ndi_daddress3");
                this.dtown = rstRead.getString("ndi_dtown");
                this.dcounty = rstRead.getString("ndi_dcounty");
                this.dpostcode = rstRead.getString("ndi_dpostcode");
                this.dcountry = rstRead.getString("ndi_dcountry");
                this.dcontactname = rstRead.getString("ndi_dcontact");
                this.dcontactphone = rstRead.getString("ndi_dphone");
                this.ddeliverby = rstRead.getString("ndi_dtime");
                this.ddeliveron = rstRead.getString("ndi_ddate");

                //Common
                this.destinationcode = rstRead.getString("ndi_destination_label");
                this.commodity = rstRead.getString("ndi_prodtype");
                this.totalpieces = rstRead.getString("ndi_totpieces");
                this.weight = rstRead.getString("ndi_totweight");
                this.instructions = rstRead.getString("ndi_dinstruct");
                this.routingcode = rstRead.getString("ndi_routingcode");

                //Build JSON Object
                objJSONArray.put(getJsonObj());
                

            }



        } catch (Exception ex) {
            vShowErrorPageReporting(res, ex);
        }

        return objJSONArray;
    }

    public JSONObject getJsonObj() {
        JSONObject obj = new JSONObject();

        try {
            obj.put("connote", this.connote);
            obj.put("collection", getCollectionJsonObj());
            obj.put("carrier", getCarrierJsonObj());
            obj.put("delivery", getDeliveryJsonObj());
            obj.put("destination-code", this.destinationcode);
            obj.put("commodity", this.commodity);
            obj.put("weight", this.weight);
            obj.put("total-pieces", this.totalpieces);
            obj.put("instructions", this.instructions);
            obj.put("routing-code", this.routingcode);
            //obj.put("extra-page1-copies", 1);
        } catch (JSONException jex) {
        }

        return obj;
    }

    public JSONObject getCarrierJsonObj() {
        JSONObject obj = new JSONObject();

        try {
            obj.put("description", this.carrierdesc);
            obj.put("location", this.carrierloc);
        } catch (JSONException jex) {
        }

        return obj;
    }

    public JSONObject getCollectionJsonObj() {
        JSONObject obj = new JSONObject();

        try {
            obj.put("company", this.ccompany);
            obj.put("address1", this.caddress1);
            obj.put("address2", this.caddress2);
            obj.put("address3", this.caddress3);
            obj.put("town", this.ctown);
            obj.put("county", this.ccounty);
            obj.put("postcode", this.cpostcode);
            obj.put("country", this.ccountry);
        } catch (JSONException jex) {
        }
        return obj;
    }

    public JSONObject getDeliveryJsonObj() {
        JSONObject obj = new JSONObject();

        try {
            obj.put("company", this.dcompany);
            obj.put("address1", this.daddress1);
            obj.put("address2", this.daddress2);
            obj.put("address3", this.daddress3);
            obj.put("town", this.dtown);
            obj.put("county", this.dcounty);
            obj.put("postcode", this.dpostcode);
            obj.put("country", this.dcountry);
            obj.put("contact-name", this.dcontactname);
            obj.put("contact-phone", this.dcontactphone);
            obj.put("deliver-by", this.ddeliverby);
            obj.put("deliver-on", this.ddeliveron);
        } catch (JSONException jex) {
        }
        return obj;
    }
}
