package citysprint.co.uk.NDI;

/*
 * BookingDetails.java
 *
 * Created on 27 March 2008, 10:31
 */
import java.io.*;
//import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
//import javax.sql.DataSource;
//import javax.naming.*;
//import javax.xml.ws.WebServiceRef;
//import webservices2.*;
//import java.util.Vector;
import com.qas.proweb.*;
import com.qas.proweb.servlet.*;
import java.util.Enumeration;

/**
 *
 * @author ramyas
 * @version
 */
public abstract class BookingDetails extends CommonClass {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    // Constants
    final String sFontColour = "#FF0000";
    final String sParam_Mode = "sMode";
    final String sParam_CalledBy = "sCalledBy";
    final String sParam_Mode1 = "sMode1";
    final String sParam_CalledBy1 = "sCalledBy1";
    final String sParam_EditCell = "sEditCell";
    final String sParam_EditCell1 = "sEditCell1";
    final String sParam_NextRowID = "sNextRow";
    final String sParam_NextRowID1 = "sNextRow1";
    final String sParam_DeleteRowID = sParam_EditCell + "3";// "4";
    final String sCalledBy_Add = "Add";
    final String sCalledBy_Edit = "Edit";
    final String sCalledBy_Delete = "Delete";
    final String sCalledBy_Summary = "Summary";
    final String sMode_Add = "Add";
    final String sMode_Edit = "Edit";
    final String sMode_View = "View";
    final String sMode_None = "none";
    final int nRowIDColumn = 4;
//    final int nRowIDColumn = 5;
    // Servlet form variables
    /**
     *
     */
    protected int nColumnCount = 0;
    /**
     *
     */
    protected int nColumnCount1 = 0;
    /**
     *
     */
    protected long lEditingRow = 0;
    /**
     *
     */
    protected String sEditedRow[] = null;
    /**
     *
     */
    protected String sMode = "";
    /**
     *
     */
    protected String sCalledBy = "";
    //For commodity grid
    /**
     *
     */
    protected long lEditingRow1 = 0;
    /**
     *
     */
    protected String sEditedRow1[] = null;
    /**
     *
     */
    protected String sMode1 = "";
    /**
     *
     */
    protected String sCalledBy1 = "";
    protected int itemWeightCount = 0;
    // Abstract methods for ItemsGrid

    /**
     *
     * @return
     */
    protected abstract boolean bAddAllowed();
//    protected static final ParcelInfo parcelInfo = new ParcelInfo();

    /**
     *
     * @param nColumn
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract boolean bEditAllowed(int nColumn) throws IllegalArgumentException;

    /**
     *
     * @param lRow
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract boolean bDeleteAllowed(long lRow) throws IllegalArgumentException;
    //protected abstract String  sSelectSQL();

    /**
     *
     * @return
     */
    protected abstract String sServletName();

    /**
     *
     * @param nColumn
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract String sGetColumnHeader(int nColumn) throws IllegalArgumentException;

    /**
     *
     * @param nColumn
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract int nGetColumnWidth(int nColumn) throws IllegalArgumentException;

    /**
     *
     * @param nColumn
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract int nGetColumnTextWidth(int nColumn) throws IllegalArgumentException;

    /**
     *
     * @param nColumn
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract int nGetColumnMaxLengths(int nColumn) throws IllegalArgumentException;

    /**
     *
     * @param sSessionID
     * @return
     */
    protected abstract boolean bAddRow(String sSessionID);

    /**
     *
     * @param lRow
     * @param sSessionID
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract boolean bEditRow(long lRow, String sSessionID) throws IllegalArgumentException;

    /**
     *
     * @param lRow
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract boolean bDeleteRow(long lRow) throws IllegalArgumentException;

    //Abstract methods for CommodityGrid
    /**
     *
     * @return
     */
    protected abstract boolean bAddAllowed1();

    /**
     *
     * @param nColumn
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract boolean bEditAllowed1(int nColumn) throws IllegalArgumentException;

    /**
     *
     * @param lRow
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract boolean bDeleteAllowed1(long lRow) throws IllegalArgumentException;
    //protected abstract String  sSelectSQL();
    //protected abstract String  sServletName();

    /**
     *
     * @param nColumn
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract String sGetColumnHeader1(int nColumn) throws IllegalArgumentException;

    /**
     *
     * @param nColumn
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract int nGetColumnWidth1(int nColumn) throws IllegalArgumentException;

    /**
     *
     * @param nColumn
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract int nGetColumnTextWidth1(int nColumn) throws IllegalArgumentException;

    /**
     *
     * @param nColumn
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract int nGetColumnMaxLengths1(int nColumn) throws IllegalArgumentException;

    /**
     *
     * @param validSession
     * @return
     */
    protected abstract boolean bAddRow1(HttpSession validSession);

    /**
     *
     * @param lRow
     * @param validSession
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract boolean bEditRow1(long lRow, HttpSession validSession) throws IllegalArgumentException;

    /**
     *
     * @param lRow
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected abstract boolean bDeleteRow1(long lRow) throws IllegalArgumentException;
    //String     sErrorMessage      = "";
    //protected String     message="";
    //protected Connection conDB=null;
    /**
     *
     */
    protected String Error = "";
    /**
     *
     */
    protected boolean bResult = false;
    /**
     *
     */
    protected boolean bResult1 = false;
    /**
     *
     */
    /**
     *
     */
    protected String sAction = "", sAction_sub = "";
    /**
     *
     */
    protected String sTotalValue = "";
    protected String sTotalValueWithIndemnity = "";

    private void logout(HttpServletRequest request, HttpServletResponse response) {
        try {
            initializeConfigValues(request, response);

            //Read session values
            //getSessionValues(request);
            clearSessionTables(request, response);

            HttpSession validSession = request.getSession(false);

            if (validSession != null) {
                try {

                    Enumeration attributeNames = validSession.getAttributeNames();
                    while (attributeNames.hasMoreElements()) {
                        String sAttribute = attributeNames.nextElement().toString();
                        validSession.removeAttribute(sAttribute);
                    }
                } catch (Exception e) {
                }
            }

            validSession.invalidate();

            request.setAttribute("bFlag", true);
        } catch (Exception ex) {
//            ex.printStackTrace();
        }
    }

    private boolean checkUserNPasswordInReq(HttpServletRequest request) {

        String urlUsername = "";
        String urlPassword = "";
        String urlJobNo = "";
        String urlFromDate = "";
        String urlToDate = "";
        String urlAction = "";
        String result = "";
        String urlHAWBNo = "";

//        if (request.getMethod().equals("GET")) {

//            } else if (request.getMethod().equals("POST")) {

        urlUsername = request.getParameter("Username") == null ? "" : request.getParameter("Username").toString();
        urlPassword = request.getParameter("Password") == null ? "" : request.getParameter("Password").toString();
        System.out.println("******************************************************************************************************");
        System.out.println("USERNAME =" + urlUsername);
        System.out.println("PASS =" + urlPassword);
        System.out.println("******************************************************************************************************");
        if (urlUsername != null
                && urlPassword != null
                && !urlUsername.trim().equals("")
                && !urlPassword.trim().equals("")) {

            return true;
        }
//            }

        return false;
    }

    //Process the HTTP request
    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        //first inilialize config values
        initializeConfigValues(request, response);


        Error = "";

        String[][] getCountriesArray = null;
        String[][] getCommodityArray = null;
        String[][] getLoginArray = null;
//        String[][] getNDIParcelArray = null;

        try {
            //check if is a direct URL request else proceed to normal login
            String sUrl = processURLRequest(request, response);

            if (sUrl.length() == 0) {
                // check wheter username and password is present in the coming request.
                if (checkUserNPasswordInReq(request)) {
                    logout(request, response);
                }
                //check whether user logged in already.

                if (!isLogin(request)) {
                    getLoginArray = newLogin(request, response);

                    if (getLoginArray != null) {
                        for (int i = 0; i < _arrayLength; i++) {
                            if (getLoginArray[i][0].equals("ER")) {
                                Error = getLoginArray[i][1];
                                i = _arrayLength;
                                request.setAttribute("message", Error.substring(3, Error.length()));
                                request.setAttribute("Username", request.getParameter("Username") == null ? "" : request.getParameter("Username").toString());
                                getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);

                            }
                        }
                        if (Error.equals("")) {
                            //Create a new session with validated account id and password.
                            HttpSession newSession = request.getSession(true);
                            newSession.setAttribute("acc_id", request.getParameter("Username"));
                            newSession.setAttribute("pwd", request.getParameter("Password"));
                            newSession.setAttribute("loginArray", getLoginArray);
                            String sSessionID = newSession.getId();
                            newSession.setAttribute("sessionid", sSessionID);
                            String sUserId = null;
                            String sPassword = null;
                            try{
                                sUserId = newSession.getAttribute("acc_id").toString();
                                sPassword = newSession.getAttribute("pwd").toString();
                            }catch (Exception ex){}
                            
                            if (sUserId == null || sPassword == null ){
                                response.sendRedirect(request.getContextPath() + "/index.jsp");
                            }
                        
                            java.util.Date d = new java.util.Date();
                            String sTimestamp = d.toString();
                            writeSessionValues(sSessionID, sUserId, sTimestamp);

                            //get commodity details
                            getCommodityArray = getCommodity(request, response, sUserId, sPassword);

                            if (getCommodityArray == null) {
                            } else {
                                newSession.setAttribute("getCommodityArray", getCommodityArray);
                            }
                            //get country details
                            getCountriesArray = getCountries(request, response, sUserId, sPassword);

                            if (getCountriesArray == null) {
                            } else {
                                newSession.setAttribute("getCountriesArray", getCountriesArray);
                            }


//                            //get NDI Parcel details
//                            getNDIParcelArray = getParcel(request, response, sUserId, sPassword);
//
//                            if (getNDIParcelArray == null) {
//                            } else {
//                                newSession.setAttribute("getNDIParcelArray", getNDIParcelArray);
//                            }
                        }
                    } else {
                        request.setAttribute("message", "CitySprintís NextDay & International booking system is currently unavailable, please contact your local CitySprint ServiceCentre to complete your booking.");
                        request.setAttribute("Username", request.getParameter("Username") == null ? "" : request.getParameter("Username").toString());
                        getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
                    }

                    //call products API and retrieve Product details.

                }

                //conDB=cOpenConnection();

                if (isLogin(request)) {

                    // Determine the edit mode and called-by
                    sMode = request.getParameter(sParam_Mode);
                    System.out.println("######################### BookingDetails: Line#: 461 sMode after getting from Request == " + sMode);
                    sCalledBy = request.getParameter(sParam_CalledBy);

                    if (sMode == null) {
                        sMode = "Add";
                    }

                    if (sCalledBy == null) {
                        sCalledBy = "Edit";
                    }

                    System.out.println("######################### sMode line# 480 == " + sMode);
                    
                    // Send data to appropriate processing function
                    if (sCalledBy.equals(sCalledBy_Delete)) {
                        bResult = bProcessDelete(request);
                    } else if (sCalledBy.equals(sCalledBy_Add)) {
                        bResult = bProcessAdd(request);
                    } else if (sCalledBy.equals(sCalledBy_Edit)) {
                        if (sMode.equals(sMode_Add)) {
                            bResult = bNewAddForm(request);
                        } else {
                            bResult = bProcessEdit(request);
                        }
                    } else if (sCalledBy.equals(sCalledBy_Summary)) {
                        if (sMode.equals(sMode_Edit)) {
                            bResult = bNewEditForm(request);
                        } else {
                            bResult = bNewViewForm(request);
                        }
                    } else {
                        throw new ValidationException("Application error - Unknown calling mode.");
                    }


                    // Determine the edit mode and called-by for commodity grid
                    sMode1 = request.getParameter(sParam_Mode1);
                    sCalledBy1 = request.getParameter(sParam_CalledBy1);

                    if (sMode1 == null) {
                        sMode1 = "Add";
                    }

                    if (sCalledBy1 == null) {
                        sCalledBy1 = "Edit";
                    }

                    // Send data to appropriate processing function
                    if (sCalledBy1.equals(sCalledBy_Delete)) {
                        bResult1 = bProcessDelete1(request);
                    } else if (sCalledBy1.equals(sCalledBy_Add)) {
                        bResult1 = bProcessAdd1(request);
                    } else if (sCalledBy1.equals(sCalledBy_Edit)) {
                        if (sMode1.equals(sMode_Add)) {
                            bResult1 = bNewAddForm1(request);
                        } else {
                            bResult1 = bProcessEdit1(request);
                        }
                    } else if (sCalledBy1.equals(sCalledBy_Summary)) {
                        if (sMode1.equals(sMode_Edit)) {
                            bResult1 = bNewEditForm1(request);
                        } else {
                            bResult1 = bNewViewForm1(request);
                        }
                    } else {
                        throw new ValidationException("Application error - Unknown calling mode.");
                    }

                    // Display output
                    if (bResult && bResult1 || (!bResult1 && sErrorMessage.contains("Indemnity"))) {
                        vPageBuilder(request, response);
                    } else {
                        vShowErrorPage(response);
                    }
                }

            } else if (sUrl.equals("false")) {
                response.sendRedirect(request.getContextPath() + "/index.jsp");
            } else {
                if (sUrl.indexOf("chkid=") >= 0) {
                    response.sendRedirect(request.getContextPath() + "/JobAudit?" + sUrl);
                } else if (sUrl.indexOf("getJobDetails") >= 0) {
                    response.sendRedirect(request.getContextPath() + "/Tracking?" + sUrl);
                } else {
                    response.sendRedirect(request.getContextPath() + "/JobDetail?" + sUrl);
                }
            }
        } catch (Exception errValid) {
            // sValidationMessage = errValid.toString() + sErrorMessage;
            vShowErrorPage(response, errValid);
        }


    }

    /**
     *
     * @param reqInput
     * @return
     * @throws citysprint.co.uk.NDI.ValidationException
     */
//    protected boolean bProcessAdd(HttpServletRequest reqInput)
//            throws ValidationException {
//        boolean bResult = false;
//        try {
////            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@ BookingDetails: Line# 566, bProcessAdd(req): parcelName=" + reqInput.getParameter("ParcelName"));
//            // Get the master data (ie master ID no, etc)
//            vGetMasterData(reqInput);
//
//            // Read the added details into the array
//            sEditedRow = new String[nColumnCount];
//            for (int nCount = 0; nCount < nColumnCount; nCount++) {
////                if (nCount != 1) {
//                    sEditedRow[nCount] = reqInput.getParameter(sParam_EditCell
//                            + Integer.toString(nCount));
////                } 
////                else {
////                    String sIndex = reqInput.getParameter("selParcel");
////                    if (sIndex.equalsIgnoreCase("SELECT")) {
////                        sEditedRow[nCount] = "SELECT";
////                    } else {
////                        sEditedRow[nCount] = (String) parcelInfo.getPnVector().elementAt(Integer.parseInt(sIndex));
////
////                    }
////                    System.out.println("Parcel Selected value =" + sEditedRow[nCount]);
////                }
//            }
//
//            // Validate the entered data
//            vValidateSubmittedData();
//
//            // Add the new row to the database
//            if ((sEditedRow[nFirstEditColumn()] == null)
//                    || (sEditedRow[nFirstEditColumn()].equals(""))) {
//                // Read the row to go to
//                String sEditRow = reqInput.getParameter(sParam_NextRowID);
//
//                // Set up edit form on that row
//                if (sEditRow.equals("new")) {
//                    lEditingRow = -1;
//                    sMode = sMode_Add;
//                    sCalledBy = sCalledBy_Add;
//                } else {
//                    lEditingRow = Long.parseLong(sEditRow);
//                    sMode = sMode_Edit;
//                    sCalledBy = sCalledBy_Edit;
//                }
//                bResult = true;
//            } else if (!bAddRow(reqInput.getSession(false).getAttribute("sessionid").toString())) {
//                throw new Exception("The following error was produced:");
//            } else {
//                // Read the row to go to
//                String sEditRow = reqInput.getParameter(sParam_NextRowID);
//
//                // Set up edit form on that row
//                if (sEditRow.equals("new")) {
//                    
//                    System.out.println("######################### sMode line# 616 == " + sMode);
////                    if (!sMode_None.equals(sMode)){                    
//                         sMode = sMode_Add;
//                        sCalledBy = sCalledBy_Add;
//                        lEditingRow = -1;
////                    }
//                   
//                    
//                    System.out.println("######################### sMode line# 620 == " + sMode);
//                } else {
//                    lEditingRow = Long.parseLong(sEditRow);
//                    sMode = sMode_Edit;
//                    sCalledBy = sCalledBy_Edit;
//                }
//                bResult = true;
//            }
//        } catch (ValidationException errValid) {
//            throw errValid;
//        } catch (Exception errOther) {
//            sErrorMessage = "<B>Error processing the added data</B><BR>"
//                    + errOther.toString() + "<BR>"
//                    + sErrorMessage;
//            errOther.printStackTrace();
//        }
//        return bResult;
//    }
//
//    /**
//     *
//     * @param reqInput
//     * @return
//     * @throws citysprint.co.uk.NDI.ValidationException
//     */
//    protected boolean bProcessEdit(HttpServletRequest reqInput)
//            throws ValidationException {
//        boolean bResult = false;
//
//        String sInput = "";
//
//        try {
//            // Get the master data (ie master ID no, etc)
//            vGetMasterData(reqInput);
//
//            // Read the edited details into the array
//            sEditedRow = new String[nColumnCount];
//            for (int nCount = 0; nCount < nColumnCount; nCount++) {
////                sInput = reqInput.getParameter(sParam_EditCell
////                        + Integer.toString(nCount));
////                sEditedRow[nCount] = reqInput.getParameter(sParam_EditCell
////                        + Integer.toString(nCount));
//
////                if (nCount != 1) {
//                    sEditedRow[nCount] = reqInput.getParameter(sParam_EditCell
//                            + Integer.toString(nCount));
////                } else {
////                    String sIndex = reqInput.getParameter("selParcel");
////                    if (sIndex.equalsIgnoreCase("SELECT")) {
////                        sEditedRow[nCount] = "SELECT";
////                    } else {
////                        sEditedRow[nCount] = (String) parcelInfo.getPnVector().elementAt(Integer.parseInt(sIndex));
////
////                    }
////                    System.out.println("Parcel Selected value =" + sEditedRow[nCount]);
////                }
//            }
//
//
//
//            // Validate the entered data
//            vValidateSubmittedData();
//
//            // Edit that row in the database
//            lEditingRow = Long.parseLong(sEditedRow[nItemIDColumn()-1]);
//            String sEditRow = reqInput.getParameter(sParam_NextRowID);
//            
//             if (!"new".equals(sEditRow)) {
//                   
//                    lEditingRow = Long.parseLong(sEditRow);
//                   
//             }
//            
//            System.out.println("@@@@@@@@@@@@@@@@@@@ lEditingRow == " + lEditingRow + ", total size of array == " + sEditedRow.length + ",");
//            if (!bEditRow(lEditingRow, reqInput.getSession(false).getAttribute("sessionid").toString())) {
//                throw new Exception("The following error was produced:");
//            } else {
//                // Read the row to go to
//                
//System.out.println("@@@@@@@@@@@@@@@@@@@ sEditRow == " + sEditRow );
//                // Set up edit form on that row
//                if (sEditRow.equals("new")) {
//                    sMode = sMode_Add;
//                    sCalledBy = sCalledBy_Add;
//                } else {
//                    lEditingRow = Long.parseLong(sEditRow);
//                    sMode = sMode_Add;
//                    sCalledBy = sCalledBy_Add;
//                }
//                bResult = true;
//            }
//        } catch (ValidationException errValid) {
//            throw errValid;
//        } catch (Exception errOther) {
//            sErrorMessage = "<B>Error processing the edited data</B><BR>"
//                    + errOther.toString() + "<BR>"
//                    + sErrorMessage;
//            errOther.printStackTrace();
//        }
//        return bResult;
//    }
//
//    /**
//     *
//     * @param reqInput
//     * @return
//     */
//    protected boolean bProcessDelete(HttpServletRequest reqInput) {
//        boolean bResult = false;
//        Connection conProcessDelete = null;
//        
//        int nRowIDColumn = 5;
//        try {
//            // Get the master data (ie master ID no, etc)
//            vGetMasterData(reqInput);
//
//            // Find out the current row id
//            String sDeleteRowID = reqInput.getParameter(sParam_NextRowID);
//            long lDeleteRowID = Long.parseLong(sDeleteRowID);
//
//            // Determine the row to go to (ie next or previous)
//            boolean bFound = false;
//            boolean bContinue = true;
//            long lPreviousID = -1;
//            long lNextID = -1;
//
//            conProcessDelete = cOpenConnection();
//            Statement qryPositionCheck = conProcessDelete.createStatement();
//            String query = sSelectSQL(reqInput.getSession(false).getAttribute("sessionid").toString());
//            
//            System.out.println("Query == " + query);
//            ResultSet rstPositionCheck = qryPositionCheck.executeQuery(query);
//            if (!rstPositionCheck.next()) {                
//                throw new Exception("The deleted record was not found in the database.");
//            }
//
//            while (!bFound && bContinue) {
//                bFound = (rstPositionCheck.getLong(nRowIDColumn + 1)
//                        == lDeleteRowID);
//                if (!bFound) {
//                    lPreviousID = rstPositionCheck.getLong(nRowIDColumn + 1);
//                    bContinue = rstPositionCheck.next();
//                }
//            }
//
//            if (!bFound) {
//                rstPositionCheck.close();
//                qryPositionCheck.close();
//                throw new Exception("The deleted record was not found in the database.");
//            } else {
//                // If the last row, choose the previous row, else the next
//                if (rstPositionCheck.next()) {
//                    lNextID = rstPositionCheck.getLong(nRowIDColumn + 1);
//                } else {
//                    lNextID = lPreviousID;
//                }
//
//                rstPositionCheck.close();
//                qryPositionCheck.close();
//            }
//
//            conProcessDelete.close();
//
//            // Delete that row in the database
//            if (!bDeleteRow(lDeleteRowID)) {
//                throw new Exception("The following error was produced:");
//            } else {
//                // Set up edit form on that row
//                if (lNextID == -1) {
//                    sMode = sMode_Add;
//                    sCalledBy = sCalledBy_Add;
//                } else {
////              		lEditingRow = lNextID;
////                  sMode       = sMode_Edit;
////                  sCalledBy   = sCalledBy_Edit;
//
//                    sMode = sMode_Add;
//                    sCalledBy = sCalledBy_Add;
//                }
//                bResult = true;
//            }
//        } catch (Exception errOther) {
//            sErrorMessage = "<B>Error processing the delete command</B><BR>"
//                    + errOther.toString() + "<BR>"
//                    + sErrorMessage;
//        }
//        return bResult;
//    }
//
//    /**
//     *
//     * @param reqInput
//     * @return
//     * @throws citysprint.co.uk.NDI.ValidationException
//     */
//    protected boolean bNewAddForm(HttpServletRequest reqInput)
//            throws ValidationException {
//        boolean bResult = false;
//
//        HttpSession validSession = reqInput.getSession(false);
//
//        try {
//            // Get the master data (ie master ID no, etc)
//            vGetMasterData(reqInput);
//
//            // If there is data to save, save it
//            if (reqInput.getParameter(sParam_EditCell + "6") != null) {
//                // Read the edited details into the array
//                sEditedRow = new String[nColumnCount];
//                for (int nCount = 0; nCount < nColumnCount; nCount++) {
////                    sEditedRow[nCount] = reqInput.getParameter(sParam_EditCell
////                            + Integer.toString(nCount));
//
//                    if (nCount != 1) {
//                        sEditedRow[nCount] = reqInput.getParameter(sParam_EditCell
//                                + Integer.toString(nCount));
//                    }
////                    else {
////                        String sIndex = reqInput.getParameter("selParcel");
////                        if (sIndex.equalsIgnoreCase("SELECT")) {
////                            sEditedRow[nCount] = "SELECT";
////                        } else {
////                            sEditedRow[nCount] = (String) parcelInfo.getPnVector().elementAt(Integer.parseInt(sIndex));
////
////                        }
////                        System.out.println("Parcel Selected value =" + sEditedRow[nCount]);
////                    }
//
//                }
//
//                // Validate the entered data
//                vValidateSubmittedData();
//
//                // Edit that row in the database
//                lEditingRow = Long.parseLong(sEditedRow[nItemIDColumn()]);
//                if (!bEditRow(lEditingRow, reqInput.getSession(false).getAttribute("sessionid").toString())) {
//                    throw new Exception("The following error was produced:");
//                }
//            }
//            // Set up for add form on last row
//            sMode = sMode_Add;
//            sCalledBy = sCalledBy_Add;
//            bResult = true;
//        } catch (ValidationException errValid) {
//            throw errValid;
//        } catch (Exception errOther) {
//            sErrorMessage = "<B>Error setting up the add form</B><BR>"
//                    + errOther.toString() + "<BR>"
//                    + sErrorMessage;
//        }
//        return bResult;
//    }
//
//    /**
//     *
//     * @param reqInput
//     * @return
//     */
//    protected boolean bNewEditForm(HttpServletRequest reqInput) {
//        boolean bResult = false;
//        try {
//            // Get the master data (ie master ID no, etc)
//            vGetMasterData(reqInput);
//
//            // Get the first row and make it the editable one
//            // Statement qryEditRow = conDB.createStatement();
//            //ResultSet rstEditRow = qryEditRow.executeQuery(sSelectSQL(sSessionID));\
//
//            //get the requested edit row id-ramya
//            String sInput = reqInput.getParameter("sNextRow");
//
//            if (sInput == null) {
//                lEditingRow = -1;
//            } else {
//                lEditingRow = Long.parseLong(sInput);
//            }
//
////          if (rstEditRow.next())
////              lEditingRow = rstEditRow.getLong(1);
////          else
////              lEditingRow = -1;
//            // rstEditRow.close();
//            //  qryEditRow.close();
//
//            // Set up for new edit form on first row as editable
//            sMode = sMode_Edit;
//            sCalledBy = sCalledBy_Edit;
//            bResult = true;
//        } catch (Exception errOther) {
//            sErrorMessage = "<B>Error setting up the edit form</B><BR>"
//                    + errOther.toString() + "<BR>"
//                    + sErrorMessage;
//        }
//        return bResult;
//    }
//
//    /**
//     *
//     * @param reqInput
//     * @return
//     */
//    protected boolean bNewViewForm(HttpServletRequest reqInput) {
//        boolean bResult = false;
//        try {
//            // Get the master data (ie master ID no, etc)
//            vGetMasterData(reqInput);
//
//            // Set up for new view form with no anchor
//            sMode = sMode_View;
//            sCalledBy = sCalledBy_Summary;
//            bResult = true;
//        } catch (Exception errOther) {
//            sErrorMessage = "<B>Error setting up the view form</B><BR>"
//                    + errOther.toString() + "<BR>"
//                    + sErrorMessage;
//        }
//        return bResult;
//    }
    
    
    
         protected boolean bProcessAdd(HttpServletRequest reqInput)
      throws ValidationException
    {
      boolean bResult = false;
      try
      {
          System.out.println("################ bProcessAdd line# 963, reqInput.getParameter(sParam_NextRowID) == " + reqInput.getParameter(sParam_NextRowID));
          // Get the master data (ie master ID no, etc)
          vGetMasterData(reqInput);

          // Read the added details into the array
          sEditedRow = new String[nColumnCount];
          for (int nCount=0; nCount < nColumnCount; nCount++)
              sEditedRow[nCount] = reqInput.getParameter(sParam_EditCell +
                                                 Integer.toString(nCount));

          // Validate the entered data
          vValidateSubmittedData();

          // Add the new row to the database
          if ((sEditedRow[nFirstEditColumn()] == null) ||
              (sEditedRow[nFirstEditColumn()].equals("")))
          {
              // Read the row to go to
              String sEditRow = reqInput.getParameter(sParam_NextRowID);

              // Set up edit form on that row
              if (sEditRow.equals("new"))
              {
                  lEditingRow = -1;
                  sMode       = sMode_Add;
                  sCalledBy   = sCalledBy_Add;
              }
              else
              {
                  lEditingRow = Long.parseLong(sEditRow);
                  sMode       = sMode_Edit;
                  sCalledBy   = sCalledBy_Edit;
              }
              bResult = true;
          }
          else if (!bAddRow(reqInput.getSession(false).getAttribute("sessionid").toString()))
              throw new Exception("The following error was produced:");
          else
          {
              // Read the row to go to
              String sEditRow = reqInput.getParameter(sParam_NextRowID);

              // Set up edit form on that row
              if (sEditRow.equals("new"))
              {
                  lEditingRow = -1;
                  sMode       = sMode_Add;
                  sCalledBy   = sCalledBy_Add;
              }
              else
              {
                  lEditingRow = Long.parseLong(sEditRow);
                  sMode       = sMode_Edit;
                  sCalledBy   = sCalledBy_Edit;
              }
              bResult = true;
          }
      }
      catch (ValidationException errValid)
      {
          throw errValid;
      }
      catch (Exception errOther)
      {
          sErrorMessage = "<B>Error processing the added data</B><BR>" +
                          errOther.toString() + "<BR>" +
                          sErrorMessage;
      }
      return bResult;
    }

    /**
     *
     * @param reqInput
     * @return
     * @throws citysprint.co.uk.NDI.ValidationException
     */
    protected boolean bProcessEdit(HttpServletRequest reqInput)
      throws ValidationException
    {
      boolean bResult = false;

      String sInput="";

      try
      {
          // Get the master data (ie master ID no, etc)
          vGetMasterData(reqInput);

          // Read the edited details into the array
          sEditedRow = new String[nColumnCount];
          for (int nCount=0; nCount < nColumnCount; nCount++)
          {
              sInput=reqInput.getParameter(sParam_EditCell +
                                                 Integer.toString(nCount));
              sEditedRow[nCount] = reqInput.getParameter(sParam_EditCell +
                                                 Integer.toString(nCount));
          }

          // Validate the entered data
          vValidateSubmittedData();

          // Edit that row in the database
          //lEditingRow = Long.parseLong(sEditedRow[nIDColumn()]);
          lEditingRow = Long.parseLong(sEditedRow[nItemIDColumn()]);
          if (!bEditRow(lEditingRow,reqInput.getSession(false).getAttribute("sessionid").toString()))
              throw new Exception("The following error was produced:");
          else
          {
              // Read the row to go to
              String sEditRow = reqInput.getParameter(sParam_NextRowID);

              // Set up edit form on that row
              if (sEditRow.equals("new"))
              {
                  sMode       = sMode_Add;
                  sCalledBy   = sCalledBy_Add;
              }
              else
              {
                  lEditingRow = Long.parseLong(sEditRow);
                  sMode       = sMode_Add;
                  sCalledBy   = sCalledBy_Add;
              }
              bResult = true;
          }
      }
      catch (ValidationException errValid)
      {
          throw errValid;
      }
      catch (Exception errOther)
      {
          sErrorMessage = "<B>Error processing the edited data</B><BR>" +
                          errOther.toString() + "<BR>" +
                          sErrorMessage;
      }
      return bResult;
    }

    /**
     *
     * @param reqInput
     * @return
     */
    protected boolean bProcessDelete(HttpServletRequest reqInput)
    {
      boolean bResult = false;
      Connection conProcessDelete=null;
      try
      {
          // Get the master data (ie master ID no, etc)
          vGetMasterData(reqInput);

          // Find out the current row id
          String sDeleteRowID = reqInput.getParameter(sParam_NextRowID);
          long   lDeleteRowID = Long.parseLong(sDeleteRowID);

          System.out.println("################# session id == " + reqInput.getSession(false).getAttribute("sessionid").toString());
          System.out.println("################# deleted row id == " + lDeleteRowID);
          // Determine the row to go to (ie next or previous)
          boolean bFound 			= false;
          boolean bContinue   = true;
          long 		lPreviousID = -1;
          long		lNextID			= -1;

          conProcessDelete=cOpenConnection();
          Statement qryPositionCheck = conProcessDelete.createStatement();
          ResultSet rstPositionCheck = qryPositionCheck.executeQuery(sSelectSQL(reqInput.getSession(false).getAttribute("sessionid").toString()));
          if (!rstPositionCheck.next())
          		throw new Exception("The deleted record was not found in the database.");

          System.out.println("################# After if (!rstPositionCheck.next())");
          while (!bFound && bContinue)
          {
              bFound 			= (rstPositionCheck.getLong(nRowIDColumn + 1)
                                           == lDeleteRowID);
              
              System.out.println("################# inside while and bFound == " + bFound);
                if (!bFound)
              {
                  lPreviousID = rstPositionCheck.getLong(nRowIDColumn + 1);
                  bContinue   = rstPositionCheck.next();
              }
          }

          if (!bFound)
          {
              rstPositionCheck.close();
              qryPositionCheck.close();
              throw new Exception("The deleted record was not found in the database.");
	  }
          else
          {
            // If the last row, choose the previous row, else the next
            if (rstPositionCheck.next())
            lNextID = rstPositionCheck.getLong(nRowIDColumn + 1);
              else
          				lNextID = lPreviousID;

          		rstPositionCheck.close();
              qryPositionCheck.close();
          }

          conProcessDelete.close();

          // Delete that row in the database
          if (!bDeleteRow(lDeleteRowID))
              throw new Exception("The following error was produced:");
          else
          {
              // Set up edit form on that row
              if (lNextID == -1)
              {
                  sMode       = sMode_Add;
                  sCalledBy   = sCalledBy_Add;
              }
              else
              {
//              		lEditingRow = lNextID;
//                  sMode       = sMode_Edit;
//                  sCalledBy   = sCalledBy_Edit;

                   sMode       = sMode_Add;
                  sCalledBy   = sCalledBy_Add;
              }
              bResult = true;
          }
      }
      catch (Exception errOther)
      {
          errOther.printStackTrace();
          sErrorMessage = "<B>Error processing the delete command</B><BR>" +
                          errOther.toString() + "<BR>" +
                          sErrorMessage;
      }
      return bResult;
    }

    /**
     *
     * @param reqInput
     * @return
     * @throws citysprint.co.uk.NDI.ValidationException
     */
    protected boolean bNewAddForm(HttpServletRequest reqInput)
      throws ValidationException
    {
      boolean bResult = false;

      HttpSession validSession=reqInput.getSession(false);

      try
      {
          // Get the master data (ie master ID no, etc)
          vGetMasterData(reqInput);

          // If there is data to save, save it
          if (reqInput.getParameter(sParam_EditCell + "4") != null)
          {
              // Read the edited details into the array
              sEditedRow = new String[nColumnCount];
              for (int nCount=0; nCount < nColumnCount; nCount++)//{
              sEditedRow[nCount] = reqInput.getParameter(sParam_EditCell +
                                                 Integer.toString(nCount));

//              System.out.println("############## nCount ==" + nCount + ", reqInput.getParameter(sParam_EditCell" +
//                                                 "Integer.toString(nCount)) == " + reqInput.getParameter(sParam_EditCell +
//                                                 Integer.toString(nCount)));
//              
//              }
              // Validate the entered data
              vValidateSubmittedData();

              // Edit that row in the database
              lEditingRow = Long.parseLong(sEditedRow[nIDColumn()]);
              if (!bEditRow(lEditingRow,reqInput.getSession(false).getAttribute("sessionid").toString()))
                  throw new Exception("The following error was produced:");
          }
       		// Set up for add form on last row
          sMode       = sMode_Add;
          sCalledBy   = sCalledBy_Add;
          bResult     = true;
      }
      catch (ValidationException errValid)
      {
          throw errValid;
      }
      catch (Exception errOther)
      {
          sErrorMessage = "<B>Error setting up the add form</B><BR>" +
                          errOther.toString() + "<BR>" +
                          sErrorMessage;
          
          errOther.printStackTrace();
          
           bResult     = true;
      }
      return bResult;
    }

    /**
     *
     * @param reqInput
     * @return
     */
    protected boolean bNewEditForm(HttpServletRequest reqInput)
    {
      boolean bResult = false;
      try
      {
          // Get the master data (ie master ID no, etc)
          vGetMasterData(reqInput);

          // Get the first row and make it the editable one
         // Statement qryEditRow = conDB.createStatement();
          //ResultSet rstEditRow = qryEditRow.executeQuery(sSelectSQL(sSessionID));\

          //get the requested edit row id-ramya
          String sInput=reqInput.getParameter("sNextRow");

          if(sInput==null)
              lEditingRow=-1;
          else
              lEditingRow=Long.parseLong(sInput);

//          if (rstEditRow.next())
//              lEditingRow = rstEditRow.getLong(1);
//          else
//              lEditingRow = -1;
         // rstEditRow.close();
        //  qryEditRow.close();

          // Set up for new edit form on first row as editable
          sMode       = sMode_Edit;
          sCalledBy   = sCalledBy_Edit;
          bResult 		= true;
      }
      catch (Exception errOther)
      {
          sErrorMessage = "<B>Error setting up the edit form</B><BR>" +
                          errOther.toString() + "<BR>" +
                          sErrorMessage;
      }
      return bResult;
    }

    /**
     *
     * @param reqInput
     * @return
     */
    protected boolean bNewViewForm(HttpServletRequest reqInput)
    {
      boolean bResult = false;
      try
      {
          // Get the master data (ie master ID no, etc)
          vGetMasterData(reqInput);

          // Set up for new view form with no anchor
          sMode       = sMode_View;
          sCalledBy   = sCalledBy_Summary;
          bResult 		= true;
      }
      catch (Exception errOther)
      {
          sErrorMessage = "<B>Error setting up the view form</B><BR>" +
                          errOther.toString() + "<BR>" +
                          sErrorMessage;
      }
      return bResult;
    }

    /**
     *
     * @param reqInput
     * @return
     * @throws citysprint.co.uk.NDI.ValidationException
     */
    protected boolean bProcessAdd1(HttpServletRequest reqInput)
            throws ValidationException {
        boolean bResult = false;
        HttpSession validSession = reqInput.getSession(false);

        try {
            // Get the master data (ie master ID no, etc)
            vGetMasterData(reqInput);

            // Read the added details into the array
            sEditedRow1 = new String[nColumnCount1];
            for (int nCount = 0; nCount < nColumnCount1; nCount++) {
                sEditedRow1[nCount] = reqInput.getParameter(sParam_EditCell1
                        + Integer.toString(nCount));
            }

            // Validate the entered data
            vValidateSubmittedData();

            // Add the new row to the database
            if ((sEditedRow1[nFirstEditColumn()] == null)
                    || (sEditedRow1[nFirstEditColumn()].equals(""))) {
                // Read the row to go to
                String sEditRow = reqInput.getParameter(sParam_NextRowID1);

                // Set up edit form on that row
                if (sEditRow.equals("new")) {
                    lEditingRow1 = -1;
                    sMode1 = sMode_Add;
                    sCalledBy1 = sCalledBy_Add;
                } else {
                    lEditingRow1 = Long.parseLong(sEditRow);
                    sMode1 = sMode_Edit;
                    sCalledBy1 = sCalledBy_Edit;
                }
                bResult = true;
            } else if (!bAddRow1(validSession)) {
                throw new Exception("The following error was produced:");
            } else {
                // Read the row to go to
                String sEditRow = reqInput.getParameter(sParam_NextRowID1);

                // Set up edit form on that row
                if (sEditRow.equals("new")) {
                    lEditingRow1 = -1;
                    sMode1 = sMode_Add;
                    sCalledBy1 = sCalledBy_Add;
                } else {
                    lEditingRow1 = Long.parseLong(sEditRow);
                    sMode1 = sMode_Edit;
                    sCalledBy1 = sCalledBy_Edit;
                }
                bResult = true;
            }
        } catch (ValidationException errValid) {
            throw errValid;
        } catch (Exception errOther) {
            if (!sErrorMessage.contains("Indemnity")) {
                sErrorMessage = "<B>Error processing the added data</B><BR>"
                        + errOther.toString() + "<BR>"
                        + sErrorMessage;
                errOther.printStackTrace();
            }
        }
        return bResult;
    }

    /**
     *
     * @param reqInput
     * @return
     * @throws citysprint.co.uk.NDI.ValidationException
     */
    protected boolean bProcessEdit1(HttpServletRequest reqInput)
            throws ValidationException {
        boolean bResult = false;
        HttpSession validSession = reqInput.getSession(false);

        String sInput = "";

        try {
            // Get the master data (ie master ID no, etc)
            vGetMasterData(reqInput);

            // Read the edited details into the array
            sEditedRow1 = new String[nColumnCount1];
            for (int nCount = 0; nCount < nColumnCount1; nCount++) {
                sInput = reqInput.getParameter(sParam_EditCell1
                        + Integer.toString(nCount));
                sEditedRow1[nCount] = reqInput.getParameter(sParam_EditCell1
                        + Integer.toString(nCount));
            }

            // Validate the entered data
            vValidateSubmittedData();

            // Edit that row in the database
            lEditingRow1 = Long.parseLong(sEditedRow1[nIDColumn()]);
            if (!bEditRow1(lEditingRow1, validSession)) {
                throw new Exception("The following error was produced:");
            } else {
                // Read the row to go to
                String sEditRow = reqInput.getParameter(sParam_NextRowID1);

                // Set up edit form on that row
                if (sEditRow.equals("new")) {
                    sMode1 = sMode_Add;
                    sCalledBy1 = sCalledBy_Add;
                } else {
                    lEditingRow1 = Long.parseLong(sEditRow);
                    sMode1 = sMode_Add;
                    sCalledBy1 = sCalledBy_Add;
                }
                bResult = true;
            }
        } catch (ValidationException errValid) {
            throw errValid;
        } catch (Exception errOther) {
            if (!sErrorMessage.contains("Indemnity")) {
                sErrorMessage = "<B>Error processing the added data</B><BR>"
                        + errOther.toString() + "<BR>"
                        + sErrorMessage;
                errOther.printStackTrace();
            }
        }
        return bResult;
    }

    /**
     *
     * @param reqInput
     * @return
     */
    protected boolean bProcessDelete1(HttpServletRequest reqInput) {
        boolean bResult = false;

        Connection conProcessDelete1 = null;

        try {
            // Get the master data (ie master ID no, etc)
            vGetMasterData(reqInput);

            // Find out the current row id
            String sDeleteRowID = reqInput.getParameter(sParam_NextRowID1);
            long lDeleteRowID = Long.parseLong(sDeleteRowID);

            // Determine the row to go to (ie next or previous)
            boolean bFound = false;
            boolean bContinue = true;
            long lPreviousID = -1;
            long lNextID = -1;

            conProcessDelete1 = cOpenConnection();

            Statement qryPositionCheck = conProcessDelete1.createStatement();
            ResultSet rstPositionCheck = qryPositionCheck.executeQuery(sSelectSQL1(reqInput.getSession(false).getAttribute("sessionid").toString()));
            if (!rstPositionCheck.next()) {
                throw new Exception("The deleted record was not found in the database.");
            }

            while (!bFound && bContinue) {
                bFound = (rstPositionCheck.getLong(nRowIDColumn + 1)
                        == lDeleteRowID);
                if (!bFound) {
                    lPreviousID = rstPositionCheck.getLong(nRowIDColumn + 1);
                    bContinue = rstPositionCheck.next();
                }
            }

            if (!bFound) {
                rstPositionCheck.close();
                qryPositionCheck.close();
                throw new Exception("The deleted record was not found in the database.");
            } else {
                // If the last row, choose the previous row, else the next
                if (rstPositionCheck.next()) {
                    lNextID = rstPositionCheck.getLong(nRowIDColumn + 1);
                } else {
                    lNextID = lPreviousID;
                }

                rstPositionCheck.close();
                qryPositionCheck.close();
            }

            conProcessDelete1.close();

            // Delete that row in the database
            if (!bDeleteRow1(lDeleteRowID)) {
                throw new Exception("The following error was produced:");
            } else {
                // Set up edit form on that row
                if (lNextID == -1) {
                    sMode1 = sMode_Add;
                    sCalledBy1 = sCalledBy_Add;
                } else {
//              		lEditingRow1 = lNextID;
//                  sMode1       = sMode1_Edit;
//                  sCalledBy1   = sCalledBy1_Edit;

                    sMode1 = sMode_Add;
                    sCalledBy1 = sCalledBy_Add;
                }
                bResult = true;
            }
        } catch (Exception errOther) {
            errOther.printStackTrace();
            sErrorMessage = "<B>Error processing the delete command</B><BR>"
                    + errOther.toString() + "<BR>"
                    + sErrorMessage;
        }
        return bResult;
    }

    /**
     *
     * @param reqInput
     * @return
     * @throws citysprint.co.uk.NDI.ValidationException
     */
    protected boolean bNewAddForm1(HttpServletRequest reqInput)
            throws ValidationException {
        boolean bResult = false;
        HttpSession validSession = reqInput.getSession(false);

        try {
            // Get the master data (ie master ID no, etc)
            vGetMasterData(reqInput);

            // If there is data to save, save it
            if (reqInput.getParameter(sParam_EditCell1 + "4") != null) {
                // Read the edited details into the array
                sEditedRow1 = new String[nColumnCount1];
                for (int nCount = 0; nCount < nColumnCount1; nCount++) {
                    sEditedRow1[nCount] = reqInput.getParameter(sParam_EditCell1
                            + Integer.toString(nCount));
                }

                // Validate the entered data
                vValidateSubmittedData();

                // Edit that row in the database
                lEditingRow1 = Long.parseLong(sEditedRow1[nIDColumn()]);
                if (!bEditRow1(lEditingRow1, validSession)) {
                    throw new Exception("The following error was produced:");
                }
            }
            // Set up for add form on last row
            sMode1 = sMode_Add;
            sCalledBy1 = sCalledBy_Add;
            bResult = true;
        } catch (ValidationException errValid) {
            throw errValid;
        } catch (Exception errOther) {
            sErrorMessage = "<B>Error setting up the add form</B><BR>"
                    + errOther.toString() + "<BR>"
                    + sErrorMessage;
        }
        return bResult;
    }

    /**
     *
     * @param reqInput
     * @return
     */
    protected boolean bNewEditForm1(HttpServletRequest reqInput) {
        boolean bResult = false;
        try {
            // Get the master data (ie master ID no, etc)
            vGetMasterData(reqInput);

            // Get the first row and make it the editable one
            // Statement qryEditRow = conDB.createStatement();
            //ResultSet rstEditRow = qryEditRow.executeQuery(sSelectSQL(sSessionID));\

            //get the requested edit row id-ramya
            String sInput = reqInput.getParameter("sNextRow1");

            if (sInput == null) {
                lEditingRow1 = -1;
            } else {
                lEditingRow1 = Long.parseLong(sInput);
            }

//          if (rstEditRow.next())
//              lEditingRow1 = rstEditRow.getLong(1);
//          else
//              lEditingRow1 = -1;
            // rstEditRow.close();
            //  qryEditRow.close();

            // Set up for new edit form on first row as editable
            sMode1 = sMode_Edit;
            sCalledBy1 = sCalledBy_Edit;
            bResult = true;
        } catch (Exception errOther) {
            sErrorMessage = "<B>Error setting up the edit form</B><BR>"
                    + errOther.toString() + "<BR>"
                    + sErrorMessage;
        }
        return bResult;
    }

    /**
     *
     * @param reqInput
     * @return
     */
    protected boolean bNewViewForm1(HttpServletRequest reqInput) {
        boolean bResult = false;
        try {
            // Get the master data (ie master ID no, etc)
            vGetMasterData(reqInput);

            // Set up for new view form with no anchor
            sMode1 = sMode_View;
            sCalledBy1 = sCalledBy_Summary;
            bResult = true;
        } catch (Exception errOther) {
            sErrorMessage = "<B>Error setting up the view form</B><BR>"
                    + errOther.toString() + "<BR>"
                    + sErrorMessage;
        }
        return bResult;
    }

    /**
     *
     * @param strmResponse
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void vWriteMasterData(ServletOutputStream strmResponse) throws ServletException, IOException {
        // Implementing classes use if required to export
        // any header data (eg DespatchID, ClientID, etc)
    }

    /**
     *
     * @param reqInput
     */
    protected void vGetMasterData(HttpServletRequest reqInput) {
        // Implementing classes use if required to import
        // any header data (eg DespatchID, ClientID, etc)
    }

    /**
     *
     * @return
     */
    protected int nFirstEditColumn() {
        return 0;
    }

    /**
     *
     * @return
     */
    protected int nIDColumn() {
        return 4;
    }

    protected int nItemIDColumn() {
//        return 6;
//        return 5;
        return 4;
    }

    /**
     *
     * @return
     */
    protected int nImageColumn() {
        return -1;
    }

    /**
     *
     * @param nColumn
     * @param sData
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected String sBuildCellEditor(int nColumn, String sData) throws IllegalArgumentException {
        if (nColumn == 0) {
            return "<INPUT TYPE=TEXT name=\"" + sParam_EditCell
                    + Integer.toString(nColumn) + "\" id=\"" + sParam_EditCell
                    + Integer.toString(nColumn) + "\" VALUE=\"" + sData
                    + "\" SIZE=" + Integer.toString(nGetColumnTextWidth(nColumn)) + " maxlength=" + Integer.toString(nGetColumnMaxLengths(nColumn)) + " style=\"text-align:right\" onKeyPress=\"return checkKey(event,'I')\">";
//        } else if (nColumn == 1) {
//
//            String sSelect = "<SELECT NAME=\"selParcel" + "\" id=\"selParcel"
//                    + "\" onchange=\"JavaScript:parcelClick();\" >\n"
//                    + "<OPTION VALUE=SELECT"
//                    + (sData.equals("SELECT") ? " SELECTED" : "") + ">SELECT\n";
////                    + "<OPTION VALUE=\"YES\""
////                    + (sData.equals("1") ? " SELECTED" : "") + ">YES\n"
////                    + "<OPTION VALUE=\"NO\""
////                    + (sData.equals("0") ? " SELECTED" : "") + ">NO\n"
//            for (int i = 0; i <= parcelInfo.getPnVector().size() - 1; i++) {
//
//                if (sData.equalsIgnoreCase((String) parcelInfo.getPnVector().elementAt(i))) {
//                    sSelect += "<option value=" + i + " SELECTED>" + parcelInfo.getPnVector().elementAt(i) + "</option>";
//                } else {
//                    sSelect += "<option value=" + i + ">" + parcelInfo.getPnVector().elementAt(i) + "</option>";
//                }
//
//            }
//            sSelect += "</SELECT>";
//            return sSelect;
        } else if (nColumn == 2) {
            // Implementing classes can override to create custom editors
            return "<INPUT TYPE=TEXT name=\"" + sParam_EditCell
                    + Integer.toString(nColumn) + "\" id=\"" + sParam_EditCell
                    + Integer.toString(nColumn) + "\" VALUE=\"" + sData
                    + "\" SIZE=" + Integer.toString(nGetColumnTextWidth(nColumn)) + " maxlength=" + Integer.toString(nGetColumnMaxLengths(nColumn)) + " style=\"text-align:right\" onKeyPress=\"return checkKeyIW(event,'I')\">";
        } else {
            // Implementing classes can override to create custom editors
            return "<INPUT TYPE=TEXT name=\"" + sParam_EditCell
                    + Integer.toString(nColumn) + "\" id=\"" + sParam_EditCell
                    + Integer.toString(nColumn) + "\" VALUE=\"" + sData
                    + "\" SIZE=" + Integer.toString(nGetColumnTextWidth(nColumn)) + " maxlength=" + Integer.toString(nGetColumnMaxLengths(nColumn)) + " style=\"text-align:right\" onKeyPress=\"return checkKey(event,'I')\">";
        }
    }

    /**
     *
     * @param nColumn
     * @param sData
     * @param validSession
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected String sBuildCellEditor1(int nColumn, String sData, HttpSession validSession) throws IllegalArgumentException {
        if (nColumn == 2) {
            // Implementing classes can override to create custom editors
            return "<INPUT TYPE=TEXT name=\"" + sParam_EditCell1
                    + Integer.toString(nColumn) + "\" id=\"" + sParam_EditCell1
                    + Integer.toString(nColumn) + "\" VALUE=\"" + sData
                    + "\" SIZE=" + Integer.toString(nGetColumnTextWidth1(nColumn)) + " maxlength=" + Integer.toString(nGetColumnMaxLengths1(nColumn)) + " style=\"text-align:right\" onKeyPress=\"return checkKey(event,'C')\">";
        } else {
            // Implementing classes can override to create custom editors
            return "<INPUT TYPE=TEXT name=\"" + sParam_EditCell1
                    + Integer.toString(nColumn) + "\" id=\"" + sParam_EditCell1
                    + Integer.toString(nColumn) + "\" VALUE=\"" + sData
                    + "\" SIZE=" + Integer.toString(nGetColumnTextWidth1(nColumn)) + " maxlength=" + Integer.toString(nGetColumnMaxLengths1(nColumn)) + " style=\"text-align:right\" onKeyPress=\"return checkKey(event,'C')\">";
        }
    }

    /**
     *
     * @param nColumn
     * @param sData
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected String sBuildCellRenderer(int nColumn, String sData) throws IllegalArgumentException {
        sData = (sData.equals("") ? "<BR>" : sData);
        return sWordWrap(sData, nGetColumnTextWidth(nColumn));
    }

    /**
     *
     * @param nColumn
     * @param sData
     * @return
     * @throws java.lang.IllegalArgumentException
     */
    protected String sBuildCellRenderer1(int nColumn, String sData) throws IllegalArgumentException {
        sData = (sData.equals("") ? "<BR>" : sData);
        return sWordWrap(sData, nGetColumnTextWidth1(nColumn));
    }

    /**
     *
     * @param nCount
     * @return
     */
    protected String sDefaultValue(int nCount) {
        return "";
    }

    /**
     *
     * @param strmResponse
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void vWritePageExtras(ServletOutputStream strmResponse)
            throws ServletException, IOException {
        // Used to include any extra form objects at the bottom of
        // the table (eg auto-populate box, etc)
    }

    /**
     *
     * @param strmResponse
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void vWriteStyleSettings(ServletOutputStream strmResponse)
            throws ServletException, IOException {
        strmResponse.println("<STYLE type=\"text/css\"><!--");
        strmResponse.println("BODY, TD, INPUT, SELECT, TEXTAREA"
                + "{font-family:Verdana, Arial,Helvetica;"
                + "font-size:8pt;color:black}");
        strmResponse.println("//--></STYLE>");
    }

    private void vCommodityGrid(HttpServletRequest rspInput, HttpServletResponse rspOutput) throws ServletException, IOException {


        // Loop counter
        long lRowCounter = 0;
        Connection conCom = null;

        ServletOutputStream strmResponse = rspOutput.getOutputStream();
        rspOutput.setContentType("text/html");
        // Write the CommodityGrid header


        strmResponse.println("<TABLE border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" align=\"center\" style=\"border:1px solid #666666;padding:5px 5px 5px 5px;background-color:#EFEFEF;\">");


        strmResponse.println("<TR BGCOLOR=#FFFAF0 HEIGHT=20>");
        for (int nCount = 0; nCount < nColumnCount1; nCount++) {
            if (nCount == 4) {
                strmResponse.println("<TD ALIGN=CENTER id=\"boxtitle\" colspan=\"2\" WIDTH=" + Integer.toString(nGetColumnWidth1(nCount)) + "><B>"
                        + sGetColumnHeader1(nCount) + "</B></TD>");
            } else //                if(nCount==nColumnCount-1)
            //                    strmResponse.println("<TD ALIGN=RIGHT style=\"text-align:right;padding-right:54px\" id=\"boxtitle\" WIDTH=" +Integer.toString(nGetColumnWidth(nCount)) + "><B>" +
            //            											sGetColumnHeader(nCount) + "</B></TD>");
            //                else
            {
                strmResponse.println("<TD ALIGN=CENTER id=\"boxtitle\" WIDTH=" + Integer.toString(nGetColumnWidth1(nCount)) + "><B>"
                        + sGetColumnHeader1(nCount) + "</B></TD>");
            }
        }
        strmResponse.println("</TR>");

        try {
            conCom = cOpenConnection();
            // Open the recordset
            Statement qryTableData = conCom.createStatement();
            ResultSet rstTableData = qryTableData.executeQuery(sSelectSQL1(rspInput.getSession(false).getAttribute("sessionid").toString()));
            // Loop through the recordset
            while (rstTableData.next()) {
                lRowCounter++;
                strmResponse.println("<TR id=\"odd\">");
                for (int nCount = 0; nCount < nColumnCount1; nCount++) {
                    // Case 1 - editable cells (except adding row)
                    if (sMode1.equals(sMode_Edit) && bEditAllowed1(nCount) && rstTableData.getLong(5) == lEditingRow1) {
                        String sData = (rstTableData.getObject(nCount + 1) == null ? "" : rstTableData.getString(nCount + 1));

                        // If this is the editable row, show as editable cells
                        strmResponse.println("<TD>" + sBuildCellEditor1(nCount, sData, rspInput.getSession(false)) + "</TD>");
                    } // Case 2 - non editable cells in the editing row (not add)
                    else if ((sMode1.equals(sMode_Edit)
                            && rstTableData.getLong(5) == lEditingRow1)) {
                        // If row counter column
                        if (nCount == 4) {
                            if (bDeleteAllowed1(rstTableData.getLong(5))) {
                                strmResponse.println(
                                        "<TD ALIGN=CENTER>"
                                        + "</TD><TD ALIGN=CENTER>"
                                        + "<input type=\"image\" border=0 src=\"images/saveico_t.gif\" alt=\"Save Item\" onclick=\"javascript:vSave1()\">"
                                        + "<INPUT TYPE=HIDDEN name=\"" + sParam_EditCell1
                                        + Integer.toString(nCount) + "\" VALUE=\""
                                        + Long.toString(rstTableData.getLong(5))
                                        + "\"></TD>");
                            } else {
                                strmResponse.println(
                                        "<TD ALIGN=CENTER>"
                                        + Long.toString(lRowCounter)
                                        + "<INPUT TYPE=HIDDEN name=\"" + sParam_EditCell1
                                        + Integer.toString(nCount) + "\" VALUE=\""
                                        + Long.toString(rstTableData.getLong(5))
                                        + "\"></TD><TD><BR></TD>");
                            }

                        } else {
                            String sData = (rstTableData.getObject(nCount + 1) == null
                                    ? "<BR>" : rstTableData.getString(nCount + 1));

                            if (nCount == 3) {
                                if (sData.equals("1")) {
                                    sData = "YES";
                                } else {
                                    sData = "NO";
                                }
                            }
                            // Otherwise show as plain text
                            strmResponse.println(
                                    "<TD align=\"center\">" + sBuildCellRenderer1(nCount, sData)
                                    + "<INPUT TYPE=HIDDEN name=\"" + sParam_EditCell1
                                    + Integer.toString(nCount) + "\" VALUE=\""
                                    + sReplaceChars(sData, "\"", "&quot") + "\"></TD>");
                        }
                    } // Case 3 - remaining rows in add/edit modes
                    else if (!sMode.equals(sMode_View)) {
                        // If row counter column
//                            if (nCount == 0)
//                                     strmResponse.println(
//                                      "<TD ALIGN=CENTER>" +
//                                      "<A HREF=\"javascript:vSubmit('" +
//                                      rstTableData.getString(nCount+1) +
//                                      "')\">" + Long.toString(lRowCounter) + "</A></TD><TD><BR></TD>");
                        if (nCount == 4) {
                            strmResponse.println(
                                    "<TD ALIGN=CENTER>"
                                    + "<input type=\"image\" alt=\"Edit Item\" src=\"images/iedit_t.gif\" onclick=\"javascript:vSubmit1('"
                                    + rstTableData.getString(nCount + 1)
                                    + "')\" style=\"cursor:pointer\" >&nbsp;<input type=\"image\" BORDER=0 alt=\"Delete Item\" SRC=\"images/idelete_t.gif\" style=\"cursor:pointer\" onclick=\"javascript:vDelete1('"
                                    + rstTableData.getString(nCount + 1)
                                    + "')\"></TD><TD><BR></TD>");
                        } else {
                            String sData = (rstTableData.getObject(nCount + 1) == null
                                    ? "<BR>" : rstTableData.getString(nCount + 1));

//                                     // Otherwise show as plain text
                            if (nCount == 3) {
                                if (sData.equals("1")) {
                                    sData = "YES";
                                } else {
                                    sData = "NO";
                                }
                            }

//                                     if(nCount==nColumnCount-1)
//                                     {
//                                            if(sData.equals("1"))
//                                                strmResponse.println("<td><input type=\"checkbox\" checked disabled=disabled></td>");
//                                            else
//                                                strmResponse.println("<td><input type=\"checkbox\" disabled=disabled></td>");
//                                     }
//                                     else
                            strmResponse.println(
                                    "<TD style=\"text-align:center\">" + sBuildCellRenderer1(nCount, sData) + "</TD>");
                        }
                    } // Case 4 - All rows in view mode
                    else {
                        // If row counter column
                        if (nCount == 4) {
                            strmResponse.println("<TD ALIGN=CENTER>"
                                    + Long.toString(lRowCounter)
                                    + "</TD><TD><BR></TD>");
                        } else {
                            String sData = (rstTableData.getObject(nCount + 1) == null
                                    ? "<BR>" : rstTableData.getString(nCount + 1));

                            // Otherwise show as plain text
                            strmResponse.println(
                                    "<TD style=\"text-align:center\">" + sBuildCellRenderer1(nCount, sData) + "</TD>");
                        }
                    }

                }
                strmResponse.println("</TR>");
            }

            // Close the recordset
            rstTableData.close();
            qryTableData.close();
            vCloseConnection(conCom);
        } catch (Exception errSQL) {
            errSQL.printStackTrace();
            strmResponse.println("Error reading table data from the database<BR>"
                    + errSQL.toString());
        }

        if (bAddAllowed1()) {
            if (sMode1.equals(sMode_Add)) {
                strmResponse.println("<TR>");
                for (int nCount = 0; nCount < nColumnCount1; nCount++) {
                    if (nCount == 4) // strmResponse.println("<TD ALIGN=CENTER>" +
                    //                       Long.toString(lRowCounter + 1) +
                    //                       "</TD><TD><BR></TD>");
                    {
                        strmResponse.println(
                                "<TD ALIGN=CENTER>"
                                + "<input type=\"image\" id=\"imgCAdd\" BORDER=0 SRC=\"images/iadd_t.gif\" alt=\"Add Item\" style=\"cursor:pointer\" onclick=\"javascript:vSubmit1('new')\"></TD><TD><BR></TD>");
                    } //strmResponse.println("");
                    else if (bEditAllowed1(nCount)) {
                        // If this is the editable row, show as editable cells
                        strmResponse.println("<TD>" + sBuildCellEditor1(nCount,
                                sDefaultValue(nCount), rspInput.getSession(false)) + "</TD>");
                    } else {
                        // Otherwise show as plain text
                        strmResponse.println("<TD style=\"text-align:center\">"
                                + (sDefaultValue(nCount).equals("") ? "<BR>"
                                : sDefaultValue(nCount)) + "</TD>");
                    }

                }
                strmResponse.println("</TR>");
            }
            if (!sMode1.equals(sMode_View) && !sMode1.equals(sMode_Add)) // dc comment
            {
                strmResponse.println(
                        "<TR><TD><BR></TD><TD ALIGN=CENTER>"
                        + "<input type=\"image\" BORDER=0 SRC=\"images/iadd_t.gif\" alt=\"Add Item\" id=\"imgCAdd\" style=\"cursor:pointer\" onclick=\"javascript:vSubmit1('new')\"></TD></TR>");
            }
            // dc end comment
            strmResponse.println("");
        }
        strmResponse.println("</TABLE>");

        //Commodity Grid Variables
        strmResponse.println("<INPUT TYPE=HIDDEN name=\"" + sParam_Mode1 + "\" " + "VALUE=\"" + sMode1 + "\">");
        strmResponse.println("<INPUT TYPE=HIDDEN name=\"" + sParam_NextRowID1 + "\" " + "VALUE=\"" + Long.toString(lEditingRow) + "\">");
        strmResponse.println("<INPUT TYPE=HIDDEN name=\"" + sParam_CalledBy1 + "\" " + "VALUE=\"" + sCalledBy1 + "\">");
        strmResponse.println("<INPUT TYPE=HIDDEN name=\"lRowCount1\" " + "VALUE=\"" + Long.toString(lRowCounter) + "\">");

        strmResponse.println("</form>");




    }

    private void vItemsGrid(HttpServletRequest rspInput,HttpServletResponse rspOutput,BookingInfo booking)
      throws ServletException, IOException
    {

        // Loop counter
        long lRowCounter = 0;
        Connection conItem=null;

        ServletOutputStream strmResponse = rspOutput.getOutputStream();
        rspOutput.setContentType("text/html");
       // Write the ItemGrid header

        strmResponse.println("<FORM METHOD=\"POST\" ACTION=\"./" +sServletName() + "\" NAME=\"frmGridServlet\" " + "onSubmit=\"return false;\">");

        strmResponse.println("<INPUT TYPE=HIDDEN name=\"lSessionID\" " +"VALUE=\"" + rspInput.getSession(false).getAttribute("sessionid").toString() + "\">");
        strmResponse.println("<INPUT TYPE=HIDDEN name=\"" + sParam_Mode + "\" " +"VALUE=\"" + sMode + "\">");
        strmResponse.println("<INPUT TYPE=HIDDEN name=\"" + sParam_NextRowID + "\" " +"VALUE=\"" + Long.toString(lEditingRow) + "\">");
        strmResponse.println("<INPUT TYPE=HIDDEN name=\"" + sParam_CalledBy + "\" " +"VALUE=\"" + sCalledBy + "\">");



        //Form address details
        strmResponse.println("<input type=hidden name=\"CCompanyName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CContactName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CAddress1\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CAddress2\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CAddress3\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CTown\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CCounty\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CPostcode\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CCountry\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CInstruct\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CLatitude\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CLongitude\" value=\"\">");

         // added by Adnan on 27 Aug 2013 against Residential Flag change
        strmResponse.println("<input type=hidden name=\"CResidentialFlag\" value=\"\">");
        
        
        strmResponse.println("<input type=hidden name=\"DCompanyName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DContactName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DAddress1\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DAddress2\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DAddress3\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DTown\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DCounty\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DPostcode\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DCountry\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DInstruct\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DLatitude\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DLongitude\" value=\"\">");

         // added by Adnan on 27 Aug 2013 against Residential Flag change
        strmResponse.println("<input type=hidden name=\"DResidentialFlag\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"itemWeightCount\" value=\"\">");
        
        strmResponse.println("<input type=hidden name=\"BDept\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BCaller\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BRef\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BHAWBNO\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"rdCollect\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CollectDate\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CollectHH\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CollectMM\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"chkPickupBefore\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"PickupBeforeHH\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"PickupBeforeMM\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"ProductType\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"TotalPieces\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"TotalWeight\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"TotalVolWeight\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sNTotalPieces\" value="+booking.getNTotalPieces()+">");
        strmResponse.println("<input type=hidden name=\"sNTotalVolWeight\" value="+booking.getNTotalVolWeight()+">");
        strmResponse.println("<input type=hidden name=\"bIndemnity\" id=\"bIndemnity\" value=\""+(rspInput.getSession(false).getAttribute("bIndemnity")==null?"true":rspInput.getSession(false).getAttribute("bIndemnity").toString())+"\">");

        strmResponse.println("<input type=hidden id=\"itemsRowCount\" name=\"itemsRowCount\" value=\"" + booking.getItemsRowCount() + "\">");
        
        // AN commenting out the following checks as We need to show the items grid in all cases
        if(booking.getProductType().equals("NDOX"))
            strmResponse.println("<TABLE border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" align=\"center\" id=\"tblItems\" style=\"border:1px solid #666666;padding:5px 5px 5px 5px;background-color:#EFEFEF;\">");
        else
            strmResponse.println("<TABLE border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" align=\"center\" id=\"tblItems\" style=\"border:1px solid #666666;padding:5px 5px 5px 5px;background-color:#EFEFEF; display:none;\">");

        strmResponse.println("<TR BGCOLOR=#FFFAF0 HEIGHT=20>");
        for (int nCount = 0; nCount < nColumnCount; nCount++)
        {
//            if (nCount==4)
            if (nCount == nColumnCount - 1 /*6*/) 
             		strmResponse.println("<TD ALIGN=CENTER id=\"boxtitle\" colspan=\"2\" WIDTH=" +Integer.toString(nGetColumnWidth(nCount)) + "><B>" +
            											sGetColumnHeader(nCount) + "</B></TD>");
            else
//                if(nCount==nColumnCount-1)
//                    strmResponse.println("<TD ALIGN=RIGHT style=\"text-align:right;padding-right:54px\" id=\"boxtitle\" WIDTH=" +Integer.toString(nGetColumnWidth(nCount)) + "><B>" +
//            											sGetColumnHeader(nCount) + "</B></TD>");
//                else
             		strmResponse.println("<TD ALIGN=CENTER id=\"boxtitle\" WIDTH=" +Integer.toString(nGetColumnWidth(nCount)) + "><B>" +
            											sGetColumnHeader(nCount) + "</B></TD>");
        }
        strmResponse.println("</TR>");

	try
        {
            conItem=cOpenConnection();
            // Open the recordset
            Statement qryTableData = conItem.createStatement();
            ResultSet rstTableData = qryTableData.executeQuery(sSelectSQL(rspInput.getSession(false).getAttribute("sessionid").toString()));
            // Loop through the recordset
            while (rstTableData.next())
            {
                lRowCounter++;
                strmResponse.println("<TR id=\"odd\">");
                for (int nCount=0; nCount < nColumnCount; nCount++)
                {
                    // Case 1 - editable cells (except adding row)
                        if (sMode.equals(sMode_Edit) &&	bEditAllowed(nCount) && rstTableData.getLong(5) == lEditingRow)
    		        {
                            String sData = (rstTableData.getObject(nCount+1)==null?"":rstTableData.getString(nCount+1));

                            // If this is the editable row, show as editable cells
                            strmResponse.println("<TD>" + sBuildCellEditor(nCount, sData) + "</TD>");
			}
                        // Case 2 - non editable cells in the editing row (not add)
                        else if ((sMode.equals(sMode_Edit) &&
                                rstTableData.getLong(5) == lEditingRow))
//                                rstTableData.getLong(6) == lEditingRow))
                        {
                            // If row counter column
//                            if (nCount == 4)
                            if (nCount == nColumnCount - 1 /*6*/) 
                            {
                                if (bDeleteAllowed(rstTableData.getLong(5)))
                                    strmResponse.println(
                                        "<TD ALIGN=CENTER>" +
                                        "</TD><TD ALIGN=CENTER>" +
                                        "<input type=\"image\" border=0 src=\"images/saveico_t.gif\" alt=\"Save Item\" onclick=\"javascript:vSave()\">"+
                                        "<INPUT TYPE=HIDDEN name=\"" + sParam_EditCell +
                                        Integer.toString(nCount) + "\" VALUE=\"" +
                                        Long.toString(rstTableData.getLong(5)) +
                                        "\"></TD>");
                                else
                                    strmResponse.println(
                                        "<TD ALIGN=CENTER>" +
                                        Long.toString(lRowCounter) +
                                        "<INPUT TYPE=HIDDEN name=\"" + sParam_EditCell +
                                        Integer.toString(nCount) + "\" VALUE=\"" +
                                        Long.toString(rstTableData.getLong(5)) +
                                        "\"></TD><TD><BR></TD>");

                            }
                            else
                            {
                                String sData = (rstTableData.getObject(nCount+1)==null?
                                            "<BR>":rstTableData.getString(nCount+1));

                                        // Otherwise show as plain text
                                        strmResponse.println(
                                        "<TD align=\"center\">" + sBuildCellRenderer(nCount, sData) +
                                        "<INPUT TYPE=HIDDEN name=\"" + sParam_EditCell +
                                        Integer.toString(nCount) + "\" VALUE=\"" +
                                        sReplaceChars(sData, "\"", "&quot") + "\"></TD>");
                            }
                        }
                        // Case 3 - remaining rows in add/edit modes
                        else if (!sMode.equals(sMode_View))
                        {
                            // If row counter column
//                            if (nCount == 0)
//                                     strmResponse.println(
//                                      "<TD ALIGN=CENTER>" +
//                                      "<A HREF=\"javascript:vSubmit('" +
//                                      rstTableData.getString(nCount+1) +
//                                      "')\">" + Long.toString(lRowCounter) + "</A></TD><TD><BR></TD>");
//                            if (nCount == 4)
                            if (nCount == nColumnCount - 1 /*6*/) 
                                     strmResponse.println(
                                      "<TD ALIGN=CENTER>" +
                                      "<input type=\"image\" alt=\"Edit Item\" src=\"images/iedit_t.gif\" onclick=\"javascript:vSubmit('" +
                                      rstTableData.getString(nCount+1) +
                                      "')\" style=\"cursor:pointer\" >&nbsp;<input type=\"image\" BORDER=0 alt=\"Delete Item\" SRC=\"images/idelete_t.gif\" style=\"cursor:pointer\" onclick=\"javascript:vDelete('" +
                                      rstTableData.getString(nCount+1) +
//                                      "', '"+rstTableData.getString("pro_iweight") +"')\"></TD><TD><BR></TD>");
                                              "', '"+rstTableData.getString(nCount+1) +"')\"></TD><TD><BR></TD>");
                            else
                            {
                                String sData = (rstTableData.getObject(nCount+1)==null?
                                            "<BR>":rstTableData.getString(nCount+1));

//                                     // Otherwise show as plain text
//                                     if(nCount==nColumnCount-1)
//                                     {
//                                            if(sData.equals("1"))
//                                                strmResponse.println("<td><input type=\"checkbox\" checked disabled=disabled></td>");
//                                            else
//                                                strmResponse.println("<td><input type=\"checkbox\" disabled=disabled></td>");
//                                     }
//                                     else
                                         strmResponse.println(
                                    "<TD style=\"text-align:center\">" + sBuildCellRenderer(nCount, sData) + "</TD>");
                            }
                        }
                    // Case 4 - All rows in view mode
                    else
    		        		{
                        // If row counter column
//                        if (nCount == 4)
                          if (nCount == nColumnCount - 1 /*6*/) 
						strmResponse.println("<TD ALIGN=CENTER>" +
                                                Long.toString(lRowCounter) +
                                                "</TD><TD><BR></TD>");
                        else
                        {
                            String sData = (rstTableData.getObject(nCount+1)==null?
                                        "<BR>":rstTableData.getString(nCount+1));

  				// Otherwise show as plain text
				strmResponse.println(
                                "<TD style=\"text-align:center\">" + sBuildCellRenderer(nCount, sData) + "</TD>");
                        }
                    }

                }
                strmResponse.println("</TR>");
      		  }

		        // Close the recordset
        		rstTableData.close();
        		qryTableData.close();
                        vCloseConnection(conItem);
        }
        catch (Exception errSQL)
        {
        		strmResponse.println("Error reading table data from the database<BR>" +
            										 errSQL.toString());
        }

        if (bAddAllowed())
        {
            if (sMode.equals(sMode_Add))
            {
            strmResponse.println("<TR>");
                for (int nCount=0; nCount < nColumnCount; nCount++)
                {
//                    if (nCount == 4)
                    if (nCount == nColumnCount - 1 /*6*/) 

                   // strmResponse.println("<TD ALIGN=CENTER>" +
                   //                       Long.toString(lRowCounter + 1) +
                   //                       "</TD><TD><BR></TD>");


                         strmResponse.println(
                        "<TD ALIGN=CENTER>" +
                        "<input type=\"image\" BORDER=0 SRC=\"images/iadd_t.gif\" alt=\"Add Item\" style=\"cursor:pointer\" onclick=\"javascript:vSubmit('new')\"></TD><TD><BR></TD>");

                    //strmResponse.println("");
                    else if (bEditAllowed(nCount))
    		    {
                    		// If this is the editable row, show as editable cells
				strmResponse.println("<TD>" + sBuildCellEditor(nCount,
                                sDefaultValue(nCount)) + "</TD>");
										}
                    else
                    {
                            // Otherwise show as plain text
                            strmResponse.println("<TD style=\"text-align:center\">" +
                            (sDefaultValue(nCount).equals("")?"<BR>":
                                sDefaultValue(nCount)) + "</TD>");
                    }

                }
		strmResponse.println("</TR>");
	}
        if (!sMode.equals(sMode_View) && !sMode.equals(sMode_Add))
                        // dc comment
                strmResponse.println(
                "<TR><TD><BR></TD><TD ALIGN=CENTER>" +
                    "<input type=\"image\" BORDER=0 SRC=\"images/iadd_t.gif\" id=\"imgAdd\" alt=\"Add Item\" style=\"cursor:pointer\" onclick=\"javascript:vSubmit('new')\"></TD></TR>");
                         // dc end comment
            strmResponse.println("");
        }

        strmResponse.println("<INPUT TYPE=HIDDEN name=\"lRowCount\" " +	"VALUE=\"" + Long.toString(lRowCounter) + "\">");
        strmResponse.println("<INPUT TYPE=HIDDEN name=\"itemfocus\" " +	"VALUE=\""+(rspInput.getParameter("itemfocus")==null?"false":rspInput.getParameter("itemfocus").toString())+"\">");
        strmResponse.println("<INPUT TYPE=HIDDEN name=\"comfocus\" " +	"VALUE=\""+(rspInput.getParameter("comfocus")==null?"false":rspInput.getParameter("comfocus").toString())+"\">");
        strmResponse.println("</TABLE>");


        //strmResponse.println("</form>");




    }
    private void vItemsGridNew(HttpServletRequest rspInput, HttpServletResponse rspOutput, BookingInfo booking)
            throws ServletException, IOException {

        // Loop counter
        long lRowCounter = 0;
        Connection conItem = null;

        ServletOutputStream strmResponse = rspOutput.getOutputStream();
        rspOutput.setContentType("text/html");
        // Write the ItemGrid header

        strmResponse.println("<FORM METHOD=\"POST\" ACTION=\"./" + sServletName() + "\" NAME=\"frmGridServlet\" " + "onSubmit=\"return false;\">");

        strmResponse.println("<INPUT TYPE=HIDDEN name=\"lSessionID\" " + "VALUE=\"" + rspInput.getSession(false).getAttribute("sessionid").toString() + "\">");
        strmResponse.println("<INPUT TYPE=HIDDEN name=\"" + sParam_Mode + "\" " + "VALUE=\"" + sMode + "\">");
        strmResponse.println("<INPUT TYPE=HIDDEN name=\"" + sParam_NextRowID + "\" " + "VALUE=\"" + Long.toString(lEditingRow) + "\">");
        strmResponse.println("<INPUT TYPE=HIDDEN name=\"" + sParam_CalledBy + "\" " + "VALUE=\"" + sCalledBy + "\">");



        //Form address details
        strmResponse.println("<input type=hidden name=\"CCompanyName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CContactName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CAddress1\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CAddress2\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CAddress3\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CTown\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CCounty\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CPostcode\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CCountry\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CInstruct\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CLatitude\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CLongitude\" value=\"\">");

        // added by Adnan on 27 Aug 2013 against Residential Flag change
        strmResponse.println("<input type=hidden name=\"CResidentialFlag\" value=\"\">");

        strmResponse.println("<input type=hidden name=\"DCompanyName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DContactName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DAddress1\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DAddress2\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DAddress3\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DTown\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DCounty\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DPostcode\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DCountry\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DInstruct\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DLatitude\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DLongitude\" value=\"\">");

        // added by Adnan on 27 Aug 2013 against Residential Flag change
        strmResponse.println("<input type=hidden name=\"DResidentialFlag\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"itemWeightCount\" value=\"\">");


        strmResponse.println("<input type=hidden name=\"BDept\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BCaller\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BRef\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BHAWBNO\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"rdCollect\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CollectDate\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CollectHH\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CollectMM\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"chkPickupBefore\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"PickupBeforeHH\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"PickupBeforeMM\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"ProductType\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"TotalPieces\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"TotalWeight\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"TotalVolWeight\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sNTotalPieces\" value=" + booking.getNTotalPieces() + ">");
        strmResponse.println("<input type=hidden name=\"sNTotalVolWeight\" value=" + booking.getNTotalVolWeight() + ">");
        strmResponse.println("<input type=hidden name=\"bIndemnity\" id=\"bIndemnity\" value=\"" + (rspInput.getSession(false).getAttribute("bIndemnity") == null ? "true" : rspInput.getSession(false).getAttribute("bIndemnity").toString()) + "\">");

//        strmResponse.println("<input type=hidden name=\"ParcelName\" value=" + booking.getsParcelName() + ">");
//        strmResponse.println("<input type=hidden name=\"ParcelNameId\" value=" + booking.getsParcelNameId() + ">");
        strmResponse.println("<input type=hidden id=\"itemsRowCount\" name=\"itemsRowCount\" value=\"" + booking.getItemsRowCount() + "\">");
        if (booking.getProductType().equals("NDOX")) {
            strmResponse.println("<TABLE border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" align=\"center\" id=\"tblItems\" style=\"border:1px solid #666666;padding:5px 5px 5px 5px;background-color:#EFEFEF;\">");
        } else {
            strmResponse.println("<TABLE border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" align=\"center\" id=\"tblItems\" style=\"border:1px solid #666666;padding:5px 5px 5px 5px;background-color:#EFEFEF;\">");//display:none;\">");
        }

        strmResponse.println("<TR BGCOLOR=#FFFAF0 HEIGHT=20>");
        for (int nCount = 0; nCount < nColumnCount; nCount++) {
            System.out.println("########################## BookingDetails line# 2131, inside vItemsGrid method ############################");
//            if (nCount == 6) {
            if (nCount == nColumnCount - 1 /*6*/) {
                strmResponse.println("<TD ALIGN=CENTER id=\"boxtitle\" colspan=\"2\" WIDTH=" + Integer.toString(nGetColumnWidth(nCount)) + "><B>"
                        + sGetColumnHeader(nCount) + "</B></TD>");
            } else //                if(nCount==nColumnCount-1)
            //                    strmResponse.println("<TD ALIGN=RIGHT style=\"text-align:right;padding-right:54px\" id=\"boxtitle\" WIDTH=" +Integer.toString(nGetColumnWidth(nCount)) + "><B>" +
            //            											sGetColumnHeader(nCount) + "</B></TD>");
            //                else
            {
                strmResponse.println("<TD ALIGN=CENTER id=\"boxtitle\" WIDTH=" + Integer.toString(nGetColumnWidth(nCount)) + "><B>"
                        + sGetColumnHeader(nCount) + "</B></TD>");
            }
        }
        strmResponse.println("</TR>");

        try {
            conItem = cOpenConnection();
            // Open the recordset
            Statement qryTableData = conItem.createStatement();
            ResultSet rstTableData = qryTableData.executeQuery(sSelectSQL(rspInput.getSession(false).getAttribute("sessionid").toString()));
            // Loop through the recordset
            while (rstTableData.next()) {
                lRowCounter++;
                strmResponse.println("<TR id=\"odd\">");
                for (int nCount = 0; nCount < nColumnCount; nCount++) {
                    
                    if (nCount == 2){
                        String sData = (rstTableData.getObject(nCount + 1) == null ? "" : rstTableData.getString(nCount + 1));
                        if (sData != null && !sData.trim().equals("")){
                        
                        }
                    }
                    // Case 1 - editable cells (except adding row)
                    System.out.println("BookingDetails: Line#1740: column 7 = " + rstTableData.getLong(nColumnCount) + ", sMode == " + sMode);
                    if (sMode.equals(sMode_Edit) && bEditAllowed(nCount) && rstTableData.getLong(nColumnCount) == lEditingRow) {
                        String sData = (rstTableData.getObject(nCount + 1) == null ? "" : rstTableData.getString(nCount + 1));

                        // If this is the editable row, show as editable cells
                        strmResponse.println("<TD>" + sBuildCellEditor(nCount, sData) + "</TD>");
                    } // Case 2 - non editable cells in the editing row (not add)
                    else if (((sMode.equals(sMode_Edit) || sMode.equals(sMode_None))
                            && rstTableData.getLong(nColumnCount) == lEditingRow)) {
                        // If row counter column
                        if (nCount == nColumnCount - 1 /*6*/) {
                            if (bDeleteAllowed(nColumnCount)) {
                                strmResponse.println(
                                        "<TD ALIGN=CENTER>"
                                        + "</TD><TD ALIGN=CENTER>"
                                        + "<input type=\"image\" border=0 src=\"images/saveico_t.gif\" alt=\"Save Item\" onclick=\"javascript:vSave()\">"
                                        + "<INPUT TYPE=HIDDEN name=\"" + sParam_EditCell
                                        + Integer.toString(nCount) + "\" VALUE=\""
                                        + Long.toString(nColumnCount)
                                        + "\"></TD>");
                            } else {
                                strmResponse.println(
                                        "<TD ALIGN=CENTER>"
                                        + Long.toString(lRowCounter)
                                        + "<INPUT TYPE=HIDDEN name=\"" + sParam_EditCell
                                        + Integer.toString(nCount) + "\" VALUE=\""
                                        + Long.toString(nColumnCount)
                                        + "\"></TD><TD><BR></TD>");
                            }

                        } else {
                            String sData = (rstTableData.getObject(nCount + 1) == null
                                    ? "<BR>" : rstTableData.getString(nCount + 1));

                            // Otherwise show as plain text
                            strmResponse.println(
                                    "<TD align=\"center\">" + sBuildCellRenderer(nCount, sData)
                                    + "<INPUT TYPE=HIDDEN name=\"" + sParam_EditCell
                                    + Integer.toString(nCount) + "\" VALUE=\""
                                    + sReplaceChars(sData, "\"", "&quot") + "\"></TD>");
                        }
                    } // Case 3 - remaining rows in add/edit modes
                    else if (!sMode.equals(sMode_View) ) {
                        System.out.println("====================== BookingDetails: Line#2207: column 7 = " + rstTableData.getLong(nColumnCount) + ", sMode == " + sMode);
//                        if ( booking.getsParcelName() == null || booking.getsParcelName().trim().equals("")){
                        
                        // If row counter column
//                            if (nCount == 0)
//                                     strmResponse.println(
//                                      "<TD ALIGN=CENTER>" +
//                                      "<A HREF=\"javascript:vSubmit('" +
//                                      rstTableData.getString(nCount+1) +
//                                      "')\">" + Long.toString(lRowCounter) + "</A></TD><TD><BR></TD>");
                        if (nCount == nColumnCount - 1 /*6*/) {
//                            if ( booking.getsParcelNameId() == null || booking.getsParcelNameId().trim().equals("") || booking.getsParcelNameId().trim().equals("SELECT")) {
//                                strmResponse.println(
//                                        "<TD ALIGN=CENTER>"
//                                        + "<input type=\"image\" alt=\"Edit Item\" src=\"images/iedit_t.gif\" onclick=\"javascript:vSubmit('"
//                                        + rstTableData.getString(nCount + 1)
//                                        + "')\" style=\"cursor:pointer\" >&nbsp;"
//                                        + "<input type=\"image\" BORDER=0 alt=\"Delete Item\" SRC=\"images/idelete_t.gif\" style=\"cursor:pointer\" onclick=\"javascript:vDelete('"
//                                        + rstTableData.getString(nCount + 1)
//                                        + "','"
//                                        + rstTableData.getString(3)
//                                        +"')\"></TD><TD><BR></TD>");
//                            }else {
//                                
//                                System.out.println("BookingDetails: Line#1843: sMode == " + sMode);
//                                 strmResponse.println(
//                                    "<TD ALIGN=CENTER>"
////                                    + "<input type=\"image\" alt=\"Edit Item\" src=\"images/iedit_t.gif\" onclick=\"javascript:vSubmit('"
////                                    + rstTableData.getString(nCount + 1)
////                                    + "')\" style=\"cursor:pointer\" >&nbsp;"
//                                    + "<input type=\"image\" BORDER=0 alt=\"Delete Item\" SRC=\"images/idelete_t.gif\" style=\"cursor:pointer\" onclick=\"javascript:vDelete('"
//                                    + rstTableData.getString(nCount + 1)
//                                    + "','"
//                                    + rstTableData.getString(3)
//                                    +"')\"></TD><TD><BR></TD>");
//                            }
                        } else {
                            String sData = (rstTableData.getObject(nCount + 1) == null
                                    ? "<BR>" : rstTableData.getString(nCount + 1));

//                                     // Otherwise show as plain text
//                                     if(nCount==nColumnCount-1)
//                                     {
//                                            if(sData.equals("1"))
//                                                strmResponse.println("<td><input type=\"checkbox\" checked disabled=disabled></td>");
//                                            else
//                                                strmResponse.println("<td><input type=\"checkbox\" disabled=disabled></td>");
//                                     }
//                                     else
                            strmResponse.println(
                                    "<TD style=\"text-align:center\">" + sBuildCellRenderer(nCount, sData) + "</TD>");
                        }
//                    }
                    } // Case 4 - All rows in view mode
                    else {
                        System.out.println("====================== BookingDetails: Line#2263: In mode View else part = " + rstTableData.getLong(nColumnCount) + ", sMode == " + sMode);
                        // If row counter column
                        if (nCount == nColumnCount - 1 /*6*/) {
                            strmResponse.println("<TD ALIGN=CENTER>"
                                    + Long.toString(lRowCounter)
                                    + "</TD><TD><BR></TD>");
                        } else {
                            String sData = (rstTableData.getObject(nCount + 1) == null
                                    ? "<BR>" : rstTableData.getString(nCount + 1));

                            // Otherwise show as plain text
                            strmResponse.println(
                                    "<TD style=\"text-align:center\">" + sBuildCellRenderer(nCount, sData) + "</TD>");
                        }
                    }

                }
                strmResponse.println("</TR>");
            }

            // Close the recordset
            rstTableData.close();
            qryTableData.close();
            vCloseConnection(conItem);
        } catch (Exception errSQL) {
            errSQL.printStackTrace();
            strmResponse.println("Error reading table data from the database<BR>"
                    + errSQL.toString());
        }

        if (bAddAllowed()) {
            if (sMode.equals(sMode_Add)) {
                 System.out.println("@@@@@   BookingDetails: Line#1888, if (sMode.equals(sMode_Add))" + sMode );//+ ", booking.getsParcelName()==" + booking.getsParcelName()
//                         + ", booking.getItemsRowCount() == " + booking.getItemsRowCount());
//                  if ( booking.getsParcelNameId() == null || booking.getsParcelNameId().trim().equals("") || booking.getsParcelNameId().trim().equals("SELECT")) {
//                strmResponse.println("<TR>");
                for (int nCount = 0; nCount < nColumnCount; nCount++) {
                    if (nCount == /*6*/ nColumnCount - 1 ) // strmResponse.println("<TD ALIGN=CENTER>" +
                    //                       Long.toString(lRowCounter + 1) +
                    //                       "</TD><TD><BR></TD>");
                    {
                        strmResponse.println(
                                "<TD ALIGN=CENTER id=\"sAddCell1\">"
                                + "<input type=\"image\" BORDER=0 SRC=\"images/iadd_t.gif\" alt=\"Add Item\" style=\"cursor:pointer\" onclick=\"javascript:vSubmit('new')\"></TD><TD><BR></TD>");
                    } //strmResponse.println("");
                    else if (bEditAllowed(nCount)) {
                        // If this is the editable row, show as editable cells
                        strmResponse.println("<TD>" + sBuildCellEditor(nCount,
                                sDefaultValue(nCount)) + "</TD>");
                    } else {
                        // Otherwise show as plain text
                        strmResponse.println("<TD style=\"text-align:center\">"
                                + (sDefaultValue(nCount).equals("") ? "<BR>"
                                : sDefaultValue(nCount)) + "</TD>");
                    }
                }
                strmResponse.println("</TR>");
//                  }
            }
            if (!sMode.equals(sMode_View) && !sMode.equals(sMode_Add) && !sMode.equals(sMode_None)) // dc comment
            {
//                strmResponse.println("###########################   if (!sMode.equals(sMode_View) && !sMode.equals(sMode_Add))");
                strmResponse.println(
                        "<TR><TD><BR></TD><TD ALIGN=CENTER>"
                        + "<input type=\"image\" BORDER=0 SRC=\"images/iadd_t.gif\" id=\"imgAdd\" alt=\"Add Item\" style=\"cursor:pointer\" onclick=\"javascript:vSubmit('new')\"></TD></TR>");
            }
            // dc end comment
            strmResponse.println("");
        }

        strmResponse.println("<INPUT TYPE=HIDDEN name=\"lRowCount\" " + "VALUE=\"" + Long.toString(lRowCounter) + "\">");
        strmResponse.println("<INPUT TYPE=HIDDEN name=\"itemfocus\" " + "VALUE=\"" + (rspInput.getParameter("itemfocus") == null ? "false" : rspInput.getParameter("itemfocus").toString()) + "\">");
        strmResponse.println("<INPUT TYPE=HIDDEN name=\"comfocus\" " + "VALUE=\"" + (rspInput.getParameter("comfocus") == null ? "false" : rspInput.getParameter("comfocus").toString()) + "\">");
        strmResponse.println("</TABLE>");


        //strmResponse.println("</form>");




    }

    private void vPageBuilder(HttpServletRequest rspInput, HttpServletResponse rspOutput)
            throws ServletException, IOException {
        // Loop counter
        long lRowCounter = 0;

        // Open output stream
        rspOutput.setContentType("text/html");
        ServletOutputStream strmResponse = rspOutput.getOutputStream();
        rspOutput.setContentType("text/html");

        writeOutputLog("Booking for Account No: " + rspInput.getSession(false).getAttribute("acc_id").toString()
                + " SessionId: " + rspInput.getSession(false).getAttribute("sessionid").toString());

        LoadPage(rspInput, rspOutput);


        strmResponse.println("<tr>");
        strmResponse.println("<td></td>");
        strmResponse.println("<td style=\"text-align:right;padding-right:20px;\" align=\"right\"><br><input type=\"button\" name=\"btnNext\" value=\"NEXT &rsaquo;\" id=\"btnNext\" onclick=\"javascript:Services('QAS');\" style=\"font-family:		Verdana, Arial, Helvetica, sans-serif;"
                + "font-size:			12px;"
                + "font-weight:		bold;"
                + "color: 				#FFFFFF;"
                + "background-color:	#0059AD; "
                + "border-top: 		1px solid #0066CC; "
                + "border-right: 		1px solid #003366; "
                + "border-bottom: 		1px solid #003366;"
                + "border-left: 		1px solid #0066CC; "
                + "padding:		2px 0px 2px 0px;\"></td>");
        strmResponse.println("</tr>");
        strmResponse.println("</table>");


        //address book form
        strmResponse.println("<form target=\"popupwinAB\" name=\"AddressBookForm\" method=\"POST\" action=\"" + Constants.srvAddressBook + "\"\">");
        strmResponse.println("<input type=hidden name=\"sCompanyName\" id=\"sCompanyName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"" + Constants.SACTION + "\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sBookingType\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sMessage\" value=\"\">");
        strmResponse.println("</form>");

        //Booking form.
        strmResponse.println("<form name=\"BookingForm\" method=\"post\" ACTION=\"" + Constants.srvEnterConsignment + "\">");
        strmResponse.println("<input type=hidden name=\"CCompanyName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CContactName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CAddress1\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CAddress2\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CAddress3\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CTown\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CCounty\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CPostcode\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CCountry\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CInstruct\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CLatitude\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CLongitude\" value=\"\">");

        // added by Adnan on 27 Aug 2013 against Residential Flag change
        strmResponse.println("<input type=hidden name=\"CResidentialFlag\" value=\"\">");

        strmResponse.println("<input type=hidden name=\"DCompanyName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DContactName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DAddress1\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DAddress2\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DAddress3\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DTown\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DCounty\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DPostcode\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DInstruct\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DCountry\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DLatitude\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DLongitude\" value=\"\">");

        // added by Adnan on 27 Aug 2013 against Residential Flag change
        strmResponse.println("<input type=hidden name=\"DResidentialFlag\" value=\"\">");

        strmResponse.println("<input type=hidden name=\"BDept\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BCaller\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BRef\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BHAWBNO\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"rdCollect\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CollectDate\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CollectHH\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CollectMM\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"chkPickupBefore\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"PickupBeforeHH\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"PickupBeforeMM\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"ProductType\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"TotalPieces\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"TotalWeight\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"TotalVolWeight\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"ShipperVAT\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"" + Constants.SACTION + "\" value=\"\">");

//        strmResponse.println("<input type=hidden name=\"ParcelName\" value=\"\">");
//        strmResponse.println("<input type=hidden name=\"ParcelNameId\" value=\"\">");
        strmResponse.println("<input type=hidden id=\"itemsRowCount\" name=\"itemsRowCount\" value=\"\">");
        
        strmResponse.println("</form>");

        //Postcode lookup form
        strmResponse.println("<form target=\"PostcodeLookup\" name=\"PostcodeLookup\" method=\"POST\" action=\"" + Constants.srvQasController + "\">");
        strmResponse.println("<input type=\"hidden\" name=DataId value=\"" + Constants.COUNTRY_CODE + "\">");
        strmResponse.println("<input type=\"hidden\" name=CountryName value=\"" + Constants.COUNTRY_NAME + "\">");
        strmResponse.println("<input type=\"hidden\" name=PromptSet value=\"Optimal\">");
        strmResponse.println("<input type=\"hidden\" name=Command value=\"" + FlatSearch.NAME + "\">");
        strmResponse.println("<input type=hidden name=\"" + Constants.USER_INPUT + "\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"" + Constants.SACTION + "\" value=\"" + Constants.sBooking + "\">");
        strmResponse.println("<input type=hidden name=\"" + Constants.SACTION_SUB + "\" value=\"" + FlatSearch.NAME + "\">");
        strmResponse.println("<input type=hidden name=\"sType\" value=\"\">");
        strmResponse.println("</form>");

        //HierSearch lookup form
        strmResponse.println("<form target=\"HierLookup\" name=\"HierLookup\" method=\"POST\" action=\"" + Constants.srvQasController + "\">");
        strmResponse.println("<input type=\"hidden\" name=DataId value=\"" + Constants.COUNTRY_CODE + "\">");
        strmResponse.println("<input type=\"hidden\" name=CountryName value=\"" + Constants.COUNTRY_NAME + "\">");
        strmResponse.println("<input type=\"hidden\" name=PromptSet value=\"Optimal\">");
        strmResponse.println("<input type=\"hidden\" name=Command value=\"" + HierSearch.NAME + "\">");
        strmResponse.println("<input type=hidden name=\"" + Constants.USER_INPUT + "\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"" + Constants.SACTION + "\" value=\"" + Constants.sBooking + "\">");
        strmResponse.println("<input type=hidden name=\"" + Constants.SACTION_SUB + "\" value=\"" + HierSearch.NAME + "\">");
        strmResponse.println("<input type=hidden name=\"sType\" value=\"\">");
        strmResponse.println("</form>");

        //QAS form
        strmResponse.println("<form target=\"QASValidation\" name=\"VerifyForm\" method=\"POST\" action=\"" + Constants.srvQASValidation + "\">");
        strmResponse.println("<input type=hidden name=\"" + Constants.COMMAND + "\" value=\"" + Verify.NAME + "\">");
        strmResponse.println("<input type=hidden name=\"" + Constants.USER_INPUT + "\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"" + Constants.USER_INPUT1 + "\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"" + Constants.COUNTRY_NAME + "\" value=\"United Kingdom\">");
        strmResponse.println("<input type=hidden name=\"" + Constants.DATA_ID + "\" value=\"" + Constants.COUNTRY_CODE + "\">");
        
        strmResponse.println("<input type=hidden name=\"itemWeightCount\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CCompanyName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CContactName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CAddress1\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CAddress2\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CAddress3\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CTown\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CCounty\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CPostcode\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CCountry\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CInstruct\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CLatitude\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CLongitude\" value=\"\">");

        // added by Adnan on 27 Aug 2013 against Residential Flag change
        strmResponse.println("<input type=hidden name=\"CResidentialFlag\" value=\"\">");

        strmResponse.println("<input type=hidden name=\"DCompanyName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DContactName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DAddress1\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DAddress2\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DAddress3\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DTown\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DCounty\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DPostcode\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DCountry\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DInstruct\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DLatitude\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"DLongitude\" value=\"\">");

        // added by Adnan on 27 Aug 2013 against Residential Flag change
        strmResponse.println("<input type=hidden name=\"DResidentialFlag\" value=\"\">");

        strmResponse.println("<input type=hidden name=\"BDept\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BCaller\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BRef\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"BHAWBNO\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"rdCollect\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CollectDate\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CollectHH\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"CollectMM\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"chkPickupBefore\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"PickupBeforeHH\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"PickupBeforeMM\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"ProductType\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"TotalPieces\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"TotalWeight\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"TotalVolWeight\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"ShipperVAT\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"itemTags\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"commodityTags\" value=\"\">");
        
        strmResponse.println("<input type=hidden name=\"itemsRowCount\" value=\"\">");
//         strmResponse.println("<input type=hidden name=\"ParcelName\" value=\"\">");
//         strmResponse.println("<input type=hidden name=\"ParcelNameId\" value=\"\">");
        
        strmResponse.println("<input type=hidden name=\"" + Constants.SACTION + "\" value=\"" + Constants.sBooking + "\">");
        strmResponse.println("</form>");

        String sAction = rspInput.getParameter(Constants.SACTION) == null ? "" : rspInput.getParameter(Constants.SACTION).toString();
        String sAction_sub = rspInput.getParameter(Constants.SACTION_SUB) == null ? "" : rspInput.getParameter(Constants.SACTION_SUB).toString();

        if (sAction_sub.equals("Validate")) {
            validateCCDetails(rspInput, rspOutput);
        } else {
            LoadServicePage(rspInput, rspOutput, "");
        }



        strmResponse.println("</BODY></HTML>");


        strmResponse.close();
    }

    /**
     *
     * @param sInputText
     * @param nColumnWidth
     * @return
     */
    protected String sWordWrap(String sInputText, int nColumnWidth) {
        String sResult = "";
        sErrorMessage = "";
        try {
            // Get length of the supplied text
            sInputText = sInputText.trim();
            int nLength = sInputText.length();

            if (nLength <= nColumnWidth) {
                sResult = sInputText;
            } else {
                // Check for presence of whitespace inside
                String sFirstPart = sInputText.substring(0, nColumnWidth);
                boolean bExit = false;
                int nSpaces = nColumnWidth;
                while ((!bExit) && (nSpaces > 0)) {
                    nSpaces--;
                    bExit = (Character.isWhitespace(sFirstPart.charAt(nSpaces))
                            || (sFirstPart.charAt(nSpaces) == '-'));
                }

                if (nSpaces <= 0) {
                    sResult = sFirstPart + "<br>" + sWordWrap(sInputText.substring(nColumnWidth), nColumnWidth);
                } else {
                    sResult = sFirstPart.substring(0, nSpaces) + "<br>"
                            + sWordWrap(sFirstPart.substring(nSpaces)
                            + sInputText.substring(nColumnWidth), nColumnWidth);
                }
            }
        } catch (Exception errString) {
            sResult = sErrorMessage + errString.toString();
        }
        return sResult;
    }

    /**
     *
     * @param rspOutput
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void vShowGridValidationErrorPage(HttpServletResponse rspOutput)
            throws ServletException, IOException {
        rspOutput.setContentType("text/html");
        ServletOutputStream strmResponse = rspOutput.getOutputStream();
        strmResponse.println("<HTML><BODY>");
        strmResponse.println("<FONT COLOR=\"" + sFontColour + "\">"
                + "<H3>Validation Error</H3></FONT><BR>");
        //strmResponse.println(sValidationMessage);
        strmResponse.println("</BODY></HTML>");
        strmResponse.close();
    }

    /**
     *
     * @throws citysprint.co.uk.NDI.ValidationException
     */
    protected abstract void vValidateSubmittedData()
            throws ValidationException;

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException Displays the booking form and handles Address
     * Entry, Product Entry and Commodities Entry
     */
    protected void LoadPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");

        HttpSession validSession = request.getSession(false);

        if (validSession == null) {
            request.setAttribute("message", "Your session has ended, please login to proceed");
            request.setAttribute("Username", request.getParameter("Username") == null ? "" : request.getParameter("Username").toString());
            getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
        } else {


            LoginInfo login = new LoginInfo();
            CollectionAddress collection = new CollectionAddress();
            DeliveryAddress delivery = new DeliveryAddress();
            BookingInfo booking = new BookingInfo();
            CreditCardDetails ccdetails = new CreditCardDetails();
            CountryInfo country = new CountryInfo();

            String sItemWeightCount = "0";

            sItemWeightCount = request.getParameter("itemWeightCount") == null ? "0" : request.getParameter("itemWeightCount").toString();

            if (sItemWeightCount == null || sItemWeightCount.trim().equals("")) {
                sItemWeightCount = "0";
            }

            int iItemWeightCount = Integer.parseInt(sItemWeightCount);

            //set login,collection,delivery address fields and other booking info fields by processing the request object
            login.parseLogin((String[][]) validSession.getAttribute("loginArray"), request);
            collection.setCollectionAddress(request);
            delivery.setDeliveryAddress(request);
            booking.setBookingInfo(request, response, login);
            country.parseCountries((String[][]) validSession.getAttribute("getCountriesArray"));

//            parcelInfo.parseParcel((String[][]) validSession.getAttribute("getNDIParcelArray"));

            String sAction = request.getParameter(Constants.SACTION) == null ? "" : request.getParameter(Constants.SACTION).toString();
            String sAction_sub = request.getParameter(Constants.SACTION_SUB) == null ? "" : request.getParameter(Constants.SACTION_SUB).toString();


            //When coming from Thankyou page product details should be cleared
            //for rebook
            if (sAction.equals("Clear")) {
                clearSessionAddress(request, response);
                request.getSession(false).setAttribute("sTrackingEmail", null);
                request.getSession(false).setAttribute("sTrackingSMS", null);
            }

            getTotalValue(request, response);
            getTotalValueWithIndemnity(request, response);

            StringBuffer sbHeader = new StringBuffer();
            sbHeader = build_Header();
            String sHeader = new String();
            sHeader = sbHeader.substring(0, sbHeader.length());

            strmResponse.println("<html>");
            strmResponse.println("<head>");
            strmResponse.println("<meta http-equiv=\"content-type\" content=\"text/html\">");
            strmResponse.println("<META HTTP-EQUIV=\"CACHE-CONTROL\" CONTENT=\"NO-CACHE\">");
            strmResponse.println("  <title>CitySprint</title>");
            strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
            //  <!-- calendar stylesheet -->
            strmResponse.println("<link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"CSS/calendar-blue.css\" title=\"win2k-cold-1\" />");
            strmResponse.println("<link rel=\"stylesheet\" href=\"CSS/dhtmlwindow.css\" type=\"text/css\" />");

            //  <!-- main calendar program -->
            strmResponse.println("<script type=\"text/javascript\" src=\"JS/calendar.js\"></script>");
            //  <!-- language for the calendar -->
            strmResponse.println("<script type=\"text/javascript\" src=\"JS/calendar-en.js\"></script>");
            //  <!-- the following script defines the Calendar.setup helper function, which makes
            //    adding a calendar a matter of 1 or 2 lines of code. -->
            strmResponse.println("<script type=\"text/javascript\" src=\"JS/calendar-setup.js\"></script>");

            //Autocomplete Javascripts
            //strmResponse.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"CSS/fonts-min.css\" />");
            strmResponse.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"CSS/autocomplete.css\" />");
            strmResponse.println("<script type=\"text/javascript\" src=\"JS/yahoo-dom-event.js\"></script>");
            strmResponse.println("<script type=\"text/javascript\" src=\"JS/animation-min.js\"></script>");

            strmResponse.println("<script type=\"text/javascript\" src=\"JS/datasource-min.js\"></script>");
            strmResponse.println("<script type=\"text/javascript\" src=\"JS/autocomplete-min.js\"></script>");
            //strmResponse.println("<script type=\"text/javascript\" src=\"JS/preload-images.js\"></script>");

            strmResponse.println("<script type=\"text/javascript\" src=\"JS/dhtmlwindow.js\">");
            strmResponse.println("/***********************************************");
            strmResponse.println("* DHTML Window Widget- © Dynamic Drive (www.dynamicdrive.com)");
            strmResponse.println("* This notice must stay intact for legal use.");
            strmResponse.println("* Visit http://www.dynamicdrive.com/ for full source code");
            strmResponse.println("***********************************************/");

            strmResponse.println("</script>");

            strmResponse.println("<script type=\"text/javascript\">");

            /**
             * *********************************************
             * Tabbed Document Viewer script- © Dynamic Drive DHTML code library
             * (www.dynamicdrive.com) This notice MUST stay intact for legal use
             * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full
             * source code *********************************************
             */
            strmResponse.println("preload1 = new Image();");
            strmResponse.println("preload2 = new Image();");
            strmResponse.println("preload3 = new Image();");
            strmResponse.println("preload4 = new Image();");
            strmResponse.println("preload5 = new Image();");
            strmResponse.println("preload6 = new Image();");

            strmResponse.println("preload1.src = \"images/booking_active.gif\";");
            strmResponse.println("preload2.src = \"images/booking_default.gif\";");
            strmResponse.println("preload3.src = \"images/reporting_active.gif\";");
            strmResponse.println("preload4.src = \"images/reporting_default.gif\";");
            strmResponse.println("preload5.src = \"images/tracking_active_410.gif\";");
            strmResponse.println("preload6.src = \"images/tracking_default_410.gif\";");

            strmResponse.println("var selectedtablink=\"\";");
            strmResponse.println("var tcischecked=false;");

            strmResponse.println("function handlelink(aobject){");
            strmResponse.println("selectedtablink=aobject.href;");
            strmResponse.println("tcischecked=(document.tabcontrol && document.tabcontrol.tabcheck.checked)? true : false;");
            strmResponse.println("if (document.getElementById && !tcischecked){");
            strmResponse.println("var tabobj=document.getElementById(\"tablist\")");
            strmResponse.println("var tabobjlinks=tabobj.getElementsByTagName(\"A\");");
            strmResponse.println("for (i=0; i<tabobjlinks.length; i++)");
            strmResponse.println("tabobjlinks[i].className=\"\"");
            strmResponse.println("aobject.className=\"current\";");
            strmResponse.println("document.getElementById(\"tabiframe\").src=selectedtablink;");
            strmResponse.println("return false;");
            strmResponse.println("}");
            strmResponse.println("else");
            strmResponse.println("return true;");
            strmResponse.println("}");

            strmResponse.println("function handleview(){");
            strmResponse.println("tcischecked=document.tabcontrol.tabcheck.checked;");
            strmResponse.println("if (document.getElementById && tcischecked){");
            strmResponse.println("if (selectedtablink!=\"\")");
            strmResponse.println("window.location=selectedtablink;");
            strmResponse.println("}");
            strmResponse.println("}");

            strmResponse.println("function frmSubmit(){");
            strmResponse.println("document.formSub.submit();");
            strmResponse.println("}");

            strmResponse.println("</script>");
            // Write javascript functions
            strmResponse.println("<SCRIPT LANGUAGE=\"JAVASCRIPT\">");

            //Declare variables for all popup windows (to set focus when they are already open)
            strmResponse.println("var itemWeightCount=" + iItemWeightCount + ";");
            strmResponse.println("var winadbook=null;");
            strmResponse.println("var winpostcode=null;");
            strmResponse.println("var winindemnity=null;");
            strmResponse.println("var winqas=null;");
            strmResponse.println("var winprice=null;");
            strmResponse.println("var wintracking=null;");
            strmResponse.println("var arrCommodity=null;");
            strmResponse.println("var arrCountryNT=null;");

//
//            strmResponse.println("var arrParcelType=null;");
//            strmResponse.println("var arrParcelLength=null;");
//            strmResponse.println("var arrParcelDepth=null;");
//            strmResponse.println("var arrParcelHeight=null;");
//
//
            strmResponse.println("if(!Array.indexOf){");
            strmResponse.println("    Array.prototype.indexOf = function(obj){");
            strmResponse.println("        for(var i=0; i<this.length; i++){");
            strmResponse.println("            if(this[i]==obj){");
            strmResponse.println("                return i;");
            strmResponse.println("            }");
            strmResponse.println("        }");
            strmResponse.println("        return -1;");
            strmResponse.println("    }");
            strmResponse.println("}");

            writeOutputLog("Before commodity:" + (String[][]) validSession.getAttribute("getCommodityArray"));
            CommodityInfo commodity = new CommodityInfo();
            commodity.parseCommodity((String[][]) validSession.getAttribute("getCommodityArray"));

            strmResponse.println("arrCommodity=new Array(" + commodity.getCsVector().size() + ")");
            strmResponse.println("arrIndemnity=new Array(" + commodity.getIfVector().size() + ")");

            for (int i = 0; i < commodity.getCsVector().size(); i++) {
                strmResponse.println("arrCommodity[" + i + "]=\"" + commodity.getCsVector().elementAt(i) + "\";");
                strmResponse.println("arrIndemnity[" + i + "]=\"" + commodity.getIfVector().elementAt(i) + "\";");
            }

            strmResponse.println("arrCountryNT=new Array(" + country.getCyntVector().size() + ")");

            for (int i = 0; i < country.getCyntVector().size(); i++) {
                strmResponse.println("arrCountryNT[" + i + "]=\"" + country.getCyntVector().elementAt(i) + "\";");

            }



            // following code related to Parcel Type arrays is added by Adnan
//            writeOutputLog("Before ParcelType:" + (String[][]) validSession.getAttribute("getNDIParcelArray"));


//            strmResponse.println("arrParcelType=new Array(" + parcelInfo.getPnVector().size() + ")");
//            strmResponse.println("arrParcelLength=new Array(" + parcelInfo.getPlVector().size() + ")");
//            strmResponse.println("arrParcelDepth=new Array(" + parcelInfo.getPdVector().size() + ")");
//            strmResponse.println("arrParcelHeight=new Array(" + parcelInfo.getPhVector().size() + ")");
//
//            for (int i = 0; i < parcelInfo.getPnVector().size(); i++) {
//                strmResponse.println("arrParcelType[" + i + "]=\"" + parcelInfo.getPnVector().elementAt(i) + "\";");
//                strmResponse.println("arrParcelLength[" + i + "]=\"" + parcelInfo.getPlVector().elementAt(i) + "\";");
//
//                strmResponse.println("arrParcelDepth[" + i + "]=\"" + parcelInfo.getPdVector().elementAt(i) + "\";");
//                strmResponse.println("arrParcelHeight[" + i + "]=\"" + parcelInfo.getPhVector().elementAt(i) + "\";");
//            }


            for (int i = 0; i < commodity.getCsVector().size(); i++) {
                strmResponse.println("arrCommodity[" + i + "]=\"" + commodity.getCsVector().elementAt(i) + "\";");
                strmResponse.println("arrIndemnity[" + i + "]=\"" + commodity.getIfVector().elementAt(i) + "\";");
            }

            strmResponse.println("arrCountryNT=new Array(" + country.getCyntVector().size() + ")");

            for (int i = 0; i < country.getCyntVector().size(); i++) {
                strmResponse.println("arrCountryNT[" + i + "]=\"" + country.getCyntVector().elementAt(i) + "\";");

            }

            vWriteJavascriptFunctions(strmResponse, request, login);
            vWriteServicesJavascriptFunctions(strmResponse, login);


//            strmResponse.println("function parcelClick()");
//            strmResponse.println("{");
//            strmResponse.println("var sLengthField=document.getElementById(\"sEditCell2\");");
//            strmResponse.println("var sWidthField=document.getElementById(\"sEditCell3\");");
//            strmResponse.println("var sHeightField=document.getElementById(\"sEditCell4\");");
//            
//            strmResponse.println("var sItemWeightField=document.getElementById(\"sEditCell1\");");
//            
//            strmResponse.println("var sSelectedParcel=document.getElementById(\"selParcel\").value;");
//            
//             strmResponse.println("var txtTotalPieces=document.getElementById(\"txtTotalPieces\");");
////            strmResponse.println("alert('sSelectedParcel == ' + sSelectedParcel); ");
//
////            strmResponse.println("alert('length == '+ arrParcelLength[sSelectedParcel] ); ");
////            strmResponse.println("alert('depth == '+ arrParcelDepth[sSelectedParcel] ); ");
//            strmResponse.println("        var txtNoItems=document.getElementById(\"" + sParam_EditCell + "0\");");
//            
//            strmResponse.println("if(sSelectedParcel==\"SELECT\")");
//            strmResponse.println("{");
//
//            strmResponse.println("sLengthField.value='';");
//            strmResponse.println("sWidthField.value='';");
//            strmResponse.println("sHeightField.value='';");
//
//            strmResponse.println("txtTotalPieces.disabled=false;");
//            strmResponse.println("} else{");
//
//            strmResponse.println("sLengthField.value=+ arrParcelLength[sSelectedParcel];");
//            strmResponse.println("sWidthField.value=+ arrParcelDepth[sSelectedParcel];");
//            strmResponse.println("sHeightField.value=+ arrParcelHeight[sSelectedParcel];");
//            strmResponse.println("txtNoItems.value=+ 1;");
//            
//            strmResponse.println("txtNoItems.value=+ 1;");
//            
//            strmResponse.println("sItemWeightField.value = document.getElementById(\"txtTotalWeight\").value;");
//            
//             strmResponse.println("txtTotalPieces.disabled=true;");
////            strmResponse.println("txtTotalPieces.disable
//             strmResponse.println("vSubmit('new');");
////            strmResponse.println("var itemWeight = ");
//            
//            
//
//            strmResponse.println("}");
//            strmResponse.println("}");



            strmResponse.println("</SCRIPT>");

            strmResponse.println("<style type=\"text/css\">");

            strmResponse.println("#comContainer {text-align:left; width:14em;}");
            strmResponse.println("#comContainer .yui-ac-bd {font-size:10px; color:#666; background-color:#E1E7F3; text-align:left;}");
            strmResponse.println("#comContainer li {text-overflow:ellipsis; cursor:pointer; }");
            strmResponse.println("#comContainer em {font-style:normal; font-weight:bold; color:#000033;}");
            strmResponse.println("html { overflow-x: hidden;overflow-y: auto;  }");
            strmResponse.println("</style>");

            strmResponse.println("</head>");

            if (sAction.equals("Booking")) {
                strmResponse.println("<BODY ONLOAD=\"disableBooking();\" class=\"yui-skin-sam\" style=\"width:895px;\">");
            } else {
                strmResponse.println("<BODY class=\"yui-skin-sam\" " + (!sMode.equals(sMode_View) ? " ONLOAD=\"vOnLoad();\"" : "") + " ONAFTERLOAD=\"vOnAfterLoad();\" style=\"width:895px;\">");
            }

//        strmResponse.println("<br><ul id=\"tablist\">");
//        strmResponse.println("<li><a class=\"current\" href=\"#\" onClick=\"return handlelink(this)\"><span>Bookings & Quotations</span></a></li>");
//        strmResponse.println("<li><a href=\"Reporting.jsp\" onClick=\"return handlelink(this)\"><span>Reporting</span></a></li>");
//        strmResponse.println("<li><a href=\"javascript:frmSubmit();\" onClick=\"return handlelink(this)\"><span>Tracking</span></a></li>");
//        strmResponse.println("</ul>");
//        //strmResponse.println("<iframe id=\"tabiframe\" src=\"document.formImage.submit();\" width=\"100%\" height=\"100%\" scrolling=\"auto\">");
//        strmResponse.println("<form name=\"formSub\" method=\"post\" action=\"Tracking\">");
//        strmResponse.println("</form>");
//        strmResponse.println("<form name=\"formImage\" method=\"post\" action=\"EnterConsignment\">");
//        strmResponse.println("</form>");
//        strmResponse.println("<form name=\"tabcontrol\" style=\"margin-top:0\">");
//        strmResponse.println("<input name=\"tabcheck\" type=\"checkbox\" onClick=\"handleview()\" checked=true> ");
//        strmResponse.println("</form>");

            strmResponse.println("<table border=\"0\" cellpadding=\"0\" cellspacing=\"1\" id=\"tblBooking\" style=\"padding-left:4px;padding-right:4px;\" >");
            strmResponse.println("<tr>");
            strmResponse.println("<td colspan=\"2\">");
            strmResponse.println("<img src=\"images/booking_active.gif\" id=\"imgTab1\" style=\"padding-bottom:0px;padding-top:1pxpadding-left:0px;\"><img src=\"images/reporting_default.gif\" id=\"imgTab2\" onclick=\"javascript:window.location='Reporting.jsp?t=" + new java.util.Date() + "';\" style=\"padding-bottom:0px;padding-top:1pxpadding-left:0px;cursor:pointer\"><img src=\"images/tracking_default_410.gif\" id=\"imgTab3\" onclick=\"javascript:window.location='Tracking?t=" + new java.util.Date() + "';\" style=\"padding-bottom:0px;padding-top:1pxpadding-left:0px;cursor:pointer\">");
            strmResponse.println("</td>");
            strmResponse.println("</tr>");
            //strmResponse.println(sHeader);

            strmResponse.println("<tr id=\"trBooking\">");
            strmResponse.println("<td width=\"100%\" valign=\"top\" colspan=\"2\">");

            // strmResponse.println("<img src=\"images/newmain_tab.gif\" id=\"imgTab\" onclick=\"javascript:window.location='Tracking';\" style=\"padding-bottom:0px;padding-top:1pxpadding-left:0px;cursor:pointer\">");

            strmResponse.println("<br><br><label id=\"lblMessage\" style=\"color:red;\"></label>");
            if (sErrorMessage.length() > 0) {
                strmResponse.println("<br><br><label id=\"lblErrorMessage\" style=\"color:red;\">" + sErrorMessage + "</label>");
            }
            strmResponse.println("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"box\" width=\"100%\">");

            strmResponse.println("<tr style=\"padding-top:4px;padding-bottom:4px;\"><td id=\"boxtitle\" colspan=\"6\">BOOKING DETAILS</td></tr>");
            strmResponse.println("<tr>");

            strmResponse.println("<td id=\"label\" width=\"16.5%\">");
            //check EYFlag to change the labels
            if (login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("V") || login.getDepartmentFlag().equals("F")) {
                if (login.isEYFlag()) {
                    strmResponse.println(login.getEy_Department() + "*");
                } else {
                    strmResponse.println("Department*");
                }
            } else if (login.getDepartmentFlag().equals("R")) {
                if (login.isEYFlag()) {
                    strmResponse.println(login.getEy_Department());
                } else {
                    strmResponse.println("Department");
                }
            }


            strmResponse.println("</td>");

            strmResponse.println("<td width=\"18%\" nowrap>");

            //if DT tag is passed department is a DropDown list else textbox
            if (login.getDeptVector().size() > 0 && login.getDepartmentFlag().equals("V")) {

                //Ramya commenting department size setting
                /*int size = login.getDeptVector().size();
                if (size > 15)
                    size = 15;
                strmResponse.println("<select size=\"" + size + "\" id=\"selDept\">");*/
                //Ramya commenting department size setting


                strmResponse.println("<select id=\"selDept\">");
                for (int i = 0; i <= login.getDeptVector().size() - 1; i++) {
                    if (booking.getBDept().equals(login.getDeptVector().elementAt(i))) {
                        strmResponse.println("<option value=\"" + login.getDeptVector().elementAt(i) + "\" selected>" + login.getDeptVector().elementAt(i) + "</option>");
                    } else {
                        strmResponse.println("<option value=\"" + login.getDeptVector().elementAt(i) + "\">" + login.getDeptVector().elementAt(i) + "</option>");
                    }
                }

                strmResponse.println("</select>");
            } else if (login.getDeptVector().size() > 0 && (login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("F"))) {

                //Ramya commenting department size setting
                /*int size = login.getDeptVector().size();
                if (size > 15)
                    size = 15;*/
                //Ramya commenting department size setting
                
                strmResponse.println("<input type=\"text\" id=\"txtBDept\" name=\"txtBDept\" maxlength=\"30\" style=\"width:115;text-transform:uppercase;\" value=" + booking.getBDept() + ">");
                strmResponse.println("<input type=\"button\" hidefocus=\"1\" value=\"&#9660;\" style='height:19; width:18; font-family: Verdana;' onclick=\"JavaScript:menuActivate('txtBDept', 'divDept', 'selDept')\">");
                strmResponse.println("<div id=\"divDept\" style=\"position:absolute; display:none; top:0px;left:0px;z-index:10000\" onmouseover=\"javascript:oOverMenu='divDept';\" onmouseout=\"javascript:oOverMenu=false;\">");
                strmResponse.println("<select size=\"" + (login.getDeptVector().size() + 1) + "\" id=\"selDept\" style=\"width: 150; border-style: none\" onclick=\"JavaScript:textSet('txtBDept',this.value);\" onkeypress=\"JavaScript:comboKey('txtBDept', this);\">");

                //Ramya commenting department size setting
                //strmResponse.println("<select size=\"" + size + "\" id=\"selDept\" style=\"width: 150; border-style: none\" onclick=\"JavaScript:textSet('txtBDept',this.value);\" onkeypress=\"JavaScript:comboKey('txtBDept', this);\">");
                //Ramya commenting department size setting
                for (int i = 0; i < login.getDeptVector().size(); i++) {
                    strmResponse.println("<option value=\"" + login.getDeptVector().elementAt(i) + "\">" + login.getDeptVector().elementAt(i) + "</option>");
                }

                strmResponse.println("</select></div>&nbsp;");
            } else if (login.getDeptVector().size() == 0 && (login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("F"))) {
                strmResponse.println("<input type=\"text\" id=\"txtBDept\" name=\"txtBDept\" value=\"" + booking.getBDept() + "\" style=\"text-transform:uppercase;\" maxlength=\"30\">&nbsp;&nbsp;");
            }
            strmResponse.println("</td>");



            if (login.getRefFlag().equals("V") || login.getRefFlag().equals("Y")) {
                strmResponse.println("<td id=\"label\">");
                if (login.isEYFlag()) {
                    strmResponse.println(login.getEy_Reference() + "*");
                } else {
                    strmResponse.println("Reference*");
                }
            } else if (login.getRefFlag().equals("R")) {
                strmResponse.println("<td id=\"label\" width=\"16.5%\">");
                if (login.isEYFlag()) {
                    strmResponse.println(login.getEy_Reference());
                } else {
                    strmResponse.println("Reference");
                }
            } else {
                strmResponse.println("<td id=\"label\">");
            }


            strmResponse.println("</td>");

            strmResponse.println("<td width=\"22%\" nowrap>");

            if (login.getRefVector().size() > 0 && login.getRefFlag().equals("V")) {
                strmResponse.println("<select id=\"selRef\">");

                for (int i = 0; i <= login.getRefVector().size() - 1; i++) {
                    if (booking.getBRef().equals(login.getRefVector().elementAt(i))) {
                        strmResponse.println("<option value=\"" + login.getRefVector().elementAt(i) + "\" selected>" + login.getRefVector().elementAt(i) + "</option>");
                    } else {
                        strmResponse.println("<option value=\"" + login.getRefVector().elementAt(i) + "\">" + login.getRefVector().elementAt(i) + "</option>");
                    }
                }

                strmResponse.println("</select>&nbsp;<img src=\"images/help.gif\" alt=\"Enter the reference number that you wish to appear on the invoice of the booking\">");
            } else if (login.getRefVector().size() > 0 && (login.getRefFlag().equals("Y") || login.getRefFlag().equals("R"))) {
                //typable combo Y/F are ref mandatory to be entered but doesn't have to match one in the list
                strmResponse.println("<input type=\"text\" id=\"txtBRef\"  name=\"txtBRef\" maxlength=\"12\" style=\"width:115;text-transform:uppercase;\" value=" + booking.getBRef() + ">");
                strmResponse.println("<input type=\"button\" hidefocus=\"1\" value=\"&#9660;\" style='height:19; width:18; font-family: Verdana;' onclick=\"JavaScript:menuActivate('txtBRef', 'divRef', 'selRef')\">");
                strmResponse.println("<div id=\"divRef\" style=\"position:absolute; display:none; top:0px;left:0px;z-index:10000\" onmouseover=\"javascript:oOverMenu='divRef';\" onmouseout=\"javascript:oOverMenu=false;\">");
                strmResponse.println("<select size=\"" + (login.getRefVector().size() + 1) + "\" id=\"selRef\" style=\"width: 150; border-style: none\" onclick=\"JavaScript:textSet('txtBRef',this.value);\" onkeypress=\"JavaScript:comboKey('txtBRef', this);\">");

                for (int i = 0; i < login.getRefVector().size(); i++) {
                    strmResponse.println("<option value=\"" + login.getRefVector().elementAt(i) + "\">" + login.getRefVector().elementAt(i) + "</option>");
                }

                strmResponse.println("</select></div>&nbsp;<img src=\"images/help.gif\" alt=\"Enter the reference number that you wish to appear on the invoice of the booking\">");

            } else if (login.getRefVector().size() == 0 && (login.getRefFlag().equals("Y") || login.getRefFlag().equals("R"))) {
                strmResponse.println("<input type=\"text\" name=\"txtBRef\" id=\"txtBRef\" value=\"" + booking.getBRef() + "\" style=\"text-transform:uppercase;\" maxlength=\"12\">&nbsp;<img src=\"images/help.gif\" alt=\"Enter the reference number that you wish to appear on the invoice of the booking\">&nbsp;&nbsp;<br>");
            }

            System.err.println("*********************************************************************************************************************");
            System.err.println("GOING TO ADD ACCOUNT NUMBER LABEL");
            System.err.println("*********************************************************************************************************************");
            strmResponse.println("<td id=\"label\" width=\"11%\">");
            strmResponse.println("Account Number");
            strmResponse.println("</td>");

            strmResponse.println("<td id=\"label\" width=\"16%\">");
//            String accountNumb = validSession.getAttribute("acc_id");
//            if (accountNumb == null)
//                accountNumb = "" ;
            strmResponse.println("" + validSession.getAttribute("acc_id"));
            strmResponse.println("</td>");


            strmResponse.println("</td></tr>");
            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\" width=\"16.5%\">");


            if (login.isEYFlag()) {
                strmResponse.println(login.getEy_YourName());
            } else {
                strmResponse.println("Caller");
            }

            strmResponse.println("</td>");

            strmResponse.println("<td width=\"18%\">");
            strmResponse.println("<input type=\"text\" name=\"txtBCaller\" id=\"txtBCaller\" value=\"" + booking.getBCaller() + "\" style=\"text-transform:uppercase;\"> &nbsp;&nbsp;&nbsp;&nbsp;");
            strmResponse.println("</td>");

            strmResponse.println("<td id=\"label\" width=\"16.5%\">");

            if (login.isEYFlag()) {
                strmResponse.println(login.getEy_Phone());
            } else {
                strmResponse.println("Phone");
            }
            strmResponse.println("</td>");

            strmResponse.println("<td width=\"22%\">");
            strmResponse.println("<input type=\"text\" name=\"txtBPhone\" id=\"txtBPhone\" value=\"" + booking.getBPhone() + "\" style=\"text-transform:uppercase;\" maxlength=\"15\">");
            strmResponse.println("</td>");

            strmResponse.println("<td id=\"label\" width=\"11%\">");
            strmResponse.println("Consignment Number");
            strmResponse.println("</td>");

            strmResponse.println("<td width=\"16%\" nowrap>");
            strmResponse.println("<input type=\"text\" name=\"txtBHAWBNO\" id=\"txtBHAWBNO\" value=\"" + booking.getBHAWBNO() + "\" style=\"text-transform:uppercase;\" onKeyPress=\"return checkIt(event)\" maxlength=\"15\">&nbsp;<img style=\"cursor:pointer\" src=\"images/help.gif\" alt=\"Enter the consignment number of the booking if already known or an auto-generated number will be assigned on completion of the booking\">");

            strmResponse.println("</td>");
            strmResponse.println("</tr>");
            strmResponse.println("</table>");

            strmResponse.println("</td></tr>");

            strmResponse.println("<tr id=\"trAddresses\">");
//        if(ff)
//            strmResponse.println("<td id=\"tdCollection\" style=\"padding-right:2px\">");
//        else
            strmResponse.println("<td id=\"tdCollection\" width=\"50%\">");

            strmResponse.println("<br>");
            strmResponse.println("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"box\" class=\"box\" width=\"99%\">");
            strmResponse.println("<tr><td id=\"boxtitle\" colspan=\"3\">COLLECTION ADDRESS</td></tr>");


//            if(ff)
//            {
//                strmResponse.println("<tr>");
//                strmResponse.println("<td id=\"label\"  width=\"22%\">Name*</td>");
//                strmResponse.println("<td colspan=\"2\" width=\"25%\"><input type=\"text\"  size=\"40\" id=\"txtCCompany\" name=\"txtCCompany\" value=\""+collection.getCCompany()+"\" style=\"text-transform:uppercase\" maxlength=\"60\"><img src=\"images/search_t.gif\" style=\"cursor:pointer\" onclick=\"javascript:Search('C');\"><img src=\"images/Address-Book-32x32.png\" style=\"cursor:pointer\" onclick=\"javascript:AddressBookPopup('C','');\"></td>");
//                strmResponse.println("</tr>");
//            }
//            else
//            {
            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\"  width=\"25%\">Name*</td>");
            strmResponse.println("<td width=\"25%\" nowrap><input type=\"text\" size=\"42\"  name=\"txtCCompany\" id=\"txtCCompany\" value=\"" + collection.getCCompany() + "\" style=\"text-transform:uppercase\" maxlength=\"60\">&nbsp;<img src=\"images/search_t.gif\" style=\"cursor:pointer\" onclick=\"javascript:Search('C');\">&nbsp;<img src=\"images/Address-Book-32x32.png\" style=\"cursor:pointer\" onclick=\"javascript:AddressBookPopup('C','');\"></td>");
            strmResponse.println("</tr>");
            //}

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">Address 1*</td>");
            strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\"  id=\"txtCAddress1\" name=\"txtCAddress1\" value=\"" + collection.getCCAddress1() + "\" style=\"text-transform:uppercase\" maxlength=\"60\"></td>");
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">Address 2</td>");
            strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtCAddress2\" id=\"txtCAddress2\" value=\"" + collection.getCAddress2() + "\" style=\"text-transform:uppercase\" maxlength=\"60\"></td>");
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">Address 3</td>");
            strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtCAddress3\" id=\"txtCAddress3\" value=\"" + collection.getCAddress3() + "\" style=\"text-transform:uppercase\" maxlength=\"60\"></td>");
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">Town*</td>");

            if (collection.getCLatitude().equals("") || collection.getCLongitude().equals("")) {
                strmResponse.println("<td ><input type=\"text\" size=\"40\"  name=\"txtCTown\" id=\"txtCTown\" value=\"" + collection.getCTown() + "\" style=\"text-transform:uppercase\" maxlength=\"60\"></td>");
            } else {
                strmResponse.println("<td ><input type=\"text\" size=\"40\"  name=\"txtCTown\" id=\"txtCTown\" value=\"" + collection.getCTown() + "\" style=\"text-transform:uppercase\" maxlength=\"60\" disabled=\"disabled\"></td>");
            }
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">County</td>");
            strmResponse.println("<td ><input type=\"text\" size=\"40\" name=\"txtCCounty\" id=\"txtCCounty\" value=\"" + collection.getCCounty() + "\" style=\"text-transform:uppercase\" maxlength=\"60\"></td>");
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td  id=\"label\" style=\"text-align:left;\">Postcode*</td>");

            if (collection.getCLatitude().equals("") || collection.getCLongitude().equals("")) {
                strmResponse.println("<td><input type=\"text\" size=\"10\"  name=\"txtCPostcode\" id=\"txtCPostcode\" value=\"" + collection.getCPostcode() + "\" style=\"text-transform:uppercase\" maxlength=\"20\">&nbsp;&nbsp;<input type=\"button\" id=\"btn\" style=\"font-size:10px\" value=\"UK Postcode Lookup\" onclick=\"javascript:PostLookup('C');\">&nbsp;<img src=\"images/help.gif\" style=\"cursor:pointer\" alt=\"Enter part or full postcode to search for the related address\"></td>");
            } else {
                strmResponse.println("<td><input type=\"text\" size=\"10\"  name=\"txtCPostcode\" id=\"txtCPostcode\" value=\"" + collection.getCPostcode() + "\" style=\"text-transform:uppercase\" maxlength=\"20\" disabled=\"disabled\">&nbsp;&nbsp;<input type=\"button\" style=\"font-size:10px\" id=\"btn\" value=\"UK Postcode Lookup\" onclick=\"javascript:PostLookup('C');\">&nbsp;<img src=\"images/help.gif\" style=\"cursor:pointer\" alt=\"Enter part or full postcode to search for the related address\" ></td>");
            }
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">Country</td>");
            strmResponse.println("<td colspan=\"2\">");

            //populate country values
            strmResponse.println("<select id=\"selCCountry\" disabled=\"disabled\">");
            strmResponse.println("<option value='" + Constants.COUNTRY_CODE + "' selected >UNITED KINGDOM</option>");
            strmResponse.println("</select>");
//            if(cnVector.size()>0)
//            {
//                strmResponse.println("<select id=\"selCCountry\" >");
//
//                for(int i=0;i<=cnVector.size()-1;i++)
//                {
//                    if(collection.getCCountry().equals(cyVector.elementAt(i)))
//                        strmResponse.println("<option value="+cyVector.elementAt(i)+" selected>"+cnVector.elementAt(i)+"</option>");
//                    else
//                        strmResponse.println("<option value="+cyVector.elementAt(i)+">"+cnVector.elementAt(i)+"</option>");
//                }
//
//                strmResponse.println("</select>&nbsp;&nbsp;");
//            }
//            else
//                strmResponse.println("<input type=\"text\" size=\"40\" name=\"selCCountry\" style=\"text-transform:uppercase\">");

            strmResponse.println("</td>");
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">Contact</td>");
            strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtCContact\" id=\"txtCContact\" value=\"" + collection.getCContact() + "\" style=\"text-transform:uppercase\" maxlength=\"20\"></td>");
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\" nowrap>Phone Number</td>");
            strmResponse.println("<td ><input type=\"text\" size=\"20\" name=\"txtCPhone\" id=\"txtCPhone\" value=\"" + collection.getCPhone() + "\" style=\"text-transform:uppercase\" maxlength=\"20\"></td>");
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">Instructions</td>");
            if (sAction.equals("Booking")) {
                strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtCInstruct\" id=\"txtCInstruct\" value=\"" + collection.getCInstruct() + "\" style=\"text-transform:uppercase\" maxlength=\"50\">");
            } else {
                strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtCInstruct\" id=\"txtCInstruct\" value=\"" + collection.getCInstruct() + "\" maxlength=\"50\" style=\"text-transform:uppercase\">");
            }

            //strmResponse.println("<div style=\"display:none\">");
            strmResponse.println("<input type=\"hidden\" name=\"txtCLatitude\" id=\"txtCLatitude\" value=\"" + collection.getCLatitude() + "\">");
            strmResponse.println("<input type=\"hidden\" name=\"txtCLongitude\" id=\"txtCLongitude\" value=\"" + collection.getCLongitude() + "\">");
            strmResponse.println("</td>");
            strmResponse.println("</tr>");


//            if(ff)
//            {
//                strmResponse.println("<tr>");
//                strmResponse.println("<td colspan=\"4\" style=\"padding-left:130px\"><input type=\"button\" name=\"btnEdit\" value=\"Edit Address\" id=\"btn\" onclick=\"javascript:editAddress('C');\">");
//                strmResponse.println("&nbsp;<input type=\"button\" id=\"btn\" value=\"Clear Address\" onclick=\"javascript:clearAddress('C');\"></td>");
//                strmResponse.println("</tr>");
//            }
//            else
//            {
//                strmResponse.println("<tr>");
//                strmResponse.println("<td colspan=\"4\" style=\"padding-left:111px\"><input type=\"button\" name=\"btnEdit\" value=\"Edit Address\" id=\"btn\" onclick=\"javascript:editAddress('C');\">");
//                strmResponse.println("&nbsp;<input type=\"button\" id=\"btn\" value=\"Clear Address\" onclick=\"javascript:clearAddress('C');\"></td>");
//                strmResponse.println("</tr>");
//            }

            // Following piece of code is added by Adnan on 21-Aug-2013 against Residential Address SCR


            strmResponse.println("<tr>");
//            strmResponse.println("<td id=\"label\">Residential Flag</td>");
            strmResponse.println("<td id=\"label\">Residential</td>");
            strmResponse.println("<td colspan=\"2\">");

//            strmResponse.println("<select id=\"selCResidentialFlag\" >");
            System.out.println("=============================== Collection Delivery Flag =" + collection.getCResidentialFlag());
            System.out.println("=============================== Delivery Delivery Flag =" + delivery.getDResidentialFlag());
//            if ("Y".equalsIgnoreCase(collection.getCResidentialFlag())){
//                      strmResponse.println("<option value='"+collection.getCResidentialFlag()+"' selected >"+collection.getCResidentialFlag()+"</option>");
//            }
//            else {
//                strmResponse.println("<option value='Y'>Y</option>");
//            }
//            
//             if ("N".equalsIgnoreCase(collection.getCResidentialFlag())){
//                      strmResponse.println("<option value='"+collection.getCResidentialFlag()+"' selected >"+collection.getCResidentialFlag()+"</option>");
//            }
//            else if (!"Y".equalsIgnoreCase(collection.getCResidentialFlag())){
//                strmResponse.println("<option value='N' selected>N</option>");
//            } 
//             else {
//                strmResponse.println("<option value='N' >N</option>");
//            }


            if ("Y".equalsIgnoreCase(collection.getCResidentialFlag())) {
//                      strmResponse.println("<option value='"+sResidentialFlag+"' selected >"+sResidentialFlag+"</option>"); onclick=\"chkResFlag_Click()\"
                strmResponse.println("<input type=\"checkbox\" name=\"chkCResidentialFlag\" id=\"chkCResidentialFlag\"  checked>");
            } //            else {
            //                strmResponse.println("<option value='Y'>Y</option>");
            //            }
            //             if ("N".equalsIgnoreCase(sResidentialFlag)){
            else {
//                      strmResponse.println("<option value='"+sResidentialFlag+"' selected >"+sResidentialFlag+"</option>");
                strmResponse.println("<input type=\"checkbox\" name=\"chkCResidentialFlag\" id=\"chkCResidentialFlag\"  >");
            }
            /*
             * else if (!"Y".equalsIgnoreCase(sResidentialFlag)){ //
             * strmResponse.println("<option value='N' selected>N</option>");
             * }else { // strmResponse.println("<option value='N' >N</option>");
             * }
             */

            //populate country values


//            strmResponse.println("</select>");
            strmResponse.println("</td>");
            strmResponse.println("</tr>");

//            if(sAction.equals("Booking"))
//                strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtCResidentialFlag\" id=\"txtCResidentialFlag\" value=\""+collection.getResidentialFlag()+"\" style=\"text-transform:uppercase\" maxlength=\"50\">");
//            else
//                strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtCResidentialFlag\" id=\"txtCResidentialFlag\" value=\""+collection.getResidentialFlag()+"\" maxlength=\"50\" style=\"text-transform:uppercase\">");


            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\"></td>");
            strmResponse.println("<td><div align=\"left\"><input type=\"button\" name=\"btnEdit\" value=\"Edit Address\" id=\"btn\" onclick=\"javascript:editAddress('C');\">");
            strmResponse.println("&nbsp;<input type=\"button\" id=\"btn\" value=\"Clear Address\" onclick=\"javascript:clearAddress('C');\"></div></td>");
            strmResponse.println("</tr>");


            strmResponse.println("</table>");

            strmResponse.println("</td>");
            strmResponse.println("<td id=\"tdDelivery\">");
            strmResponse.println("<br>");

            strmResponse.println("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"box\" class=\"box\" wdith=\"99%\">");
            strmResponse.println("<tr><td id=\"boxtitle\" colspan=\"3\">DELIVERY ADDRESS</td></tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\" width=\"25%\">Name*</td>");

//            if(ff)
//                strmResponse.println("<td colspan=\"2\" width=\"25%\"><input type=\"text\"  size=\"40\" name=\"txtDCompany\" id=\"txtDCompany\" value=\""+delivery.getDCompany()+"\" style=\"text-transform:uppercase\" maxlength=\"60\"><img src=\"images/search_t.gif\" style=\"cursor:pointer\" onclick=\"javascript:Search('D');\"><img src=\"images/Address-Book-32x32.png\" style=\"cursor:pointer\" onclick=\"javascript:AddressBookPopup('D','');\"></td>");
//            else

            strmResponse.println("<td width=\"25%\" nowrap ><input type=\"text\" size=\"42\"  name=\"txtDCompany\" id=\"txtDCompany\" value=\"" + delivery.getDCompany() + "\" style=\"text-transform:uppercase\" maxlength=\"60\">&nbsp;<img src=\"images/search_t.gif\" style=\"cursor:pointer\" onclick=\"javascript:Search('D');\">&nbsp;<img src=\"images/Address-Book-32x32.png\" style=\"cursor:pointer\" onclick=\"javascript:AddressBookPopup('D','');\"></td>");

            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">Address 1*</td>");
            strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\"  name=\"txtDAddress1\" id=\"txtDAddress1\" value=\"" + delivery.getDAddress1() + "\" style=\"text-transform:uppercase\" maxlength=\"60\"></td>");
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">Address 2</td>");
            strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtDAddress2\" id=\"txtDAddress2\" value=\"" + delivery.getDAddress2() + "\" style=\"text-transform:uppercase\" maxlength=\"60\"></td>");
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">Address 3</td>");
            strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtDAddress3\" id=\"txtDAddress3\" value=\"" + delivery.getDAddress3() + "\" style=\"text-transform:uppercase\" maxlength=\"60\"></td>");
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">Town*</td>");

            if (delivery.getDLatitude().equals("") || delivery.getDLongitude().equals("")) {
                strmResponse.println("<td ><input type=\"text\" size=\"40\" name=\"txtDTown\"  id=\"txtDTown\" value=\"" + delivery.getDTown() + "\" style=\"text-transform:uppercase\" maxlength=\"60\"></td>");
            } else {
                strmResponse.println("<td ><input type=\"text\" size=\"40\" name=\"txtDTown\"  id=\"txtDTown\" value=\"" + delivery.getDTown() + "\" style=\"text-transform:uppercase\" maxlength=\"60\" disabled=\"disabled\"></td>");
            }
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">County</td>");
            strmResponse.println("<td ><input type=\"text\" size=\"40\" name=\"txtDCounty\" id=\"txtDCounty\" value=\"" + delivery.getDCounty() + "\" style=\"text-transform:uppercase\" maxlength=\"60\"></td>");
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\" style=\"text-align:left;\"><label id=\"lblDPostcode\">Postcode*</label></td>");
            if (delivery.getDLatitude().equals("") || delivery.getDLongitude().equals("")) {
                strmResponse.println("<td><input type=\"text\" size=\"10\" name=\"txtDPostcode\"  id=\"txtDPostcode\" value=\"" + delivery.getDPostcode() + "\" style=\"text-transform:uppercase\" maxlength=\"20\">&nbsp;&nbsp;"
                        + "<input type=\"button\" id=\"btnDPostcode\" class=\"btn\" style=\"font-size:10px;\" name=\"btnDPostcode\" value=\"UK Postcode Lookup\" onclick=\"javascript:PostLookup('D');\">&nbsp;"
                        + "<img id=\"imgDPostcodeHelp\" src=\"images/help.gif\" style=\"cursor:pointer\" alt=\"Enter part or full postcode to search for the related address\"></td>");
            } else {
                strmResponse.println("<td><input type=\"text\" size=\"10\" name=\"txtDPostcode\"  id=\"txtDPostcode\" value=\"" + delivery.getDPostcode() + "\" style=\"text-transform:uppercase\" maxlength=\"20\" disabled=\"disabled\">&nbsp;&nbsp;"
                        + "<input type=\"button\" id=\"btnDPostcode\" style=\"font-size:10px;\" class=\"btn\" name\"btnDPostcode\" value=\"UK Postcode Lookup\" onclick=\"javascript:PostLookup('D');\">&nbsp;"
                        + "<img id=\"imgDPostcodeHelp\" src=\"images/help.gif\" style=\"cursor:pointer\" alt=\"Enter part or full postcode to search for the related address\"></td>");
            }
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">Country</td>");
            strmResponse.println("<td colspan=\"2\" nowrap>");

            if (country.getCnVector().size() > 0) {
                if (delivery.getDLatitude().equals("") || delivery.getDLongitude().equals("")) {
                    strmResponse.println("<select id=\"selDCountry\" onchange=\"JavaScript:dCountryChange(this.value);\">");
                } else {
                    strmResponse.println("<select id=\"selDCountry\" onchange=\"JavaScript:dCountryChange(this.value);\" disabled=\"disabled\">");
                }

                for (int i = 0; i <= country.getCnVector().size() - 1; i++) {
                    if (delivery.getDCountry().equals(country.getCyVector().elementAt(i))) {
                        strmResponse.println("<option value=" + country.getCyVector().elementAt(i) + " selected>" + country.getCnVector().elementAt(i) + "</option>");
                    } else {
                        strmResponse.println("<option value=" + country.getCyVector().elementAt(i) + ">" + country.getCnVector().elementAt(i) + "</option>");
                    }
                }

                strmResponse.println("</select>&nbsp;<input type=\"hidden\" value=\"\" name=\"bCountryAccepted\" id=\"bCountryAccepted\"><input type=\"hidden\" value=\"\" name=\"CountryValue\" id=\"CountryValue\">&nbsp;&nbsp;");
//                <input type=\"button\" value=\"Notes\" onclick=\"JavaScript:notesClick();\" id=\"btn\">
            } else {
                strmResponse.println("<input type=\"text\" size=\"40\" name=\"selDCountry\" id=\"selDCountry\" style=\"text-transform:uppercase\">");
            }

            strmResponse.println("</td>");
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">Contact</td>");
            strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtDContact\" id=\"txtDContact\" value=\"" + delivery.getDContact() + "\" style=\"text-transform:uppercase\" maxlength=\"20\"></td>");
            strmResponse.println("</tr>");


            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\" nowrap>Phone Number*</td>");
            strmResponse.println("<td ><input type=\"text\" size=\"20\" name=\"txtDPhone\"  id=\"txtDPhone\" value=\"" + delivery.getDPhone() + "\" style=\"text-transform:uppercase\" maxlength=\"20\"></td>");
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\">Instructions</td>");
            if (sAction.equals("Booking")) {
                strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtDInstruct\" id=\"txtDInstruct\" value=\"" + delivery.getDInstruct() + "\" style=\"text-transform:uppercase;\" maxlength=\"50\">");
            } else {
                strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtDInstruct\" id=\"txtDInstruct\" value=\"" + delivery.getDInstruct() + "\" style=\"text-transform:uppercase;\" maxlength=\"50\">");
            }

            //strmResponse.println("<div style=\"display:none\">");
            strmResponse.println("<input type=\"hidden\" name=\"txtDLatitude\" id=\"txtDLatitude\" value=\"" + delivery.getDLatitude() + "\">");
            strmResponse.println("<input type=\"hidden\" name=\"txtDLongitude\" id=\"txtDLongitude\" value=\"" + delivery.getDLongitude() + "\">");
            strmResponse.println("</td>");
            strmResponse.println("</tr>");

            // Following piece of code is added by Adnan on 21-Aug-2013 against Residential Address SCR

            strmResponse.println("<tr>");
//            strmResponse.println("<td id=\"label\">Residential Flag</td>");
            strmResponse.println("<td id=\"label\">Residential</td>");
            strmResponse.println("<td colspan=\"2\">");
            //populate country values
//            strmResponse.println("<select id=\"selDResidentialFlag\" >");
//            
//            if ("Y".equalsIgnoreCase(delivery.getDResidentialFlag())){
//                      strmResponse.println("<option value='"+delivery.getDResidentialFlag()+"' selected >"+delivery.getDResidentialFlag()+"</option>");
//            }
//            else {
//                strmResponse.println("<option value='Y'>Y</option>");
//            }
//            
//             if ("N".equalsIgnoreCase(delivery.getDResidentialFlag())){
//                      strmResponse.println("<option value='"+delivery.getDResidentialFlag()+"' selected >"+delivery.getDResidentialFlag()+"</option>");
//            }
//            else if (!"Y".equalsIgnoreCase(delivery.getDResidentialFlag())){
//                strmResponse.println("<option value='N' selected>N</option>");
//            }else {
//                strmResponse.println("<option value='N' >N</option>");
//            } 
//             
//            strmResponse.println("</select>");

            if ("Y".equalsIgnoreCase(delivery.getDResidentialFlag())) {
//                      strmResponse.println("<option value='"+sResidentialFlag+"' selected >"+sResidentialFlag+"</option>");
                strmResponse.println("<input type=\"checkbox\" name=\"chkDResidentialFlag\" id=\"chkDResidentialFlag\" checked>");
            } //            else {
            //                strmResponse.println("<option value='Y'>Y</option>");
            //            }
            //             if ("N".equalsIgnoreCase(sResidentialFlag)){
            else {
//                      strmResponse.println("<option value='"+sResidentialFlag+"' selected >"+sResidentialFlag+"</option>");
                strmResponse.println("<input type=\"checkbox\" name=\"chkDResidentialFlag\" id=\"chkDResidentialFlag\" >");
            }
            strmResponse.println("</td>");
            strmResponse.println("</tr>");

//            if(ff)
//            {
//                strmResponse.println("<tr>");
//                strmResponse.println("<td colspan=\"4\" style=\"padding-left:118px\"><input type=\"button\" name=\"btnEdit\" id=\"btnEdit\" value=\"Edit Address\" class=\"btn\" onclick=\"javascript:editAddress('D');\">");
//                strmResponse.println("&nbsp;<input type=\"button\" id=\"btn\" value=\"Clear Address\" onclick=\"javascript:clearAddress('D');\"></td>");
//                strmResponse.println("</tr>");
//            }
//            else
//            {
//                strmResponse.println("<tr>");
//                strmResponse.println("<td colspan=\"4\" style=\"padding-left:111px\"><input type=\"button\" name=\"btnEdit\" value=\"Edit Address\" id=\"btn\" onclick=\"javascript:editAddress('D');\">");
//                strmResponse.println("&nbsp;<input type=\"button\" id=\"btn\" value=\"Clear Address\" onclick=\"javascript:clearAddress('D');\"></td>");
//                strmResponse.println("</tr>");
//            }

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\"></td>");
            strmResponse.println("<td><div aligh=\"left\"><input type=\"button\" name=\"btnEdit\" value=\"Edit Address\" id=\"btn\" onclick=\"javascript:editAddress('D');\">");
            strmResponse.println("&nbsp;<input type=\"button\" id=\"btn\" value=\"Clear Address\" onclick=\"javascript:clearAddress('D');\"></div></td>");
            strmResponse.println("</tr>");

            strmResponse.println("</table>");

            strmResponse.println("</td>");
            strmResponse.println("</tr>");
            strmResponse.println("<tr id=\"trCollectTime\">");

//        if(ff)
//            strmResponse.println("<td style=\"padding-right:2px\">");
//        else
            strmResponse.println("<td width=\"50%\">");

            strmResponse.println("<br>");

            strmResponse.println("<table id=\"box\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"99%\">");
            strmResponse.println("<tr>");
            strmResponse.println("<td  id=\"boxtitle\" colspan=\"6\">");
            strmResponse.println("COLLECTION TIMES");
            strmResponse.println("</td></tr>");
            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\" width=\"10%\">Ready At</td>");

            if (ff) {
                strmResponse.println("<td colspan=\"5\" width=\"48%\">");
            } else {
                strmResponse.println("<td colspan=\"5\" width=\"50%\">");
            }

            if (booking.getRdCollect().equals("Now") || booking.getRdCollect().equals("")) {
                strmResponse.println("<input name=\"rdCollect\" id=\"rdCollectNow\" type=\"radio\" value=\"Now\" onclick=\"javascript:disableDateFields();\" checked>Now");
            } else {
                strmResponse.println("<input name=\"rdCollect\" id=\"rdCollectNow\" type=\"radio\" value=\"Now\" onclick=\"javascript:disableDateFields();\">Now");
            }

            if (booking.getRdCollect().equals("Advanced")) {
                strmResponse.println("<input name=\"rdCollect\" id=\"rdCollectAdvanced\" type=\"radio\" value=\"Advanced\" onclick=\"javascript:enableDateFields();\" checked>");
                strmResponse.println("<input type=\"text\" name=\"date\" id=\"txtCollectDate\" readonly=\"1\" value=" + booking.getCollectDate() + ">");
            } else {
                strmResponse.println("<input name=\"rdCollect\" id=\"rdCollectAdvanced\" type=\"radio\" value=\"Advanced\" onclick=\"javascript:enableDateFields();\" >");
                strmResponse.println("<input type=\"text\" name=\"date\" id=\"txtCollectDate\" readonly=\"1\" />");
            }

            strmResponse.println("<img src=\"images/calender1.GIF\" id=\"f_trigger_c\" title=\"Date selector\" disabled/>");
            strmResponse.println("<script type=\"text/javascript\">");
            strmResponse.println("Calendar.setup({");
            strmResponse.println("inputField     :    \"txtCollectDate\",     // id of the input field");
            strmResponse.println("ifFormat       :    \"%d/%m/%Y\",      // format of the input field");
            strmResponse.println("button         :    \"f_trigger_c\",  // trigger for the calendar (button ID)");
            strmResponse.println("align          :    \"Tl\",           // alignment (defaults to \"Bl\")");
            strmResponse.println("singleClick    :    true");
            strmResponse.println("});");
            strmResponse.println("</script>");

            if (booking.getRdCollect().equals("Advanced")) {
                strmResponse.println("<select id=\"selCollectHH\">");
            } else {
                strmResponse.println("<select id=\"selCollectHH\" disabled>");
            }

            String sI = "";

            for (int i = 0; i < 24; i++) {
                if (i < 10) {
                    sI = "0" + i;
                } else {
                    sI = "" + i;
                }

                if (booking.getRdCollect().equals("Advanced") && sI.equals(booking.getCollectHH())) {
                    strmResponse.print("<option value=\"" + sI + "\" selected>" + sI + "</option>");
                } else {
                    strmResponse.print("<option value=\"" + sI + "\">" + sI + "</option>");
                }
            }

            if (booking.getRdCollect().equals("Advanced")) {
                strmResponse.println("</select>:<select id=\"selCollectMM\">");
            } else {
                strmResponse.println("</select>:<select id=\"selCollectMM\" disabled>");
            }

            if (booking.getRdCollect().equals("Advanced") && booking.getCollectMM().equals("00")) {
                strmResponse.println("<option value=\"00\" selected>00</option>");
            } else {
                strmResponse.println("<option value=\"00\">00</option>");
            }

            if (booking.getRdCollect().equals("Advanced") && booking.getCollectMM().equals("15")) {
                strmResponse.println("<option value=\"15\" selected>15</option>");
            } else {
                strmResponse.println("<option value=\"15\">15</option>");
            }

            if (booking.getRdCollect().equals("Advanced") && booking.getCollectMM().equals("30")) {
                strmResponse.println("<option value=\"30\" selected>30</option>");
            } else {
                strmResponse.println("<option value=\"30\">30</option>");
            }

            if (booking.getRdCollect().equals("Advanced") && booking.getCollectMM().equals("45")) {
                strmResponse.println("<option value=\"45\" selected>45</option>");
            } else {
                strmResponse.println("<option value=\"45\">45</option>");
            }

            strmResponse.println("</select>");
            strmResponse.println("</td>");
            strmResponse.println("</tr>");

            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\"  width=\"10%\">");
            strmResponse.println("<td colspan=\"3\">");

            if (booking.getChkPickupBefore().equals("1")) {
                strmResponse.println("<input type=\"checkbox\" id=\"chkPickupBefore\" checked>Closes At&nbsp;");
            } else {
                strmResponse.println("<input type=\"checkbox\" id=\"chkPickupBefore\" >Closes At&nbsp;");
            }

            strmResponse.println("<select id=\"selHHPickupBefore\" >");

            sI = "";
            for (int i = 1; i <= 24; i++) {
                if (i < 10) {
                    sI = "0" + i;
                } else {
                    sI = "" + i;
                }

                if (sI.equals(booking.getPickupBeforeHH())) {
                    strmResponse.print("<option value=\"" + sI + "\" selected>" + sI + "</option>");
                } else {
                    strmResponse.print("<option value=\"" + sI + "\">" + sI + "</option>");
                }
            }


            strmResponse.println("</select>:<select id=\"selMMPickupBefore\">");

            if (booking.getPickupBeforeMM().equals("00")) {
                strmResponse.println("<option value=\"00\" selected>00</option>");
            } else {
                strmResponse.println("<option value=\"00\">00</option>");
            }

            if (booking.getPickupBeforeMM().equals("15")) {
                strmResponse.println("<option value=\"15\" selected>15</option>");
            } else {
                strmResponse.println("<option value=\"15\">15</option>");
            }

            if (booking.getPickupBeforeMM().equals("30")) {
                strmResponse.println("<option value=\"30\" selected>30</option>");
            } else {
                strmResponse.println("<option value=\"30\">30</option>");
            }

            if (booking.getPickupBeforeMM().equals("45")) {
                strmResponse.println("<option value=\"45\" selected>45</option>");
            } else {
                strmResponse.println("<option value=\"45\">45</option>");
            }

            strmResponse.println("</select>");

            strmResponse.println("</td>");
            strmResponse.println("</tr>");
            strmResponse.println("</table>");

            strmResponse.println("</td>");

            if (booking.getShipperVAT().length() > 0) {
                strmResponse.println("<td id=\"tdShipper\" width=\"50%\">");
            } else {
                strmResponse.println("<td id=\"tdShipper\" style=\"display:none\" width=\"50%\">");
            }
            strmResponse.println("<br>");

            strmResponse.println("<table id=\"box\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"99%\">");
            strmResponse.println("<tr>");
            strmResponse.println("<td  id=\"boxtitle\" colspan=\"6\">");
            strmResponse.println("SHIPPER VAT DETAILS");
            strmResponse.println("</td></tr>");
            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"label\" width=\"25%\">Shipper<br>VAT code</td>");
            strmResponse.println("<td>");
            strmResponse.println("<input type=\"text\" width=\"25%\" name=\"txtVATCode\" id=\"txtVATCode\" value=" + booking.getShipperVAT() + ">");
            strmResponse.println("</td>");
            strmResponse.println("</tr>");
            strmResponse.println("<tr>");
            strmResponse.println("<td width=\"25%\"></td>");
            strmResponse.println("<td width=\"25%\">");
            strmResponse.println("<input type=\"checkbox\" id=\"chkDefaultVAT\" name=\"chkDefaultVAT\" onclick=\"javascript:setDefaultVATcode(this);\">Use default shipper VAT code");
            strmResponse.println("</td>");
            strmResponse.println("</tr>");
            strmResponse.println("</table>");

            strmResponse.println("</td></tr>");

            strmResponse.println("<tr>");

//        if(ff)
//            strmResponse.println("<td style=\"padding-right:2px\"><br>");
//        else
            strmResponse.println("<td><br>");

            strmResponse.println("<table id=\"box\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"99%\">");
            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"boxtitle\" colspan=\"4\">");
            strmResponse.println("CONSIGNMENT DETAILS");
            strmResponse.println("</td></tr>");
            strmResponse.println("<tr><td id=\"label\" width=\"20%\">");
            strmResponse.println("Product Type");
            strmResponse.println("</td>");

            if (ff) {
                strmResponse.println("<td width=\"18%\">");
            } else {
                strmResponse.println("<td width=\"20%\">");
            }

            strmResponse.println("<select id=\"selProductType\" onchange=\"javascript:productTypeChange(this.value);\">");

            if (booking.getProductType().equals("NDOX")) {
                strmResponse.println("<option value=\"NDOX\" selected>NON DOCUMENTS</option>");
            } else {
                strmResponse.println("<option value=\"NDOX\" >NON DOCUMENTS</option>");
            }

            if (booking.getProductType().equals("DOX") || booking.getProductType().equals("")) {
                strmResponse.println("<option value=\"DOX\" selected >DOCUMENTS</option>");
            } else {
                strmResponse.println("<option value=\"DOX\">DOCUMENTS</option>");
            }

////            System.out.println("=============================== BookingDetails: Line#3314, ParcelName =" + booking.getsParcelName());
//            strmResponse.println("</select>");
//            strmResponse.println("</td></tr>");
//            
//              strmResponse.println("<tr><td id=\"label\" width=\"20%\">");
//            strmResponse.println("Parcel Type");
//            strmResponse.println("</td>");
//
//            
//                strmResponse.println("<td width=\"20%\">");
//            
//            String selParcelDisabled = "";
            
//            if (booking.getItemsRowCount() > 0){
//                System.out.println("=============================== BookingDetails: Line#3334, inside if (booking.getItemsRowCount() > 0), " );
//                selParcelDisabled = "disabled";
//            }else {
//                 selParcelDisabled = "";
//            }
                   
             
//             String sSelect = "<SELECT NAME=\"selParcel" + "\" id=\"selParcel" 
//                    + "\" " + selParcelDisabled
//                     + " onchange=\"JavaScript:parcelClick();\" >\n"
//                    + "<OPTION VALUE=SELECT"
//                    + ("SELECT".equals("SELECT") ? " SELECTED" : "") + ">SELECT\n";
////                    + "<OPTION VALUE=\"YES\""
////                    + (sData.equals("1") ? " SELECTED" : "") + ">YES\n"
////                    + "<OPTION VALUE=\"NO\""
////                    + (sData.equals("0") ? " SELECTED" : "") + ">NO\n"
//             
//             String sData = "";
//             String parcelIndx = "";
//             try{
//                 parcelIndx = booking.getsParcelNameId();
//                 if (parcelIndx!= null && !parcelIndx.trim().equals("SELECT")){
//                        int iParcelIndx = Integer.parseInt(parcelIndx);
//                       sData = (String) parcelInfo.getPnVector().elementAt(iParcelIndx);
//                 }
//             }catch(Exception ex){
//                 ex.printStackTrace();
//             }
//             
//             System.out.println("@@@@@@@@@@@@@@@@@@@@@@ parcelIndx == " + parcelIndx);
//            for (int i = 0; i <= parcelInfo.getPnVector().size() - 1; i++) {
//
//                if (sData.equalsIgnoreCase((String) parcelInfo.getPnVector().elementAt(i))) {
//                    sSelect += "<option value=" + i + " SELECTED>" + parcelInfo.getPnVector().elementAt(i) + "</option>";
//                } else {
//                    sSelect += "<option value=" + i + ">" + parcelInfo.getPnVector().elementAt(i) + "</option>";
//                }
//
//            }
//            
////            System.out.println(sSelect);
//            sSelect += "</SELECT>";
//            strmResponse.println(sSelect);
            
            strmResponse.println("<tr><td id=\"label\" width=\"20%\">");
            strmResponse.println("<label id=\"lblTotWeight\">Total Weight (kg)</label>");
            strmResponse.println("</td><td width=\"20%\">");
            strmResponse.println("<input type=\"text\" name=\"txtTotalWeight\" id=\"txtTotalWeight\" onKeyPress=\"return checkIt(event)\" value=\"" + booking.getTotalWeight() + "\">");
            strmResponse.println("</td></tr>");
            strmResponse.println("<tr><td id=\"label\" width=\"20%\">");
            strmResponse.println("Total No.of.Pieces*");
            strmResponse.println("</td><td width=\"20%\">");
            if (booking.getProductType().equals("NDOX") ){ //|| (parcelIndx!= null && !parcelIndx.trim().equals("") && !parcelIndx.trim().equals("SELECT"))){//("disabled").equals(selParcelDisabled)) {
                strmResponse.println("<input type=\"text\"  name=\"txtTotalPieces\" id=\"txtTotalPieces\" onKeyPress=\"return checkIt(event)\" value=\"" + booking.getTotalPieces() + "\" class=\"disabledstyle\" disabled=\"disabled\">");
            } else {
                strmResponse.println("<input type=\"text\"  name=\"txtTotalPieces\" id=\"txtTotalPieces\" onKeyPress=\"return checkIt(event)\" value=\"" + booking.getTotalPieces() + "\">");
            }

            strmResponse.println("</td></tr>");
            strmResponse.println("<tr><td id=\"label\" width=\"20%\">");
            strmResponse.println("Total Volumetric Weight");
            strmResponse.println("</td><td width=\"20%\">");
            strmResponse.println("<input type=\"text\" name=\"txtTotalVolWeight\" id=\"txtTotalVolWeight\" onKeyPress=\"return checkIt(event)\" value=\"" + booking.getTotalVolWeight() + "\" class=\"disabledstyle\" disabled=\"disabled\">");
            strmResponse.println("</td></tr>");
            strmResponse.println("<tr>");
            strmResponse.println("<td colspan=\"5\"><br>");
            vItemsGrid(request, response, booking);
            strmResponse.println("</td></tr>");
            strmResponse.println("</table>");

            if (booking.getProductType().equals("NDOX")) {
                strmResponse.println("<td id=\"tblCommodity\" >");
            } else {
                strmResponse.println("<td id=\"tblCommodity\" style=\"display:none\" >");
            }


            strmResponse.println("<br><table id=\"box\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"99%\">");
            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"boxtitle\" colspan=\"4\">");
            strmResponse.println("CONTENTS");
            strmResponse.println("</td>");
            strmResponse.println("</tr>");
            strmResponse.println("<tr><td height=\"1px\"></td></tr>");
            strmResponse.println("<tr style=\"border-top:solid 1px black;padding-left:4px\">");
            strmResponse.println("<td>");
            vCommodityGrid(request, response);
            strmResponse.println("</td></tr>");
            strmResponse.println("</table>");

            //Autocomplete Commodity Textbox

            strmResponse.println("<script type=\"text/javascript\">");

            strmResponse.println("YAHOO.example.BasicLocal = function() {");
            strmResponse.println("   // Use a LocalDataSource");
            strmResponse.println("   var oDS = new YAHOO.util.LocalDataSource(arrCommodity);");
            strmResponse.println("   // Optional to define fields for single-dimensional array");
            //strmResponse.println("   oDS.responseSchema = {fields : ["state"]};");

            strmResponse.println("   // Instantiate the AutoComplete");
            strmResponse.println("   var oAC = new YAHOO.widget.AutoComplete(\"" + sParam_EditCell1 + "0\", \"comContainer\", oDS);");
            //strmResponse.println("   var oAC = new YAHOO.widget.AutoComplete(\"myInput\", \"myContainer\", oDS);");
            strmResponse.println("   oAC.prehighlightClassName = \"yui-ac-prehighlight\";");
            strmResponse.println("   oAC.useShadow = true;");

            strmResponse.println("   return {");
            strmResponse.println("       oDS: oDS,");
            strmResponse.println("       oAC: oAC");
            strmResponse.println("   };");
            strmResponse.println("}();");

            strmResponse.println("</script>");
            strmResponse.println("</td>");
            strmResponse.println("</tr>");

        }

    }

    /**
     *
     * @param request
     * @param response
     * @param message - Error Message sent after validating Credit Card
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException Displays Serice guide information returned by
     * NDI_Service API
     */
    public void LoadServicePage(HttpServletRequest request, HttpServletResponse response, String message) throws ServletException, IOException {
        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");

        HttpSession validSession = request.getSession(false);
        String sAction = request.getParameter("sAction") == null ? "" : request.getParameter("sAction").toString();

        if (validSession == null) {
        } else {


            LoginInfo login = new LoginInfo();
            CollectionAddress collection = new CollectionAddress();
            DeliveryAddress delivery = new DeliveryAddress();
            BookingInfo booking = new BookingInfo();
            CreditCardDetails ccdetails = new CreditCardDetails();
            ServiceInfo service = new ServiceInfo();

            String[][] getServicesArray = null;

            //set login,collection,delivery address fields and other booking info fields by processing the request object
            login.parseLogin((String[][]) validSession.getAttribute("loginArray"), request);
            collection.setCollectionAddress(request);
            delivery.setDeliveryAddress(request);
            booking.setBookingInfo(request, response, login);
            ccdetails.setCreditCardDetails(request);

            if (login.isStoreMessages()) {
                //Read the SMS and EMail details from last_10_used_entries.
                try {
                    getEntries(validSession.getAttribute("acc_id").toString(), environment);
                } catch (Exception ex) {
                    vShowErrorPage(response, ex);
                }
            }

            if (sAction.equals("Booking")) {
                strmResponse.println("<table id=\"tblServices\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding-left:4px;padding-right:4px\">");
            } else {
                strmResponse.println("<table id=\"tblServices\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"display:none;\" style=\"padding-left:4px;padding-right:4px\">");
            }
//        strmResponse.println(sHeader);
            strmResponse.println("<tr><td colspan=\"2\"><label name=\"lblMessage1\" id=\"lblMessage1\" style=\"color:red;\">" + message + "</label></td></tr>");
            strmResponse.println("<tr><td id=\"sectionheader\"><br>Select Service Option</td></tr>");
            strmResponse.println("<tr>");
            strmResponse.println("<td colspan=\"2\" id=\"tblServiceList\">");
            strmResponse.println("<br>");
            strmResponse.println("<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"tblValidService\" width=\"100%\" style=\"border:1px solid #666666;padding:2px 5px 2px 5px;background-color:#EFEFEF;\">");
            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"boxtitle\" width=\"20\">&nbsp;</td>");
            strmResponse.println("<td id=\"boxtitle\">Service Option</td>");
            strmResponse.println("<td id=\"boxtitle\">Collection By<br>Date/Time</td>");
            strmResponse.println("<td id=\"boxtitle\">Scheduled Delivery By<br>Date/Time</td>");
            if (login.isPriceFlag()) {
                strmResponse.println("<td id=\"boxtitle\">Price<br>(excluding VAT)</td>");
                strmResponse.println("<td id=\"boxtitle\">&nbsp;</td>");
            }
            strmResponse.println("</tr>");


            if (request.isRequestedSessionIdValid()) {
                getServicesArray = (String[][]) validSession.getAttribute("getServicesArray");
            }

            if (getServicesArray != null) {
                service.parseServices(getServicesArray);

                for (int i = 0; i < service.getSvVector().size(); i++) {

                    if (i == 0) {
                        strmResponse.println("<tr name=\"tr" + service.getSvcodeVector().elementAt(i) + "\" id=\"tr" + service.getSvcodeVector().elementAt(i) + "\" bgcolor=\"#efefef\" onmouseout=\"this.bgColor='#efefef'\" onmouseover=\"this.bgColor='#33CCFF'\" style=\"border:1px solid #44FFCC\" onclick=\"serviceSelect(this,'" + service.getSvcodeVector().elementAt(i) + "')\">");
                        strmResponse.println("<td><input name=\"rdServices\" id=\"rdServicesValid\" type=\"radio\" value=\"" + service.getSvcodeVector().elementAt(i) + "\" checked/></td>");
                    } else {
                        strmResponse.println("<tr name=\"tr" + service.getSvcodeVector().elementAt(i) + "\" id=\"tr" + service.getSvcodeVector().elementAt(i) + "\" bgcolor=\"#efefef\" onmouseout=\"this.bgColor='#efefef'\" onmouseover=\"this.bgColor='#33CCFF'\" onclick=\"serviceSelect(this,'" + service.getSvcodeVector().elementAt(i) + "')\">");
                        strmResponse.println("<td><input name=\"rdServices\" id=\"rdServicesValid\" type=\"radio\" value=\"" + service.getSvcodeVector().elementAt(i) + "\" /></td>");
                    }
                    strmResponse.println("<td>" + service.getSvVector().elementAt(i) + "</td>");
                    strmResponse.println("<td>" + service.getCdVector().elementAt(i) + " " + service.getCmVector().elementAt(i) + "</td>");
                    strmResponse.println("<td>" + service.getDdVector().elementAt(i) + " " + service.getDmVector().elementAt(i) + "</td>");

                    if (login.isPriceFlag()) {
                        try {
                            double dTotal = Double.valueOf(service.getLpVector().elementAt(i).toString()).doubleValue();
                            dTotal = dTotal + Double.valueOf(service.getBpVector().elementAt(i).toString());
                            //dTotal=dTotal+Double.valueOf(service.getGsVector().elementAt(i).toString());
                            dTotal = dTotal + Double.valueOf(service.getScVector().elementAt(i).toString());

                            if (dTotal == 0) {
                                strmResponse.println("<td>POA</td>");
                            } else {
                                strmResponse.println("<td>&#163;" + roundTwoDecimals(dTotal) + "</td>");
                            }
                            //strmResponse.println("<td><input type=\"button\" id=\"btn\" value=\"Show Price\" onclick=\"javascript:ShowPrice('"+service.getLpVector().elementAt(i)+"','"+service.getBpVector().elementAt(i)+"','"+service.getGsVector().elementAt(i)+"','"+service.getScVector().elementAt(i)+"','"+i+"')\"><label id=\"lblPrice"+i+"\"></label></td>");
                        } catch (NumberFormatException numex) {
                            vShowErrorPage(response, numex);
                        }
                    }
                    strmResponse.println("</tr>");
                }

//            strmResponse.println("<tr bgcolor=\"#D2D2D2\">");
//
//            strmResponse.println("<td><input name=\"rdServicesInvalid\" type=\"radio\" disabled/></td>");
//            strmResponse.println("<td>UK By 9.00</td>");
//            strmResponse.println("<td>THU 14-02-08 16:00</td>");
//            strmResponse.println("<td>FRI 15-02-08 09:00</td>");
//            strmResponse.println("<td></td>");
//            strmResponse.println("<td></td>");
//            strmResponse.println("</tr>");
//
//            strmResponse.println("<tr bgcolor=\"#D2D2D2\">");
//
//            strmResponse.println("<td><input name=\"rdServicesInvalid\" type=\"radio\" disabled/></td>");
//            strmResponse.println("<td>UK By 10.30</td>");
//            strmResponse.println("<td>THU 14-02-08 17:00</td>");
//            strmResponse.println("<td>FRI 15-02-08 10:30</td>");
//            strmResponse.println("<td></td>");
//            strmResponse.println("<td></td>");
//            strmResponse.println("</tr>");
            }


            strmResponse.println("</table>");
            strmResponse.println("For other Service Options please call your ServiceCentre");
            strmResponse.println("</td>");
            strmResponse.println("</tr>");



            if (login.getCreditCardFlag().equals("F")) {
                strmResponse.println("<tr>");
                strmResponse.println("<td>");
                strmResponse.println("<br>");
                strmResponse.println("<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"100%\" style=\"padding-right:4px;\">");
                strmResponse.println("<tr><td id=\"boxtitle\" colspan=\"3\">ENTER PAYMENT DETAILS</td></tr>");
                strmResponse.println("<tr>");
                strmResponse.println("<td id=\"label\" width=\"20%\">Credit Card Type</td>");
                strmResponse.println("<td width=\"30%\" colspan=\"2\">");
                strmResponse.println("<select name=\"selCCType\" id=\"selCCType\" onchange='javascript:CCTypeChange();'>");
                if (ccdetails.getCCType().equals("VISA")) {
                    strmResponse.println("<option value=\"VISA\" selected>VISA</option>");
                } else {
                    strmResponse.println("<option value=\"VISA\">VISA</option>");
                }

                if (ccdetails.getCCType().equals("Mastercard")) {
                    strmResponse.println("<option value=\"Mastercard\" selected>Mastercard</option>");
                } else {
                    strmResponse.println("<option value=\"Mastercard\">Mastercard</option>");
                }

                if (ccdetails.getCCType().equals("American Express")) {
                    strmResponse.println("<option value=\"American Express\" selected>American Express</option>");
                } else {
                    strmResponse.println("<option value=\"American Express\">American Express</option>");
                }

                if (ccdetails.getCCType().equals("VISA Delta")) {
                    strmResponse.println("<option value=\"VISA Delta\" selected>VISA Delta</option>");
                } else {
                    strmResponse.println("<option value=\"VISA Delta\">VISA Delta</option>");
                }

                if (ccdetails.getCCType().equals("VISA Electron")) {
                    strmResponse.println("<option value=\"VISA Electron\" selected>VISA Electron</option>");
                } else {
                    strmResponse.println("<option value=\"VISA Electron\">VISA Electron</option>");
                }

                if (ccdetails.getCCType().equals("VISA Purchasing")) {
                    strmResponse.println("<option value=\"VISA Purchasing\" selected>VISA Purchasing</option>");
                } else {
                    strmResponse.println("<option value=\"VISA Purchasing\" >VISA Purchasing</option>");
                }

                if (ccdetails.getCCType().equals("JCB")) {
                    strmResponse.println("<option value=\"JCB\" selected>JCB</option>");
                } else {
                    strmResponse.println("<option value=\"JCB\">JCB</option>");
                }

                if (ccdetails.getCCType().equals("Laser")) {
                    strmResponse.println("<option value=\"Laser\" selected>Laser</option>");
                } else {
                    strmResponse.println("<option value=\"Laser\">Laser</option>");
                }

                if (ccdetails.getCCType().equals("Solo")) {
                    strmResponse.println("<option value=\"Solo\" selected>Solo</option>");
                } else {
                    strmResponse.println("<option value=\"Solo\">Solo</option>");
                }

                if (ccdetails.getCCType().equals("Switch")) {
                    strmResponse.println("<option value=\"Switch\" selected>Switch</option>");
                } else {
                    strmResponse.println("<option value=\"Switch\">Switch</option>");
                }

                if (ccdetails.getCCType().equals("Diners Club")) {
                    strmResponse.println("<option value=\"Diners Club\" selected>Diners Club</option>");
                } else {
                    strmResponse.println("<option value=\"Diners Club\">Diners Club</option>");
                }

                if (ccdetails.getCCType().equals("Ge Capital")) {
                    strmResponse.println("<option value=\"Ge Capital\" selected>Ge Capital</option>");
                } else {
                    strmResponse.println("<option value=\"Ge Capital\">Ge Capital</option>");
                }

                strmResponse.println("</select>");
                strmResponse.println("</td>");
                strmResponse.println("</tr>");
                strmResponse.println("<tr>");
                strmResponse.println("<td id=\"label\">Credit Card Number</td>");
                strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"20\" maxlength=\"19\"  onKeyPress=\"return checkItNo(event)\" style=\"text-transform:uppercase;\" name=\"txtCCNumber\" id=\"txtCCNumber\" value=" + ccdetails.getCardNo() + "></td>");
                strmResponse.println("</tr>");
                strmResponse.println("<tr>");
                strmResponse.println("<td id=\"label\">Card Holders Name</td>");
                strmResponse.println("<td colspan=\"2\"><input type=\"text\" maxlength=\"50\" size=\"40\"  style=\"text-transform:uppercase;\" name=\"txtCCName\" id=\"txtCCName\" value=" + ccdetails.getCCName() + " ></td>");
                strmResponse.println("</tr>");
                strmResponse.println("<tr>");
                strmResponse.println("<td id=\"label\">Expiry Date (MM/YY)</td>");
                strmResponse.println("<td colspan=\"2\">");
                strmResponse.println("<select id=\"selCExpiryMonth\">");

                String sMonth = "";
                strmResponse.println("<option value=" + sMonth + ">" + sMonth + "</option>");
                for (int i = 1; i <= 12; i++) {
                    if (i < 10) {
                        sMonth = "0" + i;
                    } else {
                        sMonth = "" + i;
                    }
                    if (ccdetails.getExpiryMonth().equals(sMonth)) {
                        strmResponse.println("<option value=" + sMonth + " selected>" + sMonth + "</option>");
                    } else {
                        strmResponse.println("<option value=" + sMonth + ">" + sMonth + "</option>");
                    }
                }

                strmResponse.println("</select>&nbsp;");
                strmResponse.println("<select id=\"selCExpiryYear\">");

                String sYear = "";
                strmResponse.println("<option value=" + sYear + ">" + sYear + "</option>");
                java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("dd/MM/yyyy");


                java.util.Date d = new java.util.Date();
                sYear = format.format(d);

                int j = java.lang.Integer.parseInt(sYear.substring(8, 10));
                int index = 0;
                String kitem = "";

                for (int k = j; k <= (j + 5); k++) {

                    String k1 = "" + k;
                    if (k1.length() < 2) {
                        kitem = "0" + k1;
                    } else {
                        kitem = k1;
                    }

                    if (ccdetails.getExpiryYear().equals(kitem)) {
                        strmResponse.println("<option value=" + kitem + " selected>" + kitem + "</option>");
                    } else {
                        strmResponse.println("<option value=" + kitem + ">" + kitem + "</option>");
                    }
                }

                strmResponse.println("</select>");

                strmResponse.println("</td>");
                strmResponse.println("</tr>");
                strmResponse.println("<tr>");
                strmResponse.println("<td id=\"label\">Start Date (Switch/Solo)</td>");
                if (ccdetails.getCCType().equals("Switch") || ccdetails.getCCType().equals("Solo")) {
                    strmResponse.println("<td colspan=\"2\"><select id=\"selStartDay\" >");
                } else {
                    strmResponse.println("<td colspan=\"2\"><select id=\"selStartDay\" disabled=\"disabled\">");
                }

                String sDay = "";
                strmResponse.println("<option value=" + sDay + ">" + sDay + "</option>");

                for (int i = 1; i <= 31; i++) {
                    if (i < 10) {
                        sDay = "0" + i;
                    } else {
                        sDay = "" + i;
                    }

                    if (ccdetails.getStartDay().equals(sDay)) {
                        strmResponse.println("<option value=" + sDay + " selected>" + sDay + "</option>");
                    } else {
                        strmResponse.println("<option value=" + sDay + ">" + sDay + "</option>");
                    }
                }

                strmResponse.println("</select>&nbsp;");
                if (ccdetails.getCCType().equals("Switch") || ccdetails.getCCType().equals("Solo")) {
                    strmResponse.println("<select id=\"selStartMonth\" >");
                } else {
                    strmResponse.println("<select id=\"selStartMonth\" disabled=\"disabled\">");
                }

                String sMonth1 = "";
                strmResponse.println("<option value=" + sMonth1 + ">" + sMonth1 + "</option>");
                for (int i = 1; i <= 12; i++) {
                    if (i < 10) {
                        sMonth1 = "0" + i;
                    } else {
                        sMonth1 = "" + i;
                    }

                    if (ccdetails.getStartMonth().equals(sMonth1)) {
                        strmResponse.println("<option value=" + sMonth1 + " selected>" + sMonth1 + "</option>");
                    } else {
                        strmResponse.println("<option value=" + sMonth1 + ">" + sMonth1 + "</option>");
                    }
                }

                strmResponse.println("</select>");
                strmResponse.println("</td>");
                strmResponse.println("</tr>");
                strmResponse.println("<tr>");
                strmResponse.println("<td id=\"label\">Issue Number (Switch/Solo)</td>");
                if (ccdetails.getCCType().equals("Switch") || ccdetails.getCCType().equals("Solo")) {
                    strmResponse.println("<td colspan=\"2\"><select id=\"selIssueNo\">");
                } else {
                    strmResponse.println("<td colspan=\"2\"><select id=\"selIssueNo\" disabled=\"disabled\">");
                }

                String sIssueNumber = "";
                strmResponse.println("<option value=" + sIssueNumber + ">" + sIssueNumber + "</option>");
                for (int i = 1; i <= 10; i++) {
                    if (i < 10) {
                        sIssueNumber = "0" + i;
                    } else {
                        sIssueNumber = "" + i;
                    }

                    if (ccdetails.getIssueNo().equals(sIssueNumber)) {
                        strmResponse.println("<option value=" + sIssueNumber + " selected>" + sIssueNumber + "</option>");
                    } else {
                        strmResponse.println("<option value=" + sIssueNumber + ">" + sIssueNumber + "</option>");
                    }
                }

                strmResponse.println("</select></td>");
                strmResponse.println("</tr>");

                strmResponse.println("<tr>");
                strmResponse.println("<td id=\"label\">Security Code</td>");
                strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"20\" name=\"txtSecurityCode\" id=\"txtSecurityCode\" value=\"" + ccdetails.getSecurityCode() + "\" style=\"text-transform:uppercase;\"></td>");
                strmResponse.println("</tr>");
                strmResponse.println("<tr>");

                if (login.isAvsAddressMandatoryFlag()) {
                    strmResponse.println("<tr>");
                    strmResponse.println("<td id=\"label\">Address 1*</td>");
                    strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtAVSAddress1\" id=\"txtAVSAddress1\" value=\"" + ccdetails.getsAVSAddress1() + "\" style=\"text-transform:uppercase;\"></td>");
                    strmResponse.println("</tr>");
                    strmResponse.println("<tr>");

                    strmResponse.println("<tr>");
                    strmResponse.println("<td id=\"label\">Address 2</td>");
                    strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtAVSAddress2\" id=\"txtAVSAddress2\" value=\"" + ccdetails.getsAVSAddress2() + "\" style=\"text-transform:uppercase;\"></td>");
                    strmResponse.println("</tr>");
                    strmResponse.println("<tr>");

                    strmResponse.println("<tr>");
                    strmResponse.println("<td id=\"label\">Address 3</td>");
                    strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtAVSAddress3\" id=\"txtAVSAddress3\" value=\"" + ccdetails.getsAVSAddress3() + "\" style=\"text-transform:uppercase;\"></td>");
                    strmResponse.println("</tr>");
                    strmResponse.println("<tr>");

                    strmResponse.println("<tr>");
                    strmResponse.println("<td id=\"label\">Address 4</td>");
                    strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" name=\"txtAVSAddress4\" id=\"txtAVSAddress4\" value=\"" + ccdetails.getsAVSAddress4() + "\" style=\"text-transform:uppercase;\"></td>");
                    strmResponse.println("</tr>");
                    strmResponse.println("<tr>");
                }

                strmResponse.println("<tr>");
                strmResponse.println("<td id=\"label\">Postcode</td>");
                strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"20\" maxlength=\"20\" name=\"txtPostcode\" id=\"txtPostcode\" value=\"" + ccdetails.getPostcode() + "\" style=\"text-transform:uppercase;\"></td>");
                strmResponse.println("</tr>");

                strmResponse.println("<td id=\"label\">Email Address</td>");
                strmResponse.println("<td colspan=\"2\"><input type=\"text\" size=\"40\" maxlength=\"80\" name=\"txtCCEmail\" id=\"txtCCEmail\" value=\"" + ccdetails.getCCEmail() + "\" style=\"text-transform:uppercase;\"></td>");
                strmResponse.println("</tr>");
                strmResponse.println("</table>");
                strmResponse.println("</td>");
                strmResponse.println("<td>");

            } else {
                strmResponse.println("<tr>");
                strmResponse.println("<td width=\"50%\">");

            }


            strmResponse.println("<br>");

            if (login.isEmailAccountFlag() || login.isSmsAccountFlag()) {
                strmResponse.println("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"box\" width=\"100%\">");
                strmResponse.println("<tr><td id=\"boxtitle\" colspan=\"3\">TRACKING NOTIFICATIONS</td></tr>");

                if (login.isEmailAccountFlag()) {
                    strmResponse.println("<tr>");
                    strmResponse.println("<td id=\"label\" width=\"5%\">Email</td>");
                    strmResponse.println("<td width=\"45%\">");

                    //if storeMessages flag is true then EMAIL and SMS dropdown list are visible else textboxes would be visisble.
                    if (emailVector.size() > 0) {
                        strmResponse.println("<input type=\"text\" id=\"txtTrackingEmail\" name=\"txtTrackingEmail\" size=\"40\" style=\"width:150;text-transform:uppercase;\" value=" + ccdetails.getTrackingEmail() + ">");
                        strmResponse.println("<input type=\"button\" hidefocus=\"1\" value=\"&#9660;\" style='height:19; width:18; font-family: Verdana;' onclick=\"JavaScript:menuActivate('txtTrackingEmail', 'divTrackingEmail', 'selTrackingEmail')\">");
                        strmResponse.println("<div id=\"divTrackingEmail\" style=\"position:absolute; display:none; top:0px;left:0px;z-index:10000\" onmouseover=\"javascript:oOverMenu='divTrackingEmail';\" onmouseout=\"javascript:oOverMenu=false;\">");
                        strmResponse.println("<select size=\"" + (emailVector.size() + 1) + "\" id=\"selTrackingEmail\" style=\"width: 150; border-style: none\" onclick=\"JavaScript:textSet('txtTrackingEmail',this.value);\" onkeypress=\"JavaScript:comboKey('txtTrackingEmail', this);\">");

                        for (int i = 0; i < emailVector.size(); i++) {
                            strmResponse.println("<option value=" + emailVector.elementAt(i) + ">" + emailVector.elementAt(i) + "</option>");
                        }

                        strmResponse.println("</select></div>&nbsp;");
                    } else {
                        strmResponse.println("<input type=\"text\" id=\"txtTrackingEmail\" size=\"40\" style=\"text-transform:uppercase;\" value=" + ccdetails.getTrackingEmail() + ">&nbsp;");
                    }


                    strmResponse.println("&nbsp;<a href=\"javascript:Tracking('E');\">Multiple..</a>");
                    strmResponse.println("</td>");
                    strmResponse.println("</tr>");
                    strmResponse.println("<tr>");
                    strmResponse.println("<td colspan=\"2\">");
                    strmResponse.println("<input type=\"checkbox\" name=\"chkBookingEmail\" id=\"chkBookingEmail\"> Booking &nbsp;");
                    strmResponse.println("<input type=\"checkbox\" name=\"chkCollectionEmail\" id=\"chkCollectionEmail\"> Collection &nbsp;");
                    strmResponse.println("<input type=\"checkbox\" name=\"chkDeliveryEmail\" id=\"chkDeliveryEmail\"> Delivery &nbsp;");
//                strmResponse.println("<input type=\"checkbox\" name=\"chkDelayedEmail\"> Delayed ");
                    strmResponse.println("</td>");
                    strmResponse.println("</tr>");
                }

                if (login.isSmsAccountFlag()) {
                    strmResponse.println("<tr>");
                    strmResponse.println("<td id=\"label\" width=\"5%\">Mobile</td>");
                    strmResponse.println("<td width=\"45%\">");

                    if (smsVector.size() > 0) {
                        strmResponse.println("<input type=\"text\" id=\"txtTrackingSMS\" name=\"txtTrackingSMS\" size=\"20\" style=\"width:150;text-transform:uppercase;\" value=" + ccdetails.getTrackingSMS() + ">");
                        strmResponse.println("<input type=\"button\" hidefocus=\"1\" value=\"&#9660;\" style='height:19; width:18; font-family: Verdana;' onclick=\"JavaScript:menuActivate('txtTrackingSMS', 'divTrackingSMS', 'selTrackingSMS')\">");
                        strmResponse.println("<div id=\"divTrackingSMS\" style=\"position:absolute; display:none; top:0px;left:0px;z-index:10000\" onmouseover=\"javascript:oOverMenu='divTrackingSMS';\" onmouseout=\"javascript:oOverMenu=false;\">");
                        strmResponse.println("<select size=\"" + (smsVector.size() + 1) + "\" id=\"selTrackingSMS\" style=\"width: 150; border-style: none\" onclick=\"JavaScript:textSet('txtTrackingSMS',this.value);\" onkeypress=\"JavaScript:comboKey('txtTrackingSMS', this);\">");


                        for (int i = 0; i < smsVector.size(); i++) {
                            strmResponse.println("<option value=" + smsVector.elementAt(i) + ">" + smsVector.elementAt(i) + "</option>");
                        }

                        strmResponse.println("</select></div>&nbsp;");
                    } else {
                        strmResponse.println("<input type=\"text\" id=\"txtTrackingSMS\" size=\"20\" style=\"text-transform:uppercase;\" value=" + ccdetails.getTrackingSMS() + ">&nbsp;");
                    }

                    strmResponse.println("&nbsp;<a href=\"javascript:Tracking('S');\">Multiple..</a>");
                    strmResponse.println("</td>");
                    strmResponse.println("</tr>");
                    strmResponse.println("<tr>");
                    strmResponse.println("<td colspan=\"2\">");
                    strmResponse.println("<input type=\"checkbox\" name=\"chkBookingSMS\" id=\"chkBookingSMS\"> Booking &nbsp;");
                    strmResponse.println("<input type=\"checkbox\" name=\"chkCollectionSMS\" id=\"chkCollectionSMS\"> Collection &nbsp;");
                    strmResponse.println("<input type=\"checkbox\" name=\"chkDeliverySMS\" id=\"chkDeliverySMS\"> Delivery &nbsp;");
//                strmResponse.println("<input type=\"checkbox\" name=\"chkDelayedSMS\"> Delayed ");

                    strmResponse.println("</td>");
                    strmResponse.println("</tr>");

                }
                strmResponse.println("</table>");
                strmResponse.println("</td>");
                strmResponse.println("</tr> ");
            }


            strmResponse.println("<tr>");
            strmResponse.println("<td></td>");
            strmResponse.println("<td style=\"text-align:right;padding-right:15px\">");
            strmResponse.println("<br><input type=\"button\" value=\"Amend Details\" id=\"btn\" onclick=\"javascript:enableBooking();\"> &nbsp;&nbsp; <input type=\"button\" id=\"btn\" value=\"Continue\" onclick=\"javascript: validateCCDetails();\">");
            strmResponse.println("</td>");
            strmResponse.println("</tr>");

            strmResponse.println("</table>");
            //strmResponse.println("</iframe>");



            //CC validate form
            strmResponse.println("<form name=\"validateForm\" method=\"post\" action=\"" + Constants.srvEnterConsignment + "\">");
            strmResponse.println("<input type=\"hidden\" name=\"CCType\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"CCNumber\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"CCName\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sServices\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sExpiryMonth\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sExpiryYear\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sStartDay\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sStartMonth\" vale=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sIssueNo\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sSecurityCode\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sAVSAddress1\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sAVSAddress2\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sAVSAddress3\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sAVSAddress4\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sPostcode\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sCCEmail\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sTrackingEmail\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sTrackingSMS\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sEmailNotify\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sSMSNotify\" value=\"\">");

            //Booking details
            strmResponse.println("<input type=hidden name=\"CCompanyName\" value=\"" + collection.getCCompany() + "\">");
            strmResponse.println("<input type=hidden name=\"CAddress1\" value=\"" + collection.getCCAddress1() + "\">");
            strmResponse.println("<input type=hidden name=\"CAddress2\" value=\"" + collection.getCAddress2() + "\">");
            strmResponse.println("<input type=hidden name=\"CAddress3\" value=\"" + collection.getCAddress3() + "\">");
            strmResponse.println("<input type=hidden name=\"CTown\" value=\"" + collection.getCTown() + "\">");
            strmResponse.println("<input type=hidden name=\"CCounty\" value=\"" + collection.getCCounty() + "\">");
            strmResponse.println("<input type=hidden name=\"CPostcode\" value=\"" + collection.getCPostcode() + "\">");
            strmResponse.println("<input type=hidden name=\"CCountry\" value=\"" + collection.getCCountry() + "\">");
            strmResponse.println("<input type=hidden name=\"CContactName\" value=\"" + collection.getCContact() + "\">");
            strmResponse.println("<input type=hidden name=\"CPhone\" value=\"" + collection.getCPhone() + "\">");
            strmResponse.println("<input type=hidden name=\"CInstruct\" value=\"" + collection.getCInstruct() + "\">");

            // added by Adnan on 27 Aug 2013 against Residential Flag change
            strmResponse.println("<input type=hidden name=\"CResidentialFlag\" value=\"" + collection.getCResidentialFlag() + "\">");

            strmResponse.println("<input type=hidden name=\"DCompanyName\" value=\"" + delivery.getDCompany() + "\">");
            strmResponse.println("<input type=hidden name=\"DContactName\" value=\"" + delivery.getDContact() + "\">");
            strmResponse.println("<input type=hidden name=\"DAddress1\" value=\"" + delivery.getDAddress1() + "\">");
            strmResponse.println("<input type=hidden name=\"DAddress2\" value=\"" + delivery.getDAddress2() + "\">");
            strmResponse.println("<input type=hidden name=\"DAddress3\" value=\"" + delivery.getDAddress3() + "\">");
            strmResponse.println("<input type=hidden name=\"DPhone\" value=\"" + delivery.getDPhone() + "\">");
            strmResponse.println("<input type=hidden name=\"DTown\" value=\"" + delivery.getDTown() + "\">");
            strmResponse.println("<input type=hidden name=\"DCounty\" value=\"" + delivery.getDCounty() + "\">");
            strmResponse.println("<input type=hidden name=\"DCountry\" value=\"" + delivery.getDCountry() + "\">");
            strmResponse.println("<input type=hidden name=\"DPostcode\" value=\"" + delivery.getDPostcode() + "\">");
            strmResponse.println("<input type=hidden name=\"DInstruct\" value=\"" + delivery.getDInstruct() + "\">");

            // added by Adnan on 27 Aug 2013 against Residential Flag change
            strmResponse.println("<input type=hidden name=\"DResidentialFlag\" value=\"" + delivery.getDResidentialFlag() + "\">");

            strmResponse.println("<input type=hidden name=\"BDept\" value=\"" + booking.getBDept() + "\">");
            strmResponse.println("<input type=hidden name=\"BCaller\" value=\"" + booking.getBCaller() + "\">");
            strmResponse.println("<input type=hidden name=\"BPhone\" value=\"" + booking.getBPhone() + "\">");
            strmResponse.println("<input type=hidden name=\"BRef\" value=\"" + booking.getBRef() + "\">");
            strmResponse.println("<input type=hidden name=\"BHAWBNO\" value=\"" + booking.getBHAWBNO() + "\">");

            strmResponse.println("<input type=hidden name=\"rdCollect\" value=\"" + booking.getRdCollect() + "\">");
            strmResponse.println("<input type=hidden name=\"CollectDate\" value=\"" + booking.getCollectDate() + "\">");
            strmResponse.println("<input type=hidden name=\"CollectHH\" value=\"" + booking.getCollectHH() + "\">");
            strmResponse.println("<input type=hidden name=\"CollectMM\" value=\"" + booking.getCollectMM() + "\">");
            strmResponse.println("<input type=hidden name=\"chkPickupBefore\" value=\"" + booking.getChkPickupBefore() + "\">");
            strmResponse.println("<input type=hidden name=\"PickupBeforeHH\" value=\"" + booking.getPickupBeforeHH() + "\">");
            strmResponse.println("<input type=hidden name=\"PickupBeforeMM\" value=\"" + booking.getPickupBeforeMM() + "\">");
            strmResponse.println("<input type=hidden name=\"ProductType\" value=\"" + booking.getProductType() + "\">");
            strmResponse.println("<input type=hidden name=\"TotalPieces\" value=\"" + booking.getTotalPieces() + "\">");
            strmResponse.println("<input type=hidden name=\"TotalWeight\" value=\"" + booking.getTotalWeight() + "\">");
            strmResponse.println("<input type=hidden name=\"TotalVolWeight\" value=\"" + booking.getTotalVolWeight() + "\">");
            strmResponse.println("<input type=hidden name=\"" + Constants.SACTION_SUB + "\" value=\"Validate\">");
            strmResponse.println("<input type=hidden name=\"" + Constants.SACTION + "\" value=\"Booking\">");

//            strmResponse.println("<input type=hidden name=\"ParcelNameId\" value=\"" + booking.getsParcelNameId() + "\">");
//            strmResponse.println("<input type=hidden name=\"ParcelName\" value=\"" + booking.getsParcelName() + "\">");
            
            strmResponse.println("<input type=hidden id=\"itemsRowCount\" name=\"itemsRowCount\" value=\"" + booking.getItemsRowCount() + "\">");
            
            strmResponse.println("</form>");


            //Tracking Notifications Form
            strmResponse.println("<form name=\"TrackingForm\" target=\"TrackingForm\" method=\"post\" action=\"" + Constants.srvTrackingNotifications + "\">");
            strmResponse.println("<input type=\"hidden\" name=\"sTrackingEmail\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sTrackingSMS\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"" + Constants.STYPE + "\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"" + Constants.SACTION + "\" value=\"" + Constants.sBooking + "\">");
            strmResponse.println("<input type=\"hidden\" name=\"sEmailNotify\" value=\"\">");
            strmResponse.println("<input type=\"hidden\" name=\"sSMSNotify\" value=\"\">");
            strmResponse.println("</form>");

            //strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">scrollBottom();</script>");
        }
    }

    /**
     *
     * @param strmResponse
     * @param request
     * @param login
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void vWriteJavascriptFunctions(ServletOutputStream strmResponse, HttpServletRequest request, LoginInfo login)
            throws ServletException, IOException {

        HttpSession newSession = request.getSession(false);
//        strmResponse.println("function bOnSubmit()");
//        strmResponse.println("{");
//        strmResponse.println("    return falsese;");
//        strmResponse.println("}");

        strmResponse.println("function vSubmit(sNextRowID)");
        strmResponse.println("{");
//        strmResponse.println("alert(\"Inside VSubmit method line# 4042: snextrowid: \"+sNextRowID);");

        strmResponse.println("document.frmGridServlet.comfocus.value=\"false\";");
        strmResponse.println("document.frmGridServlet.itemfocus.value=\"true\";");
        strmResponse.println("    document.frmGridServlet." + sParam_NextRowID
                + ".value = sNextRowID;");
        strmResponse.println("    if (sNextRowID == 'new')");
        strmResponse.println("    {");
        
        strmResponse.println("var sItemsRowCount=document.getElementById(\"itemsRowCount\").value;");
        
        strmResponse.println("sItemsRowCount = parseInt(sItemsRowCount);");

//        strmResponse.println("  alert('inside vSubmit, B4 increment sItemsRowCount == ' + sItemsRowCount);");
        
        strmResponse.println("    document.frmGridServlet.itemsRowCount.value = (sItemsRowCount+1);");

//         strmResponse.println("  alert('inside vSubmit, after increment sItemsRowCount == ' + (sItemsRowCount+1));");
//        strmResponse.println("var sSelectedParcel=document.getElementById(\"selParcel\").value;");
//        
////        strmResponse.println(" alert ('sNextRowID == new and  sSelectedParcel == ' + sSelectedParcel);  ");
//			  strmResponse.println("if(sSelectedParcel!=\"SELECT\")");
//         
//          strmResponse.println("    {");
////          strmResponse.println(" alert ('BookingDetails: Line#4060, selParcel != select');  ");
//        strmResponse.println("        document.frmGridServlet.sMode.value="
//                + "'none" + "';");
//            strmResponse.println("        document.frmGridServlet.ParcelNameId.value=sSelectedParcel;");
//            strmResponse.println("        document.frmGridServlet.ParcelName.value=arrParcelType[sSelectedParcel];");
//         strmResponse.println("    }");
//           strmResponse.println("  else  {");
//        strmResponse.println("        document.frmGridServlet.sMode.value="
//                + "'" + sMode_Add + "';");
//        
//         strmResponse.println("        document.frmGridServlet.ParcelNameId.value=sSelectedParcel;");
//          strmResponse.println("        document.frmGridServlet.ParcelName.value=arrParcelType[sSelectedParcel];");
//        
//         strmResponse.println("    }");
        
        strmResponse.println("        document.frmGridServlet.sMode1.value='" + sMode_Add + "';");
        strmResponse.println("        document.frmGridServlet.sCalledBy1.value='" + sCalledBy_Edit + "';");
        strmResponse.println("    }");
        strmResponse.println("    else");
        strmResponse.println("    {");

        strmResponse.println("        document.frmGridServlet.sMode.value='" + sMode_Edit + "';");
        strmResponse.println("        document.frmGridServlet.sCalledBy.value='" + sCalledBy_Summary + "';");
        strmResponse.println("        document.frmGridServlet.sMode1.value='" + sMode_Add + "';");
        strmResponse.println("        document.frmGridServlet.sCalledBy1.value='" + sCalledBy_Edit + "';");

        strmResponse.println("     }");
        strmResponse.println("        var txtNoItems=document.getElementById(\"" + sParam_EditCell + "0\");");
//        strmResponse.println("        var txtWeight=document.getElementById(\"" + sParam_EditCell + "1\");");
        strmResponse.println("        var txtLength=document.getElementById(\"" + sParam_EditCell + "1\");");
        strmResponse.println("        var txtWidth=document.getElementById(\"" + sParam_EditCell + "2\");");
        strmResponse.println("        var txtHeight=document.getElementById(\"" + sParam_EditCell + "3\");");

//        strmResponse.println("        var txtParcelType=document.getElementById(\"selParcel\");");
        

//
//        strmResponse.println("if( !parseFloat(txtWeight.value)==0 && txtWeight.value.length>0){");
////         strmResponse.println("if (txtWeight != \"\" || txtWeight != \"0\"){"); 
////        strmResponse.println("alert('I am inside vSubmit() method, itemWeightCount=' + itemWeightCount);");
//
//        strmResponse.println("document.frmGridServlet.itemWeightCount.value=itemWeightCount+1;");
//
////        strmResponse.println("alert('I am inside vSubmit() method, After setting itemWeightCount= document.frmGridServlet.itemWeightCount.value' );");
//
//        strmResponse.println("}");
//        strmResponse.println("else {");
         strmResponse.println("{");
        strmResponse.println("document.frmGridServlet.itemWeightCount.value=itemWeightCount;");
        strmResponse.println("}");
        //strmResponse.println("alert(txtNoItems);");
        strmResponse.println("if(txtNoItems==null)");
        strmResponse.println("{");
        strmResponse.println("saveAddress('C');");
        //strmResponse.println("alert('after save address c');");
        strmResponse.println("saveAddress('D');");
        //strmResponse.println("alert('after save address d');");
        strmResponse.println("    document.frmGridServlet.submit();");
        strmResponse.println("}else");
        strmResponse.println("{");
        strmResponse.println("        if((txtNoItems.value==\"\" || parseInt(txtNoItems.value)==0 || isNaN(parseInt(txtNoItems.value))) &&  document.frmGridServlet.sMode.value=='" + sMode_Add + "')");
        strmResponse.println("              alert('Please enter No.of.Items.');");
        strmResponse.println("        else if(isNaN(txtLength.value) && txtLength.value.length>0)");
        strmResponse.println("        {");
        strmResponse.println("          alert('Invalid length. Please correct.');");
        strmResponse.println("        }");
        strmResponse.println("        else if(isNaN(txtWidth.value) && txtWidth.value.length>0)");
        strmResponse.println("        {");
        strmResponse.println("          alert('Invalid width. Please correct.');");
        strmResponse.println("        }");
        strmResponse.println("        else if(isNaN(txtHeight.value) && txtHeight.value.length>0)");
        strmResponse.println("        {");
        strmResponse.println("          alert('Invalid height. Please correct.');");
        strmResponse.println("        }else{");
        strmResponse.println("saveAddress('C');");
        //strmResponse.println("alert('after save address c');");
        strmResponse.println("saveAddress('D');");
        //strmResponse.println("alert('after save address d');");
        strmResponse.println("    document.frmGridServlet.submit();");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("}");

        
        
         strmResponse.println("function vOnAfterLoad()");
        strmResponse.println("{");
//        strmResponse.println("alert('I am inside vOnLoad() method, itemWeightCount=' + itemWeightCount);");
//        strmResponse.println("itemWeightCount=false;");
        strmResponse.println(" if (itemWeightCount > 0) { ") ;
        strmResponse.println("document.getElementById(\"txtTotalWeight\").disabled=true;");
         strmResponse.println(" } ") ;
         strmResponse.println(" } ") ;
        
        strmResponse.println("function vOnLoad()");
        strmResponse.println("{");
//        strmResponse.println("alert('I am inside vOnLoad() method, itemWeightCount=' + itemWeightCount);");
//        strmResponse.println("itemWeightCount=false;");
        strmResponse.println(" if (itemWeightCount > 0) { ") ;
        strmResponse.println("document.getElementById(\"txtTotalWeight\").disabled=true;");
         strmResponse.println(" } ") ;
        
        strmResponse.println("    if ((document.frmGridServlet.lRowCount.value != \"0\")"
                + " && (document.frmGridServlet.sMode.value == '" + sMode_Add + "') && (document.frmGridServlet.itemfocus.value==\"true\"))");
        strmResponse.println("    {");
        strmResponse.println("     document.frmGridServlet.sEditCell"
                + Integer.toString(nFirstEditColumn()) + ".focus();");
        strmResponse.println("    }");
        strmResponse.println("else{");
        strmResponse.println("    if ((document.frmGridServlet.lRowCount1.value != \"0\")"
                + " && (document.frmGridServlet.sMode1.value == '" + sMode_Add + "') && (document.frmGridServlet.comfocus.value=\"true\"))");
        strmResponse.println("    {");
        strmResponse.println("     document.frmGridServlet.sEditCell1"
                + Integer.toString(nFirstEditColumn()) + ".focus();");
        strmResponse.println("    }");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println();
        strmResponse.println("function vDelete(sNextRowID, txtWeight)");
        strmResponse.println("{");
        strmResponse.println("    if (confirm(\"Delete record - are you sure ?\"))");
        strmResponse.println("    {");
        strmResponse.println("     document.frmGridServlet.sCalledBy.value='"
                + sCalledBy_Delete + "';");
//         strmResponse.println("        document.frmGridServlet.ParcelNameId.value='';");
//         strmResponse.println("        document.frmGridServlet.ParcelName.value='SELECT';");
         
//         strmResponse.println("        var txtWeight=document.getElementById(\"" + sParam_EditCell + "1\");");
        
        // commented started
//         strmResponse.println("var txtTotalWeight=document.getElementById(\"txtTotalWeight\");");      
//        strmResponse.println("if( !parseFloat(txtWeight)==0 && txtWeight.length>0){");
//        
////      
//       
//        
//        strmResponse.println(" if (txtTotalWeight  == 0) { ") ;
////        strmResponse.println("alert('inside if itemWeightCount == 0');");        
////        strmResponse.println("txtTotalWeight.value = txtWeight.value;");        
//        strmResponse.println("} else {");
//        
////        strmResponse.println("alert('inside ELSE itemWeightCount == 0');");        
//        
//        strmResponse.println("txtTotalWeight.value = (txtTotalWeight.value-parseFloat(txtWeight));");  
//        
////        strmResponse.println("alert('txtTotalWeight.value after minus = ' + txtTotalWeight.value);");        
//         strmResponse.println("}");
//        strmResponse.println("}");
        // commented finished
        
          strmResponse.println("var sItemsRowCount=document.getElementById(\"itemsRowCount\").value;");
          
//          strmResponse.println("  alert('inside vDelete, B4 decrement sItemsRowCount == ' + sItemsRowCount);");
         strmResponse.println("    document.frmGridServlet.itemsRowCount.value = (sItemsRowCount-1);");
      
//         strmResponse.println("  alert('inside vDelete, After decrement sItemsRowCount == ' + (sItemsRowCount-1));");
//        strmResponse.println("  alert('inside vDelete, sNextRowID == ' + sNextRowID);");
        
//         strmResponse.println("        var txtWeight=document.getElementById(\"" + sParam_EditCell + "2\");");
//           strmResponse.println("  alert('inside vDelete, txtWeight == ' + sTxtItemWeight);");
//         strmResponse.println("if( !parseFloat(sTxtItemWeight)==0 && sTxtItemWeight.length>0){");
//         strmResponse.println("itemWeightCount=itemWeightCount-1;");
//         strmResponse.println("}");
        strmResponse.println("    document.frmGridServlet." + sParam_NextRowID
                + ".value = sNextRowID;");
        strmResponse.println("        document.frmGridServlet.sMode1.value='" + sMode_Add + "';");
        strmResponse.println("        document.frmGridServlet.sCalledBy1.value='" + sCalledBy_Edit + "';");
        strmResponse.println("saveAddress('C');");
        strmResponse.println("saveAddress('D');");
        strmResponse.println("     document.frmGridServlet.submit();");
        strmResponse.println("    }");
        strmResponse.println("}");

        strmResponse.println("function vSave()");
        strmResponse.println("{");
        strmResponse.println("        document.frmGridServlet.sMode.value='" + sMode_Edit + "';");
        strmResponse.println("        document.frmGridServlet.sCalledBy.value='" + sCalledBy_Edit + "';");
        strmResponse.println("       document.frmGridServlet.sMode1.value='" + sMode_Add + "';");
        strmResponse.println("        document.frmGridServlet.sCalledBy1.value='" + sCalledBy_Edit + "';");
        strmResponse.println("        var txtNoItems=document.getElementById(\"" + sParam_EditCell + "0\");");
        strmResponse.println("        var txtLength=document.getElementById(\"" + sParam_EditCell + "1\");");
        strmResponse.println("        var txtWidth=document.getElementById(\"" + sParam_EditCell + "2\");");
        strmResponse.println("        var txtHeight=document.getElementById(\"" + sParam_EditCell + "3\");");

//        strmResponse.println("        var txtParcelType=document.getElementById(\"selParcel\");");
//        strmResponse.println("        var txtWeight=document.getElementById(\"" + sParam_EditCell + "1\");");

         strmResponse.println("var txtTotalWeight=document.getElementById(\"txtTotalWeight\");");
        strmResponse.println(" if (itemWeightCount > 0) { ") ;
        strmResponse.println(" txtTotalWeight.disabled = true;");
        strmResponse.println("}");
        
        // commented started
//        strmResponse.println("if( !parseFloat(txtWeight.value)==0 && txtWeight.value.length>0){");
//        
////        strmResponse.println(" alert('inside vSave() method');");
//        strmResponse.println(" txtTotalWeight.disabled = true;");
//       
//        
//        strmResponse.println(" if (itemWeightCount == 0) { ") ;
////        strmResponse.println("alert('inside if itemWeightCount == 0');");        
//        strmResponse.println("txtTotalWeight.value = txtWeight.value;");        
//        strmResponse.println("} else {");
//        
////        strmResponse.println("alert('inside ELSE itemWeightCount == 0');");        
//        
//        strmResponse.println("txtTotalWeight.value = (txtTotalWeight.value+txtWeight.value);");
//        strmResponse.println("");
//        strmResponse.println("}");
//        strmResponse.println("}");

        strmResponse.println("        if(txtNoItems.value==\"\" || parseInt(txtNoItems.value)==0 || isNaN(parseInt(txtNoItems.value)))");
        strmResponse.println("              alert('Please enter No.of.Items.');");
        strmResponse.println("        else if(isNaN(txtLength.value) && txtLength.value.length>0)");
        strmResponse.println("        {");
        strmResponse.println("          alert('Invalid length. Please correct.');");
        strmResponse.println("        }");
        strmResponse.println("        else if(isNaN(txtWidth.value) && txtWidth.value.length>0)");
        strmResponse.println("        {");
        strmResponse.println("          alert('Invalid width. Please correct.');");
        strmResponse.println("        }");
        strmResponse.println("        else if(isNaN(txtHeight.value) && txtHeight.value.length>0)");
        strmResponse.println("        {");
        strmResponse.println("          alert('Invalid height. Please correct.');");
        strmResponse.println("        }else{");
        strmResponse.println("saveAddress('C');");
        strmResponse.println("saveAddress('D');");
        strmResponse.println("        document.frmGridServlet.submit();");
        strmResponse.println("}");
        strmResponse.println("}");


        //commodity grid scripts
        strmResponse.println("function vSubmit1(sNextRowID)");
        strmResponse.println("{");
        strmResponse.println("var error=true;");
        strmResponse.println("var message=\"\";");

        strmResponse.println("document.frmGridServlet.itemWeightCount.value=itemWeightCount;");
        strmResponse.println("document.frmGridServlet.comfocus.value=\"true\";");
        strmResponse.println("document.frmGridServlet.itemfocus.value=\"false\";");
        strmResponse.println("var selProductType=document.getElementById(\"selProductType\");");
//        strmResponse.println("alert(\"v:\"+isNaN(sNextRowID));");
//        strmResponse.println("alert(\"v:\"+isNaN(sNextRowID));");
        strmResponse.println("document.frmGridServlet." + sParam_NextRowID1 + ".value = sNextRowID;");
//        strmResponse.println("alert(document.frmGridServlet.sMode1.value);");
        strmResponse.println("if(document.frmGridServlet.sMode1.value=='" + sMode_Add + "' && isNaN(sNextRowID) && selProductType.value==\"NDOX\")");
        strmResponse.println("{");

        strmResponse.println("  var selIndemnity=document.getElementById(\"" + sParam_EditCell1 + "3\");");
        strmResponse.println("  var txtTotalValue=document.getElementById(\"" + sParam_EditCell1 + "2\");");
        strmResponse.println("  var txtQuantity=document.getElementById(\"" + sParam_EditCell1 + "1\");");
        strmResponse.println("  var selCommodity=document.getElementById(\"" + sParam_EditCell1 + "0\");");

        //  strmResponse.println("alert(selIndemnity);");
        strmResponse.println("  if(selIndemnity.value==\"SELECT\")");
        strmResponse.println("  {");
        strmResponse.println("      message=\"Please select indemnity.\\n\";");
        strmResponse.println("      error=false;");
        strmResponse.println("  }");

        //  strmResponse.println("alert('txtvalue'+txtTotalValue.value);");
        strmResponse.println("if(txtTotalValue.value==\"\")");
        strmResponse.println("{");
        //  strmResponse.println("alert('in if');");
        strmResponse.println("      message=message+\"Please enter Total Value.\\n\";");
        strmResponse.println("      error=false;");
        strmResponse.println("}");

        strmResponse.println("if(selCommodity.value==\"\")");
        strmResponse.println("{");
        strmResponse.println("      message=message+\"Please enter or select commodity.\\n\";");
        strmResponse.println("      error=false;");
        strmResponse.println("}");

        strmResponse.println("var decTotal=format_number(txtTotalValue.value,2);");
        //strmResponse.println("alert(decTotal);");
        strmResponse.println("if(decTotal.length>15 || decTotal == 0)");
        strmResponse.println("{");
        strmResponse.println("      message=message+\"Invalid Total Value amount. Please correct.\\n\";");
        strmResponse.println("      error=false;");
        strmResponse.println("}");

        strmResponse.println("if(isNaN(txtQuantity.value) && txtQuantity.value.length>0)");
        strmResponse.println("{");
        strmResponse.println("      message=message+\"Invalid Quantity. Please correct.\\n\";");
        strmResponse.println("      error=false;");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("if(error==true)");
        strmResponse.println("{");
        strmResponse.println("    if (sNextRowID == 'new')");
        strmResponse.println("    {");
        //strmResponse.println("alert('test');");
        strmResponse.println("        document.frmGridServlet.sMode1.value="
                + "'" + sMode_Add + "';");
        strmResponse.println("        document.frmGridServlet.sMode.value='" + sMode_Add + "';");
        strmResponse.println("        document.frmGridServlet.sCalledBy.value='" + sCalledBy_Edit + "';");
        // strmResponse.println("alert('test1');");
        strmResponse.println("    }");
        strmResponse.println("    else");
        strmResponse.println("    {");
        strmResponse.println("        document.frmGridServlet.sMode1.value='" + sMode_Edit + "';");
        strmResponse.println("        document.frmGridServlet.sCalledBy1.value='" + sCalledBy_Summary + "';");
//        strmResponse.println("        document.frmGridServlet.sMode.value='" + sMode_Add + "';");
//        strmResponse.println("        document.frmGridServlet.sCalledBy.value='" + sCalledBy_Edit + "';");
        strmResponse.println("     }");
        strmResponse.println("saveAddress('C');");
        //strmResponse.println("alert('after save address c');");
        strmResponse.println("saveAddress('D');");
        //strmResponse.println("alert('after save address d');");
        strmResponse.println("    document.frmGridServlet.submit();");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("alert(message);");
        strmResponse.println("}");

        strmResponse.println("function vDelete1(sNextRowID)");
        strmResponse.println("{");
        strmResponse.println("    if (confirm(\"Delete record - are you sure ?\"))");
        strmResponse.println("    {");
        strmResponse.println("     document.frmGridServlet.sCalledBy1.value='"
                + sCalledBy_Delete + "';");
        //   strmResponse.println("  alert(sNextRowID);");
        strmResponse.println("    document.frmGridServlet." + sParam_NextRowID1
                + ".value = sNextRowID;");
        strmResponse.println("        document.frmGridServlet.sMode.value='" + sMode_Add + "';");
        strmResponse.println("        document.frmGridServlet.sCalledBy.value='" + sCalledBy_Edit + "';");
        strmResponse.println("saveAddress('C','frmGridServlet');");
        strmResponse.println("saveAddress('D','frmGridServlet');");
        strmResponse.println("     document.frmGridServlet.submit();");
        strmResponse.println("    }");
        strmResponse.println("}");

        strmResponse.println("function vSave1()");
        strmResponse.println("{");
        strmResponse.println("        document.frmGridServlet.sMode1.value='" + sMode_Edit + "';");
        strmResponse.println("        document.frmGridServlet.sCalledBy1.value='" + sCalledBy_Edit + "';");
        strmResponse.println("        document.frmGridServlet.sMode.value='" + sMode_Add + "';");
        strmResponse.println("        document.frmGridServlet.sCalledBy.value='" + sCalledBy_Edit + "';");
        strmResponse.println("  var selIndemnity=document.getElementById(\"" + sParam_EditCell1 + "3\");");
        strmResponse.println("  var txtTotalValue=document.getElementById(\"" + sParam_EditCell1 + "2\");");
        strmResponse.println("  var selCommodity=document.getElementById(\"" + sParam_EditCell1 + "0\");");

        strmResponse.println("  var txtQuantity=document.getElementById(\"" + sParam_EditCell1 + "1\");");
        strmResponse.println("var error=true;");
        strmResponse.println("var message=\"\";");
        //strmResponse.println("alert(selIndemnity);");
        strmResponse.println("  if(selIndemnity.value==\"SELECT\")");
        strmResponse.println("  {");
        strmResponse.println("      message=\"Please select indemnity.\\n\";");
        strmResponse.println("      error=false;");
        strmResponse.println("  }");

        //strmResponse.println("alert('txtvalue'+txtTotalValue.value);");
        strmResponse.println("if(txtTotalValue.value==\"\")");
        strmResponse.println("{");
        //strmResponse.println("alert('in if');");
        strmResponse.println("      message=message+\"Please enter Total Value.\\n\";");
        strmResponse.println("      error=false;");
        strmResponse.println("}");

        strmResponse.println("if(selCommodity.value==\"\")");
        strmResponse.println("{");
        strmResponse.println("      message=message+\"Please enter or select commodity.\\n\";");
        strmResponse.println("      error=false;");
        strmResponse.println("}");

        strmResponse.println("var decTotal=format_number(txtTotalValue.value,2);");
        //strmResponse.println("alert(decTotal);");
        strmResponse.println("if(decTotal.length>15 || decTotal == 0)");
        strmResponse.println("{");
        strmResponse.println("      message=message+\"Invalid Total Value amount. Please correct.\"");
        strmResponse.println("      error=false;");
        strmResponse.println("}");

        strmResponse.println("if(isNaN(txtQuantity.value) && txtQuantity.value.length>0)");
        strmResponse.println("{");
        strmResponse.println("      message=message+\"Invalid Quantity. Please correct\\n\";");
        strmResponse.println("      error=false;");
        strmResponse.println("}");
        //strmResponse.println("alert('after validations');");
        strmResponse.println("if(error==true)");
        strmResponse.println("{");
        strmResponse.println("saveAddress('C');");
        strmResponse.println("saveAddress('D');");
        strmResponse.println("        document.frmGridServlet.submit();");
        strmResponse.println("}");
        strmResponse.println("else{");
        strmResponse.println("alert(message);}");
        strmResponse.println("}");


//        strmResponse.println("function chkResFlag_Click()");
//        strmResponse.println("{");
//        strmResponse.println("var chkResidential=document.getElementById(\"chkCResidentialFlag\");");
//        strmResponse.println("if(chkResidential.checked){");
//        strmResponse.println("sResidentialFlag = \"Y\";");
////        strmResponse.println("  document.getElementById(\"lblPhone\").style.color='#CC0000';");
////        strmResponse.println("  document.getElementById(\"txtPhone\").style.border='3px solid #7F9DB9';");
//        strmResponse.println("}else{");
//        strmResponse.println("sResidentialFlag = \"N\";");
////        strmResponse.println("  document.getElementById(\"lblPhone\").style.color='#666666';");
////        strmResponse.println("  document.getElementById(\"txtPhone\").style.border='1px solid #7F9DB9';");
//        strmResponse.println("}");
//        strmResponse.println("}");



//        strmResponse.println("function clickButton(e, buttonid,type)");
//        strmResponse.println("{");
//        strmResponse.println("  var evt = e ? e : window.event;");
//        strmResponse.println("  var bt = document.getElementById(buttonid);");
//        strmResponse.println("  if (bt)");
//        strmResponse.println("  {");
//        strmResponse.println("      if (evt.keyCode == 13 || evt.keyCode == 3 )");
//        strmResponse.println("      {");
//        strmResponse.println("          if(type==\"I\")");
//        strmResponse.println("              vSubmit('new');");
//        strmResponse.println("          else");
//        strmResponse.println("              vSubmit1('new');");
//        strmResponse.println("      }");
//        strmResponse.println("  }");
//        strmResponse.println("}");

        //check the Item and Commodity Grid on Next button click
//        strmResponse.println("function gridRowAdd()");
//        strmResponse.println("{");
//        
//        strmResponse.println("alert ('inside gridRowAdd()...........');");
//        
//        strmResponse.println("var com0=document.getElementById(\"" + sParam_EditCell1 + "0\").value;");
//        strmResponse.println("var com1=document.getElementById(\"" + sParam_EditCell1 + "1\").value;");
//        strmResponse.println("var com2=document.getElementById(\"" + sParam_EditCell1 + "2\").value;");
//        strmResponse.println("var com3=document.getElementById(\"" + sParam_EditCell1 + "3\").value;");
//        
////         strmResponse.println("var itemParcel=document.getElementById(\"selParcel\").value;");
////        strmResponse.println("if(itemParcel ==\"SELECT\"){");
//        strmResponse.println("var item0=document.getElementById(\"" + sParam_EditCell + "0\").value;");
//        strmResponse.println("var item1=document.getElementById(\"" + sParam_EditCell + "3\").value;");
//        strmResponse.println("var item2=document.getElementById(\"" + sParam_EditCell + "4\").value;");
////        strmResponse.println("var item3=document.getElementById(\"" + sParam_EditCell + "5\").value;");
//
//       
//        strmResponse.println("var item3=document.getElementById(\"" + sParam_EditCell + "1\").value;");
////        strmResponse.println("}");
//        
//        strmResponse.println("var message=\"\";");
//        strmResponse.println("var sCommodity=\"\";");
//        strmResponse.println("var sItem=\"\";");
//        strmResponse.println("var selProductType=document.getElementById(\"selProductType\");");
//
////        strmResponse.println("if(selProductType.value==\"NDOX\"){");
////        strmResponse.println("if(itemParcel ==\"SELECT\"){");
//        strmResponse.println("if(item0.length >0 || item1.length >0 || item2.length >0 ||item3.length >0)");
//        strmResponse.println("{");
//        strmResponse.println("    if(item0==\"\" || parseInt(item0)==0 || isNaN(parseInt(item0)))");
//        strmResponse.println("    {");
//        strmResponse.println("       message='Please enter No.of.Items';");
//        strmResponse.println("       return message;");
//        strmResponse.println("    }");
//        strmResponse.println("    else if(isNaN(item1) && item1.length>0)");
//        strmResponse.println("    {");
//        strmResponse.println("       message='Invalid length. Please correct';");
//        strmResponse.println("       return message;");
//        strmResponse.println("    }");
//        strmResponse.println("    else if(isNaN(item2) && item2.length>0)");
//        strmResponse.println("    {");
//        strmResponse.println("       message='Invalid width. Please correct';");
//        strmResponse.println("       return message;");
//        strmResponse.println("    }");
//        strmResponse.println("    else if(isNaN(item3) && item3.length>0)");
//        strmResponse.println("    {");
//        strmResponse.println("       message='Invalid height. Please correct';");
//        strmResponse.println("       return message;");
//        strmResponse.println("    }");
//        strmResponse.println("    else");
//        strmResponse.println("    {");
//        strmResponse.println("      sItem=(item0+\",\"+item1+\",\"+item2+\",\"+item3);");
//        strmResponse.println("      document.VerifyForm.itemTags.value=sItem;");
//        //strmResponse.println("      alert(document.VerifyForm.itemTags.value);");
//        strmResponse.println("    }");
////        strmResponse.println("}");
////strmResponse.println("}");
//
//        strmResponse.println("if(com0.length >0 || com1.length >0 || com2.length >0)");
//        strmResponse.println("{");
//        strmResponse.println("  if(com3==\"SELECT\")");
//        strmResponse.println("  {");
//        strmResponse.println("      message=\"Please select indemnity.\\n\";");
//        strmResponse.println("      return message;");
//        strmResponse.println("  }");
//
//        //  strmResponse.println("alert('txtvalue'+txtTotalValue.value);");
//        strmResponse.println("if(com2==\"\" || parseInt(com2)==0)");
//        strmResponse.println("{");
//        //  strmResponse.println("alert('in if');");
//        strmResponse.println("      message=message+\"Please enter Total Value.\\n\";");
//        strmResponse.println("      return message;");
//        strmResponse.println("}");
//
//        strmResponse.println("if(com0==\"\")");
//        strmResponse.println("{");
//        strmResponse.println("      message=message+\"Please enter or select commodity.\\n\";");
//        strmResponse.println("      return message;");
//        strmResponse.println("}");
//
//        strmResponse.println("var decTotal=format_number(com2,2);");
//        //strmResponse.println("alert(decTotal);");
//        strmResponse.println("if(decTotal.length>15 || decTotal == 0)");
//        strmResponse.println("{");
//        strmResponse.println("      message=message+\"Invalid Total Value amount. Please correct.\\n\";");
//        strmResponse.println("      return message;");
//        strmResponse.println("}");
//
//        strmResponse.println("if(isNaN(com1) && com1.length>0)");
//        strmResponse.println("{");
//        strmResponse.println("      message=message+\"Invalid Quantity. Please correct\\n\";");
//        strmResponse.println("      return message;");
//        strmResponse.println("}");
//// dc - 9/12/2009 - fix up commas and single quotes
////        strmResponse.println("      sCommodity=(com0+\",\"+com1+\",\"+com2+\",\"+com3);");
//        strmResponse.println("      sCommodity=(com0.replace(/,/g,';')+\",\"+com1+\",\"+com2+\",\"+com3);");
//        strmResponse.println("      document.VerifyForm.commodityTags.value=sCommodity;");
//        strmResponse.println("}");
//        strmResponse.println("message=\"true\";");
//        strmResponse.println("}");
//        strmResponse.println("else");
//        strmResponse.println("message=\"true\";");
//        strmResponse.println("return message;");
//        strmResponse.println("}");

//        asfsadf
        
         strmResponse.println("function gridRowAdd()");
        strmResponse.println("{");
        
//        strmResponse.println("alert ('inside gridRowAdd()...........');");
        
       
        
//         strmResponse.println("var itemParcel=document.getElementById(\"selParcel\").value;");
//        strmResponse.println("if(itemParcel ==\"SELECT\"){");
        strmResponse.println("var item0=document.getElementById(\"" + sParam_EditCell + "0\").value;");
//        strmResponse.println("var item1=document.getElementById(\"" + sParam_EditCell + "1\").value;");
        strmResponse.println("var item2=document.getElementById(\"" + sParam_EditCell + "1\").value;");
//        strmResponse.println("var item3=document.getElementById(\"" + sParam_EditCell + "5\").value;");

       
        strmResponse.println("var item3=document.getElementById(\"" + sParam_EditCell + "2\").value;");
        
        strmResponse.println("var item4=document.getElementById(\"" + sParam_EditCell + "3\").value;");
//        strmResponse.println("}");
        
        strmResponse.println("var message=\"\";");
        strmResponse.println("var sCommodity=\"\";");
        strmResponse.println("var sItem=\"\";");
        strmResponse.println("var selProductType=document.getElementById(\"selProductType\");");

//        strmResponse.println("if(selProductType.value==\"NDOX\"){");
//        strmResponse.println("if(itemParcel ==\"SELECT\"){");
//        strmResponse.println("if(item0.length >0 /*|| item1.length >0 */ || item2.length >0 || item3.length >0 || item4.length >0)");
        strmResponse.println("if(item0.length >0  || item2.length >0 || item3.length >0 || item4.length >0)");
        strmResponse.println("{");
		
        strmResponse.println("    if(item0==\"\" || parseInt(item0)==0 || isNaN(parseInt(item0)))");
        strmResponse.println("    {");
        strmResponse.println("       message='Please enter No.of.Items';");
        strmResponse.println("       return message;");
        strmResponse.println("    }");
//        strmResponse.println("    else if(isNaN(item1) && item1.length>0)");
//        strmResponse.println("    {");
//        strmResponse.println("       message='Invalid weight. Please correct';");
//        strmResponse.println("       return message;");
//        strmResponse.println("    }");
        strmResponse.println("    else if(isNaN(item2) && item2.length>0)");
        strmResponse.println("    {");
        strmResponse.println("       message='Invalid length. Please correct';");
        strmResponse.println("       return message;");
        strmResponse.println("    }");
        strmResponse.println("    else if(isNaN(item3) && item3.length>0)");
        strmResponse.println("    {");
        strmResponse.println("       message='Invalid width. Please correct';");
        strmResponse.println("       return message;");
        strmResponse.println("    }");
        
        strmResponse.println("    else if(isNaN(item4) && item4.length>0)");
        strmResponse.println("    {");
        strmResponse.println("       message='Invalid height. Please correct';");
        strmResponse.println("       return message;");
        strmResponse.println("    }");
        
        strmResponse.println("    else");
        strmResponse.println("    {");
//        strmResponse.println("      sItem=(item0+\",\"+item1+\",\"+item2+\",\"+item3+\",\"+item4);");
        strmResponse.println("      sItem=(item0+\",\"+item2+\",\"+item3+\",\"+item4);");
        strmResponse.println("      document.VerifyForm.itemTags.value=sItem;");
        //strmResponse.println("      alert(document.VerifyForm.itemTags.value);");
        strmResponse.println("    }");
//        strmResponse.println("}");
//strmResponse.println("}");

strmResponse.println("if(selProductType.value==\"NDOX\"){");

 strmResponse.println("var com0=document.getElementById(\"" + sParam_EditCell1 + "0\").value;");
        strmResponse.println("var com1=document.getElementById(\"" + sParam_EditCell1 + "1\").value;");
        strmResponse.println("var com2=document.getElementById(\"" + sParam_EditCell1 + "2\").value;");
        strmResponse.println("var com3=document.getElementById(\"" + sParam_EditCell1 + "3\").value;");
        strmResponse.println("if(com0.length >0 || com1.length >0 || com2.length >0)");
        strmResponse.println("{");
        strmResponse.println("  if(com3==\"SELECT\")");
        strmResponse.println("  {");
        strmResponse.println("      message=\"Please select indemnity.\\n\";");
        strmResponse.println("      return message;");
        strmResponse.println("  }");

        //  strmResponse.println("alert('txtvalue'+txtTotalValue.value);");
        strmResponse.println("if(com2==\"\" || parseInt(com2)==0)");
        strmResponse.println("{");
        //  strmResponse.println("alert('in if');");
        strmResponse.println("      message=message+\"Please enter Total Value.\\n\";");
        strmResponse.println("      return message;");
        strmResponse.println("}");

        strmResponse.println("if(com0==\"\")");
        strmResponse.println("{");
        strmResponse.println("      message=message+\"Please enter or select commodity.\\n\";");
        strmResponse.println("      return message;");
        strmResponse.println("}");

        strmResponse.println("var decTotal=format_number(com2,2);");
        //strmResponse.println("alert(decTotal);");
        strmResponse.println("if(decTotal.length>15 || decTotal == 0)");
        strmResponse.println("{");
        strmResponse.println("      message=message+\"Invalid Total Value amount. Please correct.\\n\";");
        strmResponse.println("      return message;");
        strmResponse.println("}");

        strmResponse.println("if(isNaN(com1) && com1.length>0)");
        strmResponse.println("{");
        strmResponse.println("      message=message+\"Invalid Quantity. Please correct\\n\";");
        strmResponse.println("      return message;");
        strmResponse.println("}");
// dc - 9/12/2009 - fix up commas and single quotes
//        strmResponse.println("      sCommodity=(com0+\",\"+com1+\",\"+com2+\",\"+com3);");
        strmResponse.println("      sCommodity=(com0.replace(/,/g,';')+\",\"+com1+\",\"+com2+\",\"+com3);");
        strmResponse.println("      document.VerifyForm.commodityTags.value=sCommodity;");
        strmResponse.println("}");
		
		strmResponse.println("}");
		
        strmResponse.println("message=\"true\";");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("message=\"true\";");
        
//        strmResponse.println("alert ('inside gridRowAdd(), message = '+ message +'...........');");
        
        strmResponse.println("return message;");
        strmResponse.println("}");

        //store all the address fields values in hidden form fields.
        strmResponse.println("function saveAddress(sType)");
        strmResponse.println("{");

        if (login.getDeptVector().size() > 0 && login.getDepartmentFlag().equals("V")) {
            strmResponse.println("var selDept=document.getElementById(\"selDept\");");
        } else if (login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("F")) {
            strmResponse.println("var txtBDept=document.getElementById(\"txtBDept\");");
        }

        strmResponse.println("var txtBCaller=document.getElementById(\"txtBCaller\");");
        strmResponse.println("var txtBPhone=document.getElementById(\"txtBPhone\");");

        if (login.getRefVector().size() > 0 && login.getRefFlag().equals("V")) {
            strmResponse.println("var selRef=document.getElementById(\"selRef\");");
        } else if (login.getRefFlag().equals("R") || login.getRefFlag().equals("Y")) {
            strmResponse.println("var txtBRef=document.getElementById(\"txtBRef\");");
        }

        strmResponse.println("var txtBHAWBNO=document.getElementById(\"txtBHAWBNO\");");

        strmResponse.println("var rdCollectNow=document.getElementById(\"rdCollectNow\");");
        strmResponse.println("var rdCollectAdvanced=document.getElementById(\"rdCollectAdvanced\");");
        strmResponse.println("var txtCollectDate=document.getElementById(\"txtCollectDate\");");
        strmResponse.println("var selCollectHH=document.getElementById(\"selCollectHH\");");
        strmResponse.println("var selCollectMM=document.getElementById(\"selCollectMM\");");
        strmResponse.println("var chkPickupBefore=document.getElementById(\"chkPickupBefore\");");
        //strmResponse.println("alert('chkPickupBefore:'+chkPickupBefore.checked);");
        strmResponse.println("var selHHPickupBefore=document.getElementById(\"selHHPickupBefore\");");
        strmResponse.println("var selMMPickupBefore=document.getElementById(\"selMMPickupBefore\");");
        strmResponse.println("var selProductType=document.getElementById(\"selProductType\");");
        strmResponse.println("var txtTotalPieces=document.getElementById(\"txtTotalPieces\");");
        strmResponse.println("var txtTotalWeight=document.getElementById(\"txtTotalWeight\");");
        strmResponse.println("var txtTotalVolWeight=document.getElementById(\"txtTotalVolWeight\");");

        strmResponse.println("if(sType==\"C\")");
        strmResponse.println("{");

        strmResponse.println("var txtCompany=document.getElementById(\"txtCCompany\");");
        strmResponse.println("var txtContact=document.getElementById(\"txtCContact\");");
        strmResponse.println("var txtAddress1=document.getElementById(\"txtCAddress1\");");
        strmResponse.println("var txtAddress2=document.getElementById(\"txtCAddress2\");");
        strmResponse.println("var txtAddress3=document.getElementById(\"txtCAddress3\");");
        strmResponse.println("var txtPhone=document.getElementById(\"txtCPhone\");");
        strmResponse.println("var txtTown=document.getElementById(\"txtCTown\");");
        strmResponse.println("var txtCounty=document.getElementById(\"txtCCounty\");");
        strmResponse.println("var txtPostcode=document.getElementById(\"txtCPostcode\");");
        strmResponse.println("var selCountry=document.getElementById(\"selCCountry\");");

        // added by Adnan on 27 Aug 2013 against Residential Flag change
//        strmResponse.println("var selResidentialFlag=document.getElementById(\"chkCResidentialFlag\");");
        strmResponse.println("if (document.getElementById(\"chkCResidentialFlag\").checked ){");
        strmResponse.println("var selResidentialFlag=\"Y\";");
        strmResponse.println("} else {");
        strmResponse.println("var selResidentialFlag=\"N\";");
        strmResponse.println("}");

        strmResponse.println("var txtInstruct=document.getElementById(\"txtCInstruct\");");
        strmResponse.println("var txtLatitude=document.getElementById(\"txtCLatitude\");");
        strmResponse.println("var txtLongitude=document.getElementById(\"txtCLongitude\");");
        //strmResponse.println("alert('test c');");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");

        strmResponse.println("var txtCompany=document.getElementById(\"txtDCompany\");");
        strmResponse.println("var txtContact=document.getElementById(\"txtDContact\");");
        strmResponse.println("var txtAddress1=document.getElementById(\"txtDAddress1\");");
        strmResponse.println("var txtAddress2=document.getElementById(\"txtDAddress2\");");
        strmResponse.println("var txtAddress3=document.getElementById(\"txtDAddress3\");");
        strmResponse.println("var txtPhone=document.getElementById(\"txtDPhone\");");
        strmResponse.println("var txtTown=document.getElementById(\"txtDTown\");");
        strmResponse.println("var txtCounty=document.getElementById(\"txtDCounty\");");
        strmResponse.println("var txtPostcode=document.getElementById(\"txtDPostcode\");");
        strmResponse.println("var selCountry=document.getElementById(\"selDCountry\");");

        // added by Adnan on 27 Aug 2013 against Residential Flag change
//        strmResponse.println("var selResidentialFlag=document.getElementById(\"selDResidentialFlag\");");
        strmResponse.println("if (document.getElementById(\"chkDResidentialFlag\").checked ){");
        strmResponse.println("var selResidentialFlag=\"Y\";");
        strmResponse.println("} else {");
        strmResponse.println("var selResidentialFlag=\"N\";");
        strmResponse.println("}");

        strmResponse.println("var txtInstruct=document.getElementById(\"txtDInstruct\");");
        strmResponse.println("var txtLatitude=document.getElementById(\"txtDLatitude\");");
        strmResponse.println("var txtLongitude=document.getElementById(\"txtDLongitude\");");
        //strmResponse.println("alert('test d');");
        strmResponse.println("}");

        if (login.getDeptVector().size() > 0 && login.getDepartmentFlag().equals("V")) {
            strmResponse.println("var sBDept=selDept.value;");
        } else if (login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("F")) {
            strmResponse.println("var sBDept=txtBDept.value;");
        }

        strmResponse.println("var sBCaller=txtBCaller.value;");
        strmResponse.println("var sBPhone=txtBPhone.value;");

        if (login.getRefVector().size() > 0 && login.getRefFlag().equals("V")) {
            strmResponse.println("var sBRef=selRef.value;");
        } else if (login.getRefFlag().equals("R") || login.getRefFlag().equals("Y")) {
            strmResponse.println("var sBRef=txtBRef.value;");
        }

        strmResponse.println("var sBHAWBNO=txtBHAWBNO.value;");

        strmResponse.println("var sCompany=txtCompany.value;");
        //strmResponse.println("alert(sCompany);");
        strmResponse.println("var sContact=txtContact.value;");
        strmResponse.println("var sAddress1=txtAddress1.value;");
        strmResponse.println("var sAddress2=txtAddress2.value;");
        strmResponse.println("var sAddress3=txtAddress3.value;");
        strmResponse.println("var sPhone=txtPhone.value;");
        strmResponse.println("var sTown=txtTown.value;");
        strmResponse.println("var sCounty=txtCounty.value;");
        strmResponse.println("var sPostcode=txtPostcode.value;");
        strmResponse.println("var sCountry=selCountry.value;");
        strmResponse.println("var sInstruct=txtInstruct.value;");
        strmResponse.println("var sLatitude=txtLatitude.value;");
        strmResponse.println("var sLongitude=txtLongitude.value;");

//        strmResponse.println("var sResidentialFlag=selResidentialFlag.checked;");

//        strmResponse.println("if(selResidentialFlag.checked){");
//        strmResponse.println("var sResidentialFlag =\"Y\";");
//        strmResponse.println("} else {");
//        strmResponse.println("var sResidentialFlag =\"N\";");
//        strmResponse.println("}");

//        strmResponse.println("sResidentialFlag=selResidentialFlag.checked;");

        strmResponse.println("if(sType==\"C\")");
        strmResponse.println("{");
        strmResponse.println("document.frmGridServlet.CCompanyName.value=sCompany;");
        strmResponse.println("document.frmGridServlet.CContactName.value=sContact;");
        strmResponse.println("document.frmGridServlet.CAddress1.value=sAddress1;");
        strmResponse.println("document.frmGridServlet.CAddress2.value=sAddress2;");
        strmResponse.println("document.frmGridServlet.CAddress3.value=sAddress3;");
        strmResponse.println("document.frmGridServlet.CPhone.value=sPhone;");
        strmResponse.println("document.frmGridServlet.CTown.value=sTown;");
        strmResponse.println("document.frmGridServlet.CCounty.value=sCounty;");
        strmResponse.println("document.frmGridServlet.CPostcode.value=sPostcode;");
        strmResponse.println("document.frmGridServlet.CCountry.value=sCountry;");

        strmResponse.println("if (document.getElementById(\"chkCResidentialFlag\").checked ){");
        strmResponse.println("var sResidentialFlag =\"Y\";");
        strmResponse.println("} else {");
        strmResponse.println("var sResidentialFlag =\"N\";");
        strmResponse.println("}");
        
//        strmResponse.println("alert('sResidentialFlag == ' + sResidentialFlag);");
        // added by Adnan on 27 Aug 2013 against Residential Flag change
        strmResponse.println("document.frmGridServlet.CResidentialFlag.value=sResidentialFlag;");

        strmResponse.println("document.frmGridServlet.CInstruct.value=sInstruct;");
        strmResponse.println("document.frmGridServlet.CLatitude.value=sLatitude;");
        strmResponse.println("document.frmGridServlet.CLongitude.value=sLongitude;");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("document.frmGridServlet.DCompanyName.value=sCompany;");
        strmResponse.println("document.frmGridServlet.DContactName.value=sContact;");
        strmResponse.println("document.frmGridServlet.DAddress1.value=sAddress1;");
        strmResponse.println("document.frmGridServlet.DAddress2.value=sAddress2;");
        strmResponse.println("document.frmGridServlet.DAddress3.value=sAddress3;");
        strmResponse.println("document.frmGridServlet.DPhone.value=sPhone;");
        strmResponse.println("document.frmGridServlet.DTown.value=sTown;");
        strmResponse.println("document.frmGridServlet.DCounty.value=sCounty;");

        strmResponse.println("if (document.getElementById(\"chkDResidentialFlag\").checked ){");
        strmResponse.println("var sResidentialFlag =\"Y\";");
        strmResponse.println("} else {");
        strmResponse.println("var sResidentialFlag =\"N\";");
        strmResponse.println("}");

//        strmResponse.println("alert('sResidentialFlag for Delivery == ' + sResidentialFlag);");
        // added by Adnan on 27 Aug 2013 against Residential Flag change
        strmResponse.println("document.frmGridServlet.DResidentialFlag.value=sResidentialFlag;");

        strmResponse.println("document.frmGridServlet.DPostcode.value=sPostcode;");
        strmResponse.println("document.frmGridServlet.DCountry.value=sCountry;");
        strmResponse.println("document.frmGridServlet.DInstruct.value=sInstruct;");
        strmResponse.println("document.frmGridServlet.DLatitude.value=sLatitude;");
        strmResponse.println("document.frmGridServlet.DLongitude.value=sLongitude;");
        strmResponse.println("}");

        if (login.getDepartmentFlag().equals("V") || login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("F")) {
            strmResponse.println("document.frmGridServlet.BDept.value=sBDept;");
        }
        strmResponse.println("document.frmGridServlet.BCaller.value=sBCaller;");
        strmResponse.println("document.frmGridServlet.BPhone.value=sBPhone;");

        if (login.getRefFlag().equals("V") || login.getRefFlag().equals("Y") || login.getRefFlag().equals("R")) {
            strmResponse.println("document.frmGridServlet.BRef.value=sBRef;");
        }
        strmResponse.println("document.frmGridServlet.BHAWBNO.value=sBHAWBNO;");

        strmResponse.println("if(rdCollectAdvanced.checked)");
        strmResponse.println("document.frmGridServlet.rdCollect.value=rdCollectAdvanced.value;");
        strmResponse.println("if(rdCollectNow.checked)");
        strmResponse.println("document.frmGridServlet.rdCollect.value=rdCollectNow.value;");
        strmResponse.println("document.frmGridServlet.CollectDate.value=txtCollectDate.value;");
        strmResponse.println("document.frmGridServlet.CollectHH.value=selCollectHH.value;");
        strmResponse.println("document.frmGridServlet.CollectMM.value=selCollectMM.value;");
        strmResponse.println("if(chkPickupBefore.checked)");
        strmResponse.println("document.frmGridServlet.chkPickupBefore.value='1';");
        strmResponse.println("else");
        strmResponse.println("document.frmGridServlet.chkPickupBefore.value='0';");

        strmResponse.println("document.frmGridServlet.PickupBeforeHH.value=selHHPickupBefore.value;");
        strmResponse.println("document.frmGridServlet.PickupBeforeMM.value=selMMPickupBefore.value;");

        strmResponse.println("document.frmGridServlet.ProductType.value=selProductType.value;");
        strmResponse.println("document.frmGridServlet.TotalPieces.value=txtTotalPieces.value;");
        strmResponse.println("document.frmGridServlet.TotalWeight.value=txtTotalWeight.value;");
        strmResponse.println("document.frmGridServlet.TotalVolWeight.value=txtTotalVolWeight.value;");


        strmResponse.println("}");

        // Addressbook Popup Javascript

        strmResponse.println("function AddressBookPopup(sType,sMessage)");
        strmResponse.println("{");
        strmResponse.println("if(sType==\"C\")");
        strmResponse.println("var sCompany=document.getElementById(\"txtCCompany\");");
        strmResponse.println("else");
        strmResponse.println("var sCompany=document.getElementById(\"txtDCompany\");");
        strmResponse.println("	 sWinOpen = 1;");
        strmResponse.println("if((winadbook == null) || (winadbook.closed == true))");
        strmResponse.println("	 winadbook=window.open('blank.html','popupwinAB','resizable=no,scrollbars=yes,width=650,height=550');");
        strmResponse.println("else");
        strmResponse.println("   winadbook.focus();");
        strmResponse.println("   document.AddressBookForm." + Constants.SACTION + ".value='AddressBook';");
        strmResponse.println("   document.AddressBookForm.sBookingType.value=sType;");
        strmResponse.println("   document.AddressBookForm.sMessage.value=sMessage;");
        strmResponse.println("   document.AddressBookForm.submit();");
        strmResponse.println("}");

        strmResponse.println("function Search(sType)");
        strmResponse.println("{");
        // strmResponse.println("alert('search');");
        strmResponse.println("if(sType==\"C\")");
        strmResponse.println("var sCompany=document.getElementById(\"txtCCompany\");");
        strmResponse.println("else");
        strmResponse.println("var sCompany=document.getElementById(\"txtDCompany\");");
        strmResponse.println("	 sWinOpen = 1;");
        strmResponse.println("if((winadbook == null) || (winadbook.closed == true))");
        strmResponse.println("	 winadbook=window.open('blank.html','popupwinAB','resizable=no,scrollbars=yes,width=800,height=400');");
        strmResponse.println("else");
        strmResponse.println("   winadbook.focus();");
        strmResponse.println("   document.AddressBookForm." + Constants.SACTION + ".value='Search';");
        strmResponse.println("   document.AddressBookForm.sCompanyName.value=sCompany.value;");
        strmResponse.println("   document.AddressBookForm.sBookingType.value=sType;");
        strmResponse.println("   document.AddressBookForm.submit();");
        strmResponse.println("}");

        //Postcode Lookup

        strmResponse.println("function PostLookup(sType)");
        strmResponse.println("{");
        strmResponse.println("var lblMessage=document.getElementById(\"lblMessage\");");
        strmResponse.println("var selCountry=document.getElementById(\"selDCountry\");");
        strmResponse.println("var error=false;");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = \"\";");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = \"\";");

        strmResponse.println("if(sType==\"C\")");
        strmResponse.println("{");
        strmResponse.println("var txtAddress1=document.getElementById(\"txtCAddress1\");");
        strmResponse.println("var txtPostcode=document.getElementById(\"txtCPostcode\");");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("var txtAddress1=document.getElementById(\"txtDAddress1\");");
        strmResponse.println("var txtPostcode=document.getElementById(\"txtDPostcode\");");
        strmResponse.println("}");
        //strmResponse.println("alert('imnside fn');");
//        strmResponse.println("if(txtAddress1.value==\"\")");
//        strmResponse.println("lblMessage.innerText=\"Please enter Builiding No in the Address1 field to continue to Postcode Lookup\";");
//        strmResponse.println("else");
        strmResponse.println("if(txtPostcode.value==\"\")");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText=\"Please enter postcode to continue to Postcode Lookup\";");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = \"Please enter postcode to continue to Postcode Lookup\";");
        strmResponse.println("  error=true;");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("if(selCountry.value!=\"" + Constants.COUNTRY_CODE + "\" && sType==\"D\")");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText=\"Postcode Lookup applies for UK postcodes only. Please enter UK postcode and select country as United Kingdom to continue to Postcode Lookup\";");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = \"Postcode Lookup applies for UK postcodes only. Please enter UK postcode and select country as United Kingdom to continue to Postcode Lookup\";");
        strmResponse.println("  error=true;");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("if(error==false)");
        strmResponse.println("{");
        // strmResponse.println("alert('imnside fn');");
        strmResponse.println("if(txtAddress1.value==\"\")");
        strmResponse.println("{");
        strmResponse.println("	 sWinOpen = 1;");
        strmResponse.println("if((winpostcode == null) || (winpostcode.closed == true))");
        strmResponse.println("	 winpostcode=window.open('blank.html','HierLookup','resizable=no,scrollbars=yes,width=600,height=400');");
        strmResponse.println("else");
        strmResponse.println("   winpostcode.focus();");
        strmResponse.println("document.HierLookup." + Constants.USER_INPUT + ".value=txtPostcode.value;");
        strmResponse.println("document.HierLookup." + Constants.STYPE + ".value=sType;");
        strmResponse.println("document.HierLookup.submit();");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("	 sWinOpen = 1;");
        strmResponse.println("if((winpostcode == null) || (winpostcode.closed == true))");
        strmResponse.println("	 winpostcode=window.open('blank.html','PostcodeLookup','resizable=no,scrollbars=yes,width=600,height=400');");
        strmResponse.println("else");
        strmResponse.println("   winpostcode.focus();");
        strmResponse.println("document.PostcodeLookup." + Constants.USER_INPUT + ".value=txtPostcode.value+\"^\"+txtAddress1.value;");
        strmResponse.println("document.PostcodeLookup." + Constants.STYPE + ".value=sType;");
        strmResponse.println("document.PostcodeLookup.submit();");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("}");

        // adnan regarding 21 OCT 2014
                
                
        strmResponse.println("function Services(" + Constants.STYPE + ")");
        strmResponse.println("{");
        
//         strmResponse.println("alert('from services method type == ' + sType);");
         
        strmResponse.println("if(sType==\"Error\")");
        strmResponse.println("{");
        strmResponse.println("window.scrollTo(0,0);");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        if (login.getDeptVector().size() > 0 && login.getDepartmentFlag().equals("V")) {
            strmResponse.println("var selDept=document.getElementById(\"selDept\");");
        } else if (login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("F")) {
            strmResponse.println("var txtBDept=document.getElementById(\"txtBDept\");");
        }

        strmResponse.println("var txtBCaller=document.getElementById(\"txtBCaller\");");
        strmResponse.println("var txtBPhone=document.getElementById(\"txtBPhone\");");

        if (login.getRefVector().size() > 0 && login.getRefFlag().equals("V")) {
            strmResponse.println("var selRef=document.getElementById(\"selRef\");");
        } else if (login.getRefFlag().equals("R") || login.getRefFlag().equals("Y")) {
            strmResponse.println("var txtBRef=document.getElementById(\"txtBRef\");");
        }

        strmResponse.println("var txtBHAWBNO=document.getElementById(\"txtBHAWBNO\");");
        strmResponse.println("var txtCCompany=document.getElementById(\"txtCCompany\");");
        strmResponse.println("var txtCContact=document.getElementById(\"txtCContact\");");
        strmResponse.println("var txtCAddress1=document.getElementById(\"txtCAddress1\");");
        strmResponse.println("var txtCAddress2=document.getElementById(\"txtCAddress2\");");
        strmResponse.println("var txtCAddress3=document.getElementById(\"txtCAddress3\");");
        strmResponse.println("var txtCPhone=document.getElementById(\"txtCPhone\");");
        strmResponse.println("var txtCTown=document.getElementById(\"txtCTown\");");
        strmResponse.println("var txtCCounty=document.getElementById(\"txtCCounty\");");
        strmResponse.println("var txtCPostcode=document.getElementById(\"txtCPostcode\");");
        strmResponse.println("var selCCountry=document.getElementById(\"selCCountry\");");
        strmResponse.println("var txtCInstruct=document.getElementById(\"txtCInstruct\");");
        strmResponse.println("var txtCLatitude=document.getElementById(\"txtCLatitude\");");
        strmResponse.println("var txtCLongitude=document.getElementById(\"txtCLongitude\");");
        strmResponse.println("var txtDCompany=document.getElementById(\"txtDCompany\");");
        strmResponse.println("var txtDContact=document.getElementById(\"txtDContact\");");
        strmResponse.println("var txtDAddress1=document.getElementById(\"txtDAddress1\");");
        strmResponse.println("var txtDAddress2=document.getElementById(\"txtDAddress2\");");
        strmResponse.println("var txtDAddress3=document.getElementById(\"txtDAddress3\");");
        strmResponse.println("var txtDPhone=document.getElementById(\"txtDPhone\");");
        strmResponse.println("var txtDTown=document.getElementById(\"txtDTown\");");
        strmResponse.println("var txtDCounty=document.getElementById(\"txtDCounty\");");
        strmResponse.println("var txtDPostcode=document.getElementById(\"txtDPostcode\");");
        strmResponse.println("var selDCountry=document.getElementById(\"selDCountry\");");
        strmResponse.println("var txtDInstruct=document.getElementById(\"txtDInstruct\");");
        strmResponse.println("var txtDLatitude=document.getElementById(\"txtDLatitude\");");
        strmResponse.println("var txtDLongitude=document.getElementById(\"txtDLongitude\");");
        strmResponse.println("var rdCollectNow=document.getElementById(\"rdCollectNow\");");
        strmResponse.println("var rdCollectAdvanced=document.getElementById(\"rdCollectAdvanced\");");
        strmResponse.println("var txtCollectDate=document.getElementById(\"txtCollectDate\");");
        strmResponse.println("var selCollectHH=document.getElementById(\"selCollectHH\");");
        strmResponse.println("var selCollectMM=document.getElementById(\"selCollectMM\");");
        strmResponse.println("var chkPickupBefore=document.getElementById(\"chkPickupBefore\");");

        
//        strmResponse.println("var cmbParcelName = document.getElementById(\"selParcel\").value;");
////        strmResponse.println("alert('Inside services JS method, parcelName =' + cmbParcelName)");
                strmResponse.println("var sItemsRowCount=document.getElementById(\"itemsRowCount\").value;");
        
         strmResponse.println("    document.VerifyForm.itemsRowCount.value = sItemsRowCount;");
        strmResponse.println("    document.BookingForm.itemsRowCount.value = sItemsRowCount;");
        
//        strmResponse.println("alert('Inside services JS method, sItemsRowCount =' + sItemsRowCount)");
//        strmResponse.println("document.VerifyForm.ParcelNameId.value=cmbParcelName;");
//         strmResponse.println("document.BookingForm.ParcelNameId.value=cmbParcelName;");
//         
//         strmResponse.println("document.VerifyForm.ParcelName.value=arrParcelType[cmbParcelName];");
//         strmResponse.println("document.BookingForm.ParcelName.value=arrParcelType[cmbParcelName];");
        // added by Adnan on 27 Aug 2013 against Residential Flag change
        strmResponse.println("var chkCResFlag = document.getElementById(\"chkCResidentialFlag\");");
//         strmResponse.println("alert(chkCResFlag + 'is it checked =='+ chkCResFlag.checked);");
        strmResponse.println("if (chkCResFlag.checked == true){");
//        strmResponse.println("if (document.getElementById(\"chkCResidentialFlag\").checked ){");
        strmResponse.println("var selCResidentialFlag=\"Y\";");
        strmResponse.println("} else {");
        strmResponse.println("var selCResidentialFlag=\"N\";");
        strmResponse.println("}");
//        strmResponse.println("alert('Line# 4657: selCResidentialFlag:'+selCResidentialFlag);");

//        strmResponse.println("var selDResidentialFlag=document.getElementById(\"chkDResidentialFlag\");");
        strmResponse.println("var chkDResFlag = document.getElementById(\"chkDResidentialFlag\");");
        strmResponse.println("if (chkDResFlag.checked == true ){");
        strmResponse.println("var selDResidentialFlag=\"Y\";");
        strmResponse.println("} else {");
        strmResponse.println("var selDResidentialFlag=\"N\";");
        strmResponse.println("}");
//        strmResponse.println("alert('selDResidentialFlag:'+selDResidentialFlag);");

//        strmResponse.println("alert('chkDResFlag.checked');");


        strmResponse.println("var selHHPickupBefore=document.getElementById(\"selHHPickupBefore\");");
        strmResponse.println("var selMMPickupBefore=document.getElementById(\"selMMPickupBefore\");");
        strmResponse.println("var selProductType=document.getElementById(\"selProductType\");");
        strmResponse.println("var txtTotalPieces=document.getElementById(\"txtTotalPieces\");");
        strmResponse.println("var txtTotalWeight=document.getElementById(\"txtTotalWeight\");");
        strmResponse.println("var txtTotalVolWeight=document.getElementById(\"txtTotalVolWeight\");");
        strmResponse.println("var lblMessage=document.getElementById(\"lblMessage\");");
        strmResponse.println("var txtVATCode=document.getElementById(\"txtVATCode\");");
        strmResponse.println("var tdShipper=document.getElementById(\"tdShipper\");");

        if (login.getDeptVector().size() > 0 && login.getDepartmentFlag().equals("V")) {
            strmResponse.println("var sBDept=selDept.value;");
        } else if (login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("F")) {
            strmResponse.println("var sBDept=txtBDept.value;");
        }


        strmResponse.println("var sBCaller=txtBCaller.value;");
        strmResponse.println("var sBPhone=txtBPhone.value;");

        if (login.getRefVector().size() > 0 && login.getRefFlag().equals("V")) {
            strmResponse.println("var sBRef=selRef.value;");
        } else if (login.getRefFlag().equals("R") || login.getRefFlag().equals("Y")) {
            strmResponse.println("var sBRef=txtBRef.value;");
        }


        strmResponse.println("var sBHAWBNO=txtBHAWBNO.value;");

        strmResponse.println("var sCCompany=txtCCompany.value;");
        //strmResponse.println("alert(sCCompany);");
        strmResponse.println("var sCContact=txtCContact.value;");
        strmResponse.println("var sCAddress1=txtCAddress1.value;");
        strmResponse.println("var sCAddress2=txtCAddress2.value;");
        strmResponse.println("var sCAddress3=txtCAddress3.value;");
        strmResponse.println("var sCPhone=txtCPhone.value;");
        strmResponse.println("var sCTown=txtCTown.value;");
        strmResponse.println("var sCCounty=txtCCounty.value;");
        strmResponse.println("var sCPostcode=txtCPostcode.value;");
        //strmResponse.println("alert(sCPostcode);");
        strmResponse.println("var sCCountry=selCCountry.value;");
        strmResponse.println("var sCInstruct=txtCInstruct.value;");
        strmResponse.println("var sCLatitude=txtCLatitude.value;");
        strmResponse.println("var sCLongitude=txtCLongitude.value;");

        // added by Adnan on 27 Aug 2013 against Residential Flag change
        strmResponse.println("var sCResidentialFlag=selCResidentialFlag;");
//        strmResponse.println("alert('Line# 4709: selCResidentialFlag:'+sCResidentialFlag);");

        strmResponse.println("var sDResidentialFlag=selDResidentialFlag;");
//        strmResponse.println("alert('selDResidentialFlag:'+sDResidentialFlag);");

        strmResponse.println("var sDCompany=txtDCompany.value;");
        //strmResponse.println("alert(sCLatitude);");
        strmResponse.println("var sDContact=txtDContact.value;");
        strmResponse.println("var sDAddress1=txtDAddress1.value;");
        strmResponse.println("var sDAddress2=txtDAddress2.value;");
        strmResponse.println("var sDAddress3=txtDAddress3.value;");
        strmResponse.println("var sDPhone=txtDPhone.value;");
        strmResponse.println("var sDTown=txtDTown.value;");
        strmResponse.println("var sDCounty=txtDCounty.value;");
        strmResponse.println("var sDPostcode=txtDPostcode.value;");
        strmResponse.println("var sDCountry=selDCountry.value;");
        strmResponse.println("var sDInstruct=txtDInstruct.value;");
        strmResponse.println("var sDLatitude=txtDLatitude.value;");
        strmResponse.println("var sDLongitude=txtDLongitude.value;");

        strmResponse.println("if(rdCollectAdvanced.checked)");
        strmResponse.println("{");
        strmResponse.println("var dToday=new Date();");
        strmResponse.println("var sCollectDate = txtCollectDate.value;");
        strmResponse.println("var dayfield=sCollectDate.split(\"/\")[0]");
        //strmResponse.println("alert(dayfield);");
        strmResponse.println("var monthfield=sCollectDate.split(\"/\")[1]");
        // strmResponse.println("alert(monthfield);");
        strmResponse.println("var yearfield=sCollectDate.split(\"/\")[2]");
        //strmResponse.println("alert(yearfield);");
//        strmResponse.println("var iMonth = parseInt(monthfield);");
//        strmResponse.println("alert(iMonth);");
//        strmResponse.println("if(iMonth > 7){");
//        strmResponse.println("alert('In greater than 7');");
//        strmResponse.println("var dCollectDate=new Date((parseInt(yearfield)+1), (parseInt(monthfield)+5), parseInt(dayfield), parseInt(selCollectHH.value), parseInt(selCollectMM.value),0);");
//        strmResponse.println("}else{");
        strmResponse.println("var dCollectDate=new Date(parseInt(yearfield,10), (parseInt(monthfield,10)-1), parseInt(dayfield,10), parseInt(selCollectHH.value,10), parseInt(selCollectMM.value,10),0);");
        //strmResponse.println("alert(dToday);");
        //strmResponse.println("alert(dCollectDate);");
        strmResponse.println("}");
        strmResponse.println("var sesCountryAccepted=\"" + (newSession.getAttribute("bCountryAccepted") == null ? "" : (String) newSession.getAttribute("bCountryAccepted")) + "\";");
        strmResponse.println("var bCountryAccepted=document.getElementById(\"bCountryAccepted\");");
        strmResponse.println("if(bCountryAccepted==null)");
        strmResponse.println("   bCountryAccepted = \"\";");
        strmResponse.println("else");
        strmResponse.println("   bCountryAccepted = bCountryAccepted.value;");
        strmResponse.println("var CountryValue=document.getElementById(\"CountryValue\");");
        strmResponse.println("if(CountryValue==null)");
        strmResponse.println("   CountryValue = \"\";");
        strmResponse.println("else");
        strmResponse.println("   CountryValue = CountryValue.value;");
        //strmResponse.println("alert(arrCountryNT.indexOf(sDCountry));");
        if (sTotalValue.length() > 0) {
            double dTotalValue = Double.parseDouble(sTotalValue);

            strmResponse.println("var dTotalValue=" + dTotalValue + ";");
        } else {
            strmResponse.println("var dTotalValue=0;");
        }

//        strmResponse.println("var dIndemnityMax1="+login.getDIndemnityMax1()+";");
//        strmResponse.println("var dIndemnityMax2="+login.getDIndemnityMax2()+";");
        strmResponse.println("var dHighValue1=" + login.getDHighValue1() + ";");
        strmResponse.println("var dHighValue2=" + login.getDHighValue2() + ";");
        //If last row is entered total value should be checked
        strmResponse.println("var dValue=document.getElementById(\"" + sParam_EditCell1 + "2\").value;");
        strmResponse.println("if(dValue.length>0)");
        strmResponse.println("dTotalValue=parseInt(dTotalValue) + parseInt(dValue);");

        strmResponse.println("var gridmessage=\"\";gridmessage=gridRowAdd();");
        strmResponse.println("if(gridmessage!=\"true\")");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = gridmessage;");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = gridmessage;");
        strmResponse.println("window.scrollTo(0,0);");
        strmResponse.println("}");
        strmResponse.println("else if(sDCountry==\"GBR\" && (sCCompany==\"\" || sCAddress1==\"\" || sCPostcode==\"\" || sCTown==\"\" || sDCompany==\"\" || sDAddress1==\"\" || sDPostcode==\"\" || sDTown==\"\" || sDPhone==\"\"))");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = 'Please enter data in the mandatory fields marked(*)';");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = 'Please enter data in the mandatory fields marked(*)';");
        strmResponse.println("window.scrollTo(0,0);");
        strmResponse.println("}");
        strmResponse.println("else if(sCCompany==\"\" || sCAddress1==\"\" || sCPostcode==\"\" || sCTown==\"\" || sDCompany==\"\" || sDAddress1==\"\" || sDTown==\"\" || sDPhone==\"\")");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = 'Please enter data in the mandatory fields marked(*)';");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = 'Please enter data in the mandatory fields marked(*)';");
        strmResponse.println("window.scrollTo(0,0);");
        strmResponse.println("}");

        if (login.getDepartmentFlag().equals("V") || login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("F")) {
            strmResponse.println("else if(sBDept==\"\")");
            strmResponse.println("{");
            strmResponse.println("if(document.all)");
            strmResponse.println("lblMessage.innerText = 'Please enter/select department';");
            strmResponse.println("else");
            strmResponse.println("lblMessage.textContent = 'Please enter/select department';");
            strmResponse.println("window.scrollTo(0,0);");
            strmResponse.println("}");
        }

        if (login.getRefFlag().equals("V") || login.getRefFlag().equals("Y")) {
            strmResponse.println("else if(sBRef==\"\")");
            strmResponse.println("{");
            strmResponse.println("if(document.all)");
            strmResponse.println("lblMessage.innerText = 'Please enter/select reference';");
            strmResponse.println("else");
            strmResponse.println("lblMessage.textContent = 'Please enter/select reference';");
            strmResponse.println("window.scrollTo(0,0);");
            strmResponse.println("}");
        }

        strmResponse.println("else if(rdCollectAdvanced.checked && txtCollectDate.value==\"\")");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = 'Please select ReadyAt date';");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = 'Please select ReadyAt date';");
        strmResponse.println("window.scrollTo(0,0);");
        strmResponse.println("}");

        strmResponse.println("else if(dTotalValue==0 && selProductType.value==\"NDOX\" && (document.VerifyForm.commodityTags.value==\"\"))");
        strmResponse.println("{");

        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = 'Please enter at least one content item';");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = 'Please enter at least one content item';");
        strmResponse.println("window.scrollTo(0,0);");
        strmResponse.println("}");
        strmResponse.println("else if(txtTotalWeight.value==\"\" && selProductType.value==\"NDOX\")");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("{");
        strmResponse.println("if (itemWeightCount < 1) ");
        strmResponse.println("lblMessage.innerText = 'Please enter Total Weight';");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("if (itemWeightCount < 1) {");
        strmResponse.println("lblMessage.textContent = 'Please enter Total Weight';");
        strmResponse.println("window.scrollTo(0,0);");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("else if(isNaN(txtTotalWeight.value) && selProductType.value==\"NDOX\")");
        strmResponse.println("{");
        strmResponse.println("if(document.all){");
        strmResponse.println("if (itemWeightCount < 1) ");
        strmResponse.println("lblMessage.innerText = 'Invalid Total Weight. Please correct';");
        strmResponse.println("}else{");
        strmResponse.println("if (itemWeightCount < 1){ ");
        strmResponse.println("lblMessage.textContent = 'Invalid Total Weight. Please correct';");
        strmResponse.println("window.scrollTo(0,0);");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("else if(parseFloat(txtTotalWeight.value)==0 && selProductType.value==\"NDOX\")");
        strmResponse.println("{");
        strmResponse.println("if (itemWeightCount < 1){ ");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = 'Invalid Total Weight. Please correct';");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = 'Invalid Total Weight. Please correct';");
        strmResponse.println("window.scrollTo(0,0);");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("else if((txtTotalPieces.value==\"\" || parseInt(txtTotalPieces.value)==0) && selProductType.value==\"NDOX\" && (document.VerifyForm.itemTags.value==\"\"))");
//        strmResponse.println("else if((txtTotalPieces.value==\"\" || parseInt(txtTotalPieces.value)==0) && (document.VerifyForm.itemTags.value==\"\"))");
//        strmResponse.println("else if((sItemsRowCount==\"\"|| isNaN(sItemsRowCount) || parseInt(sItemsRowCount)==0) && (document.VerifyForm.itemTags.value==\"\")) ");
        
        
        strmResponse.println("{");       
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = 'Please enter at least one No.of.Items';");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = 'Please enter at least one No.of.Items';");
        
        strmResponse.println("window.scrollTo(0,0);");
        strmResponse.println("}");
        strmResponse.println("else if((isNaN(txtTotalPieces.value) || parseInt(txtTotalPieces.value)==0 || txtTotalPieces.value==\"\") && selProductType.value==\"DOX\")");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = 'Please enter Total No Of Pieces';");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = 'Please enter Total No Of Pieces';");
        strmResponse.println("window.scrollTo(0,0);");
        strmResponse.println("}");
        strmResponse.println("else if(bCountryAccepted==\"\" && arrCountryNT.indexOf(sDCountry)>=0 && sesCountryAccepted.length==0)");
        strmResponse.println("{");

        strmResponse.println("notesClick();");
        strmResponse.println("}");
        strmResponse.println("else if(CountryValue==\"\" && sesCountryAccepted.length==0 && arrCountryNT.indexOf(sDCountry)>=0)");
        strmResponse.println("{");
        strmResponse.println("notesClick();");
        strmResponse.println("}");
        strmResponse.println("else if(CountryValue!=\"\" && CountryValue!=sDCountry && arrCountryNT.indexOf(sDCountry)>=0)");
        strmResponse.println("{");

        //strmResponse.println("alert(document.getElementById(\"CountryValue\").value);");
        //strmResponse.println("alert(sDCountry);");
        strmResponse.println("notesClick();");
        strmResponse.println("}");
        strmResponse.println("else if(CountryValue==\"\" && sesCountryAccepted.length!=0 && sesCountryAccepted!=sDCountry && arrCountryNT.indexOf(sDCountry)>=0)");
        strmResponse.println("{");

        //strmResponse.println("alert(document.getElementById(\"CountryValue\").value);");
        //strmResponse.println("alert(sDCountry);");
        strmResponse.println("notesClick();");
        strmResponse.println("}");


//        if(sTotalValue.length()>0)
//        {
        strmResponse.println("else if(sDCountry=='" + Constants.COUNTRY_CODE + "' && dTotalValue>dHighValue1)");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = 'Sorry your booking cannot be processed as the product value has exceeded the maximum allowed. Please contact your local CitySprint ServiceCentre for more information';");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = 'Sorry your booking cannot be processed as the product value has exceeded the maximum allowed. Please contact your local CitySprint ServiceCentre for more information';");

        strmResponse.println("window.scrollTo(0,0);");
        strmResponse.println("}");
        strmResponse.println("else if(sDCountry!='" + Constants.COUNTRY_CODE + "' && dTotalValue>dHighValue2)");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = 'Sorry your booking cannot be processed as the product value has exceeded the maximum allowed. Please contact your local CitySprint ServiceCentre for more information';");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = 'Sorry your booking cannot be processed as the product value has exceeded the maximum allowed. Please contact your local CitySprint ServiceCentre for more information';");
        strmResponse.println("window.scrollTo(0,0);");
        strmResponse.println("}");
        strmResponse.println("else if(rdCollectAdvanced.checked && dCollectDate < dToday)");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = 'Ready At Date/Time is prior to Current Date/Time. Please correct.';");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = 'Ready At Date/Time is prior to Current Date/Time. Please correct.';");
        strmResponse.println("window.scrollTo(0,0);");
        strmResponse.println("}");

//            strmResponse.println("else if(sDCountry=='"+Constants.COUNTRY_CODE+"' && dTotalValue>dHighValue1 && txtVATCode.value==\"\")");
//            strmResponse.println("{");
////            strmResponse.println("alert('inside if:'+txtVATCode.value);");
////            strmResponse.println("if()");
////            strmResponse.println("{");
//            strmResponse.println("tdShipper.style.display='';");
////            strmResponse.println("alert('inside if');");
//            strmResponse.println("if(document.all)");
//            strmResponse.println("lblMessage.innerText = 'Please enter Shipper VAT code';");
//            strmResponse.println("else");
//            strmResponse.println("lblMessage.textContent = 'Please enter Shipper VAT code';");
//            strmResponse.println("window.scrollTo(0,0);");
////            strmResponse.println("}");
//            strmResponse.println("}");
//            strmResponse.println("else if(sDCountry!='"+Constants.COUNTRY_CODE+"' && dTotalValue>dHighValue2 && txtVATCode.value==\"\")");
//            strmResponse.println("{");
////            strmResponse.println("alert('inside if');");
////            strmResponse.println("if()");
////            strmResponse.println("{");
//            strmResponse.println("tdShipper.style.display='';");
//            strmResponse.println("if(document.all)");
//            strmResponse.println("lblMessage.innerText = 'Please enter Shipper VAT code';");
//            strmResponse.println("else");
//            strmResponse.println("lblMessage.textContent = 'Please enter Shipper VAT code';");
//            strmResponse.println("window.scrollTo(0,0);");
////            strmResponse.println("}");
//            strmResponse.println("}");
//        }


        strmResponse.println("else");
        strmResponse.println("{");
        //If collection and Delivery addresses validated then proceed to Services page else call QAS validation.
        strmResponse.println("if(" + Constants.STYPE + "==\"" + SearchResult.Verified + "\")");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText=\"\";");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = \"\";");
        //strmResponse.println("alert('inside if');");
        strmResponse.println("document.BookingForm.CCompanyName.value=sCCompany;");
        strmResponse.println("document.BookingForm.CContactName.value=sCContact;");
        strmResponse.println("document.BookingForm.CAddress1.value=sCAddress1;");
        strmResponse.println("document.BookingForm.CAddress2.value=sCAddress2;");
        strmResponse.println("document.BookingForm.CAddress3.value=sCAddress3;");
        strmResponse.println("document.BookingForm.CPhone.value=sCPhone;");
        strmResponse.println("document.BookingForm.CTown.value=sCTown;");
        strmResponse.println("document.BookingForm.CCounty.value=sCCounty;");
        strmResponse.println("document.BookingForm.CPostcode.value=sCPostcode;");
        strmResponse.println("document.BookingForm.CLatitude.value=sCLatitude;");
        strmResponse.println("document.BookingForm.CLongitude.value=sCLongitude;");
        //strmResponse.println("alert('country'+sCCountry);");

        // added by Adnan on 27 Aug 2013 against Residential Flag change
        strmResponse.println("document.BookingForm.CResidentialFlag.value=sCResidentialFlag;");
//        strmResponse.println("alert('Line# 4992: selCResidentialFlag:'+sCResidentialFlag);");

        strmResponse.println("document.BookingForm.DResidentialFlag.value=sDResidentialFlag;");
//        strmResponse.println("alert('selDResidentialFlag:'+sDResidentialFlag);");

        strmResponse.println("document.BookingForm.CCountry.value=sCCountry;");
        strmResponse.println("if(sCInstruct==\"E.g. knock very loudly on blue door\")");
        strmResponse.println("  document.BookingForm.CInstruct.value=\"\";");
        strmResponse.println("else");
        strmResponse.println("  document.BookingForm.CInstruct.value=sCInstruct;");
        strmResponse.println("document.BookingForm.DCompanyName.value=sDCompany;");
        strmResponse.println("document.BookingForm.DContactName.value=sDContact;");
        strmResponse.println("document.BookingForm.DAddress1.value=sDAddress1;");
        strmResponse.println("document.BookingForm.DAddress2.value=sDAddress2;");
        strmResponse.println("document.BookingForm.DAddress3.value=sDAddress3;");
        strmResponse.println("document.BookingForm.DPhone.value=sDPhone;");
        strmResponse.println("document.BookingForm.DTown.value=sDTown;");
        strmResponse.println("document.BookingForm.DCounty.value=sDCounty;");
        strmResponse.println("document.BookingForm.DPostcode.value=sDPostcode;");
        strmResponse.println("document.BookingForm.DCountry.value=sDCountry;");
        strmResponse.println("if(sDInstruct==\"E.g. knock very loudly on blue door\")");
        strmResponse.println("  document.BookingForm.DInstruct.value=\"\";");
        strmResponse.println("else");
        strmResponse.println("  document.BookingForm.DInstruct.value=sDInstruct;");
        strmResponse.println("document.BookingForm.DLatitude.value=sDLatitude;");
        strmResponse.println("document.BookingForm.DLongitude.value=sDLongitude;");
        // strmResponse.println("alert(sBDept);");
        if (login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("V") || login.getDepartmentFlag().equals("F")) {
            strmResponse.println("document.BookingForm.BDept.value=sBDept;");
        }
        //strmResponse.println("alert(document.BookingForm.BDept.value);");
        strmResponse.println("document.BookingForm.BCaller.value=sBCaller;");
        strmResponse.println("document.BookingForm.BPhone.value=sBPhone;");

        if (login.getRefFlag().equals("R") || login.getRefFlag().equals("Y") || login.getRefFlag().equals("V")) {
            strmResponse.println("document.BookingForm.BRef.value=sBRef;");
        }
        strmResponse.println("document.BookingForm.BHAWBNO.value=sBHAWBNO;");
        strmResponse.println("document.BookingForm." + Constants.SACTION + ".value=\"Booking\";");
        strmResponse.println("if(rdCollectAdvanced.checked)");
        strmResponse.println("document.BookingForm.rdCollect.value=rdCollectAdvanced.value;");
        strmResponse.println("if(rdCollectNow.checked)");
        strmResponse.println("document.BookingForm.rdCollect.value=rdCollectNow.value;");
        strmResponse.println("document.BookingForm.CollectDate.value=txtCollectDate.value;");
        strmResponse.println("document.BookingForm.CollectHH.value=selCollectHH.value;");
        strmResponse.println("document.BookingForm.CollectMM.value=selCollectMM.value;");
        strmResponse.println("if(chkPickupBefore.checked)");
        strmResponse.println("document.BookingForm.chkPickupBefore.value='1';");
        strmResponse.println("else");
        strmResponse.println("document.BookingForm.chkPickupBefore.value='0';");

        strmResponse.println("document.BookingForm.PickupBeforeHH.value=selHHPickupBefore.value;");
        strmResponse.println("document.BookingForm.PickupBeforeMM.value=selMMPickupBefore.value;");
        strmResponse.println("document.BookingForm.ProductType.value=selProductType.value;");
        strmResponse.println("document.BookingForm.TotalPieces.value=txtTotalPieces.value;");
        strmResponse.println("document.BookingForm.TotalWeight.value=txtTotalWeight.value;");
        strmResponse.println("document.BookingForm.TotalVolWeight.value=txtTotalVolWeight.value;");
        strmResponse.println("document.BookingForm.ShipperVAT.value=txtVATCode.value;");
        strmResponse.println("document.BookingForm.submit();");
        strmResponse.println("}");
        strmResponse.println("else if(sType==\"QAS\")");
        strmResponse.println("{");
        
//                strmResponse.println("var sSelectedParcel=document.getElementById(\"selParcel\").value;");
//strmResponse.println("        document.VerifyForm.ParcelNameId.value=sSelectedParcel;");
//strmResponse.println("        document.VerifyForm.ParcelName.value=arrParcelType[sSelectedParcel];");

//strmResponse.println("alert ('Services method line# 5425,  document.VerifyForm.ParcelName.value == ' +  document.VerifyForm.ParcelName.value);");

 strmResponse.println("var sItemsRowCount=document.getElementById(\"itemsRowCount\").value;");
         strmResponse.println("    document.VerifyForm.itemsRowCount.value = sItemsRowCount;");


        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText=\"\";");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = \"\";");
        strmResponse.println("	 sWinOpen = 1;");
        strmResponse.println("if((winqas == null) || (winqas.closed == true))");
        strmResponse.println("{");
        strmResponse.println("	 winqas=window.open('blank.html','QASValidation','resizable=yes,scrollbars=yes,width=700,height=500');");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("   winqas.focus();");
        strmResponse.println("}");
        strmResponse.println("document.VerifyForm." + Constants.USER_INPUT + ".value=sCCompany +\"^\" + sCAddress1+\"^\"+sCAddress2+\"^\"+sCTown+ \"^\"+sCCounty+\"^\"+sCPostcode;");
        strmResponse.println("document.VerifyForm." + Constants.USER_INPUT1 + ".value=sDCompany +\"^\" + sDAddress1+\"^\"+sDAddress2+\"^\"+sDTown+ \"^\"+sDCounty+\"^\"+sDPostcode;");
        strmResponse.println("document.VerifyForm.CCompanyName.value=sCCompany;");
        strmResponse.println("document.VerifyForm.CContactName.value=sCContact;");
        strmResponse.println("document.VerifyForm.CAddress1.value=sCAddress1;");
        strmResponse.println("document.VerifyForm.CAddress2.value=sCAddress2;");
        strmResponse.println("document.VerifyForm.CAddress3.value=sCAddress3;");
        strmResponse.println("document.VerifyForm.CPhone.value=sCPhone;");
        strmResponse.println("document.VerifyForm.CTown.value=sCTown;");
        strmResponse.println("document.VerifyForm.CCounty.value=sCCounty;");
        strmResponse.println("document.VerifyForm.CPostcode.value=sCPostcode;");
        strmResponse.println("document.VerifyForm.CCountry.value=sCCountry;");
        strmResponse.println("document.VerifyForm.CInstruct.value=sCInstruct;");
        strmResponse.println("document.VerifyForm.CLatitude.value=sCLatitude;");
        strmResponse.println("document.VerifyForm.CLongitude.value=sCLongitude;");
        strmResponse.println("document.VerifyForm.DCompanyName.value=sDCompany;");
        strmResponse.println("document.VerifyForm.DContactName.value=sDContact;");
        strmResponse.println("document.VerifyForm.DAddress1.value=sDAddress1;");
        strmResponse.println("document.VerifyForm.DAddress2.value=sDAddress2;");
        strmResponse.println("document.VerifyForm.DAddress3.value=sDAddress3;");
        strmResponse.println("document.VerifyForm.DPhone.value=sDPhone;");
        strmResponse.println("document.VerifyForm.DTown.value=sDTown;");
        strmResponse.println("document.VerifyForm.DCounty.value=sDCounty;");
        strmResponse.println("document.VerifyForm.DPostcode.value=sDPostcode;");
        strmResponse.println("document.VerifyForm.DCountry.value=sDCountry;");
        strmResponse.println("document.VerifyForm.DInstruct.value=sDInstruct;");
        strmResponse.println("document.VerifyForm.DLatitude.value=sDLatitude;");
        strmResponse.println("document.VerifyForm.DLongitude.value=sDLongitude;");
        
        strmResponse.println("document.VerifyForm.itemWeightCount.value=itemWeightCount;");

        // added by Adnan on 27 Aug 2013 against Residential Flag change
        strmResponse.println("document.VerifyForm.CResidentialFlag.value=sCResidentialFlag;");
//        strmResponse.println("alert('Line# 5093: selCResidentialFlag:'+sCResidentialFlag);");

        strmResponse.println("document.VerifyForm.DResidentialFlag.value=sDResidentialFlag;");
//        strmResponse.println("alert('selDResidentialFlag:'+sDResidentialFlag);");


        if (login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("V") || login.getDepartmentFlag().equals("F")) {
            strmResponse.println("document.VerifyForm.BDept.value=sBDept;");
        }
        //strmResponse.println("alert(sBDept);");
        strmResponse.println("document.VerifyForm.BCaller.value=sBCaller;");
        strmResponse.println("document.VerifyForm.BPhone.value=sBPhone;");
        if (login.getRefFlag().equals("R") || login.getRefFlag().equals("Y") || login.getRefFlag().equals("V")) {
            strmResponse.println("document.VerifyForm.BRef.value=sBRef;");
        }
        strmResponse.println("document.VerifyForm.BHAWBNO.value=sBHAWBNO;");
        strmResponse.println("if(rdCollectAdvanced.checked)");
        strmResponse.println("document.VerifyForm.rdCollect.value=rdCollectAdvanced.value;");
        strmResponse.println("if(rdCollectNow.checked)");
        strmResponse.println("document.VerifyForm.rdCollect.value=rdCollectNow.value;");
        strmResponse.println("document.VerifyForm.CollectDate.value=txtCollectDate.value;");
        strmResponse.println("document.VerifyForm.CollectHH.value=selCollectHH.value;");
        strmResponse.println("document.VerifyForm.CollectMM.value=selCollectMM.value;");
        strmResponse.println("if(chkPickupBefore.checked)");
        strmResponse.println("document.VerifyForm.chkPickupBefore.value='1';");
        strmResponse.println("else");
        strmResponse.println("document.VerifyForm.chkPickupBefore.value='0';");

        strmResponse.println("document.VerifyForm.PickupBeforeHH.value=selHHPickupBefore.value;");
        strmResponse.println("document.VerifyForm.PickupBeforeMM.value=selMMPickupBefore.value;");
//        strmResponse.println("if(rdCollectAdvanced.checked)");
//        strmResponse.println("{");
//        strmResponse.println("document.VerifyForm.rdAdvanced.value='Y';");
//        strmResponse.println("document.VerifyForm.PickupDate.value=txtCollectDate.value;");
//        strmResponse.println("document.VerifyForm.PickupTime.value=selCollectHH.value + \":\" + selCollectMM.value;");
//        strmResponse.println("}");
//        strmResponse.println("else");
//        strmResponse.println("{");
//        strmResponse.pri461ntln("document.VerifyForm.rdAdvanced.value='N';");
//        strmResponse.println("var d = new Date();");
//        strmResponse.println("document.VerifyForm.PickupDate.value=d.getDate();");
//        strmResponse.println("document.VerifyForm.PickupTime.value=d.getHours() + \":\" + d.getMinutes();");
//        strmResponse.println("}");
//        strmResponse.println("var sSelectedParcel=document.getElementById(\"selParcel\").value;");
//strmResponse.println("        document.VerifyForm.ParcelNameId.value=sSelectedParcel;");
//strmResponse.println("        document.frmGridServlet.ParcelName.value=arrParcelType[sSelectedParcel];");

//strmResponse.println("alert ('Services method line# 5513,  document.VerifyForm.ParcelName.value == ' +  document.VerifyForm.ParcelName.value);");

 strmResponse.println("var sItemsRowCount=document.getElementById(\"itemsRowCount\").value;");
         strmResponse.println("    document.VerifyForm.itemsRowCount.value = sItemsRowCount;");
                
        strmResponse.println("document.VerifyForm.ProductType.value=selProductType.value;");
        strmResponse.println("document.VerifyForm.TotalPieces.value=txtTotalPieces.value;");
        strmResponse.println("document.VerifyForm.TotalWeight.value=txtTotalWeight.value;");
        strmResponse.println("document.VerifyForm.TotalVolWeight.value=txtTotalVolWeight.value;");
        strmResponse.println("document.VerifyForm.ShipperVAT.value=txtVATCode.value;");
        strmResponse.println("document.VerifyForm.submit();");
        strmResponse.println("}");
        // strmResponse.println("else
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("}");

  // adnan regarding 21 OCT 2014  
        
        strmResponse.println("function indemnityClick(sIndemnity)");
        strmResponse.println("{");
        strmResponse.println("var bIndemnity=document.getElementById(\"bIndemnity\");");
        strmResponse.println("var selIndemnity=document.getElementById(sIndemnity);");
        strmResponse.println("var sDCountry=document.getElementById(\"selDCountry\").value;");
        strmResponse.println("if(selIndemnity.value==\"YES\")");
        strmResponse.println("{");
        if (sTotalValueWithIndemnity.length() > 0) {
            double dTotalValueWithIndemnity = Double.parseDouble(sTotalValueWithIndemnity);

            strmResponse.println("var dTotalValueWithIndemnity=" + dTotalValueWithIndemnity + ";");
        } else {
            strmResponse.println("var dTotalValueWithIndemnity=0;");
        }

        strmResponse.println("var dIndemnityMax1=" + login.getDIndemnityMax1() + ";");
        strmResponse.println("var dIndemnityMax2=" + login.getDIndemnityMax2() + ";");
        strmResponse.println("var dValue=document.getElementById(\"" + sParam_EditCell1 + "2\").value;");
        strmResponse.println("if(dValue.length>0)");
        strmResponse.println("dTotalValueWithIndemnity=parseInt(dTotalValueWithIndemnity) + parseInt(dValue);");

        strmResponse.println("if(sDCountry=='" + Constants.COUNTRY_CODE + "' && dTotalValueWithIndemnity>dIndemnityMax1)");
        strmResponse.println("{");
        strmResponse.println("selIndemnity.value=\"NO\";");
        strmResponse.println("alert('Indemnity cannot be applied for this commodity as maximum indemnity value would be exceeded');");
        strmResponse.println("}");
        strmResponse.println("else if(sDCountry!='" + Constants.COUNTRY_CODE + "' && dTotalValueWithIndemnity>dIndemnityMax2)");
        strmResponse.println("{");
        strmResponse.println("selIndemnity.value=\"NO\";");
        strmResponse.println("alert('Indemnity cannot be applied for this commodity as maximum indemnity value would be exceeded');");
        strmResponse.println("}");
        strmResponse.println("else if (bIndemnity.value == \"true\" && selIndemnity.value==\"YES\")");
        strmResponse.println("{");
        strmResponse.println("selIndemnity.value=\"SELECT\";");
        strmResponse.println("if((winindemnity == null) || (winindemnity.closed == true))");
        strmResponse.println("  winindemnity=window.open('Indemnity.html','Calendar','width=450,height=130,toolbar=no,resizable=no');");
        strmResponse.println("else");
        strmResponse.println("  winindemnity.focus();");

        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("}");

        //Clearing Address fields
        strmResponse.println("function clearAddress(sType)");
        strmResponse.println("{");
        strmResponse.println("var lblMessage=document.getElementById(\"lblMessage\");");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText=\"\";");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = \"\";");
        strmResponse.println("if(sType==\"C\")");
        strmResponse.println("var tdAddress=document.getElementById(\"tdCollection\");");
        strmResponse.println("else");
        strmResponse.println("var tdAddress=document.getElementById(\"tdDelivery\");");
        //Identify all the input types first
        strmResponse.println("var CInputAddress=tdAddress.getElementsByTagName('input');");
        //Clear all the input fields in the Collection Address section.
        strmResponse.println("for (var i=0,thisInput; thisInput=CInputAddress[i]; i++) {");
        strmResponse.println("if(thisInput.type==\"text\")");
        strmResponse.println("thisInput.value='';");
        strmResponse.println("}");
        strmResponse.println("var selDCountry=document.getElementById(\"selDCountry\");");
        strmResponse.println("selDCountry.value=\"" + Constants.COUNTRY_CODE + "\";");
        strmResponse.println("if(sType==\"C\")");
        strmResponse.println("{");
        strmResponse.println("var txtCLatitude=document.getElementById(\"txtCLatitude\");");
        strmResponse.println("var txtCLongitude=document.getElementById(\"txtCLongitude\");");
        strmResponse.println("txtCLatitude.value=\"\";");
        strmResponse.println("txtCLongitude.value=\"\";");
        strmResponse.println("document.getElementById(\"txtCTown\").disabled=false;");
        strmResponse.println("document.getElementById(\"txtCPostcode\").disabled=false;");
        strmResponse.println("document.getElementById(\"selCCountry\").disabled=false;");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("var txtDLatitude=document.getElementById(\"txtDLatitude\");");
        strmResponse.println("var txtDLongitude=document.getElementById(\"txtDLongitude\");");
        strmResponse.println("txtDLatitude.value=\"\";");
        strmResponse.println("txtDLongitude.value=\"\";");
        strmResponse.println("document.getElementById(\"txtDTown\").disabled=false;");
        strmResponse.println("document.getElementById(\"txtDPostcode\").disabled=false;");
        strmResponse.println("document.getElementById(\"selDCountry\").disabled=false;");
        strmResponse.println("}");
        strmResponse.println("}");

        //Editing Address fields
        strmResponse.println("function editAddress(sType)");
        strmResponse.println("{");
        strmResponse.println("if(sType==\"C\")");
        strmResponse.println("{");
        strmResponse.println("var txtCLatitude=document.getElementById(\"txtCLatitude\");");
        strmResponse.println("var txtCLongitude=document.getElementById(\"txtCLongitude\");");
        strmResponse.println("txtCLatitude.value=\"\";");
        strmResponse.println("txtCLongitude.value=\"\";");
        strmResponse.println("document.getElementById(\"txtCTown\").disabled=false;");
        strmResponse.println("document.getElementById(\"txtCPostcode\").disabled=false;");
        strmResponse.println("document.getElementById(\"selCCountry\").disabled=false;");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("var txtDLatitude=document.getElementById(\"txtDLatitude\");");
        strmResponse.println("var txtDLongitude=document.getElementById(\"txtDLongitude\");");
        strmResponse.println("txtDLatitude.value=\"\";");
        strmResponse.println("txtDLongitude.value=\"\";");
        strmResponse.println("document.getElementById(\"txtDTown\").disabled=false;");
        strmResponse.println("document.getElementById(\"txtDPostcode\").disabled=false;");
        strmResponse.println("document.getElementById(\"selDCountry\").disabled=false;");
        strmResponse.println("}");
        strmResponse.println("}");

        //disable the booking entry section once services are displayed
        strmResponse.println("function disableBooking()");
        strmResponse.println("{");
        strmResponse.println("var tblBooking=document.getElementById(\"tblBooking\");");
        //strmResponse.println("alert(tblBooking);");

        //Identify all the input types first
        strmResponse.println("var CInputBooking=tblBooking.getElementsByTagName('input');");

        //Identify all the select controls
        strmResponse.println("var CSelectBooking=tblBooking.getElementsByTagName('select');");

        //Identify all the img buttons
        strmResponse.println("var CImgBooking=tblBooking.getElementsByTagName('img');");

        //Disable all the input fields in the booking section.
        strmResponse.println("for (var i=0,thisInput; thisInput=CInputBooking[i]; i++) {");
        strmResponse.println("thisInput.disabled=true;");
        strmResponse.println("}");

        //Disable all the select controls in the booking section.
        strmResponse.println("for (var i=0,thisInput; thisInput=CSelectBooking[i]; i++) {");
        strmResponse.println("thisInput.disabled=true;");
        strmResponse.println("}");

        //Disable all the img controls in the booking section.
        strmResponse.println("for (var i=0,thisInput; thisInput=CImgBooking[i]; i++) {");
        strmResponse.println("thisInput.disabled=true;");
        strmResponse.println("}");

        strmResponse.println("var lblMessage1=document.getElementById(\"lblMessage1\");");
        // strmResponse.println("alert(lblMessage1);");
        strmResponse.println("lblMessage1.focus();");

        strmResponse.println("var btnNext=document.getElementById(\"btnNext\");");
        strmResponse.println("btnNext.visible=false;");
        strmResponse.println("btnNext.style.display='none';");
        strmResponse.println("window.scroll(0,screen.height);");
        //strmResponse.println("document.getElementById(\"rdServicesValid\").focus();");
        strmResponse.println("}");

        //enable the booking entry section once user hits on Amend Details
        strmResponse.println("function enableBooking()");
        strmResponse.println("{");
        //strmResponse.println("alert('inside enablebooking');");
        strmResponse.println("var tblBooking=document.getElementById(\"tblBooking\");");

		strmResponse.println("var sItemsRowCount=document.getElementById(\"itemsRowCount\").value ;");
strmResponse.println("sItemsRowCount = parseInt(sItemsRowCount);");
 

        // strmResponse.println("alert(tblBooking);");

        //Identify all the input types first
        strmResponse.println("var CInputBooking=tblBooking.getElementsByTagName('input');");

        //Identify all the select controls
        strmResponse.println("var CSelectBooking=tblBooking.getElementsByTagName('select');");

        //Identify all the img buttons
        strmResponse.println("var CImgBooking=tblBooking.getElementsByTagName('img');");

        //Enable all the input fields in the booking section.
        strmResponse.println("for (var i=0,thisInput; thisInput=CInputBooking[i]; i++) {");
        strmResponse.println("thisInput.disabled=false;");
        strmResponse.println("}");

        //Enable all the select controls in the booking section.
        strmResponse.println("for (var i=0,thisInput; thisInput=CSelectBooking[i]; i++) {");
        strmResponse.println("thisInput.disabled=false;");
        strmResponse.println("}");

        
//        strmResponse.println("if (sItemsRowCount > 0 ){");
////strmResponse.println("alert('inside enablebooking, sItemsRowCount == ' + sItemsRowCount);");
//strmResponse.println("document.getElementById(\"selParcel\").disabled = true;");
//strmResponse.println("}");


        //Enable all the img controls in the booking section.
        strmResponse.println("for (var i=0,thisInput; thisInput=CImgBooking[i]; i++) {");
        strmResponse.println("thisInput.disabled=false;");
        strmResponse.println("}");

        //Hide the services table
        strmResponse.println("var tblServices=document.getElementById(\"tblServices\");");
        strmResponse.println("tblServices.style.display='none';");

        strmResponse.println("var btnNext=document.getElementById(\"btnNext\");");
        strmResponse.println("btnNext.visible=true;");
        strmResponse.println("btnNext.style.display='';");

        strmResponse.println("var txtCLatitude=document.getElementById(\"txtCLatitude\");");
        strmResponse.println("var txtCLongitude=document.getElementById(\"txtCLongitude\");");

        strmResponse.println("if(txtCLatitude.value==\"\" && txtCLongitude.value==\"\")");
        strmResponse.println("{}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("document.getElementById(\"txtCTown\").disabled=true;");
        strmResponse.println("document.getElementById(\"txtCPostcode\").disabled=true;");
        strmResponse.println("document.getElementById(\"selCCountry\").disabled=true;");
        strmResponse.println("}");

        strmResponse.println("var txtDLatitude=document.getElementById(\"txtDLatitude\");");
        strmResponse.println("var txtDLongitude=document.getElementById(\"txtDLongitude\");");

        strmResponse.println("if(txtDLatitude.value==\"\" && txtDLongitude.value==\"\")");
        strmResponse.println("{}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("document.getElementById(\"txtDTown\").disabled=true;");
        strmResponse.println("document.getElementById(\"txtDPostcode\").disabled=true;");
        strmResponse.println("document.getElementById(\"selDCountry\").disabled=true;");
        strmResponse.println("}");

        strmResponse.println("}");

        //enable date fields if advanced booking option is selected
        strmResponse.println("function enableDateFields()");
        strmResponse.println("{");
        strmResponse.println("var f_trigger_c=document.getElementById(\"f_trigger_c\");");
        strmResponse.println("var selCollectHH=document.getElementById(\"selCollectHH\");");
        strmResponse.println("var selCollectMM=document.getElementById(\"selCollectMM\");");
        //  strmResponse.println("alert('after declaration');");
        strmResponse.println("f_trigger_c.disabled=false;");
        strmResponse.println("selCollectHH.disabled=false;");
        strmResponse.println("selCollectMM.disabled=false;");
        strmResponse.println("CurrentDate=new Date();");
        strmResponse.println("document.getElementById(\"txtCollectDate\").value=(CurrentDate.getDate() + \"/\" + (CurrentDate.getMonth()+1) + \"/\" + CurrentDate.getFullYear())");
        strmResponse.println("}");

        //disable date fields if now option is selected
        strmResponse.println("function disableDateFields()");
        strmResponse.println("{");
        strmResponse.println("var f_trigger_c=document.getElementById(\"f_trigger_c\");");
        strmResponse.println("var selCollectHH=document.getElementById(\"selCollectHH\");");
        strmResponse.println("var selCollectMM=document.getElementById(\"selCollectMM\");");
        //  strmResponse.println("alert('after declaration');");
        strmResponse.println("f_trigger_c.disabled=true;");
        strmResponse.println("selCollectHH.disabled=true;");
        strmResponse.println("selCollectMM.disabled=true;");
        strmResponse.println("}");

        //product type dropdown on change event
        strmResponse.println("function productTypeChange(sValue)");
        strmResponse.println("{");
        //strmResponse.println("alert('inside fn');");
        strmResponse.println("var tblItems=document.getElementById(\"tblItems\");");
        strmResponse.println("var tblCommodity=document.getElementById(\"tblCommodity\");");
        strmResponse.println("var txtTotalPieces=document.getElementById(\"txtTotalPieces\");");
        strmResponse.println("var txtTotalWeight=document.getElementById(\"txtTotalWeight\");");
        strmResponse.println("var txtTotalVolWeight=document.getElementById(\"txtTotalVolWeight\");");
        strmResponse.println("var imgAdd=document.getElementById(\"imgAdd\");");
        strmResponse.println("var imgAdd1=document.getElementById(\"imgAdd1\");");
        //strmResponse.println("alert(sValue);");
        strmResponse.println("if(sValue==\"DOX\")");
        strmResponse.println("{");
        strmResponse.println("tblItems.style.display='none'");
//        strmResponse.println("tblItems.style.display='';");
        strmResponse.println("tblCommodity.style.display='none'");
        strmResponse.println("txtTotalPieces.disabled=false;");
        strmResponse.println("txtTotalPieces.setAttribute(\"class\",\"enabledstyle\");");
        strmResponse.println("txtTotalPieces.setAttribute(\"className\",\"enabledstyle\");");
//        strmResponse.println("txtTotalWeight.setAttribute(\"class\",\"txtnormal\");");
//        strmResponse.println("txtTotalWeight.setAttribute(\"className\",\"txtnormal\");");
        //strmResponse.println("txtTotalWeight.value=='';");
        strmResponse.println("txtTotalVolWeight.value='0';");
        strmResponse.println("document.getElementById(\"lblTotWeight\").innerHTML = \"Total Weight (kg)\";");
        //  strmResponse.println("document.getElementById(\"lblTotWeight\").style.color='#666666';");
        //strmResponse.println("txtTotalPieces.value='0';");
        // strmResponse.println("txtTotalWeight.disabled=true;");
        // strmResponse.println("imgAdd.disabled=true;");
        // strmResponse.println("imgAdd1.disabled=true;");
        
        strmResponse.println("txtTotalWeight.disabled=false;");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("tblItems.style.display='';");
        strmResponse.println("tblCommodity.style.display='';");
        strmResponse.println("txtTotalPieces.value=document.frmGridServlet.sNTotalPieces.value;");
        //strmResponse.println("txtTotalWeight.value=='';");
        strmResponse.println("txtTotalPieces.disabled=true;");
        strmResponse.println("txtTotalPieces.setAttribute(\"class\",\"disabledstyle\");");
        strmResponse.println("txtTotalPieces.setAttribute(\"className\",\"disabledstyle\");");
//        strmResponse.println("txtTotalWeight.setAttribute(\"class\",\"txt\");");
//        strmResponse.println("txtTotalWeight.setAttribute(\"className\",\"txt\");");
        strmResponse.println("txtTotalVolWeight.value=document.frmGridServlet.sNTotalVolWeight.value;");
        strmResponse.println("document.getElementById(\"lblTotWeight\").innerHTML = \"Total Weight (kg) *\";");
        // strmResponse.println("document.getElementById(\"lblTotWeight\").style.color='#CC0000';");

        // strmResponse.println("imgAdd.disabled=false;");
        //  strmResponse.println("imgAdd1.disabled=false;");
        
         strmResponse.println(" if (itemWeightCount > 0) { ") ;
        strmResponse.println("txtTotalWeight.disabled=true;");
         strmResponse.println(" } ") ;
        strmResponse.println("}");
        strmResponse.println("}");

        strmResponse.println("function setDefaultVATcode(chkDefaultVAT)");
        strmResponse.println("{");
        //strmResponse.println("alert(chkDefaultVAT);");
        strmResponse.println("var txtVATCode=document.getElementById(\"txtVATCode\");");
        strmResponse.println("if(chkDefaultVAT.checked)");
        strmResponse.println("{");
        strmResponse.println("txtVATCode.value='" + login.getDefaultVATcode() + "';");
        strmResponse.println("txtVATCode.disabled=true;");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("txtVATCode.disabled=false;");
        strmResponse.println("txtVATCode.value=\"\";");
        strmResponse.println("}");
        strmResponse.println("}");

        //Number only checking
        strmResponse.println("function checkKey(evt,type) {");
        //strmResponse.println("alert('inside fn');");
        strmResponse.println("evt = (evt) ? evt : window.event");
        strmResponse.println("var charCode = (evt.which) ? evt.which : evt.keyCode");
        strmResponse.println("if (charCode==46) return true;");
        strmResponse.println("if (charCode > 31 && (charCode < 48 || charCode > 57)) {");
        strmResponse.println("status = \"This field accepts numbers only.\";");
        strmResponse.println("return false;");
        strmResponse.println("}");
        //strmResponse.println("alert(charCode);");
//        strmResponse.println("if (charCode == 9)");
//        strmResponse.println("{");
//        strmResponse.println("   if(type==\"I\")");
//        strmResponse.println("      document.getElementById(\"imgIAdd\").focus();");
//        strmResponse.println("   else");
//        strmResponse.println("      document.getElementById(\"imgCAdd\").focus();");
//        //strmResponse.println("return true;");
//        strmResponse.println("}");
//        strmResponse.println("if (charCode == 13 || charCode == 3 )");
//        strmResponse.println("{");
//        //strmResponse.println("alert(type);");
//        strmResponse.println("   if(type==\"I\")");
//        strmResponse.println("      vSubmit('new');");
//        strmResponse.println("   else");
//        strmResponse.println("      vSubmit1('new');");
//        strmResponse.println("return true;");
//        strmResponse.println("}");
        strmResponse.println("status = \"\";");
        strmResponse.println("return true;");
        strmResponse.println("}");


        strmResponse.println("function checkKeyIW(evt,type) {");
        //strmResponse.println("alert('inside fn');");
        strmResponse.println("evt = (evt) ? evt : window.event");
        strmResponse.println("var charCode = (evt.which) ? evt.which : evt.keyCode");
        strmResponse.println("if (charCode==46){ ");
//         strmResponse.println("itemWeightCount=itemWeightCount+1;");
        strmResponse.println("return true;");
        strmResponse.println("}");
        strmResponse.println("if (charCode > 31 && (charCode < 48 || charCode > 57)) {");
        strmResponse.println("status = \"This field accepts numbers only.\";");
        strmResponse.println("return false;");
        strmResponse.println("}");
        strmResponse.println("status = \"\";");
//        strmResponse.println("itemWeightCount=itemWeightCount+1;");
        strmResponse.println("return true;");
        strmResponse.println("}");

        //Number only checking
        strmResponse.println("function checkIt(evt) {");
        strmResponse.println("evt = (evt) ? evt : window.event");
        strmResponse.println("var charCode = (evt.which) ? evt.which : evt.keyCode");
        strmResponse.println("if (charCode==46) return true;");
        strmResponse.println("if (charCode > 31 && (charCode < 48 || charCode > 57)) {");
        strmResponse.println("status = \"This field accepts numbers only.\";");
        strmResponse.println("return false;");
        strmResponse.println("}");
        strmResponse.println("status = \"\";");
        strmResponse.println("return true;");
        strmResponse.println("}");

        //Number only checking
        strmResponse.println("function checkItNo(evt) {");
        strmResponse.println("evt = (evt) ? evt : window.event");
        strmResponse.println("var charCode = (evt.which) ? evt.which : evt.keyCode");
        strmResponse.println("if ((charCode < 48 || charCode > 57)) {");
        strmResponse.println("status = \"This field accepts numbers only.\";");
        strmResponse.println("return false;");
        strmResponse.println("}");
        strmResponse.println("status = \"\";");
        strmResponse.println("return true;");
        strmResponse.println("}");

        //Decimal Format
        strmResponse.println("function format_number(pnumber,decimals)");
        strmResponse.println("{");
        strmResponse.println("    if (isNaN(pnumber)) { return 0};");
        strmResponse.println("    if (pnumber=='') { return 0};");

        strmResponse.println("    var snum = new String(pnumber);");
        strmResponse.println("    var sec = snum.split('.');");
        strmResponse.println("    var whole = parseFloat(sec[0]);");
        strmResponse.println("    var result = '';");

        strmResponse.println("    if(sec.length > 1){");
        strmResponse.println("            var dec = new String(sec[1]);");
        strmResponse.println("            dec = String(parseFloat(sec[1])/Math.pow(10,(dec.length - decimals)));");
        strmResponse.println("            dec = String(whole + Math.round(parseFloat(dec))/Math.pow(10,decimals));");
        strmResponse.println("            var dot = dec.indexOf('.');");
        strmResponse.println("            if(dot == -1){");
        strmResponse.println("                    dec += '.'; ");
        strmResponse.println("                    dot = dec.indexOf('.');");
        strmResponse.println("            }");
        strmResponse.println("            while(dec.length <= dot + decimals) { dec += '0'; }");
        strmResponse.println("            result = dec;");
        strmResponse.println("    } else{");
        strmResponse.println("            var dot;");
        strmResponse.println("            var dec = new String(whole);");
        strmResponse.println("            dec += '.';");
        strmResponse.println("            dot = dec.indexOf('.');	");
        strmResponse.println("            while(dec.length <= dot + decimals) { dec += '0'; }");
        strmResponse.println("            result = dec;");
        strmResponse.println("    }	");
        strmResponse.println("    return result;");
        strmResponse.println("}");

        //Disable Indemnity
        strmResponse.println("function disableIndemnity(value)");
        strmResponse.println("{");
        //strmResponse.println("alert(arrCommodity.indexOf(value));");
        strmResponse.println("var selIndemnity=document.getElementById(\"" + sParam_EditCell1 + "3\");");
        //strmResponse.println("alert(selIndemnity);");
        strmResponse.println("if(arrCommodity.indexOf(value)==-1)");
        strmResponse.println("{");
        //strmResponse.println("alert('inside 1');");
        strmResponse.println("selIndemnity.value=\"NO\";");
        strmResponse.println("selIndemnity.disabled=true;");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("if(arrIndemnity[arrCommodity.indexOf(value)]==\"N\")");
        strmResponse.println("{");
        //strmResponse.println("alert('inside 2');");
        strmResponse.println("selIndemnity.value=\"NO\";");
        strmResponse.println("selIndemnity.disabled=true;");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("selIndemnity.value=\"SELECT\";");
        strmResponse.println("selIndemnity.disabled=false;");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("}");

        strmResponse.println("var winCountry=null;");
        strmResponse.println("function notesClick()");
        strmResponse.println("{");
        strmResponse.println("var dCountry=document.getElementById(\"selDCountry\").value;");
        strmResponse.println("  winCountry=dhtmlwindow.open('countrynotes','iframe','CountryNotes?CY='+ dCountry + '','CitySprint - Country Information','width=590px,height=430px,center=1,scrolling=0,resize=0');");
        strmResponse.println("disableBooking();");
        strmResponse.println("}");
        //Delivery Country Change
        strmResponse.println("function dCountryChange(dCountry)");
        strmResponse.println("{");
        //strmResponse.println("alert(dCountry);");
        strmResponse.println("if(dCountry==\"GBR\")");
        strmResponse.println("{");
        strmResponse.println("  document.getElementById(\"lblDPostcode\").innerHTML=\"Postcode*\";");
//        strmResponse.println("  document.getElementById(\"lblDPostcode\").style.color='#CC0000';");
//        strmResponse.println("  document.getElementById(\"txtDPostcode\").style.border='3px solid #7F9DB9';");
        strmResponse.println("  document.getElementById(\"btnDPostcode\").style.display='';");
        strmResponse.println("  document.getElementById(\"imgDPostcodeHelp\").style.display='';");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("  document.getElementById(\"lblDPostcode\").innerHTML=\"Postcode\";");
//        strmResponse.println("  document.getElementById(\"lblDPostcode\").style.color='#666666';");
//        strmResponse.println("  document.getElementById(\"txtDPostcode\").style.border='1px solid #7F9DB9';");
        strmResponse.println("  document.getElementById(\"btnDPostcode\").style.display='none';");
        strmResponse.println("  document.getElementById(\"imgDPostcodeHelp\").style.display='none';");
        strmResponse.println("}");
        //strmResponse.println("notesClick();");
        strmResponse.println("}");

        //typable combobox script
        strmResponse.println("var fActiveMenu = false;");
        strmResponse.println("var oOverMenu = false;");
        strmResponse.println("function mouseSelect(e)");
        strmResponse.println("{");
        strmResponse.println("if (fActiveMenu)");
        strmResponse.println("{");
        strmResponse.println("if (oOverMenu == false)");
        strmResponse.println("{");
        strmResponse.println("oOverMenu = false;");
        strmResponse.println("document.getElementById(fActiveMenu).style.display = \"none\";");
        strmResponse.println("fActiveMenu = false;");
        strmResponse.println("return false;");
        strmResponse.println("}");
        strmResponse.println("return false;");
        strmResponse.println("}");
        strmResponse.println("return true;");
        strmResponse.println("}");
        strmResponse.println("function menuActivate(idEdit, idMenu, idSel)");
        strmResponse.println("{");
        strmResponse.println("if (fActiveMenu) return mouseSelect(0);");
        strmResponse.println("oMenu = document.getElementById(idMenu);");
        strmResponse.println("oEdit = document.getElementById(idEdit);");
        strmResponse.println("nTop = oEdit.offsetTop + oEdit.offsetHeight;");
        strmResponse.println("nLeft = oEdit.offsetLeft;");
        strmResponse.println("while (oEdit.offsetParent != document.body)");
        strmResponse.println("{");
        strmResponse.println("oEdit = oEdit.offsetParent;");
        strmResponse.println("nTop += oEdit.offsetTop;");
        strmResponse.println("nLeft += oEdit.offsetLeft;");
        strmResponse.println("}");
        strmResponse.println("oMenu.style.left = nLeft;");
        strmResponse.println("oMenu.style.top = nTop;");
        strmResponse.println("oMenu.style.display = \"\";");
        strmResponse.println("fActiveMenu = idMenu;");
        strmResponse.println("document.getElementById(idSel).focus();");
        strmResponse.println("return false;");
        strmResponse.println("}");
        strmResponse.println("function textSet(idEdit, text)");
        strmResponse.println("{");
//      strmResponse.println("alert(text);");
        strmResponse.println("document.getElementById(idEdit).value = text;");
//        strmResponse.println("var valueSplit=value.split(\"^\");");
//        strmResponse.println("if(valueSplit.length>0)");
//        strmResponse.println("{");
//        strmResponse.println("var selIndemnity=document.getElementById(\""+sParam_EditCell1+"3\");");
//        strmResponse.println("if(valueSplit[1]==\"N\")");
//        strmResponse.println("{");
//        strmResponse.println("selIndemnity.value=\"NO\";");
//        strmResponse.println("selIndemnity.disabled=true;");
//        strmResponse.println("}");
//        strmResponse.println("else");
//        strmResponse.println("selIndemnity.disabled=false");
//        strmResponse.println("}");
        strmResponse.println("oOverMenu = false;");
        strmResponse.println("mouseSelect(0);");
        strmResponse.println("document.getElementById(idEdit).focus();");
        strmResponse.println("}");
        strmResponse.println("function comboKey(idEdit, text)");
        strmResponse.println("{");
        strmResponse.println("if (window.event.keyCode == 13 || window.event.keyCode == 32)");
        strmResponse.println("textSet(idEdit,text.value);");
        strmResponse.println("else if (window.event.keyCode == 27)");
        strmResponse.println("{");
        strmResponse.println("mouseSelect(0);");
        strmResponse.println("document.getElementById(idEdit).focus();");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("document.onmousedown = mouseSelect;");



    }

    /**
     *
     * @param strmResponse
     * @param login
     * @throws java.io.IOException
     */
    protected void vWriteServicesJavascriptFunctions(ServletOutputStream strmResponse, LoginInfo login) throws IOException {

        strmResponse.println("function validateCCDetails()");
        strmResponse.println("{");
        strmResponse.println("var error=false;");
        strmResponse.println("var message=\"\";");
        strmResponse.println("var trServices=document.getElementById(\"tblServiceList\");");
        //strmResponse.println("alert(trServices);");
        //strmResponse.println("alert(trServices.getElementsByTagName('input'));");
        strmResponse.println("var rdServices=trServices.getElementsByTagName('input');");
        strmResponse.println("var lblMessage=document.getElementById(\"lblMessage1\");");
        strmResponse.println("lblMessage.innerText=\"\";");
        
//        strmResponse.println("alert('login credit flag ==" + login.getCreditCardFlag() + "');");
        

        if (login.getCreditCardFlag().equals("F")) {
            strmResponse.println("var txtCCNumber=document.getElementById(\"txtCCNumber\");");
            strmResponse.println("var selCCType=document.getElementById(\"selCCType\");");
            strmResponse.println("var txtCCName=document.getElementById(\"txtCCName\");");
            strmResponse.println("var selCExpiryMonth=document.getElementById(\"selCExpiryMonth\");");
            strmResponse.println("var selCExpiryYear=document.getElementById(\"selCExpiryYear\");");
            strmResponse.println("var selStartDay=document.getElementById(\"selStartDay\");");
            strmResponse.println("var selStartMonth=document.getElementById(\"selStartMonth\");");
            strmResponse.println("var selIssueNo=document.getElementById(\"selIssueNo\");");
            strmResponse.println("var txtSecurityCode=document.getElementById(\"txtSecurityCode\");");
            strmResponse.println("var txtPostcode=document.getElementById(\"txtPostcode\");");
            strmResponse.println("var txtCCEmail=document.getElementById(\"txtCCEmail\");");

            if (login.isAvsAddressMandatoryFlag()) {
                strmResponse.println("var txtAVSAddress1=document.getElementById(\"txtAVSAddress1\");");
                strmResponse.println("var txtAVSAddress2=document.getElementById(\"txtAVSAddress2\");");
                strmResponse.println("var txtAVSAddress3=document.getElementById(\"txtAVSAddress3\");");
                strmResponse.println("var txtAVSAddress4=document.getElementById(\"txtAVSAddress4\");");

                strmResponse.println("var sAVSAddress1=txtAVSAddress1.value;");
                strmResponse.println("var sAVSAddress2=txtAVSAddress2.value;");
                strmResponse.println("var sAVSAddress3=txtAVSAddress3.value;");
                strmResponse.println("var sAVSAddress4=txtAVSAddress4.value;");
            }
            strmResponse.println("var sType=selCCType.value;");
            strmResponse.println("var sCCNumber=txtCCNumber.value;");
            strmResponse.println("var sCCName=txtCCName.value;");
            strmResponse.println("var sSecurityCode=txtSecurityCode.value;");
            strmResponse.println("var sPostcode=txtPostcode.value;");
            strmResponse.println("var sCCEmail=txtCCEmail.value;");

        }

        if (login.isEmailAccountFlag()) {
            strmResponse.println("var txtTrackingEmail=document.getElementById(\"txtTrackingEmail\");");
            strmResponse.println("var chkBookingEmail=document.getElementById(\"chkBookingEmail\");");
            strmResponse.println("var chkCollectionEmail=document.getElementById(\"chkCollectionEmail\");");
            strmResponse.println("var chkDeliveryEmail=document.getElementById(\"chkDeliveryEmail\");");
            strmResponse.println("var sTrackingEmail=txtTrackingEmail.value;");
            strmResponse.println("var sEmailNotify=\"\";");
        }

        if (login.isSmsAccountFlag()) {
            strmResponse.println("var chkBookingSMS=document.getElementById(\"chkBookingSMS\");");
            strmResponse.println("var chkCollectionSMS=document.getElementById(\"chkCollectionSMS\");");
            strmResponse.println("var chkDeliverySMS=document.getElementById(\"chkDeliverySMS\");");
            strmResponse.println("var txtTrackingSMS=document.getElementById(\"txtTrackingSMS\");");
            strmResponse.println("var sTrackingSMS=txtTrackingSMS.value;");
            strmResponse.println("var sSMSNotify=\"\";");
        }


        if (login.getCreditCardFlag().equals("F")) {
            strmResponse.println("if(sCCNumber==\"\")");
            strmResponse.println("{");
            strmResponse.println("message=\"Credit Card Number should be entered.\\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");

            strmResponse.println("if(sCCName==\"\")");
            strmResponse.println("{");

            strmResponse.println("message=message+\"Credit Card holder's name should be entered.\\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");

            strmResponse.println("if(selCExpiryMonth.value==\"\")");
            strmResponse.println("{");
            strmResponse.println("message=message+\"Credit Card Expiry Month should be selected.\\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");

            strmResponse.println("if(selCExpiryYear.value==\"\")");
            strmResponse.println("{");
            strmResponse.println("message=message+\"Credit Card Expiry Year should be selected.\\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");
            strmResponse.println("else");
            strmResponse.println("{");
            strmResponse.println("       var myDate = new Date();");
            strmResponse.println("       var sMonth = myDate.getMonth()+1;");
            strmResponse.println("       var sDate=myDate.getFullYear();");
            strmResponse.println("       var j=String(sDate).substring(2);");

            strmResponse.println("       if(selCExpiryMonth.value<= sMonth && selCExpiryYear.value==j)");
            strmResponse.println("       {");
            strmResponse.println("           message=message+\"Invalid credit card expiry month.\\n\";");
            strmResponse.println("           error=true;");
            strmResponse.println("       }");

            strmResponse.println("}");

            strmResponse.println("if((sType==\"Switch\" || sType==\"Solo\") && (selStartDay.value==\"\" || selStartMonth.value==\"\"))");
            strmResponse.println("{");
            // strmResponse.println("alert('inside start date');");
            strmResponse.println("message=message+\"Start Date (Switch/Solo) should be selected.\\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");

            strmResponse.println("if((sType==\"Switch\" || sType==\"Solo\") && selIssueNo.value==\"\")");
            strmResponse.println("{");
            // strmResponse.println("alert('inside issueno');");
            strmResponse.println("message=message+\"Issue Number (Switch/Solo) should be selected.\\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");

            if (login.isCv2SecurityCodeRequiredFlag()) {
                strmResponse.println("if(sSecurityCode==\"\")");
                strmResponse.println("{");
                strmResponse.println("message=message+\"Security Code should be entered.\\n\";");
                strmResponse.println("error=true;");
                strmResponse.println("}");
            }

            if (login.isPostcodeRequiredFlag()) {
                strmResponse.println("if(sPostcode==\"\")");
                strmResponse.println("{");
                strmResponse.println("message=message+\"Postcode should be entered.\\n\";");
                strmResponse.println("error=true;");
                strmResponse.println("}");
            }

            if (login.isAvsAddressMandatoryFlag()) {
                strmResponse.println("if(sAVSAddress1==\"\")");
                strmResponse.println("{");
                strmResponse.println("message=message+\"Address1 should be entered.\\n\";");
                strmResponse.println("error=true;");
                strmResponse.println("}");
            }
        }

        if (login.isEmailAccountFlag()) {
            strmResponse.println("if(chkBookingEmail.checked || chkCollectionEmail.checked || chkDeliveryEmail.checked )");
            strmResponse.println("{");
            strmResponse.println("if(!validateEmail(sTrackingEmail))");
            strmResponse.println("{");
            strmResponse.println("message=\"Tracking Email is Missing or Invalid.\\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");
            strmResponse.println("}");
            strmResponse.println("else");
            strmResponse.println("if(sTrackingEmail.length>0)");
            strmResponse.println("{");
            strmResponse.println("if(!chkBookingEmail.checked && !chkCollectionEmail.checked && !chkDeliveryEmail.checked )");
            strmResponse.println("{");
            strmResponse.println("	message=\"Please tick any one of the Tracking Notification Type (Booking|Collection|Delivery) of Email.\\n\";");
            strmResponse.println("	error=true; ");
            strmResponse.println("}");
            strmResponse.println("}");
        }

        if (login.isSmsAccountFlag()) {
            strmResponse.println("if(chkBookingSMS.checked || chkCollectionSMS.checked || chkDeliverySMS.checked)");
            strmResponse.println("{");
//             strmResponse.println("if(!validateMobNo(sTrackingSMS))");
//             strmResponse.println("{");
//             strmResponse.println("message=\"Tracking SMS is Missing or Invalid \\n\";");
//             strmResponse.println("error=true;");
//             strmResponse.println("}");
            strmResponse.println("}");
            strmResponse.println("else");
            strmResponse.println("if(sTrackingSMS.length>0)");
            strmResponse.println("{");
            strmResponse.println("if(!chkBookingSMS.checked && !chkCollectionSMS.checked && !chkDeliverySMS.checked )");
            strmResponse.println("{");
            strmResponse.println("	message=\"Please tick any one of the Tracking Notification Type (Booking|Collection|Delivery) of SMS\\n\";");
            strmResponse.println("	error=true; ");
            strmResponse.println("}");
            strmResponse.println("}");
        }

        strmResponse.println("if(error==true)");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText=message");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = message");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");

        if (login.getCreditCardFlag().equals("F")) {
            strmResponse.println("document.validateForm.CCType.value=sType;");
            strmResponse.println("document.validateForm.CCNumber.value=sCCNumber;");
            strmResponse.println("document.validateForm.CCName.value=sCCName;");
            strmResponse.println("document.validateForm.sExpiryMonth.value=selCExpiryMonth.value;");
            strmResponse.println("document.validateForm.sExpiryYear.value=selCExpiryYear.value;");
            strmResponse.println("document.validateForm.sSecurityCode.value=sSecurityCode;");
            strmResponse.println("document.validateForm.sPostcode.value=sPostcode;");

            if (login.isAvsAddressMandatoryFlag()) {
                strmResponse.println("document.validateForm.sAVSAddress1.value=sAVSAddress1;");
                strmResponse.println("document.validateForm.sAVSAddress2.value=sAVSAddress2;");
                strmResponse.println("document.validateForm.sAVSAddress3.value=sAVSAddress3;");
                strmResponse.println("document.validateForm.sAVSAddress4.value=sAVSAddress4;");
            }

            strmResponse.println("document.validateForm.sCCEmail.value=sCCEmail;");

            strmResponse.println("if(sType==\"Switch\" || sType==\"Solo\")");
            strmResponse.println("{");
            strmResponse.println("document.validateForm.sStartDay.value=selStartDay.value;");
            strmResponse.println("document.validateForm.sStartMonth.value=selStartMonth.value;");
            strmResponse.println("document.validateForm.sIssueNo.value=selIssueNo.value;");
            strmResponse.println("}");
        }

        strmResponse.println("for (var i=0,thisInput; thisInput=rdServices[i]; i++) {");
        strmResponse.println("if(thisInput.type==\"radio\" && thisInput.checked)");
        strmResponse.println("document.validateForm.sServices.value=thisInput.value;");
        strmResponse.println("}");
        //strmResponse.println("alert('services:'+document.validateForm.sServices.value);");


        if (login.isEmailAccountFlag()) {
            strmResponse.println("document.validateForm.sTrackingEmail.value=sTrackingEmail;");
            strmResponse.println("if(chkBookingEmail.checked)");
            strmResponse.println("sEmailNotify=\"B\";");
            strmResponse.println("if(chkCollectionEmail.checked)");
            strmResponse.println("sEmailNotify=sEmailNotify+\"C\";");
            strmResponse.println("if(chkDeliveryEmail.checked)");
            strmResponse.println("sEmailNotify=sEmailNotify+\"D\";");
            strmResponse.println("document.validateForm.sEmailNotify.value=sEmailNotify;");
        }

        if (login.isSmsAccountFlag()) {
            strmResponse.println("document.validateForm.sTrackingSMS.value=sTrackingSMS;");
            strmResponse.println("if(chkBookingSMS.checked)");
            strmResponse.println("sSMSNotify=\"B\";");
            strmResponse.println("if(chkCollectionSMS.checked)");
            strmResponse.println("sSMSNotify=sSMSNotify+\"C\";");
            strmResponse.println("if(chkDeliverySMS.checked)");
            strmResponse.println("sSMSNotify=sSMSNotify+\"D\";");
            strmResponse.println("document.validateForm.sSMSNotify.value=sSMSNotify;");
        }

        strmResponse.println("document.validateForm.submit();");
        strmResponse.println("}");
        strmResponse.println("}");

        strmResponse.println("function CCTypeChange()");
        strmResponse.println("{");
        strmResponse.println("var selCCType=document.getElementById(\"selCCType\");");
        strmResponse.println("var selStartDay=document.getElementById(\"selStartDay\");");
        strmResponse.println("var selStartMonth=document.getElementById(\"selStartMonth\");");
        strmResponse.println("var selIssueNo=document.getElementById(\"selIssueNo\");");
        strmResponse.println("if(selCCType.value==\"Switch\" || selCCType.value==\"Solo\")");
        strmResponse.println("{");
        strmResponse.println("selStartDay.disabled=false;");
        strmResponse.println("selIssueNo.disabled=false;");
        strmResponse.println("selStartMonth.disabled=false;");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("selStartDay.disabled=true;");
        strmResponse.println("selIssueNo.disabled=true;");
        strmResponse.println("selStartMonth.disabled=true;");
        strmResponse.println("}");
        strmResponse.println("}");

        //Service Row Selected
        strmResponse.println("function serviceSelect(obj,value)");
        strmResponse.println("{");
        strmResponse.println("var trServices=document.getElementById(\"tblServiceList\");");
        strmResponse.println("var rdServices=trServices.getElementsByTagName('input');");
        strmResponse.println("var trRows=trServices.getElementsByTagName('tr');");
        strmResponse.println("for (var i=0,thisInput; thisInput=rdServices[i]; i++) {");
        strmResponse.println("if(thisInput.type==\"radio\" && thisInput.value==value)");
        strmResponse.println("{");
        strmResponse.println("thisInput.checked=true;");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("thisInput.checked=false;");
        //strmResponse.println("obj.bgColor='#efefef';");
        strmResponse.println("}");
        strmResponse.println("}");

        strmResponse.println("for (var j=0,thisTr; thisTr=trRows[j]; j++) {");
        strmResponse.println("if(thisTr.id==('tr'+value))");
        strmResponse.println("{");
        //strmResponse.println("alert('inside if');");
        strmResponse.println("           thisTr.style.border = \"1px solid #44FFCC\";");
        //strmResponse.println("           thisTr.onmouseover=highlight;");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("          thisTr.style.border = \"none\";");
        //strmResponse.println("          thisTr.onmouseover=highlight;");
        //strmResponse.println("          thisTr.onmouseout=normal;");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("}");

        strmResponse.println("function highlight(event) {");
        //strmResponse.println("alert('inside highlight');");
        strmResponse.println(" this.style.backgroundColor=\"#33CCFF\";");
        //strmResponse.println(" this.style.color='white';");
        strmResponse.println("}");

        strmResponse.println("function normal(event) {");
        strmResponse.println("var trServices=document.getElementById(\"tblServiceList\");");
        strmResponse.println("var rdServices=trServices.getElementsByTagName('input');");
        strmResponse.println("var trRows=trServices.getElementsByTagName('tr');");
        strmResponse.println("for (var i=0,thisInput; thisInput=rdServices[i]; i++) {");
        strmResponse.println("if(thisInput.type==\"radio\" && thisInput.checked==true)");
        strmResponse.println("{");
        strmResponse.println(" thisInput.style.backgroundColor=\"#44FFCC\";");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println(" this.style.backgroundColor=\"#efefef\";");
        //strmResponse.println("obj.bgColor='#efefef';");
        strmResponse.println("}");
        strmResponse.println("}");

        //strmResponse.println(" this.style.color='';");
        strmResponse.println("}");

        //Function show Price

        strmResponse.println("function ShowPrice(sLP,sBP,sGS,sSC,sI)");
        strmResponse.println("{");
        //  strmResponse.println("alert('Show details');");

        strmResponse.println("var fLP=parseFloat(sLP);");
        strmResponse.println("var fBP=parseFloat(sBP);");
        strmResponse.println("var fGS=parseFloat(sGS);");
        strmResponse.println("var fSC=parseFloat(sSC);");
        strmResponse.println("var fTotal=fLP+fBP+fGS+fSC;");
        //  strmResponse.println("alert(fTotal);");
        strmResponse.println("var lblPrice=document.getElementById(\"lblPrice\"+sI);");
        strmResponse.println("var sPrice=\"\"");

        //Display the total price and the spilt of price.
        //   strmResponse.println("lblPrice.innerText=\"Total Charges: \"+fTotal+\" ( Value \"+fBP+\" + Indeminity \\n\"+fLP+\" + GST/VAT \"+fGS+\" + Total Surcharge \"+ fSC +\")\";");
        strmResponse.println("sPrice=\"Total Charges: £\"+fTotal.toFixed(2)+\" (Shipping Charge: £\"+fBP.toFixed(2)+\" + Indeminity: £\"+fLP.toFixed(2)+\" + VAT: £\"+fGS.toFixed(2)+\" + Total Surcharge: £\"+ fSC.toFixed(2) +\")\";");
        strmResponse.println("var sLen=sPrice.length;");
        strmResponse.println("if(sLen>80)");
        strmResponse.println("sLen=sLen+565;");
        strmResponse.println("else");
        strmResponse.println("sLen=sLen+545;");
        //strmResponse.println("alert(sLen);");
        strmResponse.println("	 sWinOpen = 1;");
        strmResponse.println("if((winprice == null) || (winprice.closed == true))");
        strmResponse.println("	 winprice=window.open('PricePopup.jsp?sPrice='+sPrice,'TrackingForm','resizable=yes,scrollbars=no,width='+sLen+' ,height=150');");
        strmResponse.println("else");
        strmResponse.println(" winprice.focus();");
        strmResponse.println("}");

        //Multiple Tracking Notification

        strmResponse.println("function Tracking(sType)");
        strmResponse.println("{");
        //    strmResponse.println("alert('inside fn');");
        if (login.isEmailAccountFlag()) {
            strmResponse.println("var txtTrackingEmail=document.getElementById(\"txtTrackingEmail\");");
            strmResponse.println("var chkBookingEmail=document.getElementById(\"chkBookingEmail\");");
            strmResponse.println("var chkCollectionEmail=document.getElementById(\"chkCollectionEmail\");");
            strmResponse.println("var chkDeliveryEmail=document.getElementById(\"chkDeliveryEmail\");");
        }
//         strmResponse.println("var chkDelayedEmail=document.getElementById(\"chkDelayedEmail\");");

        if (login.isSmsAccountFlag()) {
            strmResponse.println("var txtTrackingSMS=document.getElementById(\"txtTrackingSMS\");");
            strmResponse.println("var chkBookingSMS=document.getElementById(\"chkBookingSMS\");");
            strmResponse.println("var chkCollectionSMS=document.getElementById(\"chkCollectionSMS\");");
            strmResponse.println("var chkDeliverySMS=document.getElementById(\"chkDeliverySMS\");");
        }
//         strmResponse.println("var chkDelayedSMS=document.getElementById(\"chkDelayedSMS\");");
        strmResponse.println("var lblMessage1=document.getElementById(\"lblMessage1\");");
        strmResponse.println("lblMessage1.innerText=\"\";");
        strmResponse.println("lblMessage1.textContent=\"\";");
        strmResponse.println("var error=false;");
        strmResponse.println("var message=\"\";");
        strmResponse.println("var sEmailNotify=\"\";");
        strmResponse.println("var sSMSNotify=\"\";");

        if (login.isSmsAccountFlag()) {
            strmResponse.println("if(sType==\"S\")");
            strmResponse.println("{");
            strmResponse.println("if(txtTrackingSMS.value==\"\")");
            strmResponse.println("{");
            strmResponse.println("message=message+\"Please enter Tracking SMS to proceed to Multiple Notifications.\\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");
            strmResponse.println("else");
            strmResponse.println("if(!chkBookingSMS.checked && !chkCollectionSMS.checked && !chkDeliverySMS.checked )");
            strmResponse.println("{");
            strmResponse.println("message=message+\"Please select any one of the Tracking Notification Type (Booking|Collection|Delivery)of SMS to proceed to Multiple Notifications.\\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");
            strmResponse.println("}");
        }

        if (login.isEmailAccountFlag()) {
            strmResponse.println("if(sType==\"E\")");
            strmResponse.println("{");
            strmResponse.println("if(txtTrackingEmail.value==\"\")");
            strmResponse.println("{");
            strmResponse.println("message=message+\"Please enter Tracking Email to proceed to Multiple Notifications.\\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");
            strmResponse.println("else");
            strmResponse.println("if(!chkBookingEmail.checked && !chkCollectionEmail.checked && !chkDeliveryEmail.checked )");
            strmResponse.println("{");
            strmResponse.println("message=message+\"Please select any one of the Tracking Notification Type (Booking|Collection|Delivery) of Email to proceed to Multiple Notifications\\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");
            strmResponse.println("}");
        }

        strmResponse.println("if(error==true)");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage1.innerText=message;");
        strmResponse.println("else");
        strmResponse.println("lblMessage1.textContent=message;");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("	 sWinOpen = 1;");
        strmResponse.println("if((wintracking == null) || (wintracking.closed == true))");
        strmResponse.println("	 wintracking=window.open('blank.html','TrackingForm','resizable=no,scrollbars=yes,width=800,height=500');");
        strmResponse.println("else");
        strmResponse.println("  wintracking.focus();");

        if (login.isEmailAccountFlag()) {
            strmResponse.println("document.TrackingForm.sTrackingEmail.value=txtTrackingEmail.value;");
            strmResponse.println("if(chkBookingEmail.checked)");
            strmResponse.println("sEmailNotify=\"B\";");
            strmResponse.println("if(chkCollectionEmail.checked)");
            strmResponse.println("sEmailNotify=sEmailNotify+\"C\";");
            strmResponse.println("if(chkDeliveryEmail.checked)");
            strmResponse.println("sEmailNotify=sEmailNotify+\"D\";");
            //         strmResponse.println("if(chkDelayedEmail.checked)");
            //         strmResponse.println("sEmailNotify=sEmailNotify+\"E\";");
            strmResponse.println("document.TrackingForm.sEmailNotify.value=sEmailNotify;");
        }

        if (login.isSmsAccountFlag()) {
            strmResponse.println("document.TrackingForm.sTrackingSMS.value=txtTrackingSMS.value;");
            strmResponse.println("document.TrackingForm." + Constants.STYPE + ".value=sType;");

            strmResponse.println("if(chkBookingSMS.checked)");
            strmResponse.println("sSMSNotify=\"B\";");
            strmResponse.println("if(chkCollectionSMS.checked)");
            strmResponse.println("sSMSNotify=sSMSNotify+\"C\";");
            strmResponse.println("if(chkDeliverySMS.checked)");
            strmResponse.println("sSMSNotify=sSMSNotify+\"D\";");
            //         strmResponse.println("if(chkDelayedSMS.checked)");
            //         strmResponse.println("sSMSNotify=sSMSNotify+\"E\";");
            strmResponse.println("document.TrackingForm.sSMSNotify.value=sSMSNotify;");
        }

        strmResponse.println("document.TrackingForm.submit();");
        strmResponse.println("}");
        strmResponse.println("}");

        //Validate email address.
        strmResponse.println("function validateEmail(sEmail)");
        strmResponse.println("{");
        strmResponse.println("var result=false;");
        strmResponse.println("var nIndex = sEmail.indexOf('@');");
        strmResponse.println("var nIndexDot = sEmail.indexOf('.');");
        strmResponse.println("if(nIndex == -1 || nIndexDot == -1)");
        strmResponse.println("result=false;");
        strmResponse.println("else");
        strmResponse.println("result=true;");
        strmResponse.println("return result;");
        strmResponse.println("}");

        //Validate Mobile number
        strmResponse.println("function validateMobNo(tempstr)");
        strmResponse.println("{");
        // strmResponse.println("alert('inside fn');");
        strmResponse.println("var result=false;");
        strmResponse.println("if((tempstr.length==11) && (tempstr.substring(0,3)==\"077\" || tempstr.substring(0,3)==\"078\" || tempstr.substring(0,3)==\"079\"))");
        strmResponse.println("{");
        strmResponse.println("  result=true; ");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("if(tempstr.length==12 && tempstr.substring(0,4)==\"+614\")");
        strmResponse.println("{");
        strmResponse.println(" result=true;");
        strmResponse.println("}");
        strmResponse.println("return result;");
        strmResponse.println("}");


    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void validateCCDetails(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");

        String message = "";

        HttpSession validSession = request.getSession(false);

        if (validSession == null) {
        } else {
            String[][] sTrackingEmail1 = new String[5][5];
            String[][] sTrackingSMS1 = new String[5][5];
            String[][] sTrackingEmail2 = new String[5][5];
            String[][] sTrackingSMS2 = new String[5][5];
            boolean error = true;
            long number = 0;

            LoginInfo login = new LoginInfo();
            BookingInfo booking = new BookingInfo();
            CreditCardDetails ccdetails = new CreditCardDetails();

            //set login,collection,delivery address fields and other booking info fields by processing the request object
            login.parseLogin((String[][]) validSession.getAttribute("loginArray"), request);
            ccdetails.setCreditCardDetails(request);
            booking.setBookingInfo(request, response, login);

            if (login.getCreditCardFlag().equals("F")) {
                try {
                    number = Long.parseLong(ccdetails.getCardNo().trim());
                } catch (Exception ex) {
                    error = false;
                }
                long orignumber = number;

                if (!error) {
                    if (ccdetails.getCCType().equals("American Express")) {
                        if ((orignumber < 340000000000000L) || (orignumber > 379199999999999L)) {
                            error = false;
                        }
                        //If the customer enters 16 digits then ...
                        if ((orignumber > 3528000000000000L) && (orignumber < 3589999999999999L)) {
                            error = false;
                        }
                    }
                    // visa delta
                    if (ccdetails.getCCType().equals("VISA Delta")) {
                        if ((orignumber > 4137330000000000L) && (orignumber < 4137379999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4462000000000000L) && (orignumber < 4461990000000000L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4539780000000000L) && (orignumber < 4539799999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4543130000000000L) && (orignumber < 4543139999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4544320000000000L) && (orignumber < 4544359999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4547420000000000L) && (orignumber < 4547429999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4567250000000000L) && (orignumber < 4567459999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4658300000000000L) && (orignumber < 4658799999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4659010000000000L) && (orignumber < 4569509999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4844090000000000L) && (orignumber < 4844109999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4909600000000000L) && (orignumber < 4909799999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4921810000000000L) && (orignumber < 4921829999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4988240000000000L) && (orignumber < 4988249999999999L)) {
                            // valid number
                            error = true;
                        } else // invalid Visa Delta
                        {
                            error = false;
                        }
                    }
                    //JCB
                    if (ccdetails.getCCType().equals("JCB")) {
                        if (!(orignumber > 3528000000000000L) && (orignumber < 3589999999999999L)) {
                            error = false;
                        }
                    }
                    // Mastercard
                    if (ccdetails.getCCType().equals("Mastercard")) {
                        if ((orignumber < 5100000000000000L) || (orignumber > 5599999999999999L)) {
                            error = false;
                        }
                    }
                    // Solo
                    if (ccdetails.getCCType().equals("Solo")) {
                        if ((orignumber > 6334500000000000L) && (orignumber < 6334999999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 6767000000000000L) && (orignumber < 6767999999999999L)) {
                            // valid number
                            error = true;
                        } //for length 18 digits
                        else if ((orignumber > 633450000000000000L) && (orignumber < 633499999999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 676700000000000000L) && (orignumber < 676799999999999999L)) {
                            // valid number
                            error = true;
                        } //check if length is 19 digits
                        else if ((orignumber > 6334500000000000000L) && (orignumber < 6334999999999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 6767000000000000000L) && (orignumber < 6767999999999999999L)) {
                            // valid number
                            error = true;
                        } else // invalid number for Solo card
                        {
                            error = false;
                        }
                    }

                    // Switch
                    if (ccdetails.getCCType().equals("Switch")) {
                        if ((orignumber > 4903020000000000L) && (orignumber < 4903099999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4903350000000000L) && (orignumber < 4903390000000000L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4911010000000000L) && (orignumber < 4911029999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4911740000000000L) && (orignumber < 4911829999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4936000000000000L) && (orignumber < 4936999999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 5641820000000000L) && (orignumber < 5641829999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 6333000000000000L) && (orignumber < 6333499999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 6759000000000000L) && (orignumber < 6759999999999999L)) {
                            // valid number
                            error = true;
                        } //check for card numbers
                        else if (ccdetails.getCardNo().length() == 18 || ccdetails.getCardNo().length() == 19) {
                            if (ccdetails.getCardNo().startsWith("490302") || ccdetails.getCardNo().startsWith("490309")) {
                                // valid number
                                error = true;
                            } else if (ccdetails.getCardNo().startsWith("490335") || ccdetails.getCardNo().startsWith("490339")) {
                                // valid number
                                error = true;
                            } else if (ccdetails.getCardNo().startsWith("491101") || ccdetails.getCardNo().startsWith("491102")) {
                                // valid number
                                error = true;
                            } else if (ccdetails.getCardNo().startsWith("491174") || ccdetails.getCardNo().startsWith("491182")) {
                                // valid number
                                error = true;
                            } else if (ccdetails.getCardNo().startsWith("493600") || ccdetails.getCardNo().startsWith("493699")) {
                                // valid number
                                error = true;
                            } else if (ccdetails.getCardNo().startsWith("564182")) {
                                // valid number
                                error = true;
                            } else if (ccdetails.getCardNo().startsWith("6333") || ccdetails.getCardNo().startsWith("633349")) {
                                // valid number
                                error = true;
                            } else if (ccdetails.getCardNo().startsWith("6759")) {
                                // valid number
                                error = true;
                            }
                        } else // invalid number for Solo card
                        {

                            error = false;
                        }
                    }

                    // Visa
                    if (ccdetails.getCCType().equals("VISA")) {

                        if (orignumber < 4000000000000000L) {

                            error = false;
                        }
                        if (orignumber > 4999999999999999L) {

                            error = false;
                        }
                        // Switch range invalid for Visa
                        if ((orignumber > 4903020000000000L) && (orignumber < 4903099999999999L)) {

                            error = false;
                        }
                        if ((orignumber > 4903350000000000L) && (orignumber < 4903399999999999L)) {

                            error = false;
                        }
                        if ((orignumber > 4911010000000000L) && (orignumber < 4911029999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4911740000000000L) && (orignumber < 4911829999999999L)) {

                            error = false;
                        }
                        if ((orignumber > 4936000000000000L) && (orignumber < 4936999999999999L)) {

                            error = false;
                        }
                        // visa delta range
                        if ((orignumber > 4137330000000000L) && (orignumber < 4137379999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4462000000000000L) && (orignumber < 4461990000000000L)) {

                            error = false;
                        }
                        if ((orignumber > 4539780000000000L) && (orignumber < 4539799999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4543130000000000L) && (orignumber < 4543139999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4544320000000000L) && (orignumber < 4544359999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4547420000000000L) && (orignumber < 4547429999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4567250000000000L) && (orignumber < 4567459999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4658300000000000L) && (orignumber < 4658799999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4659010000000000L) && (orignumber < 4569509999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4844090000000000L) && (orignumber < 4844109999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4909600000000000L) && (orignumber < 4909799999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4921810000000000L) && (orignumber < 4921829999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4988240000000000L) && (orignumber < 4988249999999999L)) {


                            error = false;
                        }
                        // visa electron range
                        if ((orignumber > 4508750000000000L) && (orignumber < 4508759999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4844060000000000L) && (orignumber < 4844089999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4844110000000000L) && (orignumber < 4844559999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4917300000000000L) && (orignumber < 4917599999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4918800000000000L) && (orignumber < 4918809999999999L)) {


                            error = false;
                        }
                        // visa purchasing
                        if ((orignumber > 4484000000000000L) && (orignumber < 4486999999999999L)) {


                            error = false;
                        }
                        if ((orignumber > 4715000000000000L) && (orignumber < 4715999999999999L)) {


                            error = false;
                        }
                    }

                    // Visa Electron
                    if (ccdetails.getCCType().equals("VISA Electron")) {
                        if ((orignumber > 4508750000000000L) && (orignumber < 4508759999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4844060000000000L) && (orignumber < 4844089999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4844110000000000L) && (orignumber < 4844559999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4917300000000000L) && (orignumber < 4917599999999999L)) {
                            // valid number
                            error = true;
                        } else if ((orignumber > 4918800000000000L) && (orignumber < 4918809999999999L)) {
                            // valid number
                            error = true;
                        } else {

                            error = false;
                        }
                    }
                }
            }
            if (error == false) {
                message = "Invalid credit card number. Please correct.";
                LoadServicePage(request, response, message);
            } else {
                try {
                    if (login.isEmailAccountFlag() || login.isSmsAccountFlag()) {
                        //add entries to last_ten_used_entries table
                        if (login.isStoreMessages() && (ccdetails.getTrackingEmail().length() > 0 || ccdetails.getTrackingSMS().length() > 0)) {
                            addEntries(validSession.getAttribute("acc_id").toString(), environment, ccdetails.getTrackingEmail(), ccdetails.getTrackingSMS());
                        }



                        if (validSession == null) {
                        } else {

                            if (validSession.getAttribute("sTrackingEmail") != null) {
                                sTrackingEmail1 = (String[][]) validSession.getAttribute("sTrackingEmail");
                            }

                            if (validSession.getAttribute("sTrackingSMS") != null) {
                                sTrackingSMS1 = (String[][]) validSession.getAttribute("sTrackingSMS");
                            }

                            sTrackingEmail1[0][0] = ccdetails.getTrackingEmail();
                            sTrackingEmail1[0][1] = ccdetails.getEmailNotify();
                            sTrackingSMS1[0][0] = ccdetails.getTrackingSMS();
                            sTrackingSMS1[0][1] = ccdetails.getSMSNotify();

                            validSession.setAttribute("sTrackingEmail", sTrackingEmail1);
                            validSession.setAttribute("sTrackingSMS", sTrackingSMS1);

                        }
                    }

                    //Confirm Booking Form
                    strmResponse.println("<form name=\"ConfirmBookingForm\" method=\"post\" action=\"" + Constants.srvConfirmBooking + "\">");

                    strmResponse.println("<input type=\"hidden\" name=\"CCType\" value=\"" + ccdetails.getCCType() + "\">");
                    strmResponse.println("<input type=\"hidden\" name=\"CCNumber\" value=\"" + getMaskedCreditCardNo(ccdetails.getCardNo()) + "\">");
                    validSession.setAttribute("CreditCardNo", ccdetails.getCardNo());
                    strmResponse.println("<input type=\"hidden\" name=\"CCName\" value=\"" + ccdetails.getCCName() + "\">");
                    strmResponse.println("<input type=\"hidden\" name=\"sServices\" value=\"" + ccdetails.getServices() + "\">");
                    strmResponse.println("<input type=\"hidden\" name=\"sExpiryMonth\" value=\"" + ccdetails.getExpiryMonth() + "\">");
                    strmResponse.println("<input type=\"hidden\" name=\"sExpiryYear\" value=\"" + ccdetails.getExpiryYear() + "\">");
                    strmResponse.println("<input type=\"hidden\" name=\"sStartDay\" value=\"" + ccdetails.getStartDay() + "\">");
                    strmResponse.println("<input type=\"hidden\" name=\"sStartMonth\" vale=\"" + ccdetails.getStartMonth() + "\">");
                    strmResponse.println("<input type=\"hidden\" name=\"sIssueNo\" value=\"" + ccdetails.getIssueNo() + "\">");
                    strmResponse.println("<input type=\"hidden\" name=\"sSecurityCode\" value=\"" + ccdetails.getSecurityCode() + "\">");
                    strmResponse.println("<input type=\"hidden\" name=\"sAVSAddress1\" value=\"" + ccdetails.getsAVSAddress1() + "\">");
                    strmResponse.println("<input type=\"hidden\" name=\"sAVSAddress2\" value=\"" + ccdetails.getsAVSAddress2() + "\">");
                    strmResponse.println("<input type=\"hidden\" name=\"sAVSAddress3\" value=\"" + ccdetails.getsAVSAddress3() + "\">");
                    strmResponse.println("<input type=\"hidden\" name=\"sAVSAddress4\" value=\"" + ccdetails.getsAVSAddress4() + "\">");
                    strmResponse.println("<input type=\"hidden\" name=\"sPostcode\" value=\"" + ccdetails.getPostcode() + "\">");
                    strmResponse.println("<input type=\"hidden\" name=\"sCCEmail\" value=\"" + ccdetails.getCCEmail() + "\">");

                    strmResponse.println("<input type=hidden name=\"BDept\" value=\"" + booking.getBDept() + "\">");
                    strmResponse.println("<input type=hidden name=\"BCaller\" value=\"" + booking.getBCaller() + "\">");
                    strmResponse.println("<input type=hidden name=\"BPhone\" value=\"" + booking.getBPhone() + "\">");
                    strmResponse.println("<input type=hidden name=\"BRef\" value=\"" + booking.getBRef() + "\">");
                    strmResponse.println("<input type=hidden name=\"BHAWBNO\" value=\"" + booking.getBHAWBNO() + "\">");

                    strmResponse.println("<input type=hidden name=\"rdCollect\" value=\"" + booking.getRdCollect() + "\">");
                    strmResponse.println("<input type=hidden name=\"CollectDate\" value=\"" + booking.getCollectDate() + "\">");
                    strmResponse.println("<input type=hidden name=\"CollectHH\" value=\"" + booking.getCollectHH() + "\">");
                    strmResponse.println("<input type=hidden name=\"CollectMM\" value=\"" + booking.getCollectMM() + "\">");
                    strmResponse.println("<input type=hidden name=\"chkPickupBefore\" value=\"" + booking.getChkPickupBefore() + "\">");
                    strmResponse.println("<input type=hidden name=\"PickupBeforeHH\" value=\"" + booking.getPickupBeforeHH() + "\">");
                    strmResponse.println("<input type=hidden name=\"PickupBeforeMM\" value=\"" + booking.getPickupBeforeMM() + "\">");

                    strmResponse.println("<input type=hidden name=\"ProductType\" value=\"" + booking.getProductType() + "\">");
                    strmResponse.println("<input type=hidden name=\"TotalPieces\" value=\"" + booking.getTotalPieces() + "\">");
                    strmResponse.println("<input type=hidden name=\"TotalWeight\" value=\"" + booking.getTotalWeight() + "\">");
                    strmResponse.println("<input type=hidden name=\"TotalVolWeight\" value=\"" + booking.getTotalVolWeight() + "\">");
                    
//                      strmResponse.println("<input type=hidden name=\"ParcelNameId\" value=\"" + booking.getsParcelNameId() + "\">");
//            strmResponse.println("<input type=hidden name=\"ParcelName\" value=\"" + booking.getsParcelName() + "\">");
            
            strmResponse.println("<input type=hidden id=\"itemsRowCount\" name=\"itemsRowCount\" value=\"" + booking.getItemsRowCount() + "\">");
                    
                    strmResponse.println("</form>");

                    strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");
                    strmResponse.println("ConfirmBookingForm.submit();");
                    strmResponse.println("</script>");
                } catch (Exception ex) {
                    vShowErrorPage(response, ex);
                }
            }
        }
    }
    //Product Type", "Description", "Value","Weight", "No.of.items", "Length", "Width", "Height","Cubic","Indemnity"

    /**
     *
     * @param sSessionID
     * @return
     */
    protected String sSelectSQL(String sSessionID) {
//        return ("Select pro_noitems,pro_ptype, pro_iweight, pro_length,pro_width,pro_height,pro_ID from pro_product where pro_sessionid='" + sSessionID + "'");
        return ("Select pro_noitems, "
//                + "pro_iweight, "
                + "pro_length,pro_width,pro_height,pro_ID from pro_product where pro_sessionid='" + sSessionID + "'");
    }

    /**
     *
     * @param sSessionID
     * @return
     */
    protected String sSelectSQL1(String sSessionID) {
        return ("Select com_description,com_quantity,com_value,com_indemnity,com_ID from com_commodity where com_sessionid='" + sSessionID + "'");
    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException gets totalValue of all commodities for the
     * current session
     */
    protected void getTotalValue(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sSql = "select SUM(com_value) as dTotal from com_commodity where com_sessionid='" + (request.getSession(false).getAttribute("sessionid").toString()) + "'";

        Connection conDB = null;
        Statement stmRead;
        ResultSet rstRead;

        try {
            conDB = cOpenConnection();
            stmRead = conDB.createStatement();
            rstRead = stmRead.executeQuery(sSql);

            while (rstRead.next()) {
                sTotalValue = rstRead.getString("dTotal") == null ? "" : rstRead.getString("dTotal");
            }

            vCloseConnection(conDB);
        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException gets total value of the commodities for the
     * current session with indemnity
     */
    protected void getTotalValueWithIndemnity(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sSql = "select SUM(com_value) as dTotal from com_commodity where com_indemnity=true and com_sessionid='" + (request.getSession(false).getAttribute("sessionid").toString()) + "'";

        Connection conDB = null;
        Statement stmRead;
        ResultSet rstRead;

        try {
            conDB = cOpenConnection();
            stmRead = conDB.createStatement();
            rstRead = stmRead.executeQuery(sSql);

            while (rstRead.next()) {
                sTotalValueWithIndemnity = rstRead.getString("dTotal") == null ? "" : rstRead.getString("dTotal");
            }

            vCloseConnection(conDB);
        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try {
            service(request, response);
        } catch (Exception ex) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}