package citysprint.co.uk.NDI;

/*
 * ClocReportServlet.java
 *
 * Created on 29 March 2011, 10:45
 */
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.net.URL;
import java.net.URLConnection;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 *
 * @author Adnan
 * @version
 */
public class ClocReportServlet extends CommonClass {

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            String s_Action = "";
            s_Action = request.getParameter("sAction") == null ? "" : request.getParameter("sAction").toString();
            if (isLogin(request)) {

                initializeConfigValues(request, response);

                String acc_id = "";
                //Get Hidden name for checking various Reports
                acc_id = (String) request.getSession(false).getAttribute("acc_id");
                writeOutputLog("CLOC for Account No: " + acc_id
                        + " SessionId: " + request.getSession(false).getAttribute("sessionid").toString() + " Action: " + s_Action);

                generatePDF(request, response, request.getSession(false).getAttribute("sessionid").toString(), acc_id, s_Action);
            } else {
                writeOutputLog("Session expired redirecting to index page");
                response.sendRedirect("index.jsp");
            }
        } catch (Exception ex) {
            vShowErrorPageReporting(response, ex);
        }


    }

    /**
     *
     * @param request
     * @param response
     * @param session_ID
     * @param acc_id
     * @param s_Action
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     */
    public void generatePDF(HttpServletRequest request, HttpServletResponse response, String session_ID, String acc_id,
            String s_Action) throws ServletException, IOException, SQLException {

        Connection conCloc = null;
        Statement stmtCloc = null;
        ResultSet rstCloc = null;
        String sqlCLOCRpt = "";

        try {
            //Delete sessionid and userid in CLOC table

            //CLOC (Coded Location)
            String sqlCLOCDelete = "delete from cloc_report where cloc_sessionid='" + session_ID + "' and cloc_accno='" + acc_id + "' ";

            deleteCLOCDetails(sqlCLOCDelete, response);

            String chk_Hawbno = request.getParameter("chkid") == null ? "" : request.getParameter("chkid").toString();

            String[] chk_NewHawbno = chk_Hawbno.split(",");

            //Loop through
            for (int i = 0; i < chk_NewHawbno.length; i++) {

                //Add Hawbno parameters as String

                JobAuditInfo jAudit = getCLOCDetails(request, response, chk_NewHawbno[i]);

                if (!jAudit.isResult()) {
//                    writeOutputLog("===========================================no result found, cannot call parseCLOCResponse======================================================");
                    vShowErrorPage(response, jAudit.getSServiceAPIError());
                } else {
                    parseCLOCResponse (jAudit.getGetAuditDetailsArray(), session_ID, acc_id, response);
                }
            }

            String chk_HawbNoNew = chk_Hawbno.substring(0, chk_Hawbno.length() - 1);

            sqlCLOCRpt = "select cloc_jobno, cloc_output from cloc_report where cloc_accno = '" + acc_id + "' and cloc_sessionid='" + session_ID + "' and cloc_jobno in (" + chk_HawbNoNew + ");";

            writeOutputLog("sqlCLOCRpt == " + sqlCLOCRpt);
            ResourceBundle localBdl;
            localBdl = new PropertyResourceBundle(new FileInputStream(getLocalDirName() + "/Config.properties"));


            String PDFlocation = localBdl.getString("CommonPDFPath") + "/" + localBdl.getString("ReportsOutput") + "/";
            PDFlocation = PDFlocation.replace("/", "//");

            DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
            java.util.Date date = new java.util.Date();
            String dateStr = dateFormat.format(date);
            String PDFloginname = acc_id + "_" + dateStr;
            //PDF File Location

            String PDFfilename = PDFloginname + ".pdf";
            String PDFfilename1 = PDFlocation + PDFfilename;

            //Label Path
            String locationComPDF = localBdl.getString("CommonPDFPath") + "/" + localBdl.getString("ReportsFolder") + "/";
            locationComPDF = locationComPDF.replace("/", "//");

            String filenameLabel = localBdl.getString("PDFCLOCReprot");
            String fileLabel = locationComPDF + filenameLabel;


            //CitySprint Logo Path
            String filenameLogo = localBdl.getString("LogoPath");
            String filenameArrow = localBdl.getString("arrowImagePath");
            String fileLabelLogo = localBdl.getString("LabelLogo");

            conCloc = cOpenConnection();
            stmtCloc = conCloc.createStatement();

            if (sqlCLOCRpt.length() > 0) {
                rstCloc = stmtCloc.executeQuery(sqlCLOCRpt);
            }

            //Check PrintCLOC
            if (s_Action.equals("PrintCLOC")) {

                Map map = new HashMap();
                map.put("sqlconnection", conCloc);
                map.put("clocSessionId", session_ID);
                map.put("LogoPath", fileLabelLogo);
                map.put("accNo", acc_id);
                JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(rstCloc);
                JasperRunManager.runReportToPdfFile(fileLabel, PDFlocation + PDFfilename, map, resultSetDataSource);

            } //Check PrintInvoice

            if (s_Action.equals("QuikTrak") && request.getSession(false).getAttribute("acc_id") == null) {
                request.getSession(false).invalidate();
            }else {
                File f = new File(PDFlocation, PDFfilename);
                OutputStream outStream = response.getOutputStream();

                ServletOutputStream stream = response.getOutputStream();

                response.setContentType("application/pdf");
                response.setContentLength((int) f.length());
                response.setHeader("Content-Disposition", "attachment;filename=" + PDFfilename);

                byte[] buf = new byte[8192];
                FileInputStream inStream = new FileInputStream(f);
                int sizeRead = 0;
                while ((sizeRead = inStream.read(buf, 0, buf.length)) > 0) {

                    outStream.write(buf, 0, sizeRead);
                }

                inStream.close();
                outStream.close();
            }

        } catch (Exception e) {
            vShowErrorPageReporting(response, e);
        } finally {

            //Close ResultSet,Statement and Connection
            this.closeConnection(conCloc, stmtCloc, rstCloc);
            sqlCLOCRpt = "";
        }

    }

    /**
     *
     * @param req
     * @param res
     * @param NewHawbno
     * @return
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @SuppressWarnings("all")
    public JobAuditInfo getCLOCDetails(HttpServletRequest req, HttpServletResponse res, String jobNo) throws ServletException, IOException {

        System.out.println("Inside getAuditDetails=" + req + "  " + res + " " + jobNo);

        JobAuditInfo jAuditOutput = new JobAuditInfo();
        jAuditOutput.setResult(true);
        URL urlApi = null;


        java.util.Date d = new java.util.Date();
        String timeStamp = d.toString();

        StringBuffer clocDetails = new StringBuffer();

        //API Input
        String s_Action = req.getParameter("sAction") == null ? "" : req.getParameter("sAction").toString();

        clocDetails.append("CK=" + encStr(req.getSession(false).getAttribute("acc_id").toString()));                                      //Customer Key or Account No
        clocDetails.append("&PW=" + encStr(req.getSession(false).getAttribute("pwd").toString()));

        clocDetails.append("&JN=" + encStr(jobNo));                                  //JobNo

        /**
         * Following can be multiple ST (stop number) just hard coding for NDI which is 2
         */
        clocDetails.append("&ST=2");                                  //Stop No, For NDI it should be 2

//         URL urlApi = new URL("http://ijb.citysprint.co.uk/dmsndi/bin/ndi_Audit?"+clocDetails.toString());
        //URL urlApi = new URL("http://localhost:8080/ndi_Reports/AuditDetails.txt");
        if (environ.equals("PROD")) {
            urlApi = new URL(scriptsurl + bdl.getString("clocScript") + "?" + clocDetails.toString());
        }
//        else if (NewHawbno.equals("280545")) {
//            urlApi = new URL(bdl.getString("devhost") + "codedloc.txt");
//        }


        writeOutputLog("Input to AdHocCLOC:" + urlApi.toString());

        URLConnection con = urlApi.openConnection();
        con.setDoInput(true);
        con.setDoOutput(true);
        con.setUseCaches(false);

        BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String s;
        String outputLines = ""; //"start^JN27979^CL  JobBookedFor27979^zz";

        while ((s = br.readLine()) != null) {
            outputLines = outputLines + s;
        }

        writeOutputLog("Output from CLOC_API:" + outputLines);

        br.close();
        outputLines = outputLines + "^ZZ";
        jAuditOutput.setGetAuditDetailsArray(parseOutput(outputLines));

        if (jAuditOutput.getGetAuditDetailsArray() == null) {
            jAuditOutput.setResult(false);
        } else {
            for (int i = 0; i < jAuditOutput.getGetAuditDetailsArray().length - 1; i++) {
                if (jAuditOutput.getGetAuditDetailsArray()[i][0].equals("ER")) {
                    jAuditOutput.setSServiceAPIError(null);
                    jAuditOutput.setSServiceAPIError(jAuditOutput.getGetAuditDetailsArray()[i][1].substring(3, jAuditOutput.getGetAuditDetailsArray()[i][1].length()));
                    break;
                }
            }

            if (jAuditOutput.getSServiceAPIError().length() > 0) {
                jAuditOutput.setResult(false);
            }

        }
        return jAuditOutput;
    }

  
      /**
     *
     * @param outputLines
     * @return
     */
    @Override
    public String[][] parseOutput(String outputLines) {
        String temp = "";
        outputLines = outputLines.trim();
        outputLines = outputLines.replace('^', '\n');
        String stringArray[] = outputLines.split("\n");
        _arrayLength = stringArray.length;
        String parsedOutput[][] = null;
        parsedOutput = new String[_arrayLength + 1][2];
        for (int i = 0; i < _arrayLength; i++) {
            temp = stringArray[i];
            if (temp.length() > 0) {
                parsedOutput[i][0] = stringArray[i].substring(0, 2);
                parsedOutput[i][1] = stringArray[i].substring(2);
            } else {
                parsedOutput[i][0] = "ST";
                parsedOutput[i][1] = "START";
            }
        }
        return parsedOutput;
    }

    public void parseCLOCResponse(String[][] AuditDetailsArray, String session_ID, String acc_id, HttpServletResponse res) throws ServletException, IOException {

        String tempstr = "";
        String tag = "";
        String sSQL = "";
        String sqlCLOCReport = "";
        String emptyString = "''";
        String sJobNo = "";
        Connection conCloc = null;
        Statement stmtCloc = null;
//        JobAuditInfoParsed jAuditParsed = new JobAuditInfoParsed();

        sqlCLOCReport = "insert into cloc_report (cloc_sessionid ,cloc_accno ,cloc_jobno, cloc_output) values ('" + session_ID + "','" + acc_id +"', ";

        int i;
        int k = AuditDetailsArray.length;
        System.out.println(k);
        try {
            for (i = 0; i < AuditDetailsArray.length - 1; i++) {


                System.out.println(AuditDetailsArray.length);

                //tag declaration
                tag = AuditDetailsArray[i][0];
                //cloc_jobno
                if (tag.equals("JN")) {

                    tempstr = AuditDetailsArray[i][1].trim();
                    if (tempstr.length() > 0) {
                        sJobNo = tempstr;
                        sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";

                    } else {
                        sSQL = sSQL + emptyString + ",";
                    }
                }

                //Coded Location
                if (tag.equals("CL")) {

                    tempstr = AuditDetailsArray[i][1].trim();
                    if (tempstr.length() > 0) {
                        if (tempstr.length() > 20)
                            tempstr = tempstr.substring(0, 20);

                        sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" ;

                    } else {
                        sSQL = sSQL + emptyString ;
                    }
                }
                }
                sSQL = sqlCLOCReport + sSQL + ")";

                conCloc = cOpenConnection();
                stmtCloc = conCloc.createStatement();
                writeOutputLog("sSQL == " + sSQL);
                stmtCloc.executeUpdate(sSQL);
                sSQL = "";
                sqlCLOCReport = "";
           
        } catch (Exception ex) {
            vShowErrorPageReporting(res, ex);
        }finally {
            this.closeConnection(conCloc, stmtCloc, null);
        }
    }

    /**
     * 
     * @param connection
     * @param statement
     * @param resultSet
     */
    
    protected void closeConnection(Connection connection, Statement statement, ResultSet resultSet){
          if(resultSet != null)
          {
              try{ resultSet.close(); } catch(Exception e) { /* Do nothing */ }
          }

          if(statement != null)
          {
              try{ statement.close(); } catch(Exception e) { /* Do nothing */ }
          }

          if(connection != null)
          {
              try{ connection.commit(); connection.close(); } catch(Exception e) { /* Do nothing */ }
          }
    }
    /**
     *
     * @param sDelete
     * @param res
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    private void deleteCLOCDetails(String sDelete, HttpServletResponse res) throws ServletException, IOException {
        try {
            Connection conDelete = cOpenConnection();
            Statement stmtDelete = conDelete.createStatement();
            stmtDelete.executeUpdate(sDelete);
            System.out.println("Delete=" + sDelete);
            conDelete.close();
        } catch (Exception e) {
            vShowErrorPageReporting(res, e);
        }
    }

    /**
     *
     * @param filename1
     * @param out1
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     */
    public static void returnFile(String filename1, Writer out1) throws FileNotFoundException, IOException {
        Reader in = null;

        try {

            in = new BufferedReader(new FileReader(filename1));
            char[] buf = new char[4 * 1024];  // 4K char buffer
            int charsRead;
            while ((charsRead = in.read(buf)) != -1) {
                out1.write(buf, 0, charsRead);
            }


        } finally {
            if (in != null) {
                in.close();
            }

        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     * @return
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
