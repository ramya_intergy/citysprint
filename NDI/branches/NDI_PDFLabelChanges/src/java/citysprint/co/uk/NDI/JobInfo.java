package citysprint.co.uk.NDI;

/*
 * JobInfo.java
 *
 * Created on 13 August 2008, 18:38
 */

import java.beans.*;
import java.io.Serializable;

/**
 * @author ramyas
 */
public class JobInfo extends Object implements Serializable {
    /**
     * Holds value of property sJobNo.
     */
    private String sJobNo="";

    /**
     * Getter for property sJobNo.
     * @return Value of property sJobNo.
     */
    public String getSJobNo() {
        return this.sJobNo;
    }

    /**
     * Holds value of property sBHAWBNO.
     */
    private String sBHAWBNO="";

    /**
     * Getter for property sBHAWBNO.
     * @return Value of property sBHAWBNO.
     */
    public String getSBHAWBNO() {
        return this.sBHAWBNO;
    }

    /**
     * Holds value of property error.
     */
    private String error="";

    public void setError(String error) {
        this.error = error;
    }

    /**
     * Getter for property error.
     * @return Value of property error.
     */
    public String getError() {
        return this.error;
    }

   //parse array returned from Booking API 
    /**
     *
     * @param bookingArray - parses NDI_Booking API result
     * //^TS TRanssaction Stamp^JN Job Number^BP Price^GSGST/VAT^SCTotal Surcharge^DTPickup Delay
       //^XT Delivery Delay^PT Product Code^PV Product Value^LPProduct Liability^CMCommodity Code
       //^HN HAWB Number^DD Delivery Date^DM Delivery Time^PI Special Pickup Instructions^DI Delievery Instructions

     */
    public void parseBooking(String[][] bookingArray)
   {
       String tempstr = "";
       String tag = "";
      
      
       
       
       for(int i=0;i<bookingArray.length-1;i++)
       {
           tag=bookingArray[i][0];
           
           //HAWBNO
           if(tag.equals("HN"))
           {
               this.sBHAWBNO=bookingArray[i][1].trim();              
           }
           
           //Job Number
           if(tag.equals("JN"))
           {
               this.sJobNo=bookingArray[i][1].trim();  
           }
           
           if(tag.equals("ER"))
           {
               if(bookingArray[i][1].trim().length()>3)
               {
                   int iIndex = bookingArray[i][1].indexOf(" ");
                   this.error=bookingArray[i][1].trim().substring(iIndex, bookingArray[i][1].length()-1);
               }
               
           }
       }
       
       
   }
    
   
    
  
    
}
