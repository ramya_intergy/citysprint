package citysprint.co.uk.NDI;

/*
 * LoginInfo.java
 *
 * Created on 12 August 2008, 17:59
 */
import citysprint.co.uk.NDI.*;
import java.beans.*;
import java.io.Serializable;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;

/**
 * @author ramyas
 */
public class LoginInfo extends CommonClass implements Serializable {
    //Login API output
    private Vector deptVector = new Vector();         //Department
    private Vector refVector = new Vector();           //Reference
    private Vector yzVector = new Vector();            //Service Vector
    private Vector mvVector = new Vector();            //Maximum Value of Service Group
    private Vector ipVector = new Vector();            //Indemnity Percentage of Service Group
    private Vector inVector = new Vector();            //Indemnity Minimum Charge of Service Group
    private Vector ixVector = new Vector();            //Indemnity Maximum Value of Service Group
    
    // Following variable is added by Adnan on 3rd OCT 2013
    private Vector parcelVector = new Vector();         //Department
    
    private String ey_YourName = "Your Name";
    private String ey_Phone = "Phone";
    private String ey_Reference = "Your Reference";
    private String ey_Department = "Department";
    private String departmentFlag = "N";
    private String refFlag = "N";
    private boolean deptmandatory = false;
    private boolean refmandatory = false;
    private String cus_phone = "";
    private String creditCardFlag = "";
    private String sCubingFactor = "";
    private String defaultVATcode = "";
    private boolean EYFlag = false;
    private boolean referenceFlag = false;
    private boolean storeMessages = true;
    private boolean priceFlag = false;
    private boolean smsAccountFlag = false;
    private boolean emailAccountFlag = false;
    private boolean cv2SecurityCodeRequiredFlag = false;
    private boolean postcodeRequiredFlag = false;
    private boolean avsAddressMandatoryFlag = false;
    private double dHighValue2;
    private double dIndemnityMax1;
    private double dHighValue1;
    private double dIndemnityMax2;

    /**
     * Getter for property deptVector.
     * @return Value of property deptVector.
     */
    public Vector getDeptVector() {
        return this.deptVector;
    }

     /**
     * Getter for property parcelVector.
     * @return Value of property parcelVector.
     */
    public Vector getParcelVector() {
        return this.parcelVector;
    }
    
    /**
     * Getter for property refVector.
     * @return Value of property refVector.
     */
    public Vector getRefVector() {
        return this.refVector;
    }

    /**
     * Getter for property yzVector.
     * @return Value of property yzVector.
     */
    public Vector getYzVector() {
        return this.yzVector;
    }

    /**
     * Getter for property mvVector.
     * @return Value of property mvVector.
     */
    public Vector getMvVector() {
        return this.mvVector;
    }

    /**
     * Getter for property ipVector.
     * @return Value of property ipVector.
     */
    public Vector getIpVector() {
        return this.ipVector;
    }

    /**
     * Getter for property inVector.
     * @return Value of property inVector.
     */
    public Vector getInVector() {
        return this.inVector;
    }

    /**
     * Getter for property ixVector.
     * @return Value of property ixVector.
     */
    public Vector getIxVector() {
        return this.ixVector;
    }

    /**
     * Getter for property ey_YourName.
     * @return Value of property ey_YourName.
     */
    public String getEy_YourName() {
        return this.ey_YourName;
    }

    /**
     * Getter for property ey_Phone.
     * @return Value of property ey_Phone.
     */
    public String getEy_Phone() {
        return this.ey_Phone;
    }

    /**
     * Getter for property ey_Reference.
     * @return Value of property ey_Reference.
     */
    public String getEy_Reference() {
        return this.ey_Reference;
    }

    /**
     * Getter for property ey_Department.
     * @return Value of property ey_Department.
     */
    public String getEy_Department() {
        return this.ey_Department;
    }

    /**
     * Getter for property departmentFlag.
     * @return Value of property departmentFlag.
     */
    public String getDepartmentFlag() {
        return this.departmentFlag;
    }

    /**
     * Getter for property refFlag.
     * @return Value of property refFlag.
     */
    public String getRefFlag() {
        return this.refFlag;
    }

    /**
     * Getter for property EYFlag.
     * @return Value of property EYFlag.
     */
    public boolean isEYFlag() {
        return this.EYFlag;
    }

    /**
     * Getter for property referenceFlag.
     * @return Value of property referenceFlag.
     */
    public boolean isReferenceFlag() {
        return this.referenceFlag;
    }

    /**
     * Getter for property storeMessages.
     * @return Value of property storeMessages.
     */
    public boolean isStoreMessages() {
        return this.storeMessages;
    }

    /**
     * Getter for property priceFlag.
     * @return Value of property priceFlag.
     */
    public boolean isPriceFlag() {
        return this.priceFlag;
    }

    /**
     * Getter for property smsAccountFlag.
     * @return Value of property smsAccountFlag.
     */
    public boolean isSmsAccountFlag() {
        return this.smsAccountFlag;
    }

    /**
     * Getter for property emailAccountFlag.
     * @return Value of property emailAccountFlag.
     */
    public boolean isEmailAccountFlag() {
        return this.emailAccountFlag;
    }

    /**
     * Getter for property creditCardFlag.
     * @return Value of property creditCardFlag.
     */
    public String getCreditCardFlag() {
        return this.creditCardFlag;
    }

    /**
     * Getter for property cv2SecurityCodeRequiredFlag.
     * @return Value of property cv2SecurityCodeRequiredFlag.
     */
    public boolean isCv2SecurityCodeRequiredFlag() {
        return this.cv2SecurityCodeRequiredFlag;
    }

    /**
     * Getter for property postcodeRequiredFlag.
     * @return Value of property postcodeRequiredFlag.
     */
    public boolean isPostcodeRequiredFlag() {
        return this.postcodeRequiredFlag;
    }

    /**
     * Getter for property avsAddressMandatoryFlag.
     * @return Value of property avsAddressMandatoryFlag.
     */
    public boolean isAvsAddressMandatoryFlag() {
        return this.avsAddressMandatoryFlag;
    }

    /**
     * Getter for property dIndemnityMax1.
     * @return Value of property dIndemnityMax1.
     */
    public double getDIndemnityMax1() {
        return this.dIndemnityMax1;
    }

    /**
     * Getter for property dHighValue1.
     * @return Value of property dHighValue1.
     */
    public double getDHighValue1() {
        return this.dHighValue1;
    }

    /**
     * Getter for property dIndemnityMax2.
     * @return Value of property dIndemnityMax2.
     */
    public double getDIndemnityMax2() {
        return this.dIndemnityMax2;
    }

    /**
     * Getter for property dHighValue2.
     * @return Value of property dHighValue2.
     */
    public double getDHighValue2() {
        return this.dHighValue2;
    }

    /**
     * Getter for property sCubingFactor.
     * @return Value of property sCubingFactor.
     */
    public String getSCubingFactor() {
        return this.sCubingFactor;
    }

    /**
     * Getter for property defaultVATcode.
     * @return Value of property defaultVATcode.
     */
    public String getDefaultVATcode() {
        return this.defaultVATcode;
    }

    /**
     * Getter for property cus_phone.
     * @return Value of property cus_phone.
     */
    public String getCus_phone() {
        return this.cus_phone;
    }

    /**
     *
     * @param clientStr - received from NDI_Login API
     * This method parses the tags returned from API and inialises the appropriate fields
     * parse the login array and set all the flags.
    Customer Name-CC, Customer Phone-PH, Departments Name-DT, Department Flag-DB, Reference flag-RF, Weight Flag-WE,

    Service Group repeating set
    Service Group-YZ, Max Product Value-MV, Indemnity Percentage-IP, Indemnity Minimum Charge-IN, Indemnity Maximum Value-IX,
    Cubing Factor-CF,

    Credit Cards Flag-C2(N/F), CV2 Security Code Required-C3, Post Code required-C4,
    AVS Address Mand-C5, E & Y Flag-EY, Prompt for Dangerous Goods-DG, Default VAT Code-DV
     */
    public void parseLogin(String[][] clientStr, HttpServletRequest request) {
        java.lang.String clientConfigString;
        String tempstr = "";
        String tag = "";
        deptVector.clear();
        refVector.clear();
        yzVector.clear();
        inVector.clear();
        ixVector.clear();
        mvVector.clear();
        parcelVector.clear();

        for (int i = 0; i < clientStr.length - 1; i++) {
            tag = clientStr[i][0];

            if (tag.equals("CC")) {//                label_customer.setText( clientStr[i][1] );
            }
            if (tag.equals("PH")) {
                this.cus_phone = clientStr[i][1].trim();
            }

            //Tags for E&Y customer.
            if (tag.equals("L1")) {
                this.ey_YourName = clientStr[i][1];
            }
            if (tag.equals("L2")) {
                this.ey_Phone = clientStr[i][1];
            }
            if (tag.equals("L3")) {
                this.ey_Reference = clientStr[i][1];
            }
            if (tag.equals("L4")) {
                this.ey_Department = clientStr[i][1];
            }
            if (tag.equals("EY")) {
                tempstr = clientStr[i][1];

                if (tempstr.equals("Y")) {

                    this.EYFlag = true;

                }
            }

            if (tag.equals("DB")) {
                this.departmentFlag = clientStr[i][1];

            }

            if (departmentFlag.equals("Y") || departmentFlag.equals("R") || departmentFlag.equals("V")) {
                this.deptmandatory = true;
            }

            if (tag.equals("DT")) {
                tempstr = clientStr[i][1].trim();

                if (tempstr.length() > 0) {
                    //_Form1_page_jobEntry.choice_department.addItem(tempstr);
                    //addElement(tempstr);
                    try {
                        this.deptVector.addElement(tempstr);
                    } catch (Exception ex) {
                        String sMessage = ex.toString();
                        System.out.print(sMessage);
                    }

                }
            }

            if (tag.equals("RF")) {
                this.refFlag = clientStr[i][1];
                if (refFlag.equals("Y") || refFlag.equals("M") || refFlag.equals("F")) {
                    this.referenceFlag = true;
                } else {
                    //if (refFlag.equals("N"))
                    //   referenceFlag = false;
                    //else
                    this.referenceFlag = false;
                }
            }

            if (refFlag.equals("V") || refFlag.equals("Y") || refFlag.equals("F")) {
                refmandatory = true;
            }

            if (tag.equals("RN")) {
                tempstr = clientStr[i][1].trim();
                int un_len = tempstr.length();
                if (tempstr.length() > 0) {
                    this.refVector.addElement(tempstr);

                }
            }

            if (tag.equals("TM")) {
                tempstr = clientStr[i][1].trim();
                if (tempstr.equals("B")) {
                    this.smsAccountFlag = true;
                    this.emailAccountFlag = true;
                } else if (tempstr.equals("S")) {
                    this.smsAccountFlag = true;
                } else if (tempstr.equals("E")) {
                    this.emailAccountFlag = true;
                } else {
                    this.smsAccountFlag = false;
                    this.emailAccountFlag = false;
                }
            }

            if (tag.equals("SC")) {
                String storeMessagesConfig=request.getSession(false).getAttribute("storeMessagesConfig")==null?"":request.getSession(false).getAttribute("storeMessagesConfig").toString();
                if (storeMessagesConfig.equals("true")) {
                    tempstr = clientStr[i][1].trim();
                    if (tempstr.equals("Y")) {
                        this.storeMessages = true;
                    } else {
                        this.storeMessages = false;
                    }
                } else {
                    this.storeMessages = false;
                }
            }

            // Credit Cards flag
            if (tag.equals("C2")) {
                this.creditCardFlag = clientStr[i][1];
            //this.creditCardFlag="F";//ramya defaulted for testing.
            }
            // CV2 Security Code Required
            if (tag.equals("C3")) {
                tempstr = clientStr[i][1];
                if (tempstr.equals("Y")) {
                    this.cv2SecurityCodeRequiredFlag = true;
                } else {
                    this.cv2SecurityCodeRequiredFlag = false;
                }
            }

            // Postcode required
            if (tag.equals("C4")) {
                tempstr = clientStr[i][1];
                if (tempstr.equals("Y")) {
                    this.postcodeRequiredFlag = true;
                } else {
                    this.postcodeRequiredFlag = false;
                }
            }

            // AVS address mandatory
            if (tag.equals("C5")) {
                tempstr = clientStr[i][1];

                if(tempstr.equals("Y"))
                    this.avsAddressMandatoryFlag = true;
                else
                    this.avsAddressMandatoryFlag = false;

            }

            //Service Group Repeating Set
            if (tag.equals("YZ")) {
                tempstr = clientStr[i][1];
                this.yzVector.addElement(tempstr);
            }

            // Maximum Value
            if (tag.equals("MV")) {
                tempstr = clientStr[i][1];
                this.mvVector.addElement(tempstr);
            }

            //Indemnity Minimum
            if (tag.equals("IN")) {
                this.inVector.addElement(clientStr[i][1]);
            }

            //Indemnity Maximum
            if (tag.equals("IX")) {
                this.ixVector.addElement(clientStr[i][1]);
            }

            // Cubing Factor
            if (tag.equals("CF")) {
                tempstr = clientStr[i][1];
                this.sCubingFactor = tempstr;
            }

            // Default VAT code
            if (tag.equals("DV")) {
                this.defaultVATcode = clientStr[i][1];
            }

            //Display prices flag
            if (tag.equals("PR")) {
                tempstr = clientStr[i][1];
                if (tempstr.equals("Y")) {
                    this.priceFlag = true;
                } else {
                    this.priceFlag = false;
                }
            }

        }

        if (this.yzVector.size() > 0) {
            for (int j = 0; j < this.yzVector.size(); j++) {
                String sTemp = this.yzVector.elementAt(j).toString().substring(0, 1);

                if (sTemp.equals("3")) {
                    this.dIndemnityMax1 = Double.parseDouble(this.ixVector.elementAt(j).toString());
                    this.dHighValue1 = Double.parseDouble(this.mvVector.elementAt(j).toString());

                }

                if (sTemp.equals("4")) {
                    this.dIndemnityMax2 = Double.parseDouble(this.ixVector.elementAt(j).toString());
                    this.dHighValue2 = Double.parseDouble(this.mvVector.elementAt(j).toString());

                }
            }
        }

    }
}