package citysprint.co.uk.NDI;

/*
 * CommodityInfo.java
 *
 * Created on 13 August 2008, 10:57
 */

import java.beans.*;
import java.io.Serializable;
import java.util.Vector;

/**
 * @author ramyas
 */
public class ParcelInfo extends Object implements Serializable {
    
    //Commodity API output
    private Vector pnVector = new Vector();           //Parcel Name
    private Vector phVector = new Vector();           //Parcel Height
    private Vector plVector = new Vector();           //Parcel Length
    private Vector pdVector = new Vector();           //Parcel Depth
    
    /**
     * Getter for property ccVector .
     * @return Value of property ccVector .
     */
    public Vector getPnVector () {
        return this.pnVector ;
    }

     /**
     * Getter for property csVector .
     * @return Value of property csVector .
     */
    public Vector getPhVector () {
        return this.phVector ;
    }
    
    /**
     * Getter for property ifVector .
     * @return Value of property ifVector .
     */
    public Vector getPlVector () {
        return this.plVector ;
    }
    
    public Vector getPdVector () {
        return this.pdVector ;
    }
    
   //parse parcel details array
    /**
     *
     * @param parcelArray
     */
    public void parseParcel(String[][] parcelArray)
   {
       String tempstr = "";
       String tag = "";
       
      //Product API output  ^TS Transaction Stamp ^PT Parcel Name ^HE Heigth in CM ^PL Length in CM ^PD Depth in CM
       pnVector.clear();
       phVector.clear();
       plVector.clear();
       pdVector.clear();
       
       for(int i=0;i<parcelArray.length-1;i++)
       {
           tag=parcelArray[i][0];
           
           //Parcel Name
           if(tag.equals("PT"))
           {
               tempstr=parcelArray[i][1].trim();
               
               pnVector.addElement(tempstr);    
           }
           
                    
           //Parcel Heigth in CM
           if(tag.equals("HE"))
           {
               tempstr=parcelArray[i][1].trim();
               
               phVector.addElement(tempstr);
           }
           
           //Parcel Length
           if(tag.equals("PL"))
           {
               tempstr=parcelArray[i][1].trim();
               
               plVector.addElement(tempstr);
           }
           
           //Parcel Depth
           if(tag.equals("PD"))
           {
               tempstr=parcelArray[i][1].trim();
               
               pdVector.addElement(tempstr);
           }
           
      }

     
   }
    
}
