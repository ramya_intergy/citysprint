/**
 * EngineEnumType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class EngineEnumType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected EngineEnumType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _Singleline = "Singleline";
    public static final java.lang.String _Typedown = "Typedown";
    public static final java.lang.String _Verification = "Verification";
    public static final EngineEnumType Singleline = new EngineEnumType(_Singleline);
    public static final EngineEnumType Typedown = new EngineEnumType(_Typedown);
    public static final EngineEnumType Verification = new EngineEnumType(_Verification);
    public java.lang.String getValue() { return _value_;}
    public static EngineEnumType fromValue(java.lang.String value)
          throws java.lang.IllegalStateException {
        EngineEnumType enum1 = (EngineEnumType)
            _table_.get(value);
        if (enum1==null) throw new java.lang.IllegalStateException();
        return enum1;
    }
    public static EngineEnumType fromString(java.lang.String value)
          throws java.lang.IllegalStateException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
}
