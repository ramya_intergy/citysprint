/**
 * EngineIntensityType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class EngineIntensityType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected EngineIntensityType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _Exact = "Exact";
    public static final java.lang.String _Close = "Close";
    public static final java.lang.String _Extensive = "Extensive";
    public static final EngineIntensityType Exact = new EngineIntensityType(_Exact);
    public static final EngineIntensityType Close = new EngineIntensityType(_Close);
    public static final EngineIntensityType Extensive = new EngineIntensityType(_Extensive);
    public java.lang.String getValue() { return _value_;}
    public static EngineIntensityType fromValue(java.lang.String value)
          throws java.lang.IllegalStateException {
        EngineIntensityType enum1 = (EngineIntensityType)
            _table_.get(value);
        if (enum1==null) throw new java.lang.IllegalStateException();
        return enum1;
    }
    public static EngineIntensityType fromString(java.lang.String value)
          throws java.lang.IllegalStateException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
}
