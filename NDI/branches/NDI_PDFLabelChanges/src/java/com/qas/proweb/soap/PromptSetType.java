/**
 * PromptSetType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class PromptSetType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected PromptSetType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _OneLine = "OneLine";
    public static final java.lang.String _Default = "Default";
    public static final java.lang.String _Generic = "Generic";
    public static final java.lang.String _Optimal = "Optimal";
    public static final java.lang.String _Alternate = "Alternate";
    public static final java.lang.String _Alternate2 = "Alternate2";
    public static final java.lang.String _Alternate3 = "Alternate3";
    public static final PromptSetType OneLine = new PromptSetType(_OneLine);
    public static final PromptSetType Default = new PromptSetType(_Default);
    public static final PromptSetType Generic = new PromptSetType(_Generic);
    public static final PromptSetType Optimal = new PromptSetType(_Optimal);
    public static final PromptSetType Alternate = new PromptSetType(_Alternate);
    public static final PromptSetType Alternate2 = new PromptSetType(_Alternate2);
    public static final PromptSetType Alternate3 = new PromptSetType(_Alternate3);
    public java.lang.String getValue() { return _value_;}
    public static PromptSetType fromValue(java.lang.String value)
          throws java.lang.IllegalStateException {
        PromptSetType enum1 = (PromptSetType)
            _table_.get(value);
        if (enum1==null) throw new java.lang.IllegalStateException();
        return enum1;
    }
    public static PromptSetType fromString(java.lang.String value)
          throws java.lang.IllegalStateException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
}
