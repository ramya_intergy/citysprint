/**
 * QAConfigType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class QAConfigType  implements java.io.Serializable {
    private java.lang.String iniFile;
    private java.lang.String iniSection;

    public QAConfigType() {
    }

    public java.lang.String getIniFile() {
        return iniFile;
    }

    public void setIniFile(java.lang.String iniFile) {
        this.iniFile = iniFile;
    }

    public java.lang.String getIniSection() {
        return iniSection;
    }

    public void setIniSection(java.lang.String iniSection) {
        this.iniSection = iniSection;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QAConfigType)) return false;
        QAConfigType other = (QAConfigType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((iniFile==null && other.getIniFile()==null) || 
             (iniFile!=null &&
              iniFile.equals(other.getIniFile()))) &&
            ((iniSection==null && other.getIniSection()==null) || 
             (iniSection!=null &&
              iniSection.equals(other.getIniSection())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIniFile() != null) {
            _hashCode += getIniFile().hashCode();
        }
        if (getIniSection() != null) {
            _hashCode += getIniSection().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QAConfigType.class);

    static {
        org.apache.axis.description.FieldDesc field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("iniFile");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "IniFile"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("iniSection");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "IniSection"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
