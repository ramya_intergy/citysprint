/**
 * QAGetPromptSet.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class QAGetPromptSet  implements java.io.Serializable {
    private com.qas.proweb.soap.ISOType country;
    private com.qas.proweb.soap.EngineType engine;
    private com.qas.proweb.soap.PromptSetType promptSet;
    private com.qas.proweb.soap.QAConfigType QAConfig;

    public QAGetPromptSet() {
    }

    public com.qas.proweb.soap.ISOType getCountry() {
        return country;
    }

    public void setCountry(com.qas.proweb.soap.ISOType country) {
        this.country = country;
    }

    public com.qas.proweb.soap.EngineType getEngine() {
        return engine;
    }

    public void setEngine(com.qas.proweb.soap.EngineType engine) {
        this.engine = engine;
    }

    public com.qas.proweb.soap.PromptSetType getPromptSet() {
        return promptSet;
    }

    public void setPromptSet(com.qas.proweb.soap.PromptSetType promptSet) {
        this.promptSet = promptSet;
    }

    public com.qas.proweb.soap.QAConfigType getQAConfig() {
        return QAConfig;
    }

    public void setQAConfig(com.qas.proweb.soap.QAConfigType QAConfig) {
        this.QAConfig = QAConfig;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QAGetPromptSet)) return false;
        QAGetPromptSet other = (QAGetPromptSet) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((country==null && other.getCountry()==null) || 
             (country!=null &&
              country.equals(other.getCountry()))) &&
            ((engine==null && other.getEngine()==null) || 
             (engine!=null &&
              engine.equals(other.getEngine()))) &&
            ((promptSet==null && other.getPromptSet()==null) || 
             (promptSet!=null &&
              promptSet.equals(other.getPromptSet()))) &&
            ((QAConfig==null && other.getQAConfig()==null) || 
             (QAConfig!=null &&
              QAConfig.equals(other.getQAConfig())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getEngine() != null) {
            _hashCode += getEngine().hashCode();
        }
        if (getPromptSet() != null) {
            _hashCode += getPromptSet().hashCode();
        }
        if (getQAConfig() != null) {
            _hashCode += getQAConfig().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QAGetPromptSet.class);

    static {
        org.apache.axis.description.FieldDesc field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("country");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Country"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "ISOType"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("engine");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Engine"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "EngineType"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("promptSet");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "PromptSet"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "PromptSetType"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("QAConfig");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAConfig"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAConfigType"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
