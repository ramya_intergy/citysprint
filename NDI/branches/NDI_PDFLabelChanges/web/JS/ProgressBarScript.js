/*
File:	
	Progress.js

Usage: 	
	Set "backColor", "color", "font", "start". "full", and "width" values, call deplete(x), and restore(x) as needed. Both functions will accept negative numbers.

Required in main file:
	HTML:
		<span id="barArea"></span>

	Javascript (optional, include any, all, or none. Must be placed before this file is loaded.):
		var backColor = "darkblue"	//Background color, default = darkblue
		var color = "blue"		//Bar color, default = blue
		var font = "black"		//Font color, default = black
		var start = 0			//Starting value, default = 0
		var full = 100			//"Full" Value, default = 100
		var width = 250			//Full width of the bar, default = 250
	
Reserved names:
	Element ID's:
		oD, dD, bD
	Variables:
		backColor*, color*, font*, start*, full, width, v, value, OuterDIV, displayDIV, barDIV
			* = Nonessential past first execution
	Functions:
		deplete, restore, animate
*/

if(document.getElementById("barArea") != null){
alert(document.getElementById("barArea"));
	if(backColor == null){
		var backColor = "darkblue"
	}
	if(color == null){
		var color = "blue"
	}
	if(font == null){
		var font = "black"
	}
	if(start == null){
		var start = 0
	}
	if(full == null){
		var full = 100
	}
	if(width == null){
		var width = 250
	}
	document.getElementById("barArea").innerHTML = 
		'<div id="oD" style="width: 250; height: 15; border: solid;">'
		+ '<div id="bD" style="background-color:'
		+ color
		+ ';height:15;width:'
		+ start * (width / full)
		+ ';"></div>'
		+ '</div>'
		+ '<div id="dD" style="width: 250; height: 15; margin-top:-20; font-weight: bold; color: '
		+ font
		+ '; text-align: center;">'
		+ start
		+ '</div>'
	var outerDIV = document.getElementById("oD")
	var displayDIV = document.getElementById("dD")
	var barDIV = document.getElementById("bD")
	var value = start
	var v = start
	outerDIV.style.backgroundColor = backColor
	if(navigator.appName != "Microsoft Internet Explorer"){
		outerDIV.style.width = width
		displayDIV.style.width = width + 4
	}else{
		outerDIV.style.width = width + 8
		displayDIV.style.width = width + 8
		displayDIV.style.marginTop = -23
	}
}else{
	alert('Object "barArea" not found!');
}

function deplete(x){
	v = value
	if(x < 0){
		x = -x
		return restore(x)
	}
	value -= x
	if(value < 0){
		value = 0
	}
	return animate()
}

function restore(x){
	v = value
	if(x < 0){
		x = -x
		return deplete(x)
	}
	value -= -x
	if(value > full){
		value = full
	}
	return animate()
}

function animate(){
	if(v == value){
		return
	}else{
		if(v < value){
			v -= -1
		}
		if(v > value){
			v -= 1
		}
		l = v * (width / full)
		barDIV.style.width = l
		displayDIV.innerText = v
	}
	setTimeout("animate()",25)
}