<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page contentType="text/html" %>
<%@ page import="com.qas.proweb.servlet.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%
	// retrieve picklist display arrays
	String[] asMonikers = (String[]) request.getAttribute(Constants.PICK_MONIKERS);
	String[] asCommands = (String[]) request.getAttribute(Constants.PICK_FUNCTIONS);
	String[] asPickTexts = (String[]) request.getAttribute(Constants.PICK_TEXTS);
	String[] asPostcodes 	= (String[]) request.getAttribute(Constants.PICK_POSTCODES);
%>

<html>
	<head>
		<link rel="stylesheet" href="CSS/style.css" type="text/css" />
		<title>QuickAddress Pro Web - Address Capture</title>
		<script type="text/javascript">
			/* submitForm ensures that at least one picklist option button has been selected before submitting the form
			 * If no option buttons have been selected, the user is alerted, and the form is not submitted. 
			 * It also sets the command by looking at the destination hidden field. */
			function submitForm()
			{
				var aItems = document.getElementsByName("<%= Constants.MONIKER %>");
				for (var iIndex=0; iIndex < aItems.length; iIndex++)
				{
					if (aItems[iIndex].checked == true)
					{
						document.FormPicklist.<%= Constants.COMMAND %>.value = document.FormPrivateData.getElementsByTagName("input")[iIndex].value;
						return true;
					}
				}
				alert ("Please select an address first.");
				return false;
			}
			
			/* Hyperlink clicked: select the appropriate radio button, pick up private data and submit form */
			function submitCommand(iIndex)
			{
				document.getElementsByName("<%= Constants.MONIKER %>")[iIndex].checked = true;
				document.FormPicklist.<%= Constants.MONIKER %>.selectedIndex = iIndex;
				document.FormPicklist.<%= Constants.COMMAND %>.value = document.FormPrivateData.getElementsByTagName("input")[iIndex].value;
				document.FormPicklist.submit();
			}
			
			/* back button */
			function goBack()
			{
				document.FormPicklist.<%= Constants.COMMAND %>.value = "<%= FlatPromptEntry.NAME %>";
				document.FormPicklist.submit();
			}
		</script>
	</head>
	
	<body>
		<table width=800 class="nomargin">
			<tr>
				<th class="banner">
					<a href="index.htm"><h1>QuickAddress Pro Web</h1><h2>JSP Samples</h2></a>
				</th>
			</tr>
			<tr>
				<td class="nomargin">
					<h1>Address Capture</h1>
					<h3>Select your address</h3>
					
					<form action="QasController" method=post onSubmit="return submitForm();" name=FormPicklist>
						Select one of the following addresses that matched your selection.<br /><br />
						
						<table>
<%	for (int i=0; i < asMonikers.length; i++)
	{
%>							<tr>
								<td>
									<input name=<%= Constants.MONIKER %> type=radio value="<%= asMonikers[i] %>" />
									<a href="javascript:submitCommand('<%= i %>');"><%= asPickTexts[i]%></a>
								</td>
								<td width="15"></td>
								<td nowrap><%= asPostcodes[i]%></td>
<%	}
%>						</table>
						
						<br />
						The next step is to confirm your address.
						<br /><br />
						
						<input type="button" value="   < Back   " onClick="goBack();" />
						<input type="submit" value="    Next >   " />

						<input type="hidden" name="<%= Constants.DATA_ID %>" value="<%= request.getAttribute(Constants.DATA_ID) %>" />
						<input type="hidden" name="<%= Constants.COUNTRY_NAME %>" value="<%= request.getAttribute(Constants.COUNTRY_NAME) %>" />
						<input type="hidden" name="<%= Constants.PROMPT_SET %>" value="<%= request.getAttribute(Constants.PROMPT_SET) %>" />
						<input type="hidden" name="<%= Constants.PICKLIST_MONIKER %>" value="<%= request.getAttribute(Constants.PICKLIST_MONIKER) %>" />
						<input type="hidden" name="<%= Constants.COMMAND %>" value="<%= FlatRefineAddress.NAME %>" />
						<input type="hidden" name="<%= Constants.ROUTE %>" value="<%= Constants.ROUTE_NORMAL %>" />
<%	// set up hidden fields for UserInput array
	String[] asUserInput = (String[]) request.getAttribute(Constants.USER_INPUT);
	for (int i=0; i < asUserInput.length; i++)
	{	// we need to HTML escape special characters %>
						<input type="hidden" name="<%= Constants.USER_INPUT %>" value="<%= StringEscapeUtils.escapeHtml(asUserInput[i]) %>" />
<%	}
%>					</form>
					
					<br/><br/><hr/><br/>
					<a href="index.htm">Click here to return to the JSP Samples home page.</a>
				</td>
			</tr>
		</table>

		<form name="FormPrivateData">
<%	// One of these values will be picked up and sent by client JavaScript to the server
	for (int i=0; i < asCommands.length; i++)
	{
%>			<input type="hidden" name="<%= Constants.COMMAND %>" value="<%= asCommands[i] %>" />
<%	}
%>		</form>
	</body>
</html>
