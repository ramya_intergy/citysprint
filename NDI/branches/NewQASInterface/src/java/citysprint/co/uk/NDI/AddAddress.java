package citysprint.co.uk.NDI;

/*
 * AddAddress.java
 *
 * Created on 27 July 2007, 09:35
 */

import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.sql.DataSource;
import java.sql.*; 
//import javax.xml.ws.WebServiceRef;
import com.qas.proweb.servlet.*;
/**
 *
 * @author ramyas
 * @version
 */
public class AddAddress extends CommonClass {

        
    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");
        String sId="";

        if (isLogin(request)) {
            String acc_id = request.getSession(false).getAttribute("acc_id").toString();

            //initialize config values
            initializeConfigValues(request, response);

            //read session values
            //getSessionValues(request);


            sId = request.getParameter("sId") == null ? "" : request.getParameter("sId").toString();
            
            String sAction = request.getParameter(Constants.SACTION);

            if (!sId.equals("") && (sAction == null || sAction.equals(""))) {
                sAction = "Edit";
            }

            if (sId.equals("") && (sAction == null || sAction.equals(""))) {
                sAction = "Add";
            }

            if (sAction.equals("display")) {
                displayAddress(request, response, sId);
            } else if (sAction.equals("Add")) //call save address
            {
                saveAddress(request, response, acc_id);
            } else if (sAction.equals("Edit")) {
                updateAddress(request, response, acc_id, sId);
            } else if (sAction.equals("Delete")) {
                deleteAddress(request, response, sId);
            }

        }else {
                writeOutputLog("Session expired redirecting to index page");
                response.sendRedirect("index.jsp");
            }
        
     }
    
    /**
     *
     * @param request
     * @param response
     * @param acc_id
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void saveAddress(HttpServletRequest request, HttpServletResponse response,String acc_id)throws ServletException, IOException
    {
        
        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");
        //retrieve all the fields to be stored on the adddress book.
        String sCompany=sReplaceChars(request.getParameter("sCompanyName")==null?"":request.getParameter("sCompanyName").toString(),"'","''");
        String sContact=sReplaceChars(request.getParameter("sContactName")==null?"":request.getParameter("sContactName").toString(),"'","''");
        String sAddress1=sReplaceChars(request.getParameter("sAddress1")==null?"":request.getParameter("sAddress1").toString(),"'","''");
        String sAddress2=sReplaceChars(request.getParameter("sAddress2")==null?"":request.getParameter("sAddress2").toString(),"'","''");
        String sAddress3=sReplaceChars(request.getParameter("sAddress3")==null?"":request.getParameter("sAddress3").toString(),"'","''");
        String sPhone=sReplaceChars(request.getParameter("sPhone")==null?"":request.getParameter("sPhone").toString(),"'","''");
        String sTown=sReplaceChars(request.getParameter("sTown")==null?"":request.getParameter("sTown").toString(),"'","''");
        String sCounty=sReplaceChars(request.getParameter("sCounty")==null?"":request.getParameter("sCounty").toString(),"'","''");
        String sInstruct=sReplaceChars(request.getParameter("sInstruct")==null?"":request.getParameter("sInstruct").toString(),"'","''");
        String sPostcode=sReplaceChars(request.getParameter("sPostcode")==null?"":request.getParameter("sPostcode").toString(),"'","''");
        String sCountry=sReplaceChars(request.getParameter("sCountry")==null?"":request.getParameter("sCountry").toString(),"'","''");
        String sClosesByHH=sReplaceChars(request.getParameter("sClosesByHH")==null?"":request.getParameter("sClosesByHH").toString(),"'","''");
        String sClosesByMM=sReplaceChars(request.getParameter("sClosesByMM")==null?"":request.getParameter("sClosesByMM").toString(),"'","''");
        String sLatitude=sReplaceChars(request.getParameter("sLatitude")==null?"":request.getParameter("sLatitude").toString(),"'","''");
        String sLongitude=sReplaceChars(request.getParameter("sLongitude")==null?"":request.getParameter("sLongitude").toString(),"'","''");
        String sType = request.getParameter(Constants.STYPE) == null ? "CD" : request.getParameter(Constants.STYPE).toString();
        String sBookingType = request.getParameter("sBookingType") == null ? "" : request.getParameter("sBookingType").toString();
        
        String sResidentialFlag = request.getParameter("sResidentialFlag") == null ? "" : request.getParameter("sResidentialFlag").toString();
        
        String sInsert = "";
        String sMessage = "";
        
       
        if(sType.equals("C"))
        {
            sInsert="Insert into address (owned_by,environment,collection,delivery,server,company,contact,address1,address2,address3,phone,town,county,postcode,country,instructions,closesat,latitude,longitude, residential_flag)" +
                "values ('"+acc_id+"','"+environment+"',1,0,'"+host+"','"+sCompany+"','"+sContact+"','"+sAddress1+"','"+sAddress2+"','"+sAddress3+"','"+sPhone+"','"+sTown+"','"+sCounty+"','"+sPostcode+"','"+sCountry+"','"+sInstruct+"','"+sClosesByHH+":"+sClosesByMM+"','"+sLatitude+"','"+sLongitude+"','"+sResidentialFlag+"')";
        }
        if(sType.equals("D"))
        {
            sInsert="Insert into address (owned_by,environment,collection,delivery,server,company,contact,address1,address2,address3,phone,town,county,postcode,country,instructions,closesat,latitude,longitude, residential_flag)" +
                "values ('"+acc_id+"','"+environment+"',0,1,'"+host+"','"+sCompany+"','"+sContact+"','"+sAddress1+"','"+sAddress2+"','"+sAddress3+"','"+sPhone+"','"+sTown+"','"+sCounty+"','"+sPostcode+"','"+sCountry+"','"+sInstruct+"','"+sClosesByHH+":"+sClosesByMM+"','"+sLatitude+"','"+sLongitude+"','"+sResidentialFlag+"')";
        }
        if(sType.equals("CD"))
        {
            sInsert="Insert into address (owned_by,environment,collection,delivery,server,company,contact,address1,address2,address3,phone,town,county,postcode,country,instructions,closesat,latitude,longitude, residential_flag)" +
                "values ('"+acc_id+"','"+environment+"',1,1,'"+host+"','"+sCompany+"','"+sContact+"','"+sAddress1+"','"+sAddress2+"','"+sAddress3+"','"+sPhone+"','"+sTown+"','"+sCounty+"','"+sPostcode+"','"+sCountry+"','"+sInstruct+"','"+sClosesByHH+":"+sClosesByMM+"','"+sLatitude+"','"+sLongitude+"','"+sResidentialFlag+"')";
        }
        
        Connection conDB = null;
        Statement stmInsert;
        String sCompanyName="";
        String sId="odd";
        
        try
        {
            conDB = cOpenConnection();
            stmInsert= conDB.createStatement();
            stmInsert.executeUpdate(sInsert);	
            sMessage="Address Book Entry added successfully";
            vCloseConnection(conDB);
        }
        catch(Exception ex)
        {
               //strmResponse.println("<html><body><table><tr><td><FONT SIZE=\"-0\" face=\"Verdana,Tahoma,Arial,Helvetica,sans-serif\" COLOR=\"\" + sFontColor+ \">"+ex.toString()+"</font></td></tr></table></boby></html>");
               sMessage=ex.toString();
        }

        strmResponse.println("<html>");
        strmResponse.println("<script language=javascript>");
       
        strmResponse.println("opener.AddressBookPopup('"+sBookingType+"','"+sMessage+"')");
        //strmResponse.println("window.close();");
        strmResponse.println("</script>");
        strmResponse.println("<html>");
    }
    
    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void displayAddress(HttpServletRequest request, HttpServletResponse response, String sId)throws ServletException, IOException
    {
        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");
               
        String sContact = "", sPhone = "", sCompany = "", sAddress = "", sTown = "", sMobile = "", sPostcode = "", sCountry = "", sEmail = "", sAddress2 = "", sAddress3="", sCounty = "", sInstruct = "";
        String sLatitude = "",sLongitude="", sResidentialFlag = "";
        String sql="Select * from address where ID="+sId+"";
        String sBookingType = request.getParameter("sBookingType") == null ? "" : request.getParameter("sBookingType").toString();

        Connection conDB = null;
        Statement stmRead;
        ResultSet rstRead;
        String[] asClosesBy = null;

        String sClosesByHH="",sClosesByMM="",sClosesBy="";

        try
        {
            conDB = cOpenConnection();
            stmRead = conDB.createStatement();
            rstRead = stmRead.executeQuery(sql);
            
            if(rstRead.next())
            {
//                sContact = sReplaceChars(rstRead.getString("contact"),"'","&apos;");
//                sPhone = sReplaceChars(rstRead.getString("phone"),"'","&apos;");
//                sCompany = sReplaceChars(rstRead.getString("company"),"'","&apos;");
//                sAddress = sReplaceChars(rstRead.getString("address1"),"'","&apos;");
//                sAddress2 = sReplaceChars(rstRead.getString("address2"),"'","&apos;");
//                sAddress3 = sReplaceChars(rstRead.getString("address3"),"'","&apos;");
//                sTown = sReplaceChars(rstRead.getString("town"),"'","&apos;");
//                sCounty= sReplaceChars(rstRead.getString("county"),"'","&apos;");
//                sInstruct= sReplaceChars(rstRead.getString("instructions"),"'","&apos;");
//                sPostcode = sReplaceChars(rstRead.getString("postcode"),"'","&apos;");
//                sCountry = sReplaceChars(rstRead.getString("country"),"'","&apos;");
//                sLatitude = sReplaceChars(rstRead.getString("latitude"),"'","&apos;");
//                sLongitude = sReplaceChars(rstRead.getString("longitude"),"'","&apos;");
//               sClosesBy=sReplaceChars(rstRead.getString("closesat"),"'","''");

                sContact = rstRead.getString("contact");
                sPhone = rstRead.getString("phone");
                sCompany = rstRead.getString("company");
                sAddress = rstRead.getString("address1");
                sAddress2 = rstRead.getString("address2");
                sAddress3 = rstRead.getString("address3");
                sTown = rstRead.getString("town");
                sCounty=rstRead.getString("county");
                sInstruct=rstRead.getString("instructions");
                sPostcode = rstRead.getString("postcode");
                sCountry = rstRead.getString("country");
                sLatitude = rstRead.getString("latitude");
                sLongitude = rstRead.getString("longitude");
                
                sResidentialFlag = rstRead.getString("residential_flag");
                        
                sClosesBy=rstRead.getString("closesat");
                asClosesBy=sClosesBy.split(":");

                if(sLatitude==null)
                    sLatitude="";

                if(sLongitude==null)
                    sLongitude="";
    
                if(asClosesBy.length>0)
                {
                    sClosesByHH=asClosesBy[0]==""?"17":asClosesBy[0];
                    sClosesByMM=asClosesBy[1]==""?"00":asClosesBy[1];
                }

                    strmResponse.println("<html>");
                    strmResponse.println("<head></head>");
                    strmResponse.println("<body>");

                    if(sBookingType.equals("C"))
                    {
                        strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\" type=\"text/javascript\">");

                        //strmResponse.println("function displayAddress(){");
                        strmResponse.println("var txtCContact = window.opener.document.getElementById(\"txtCContact\");");
                        strmResponse.println("var txtCPhone = window.opener.document.getElementById(\"txtCPhone\");");
                        strmResponse.println("var txtCCompany = window.opener.document.getElementById(\"txtCCompany\");");
                        strmResponse.println("var txtCAddress1 = window.opener.document.getElementById(\"txtCAddress1\");");
                        strmResponse.println("var txtCAddress2 = window.opener.document.getElementById(\"txtCAddress2\");");
                        strmResponse.println("var txtCAddress3 = window.opener.document.getElementById(\"txtCAddress3\");");
                        strmResponse.println("var txtCTown = window.opener.document.getElementById(\"txtCTown\");");
                        strmResponse.println("var txtCCounty = window.opener.document.getElementById(\"txtCCounty\");");
                        strmResponse.println("var txtCInstruct = window.opener.document.getElementById(\"txtCInstruct\");");
                        strmResponse.println("var txtCPostcode = window.opener.document.getElementById(\"txtCPostcode\");");
                        strmResponse.println("var txtCLatitude = window.opener.document.getElementById(\"txtCLatitude\");");
                        //strmResponse.println("alert(txtCLatitude);");
                        strmResponse.println("var txtCLongitude = window.opener.document.getElementById(\"txtCLongitude\");");
                        //strmResponse.println("var selCCountry = window.opener.document.getElementById(\"selCCountry\");");
                        strmResponse.println("var selHHPickupBefore = window.opener.document.getElementById(\"selHHPickupBefore\");");
                        strmResponse.println("var selMMPickupBefore = window.opener.document.getElementById(\"selMMPickupBefore\");");
                        
                        strmResponse.println("var selCResidentialFlag = window.opener.document.getElementById(\"chkCResidentialFlag\");");
//                        strmResponse.println("selCResidentialFlag.value = \"" + sResidentialFlag + "\";");
//                        strmResponse.println( " if (Y == \"" + sResidentialFlag + "\" || y == \"" + sResidentialFlag + "\"){");
                        strmResponse.println( " if ('Y' == '" + sResidentialFlag + "' || 'y' == '" + sResidentialFlag + "'){");
                        strmResponse.println( " selCResidentialFlag.checked= true; " );
                        strmResponse.println( " } else { " );
                        strmResponse.println( " selCResidentialFlag.checked= false ; }" );
//                         strmResponse.println(" "
//                                    + "alert (\"Collection Residential Flag = '" +  sResidentialFlag + "'\");");
//                                strmResponse.println( " for (var indx=0; indx < selCResidentialFlag.options.length; indx++){");
////                                 strmResponse.println(" "
////                                    + "alert (\"selCResidentialFlag.options[indx].value\");");
//                                 strmResponse.println( " if (selCResidentialFlag.options[indx].value == \"" + sResidentialFlag + "\"){");
//                                 strmResponse.println( " selCResidentialFlag.options[indx].selected = true; " );
//                                 strmResponse.println( " break; ");
//                                 strmResponse.println( "}" );
//                                 strmResponse.println( "}");
                                 
//                         strmResponse.println(" "
//                                 + "alert ('Collection Residential Flag = '" +  sResidentialFlag + ");"
//                                 + "for (var indx=0; indx < selCResidentialFlag.options.length; indx++){"
//                                 + " if (selCResidentialFlag.options[indx].value == " + sResidentialFlag + "){"
//                                 + " selCResidentialFlag.options[indx].selected = true;"
//                                 + " return;"
//                                 + "}"
//                                 + "}");
                        
                        strmResponse.println("txtCContact.value = \"" + sContact + "\";");
                        strmResponse.println("txtCPhone.value = \"" + sPhone + "\";");
                        strmResponse.println("txtCCompany.value = \"" + sCompany + "\";");
                        strmResponse.println("txtCAddress1.value = \"" + sAddress + "\";");
                        strmResponse.println("txtCAddress2.value = \"" + sAddress2 + "\";");
                        strmResponse.println("txtCAddress3.value = \"" + sAddress3 + "\";");
                        strmResponse.println("txtCTown.value = \"" + sTown + "\";");
                        strmResponse.println("txtCCounty.value = \"" + sCounty + "\";");
                        strmResponse.println("txtCInstruct.value = \"" + sInstruct + "\";");
                        strmResponse.println("txtCPostcode.value = \"" + sPostcode + "\";");
                        strmResponse.println("txtCLatitude.value = \"" + sLatitude + "\";");
                        strmResponse.println("txtCLongitude.value = \"" + sLongitude + "\";");
                        //strmResponse.println("selCCountry.value = '" + sCountry + "';");
                        strmResponse.println("txtCInstruct.style.fontStyle='normal';");
                        strmResponse.println("selHHPickupBefore.value = '"+ sClosesByHH + "'");
                        strmResponse.println("selMMPickupBefore.value = '"+ sClosesByMM + "'");

                        if(sCountry.equals(Constants.COUNTRY_CODE) && !sLatitude.equals("") && !sLongitude.equals(""))
                        {
                            strmResponse.println("txtCTown.disabled=true;");
                            strmResponse.println("txtCPostcode.disabled=true;");
                            

                        }
                        else
                        {
                            strmResponse.println("txtCTown.disabled=false;");
                            strmResponse.println("txtCPostcode.disabled=false;");
                            

                        }

                        strmResponse.println("window.close();");
                        //strmResponse.println("}");
                        strmResponse.println("</script>");
                    }
                    else
                    {
                        strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\" type=\"text/javascript\">");
                        //strmResponse.println("function displayAddress(){");
                        strmResponse.println("var txtDContact = window.opener.document.getElementById(\"txtDContact\");");
                        strmResponse.println("var txtDPhone = window.opener.document.getElementById(\"txtDPhone\");");
                        strmResponse.println("var txtDCompany = window.opener.document.getElementById(\"txtDCompany\");");
                        strmResponse.println("var txtDAddress1 = window.opener.document.getElementById(\"txtDAddress1\");");
                        strmResponse.println("var txtDAddress2 = window.opener.document.getElementById(\"txtDAddress2\");");
                        strmResponse.println("var txtDAddress3 = window.opener.document.getElementById(\"txtDAddress3\");");
                        strmResponse.println("var txtDTown = window.opener.document.getElementById(\"txtDTown\");");
                        strmResponse.println("var txtDCounty = window.opener.document.getElementById(\"txtDCounty\");");
                        strmResponse.println("var txtDInstruct = window.opener.document.getElementById(\"txtDInstruct\");");
                        strmResponse.println("var txtDPostcode = window.opener.document.getElementById(\"txtDPostcode\");");
                        strmResponse.println("var selDCountry = window.opener.document.getElementById(\"selDCountry\");");
                        strmResponse.println("var lblDPostcode = window.opener.document.getElementById(\"lblDPostcode\");");
                        strmResponse.println("var txtDLatitude = window.opener.document.getElementById(\"txtDLatitude\");");
                        strmResponse.println("var txtDLongitude = window.opener.document.getElementById(\"txtDLongitude\");");

                        strmResponse.println("txtDContact.value = \"" + sContact + "\";");
                        strmResponse.println("txtDPhone.value = \"" + sPhone + "\";");
                        strmResponse.println("txtDCompany.value = \"" + sCompany + "\";");
                        strmResponse.println("txtDAddress1.value = \"" + sAddress + "\";");
                        strmResponse.println("txtDAddress2.value = \"" + sAddress2 + "\";");
                        strmResponse.println("txtDAddress3.value = \"" + sAddress3 + "\";");
                        strmResponse.println("txtDTown.value = \"" + sTown + "\";");
                        strmResponse.println("txtDCounty.value = \"" + sCounty + "\";");
                        strmResponse.println("txtDInstruct.value = \"" + sInstruct + "\";");
                        strmResponse.println("txtDPostcode.value = \"" + sPostcode + "\";");
                        strmResponse.println("txtDLatitude.value = \"" + sLatitude + "\";");
                        strmResponse.println("txtDLongitude.value = \"" + sLongitude + "\";");
                        strmResponse.println("selDCountry.value = \"" + sCountry + "\";");

                        if(sCountry.equals(Constants.COUNTRY_CODE) && !sLatitude.equals("") && !sLongitude.equals(""))
                        {
                            strmResponse.println("txtDTown.disabled=true;");
                            strmResponse.println("txtDPostcode.disabled=true;");
                            strmResponse.println("selDCountry.disabled=true;");
                            strmResponse.println("lblDPostcode.innerHTML=\"Postcode *\";");
                        }
                        else
                        {
                            strmResponse.println("txtDTown.disabled=false;");
                            strmResponse.println("txtDPostcode.disabled=false;");
                            strmResponse.println("selDCountry.disabled=false;");

                            if(sCountry.equals(Constants.COUNTRY_CODE))
                                strmResponse.println("lblDPostcode.innerHTML=\"Postcode *\";");
                            else
                                strmResponse.println("lblDPostcode.innerHTML=\"Postcode\";");
                        }

                        strmResponse.println("var selDResidentialFlag = window.opener.document.getElementById(\"chkDResidentialFlag\");");
                        
//                        strmResponse.println("selCResidentialFlag.value = \"" + sResidentialFlag + "\";");
                        strmResponse.println( " if ('Y' == '" + sResidentialFlag + "' || 'y' == '" + sResidentialFlag + "'){");
                        strmResponse.println( " selDResidentialFlag.checked= true; " );
                        strmResponse.println( " } else { " );
                        strmResponse.println( " selDResidentialFlag.checked= false ; }" );
//                         strmResponse.println(" "
//                                    + "alert (\"Delivery Residential Flag = '" +  sResidentialFlag + "'\");");
//                                strmResponse.println( " for (var indx=0; indx < selDResidentialFlag.options.length; indx++){");
////                                strmResponse.println(" "
////                                    + "alert (\"selDResidentialFlag.options[indx].value\");");
//                                 strmResponse.println( " if (selDResidentialFlag.options[indx].value == \"" + sResidentialFlag + "\"){");
//                                 strmResponse.println( " selDResidentialFlag.options[indx].selected = true; " );
//                                 strmResponse.println( " break; ");
//                                 strmResponse.println( "}" );
//                                 strmResponse.println( "}");
//                        strmResponse.println("selDResidentialFlag.value = \"" + sResidentialFlag + "\";");
        
                        strmResponse.println("txtDInstruct.style.fontStyle='normal';");
                        strmResponse.println("window.close();");
                        //strmResponse.println("}");
                        strmResponse.println("</script>");
                    }

                            
                    strmResponse.println("</body>");
                    strmResponse.println("</html>");
            }

             vCloseConnection(conDB);
        }
        catch(Exception ex)
        {
             vShowErrorPage(response,ex);
        }
    }
    
    /**
     *
     * @param request
     * @param response
     * @param acc_id
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void updateAddress(HttpServletRequest request, HttpServletResponse response,String acc_id, String sId)throws ServletException, IOException
    {
        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");

        //retrieve all the fields to be stored on the adddress book.
        String sCompany=sReplaceChars(request.getParameter("sCompanyName")==null?"":request.getParameter("sCompanyName").toString(),"'","''");
        String sContact=sReplaceChars(request.getParameter("sContactName")==null?"":request.getParameter("sContactName").toString(),"'","''");
        String sAddress1=sReplaceChars(request.getParameter("sAddress1")==null?"":request.getParameter("sAddress1").toString(),"'","''");
        String sAddress2=sReplaceChars(request.getParameter("sAddress2")==null?"":request.getParameter("sAddress2").toString(),"'","''");
        String sAddress3=sReplaceChars(request.getParameter("sAddress3")==null?"":request.getParameter("sAddress3").toString(),"'","''");
        String sPhone=sReplaceChars(request.getParameter("sPhone")==null?"":request.getParameter("sPhone").toString(),"'","''");
        String sTown=sReplaceChars(request.getParameter("sTown")==null?"":request.getParameter("sTown").toString(),"'","''");
        String sCounty=sReplaceChars(request.getParameter("sCounty")==null?"":request.getParameter("sCounty").toString(),"'","''");
        String sInstruct=sReplaceChars(request.getParameter("sInstruct")==null?"":request.getParameter("sInstruct").toString(),"'","''");
        String sPostcode=sReplaceChars(request.getParameter("sPostcode")==null?"":request.getParameter("sPostcode").toString(),"'","''");
        String sCountry=sReplaceChars(request.getParameter("sCountry")==null?"":request.getParameter("sCountry").toString(),"'","''");
        String sClosesByHH=sReplaceChars(request.getParameter("sClosesByHH")==null?"":request.getParameter("sClosesByHH").toString(),"'","''");
        String sClosesByMM=sReplaceChars(request.getParameter("sClosesByMM")==null?"":request.getParameter("sClosesByMM").toString(),"'","''");
        String sLatitude=sReplaceChars(request.getParameter("sLatitude")==null?"":request.getParameter("sLatitude").toString(),"'","''");
        String sLongitude=sReplaceChars(request.getParameter("sLongitude")==null?"":request.getParameter("sLongitude").toString(),"'","''");
        String sType = request.getParameter(Constants.STYPE) == null ? "CD" : request.getParameter(Constants.STYPE).toString();
        String sBookingType = request.getParameter("sBookingType") == null ? "" : request.getParameter("sBookingType").toString();
        
        String sResidentialFlag = request.getParameter("sResidentialFlag") == null ? "" : request.getParameter("sResidentialFlag").toString();
        
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<< AddAddress Line# 444: sResidentialFlag = " + sResidentialFlag);
        if (sResidentialFlag.equalsIgnoreCase("true") || sResidentialFlag.equalsIgnoreCase("Y")){
            sResidentialFlag = "Y";
        }else {
            sResidentialFlag = "N";
        }
        String sUpdate="";
        String sMessage = "";
             
        if(sType.equals("C"))
        {
            sUpdate="Update address SET owned_by='"+acc_id+"', environment='"+environment+"',collection=1,delivery=0,server='"+host+"',company='"+sCompany+"',contact='"+sContact+"',address1='"+sAddress1+"',address2='"+sAddress2+"',address3='"+sAddress3+"',phone='"+sPhone+"',town='"+sTown+"',county='"+sCounty+"',instructions='"+sInstruct+"',postcode='"+sPostcode+"',country='"+sCountry+"'" +
                ", closesat='"+sClosesByHH+":"+sClosesByMM+"', latitude='"+sLatitude+"', longitude='"+sLongitude+ "', residential_flag='"+sResidentialFlag+"' where ID="+sId+"";
        }
        if(sType.equals("D"))
        {
            sUpdate="Update address SET owned_by='"+acc_id+"',environment='"+environment+"',collection=0,delivery=1,server='"+host+"',company='"+sCompany+"',contact='"+sContact+"',address1='"+sAddress1+"',address2='"+sAddress2+"',address3='"+sAddress3+"',phone='"+sPhone+"',town='"+sTown+"',county='"+sCounty+"',instructions='"+sInstruct+"',postcode='"+sPostcode+"',country='"+sCountry+"'" +
                ", closesat='"+sClosesByHH+":"+sClosesByMM+"', latitude='"+sLatitude+"', longitude='"+sLongitude+ "', residential_flag='"+sResidentialFlag+"' where ID="+sId+"";
        }
        if(sType.equals("CD"))
        {
            sUpdate="Update address SET owned_by='"+acc_id+"',environment='"+environment+"',collection=1,delivery=1,server='"+host+"',company='"+sCompany+"',contact='"+sContact+"',address1='"+sAddress1+"',address2='"+sAddress2+"',address3='"+sAddress3+"',phone='"+sPhone+"',town='"+sTown+"',county='"+sCounty+"',instructions='"+sInstruct+"',postcode='"+sPostcode+"',country='"+sCountry+"'" +
                ", closesat='"+sClosesByHH+":"+sClosesByMM+"', latitude='"+sLatitude+"', longitude='"+sLongitude+ "', residential_flag='"+sResidentialFlag+"' where ID="+sId+"";
        }
        
        Connection conDB = null;
        Statement stmInsert;
      
        
        try
        {
            conDB = cOpenConnection();
            stmInsert= conDB.createStatement();
            stmInsert.executeUpdate(sUpdate);	
            sMessage="Address Book Entry updated successfully.";
            vCloseConnection(conDB);
        }
        catch(Exception ex)
        {
              //strmResponse.println("<html><body><table><tr><td><FONT SIZE=\"-0\" face=\"Verdana,Tahoma,Arial,Helvetica,sans-serif\" COLOR=\"\" + sFontColor+ \">"+ex.toString()+"</font></td></tr></table></boby></html>");
              sMessage=ex.toString();
        }
        
        strmResponse.println("<html>");
        strmResponse.println("<script language=javascript>");
       
        strmResponse.println("opener.AddressBookPopup('"+sBookingType+"','"+sMessage+"')");
	//strmResponse.println("window.close();");
        strmResponse.println("</script>");
        strmResponse.println("<html>");
    }
     
    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void deleteAddress(HttpServletRequest request, HttpServletResponse response, String sId)throws ServletException, IOException
    {
         ServletOutputStream strmResponse = response.getOutputStream();
         response.setContentType("text/html");
         String sMessage = "";
         Connection conDB = null;
         Statement stmDelete;
         String sDelete="Delete from address where ID="+sId+"";
         String sBookingType = request.getParameter("sBookingType") == null ? "" : request.getParameter("sBookingType").toString();

        try
        {
            conDB = cOpenConnection();
            stmDelete= conDB.createStatement();
            stmDelete.executeUpdate(sDelete);	
            sMessage="Address Book Entry deleted successfully.";
            vCloseConnection(conDB);
        }
        catch(Exception ex)
        {
             //strmResponse.println("<html><body><table><tr><td><FONT SIZE=\"-0\" face=\"Verdana,Tahoma,Arial,Helvetica,sans-serif\" COLOR=\"Red\">"+ex.toString()+"</font></td></tr></table></boby></html>");
             sMessage=ex.toString();
        }
         
        strmResponse.println("<html>");
        strmResponse.println("<script language=javascript>");
  
        strmResponse.println("opener.AddressBookPopup('"+sBookingType+"','"+sMessage+"')");
	//strmResponse.println("window.close();");
        strmResponse.println("</script>");
        strmResponse.println("<html>");
    }

    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     * @return
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
