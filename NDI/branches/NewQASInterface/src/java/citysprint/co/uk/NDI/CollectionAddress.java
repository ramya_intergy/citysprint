package citysprint.co.uk.NDI;

/*
 * CollectionAddress.java
 *
 * Created on 27 March 2008, 11:11
 */

import java.beans.*;
import java.io.Serializable;
import javax.servlet.*;
import javax.servlet.http.*;
/**
 * @author ramyas
 */
public class CollectionAddress extends Object implements Serializable {
    
    /**
     *
     */
    public static final String PROP_SAMPLE_PROPERTY = "sampleProperty";
    
    private String sCCompany="",sCContact="",sCAddress1="",sCAddress2="",sCAddress3="",sCTown="",sCPostcode="",sCPhone="",sCCounty="",sCInstruct="",sCCountry="";
    private String sCLatitude="",sCLongitude="";
    // Following variable is added by Adnan on 21-Aug-2013 against Residential Flag
    private String sCResidentialFlag = "N";
    private HttpServletRequest request;
    private PropertyChangeSupport propertySupport;
    
    /**
     *
     */
    public CollectionAddress() {
        propertySupport = new PropertyChangeSupport(this);
    }
    
    // Residential Flag
    /**
     * @return 
     */
    public String getCResidentialFlag(){
        return sCResidentialFlag;
    }
    
    //Company Name
    /**
     *
     * @return
     */
    public String getCCompany() {
         return sCCompany;
    }
    
    //Contact
    /**
     *
     * @return
     */
    public String getCContact() {
         return sCContact;
    }
    
    //Address Line 1
    /**
     *
     * @return
     */
    public String getCCAddress1() {
         return sCAddress1;
    }
    
    //Address Line 2
    /**
     *
     * @return
     */
    public String getCAddress2() {
         return sCAddress2;
    }
    
    //Address Line 3
    /**
     *
     * @return
     */
    public String getCAddress3() {
         return sCAddress3;
    }
    
    //Town
    /**
     *
     * @return
     */
    public String getCTown() {
         return sCTown;
    }
    
    //County
    /**
     *
     * @return
     */
    public String getCCounty() {
         return sCCounty;
    }
    
    //Postcode
    /**
     *
     * @return
     */
    public String getCPostcode() {
         return sCPostcode;
    }
    
    //Country
    /**
     *
     * @return
     */
    public String getCCountry() {
         return sCCountry;
    }
    
    //Instruct
    /**
     *
     * @return
     */
    public String getCInstruct() {
         return sCInstruct;
    }

     //Instruct
    /**
     *
     * @param instruct
     */
    public void setCInstruct(String instruct) {
         this.sCInstruct=instruct;
    }

    //Phone
    /**
     *
     * @return
     */
    public String getCPhone() {
         return sCPhone;
    }

     //Latitude
    /**
     *
     * @return
     */
    public String getCLatitude() {
         return sCLatitude;
    }

    //Longitude
    /**
     *
     * @return
     */
    public String getCLongitude() {
         return sCLongitude;
    }

    /**
     *
     * @param request
     * populates collection address fields
     */
    public void setCollectionAddress(HttpServletRequest request)
    {
        sCCompany=request.getParameter("CCompanyName")==null?"":request.getParameter("CCompanyName").toString();
        sCAddress1=request.getParameter("CAddress1")==null?"":request.getParameter("CAddress1").toString();
        sCAddress2=request.getParameter("CAddress2")==null?"":request.getParameter("CAddress2").toString();
        sCAddress3=request.getParameter("CAddress3")==null?"":request.getParameter("CAddress3").toString();
        sCTown=request.getParameter("CTown")==null?"":request.getParameter("CTown").toString();
        sCCounty=request.getParameter("CCounty")==null?"":request.getParameter("CCounty").toString();
        sCPostcode=request.getParameter("CPostcode")==null?"":request.getParameter("CPostcode").toString();
        sCCountry=request.getParameter("CCountry")==null?"":request.getParameter("CCountry").toString();
        sCContact=request.getParameter("CContactName")==null?"":request.getParameter("CContactName").toString();
        sCPhone=request.getParameter("CPhone")==null?"":request.getParameter("CPhone").toString();
        sCInstruct=request.getParameter("CInstruct")==null?"":request.getParameter("CInstruct").toString();
        sCLatitude=request.getParameter("CLatitude")==null?"":request.getParameter("CLatitude").toString();
        sCLongitude=request.getParameter("CLongitude")==null?"":request.getParameter("CLongitude").toString();
        
        sCResidentialFlag=request.getParameter("CResidentialFlag")==null?"":request.getParameter("CResidentialFlag").toString();
        
    }
}
