package citysprint.co.uk.NDI;

/*
 * CommonClass.java
 *
 * Created on 27 July 2007, 09:38
 */

import java.io.*;
import java.net.*;
import java.lang.*;
import java.util.*;
import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import javax.sql.DataSource;
import javax.naming.*;
import java.util.Vector;
import com.qas.proweb.servlet.*;
import java.net.URLEncoder;
import java.util.Date;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author ramyas
 * @version
 */
public abstract class CommonClass extends HttpServlet{

    /**
     *
     */

   // private DataSource jdbcConnectionPool;

    public static int _arrayLength = 0;
//
    /**
     *
     */

    /**
     *
     */
    public Vector emailVector=new Vector();         //Stores email entries from the Last_10_used_entries table if StoreMessages Flag is true
    /**
     *
     */
    public Vector smsVector=new Vector();           //Stores SMS entries from the Last_10_used_entries table if StoreMessages Flag is true

    /**
     *
     */
    protected String     sErrorMessage      = "";
    /**
     *
     */
    public String scriptsurl  = "";
    /**
     *
     */
    public String environment = "";
    /**
     *
     */
    public String host = "";
    /**
     *
     */
    public boolean debug = false;

    /**
     *
     */
    public ResourceBundle bdl;
    /**
     *
     */
    public String environ;
    /**
     *
     */
    public String server;

    /**
     *
     */
    public String useragent = null;
    /**
     *
     */
    public boolean ie = false;
    /**
     *
     */
    public boolean ff = false;
    /**
     *
     */
    public boolean ns = false;
    public boolean chrome = false;

    public boolean storeMessgesConfig = false;
    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException,SQLException  {


    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void initializeConfigValues(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException
    {
        try
        {
            bdl =  new PropertyResourceBundle(new FileInputStream(getLocalDirName() + "/Config.properties"));

            //Inialize script url
            scriptsurl = bdl.getString("httpProtocol")+"://"+bdl.getString("host")+"/"+bdl.getString("dmsServer");
            environment = bdl.getString("environment");
            host = bdl.getString("host");
            environ = bdl.getString("environ");
            server = bdl.getString("dmsServer");

            if(request.isRequestedSessionIdValid())
                request.getSession(false).setAttribute("storeMessagesConfig", (bdl.getString("storeMessages").equals("Y")?"true":"false"));
//            useragent = request.getHeader("User-Agent");
//            String user = useragent.toLowerCase();
//            if(user.indexOf("msie") != -1) {
//                    ie = true;
//            } else if(user.indexOf("netscape6") != -1) {
//                    ns = true;
//            } else if(user.indexOf("mozilla") != -1) {
//                    ff = true;
//            }

            String ua = request.getHeader( "User-Agent" );
            ff = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
            ie = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
            chrome = (ua != null && ua.indexOf("Chrome/") != -1);
            response.setHeader( "Vary", "User-Agent" );

        }
        catch (Exception ex)
        {
            vShowErrorPage(response, ex);
        }
    }

    /**
     *
     * @param request
     * @return
     * @throws javax.servlet.ServletException
     */
    public boolean isLogin(HttpServletRequest request)throws ServletException
    {
        HttpSession validSession = request.getSession(false);
        boolean isLoggedIn=false;
        //if(request.isRequestedSessionIdValid())
        if(validSession!=null)
        {
            String acc_id = (String)validSession.getAttribute("acc_id");
            String pwd = (String)validSession.getAttribute("pwd");
            if(acc_id==null || pwd==null)
                 isLoggedIn=false;
            else
                isLoggedIn=true;

        }
       return isLoggedIn;
    }

    //New login
    /**
     *
     * @param request
     * @param response
     * @return
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public String[][] newLogin(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException
    {
        String acc_id = request.getParameter("Username")==null?"":request.getParameter("Username").toString();
        String pwd = request.getParameter("Password")==null?"":request.getParameter("Password").toString();
        String[][] loginArray=null;

        boolean result=false;
        java.util.Date d = new java.util.Date();
        String timeStamp = d.toString();

        StringBuffer sInput=new StringBuffer();

        sInput.append("CK="+encStr(acc_id));                                //Customer Key or Account No
        sInput.append("&PW="+encStr(pwd));                                  //Password
        sInput.append("&TS="+encStr(timeStamp));                            //Timestamp

        URL url;



        if(environ.equals("PROD"))
           url = new URL(scriptsurl+bdl.getString("loginScript")+"?"+sInput.toString());
        else
            url = new URL(bdl.getString("devhost")+"login1.txt");

         writeOutputLog("Input to NDI_Login:"+url.toString());

         URLConnection con = url.openConnection();
         con.setDoOutput(true);
         con.setDoInput(true);
         con.setUseCaches(false);
         BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
         String s;
         String outputLines = "";
         while ((s = br.readLine()) != null) {
            outputLines = outputLines + s;
         }

         writeOutputLog("Ouput from NDI_Login:"+outputLines);

         br.close();

        if(outputLines.length()>0)
        {
            outputLines = outputLines + "^ZZ";
            loginArray = parseOutput(outputLines);

        }

        return loginArray;
    }

    /**
     *
     * @param acc_id
     * @param pwd
     * @param request
     * @return
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * Used by ProcessURLRequest function
     */

    public boolean newLogin(String acc_id, String pwd, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String[][] loginArray = null;
        String[][] getCountriesArray = null;
        String[][] getCommodityArray = null;
        String[][] getLoginArray = null;
//        String[][] getNDIParcelArray = null;

        boolean result = false;
        java.util.Date d = new java.util.Date();
        String timeStamp = d.toString();
        String Error = "";

        StringBuffer sInput = new StringBuffer();

        sInput.append("CK=" + encStr(acc_id));                                //Customer Key or Account No
        sInput.append("&PW=" + encStr(pwd));                                  //Password
        sInput.append("&TS=" + encStr(timeStamp));                            //Timestamp

        URL url;



        if (environ.equals("PROD")) {
            url = new URL(scriptsurl + bdl.getString("loginScript") + "?" + sInput.toString());
        } else {
            url = new URL(bdl.getString("devhost") + "login.txt");
        }

        writeOutputLog("Input to NDI_Login:" + url.toString());

        URLConnection con = url.openConnection();
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setUseCaches(false);
        BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String s;
        String outputLines = "";
        while ((s = br.readLine()) != null) {
            outputLines = outputLines + s;
        }

        writeOutputLog("Ouput from NDI_Login:" + outputLines);

        br.close();

        if (outputLines.length() > 0) {
            outputLines = outputLines + "^ZZ";
            loginArray = parseOutput(outputLines);

        }

        if (loginArray == null) {
            result = false;
        } else {
            for (int i = 0; i < _arrayLength; i++) {
                if (loginArray[i][0].equals("ER")) {
                    Error = loginArray[i][1];
                    i = _arrayLength;
                }
            }

            if (Error.equals("")) {
                try {
                    HttpSession newSession = request.getSession(true);
                    newSession.setAttribute("acc_id", acc_id);
                    newSession.setAttribute("pwd", pwd);
                    newSession.setAttribute("loginArray", loginArray);
                    String sSessionID = newSession.getId();
                    newSession.setAttribute("sessionid", sSessionID);

                    //get commodity details
                    getCommodityArray = getCommodity(request, response, acc_id, pwd);

                    if (getCommodityArray == null) {
                    } else {
                        newSession.setAttribute("getCommodityArray", getCommodityArray);
                    }
                    //get country details
                    getCountriesArray = getCountries(request, response, acc_id, pwd);

                    if (getCountriesArray == null) {
                    } else {
                        newSession.setAttribute("getCountriesArray", getCountriesArray);
                    }

//                    //get NDI Parcel details
//                    getNDIParcelArray=getParcel(request,response,acc_id, pwd);
//
//                    if(getNDIParcelArray==null){
//                    } else {
//                       newSession.setAttribute("getNDIParcelArray",getNDIParcelArray);
//                    }
                        
                    writeOutputLog("After commodity and country array");
                    writeOutputLog(getCommodityArray[1][1]);
                    writeOutputLog(""+newSession.getAttribute("getCommodityArray"));
                } catch (Exception ex) {
                    vShowErrorPage(response, ex);
                }
                result = true;
            } else {
                result = false;
            }
        }

        return result;
    }

//    public boolean getAllEntries(HttpServletRequest request,HttpServletResponse response) throws UnknownHostException,IOException
//    {
//        java.util.Date d = new java.util.Date();
//        String timeStamp = d.toString();
//        //response.setContentType("text/html");
//
//
//        /*HttpSession validSession = request.getSession(false);
//        acc_id = validSession.getAttribute("acc_id").toString();
//        pwd = validSession.getAttribute("pwd").toString();*/
//
//        //ServletOutputStream out = response.getOutputStream();
//         String fileLoc = "/ad_book/get_all_entries.php?CK=W99969&PW=LYNNE&TS="+timeStamp+"&SN=ijb.citysprint.co.uk&EN=csfo_cc";
//         URL url = new URL(httpProtocol,host,portNo,fileLoc);
//         URLConnection con = url.openConnection();
//         con.setDoOutput(true);
//         con.setDoInput(true);
//         con.setUseCaches(false);
//         BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
//         String s;
//         String outputLines = "";
//         while ((s = br.readLine()) != null) {
//            outputLines = outputLines + s;
//         }
//
//        br.close();
//        outputLines = outputLines + "^ZZ";
//        getAllEntriesArray = parseOutput(outputLines);
//        if(getAllEntriesArray == null)
//            return false;
//      return true;
//    }

     /**
      *
      * @param request
      * @param response
      * @param acc_id
      * @param pwd
      * @return
      * @throws java.net.UnknownHostException
      * @throws java.io.IOException
      */
     public String[][] getCommodity(HttpServletRequest request,HttpServletResponse response,String acc_id,String pwd) throws UnknownHostException,IOException
    {
        String[][] getCommodityArray=null;

        java.util.Date d = new java.util.Date();
        String timeStamp = d.toString();

        StringBuffer sInput=new StringBuffer();

        sInput.append("CK="+encStr(acc_id));                                //Customer Key or Account No
        sInput.append("&PW="+encStr(pwd));                                  //Password
        sInput.append("&TS="+encStr(timeStamp));                            //Timestamp

         String fileLoc = "";
         URL url;



         if(environ.equals("PROD"))
             url = new URL(scriptsurl+bdl.getString("commodityScript")+"?"+sInput.toString());
         else
             url = new URL(bdl.getString("devhost")+"products.txt");

         writeOutputLog("Input to NDI_Commodity:"+url.toString());

         URLConnection con = url.openConnection();
         con.setDoOutput(true);
         con.setDoInput(true);
         con.setUseCaches(false);
         BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
         String s;
         String outputLines = "";
         while ((s = br.readLine()) != null) {
            outputLines = outputLines + s;
         }

         writeOutputLog("Output From NDI_Commodity:"+outputLines);

         br.close();
         outputLines = outputLines + "^ZZ";
         getCommodityArray = parseOutput(outputLines);

         return getCommodityArray;
    }

//     public String[][] getParcel (HttpServletRequest request,HttpServletResponse response,String acc_id,String pwd) throws UnknownHostException,IOException
//    {
//        String[][] getParcelArray=null;
//
//        java.util.Date d = new java.util.Date();
//        String timeStamp = d.toString();
//
//        boolean result=false;
//
//        StringBuffer sInput=new StringBuffer();
//
//        sInput.append("CK="+encStr(acc_id));                                //Customer Key or Account No
//        sInput.append("&PW="+encStr(pwd));                                  //Password
//        sInput.append("&TS="+encStr(timeStamp));                            //Timestamp
//
//        URL url;
//
//
//
//        if(environ.equals("PROD"))
//           url = new URL(scriptsurl+bdl.getString("parcelScript")+"?"+sInput.toString());
//        else
//            url = new URL(bdl.getString("devhost")+"parcel1.txt");
//
//         writeOutputLog("Input to NDI_Parcel:"+url.toString());
//
//         URLConnection con = url.openConnection();
//         con.setDoOutput(true);
//         con.setDoInput(true);
//         con.setUseCaches(false);
//         BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
//         String s;
//         String outputLines = "";
//         while ((s = br.readLine()) != null) {
//            outputLines = outputLines + s;
//         }
//
//         writeOutputLog("Ouput from NDI_Parcel:"+outputLines);
//
//         br.close();
//
//        if(outputLines.length()>0)
//        {
//            outputLines = outputLines + "^ZZ";
//            getParcelArray = parseOutput(outputLines);
//
//        }
//
//        return getParcelArray;
//    }
     /**
      *
      * @param request
      * @param response
      * @param acc_id
      * @param pwd
      * @return
      * @throws java.net.UnknownHostException
      * @throws java.io.IOException
      */
     public String[][] getCountries(HttpServletRequest request,HttpServletResponse response,String acc_id,String pwd) throws UnknownHostException,IOException
    {

        String[][] getCountriesArray=null;

        java.util.Date d = new java.util.Date();
        String timeStamp = d.toString();

        StringBuffer sInput=new StringBuffer();

        sInput.append("CK="+encStr(acc_id));                                //Customer Key or Account No
        sInput.append("&PW="+encStr(pwd));                                  //Password
        sInput.append("&TS="+encStr(timeStamp));                            //Timestamp

         String fileLoc = "";

         URL url;


         if(environ.equals("PROD"))
             url = new URL(scriptsurl+bdl.getString("countryScript")+"?"+sInput.toString());
         else
             url = new URL(bdl.getString("devhost")+"countries.txt");

         writeOutputLog("Input to NDI_Country:"+url.toString());

         URLConnection con = url.openConnection();
         con.setDoOutput(true);
         con.setDoInput(true);
         con.setUseCaches(false);
         BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
         String s;
         String outputLines = "";
         while ((s = br.readLine()) != null) {
            outputLines = outputLines + s;
         }

         writeOutputLog("Output from NDI_Country:"+outputLines);

         br.close();
         outputLines = outputLines + "^ZZ";
         getCountriesArray = parseOutput(outputLines);

         return getCountriesArray;
    }

    /**
     *
     * @param outputLines
     * @return
     */
    public String[][] parseOutput(String outputLines)
    {
        String temp="";
        outputLines = outputLines.trim();
        outputLines = outputLines.replace('^','\n');
        String stringArray[] = outputLines.split("\n");
        _arrayLength = stringArray.length;
        String parsedOutput[][]=null;
        parsedOutput = new String[_arrayLength+1][2];

        for(int i=0;i<_arrayLength;i++)
        {
           temp=stringArray[i];
           if(temp.length()>0)
           {
               parsedOutput[i][0] = temp.substring(0,2);
               parsedOutput[i][1] = temp.substring(2);
           }
           else
           {
               parsedOutput[i][0] = "ST";
               parsedOutput[i][1] = "START";
           }
        }

        return parsedOutput;
    }

//     public void parseJobdetails(String[][] JobdetailsArray,String acc_id,String session_ID) throws  IOException
//   {
//       String tempstr="";
//       String tag ="";
//       String sSQL="";
//       String sSQLStamp="";
//       String sComDescrip="";
//       String empty_String="''";
//
////       String sqlHeader = "insert into ndi_details (det_sessionid,det_accid,det_server,det_environ,det_jno,det_hawbno,det_readyon,det_readyat,det_servcode,det_ref,det_price,det_surcharge,det_vat,det_liability,det_dcomp,det_dcountry,det_ptype,det_totpieces,det_totweight,det_pod,det_jstatus,det_descrip)" +
////              "values('"+session_ID+"','"+acc_id+"','"+server+"','"+environment+"', ";
//
//       
//       String sqlHeader = "insert into ndi_details (det_sessionid,det_accid,det_server,det_environ,det_jno,det_hawbno,det_readyon,det_readyat,det_servcode,det_ref,det_price,det_surcharge,det_vat,det_liability,det_dcomp,det_dcountry,det_ptype,det_totpieces,det_totweight,det_pod,det_jstatus,det_descrip, det_dept)" +
//              "values('"+session_ID+"','"+acc_id+"','"+server+"','"+environment+"', "; 
// 
//       int i,itemp=0;
//       //int j=14;
//       int k = JobdetailsArray.length;
//       System.out.println(k);
//       int sqllen;
//       for( i=0;i<JobdetailsArray.length-1;i++){
//
//           tag = JobdetailsArray[i][0];
//
//           //Tag equals Transaction 
//
//           if (tag.equals("TS")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//           }
//
//           //Tag equals JobNumber 
//           if (tag.equals("JN")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//
//                   sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
//
//               } else {
//                   //String sqlJobNo="''";
//                   sSQL = sSQL + empty_String + ",";
//               }
//
//           }
//
//           //Tag equals Product Type 
//           if (tag.equals("TP")) {
//
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//                   // MySql Date Format
//                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
//
//               } else {
//                   //String sqlProd="''";
//                   sSQL = sSQL + empty_String + ",";
//               }
//
//           }
//           //Tag equals Ready on
//           if (tag.equals("RO")) {
//
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//                   // MySql Date Format
//                   sSQL = sSQL + "DATE_FORMAT('" + tempstr + "','%d/%m/%y'),";
//
//               } else {
//                   //String sqlReadyOn="''"; 
//                   sSQL = sSQL + empty_String + ",";
//               }
//
//           }
//           //Tag Equals ReadyAt
//           if (tag.equals("RA")) {
//
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//                   // MySql Date Format
//                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
//
//               } else {
//                   // String sqlReadyAt="''";
//                   sSQL = sSQL + empty_String + ",";
//               }
//
//           }
//
//
//           //Tag equals Reference 
//           if (tag.equals("RE")) {
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//                   //MySql Query
//
//                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
//
//
//               } else {
//                   //String Ref="''";
//                   sSQL = sSQL + empty_String + ",";
//               }
//
//           }
//
//
//
//           //Tag equals Delivery Country
//           if (tag.equals("DC")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//
//                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
//               } else {
//                   //String sqlCountry="''";
//                   sSQL = sSQL + empty_String + ",";
//               }
//           }
//
//           //Tag equals Delivery Company 
//           if (tag.equals("DY")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//
//                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
//               } else {
//                   //String sqlCompany="''";
//                   sSQL = sSQL + empty_String + ",";
//               }
//           }
//
//
//
//           //Tag equals Service Level 
//
//           if (tag.equals("SV")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//
//                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
//               } else {
//                   // String sqlService="''";
//                   sSQL = sSQL + empty_String + ",";
//               }
//
//           }
//
//           //Tag equals total no of Pieces 
//           if (tag.equals("PN")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//
//                   sSQL = sSQL + tempstr + ",";
//               } else {
//                   // String sqlItem="''";
//                   sSQL = sSQL + itemp + ",";
//               }
//
//           }
//
//           //Tag equals Weight
//
//           if (tag.equals("TW")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//
//                   sSQL = sSQL + tempstr + ",";
//               } else {
//                   // String sqlWeight="''";
//                   sSQL = sSQL + itemp + ",";
//               }
//
//           }
//
//           //Tag equals POD details 
//           if (tag.equals("PO")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//
//                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
//               } else {
//                   //String sqlPod="''";
//                   sSQL = sSQL + empty_String + ",";
//               }
//
//           }
//
//
//
//           //Tag equals Hawbno
//           if (tag.equals("HN")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//               String tempHawbno = "";
//               String tempHawbno1 = "";
//               String tempHawbno2 = "";
//
//               if (tempstr.length() > 0) {
//
//                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
//
//               } else {
//
//                   sSQL = sSQL + empty_String + ",";
//               }
//
//           }
//
//           //Tag equals Basic Price
//
//           if (tag.equals("BP")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//
//                   sSQL = sSQL + tempstr + ",";
//               } else {
//                   // String sqlPrice ="''";
//                   sSQL = sSQL + itemp + ",";
//               }
//
//           }
//           
//           //Tag equals SurCharges
//           if (tag.equals("SC")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//
//                   sSQL = sSQL + tempstr + ",";
//               } else {
//                   //  String sqlSurcharge="''";
//                   sSQL = sSQL + itemp + ",";
//               }
//
//           }
//
//           //Tag equals VAT
//           if (tag.equals("GS")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//
//                   sSQL = sSQL + tempstr + ",";
//               } else {
//                   //String sqlGst="''";
//                   sSQL = sSQL + itemp + ",";
//               }
//
//           }
//
//           //Tag equals Liability
//           if (tag.equals("LP")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//
//                   sSQL = sSQL + tempstr + ",";
//               } else {
//                   // String sqlLiability="''";
//                   sSQL = sSQL + itemp + ",";
//               }
//
//           }
//
//
//           //Tag equals JobStatus
//           if (tag.equals("JS")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//
//                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
//               } else {
//                   //String sqlJobstatus="''";
//                   sSQL = sSQL + empty_String + ",";
//               }
//           }
//
//           int czvalue = 0;
//           if (tag.equals("CZ")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//               czvalue = Integer.parseInt(tempstr);
//
//               if (tempstr.equals("0")) {
//                   String sqlTest = "''";
//                   sSQL = sSQL + sqlTest;
//               }
//               } else {
//
//
//                   for (int c = 0; c <= czvalue; c++) {
//                       //Tag Equals Commodity Code
//
//                       if (tag.equals("CC")) {
//                           tempstr = JobdetailsArray[i][1].trim();
//                       }
//
//
//                       //Tag Equals Commodity Description
//
//                       if (tag.equals("CD")) {
//
//                           tempstr = JobdetailsArray[i][1].trim();
//
//                           if (tempstr.length() > 0) {
//
//                               //sComDescrip= sComDescrip + tempstr;
//                           sComDescrip = sComDescrip + sReplaceChars(tempstr,"'","''");
//                           } else {
//                               //String sqldescrip="''";
//                               sComDescrip = sComDescrip + empty_String;
//                           // sSQL = sSQL+"'" + sComDescrip +"'" ;
//                           }
//
//
//                       }
//
//                       //Tag Equals Commodity Quantity
//
//                       if (tag.equals("CQ")) {
//
//                           tempstr = JobdetailsArray[i][1].trim();
//
//
//                       }
//
//                       //Tag Equals Commodity Value
//                       if (tag.equals("CV")) {
//
//                           tempstr = JobdetailsArray[i][1].trim();
//
//                       }
//
//                       // Tag Equals Commodity Indemnity
//                       if (tag.equals("CI")) {
//
//                           tempstr = JobdetailsArray[i][1].trim();
//
//                       sSQL = sSQL + "'" + sComDescrip + "'";
//                           sComDescrip = "";
//
//                       }
//
//
//                   }
//
//
//
//               }
//           
//
//           //Tag equals Department
//           if (tag.equals("DT")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               if (tempstr.length() > 0) {
//
//                   sSQL = sSQL + ",'" + sReplaceChars(tempstr,"'","''") + "'";
//               } else {
//                   sSQL = sSQL + "," + empty_String;
//               }
//           }
//
//           //End Tag for Each Job set
//
//           if (tag.equals("XX")) {
//
//               tempstr = JobdetailsArray[i][1].trim();
//
//               // sSQL=sSQL+"'"+tempstr+"'";
//
//               sSQL = sqlHeader + sSQL + ")";
//
//               try {
//
//                   Connection condetails = null;
//                    try
//      {
//          // Opening connection to database by registering the driver.
////         Class.forName("com.mysql.jdbc.Driver").newInstance();
////         condetails  = DriverManager.getConnection("jdbc:mysql://10.2.10.112:3306/ad_book_nextday?"+"user=adnan&password=adnann");
//          
//           Class.forName(bdl.getString("sJDBCDriver")).newInstance();
//         condetails  = DriverManager.getConnection(bdl.getString("sJDBCurl")+"user="+bdl.getString("sMySqlUser")+"&password="+bdl.getString("sMySqlPassword"));
//
//      }
//      catch (Exception errOpen)
//      {
//          throw new SQLException("<B>Error opening database connection</B><BR>" +
//                          errOpen.toString());
//      }
//                   Statement stmtdetails = condetails.createStatement();
//
//                   stmtdetails.executeUpdate(sSQL);//,Statement.RETURN_GENERATED_KEYS);
//                   condetails.close();
//                   System.out.println("Successful insertion ==> SQL ==> " + sSQL);
//                }
//                catch(Exception e){
//                    System.out.println("Exception block ==> SQL ==> " + sSQL);
////                    vShowErrorPageReporting(res, e);
//                    e.printStackTrace();
//                }
//                sSQL="";
//            } 
//        
//       }       
//    }
public static void main (String args[]) throws Exception{
    CommonClass commObj = new JobDetail();
//    String input = "^TSTUE FEB 19 02:00:44 GMT 2013^JN11651596^HN3662141^RO12/02/13^RA15:08^SVIX        INTERNATIONAL EXPRESS^RECHARIS/STEF^BP25.46^SC2.93^GS0.00^LP0.00^DCOTTO LEATHERS^DYTUR^TPNDOX^PN1^TW4.00^PO13/02/13 11:32 BERK BAYRAM^JSD^CZ2^CC^CD6M LEATHER SKIN SAMPLE^CQ1^CV6.00^CIN^CC^CD6M LINING FABRIC SAMPLE^CQ1^CV6.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11651435^HN3662097^RO12/02/13^RA14:59^SVIX        INTERNATIONAL EXPRESS^REFH/MW/IP/CS^BP26.69^SC3.07^GS0.00^LP0.00^DC2 ALLSAINTS INDIA OFFICE^DYIND^TPNDOX^PN1^TW2.00^PO14/02/13 14:30 CO STAMP^JSD^CZ6^CC^CDBELT SAMPLES^CQ3^CV3.00^CIN^CC^CDEARRING SAMPLE^CQ1^CV1.00^CIN^CC^CDSHOE SAMPLES^CQ2^CV4.00^CIN^CC^CDDRESS SAMPLES^CQ2^CV8.00^CIN^CC^CDSTUD SAMPLES^CQ5^CV0.00^CIN^CC^CDSWATCH SAMPLES^CQ5^CV0.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11651293^HN3662063^RO12/02/13^RA14:53^SVIX        INTERNATIONAL EXPRESS^RELIZ/VIKKI^BP12.19^SC1.40^GS2.72^LP0.00^DC3 ALLSAINTS PORTUGAL OFFICE^DYPRT^TPNDOX^PN1^TW1.00^PO13/02/13  9:13 BLAS S^JSD^CZ1^CC^CDJERSEY SAMPLES^CQ6^CV6.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11651196^HN3662040^RO12/02/13^RA14:48^SVIX        INTERNATIONAL EXPRESS^RETHOMAS^BP17.86^SC2.05^GS0.00^LP0.00^DC1 ALLSAINTS HONG KONG OFFICE^DYHKG^TPNDOX^PN1^TW1.00^PO15/02/13 10:00 COMPANY STAMP^JSD^CZ1^CC^CDJERSEY POLO SAMPLES^CQ4^CV4.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11632214^HN3660512^RO11/02/13^RA15:56^SVIX        INTERNATIONAL EXPRESS^REHOLLY^BP10.04^SC1.15^GS2.24^LP0.00^DCACTION MAILLE^DYFRA^TPNDOX^PN1^TW0.50^PO12/02/13 10:11 DUCHESNE^JSD^CZ1^CC^CDSWATCH SAMPLES^CQ4^CV1.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11631729^HN3660399^RO11/02/13^RA15:26^SVIX        INTERNATIONAL EXPRESS^REKRYSTYNA^BP27.9" + "ZZ";
   // String input = "^TSTUE FEB 19 02:00:44 GMT 2013^JN11651596^HN3662141^RO12/02/13^RA15:08^SVIX INTERNATIONAL EXPRESS^RECHARIS/STEF^BP25.46^SC2.93^GS0.00^LP0.00^DCOTTO LEATHERS^DYTUR^TPNDOX^PN1^TW4.00^PO13/02/13 11:32 BERK BAYRAM^JSD^CZ2^CC^CD6M LEATHER SKIN SAMPLE^CQ1^CV6.00^CIN^CC^CD6M LINING FABRIC SAMPLE^CQ1^CV6.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11651435^HN3662097^RO12/02/13^RA14:59^SVIX INTERNATIONAL EXPRESS^REFH/MW/IP/CS^BP26.69^SC3.07^GS0.00^LP0.00^DC2 ALLSAINTS INDIA OFFICE^DYIND^TPNDOX^PN1^TW2.00^PO14/02/13 14:30 CO STAMP^JSD^CZ6^CC^CDBELT SAMPLES^CQ3^CV3.00^CIN^CC^CDEARRING SAMPLE^CQ1^CV1.00^CIN^CC^CDSHOE SAMPLES^CQ2^CV4.00^CIN^CC^CDDRESS SAMPLES^CQ2^CV8.00^CIN^CC^CDSTUD SAMPLES^CQ5^CV0.00^CIN^CC^CDSWATCH SAMPLES^CQ5^CV0.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11651293^HN3662063^RO12/02/13^RA14:53^SVIX INTERNATIONAL EXPRESS^RELIZ/VIKKI^BP12.19^SC1.40^GS2.72^LP0.00^DC3 ALLSAINTS PORTUGAL OFFICE^DYPRT^TPNDOX^PN1^TW1.00^PO13/02/13 9:13 BLAS S^JSD^CZ1^CC^CDJERSEY SAMPLES^CQ6^CV6.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11651196^HN3662040^RO12/02/13^RA14:48^SVIX INTERNATIONAL EXPRESS^RETHOMAS^BP17.86^SC2.05^GS0.00^LP0.00^DC1 ALLSAINTS HONG KONG OFFICE^DYHKG^TPNDOX^PN1^TW1.00^PO15/02/13 10:00 COMPANY STAMP^JSD^CZ1^CC^CDJERSEY POLO SAMPLES^CQ4^CV4.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11632214^HN3660512^RO11/02/13^RA15:56^SVIX INTERNATIONAL EXPRESS^REHOLLY^BP10.04^SC1.15^GS2.24^LP0.00^DCACTION MAILLE^DYFRA^TPNDOX^PN1^TW0.50^PO12/02/13 10:11 DUCHESNE^JSD^CZ1^CC^CDSWATCH SAMPLES^CQ4^CV1.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11631729^HN3660399^RO11/02/13^RA15:26^SVIX INTERNATIONAL EXPRESS^REKRYSTYNA^BP27.94^SC8.73^GS7.33^LP0.00^DCKATARZYNA JENEK^DYPOL^TPDOX^PN1^TW6.00^PO12/02/13 13:43 JENEK^JSD^CZ0^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11631654^HN3660383^RO11/02/13^RA15:21^SVIX INTERNATIONAL EXPRESS^REGERALDINE^BP12.59^SC1.45^GS0.00^LP0.00^DC6 ALLSAINTS NEW YORK OFFICE^DYUSA^TPNDOX^PN1^TW1.50^PO12/02/13 14:43 MORTON^JSD^CZ1^CC^CDLEATHER JACKET SAMPLE^CQ1^CV5.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11631548^HN3660353^RO11/02/13^RA15:15^SVIX INTERNATIONAL EXPRESS^RETRAM/FELICI^BP41.73^SC4.80^GS0.00^LP0.00^DC2 ALLSAINTS INDIA OFFICE^DYIND^TPNDOX^PN2^TW8.00^PO13/02/13 17:20 CO STAMP^JSD^CZ2^CC^CDSWATCH SAMPLE^CQ1^CV1.00^CIN^CC^CDBAG SAMPLES^CQ5^CV10.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11631448^HN3660330^RO11/02/13^RA15:10^SVIX INTERNATIONAL EXPRESS^RESTEF^BP15.06^SC1.73^GS0.00^LP0.00^DCOTTO LEATHERS^DYTUR^TPNDOX^PN1^TW0.50^PO12/02/13 11:01 EZGI TARHAN^JSD^CZ2^CC^CD2M CLOTH SAMPLE^CQ1^CV2.00^CIN^CC^CDBUTTON SAMPLES^CQ8^CV1.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11631383^HN3660314^RO11/02/13^RA15:07^SVIX INTERNATIONAL EXPRESS^RECS/BG/MT^BP22.42^SC2.58^GS0.00^LP0.00^DC5 THE APPARE LAB LLC^DYTUR^TPNDOX^PN1^TW3.00^PO12/02/13 16:00 AYSEGUL GOKDAL DELIVERED^JSD^CZ4^CC^CD1.3M SHIRT FABRIC SAMPLE^CQ1^CV1.00^CIN^CC^CDLEATHER JACKET SAMPLE^CQ1^CV5.00^CIN^CC^CDBUCKLE SAMPLES^CQ12^CV1.00^CIN^CC^CDSWATCH SAMPLE^CQ1^CV0.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11631258^HN3660284^RO11/02/13^RA15:01^SVIX INTERNATIONAL EXPRESS^REIAN^BP13.46^SC1.55^GS3.00^LP0.00^DCMARTIAPE CALCADAS S.A.^DYPRT^TPNDOX^PN1^TW1.50^PO12/02/13 11:41 GOMES M^JSD^CZ1^CC^CDPAIR BOOTS SAMPLE^CQ1^CV4.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11630998^HN3660226^RO11/02/13^RA14:49^SVIX INTERNATIONAL EXPRESS^RELIZ/REGINA^BP10.60^SC1.22^GS2.36^LP0.00^DC3 ALLSAINTS PORTUGAL OFFICE^DYPRT^TPNDOX^PN1^TW0.50^PO12/02/13 8:44 BLAS S^JSD^CZ2^CC^CDTOP SAMPLES^CQ3^CV3.00^CIN^CC^CDDRESS SAMPLE^CQ1^CV2.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11604592^HN3658804^RO08/02/13^RA15:44^SVIX INTERNATIONAL EXPRESS^REYUKA^BP16.35^SC7.50^GS0.00^LP0.00^DC2 ALLSAINTS INDIA OFFICE^DYIND^TPDOX^PN1^TW1.00^PO13/02/13 17:20 CO STAMP^JSD^CZ0^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11604480^HN3658774^RO08/02/13^RA15:39^SVIX INTERNATIONAL EXPRESS^RESTELLA^BP17.86^SC2.05^GS0.00^LP0.00^DCAUSSCO^DYHKG^TPNDOX^PN1^TW1.00^PO18/02/13 11:06 COMPANY STAMP^JSD^CZ1^CC^CDKNIT SWATCH SAMPLE^CQ3^CV3.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11604393^HN3658753^RO08/02/13^RA15:35^SVIX INTERNATIONAL EXPRESS^REREGINA^BP17.86^SC2.05^GS0.00^LP0.00^DCCAMBERLEY^DYHKG^TPNDOX^PN1^TW1.00^PO14/02/13 11:55 COMPANY STAMP^JSD^CZ1^CC^CDDRESS SAMPLE^CQ2^CV4.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11604254^HN3658717^RO08/02/13^RA15:28^SVIX INTERNATIONAL EXPRESS^RESTELLA^BP21.42^SC2.46^GS0.00^LP0.00^DCFENIX^DYHKG^TPNDOX^PN1^TW2.00^PO14/02/13 11:55 COMPANY STAMP^JSD^CZ1^CC^CDKNITWEAR SAMPLE^CQ5^CV5.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11604167^HN3658700^RO08/02/13^RA15:24^SVIX INTERNATIONAL EXPRESS^REIAN^BP12.19^SC1.40^GS2.72^LP0.00^DCVITORINO DA SILVA COELHO^DYPRT^TPNDOX^PN1^TW1.00^PO11/02/13 10:36 CEU M^JSD^CZ1^CC^CDSINGLE SHOE SAMPLE^CQ1^CV2.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11604090^HN3658680^RO08/02/13^RA15:21^SVIX INTERNATIONAL EXPRESS^REIAN^BP12.19^SC1.40^GS2.72^LP0.00^DCMARTIAPE CALCADAS S.A.^DYPRT^TPNDOX^PN1^TW1.00^PO11/02/13 10:27 FERREIRA JOAO^JSD^CZ1^CC^CDFABRIC SAMPLES^CQ1^CV2.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11604019^HN3658654^RO08/02/13^RA15:17^SVIX INTERNATIONAL EXPRESS^REIAN^BP20.97^SC2.41^GS0.00^LP0.00^DCDAWAR GROUP^DYIND^TPNDOX^PN1^TW1.00^PO12/02/13 16:01 STAMP^JSD^CZ1^CC^CDSINGLE FOOTWEAR SAMPLE^CQ1^CV2.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11603944^HN3658636^RO08/02/13^RA15:13^SVIX INTERNATIONAL EXPRESS^REMIMI^BP12.19^SC1.40^GS2.72^LP0.00^DC3 ALLSAINTS PORTUGAL OFFICE^DYPRT^TPNDOX^PN1^TW1.00^PO11/02/13 9:35 VELOSO S^JSD^CZ1^CC^CDJERSEY SAMPLES^CQ3^CV3.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11603628^HN3658577^RO08/02/13^RA14:58^SVIX INTERNATIONAL EXPRESS^RE^BP31.53^SC9.46^GS0.00^LP0.00^DC1 ALLSAINTS HONG KONG OFFICE^DYHKG^TPNDOX^PN2^TW9.00^PO15/02/13 10:00 COMPANY STAMP^JSD^CZ5^CC^CDKNITWEAR SAMPLE^CQ1^CV2.00^CIN^CC^CDLEATHER JACKET SAMPLE^CQ6^CV24.00^CIN^CC^CDSHIRT SAMPLE^CQ4^CV8.00^CIN^CC^CDSHORTS SAMPLE^CQ1^CV2.00^CIN^CC^CDSHORTS SAMPLE^CQ12^CV24.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11583952^HN3656964^RO07/02/13^RA15:28^SVIX INTERNATIONAL EXPRESS^REREGINA^BP16.07^SC7.50^GS0.00^LP0.00^DC1 ALLSAINTS HONG KONG OFFICE^DYHKG^TPNDOX^PN1^TW0.50^PO15/02/13 11:11 COMPANY STAMP^JSD^CZ1^CC^CDSHORTS SAMPLE^CQ1^CV2.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11583883^HN3656944^RO07/02/13^RA15:24^SVIX INTERNATIONAL EXPRESS^RECLAIRE^BP16.52^SC1.90^GS0.00^LP0.00^DC5 THE APPARE LAB LLC^DYTUR^TPNDOX^PN1^TW1.00^PO08/02/13 14:30 PETEK MENGEN DELIVERED^JSD^CZ3^CC^CDFABRIC SWATCH SAMPLE^CQ1^CV1.00^CIN^CC^CDJEANS SAMPLE^CQ1^CV2.00^CIN^CC^CDBLAZER SAMPLE^CQ1^CV3.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11583690^HN3656899^RO07/02/13^RA15:14^SVIX INTERNATIONAL EXPRESS^RELISA^BP16.07^SC1.85^GS0.00^LP0.00^DCCAMBERLEY^DYHKG^TPNDOX^PN1^TW0.50^PO14/02/13 11:56 COMPANY STAMP^JSD^CZ1^CC^CDPATTERN/DRESS SAMPLE^CQ1^CV3.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11583611^HN3656879^RO07/02/13^RA15:08^SVIX INTERNATIONAL EXPRESS^RELISA^BP28.76^SC3.31^GS0.00^LP0.00^DCCAMBERLEY^DYHKG^TPNDOX^PN1^TW4.50^PO14/02/13 11:56 COMPANY STAMP^JSD^CZ1^CC^CD20 YARDS MESH FABRIC^CQ1^CV15.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11583539^HN3656858^RO07/02/13^RA15:03^SVIXNX INTL NEXT WORKING DAY^REKARI/VIKKI/^BP29.55^SC3.40^GS0.00^LP0.00^DC2 ALLSAINTS INDIA OFFICE^DYIND^TPNDOX^PN2^TW2.50^PO08/02/13 17:45 DIKKAR SINGH^JSD^CZ3^CC^CDLEATHER BAG SAMPLE^CQ1^CV3.00^CIN^CC^CDWOVEN/LEATHER TOP SAMPLE^CQ2^CV6.00^CIN^CC^CDSHIRT SAMPLE^CQ2^CV4.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11583357^HN3656812^RO07/02/13^RA14:53^SVIX INTERNATIONAL EXPRESS^REIAN PORTER^BP22.30^SC2.56^GS4.97^LP0.00^DCMARTIAPE CALCADAS S.A.^DYPRT^TPNDOX^PN4^TW8.80^PO08/02/13 10:14 FERREIRA JOAO^JSD^CZ1^CC^CDPAIR BOOTS SAMPLE^CQ4^CV16.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11565571^HN3655403^RO06/02/13^RA16:09^SVIX INTERNATIONAL EXPRESS^REJONATHON^BP14.29^SC7.16^GS0.00^LP0.00^DCMALIN LANDEAUS^DYUSA^TPNDOX^PN1^TW1.00^PO^JSD^CZ1^CC^CDLEATHER JACKET SAMPLE^CQ1^CV5.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11565521^HN3655391^RO06/02/13^RA16:05^SVIX INTERNATIONAL EXPRESS^REHOLLY^BP10.60^SC1.22^GS2.36^LP0.00^DCITHITEX^DYITA^TPNDOX^PN1^TW0.50^PO07/02/13 17:21 GIUSEPPE^JSD^CZ1^CC^CDFABRIC CUTTING SAMPLE^CQ1^CV1.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11565444^HN3655371^RO06/02/13^RA16:01^SVIX INTERNATIONAL EXPRESS^REIAN/TRAM/LI^BP41.73^SC4.80^GS0.00^LP0.00^DC2 ALLSAINTS INDIA OFFICE^DYIND^TPNDOX^PN3^TW10.00^PO07/02/13 16:00 DIKKAR SINGH^JSD^CZ5^CC^CDBAG SAMPLES^CQ4^CV8.00^CIN^CC^CDBOOTS PAIR SAMPLE^CQ1^CV4.00^CIN^CC^CDWASH BAG SAMPLE^CQ1^CV2.00^CIN^CC^CDCASE SAMPLES^CQ3^CV3.00^CIN^CC^CDLEATHER JACKET SAMPLE^CQ1^CV5.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11565290^HN3655339^RO06/02/13^RA15:51^SVIX INTERNATIONAL EXPRESS^REBG/JL/RW^BP29.70^SC3.42^GS0.00^LP0.00^DC5 THE APPARE LAB LLC^DYTUR^TPNDOX^PN2^TW6.00^PO07/02/13 16:10 AYSEGUL GOKDAL DELIVERED^JSD^CZ5^CC^CDLEATHER JACKET SAMPLESQ^CQ3^CV15.00^CIN^CC^CD5M FABRIC SAMPLE^CQ1^CV5.00^CIN^CC^CDSWATCH SAMPLES^CQ3^CV0.00^CIN^CC^CDJACKET SAMPLE^CQ1^CV3.00^CIN^CC^CDSKIRT SAMPLE^CQ1^CV1.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11565141^HN3655301^RO06/02/13^RA15:44^SVIX INTERNATIONAL EXPRESS^REBARBARA^BP13.46^SC1.55^GS3.00^LP0.00^DCVITORINO DA SILVA COELHO^DYPRT^TPNDOX^PN1^TW1.50^PO07/02/13 10:22 GONCALVES C^JSD^CZ1^CC^CD30FT LEATHER SAMPLE^CQ1^CV5.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11565045^HN3655276^RO06/02/13^RA15:39^SVIX INTERNATIONAL EXPRESS^REIAN^BP17.06^SC1.96^GS3.80^LP0.00^DCMARTIAPE CALCADAS S.A.^DYPRT^TPNDOX^PN1^TW2.80^PO07/02/13 10:19 FERREIRA JOAO^JSD^CZ1^CC^CDPAIR BOOTS SAMPLE^CQ1^CV4.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11564922^HN3655247^RO06/02/13^RA15:34^SVIX INTERNATIONAL EXPRESS^REIAN/BRIA/MI^BP13.46^SC1.55^GS3.00^LP0.00^DC3 ALLSAINTS PORTUGAL OFFICE^DYPRT^TPNDOX^PN1^TW1.50^PO07/02/13 9:00 FREDERICO^JSD^CZ3^CC^CDSWATCH SAMPLE^CQ1^CV0.00^CIN^CC^CDPAIR BOOTS SAMPLE^CQ1^CV4.00^CIN^CC^CDJERSEY TEE SAMPLE^CQ1^CV1.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11564728^HN3655197^RO06/02/13^RA15:23^SVIX INTERNATIONAL EXPRESS^RELIZ^BP21.42^SC2.46^GS0.00^LP0.00^DC1 ALLSAINTS HONG KONG OFFICE^DYHKG^TPNDOX^PN1^TW2.00^PO08/02/13 9:50 COMPANY STAMP^JSD^CZ2^CC^CDDENIM SAMPLES^CQ2^CV2.00^CIN^CC^CDSHIRT SAMPLES^CQ6^CV0.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11564679^HN3655186^RO06/02/13^RA15:21^SVIX INTERNATIONAL EXPRESS^REREGINA^BP16.07^SC1.85^GS0.00^LP0.00^DCCAMBERLEY^DYHKG^TPNDOX^PN1^TW0.50^PO08/02/13 10:43 COMPANY STAMP^JSD^CZ1^CC^CDDRESS SAMPLE^CQ1^CV2.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11564544^HN3655148^RO06/02/13^RA15:15^SVIX INTERNATIONAL EXPRESS^REANDREA^BP15.38^SC1.77^GS0.00^LP0.00^DCFAZ LTD^DYHKG^TPDOX^PN1^TW1.00^PO08/02/13 12:31 COMPANY STAMP^JSD^CZ0^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11564314^HN3655086^RO06/02/13^RA15:02^SVIX INTERNATIONAL EXPRESS^REELIZABETH P^BP14.73^SC7.21^GS4.39^LP0.00^DCERWIN PEK^DYAUT^TPNDOX^PN1^TW2.00^PO07/02/13 16:44 PEK^JSD^CZ3^CC^CDPOLO SHIRT SAMPLE^CQ1^CV20.00^CIN^CC^CDPLAYBALLS^CQ4^CV9.00^CIN^CC^CDSWEETS/NUTS^CQ1^CV1.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11545882^HN3653655^RO05/02/13^RA16:00^SVIX INTERNATIONAL EXPRESS^REANNA BLOCHB^BP13.15^SC7.03^GS0.00^LP0.00^DCYUE YAO^DYUSA^TPNDOX^PN1^TW0.50^PO^JSD^CZ1^CC^CDGIFT CARD^CQ1^CV1.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11545418^HN3653523^RO05/02/13^RA15:35^SVIX INTERNATIONAL EXPRESS^REHOLLY BROWN^BP22.30^SC2.56^GS4.97^LP0.00^DCEMMEBI SMART^DYITA^TPNDOX^PN1^TW6.00^PO07/02/13 17:31 NISTRI^JSD^CZ1^CC^CDJACKET SAMPLES^CQ2^CV4.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11545300^HN3653489^RO05/02/13^RA15:29^SVIX INTERNATIONAL EXPRESS^RESTEF^BP39.75^SC4.57^GS0.00^LP0.00^DC2 ALLSAINTS INDIA OFFICE^DYIND^TPNDOX^PN1^TW5.00^PO07/02/13 16:00 DIKKAR SINGH^JSD^CZ1^CC^CDLEATHER JACKET SAMPLES^CQ4^CV20.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11544955^HN3653424^RO05/02/13^RA15:12^SVIX INTERNATIONAL EXPRESS^RETRAM/HB/KB^BP149.40^SC17.18^GS0.00^LP0.00^DC2 ALLSAINTS INDIA OFFICE^DYIND^TPNDOX^PN4^TW29.70^PO07/02/13 16:00 DIKKAR SINGH^JSD^CZ6^CC^CDCASE SAMPLE^CQ1^CV2.00^CIN^CC^CDBAG SAMPLES^CQ5^CV10.00^CIN^CC^CDBELT SAMPLES^CQ5^CV5.00^CIN^CC^CDLEATHER JACKET SAMPLE^CQ1^CV5.00^CIN^CC^CDFOOTWEAR SAMPLES PAIRS^CQ2^CV8.00^CIN^CC^CDBAG SAMPLES^CQ3^CV3.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11544605^HN3653340^RO05/02/13^RA14:59^SVIX INTERNATIONAL EXPRESS^RECLAIRE S^BP16.52^SC1.90^GS0.00^LP0.00^DC5 THE APPARE LAB LLC^DYTUR^TPNDOX^PN1^TW1.00^PO06/02/13 15:50 NIHAL KANIT DELIVERED^JSD^CZ1^CC^CD2M FABRIC SAMPLE^CQ1^CV2.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^JN11544531^HN3653319^RO05/02/13^RA14:56^SVIX INTERNATIONAL EXPRESS^REKELLY/REGIN^BP14.73^SC1.69^GS3.28^LP0.00^DC3 ALLSAINTS PORTUGAL OFFICE^DYPRT^TPNDOX^PN1^TW2.00^PO07/02/13 9:00 FREDERICO^JSD^CZ2^CC^CDDRESS SAMPLE^CQ1^CV2.00^CIN^CC^CDJERSEY TEE SAMPLES^CQ17^CV17.00^CIN^EYY^L1Your Name^L2NA^L3SENDER'S NAME^L4DEPARTMENT NAME^DT^XX^ZZ" + "ZZ";
     String input = "^TSWED AUG 28 07:02:24 BST 2013^JN14644319^HN3922117^RO23/08/13^RA10:20^DTDESPATCH^RE211007^CAMARTYN PITMAN^SVUKND UK OVERNIGHT - END OF DAY^VTNOT APPLICABLE^PH02076848563^ABN^BP11.00^SC0.00^GS2.34^LP0.000000^PCMOORFIELDS^PA34 NILE STREET^P2^P3^PK0^PSLONDON^PO^PPN1 7TP^PYGBR^PRUNITED KINGDOM^PG ^PU ^PI^PXMARTYN PITMAN^PE02076848575^DCNEW VICTORIA HOSPITAL^DAPHARMACY DEPT^D2184 COOMBE LANE WEST^D3^DK0^DSKINGSTON UPON THAMES^DODO NOT USE FOR MR HORGAN ORDERS^DPKT2 7EG^DYGBR^DRUNITED KINGDOM^DL ^DU ^DIVERY URGENT MEDICAL SUPPLIES^DXPHARMACY DEPARTMENT^DH^DV01^DD27/08/13^DM17:30^SNCITYSPRINT HEALTHCARE^SA58-62 SCRUTTON STREET^S2^SSLONDON^SO^SPEC2A 4PH^SYGBR^SG^SU^TPNDOX ^PN1^TW1.50^NZ1^NI1^HE0^PL0^PD0^CZ1^CC^CDMEDICAL SUPPLIES^CQ1^CV0.00^CIN^EYN^AD23/08/13^AT10:20^AECOLLECTION SCHEDULED ^AD23/08/13^AT14:07^AECOLLECTION SCHEDULED ^AD23/08/13^AT14:07^AECONFIRMED CONSIGNMENT ^AD23/08/13^AT14:08^AECONSIGNMENT MANIFESTED ^AD23/08/13^AT18:16^AEDEPARTURE SCAN LONDON ^AD23/08/13^AT20:38^AEORIGIN SCAN LONDON ^AD23/08/13^AT23:00^AEDEPARTURE SCAN LONDON ^AD23/08/13^AT23:48^AEARRIVAL SCAN BARKING ^AD24/08/13^AT02:39^AEDEPARTURE SCAN BARKING ^AD24/08/13^AT02:40^AEIN TRANSIT TO DESTINATION CROYDON ^AD27/08/13^AT06:30^AEAT DLVY DEPOT & SCHEDULED FOR DLVY CROYDON ^AD27/08/13^AT09:41^AEDELIVERED LONDON ^AD27/08/13^AT09:41^AEPOD: IKQBAL"  + "ZZ";
    String[][] result = commObj.parseOutput(input);
//   commObj.parseJobdetails(result,"C16977","6575661B0F72F767E1394ED14B5E92D9");
}
    /**
     *
     * @param request
     * @param response
     * @param Error
     * @return
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     */
    public boolean generateErrorPage(HttpServletRequest request, HttpServletResponse response,String Error)throws IOException,ServletException
    {
      request.setAttribute("Error",Error);
      //getServletConfig().getServletContext().getRequestDispatcher("/ErrorPage.jsp").forward(request, response);
      return true;
    }

    /**
     *
     * @return
     */
    public static StringBuffer build_Header()
    {
        StringBuffer sHeader=new StringBuffer();

        sHeader.append("<tr>");
        sHeader.append("<td id=\"pageheader\" style=\"Font-size:12px;\" colspan=\"2\">");
        sHeader.append("<b>BOOKING : NextDay & International Courier<b>");
        sHeader.append("<label style=\"padding-left:470px;text-align:right;color:white;\">Help |  <a href=\"Logout\" style=\"color:white;font-weight:bold;\">Logout</a></label>");
        sHeader.append("</td>");
        sHeader.append("</tr>");

//        sHeader.append("<tr>");
//        sHeader.append("<td id=\"PageHeader\" style=\"Font-size:12px;\">");
//	  sHeader.append("<b>BOOKING : NextDay & International Courier<b>");
//        sHeader.append("</td>");
//        sHeader.append("<td style=\"text-align:right\" id=\"PageHeader\" style=\"Font-size:12px;\">Help | <a href=\"Logout\" style=\"color:white;font-weight:bold;\">Logout</a></td>");
//        sHeader.append("</tr>");

        return sHeader;
    }


    /**
     *
     * @return
     * @throws java.sql.SQLException
     */
    public Connection cOpenConnection() throws SQLException
    {

      Connection conDB = null;


      try
      {
          // Opening connection to database by registering the driver.
         Class.forName(bdl.getString("sJDBCDriver")).newInstance();
         conDB  = DriverManager.getConnection(bdl.getString("sJDBCurl")+"user="+bdl.getString("sMySqlUser")+"&password="+bdl.getString("sMySqlPassword"));

      }
      catch (Exception errOpen)
      {
          throw new SQLException("<B>Error opening database connection</B><BR>" +
                          errOpen.toString());
      }

      return conDB;
    }


   //closing database connection..
    /**
     *
     * @param conDB
     * @throws java.sql.SQLException
     */
    public void vCloseConnection(Connection conDB) throws SQLException
   {
       try
       {
           conDB.close();
       }
       catch (Exception errClose)
       {
           throw new SQLException("<B>Error closing database.</B><BR>" + errClose.toString());
       }
   }

    /**
     *
     * @return
     * @throws javax.naming.NamingException
     */
    public DataSource getJdbcConnectionPool() throws NamingException {
    Context c=new InitialContext();
    return (DataSource) c.lookup("java:comp/env/jdbc/connectionPool");
}

    /**
     *
     * @param sSessionId
     * @param sUserId
     * @param sTimestamp
     * @throws java.lang.Exception
     */
    public void writeSessionValues(String sSessionId, String sUserId, String sTimestamp) throws Exception
   {
       Connection conDB = null;
       Statement stmInsert;
       ResultSet rs;
       String sInsert="Insert into srs_session (srs_sessionid,srs_userid,srs_timestamp) values('"+sSessionId+"','"+sUserId+"','"+sTimestamp+"')";
       try
       {
            conDB = cOpenConnection();
            stmInsert= conDB.createStatement();
            stmInsert.executeUpdate(sInsert);
            vCloseConnection(conDB);
       }
       catch(Exception ex)
       {

             throw new Exception("<B>Error in writeSessionValues.</B><BR>" + ex.toString());
       }
   }

   /**
    *
    * @param acc_id
    * @param sessionid
    * @param sCompany
    * @param sAddress1
    * @param sAddress2
    * @param sAddress3
    * @param sTown
    * @param sCounty
    * @param sPostcode
    * @param sCountry
    * @param sContact
    * @param sPhone
    * @param sInstruct
    * @param sLongitude
    * @param sLatitude
    * @param indicator
    * @param sTownKey
    * @throws java.lang.Exception
    */
   public void writeSessionAddress(String acc_id,String sessionid,String sCompany,String sAddress1,String sAddress2,String sAddress3,String sTown,
           String sCounty,String sPostcode,String sCountry,String sContact,String sPhone, String sInstruct,String sLongitude, String sLatitude,
           String indicator,String sTownKey, String sResidentialFlag) throws Exception
   {

       Connection conDB = null;
       Statement stmInsert;
       ResultSet rs;
       String sInsert="";

       sCompany=sReplaceChars(sCompany,"'","''");
       sAddress1=sReplaceChars(sAddress1,"'","''");
       sAddress2=sReplaceChars(sAddress2,"'","''");
       sAddress3=sReplaceChars(sAddress3,"'","''");
       sTown=sReplaceChars(sTown,"'","''");
       sCounty=sReplaceChars(sCounty,"'","''");
       sPostcode=sReplaceChars(sPostcode,"'","''");
       sCountry=sReplaceChars(sCountry,"'","''");
       sContact=sReplaceChars(sContact,"'","''");
       sPhone=sReplaceChars(sPhone,"'","''");
       sInstruct=sReplaceChars(sInstruct,"'","''");
       sTownKey=sReplaceChars(sTownKey,"'","''");

       if(indicator.equals("C"))
       {
            sInsert="Insert into srs_address (srs_owned_by,srs_collection,srs_delivery,srs_server,srs_company,srs_address1," +
                    "srs_address2,srs_address3,srs_town,srs_county,srs_postcode,srs_country,srs_contact,srs_phone,srs_instructions," +
                    "srs_longitude,srs_latitude,srs_sessionid,srs_townkey, srs_residential_flag)" +
                    "values ('"+acc_id+"',1,0,'"+bdl.getString("host")+"','"+sCompany+"','"+sAddress1+"','"+sAddress2+"','"+sAddress3+"'," +
                    "'"+sTown+"','"+sCounty+"','"+sPostcode+"','"+sCountry+"','"+sContact+"','"+sPhone+"','"+sInstruct+"'," +
                    "'"+sLongitude+"','"+sLatitude+"','"+sessionid+"','"+sTownKey+"','"+sResidentialFlag+"')";
       }
       else
       {
            sInsert="Insert into srs_address (srs_owned_by,srs_collection,srs_delivery,srs_server,srs_company,srs_address1," +
                    "srs_address2,srs_address3,srs_town,srs_county,srs_postcode,srs_country,srs_contact,srs_phone,srs_instructions," +
                    "srs_longitude,srs_latitude,srs_sessionid,srs_townkey, srs_residential_flag)" +
                    "values ('"+acc_id+"',0,1,'"+bdl.getString("host")+"','"+sCompany+"','"+sAddress1+"','"+sAddress2+"','"+sAddress3+"'," +
                    "'"+sTown+"','"+sCounty+"','"+sPostcode+"','"+sCountry+"','"+sContact+"','"+sPhone+"','"+sInstruct+"'," +
                    "'"+sLongitude+"','"+sLatitude+"','"+sessionid+"','"+sTownKey+"','"+sResidentialFlag+"')";
       }
       try
       {
            conDB = cOpenConnection();
            stmInsert= conDB.createStatement();
            stmInsert.executeUpdate(sInsert);
            vCloseConnection(conDB);
       }
       catch(Exception ex)
       {

             throw new Exception("<B>Error in writeSessionAddress.</B><BR>" + ex.toString());
       }
   }

   /**
    *
    * @param owned_by
    * @param environment
    * @throws java.lang.Exception
    */
   public void getEntries(String owned_by,String environment) throws Exception
   {
       //retrieve Email and SMS details from last_10_used_entries
       Connection conDB = null;
       Statement stmSelect;
       ResultSet rs;

       //Clear Email and SMS vectors before retrieving entries.
       smsVector.clear();
       emailVector.clear();

       String sSelect="Select type,entry from last_10_used_entries where owned_by='"+owned_by+"' and environment='"+environment+"' and (type='MB' or type='EM')";

       try
       {
           conDB=cOpenConnection();
           stmSelect=conDB.createStatement();
           rs=stmSelect.executeQuery(sSelect);

           while(rs.next())
           {
               if(rs.getString(1).equals("MB"))
                   smsVector.addElement(rs.getString(2));

               if(rs.getString(1).equals("EM"))
                   emailVector.addElement(rs.getString(2));
           }

           vCloseConnection(conDB);
       }
       catch(Exception ex)
       {
           throw new Exception("<B>Error in getEntries.</B><BR>" + ex.toString());
       }

   }

   /**
    *
    * @param owned_by
    * @param environemnt
    * @param sTrackingEmail
    * @param sTrackingSMS
    * @throws java.lang.Exception
    */
   public void addEntries(String owned_by,String environemnt,String sTrackingEmail,String sTrackingSMS) throws Exception
   {
       Connection conDB = null;

       Statement stmInsert;
       Statement stmDelete;
       ResultSet rs,rs1;
       int No_SMS_Entries=0;
       int No_Email_Entries=0;


       String sInsertSMS="Insert into last_10_used_entries values ('"+environment+"','"+owned_by+"','MB','"+sReplaceChars(sTrackingSMS,"'","''")+"',now())";
       String sInsertEmail="Insert into last_10_used_entries values ('"+environment+"','"+owned_by+"','EM','"+sReplaceChars(sTrackingEmail,"'","''")+"',now())";



       try
       {


           No_SMS_Entries=getCount("SMS",owned_by,environment);
           No_Email_Entries=getCount("Email",owned_by,environment);

           conDB=cOpenConnection();
           //if SMS entries less than 10 then add the new entry else delete one entry from the table and add new entry.
           if(No_SMS_Entries<10)
           {
               if(!checkEntryExists("SMS",sTrackingSMS,owned_by,environment) && sTrackingSMS.length()>0)
               {
                   stmInsert=conDB.createStatement();
                   stmInsert.executeUpdate(sInsertSMS);
               }
           }
           else
           {
              //delete one entry from the table
               deleteEntry("SMS",owned_by,environment);

               //add the new entry.
               if(!checkEntryExists("SMS",sTrackingSMS,owned_by,environment))
               {
                   stmInsert=conDB.createStatement();
                   stmInsert.executeUpdate(sInsertSMS);
               }
           }

           vCloseConnection(conDB);
           conDB=cOpenConnection();

           //if Email entries less than 10 then add the new entry else delete one entry from the table and add new entry.
           if(No_Email_Entries<10)
           {
               if(!checkEntryExists("Email",sTrackingEmail,owned_by,environment) && sTrackingEmail.length()>0)
               {
                   stmInsert=conDB.createStatement();
                   stmInsert.executeUpdate(sInsertEmail);
               }
           }
           else
           {
               //delete one entry from the table
               deleteEntry("Email",owned_by,environment);

               //add the new entry.
               if(!checkEntryExists("Email",sTrackingEmail,owned_by,environment))
               {
                   stmInsert=conDB.createStatement();
                   stmInsert.executeUpdate(sInsertEmail);
               }
           }

            vCloseConnection(conDB);
       }
       catch(Exception ex)
       {
           throw new Exception("<B>Error in addEntries.</B><BR>" + ex.toString());
       }
   }

   /**
    *
    * @param sType
    * @param owned_by
    * @param environemnt
    * @return
    * @throws java.lang.Exception
    */
   public int getCount(String sType,String owned_by,String environemnt) throws Exception
   {
       String sSelSMS="Select count(*) from last_10_used_entries where owned_by='"+owned_by+"' and environment='"+environment+"' and type='MB'";
       String sSelEmail="Select count(*) from last_10_used_entries where owned_by='"+owned_by+"' and environment='"+environment+"' and type='EM'";

       Connection conDB = null;
       Statement stmSelect;

       ResultSet rs;
       int No_entries=0;

       try
       {
           conDB=cOpenConnection();
           stmSelect=conDB.createStatement();

           if(sType.equals("SMS"))
               rs=stmSelect.executeQuery(sSelSMS);
           else
               rs=stmSelect.executeQuery(sSelEmail);

           //get entries count
           while(rs.next())
           {
               No_entries=rs.getInt(1);
           }

           rs.close();
           stmSelect.close();
           vCloseConnection(conDB);
       }
       catch(Exception ex)
       {
           throw new Exception("<B>Error in getCount.</B><BR>" + ex.toString());
       }
       return No_entries;
   }

   /**
    *
    * @param sType
    * @param sEntry
    * @param owned_by
    * @param environemnt
    * @return
    * @throws java.lang.Exception
    */
   public boolean checkEntryExists(String sType,String sEntry,String owned_by,String environemnt) throws Exception
   {
       String sSelSMS="Select 1 from last_10_used_entries where owned_by='"+owned_by+"' and environment='"+environment+"' and type='MB' and entry='"+sEntry+"'";
       String sSelEmail="Select 1 from last_10_used_entries where owned_by='"+owned_by+"' and environment='"+environment+"' and type='EM' and entry='"+sEntry+"'";

       Connection conDB = null;
       Statement stmSelect;

       ResultSet rs;
       int iResult=0;
       boolean bExists=false;

       try
       {
           conDB=cOpenConnection();
           stmSelect=conDB.createStatement();

           if(sType.equals("SMS"))
               rs=stmSelect.executeQuery(sSelSMS);
           else
               rs=stmSelect.executeQuery(sSelEmail);

           //get entries count
           while(rs.next())
           {
               iResult=rs.getInt(1);
           }

           if(iResult==1)
               bExists=true;
           else
               bExists=false;

           rs.close();
           stmSelect.close();
           vCloseConnection(conDB);
       }
       catch(Exception ex)
       {
           throw new Exception("<B>Error in checkEntryExists.</B><BR>" + ex.toString());
       }
       return bExists;
   }
   /**
    *
    * @param sType
    * @param owned_by
    * @param environment
    * @throws java.lang.Exception
    */
   public void deleteEntry(String sType,String owned_by,String environment) throws Exception
   {
       String sDelSMS="delete from last_10_used_entries where owned_by='"+owned_by+"' and environment='"+environment+"' and type='MB' limit 1";
       String sDelEmail="delete from last_10_entries where owned_by='"+owned_by+"' and environment='"+environment+"' and type='EM' limit 1";

       Connection conDB = null;
       Statement stmDelete;


       try
       {
           conDB=cOpenConnection();
           stmDelete=conDB.createStatement();

           if(sType.equals("SMS"))
               stmDelete.executeUpdate(sDelSMS);
           else
               stmDelete.executeUpdate(sDelEmail);


           stmDelete.close();
           vCloseConnection(conDB);
       }
       catch(Exception ex)
       {
           throw new Exception("<B>Error in deleteEntry.</B><BR>" + ex.toString());
       }

   }
//   public void parseLogin(String[][] clientStr)
//   {
//        java.lang.String clientConfigString;
//        String tempstr = "";
//        String tag = "";
//        deptVector.clear();
//        refVector.clear();
//        yzVector.clear();
//        inVector.clear();
//        ixVector.clear();
//        mvVector.clear();
//        cfVector.clear();
//
//       //parse the login array and set all the flags.
//       //Customer Name-CC, Customer Phone-PH, Departments Name-DT, Department Flag-DB, Reference flag-RF, Weight Flag-WE,
//
//       //Service Group repeating set
//       //Service Group-YZ, Max Product Value-MV, Indemnity Percentage-IP, Indemnity Minimum Charge-IN, Indemnity Maximum Value-IX,
//       //Cubing Factor-CF,
//
//       //Credit Cards Flag-C2(Y/N/F), CV2 Security Code Required-C3, Post Code required-C4,
//       //AVS Address Mand-C5, E & Y Flag-EY, Prompt for Dangerous Goods-DG, Default VAT Code-DV
//
//        for(int i=0;i<clientStr.length-1;i++)
//        {
//            tag=clientStr[i][0];
//
//            if (tag.equals("CC")) {
////                label_customer.setText( clientStr[i][1] );
//            }
//            if (tag.equals("PH")) {
//                cus_phone=clientStr[i][1].trim();
//            }
//
//            //Tags for E&Y customer.
//            if (tag.equals("L1")) {
//                ey_YourName = clientStr[i][1];
//            }
//            if (tag.equals("L2")) {
//                ey_Phone = clientStr[i][1];
//            }
//            if (tag.equals("L3")) {
//                ey_Reference = clientStr[i][1];
//            }
//            if (tag.equals("L4")) {
//                ey_Department = clientStr[i][1];
//            }
//            if (tag.equals("EY"))
//            {
//                tempstr = clientStr[i][1];
//
//                if (tempstr.equals("Y"))
//                {
//                 if (debug){ System.out.println("Inside EY"); }
//                 EYFlag = true;
//
//                }
//            }
////            if (tag.equals("TD")) {
////                tempstr = clientStr[i][1];
////                if (!(tempstr.equals("Y"))) {
////                    tradingDayFlag = false;
////                }
////            }
////            if (tag.equals("TX")) {
////                tempstr = clientStr[i][1];
////                if (!(tempstr.equals(""))) {
////                    tradeStart = tempstr;
////                }
////            }
////            if (tag.equals("TF")) {
////                tempstr = clientStr[i][1];
////                if (!(tempstr.equals(""))) {
////                    tradeFinish = tempstr;
////                }
////            }
//
//            if (tag.equals("DB")) {
//                departmentFlag = clientStr[i][1];
//                //Ramya remove the below line after testing.
//                //departmentFlag="Y";
//                if (departmentFlag.equals("Y")) {
//                    departmentFlag = "Y";
//                   // addDepartmentFlag = "Y";
//                }
//                if (departmentFlag.equals("R")) {
//                    // free form entry
//                    departmentFlag = "R";
//                   // addDepartmentFlag = "N";
//                }
//                if (departmentFlag.equals("V")) {
//                    departmentFlag = "Y";
//                    //addDepartmentFlag = "N";
//                }
//                if (departmentFlag.equals("F")) {
//                    // free form entry
//                    departmentFlag = "F";
//                   // addDepartmentFlag = "N";
//                }
//            }
//            if (tag.equals("DT"))
//            {
//                tempstr = clientStr[i][1].trim();
//
//                if (tempstr.length() > 0) {
//                    //_Form1_page_jobEntry.choice_department.addItem(tempstr);
//                     //addElement(tempstr);
//                    try
//                    {
//                        deptVector.addElement(tempstr);
//                    }
//                    catch(Exception ex)
//                    {
//                       String sMessage=ex.toString();
//                       System.out.print(sMessage);
//                    }
//                     if (debug) { System.out.println("Dept: "+tempstr);}
//                }
//            }
//
//           if (tag.equals("RF"))
//           {
//                refFlag = clientStr[i][1];
//                if (refFlag.equals("Y")||refFlag.equals("M")||refFlag.equals("F"))
//                    referenceFlag = true;
//                else
//                {
//                    //if (refFlag.equals("N"))
//                     //   referenceFlag = false;
//                    //else
//                        referenceFlag = false;
//                }
//           }
//           if (tag.equals("RN")) {
//                tempstr = clientStr[i][1].trim();
//                int un_len = tempstr.length();
//                if (tempstr.length() > 0) {
//                   refVector.addElement(tempstr);
//                 if (debug) { System.out.println("Ref: "+tempstr);}
//                }
//            }
//
//           if (tag.equals("TM"))
//           {
//                tempstr = clientStr[i][1].trim();
//                if(tempstr.equals("B"))
//                 {
//                    smsAccountFlag = true;
//                    emailAccountFlag = true;
//                 }
//                else
//                   if(tempstr.equals("S"))
//                      smsAccountFlag = true;
//                     else
//                        if(tempstr.equals("E"))
//                            emailAccountFlag = true;
//                        else
//                         {
//                             smsAccountFlag=false;
//                             emailAccountFlag = false;
//                         }
//           }
//
//           if(tag.equals("SC"))
//           {
//               tempstr = clientStr[i][1].trim();
//               if(tempstr.equals("Y"))
//                 storeMessages = true;
//               else
//                 storeMessages = false;
//
//           }
//
//            // Credit Cards flag
//            if (tag.equals("C2"))
//            {
//                creditCardFlag = clientStr[i][1];
//               // creditCardFlag="Y";//ramya defaulted for testing.
//            }
//            // CV2 Security Code Required
//            if (tag.equals("C3"))
//            {
//                tempstr = clientStr[i][1];
//                if (tempstr.equals("Y"))
//                {
//                    cv2SecurityCodeRequiredFlag = true;
//                }
//                else
//                {
//                    cv2SecurityCodeRequiredFlag = false;
//                }
//            }
//
//            // Postcode required
//            if (tag.equals("C4"))
//            {
//                tempstr = clientStr[i][1];
//                if (tempstr.equals("Y"))
//                {
//                    postcodeRequiredFlag = true;
//                }
//                else
//                {
//                    postcodeRequiredFlag = false;
//                }
//            }
//
//            // AVS address mandatory
//            if (tag.equals("C5"))
//            {
//                tempstr = clientStr[i][1];
//                avsAddressMandatoryFlag = false;
//
//            }
//
//            //Service Group Repeating Set
//            if (tag.equals("YZ"))
//            {
//                tempstr = clientStr[i][1];
//                yzVector.addElement(tempstr);
//            }
//
//             // Maximum Value
//            if(tag.equals("MV"))
//            {
//                tempstr = clientStr[i][1];
//                mvVector.addElement(tempstr);
//            }
//
//            //Indemnity Minimum
//            if(tag.equals("IN"))
//            {
//                inVector.addElement(clientStr[i][1]);
//            }
//
//            //Indemnity Maximum
//            if(tag.equals("IX"))
//            {
//                ixVector.addElement(clientStr[i][1]);
//            }
//
//            // Cubing Factor
//            if (tag.equals("CF"))
//            {
//                tempstr = clientStr[i][1];
//                sCubingFactor=tempstr;
//            }
//
//            // Default VAT code
//            if(tag.equals("DV"))
//            {
//                defaultVATcode=clientStr[i][1];
//            }
//
//            //Display prices flag
//            if(tag.equals("PR"))
//            {
//                tempstr = clientStr[i][1];
//                if(tempstr.equals("Y"))
//                    priceFlag=true;
//                else
//                    priceFlag=false;
//            }
//
//        }
//
//        if(yzVector.size()>0)
//        {
//            for(int j=0;j<yzVector.size();j++)
//            {
//                String sTemp=yzVector.elementAt(j).toString().substring(0,1);
//
//                if(sTemp.equals("3"))
//                {
//                   dIndemnityMax1=Double.parseDouble(ixVector.elementAt(j).toString());
//                   dHighValue1=Double.parseDouble(mvVector.elementAt(j).toString());
//
//                }
//
//                if(sTemp.equals("4"))
//                {
//                   dIndemnityMax2=Double.parseDouble(ixVector.elementAt(j).toString());
//                   dHighValue2=Double.parseDouble(mvVector.elementAt(j).toString());
//
//                }
//            }
//        }
//
//   }
//
//   //parse commodity details array
//   public void parseCommodity(String[][] commodityArray)
//   {
//       String tempstr = "";
//       String tag = "";
//
//      //Product API output  ^TS Transaction Stamp ^CC Commodity Code ^CD Commodity Description ^IF Indemnity Flag
//       ccVector.clear();
//       csVector.clear();
//       ifVector.clear();
//
//       for(int i=0;i<commodityArray.length-1;i++)
//       {
//           tag=commodityArray[i][0];
//
//           //Commodity Code
//           if(tag.equals("CC"))
//           {
//               tempstr=commodityArray[i][1].trim();
//
//               ccVector.addElement(tempstr);
//           }
//
//
//           //Commodity Description
//           if(tag.equals("CD"))
//           {
//               tempstr=commodityArray[i][1].trim();
//
//               csVector.addElement(tempstr);
//           }
//
//           //Indemnity Apply Flag
//           if(tag.equals("IF"))
//           {
//               tempstr=commodityArray[i][1].trim();
//
//               ifVector.addElement(tempstr);
//           }
//
//      }
//   }

//   //parse countries array
//   public void parseCountries(String[][] countriesArray)
//   {
//       String tempstr = "";
//       String tag = "";
//
//      //Countries API output  ^TS Transaction Stamp ^CY Country Code ^CN Country Name ^VC VAT Code ^HA High Value Amount
//
//       cyVector.clear();
//       cnVector.clear();
//       vcVector.clear();
//       haVector.clear();
//
//       for(int i=0;i<countriesArray.length-1;i++)
//       {
//           tag=countriesArray[i][0];
//
//           int isize=cyVector.size();
//           int isize1=cnVector.size();
//
//           //Country Code
//           if(tag.equals("CY"))
//           {
//               tempstr=countriesArray[i][1].trim();
//
//               cyVector.addElement(tempstr);
//           }
//
//           //Country Name
//           if(tag.equals("CN"))
//           {
//               tempstr=countriesArray[i][1].trim();
//
//               cnVector.addElement(tempstr.toUpperCase());
//           }
//
//           //VAT Code
//           if(tag.equals("VC"))
//           {
//               tempstr=countriesArray[i][1].trim();
//
//               vcVector.addElement(tempstr);
//           }
//
//           //High Value Amount
//           if(tag.equals("HA"))
//           {
//               tempstr=countriesArray[i][1].trim();
//
//               if(tempstr.length()>0)
//                   haVector.addElement(tempstr);
//           }
//
//      }
//   }


//   //parse array returned from Services API and assign to corresponding Vectors.
//   public void parseServices(String[][] servicesArray)
//   {
//       String tempstr = "";
//       String tag = "";
//
//       //Service tags ^SV Valid Service ^CD Collection Date ^CM Collection Time ^DD Delivery Date ^DM Delivery Time
//       //             ^LP Product Liability ^BP Price ^GS GST/VAT ^SC Total Surcharge
//       //SV value spilts into code: 10 characters and name 30 characters.
//
//       //Remove all the elements of the vectors
//       svVector.clear();
//       svcodeVector.clear();
//       cdVector.clear();
//       cmVector.clear();
//       ddVector.clear();
//       dmVector.clear();
//       lpVector.clear();
//       bpVector.clear();
//       gsVector.clear();
//       scVector.clear();
//
//       for(int i=0;i<servicesArray.length-1;i++)
//       {
//           tag=servicesArray[i][0];
//
//           //Valid Services
//           if(tag.equals("SV"))
//           {
//               tempstr=servicesArray[i][1].trim();
//
//               if(tempstr.length()>0)
//               {
//                   svcodeVector.addElement(tempstr.substring(0,9));     //split the SV value into code and name.
//                   svVector.addElement(tempstr.substring(9,tempstr.length()));
//               }
//               else
//               {
//                   svcodeVector.addElement("");
//                   svVector.addElement("");
//               }
//
//           }
//
//           //Collection Date
//           if(tag.equals("CD"))
//           {
//               tempstr=servicesArray[i][1].trim();
//
//               cdVector.addElement(tempstr);
//           }
//
//           //Collection Time
//           if(tag.equals("CM"))
//           {
//               tempstr=servicesArray[i][1].trim();
//
//               cmVector.addElement(tempstr);
//           }
//
//           //Delivery Date
//           if(tag.equals("DD"))
//           {
//               tempstr=servicesArray[i][1].trim();
//
//               ddVector.addElement(tempstr);
//           }
//
//           //Delivery Time
//           if(tag.equals("DM"))
//           {
//               tempstr=servicesArray[i][1].trim();
//
//               dmVector.addElement(tempstr);
//           }
//
//           //Product Liability
//           if(tag.equals("LP"))
//           {
//               tempstr=servicesArray[i][1].trim();
//
//               lpVector.addElement(tempstr);
//           }
//
//           // Price
//           if(tag.equals("BP"))
//           {
//               tempstr=servicesArray[i][1].trim();
//
//               bpVector.addElement(tempstr);
//           }
//
//           //GST/VAT
//           if(tag.equals("GS"))
//           {
//               tempstr=servicesArray[i][1].trim();
//
//               gsVector.addElement(tempstr);
//           }
//
//           //Total Surcharge
//            if(tag.equals("SC"))
//           {
//               tempstr=servicesArray[i][1].trim();
//
//               scVector.addElement(tempstr);
//           }
//
//           //Agent
//            if(tag.equals("AG"))
//           {
//               tempstr=servicesArray[i][1].trim();
//
//               agVector.addElement(tempstr);
//           }
//
//           //Agent Cut of time
//            if(tag.equals("AT"))
//           {
//               tempstr=servicesArray[i][1].trim();
//
//               atVector.addElement(tempstr);
//           }
//
//
//       }
//   }

   /**
    *
    * @param sMaster
    * @param sFind
    * @param sReplace
    * @return
    */
   public String sReplaceChars(String sMaster, String sFind, String sReplace)
   {
       try
       {
           int nPosition = sMaster.indexOf(sFind);
           if (nPosition != -1)
           {
               return sReplaceChars(sMaster.substring(0, nPosition), sFind, sReplace)
                                   + sReplace +
                      sReplaceChars(sMaster.substring(nPosition + sFind.length()), sFind, sReplace);
           }
           else
               return sMaster;
       }
       catch (Exception errReplace)
       {
           return sMaster;
       }
   }

//   public void getSessionValues(HttpServletRequest request)
//   {
//       HttpSession validSession = request.getSession(false);
//
//       if(request.isRequestedSessionIdValid())
//       {
//           acc_id=(String)validSession.getAttribute("acc_id");
//           pwd=(String)validSession.getAttribute("pwd");
//           sSessionID=(String)validSession.getAttribute("sessionid");
//           loginArray=(String[][])validSession.getAttribute("loginArray");
//       }
//   }


   /**
    *
    * @param rspOutput
    * @throws javax.servlet.ServletException
    * @throws java.io.IOException
    */
   protected void vShowErrorPage(HttpServletResponse rspOutput)
        throws ServletException, IOException
    {
        writeOutputLog(sErrorMessage);
        rspOutput.setContentType("text/html");
        ServletOutputStream strmResponse = rspOutput.getOutputStream();
        strmResponse.println("<HTML>");
        strmResponse.println("<HEAD>");
        strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
        strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");
        strmResponse.println("function enableErrorDiv()");
        strmResponse.println("{document.getElementById(\"divError\").style.display='';document.getElementById(\"aTechical\").style.display='none';}");
        strmResponse.println("</script>");
        strmResponse.println("</HEAD>");
        strmResponse.println("<BODY>");
        strmResponse.println("<table cellpadding=\"4\" height=\"100%\"><tr><td height=\"30%\">");
        strmResponse.println("<H5>CitySprint's NextDay & International booking system is currently unavailable, please contact your local CitySprint ServiceCentre to complete your booking.</H5><BR>");
        strmResponse.println("</td></tr><tr><td height=\"70%\">");
        strmResponse.println("<div style=\"display:none\" id=\"divError\">");
        strmResponse.println(sErrorMessage);
        strmResponse.println("</div>");
        strmResponse.println("<div valign=\"bottom\">");
        strmResponse.println("<a href=\"javascript:enableErrorDiv()\" id=\"aTechical\">Technical Information</a>");
        strmResponse.println("</div>");
        strmResponse.println("</td></tr>");
        strmResponse.println("</BODY></HTML>");
        strmResponse.close();
    }


   /**
    *
    * @param rspOutput
    * @param Error
    * @throws javax.servlet.ServletException
    * @throws java.io.IOException
    * Custom Error Page
    */
   protected void vShowErrorPage(HttpServletResponse rspOutput,Exception Error)
        throws ServletException, IOException
    {
        String sErrorMessage="";
        String NEW_LINE = System.getProperty("line.separator");
        sErrorMessage=Error.getMessage()+NEW_LINE+getCustomStackTrace(Error)+NEW_LINE+Error.getClass().toString();
        writeOutputLog(sErrorMessage);

        rspOutput.setContentType("text/html");
        ServletOutputStream strmResponse = rspOutput.getOutputStream();
        strmResponse.println("<HTML>");
        strmResponse.println("<HEAD>");
        strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
        strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");
        strmResponse.println("function enableErrorDiv()");
        strmResponse.println("{document.getElementById(\"divError\").style.display='';document.getElementById(\"aTechical\").style.display='none';}");
        strmResponse.println("</script>");
        strmResponse.println("</HEAD>");
        strmResponse.println("<BODY>");
        strmResponse.println("<table cellpadding=\"4\" height=\"100%\"><tr><td height=\"30%\">");
        strmResponse.println("<H5>CitySprint's NextDay & International booking system is currently unavailable, please contact your local CitySprint ServiceCentre to complete your booking.</H5><BR>");
        strmResponse.println("</td></tr><tr><td height=\"70%\">");
        strmResponse.println("<div style=\"display:none\" id=\"divError\">");
        strmResponse.println(Error.getMessage()+"<BR>");
        strmResponse.println(getCustomStackTrace(Error)+"<BR>");
        strmResponse.println(Error.getClass().toString()+"<BR>");
        strmResponse.println("</div>");
        strmResponse.println("<div>");
        strmResponse.println("<a href=\"javascript:enableErrorDiv()\" id=\"aTechical\">Technical Information</a>");
        strmResponse.println("</div>");
        strmResponse.println("</td></tr>");
        strmResponse.println("</BODY></HTML>");
        strmResponse.close();
    }

   /**
    *
    * @param rspOutput
    * @param Error
    * @throws javax.servlet.ServletException
    * @throws java.io.IOException
    * Custom Error Page for Reporting section
    */
   protected void vShowErrorPageReporting(HttpServletResponse rspOutput,Exception Error)
        throws ServletException, IOException
    {
        String sErrorMessage="";
        String NEW_LINE = System.getProperty("line.separator");
        sErrorMessage=Error.getMessage()+NEW_LINE+getCustomStackTrace(Error)+NEW_LINE+Error.getClass().toString();
        writeOutputLog(sErrorMessage);
        ServletOutputStream strmResponse = null;
        PrintWriter out = null;
        rspOutput.setContentType("text/html");
        try{
            strmResponse = rspOutput.getOutputStream();
        }catch(IllegalStateException ise){
            out = rspOutput.getWriter();
//            strmResponse = out.get;
        }
        if (strmResponse != null){
        strmResponse.println("<HTML>");
        strmResponse.println("<HEAD>");
        strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
        strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");
        strmResponse.println("function enableErrorDiv()");
        strmResponse.println("{document.getElementById(\"divError\").style.display='';document.getElementById(\"aTechical\").style.display='none';}");
        strmResponse.println("</script>");
        strmResponse.println("</HEAD>");
        strmResponse.println("<BODY>");
        strmResponse.println("<table cellpadding=\"4\" height=\"100%\"><tr><td height=\"30%\">");
        strmResponse.println("<H5>CitySprint's NextDay & International Reporting/Tracking system is currently unavailable, please contact your local CitySprint ServiceCentre to check about your booking.</H5><BR>");
        strmResponse.println("</td></tr><tr><td height=\"70%\">");
        strmResponse.println("<div style=\"display:none\" id=\"divError\">");
        strmResponse.println(Error.getMessage()+"<BR>");
        strmResponse.println(getCustomStackTrace(Error)+"<BR>");
        strmResponse.println(Error.getClass().toString()+"<BR>");
        strmResponse.println("</div>");
        strmResponse.println("<div>");
        strmResponse.println("<a href=\"javascript:enableErrorDiv()\" id=\"aTechical\">Technical Information</a>");
        strmResponse.println("</div>");
        strmResponse.println("</td></tr>");
        strmResponse.println("</BODY></HTML>");
        // Following line is being commented by Adnan against following error:
        // java.lang.IllegalStateException: getWriter() has already been called for this response
        // found on web that you should not close the ServletOutputStream, it is the servlet container who need to do that.
//        strmResponse.close();
        }else{
            out.println("<HTML>");
        out.println("<HEAD>");
        out.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
        out.println("<script LANGUAGE=\"JAVASCRIPT\">");
        out.println("function enableErrorDiv()");
        out.println("{document.getElementById(\"divError\").style.display='';document.getElementById(\"aTechical\").style.display='none';}");
        out.println("</script>");
        out.println("</HEAD>");
        out.println("<BODY>");
        out.println("<table cellpadding=\"4\" height=\"100%\"><tr><td height=\"30%\">");
        out.println("<H5>CitySprint's NextDay & International Reporting/Tracking system is currently unavailable, please contact your local CitySprint ServiceCentre to check about your booking.</H5><BR>");
        out.println("</td></tr><tr><td height=\"70%\">");
        out.println("<div style=\"display:none\" id=\"divError\">");
        out.println(Error.getMessage()+"<BR>");
        out.println(getCustomStackTrace(Error)+"<BR>");
        out.println(Error.getClass().toString()+"<BR>");
        out.println("</div>");
        out.println("<div>");
        out.println("<a href=\"javascript:enableErrorDiv()\" id=\"aTechical\">Technical Information</a>");
        out.println("</div>");
        out.println("</td></tr>");
        out.println("</BODY></HTML>");
        }
    }

    /**
     *
     * @param rspOutput
     * @param Error
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * Custom Error Page
     */
    protected void vShowErrorPage(HttpServletResponse rspOutput,String Error)
        throws ServletException, IOException
    {
        rspOutput.setContentType("text/html");
        ServletOutputStream strmResponse = rspOutput.getOutputStream();
        strmResponse.println("<HTML><BODY>");
        strmResponse.println("<HEAD>");
        strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\"></HEAD>");
        strmResponse.println("<br><br><br><center><H5>"+Error + "</H5></center><BR>");
        strmResponse.println("</BODY></HTML>");
        strmResponse.close();
    }

    /**
     *
     * @param aThrowable
     * @return
     */
    public static String getCustomStackTrace(Exception aThrowable) {
    //add the class name and any message passed to constructor
    final StringBuilder result = new StringBuilder();
    result.append(aThrowable.toString());
    final String NEW_LINE = System.getProperty("line.separator");
    result.append(NEW_LINE);

    //add each element of the stack trace
    for (StackTraceElement element : aThrowable.getStackTrace() ){
      result.append( element );
      result.append( NEW_LINE );
    }
    return result.toString();
  }

    /**
     *
     * @return
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     */
    public String getLocalDirName() throws FileNotFoundException, IOException
   {
      String localDirName;

      //Use that name to get a URL to the directory we are executing in
      java.net.URL myURL = this.getClass().getResource(getClassName());  //Open a URL to the our .class file

      //Clean up the URL and make a String with absolute path name
      localDirName = myURL.getPath();  //Strip path to URL object out

      localDirName = myURL.getPath().replaceAll("%20", " ");  //change %20 chars to spaces

      //Get the current execution directory
      localDirName = localDirName.substring(0,localDirName.lastIndexOf("/"));  //clean off the file name

      //writeOutputLog(localDirName);

      return localDirName;
   }

    /**
     *
     * @return
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     */
    public String getClassName() throws FileNotFoundException, IOException
   {
      String thisClassName;

      //Build a string with executing class's name
      thisClassName = this.getClass().getName();
      thisClassName = thisClassName.substring(thisClassName.lastIndexOf(".") + 1,thisClassName.length());
      thisClassName += ".class";  //this is the name of the bytecode file that is executing

      //writeOutputLog(thisClassName);
      return thisClassName;
   }

    /**
     *
     * @param aContents
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     */
    public void writeOutputLog(String aContents)
                                 throws FileNotFoundException, IOException
     {

        java.util.Date d = new java.util.Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");


        File file;
        FileWriter fw;
        final String NEW_LINE = System.getProperty("line.separator");

        File baseDir = new File(getLocalDirName());
        File logDir = new File(baseDir,"NDILogs");
        boolean bDirExists=logDir.mkdirs();
        int len=d.toString().length();

        String sFileName="console_"+d.toString().substring(0,10)+"_"+d.toString().substring(len-4,len)+".txt";

        try {
                file = new File(logDir,sFileName);

                if (!file.exists())
                {
                    // a new file was created
                    boolean fCreated=file.createNewFile();
                    Writer output = new BufferedWriter(new FileWriter(file));

                    //FileWriter always assumes default encoding is OK!
                    output.write(format.format(d));
                    output.write(NEW_LINE);
                    output.write( aContents );
                    output.close();
                }
                else {
                    Writer output = new BufferedWriter(new FileWriter(file,true));

                    //FileWriter always assumes default encoding is OK!
                    output.write(NEW_LINE);
                    output.write(NEW_LINE);
                    output.write(format.format(d));
                    output.write(NEW_LINE);
                    output.write( NEW_LINE+aContents );
                    output.close();
                }
            }
            catch (Exception e)
            {
                throw new Error("unable to create log file "  +
                                "\n" + e.toString());
            }


    }

    /**
     *
     * @param paramHttp
     * @return
     * @throws java.io.UnsupportedEncodingException
     */
    public String encStr(String paramHttp) throws java.io.UnsupportedEncodingException
    {
        String newparamHttp = paramHttp.replace( '^', ' ' );
        return URLEncoder.encode(newparamHttp.toUpperCase(),"UTF-8");

    }

    /**
     *
     * @param paramHttp
     * @return
     * @throws java.io.UnsupportedEncodingException
     */
    public String encStrLower(String paramHttp) throws java.io.UnsupportedEncodingException
    {
        String newparamHttp = paramHttp.replace( '^', ' ' );
        return URLEncoder.encode(newparamHttp,"UTF-8");

    }

    /**
     *
     * @param cc-credit card number
     * @return
     * encrypts the credit card number before passing to the NDI_Booking API
     */
    public String encryptCreditCard(String cc)
    {
        char[] ccArray = cc.toCharArray();
        java.util.Hashtable hshChar = getCharFromAsciiCodeHashtable();
        java.util.Hashtable hshAscii = getAsciiCodeHashtable();
        String ccAfter = "";
        String ccAscii = "";
        int digitAscii = 0;
        for(int i = 0; i < ccArray.length; i++)
        {
            // get Ascii code of each credit card digit
            ccAscii = hshChar.get(String.valueOf(ccArray[i])).toString();
            //if(debug) { System.out.println("Card digit is " + String.valueOf(ccArray[i]) + " Ascii value is " + ccAscii); }
            digitAscii = Integer.parseInt(ccAscii);
            ccAfter = ccAfter + hshAscii.get(String.valueOf(digitAscii^0x55)).toString();
        }

        return ccAfter;
    }

    /**
     *
     * @return
     *
     */
    public java.util.Hashtable getCharFromAsciiCodeHashtable()
    {
        java.util.Hashtable hshChar = new java.util.Hashtable();

		hshChar.put("!", "33");
		hshChar.put("\"", "34");
		hshChar.put("#", "35");
		hshChar.put("$", "36");
		hshChar.put("%", "37");
		hshChar.put("&", "38");
		hshChar.put("'", "39");
		hshChar.put("(", "40");
		hshChar.put(")", "41");
		hshChar.put("*", "42");
		hshChar.put("+", "43");
		hshChar.put(",", "44");
		hshChar.put("-", "45");
		hshChar.put(".", "46");
		hshChar.put("/", "47");
		hshChar.put("/", "47");
		hshChar.put("0", "48");
		hshChar.put("1", "49");
		hshChar.put("2", "50");
		hshChar.put("3", "51");
		hshChar.put("4", "52");
		hshChar.put("5", "53");
		hshChar.put("6", "54");
		hshChar.put("7", "55");
		hshChar.put("8", "56");
		hshChar.put("9", "57");
		hshChar.put("@", "64");
		hshChar.put("A", "65");
		hshChar.put("B", "66");
		hshChar.put("C", "67");
		hshChar.put("D", "68");
		hshChar.put("E", "69");
		hshChar.put("F", "70");
		hshChar.put("G", "71");
		hshChar.put("H", "72");
		hshChar.put("I", "73");
		hshChar.put("J", "74");
		hshChar.put("K", "75");
		hshChar.put("L", "76");
		hshChar.put("M", "77");
		hshChar.put("N", "78");
		hshChar.put("O", "79");
		hshChar.put("P", "80");
		hshChar.put("Q", "81");
		hshChar.put("R", "82");
		hshChar.put("S", "83");
		hshChar.put("T", "84");
		hshChar.put("U", "85");
		hshChar.put("V", "86");
		hshChar.put("W", "87");
		hshChar.put("X", "88");
		hshChar.put("Y", "89");
		hshChar.put("Z", "90");
		hshChar.put("[", "91");
		hshChar.put("\\", "92");
		hshChar.put("]", "93");
		hshChar.put("^", "94");
		hshChar.put("_", "95");
		hshChar.put("`", "96");
		hshChar.put("a", "97");
		hshChar.put("b", "98");
		hshChar.put("c", "99");
		hshChar.put("d", "100");
		hshChar.put("e", "101");
		hshChar.put("f", "102");
		hshChar.put("g", "103");
		hshChar.put("h", "104");
		hshChar.put("i", "105");
		hshChar.put("j", "106");
		hshChar.put("k", "107");
		hshChar.put("l", "108");
		hshChar.put("m", "109");
		hshChar.put("n", "110");
		hshChar.put("o", "111");
		hshChar.put("p", "112");
		hshChar.put("q", "113");
		hshChar.put("r", "114");
		hshChar.put("s", "115");
		hshChar.put("t", "116");
		hshChar.put("u", "117");
		hshChar.put("v", "118");
		hshChar.put("w", "119");
		hshChar.put("x", "120");
		hshChar.put("y", "121");
		hshChar.put("z", "122");
		hshChar.put("{", "123");
		hshChar.put("|", "124");
		hshChar.put("}", "125");
		hshChar.put("~", "126");


        return hshChar;

    }

    /**
     *
     * @return
     */
    public java.util.Hashtable getAsciiCodeHashtable()
    {
        java.util.Hashtable hshAscii = new java.util.Hashtable();

		hshAscii.put("33", "!");
        hshAscii.put("34", "\"");
        hshAscii.put("35", "#");
		hshAscii.put("36", "$");
		hshAscii.put("37", "%");
		hshAscii.put("38", "&");
		hshAscii.put("39", "'");
		hshAscii.put("40", "(");
		hshAscii.put("41", ")");
		hshAscii.put("42", "*");
		hshAscii.put("43", "+");
		hshAscii.put("44", ",");
		hshAscii.put("45", "-");
		hshAscii.put("46", ".");
		hshAscii.put("47", "/");
		hshAscii.put("47", "/");
		hshAscii.put("48", "0");
		hshAscii.put("49", "1");
		hshAscii.put("50", "2");
		hshAscii.put("51", "3");
		hshAscii.put("52", "4");
		hshAscii.put("53", "5");
		hshAscii.put("54", "6");
		hshAscii.put("55", "7");
		hshAscii.put("56", "8");
		hshAscii.put("57", "9");
		hshAscii.put("64", "@");
		hshAscii.put("65", "A");
		hshAscii.put("66", "B");
		hshAscii.put("67", "C");
		hshAscii.put("68", "D");
		hshAscii.put("69", "E");
		hshAscii.put("70", "F");
		hshAscii.put("71", "G");
		hshAscii.put("72", "H");
		hshAscii.put("73", "I");
		hshAscii.put("74", "J");
		hshAscii.put("75", "K");
		hshAscii.put("76", "L");
		hshAscii.put("77", "M");
		hshAscii.put("78", "N");
		hshAscii.put("79", "O");
		hshAscii.put("80", "P");
		hshAscii.put("81", "Q");
		hshAscii.put("82", "R");
		hshAscii.put("83", "S");
		hshAscii.put("84", "T");
		hshAscii.put("85", "U");
		hshAscii.put("86", "V");
		hshAscii.put("87", "W");
		hshAscii.put("88", "X");
		hshAscii.put("89", "Y");
		hshAscii.put("90", "Z");
		hshAscii.put("91", "[");
		hshAscii.put("92", "\\");
		hshAscii.put("93", "]");
		hshAscii.put("94", "^");
		hshAscii.put("95", "_");
		hshAscii.put("96", "`");
		hshAscii.put("97", "a");
		hshAscii.put("98", "b");
		hshAscii.put("99", "c");
		hshAscii.put("100", "d");
		hshAscii.put("101", "e");
		hshAscii.put("102", "f");
		hshAscii.put("103", "g");
		hshAscii.put("104", "h");
		hshAscii.put("105", "i");
		hshAscii.put("106", "j");
		hshAscii.put("107", "k");
		hshAscii.put("108", "l");
		hshAscii.put("109", "m");
		hshAscii.put("110", "n");
		hshAscii.put("111", "o");
		hshAscii.put("112", "p");
		hshAscii.put("113", "q");
		hshAscii.put("114", "r");
		hshAscii.put("115", "s");
		hshAscii.put("116", "t");
		hshAscii.put("117", "u");
		hshAscii.put("118", "v");
		hshAscii.put("119", "w");
		hshAscii.put("120", "x");
		hshAscii.put("121", "y");
		hshAscii.put("122", "z");
		hshAscii.put("123", "{");
		hshAscii.put("124", "|");
		hshAscii.put("125", "}");
		hshAscii.put("126", "~");

        return hshAscii;

    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void deleteProductDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
    {
         Connection conDB = null;
         Statement stmDelete;
         String sDeleteProduct="Delete from pro_product where pro_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";
         String sDeleteCommodity="Delete from com_commodity where com_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";


        try
        {
            conDB = cOpenConnection();
            stmDelete= conDB.createStatement();
            stmDelete.executeUpdate(sDeleteProduct);
            stmDelete.executeUpdate(sDeleteCommodity);
            vCloseConnection(conDB);
        }
        catch(Exception ex)
        {
            vShowErrorPage(response,ex);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void clearSessionTables(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
    {
         Connection conDB = null;
         Statement stmDelete;
         String sDeleteSession="Delete from srs_session where srs_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";
         String sSessionAddress="Delete from srs_address where srs_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";

        try
        {
            conDB = cOpenConnection();
            stmDelete= conDB.createStatement();
            stmDelete.executeUpdate(sDeleteSession);
            stmDelete.executeUpdate(sSessionAddress);

            deleteProductDetails(request,response);
            vCloseConnection(conDB);
        }
        catch(Exception ex)
        {
            vShowErrorPage(response,ex);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void clearSessionAddress(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
    {
         Connection conDB = null;
         Statement stmDelete;

         String sSessionAddress="Delete from srs_address where srs_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";

        try
        {
            conDB = cOpenConnection();
            stmDelete= conDB.createStatement();
            stmDelete.executeUpdate(sSessionAddress);

            deleteProductDetails(request,response);
            vCloseConnection(conDB);
        }
        catch(Exception ex)
        {
            vShowErrorPage(response,ex);
        }
    }

     /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void deleteSessionAddress(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
    {
         Connection conDB = null;
         Statement stmDelete;

         String sSessionAddress="Delete from srs_address where srs_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";

        try
        {
            conDB = cOpenConnection();
            stmDelete= conDB.createStatement();
            stmDelete.executeUpdate(sSessionAddress);

            vCloseConnection(conDB);
        }
        catch(Exception ex)
        {
            vShowErrorPage(response,ex);
        }
    }
    
    /**
     *
     * @param request
     * @param response
     * @return
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public String processURLRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException
	{
	    String urlUsername = "";
		String urlPassword = "";
		String urlJobNo = "";
		String urlFromDate = "";
		String urlToDate = "";
		String urlAction = "";
		String result="";
                String urlHAWBNo = "";

        try
        {
            if (request.getMethod().equals("GET")) {
                String completeURL = request.getQueryString();
                System.out.println("Complete URL: " + completeURL);

                int pos = 0;
                int end = 0;
                completeURL += "&"; // last character delimeter

                pos = completeURL.indexOf("theaction=");


                if (pos >= 0) {

                    pos = completeURL.indexOf("username=");
                    end = completeURL.indexOf("&", pos + 1);
                    urlUsername = completeURL.substring(pos + 9, end);
                    System.out.println("urlUsername = " + urlUsername);
                    urlUsername = urlUsername.replace("%20", " ");

                    pos = completeURL.indexOf("password=");
                    if (pos >= 0) {
                        end = completeURL.indexOf("&", pos + 1);
                        urlPassword = completeURL.substring(pos + 9, end);
                        urlPassword = urlPassword.replace("%20", " ");
                    }

                    pos = completeURL.indexOf("jbn=");
                    if (pos >= 0) {
                        end = completeURL.indexOf("&", pos + 1);
                        urlJobNo = completeURL.substring(pos + 4, end);
                        urlJobNo = urlJobNo.replace("%20", " ");
                    }

                    pos = completeURL.indexOf("hawbno=");
                    if (pos >= 0) {
                        end = completeURL.indexOf("&", pos + 1);
                        urlHAWBNo = completeURL.substring(pos + 7, end);
                        urlHAWBNo = urlHAWBNo.replace("%20", " ");
                    }

                    pos = completeURL.indexOf("theaction=");
                    if (pos >= 0) {
                        end = completeURL.indexOf("&", pos + 1);
                        urlAction = completeURL.substring(pos + 10, end);
                    }

                    pos = completeURL.indexOf("fromdate=");
                    if (pos >= 0) {
                        end = completeURL.indexOf("&", pos + 1);
                        urlFromDate = completeURL.substring(pos + 9, end);
                        urlFromDate = urlFromDate.replace("%2f", "/");
                        urlFromDate = urlFromDate.replace("%2F", "/");
                        urlFromDate = urlFromDate.replace("%20", " ");
                    }


                    pos = completeURL.indexOf("todate=");
                    if (pos >= 0) {
                        end = completeURL.indexOf("&", pos + 1);
                        urlToDate = completeURL.substring(pos + 7, end);
                        urlToDate = urlToDate.replace("%2f", "/");
                        urlToDate = urlToDate.replace("%2F", "/");
                        urlToDate = urlToDate.replace("%20", " ");
                    }

                    //Password can be blank
                    if (urlUsername.length() > 0 && urlAction.equals("cssearch")) {
                        if (newLogin(urlUsername, urlPassword, request, response)) {

                            if ((urlHAWBNo.trim().length() > 0) && (urlAction.equals("cssearch"))) {
                                //Ramya 26/07/2010 Change job number account activity to Tracking tab as opposed to Reporting tab
                                //result = "group=1&cgno=" + urlJobNo.trim() + "&cInclude=det_hawbno,det_readyon,det_dcountry,det_dcomp,det_descrip,det_servcode,det_totpieces,det_totweight,det_pod,det_totcharge,";
                                result = "cgno=" + urlHAWBNo.trim() + "&sAction=getJobDetails";
                            } else if (urlFromDate.length() > 0 && (urlAction.equals("cssearch"))) {

                                result = "group=2&fmdate=" + encStr(urlFromDate) + "&todate=" + encStr(urlToDate) + "&cInclude=det_hawbno,det_readyon,det_dcountry,det_dcomp,det_descrip,det_servcode,det_totpieces,det_totweight,det_pod,det_totcharge,";
                            }
                        } else {
                            result = "false";
                        }

                    } else {
                        result = "";
                    }

                } else {
                    //If from quiktrak

                    pos=completeURL.indexOf("CK=");

                    if (pos >= 0) {

                        pos = completeURL.indexOf("CK=");
                        end = completeURL.indexOf("&", pos + 1);
                        urlUsername = completeURL.substring(pos + 3, end);
                        System.out.println("urlUsername = " + urlUsername);
                        urlUsername = urlUsername.replace("%20", " ");

                        pos = completeURL.indexOf("wwhawb=");
                        if (pos >= 0) {
                            end = completeURL.indexOf("&", pos + 1);
                            urlJobNo = completeURL.substring(pos + 7, end);
                            urlJobNo = urlJobNo.replace("%20", " ");
                        }
                    }

                    if(urlUsername.length()>0 && urlJobNo.length()>0)
                    {

                        HttpSession checkSession = request.getSession(false);
                        if (checkSession == null) {
                            HttpSession newSession = request.getSession(true);
//                            newSession.setAttribute("acc_id", urlUsername);
//                            newSession.setAttribute("pwd", "");
                            newSession.setAttribute("qacc_id", urlUsername);
                            String sSessionID = newSession.getId();
                            newSession.setAttribute("sessionid", sSessionID);
                            writeOutputLog("QuikTrak with new session created: "+sSessionID);
                        } else {
                            String sessionid = checkSession.getAttribute("sessionid") == null ? "" : (String) checkSession.getAttribute("sessionid");
                            if (sessionid.equals("")) {
                                HttpSession newSession = request.getSession(true);
//                            newSession.setAttribute("acc_id", urlUsername);
//                            newSession.setAttribute("pwd", "");
                                newSession.setAttribute("qacc_id", urlUsername);
                                String sSessionID = newSession.getId();
                                newSession.setAttribute("sessionid", sSessionID);
                                writeOutputLog("QuikTrak with new session created: "+sSessionID);
                            } else {
                                checkSession.setAttribute("qacc_id", urlUsername);
                                writeOutputLog("QuikTrak with existing session: "+sessionid);
                            }
                        }
                        //result="chkid="+urlJobNo+",&sAction=PrintConsignment";
                        result="chkid="+urlJobNo+",&sAction=QuikTrak";

                    }
                    else
                        result = "";
                }
            } else if (request.getMethod().equals("POST")) {
                urlAction = request.getParameter("theaction") == null ? "" : request.getParameter("theaction").toString();
                urlUsername = request.getParameter("username") == null ? "" : request.getParameter("username").toString();
                urlPassword = request.getParameter("password") == null ? "" : request.getParameter("password").toString();
                urlJobNo = request.getParameter("jbn") == null ? "" : request.getParameter("jbn").toString();
                urlHAWBNo = request.getParameter("hawbno") == null ? "" : request.getParameter("hawbno").toString();
                urlFromDate = request.getParameter("fromdate") == null ? "" : request.getParameter("fromdate").toString();
                urlToDate = request.getParameter("todate") == null ? "" : request.getParameter("todate").toString();

                if (urlAction.equals("cssearch")) {
                    //Password can be blank
                    if (urlUsername.length() > 0 && urlAction.equals("cssearch")) {
                        if (newLogin(urlUsername, urlPassword, request, response)) {

                            if ((urlHAWBNo.trim().length() > 0) && (urlAction.equals("cssearch"))) {
                                //Ramya 26/07/2010 Change job number account activity to Tracking tab as opposed to Reporting tab
                                //result = "group=1&cgno=" + urlJobNo.trim() + "&cInclude=det_hawbno,det_readyon,det_dcountry,det_dcomp,det_descrip,det_servcode,det_totpieces,det_totweight,det_pod,det_totcharge,";
                                result = "cgno=" + urlHAWBNo.trim() + "&sAction=getJobDetails";
                            } else if (urlFromDate.length() > 0 && (urlAction.equals("cssearch"))) {

                                result = "group=2&fmdate=" + encStr(urlFromDate) + "&todate=" + encStr(urlToDate) + "&cInclude=det_hawbno,det_readyon,det_dcountry,det_dcomp,det_descrip,det_servcode,det_totpieces,det_totweight,det_pod,det_totcharge,";
                            }
                        } else {
                            result = "false";
                        }

                    } else {
                        result = "";
                    }

                } else {
                    result = "";
                }
            }

        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }

        return result;
	}

    /**
     *
     * @param d
     * @return
     */
    public String roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#0.00#");
        return (twoDForm.format(d));
    }

    public String getMaskedCreditCardNo(String sCardNumber)
    {
        String sMaskedCardNo = "";
        String sPaddingCharaters = "";
        if(sCardNumber.length()>0)
            sMaskedCardNo = sCardNumber.substring(sCardNumber.length()-4, sCardNumber.length());

        for(int i = 1;i<=sCardNumber.length()-4;i++)
        {
            sPaddingCharaters = sPaddingCharaters + "*";
        }

        return(sPaddingCharaters+sMaskedCardNo);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try
        {
        processRequest(request, response);
        }
        catch(Exception ex)
        {
            vShowErrorPage(response,ex);
        }
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         try
        {
        processRequest(request, response);
        }
        catch(Exception ex)
        {
            vShowErrorPage(response,ex);
        }
    }

    /** Returns a short description of the servlet.
     * @return
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}