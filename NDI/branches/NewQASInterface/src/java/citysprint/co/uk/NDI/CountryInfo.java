package citysprint.co.uk.NDI;

/*
 * CountryInfo.java
 *
 * Created on 13 August 2008, 10:57
 */

import citysprint.co.uk.NDI.*;
import java.beans.*;
import java.io.Serializable;
import java.util.Vector;
import java.io.*;
/**
 * @author ramyas
 */
public class CountryInfo extends CommonClass implements Serializable {
   
    private Vector cyVector = new Vector();           //Country Code
    private Vector cnVector = new Vector();           //Country Name
    private Vector vcVector = new Vector();           //VAT code
    private Vector haVector = new Vector();           //High Value Amount
    private Vector ntVector = new Vector();           //Notes
    private Vector cyntVector = new Vector();         //Country Code sequence for Notes

    /**
     *
     * @return
     */
    public Vector getCyntVector() {
        return cyntVector;
    }
    /**
     *
     * @return
     */
    public Vector getNtVector() {
        return ntVector;
    }

    /**
     * Getter for property cyVector .
     * @return Value of property cyVector .
     */

    public Vector getCyVector () {
        return this.cyVector ;
    }

     /**
     * Getter for property cnVector .
     * @return Value of property cnVector .
     */
    public Vector getCnVector () {
        return this.cnVector ;
    }
    
    /**
     * Getter for property vcVector .
     * @return Value of property vcVector .
     */
    public Vector getVcVector () {
        return this.vcVector ;
    }
    
    /**
     * Getter for property haVector .
     * @return Value of property haVector .
     */
    public Vector getHaVector () {
        return this.haVector ;
    }
   
    
   //parse countries array
    /**
     *
     * @param countriesArray
     * @throws java.io.IOException
     */
    public void parseCountries(String[][] countriesArray) throws IOException {
        String tempstr = "";
        String tag = "";
        String cyCode = "";
        //Countries API output  ^TS Transaction Stamp ^CY Country Code ^CN Country Name ^VC VAT Code ^HA High Value Amount
        //^NT Notes

        cyVector.clear();
        cnVector.clear();
        vcVector.clear();
        haVector.clear();
        ntVector.clear();
        cyntVector.clear();

        try {

            for (int i = 0; i < countriesArray.length - 1; i++) {
                tag = countriesArray[i][0];


                //Country Code
                if (tag.equals("CY")) {
                    tempstr = countriesArray[i][1].trim();
                    cyVector.addElement(tempstr);
                    cyCode=tempstr;
                }

                //Country Name
                if (tag.equals("CN")) {
                    tempstr = countriesArray[i][1].trim();

                    cnVector.addElement(tempstr.toUpperCase());
                }

                //VAT Code
                if (tag.equals("VC")) {
                    tempstr = countriesArray[i][1].trim();

                    vcVector.addElement(tempstr);
                }

                //High Value Amount
                if (tag.equals("HA")) {
                    tempstr = countriesArray[i][1].trim();

                    if (tempstr.length() > 0) {
                        haVector.addElement(tempstr);
                    }
                }

                //Notes
                if (tag.equals("NT")) {
                    tempstr = countriesArray[i][1].trim();

                    if (tempstr.length() > 0) {
                        ntVector.addElement(tempstr);
                        cyntVector.addElement(cyCode);
                    }
                }


            }
        } catch (Exception ex) {
           writeOutputLog("Error in parseCountries:"+ex.getMessage());

        }
    }
}
