/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package citysprint.co.uk.NDI;


import citysprint.co.uk.NDI.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import citysprint.co.uk.NDI.CountryInfo;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;

/**
 *
 * @author ramyas
 */
public class CountryNotes extends CommonClass {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (isLogin(request)) {

                //initialize config values
                initializeConfigValues(request, response);

                LoadPage(request, response);
            } else {
                writeOutputLog("Session expired redirecting to index page");
                response.sendRedirect("index.jsp");
            }
        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void LoadPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");
        String cyCode = request.getParameter("CY") == null ? "" : request.getParameter("CY").toString();

        CountryInfo country = new CountryInfo();
        Vector notes = new Vector();

        HttpSession validSession = request.getSession(false);

        if (validSession != null) {
           
            country.parseCountries((String[][]) validSession.getAttribute("getCountriesArray"));

            
            validSession.setAttribute("bCountryAccepted", "true");
            validSession.setAttribute("CountryValue", cyCode);

            if (cyCode.length() > 0) {
                if (country.getCyntVector().contains(cyCode)) {
                    int index = country.getCyntVector().indexOf(cyCode);
                    while (index < country.getCyntVector().size()) {
                        if (country.getCyntVector().elementAt(index).equals(cyCode)) {
                            notes.add(country.getNtVector().elementAt(index));
                        }
                        index++;
                    }
                }
            }

            if (notes.size() > 0) {
                strmResponse.println("<html>");
                strmResponse.println("<head>");
                strmResponse.println("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
                strmResponse.println("    <title>CitySprint - Price Details</title>");
                strmResponse.println("    <link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
                strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");

                strmResponse.println("  //Accept Terma");
                strmResponse.println("  function acceptTerms()");
                strmResponse.println("  {");
                //strmResponse.println("     var chkTerms=document.getElementById(\"chkTerms\");");

                //strmResponse.println("     if(chkTerms.checked)");
                //strmResponse.println("     {");
                //strmResponse.println("        alert('Country Information');");
                strmResponse.println("          window.parent.document.getElementById(\"CountryValue\").value=\""+cyCode+"\";");
                strmResponse.println("          window.parent.document.getElementById(\"bCountryAccepted\").value=\"true\"");
                //strmResponse.println("        alert('Country Information');");
                strmResponse.println("          window.parent.enableBooking();");
                strmResponse.println("          window.parent.Services('QAS');");
                strmResponse.println("          return true;");
                //strmResponse.println("     }");
                //strmResponse.println("     else");
                //strmResponse.println("     {");
                //strmResponse.println("        alert('Please read and accept the Country Information');");
                //strmResponse.println("        return false;");
                //strmResponse.println("     }");

                strmResponse.println("  }");
                strmResponse.println("</script>");

                strmResponse.println("</head>");
                strmResponse.println("<body style=\"padding-left:14px\">");

                strmResponse.println("<div style='overflow:auto;height:350px;'><br><br>");
                strmResponse.println("<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" height=\"20\">");
               
                
                for (int i = 0; i <= notes.size() - 1; i++) {

                    strmResponse.println("       <tr>");
                    strmResponse.println("            <td>");
                    strmResponse.println(notes.elementAt(i).toString());
                    strmResponse.println("            </td>");
                    strmResponse.println("        </tr>");

                }

                
                strmResponse.println("       </table>  ");
                strmResponse.println("       </div>  ");
                strmResponse.println("<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\">");
                strmResponse.println("       <tr>");
                strmResponse.println("            <td>");
                //strmResponse.println(" <input type=\"checkbox\" id=\"chkTerms\"> Accept Country Information");
                strmResponse.println("</td>");
                strmResponse.println("</tr>");
                strmResponse.println("<tr><td>");
                strmResponse.println("<br><center> <input type=\"button\" id=\"btn\" value=\"OK\" onclick=\"if(acceptTerms())parent.winCountry.close();\">&nbsp;<input type=\"button\" id=\"btn\" value=\"Cancel\" onclick=\"parent.enableBooking();parent.winCountry.close();\"></center>");
                strmResponse.println("</td>");
                strmResponse.println("</tr>");
                strmResponse.println("</table>");
                strmResponse.println("</body>");
                strmResponse.println("</html>");

            }
            else
            {
                strmResponse.println("<script>parent.document.getElementById(\"CountryValue\").value=\""+cyCode+"\";parent.document.getElementById(\"bCountryAccepted\").value=\"true\";parent.enableBooking();parent.Services('QAS');parent.winCountry.close();</script>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
