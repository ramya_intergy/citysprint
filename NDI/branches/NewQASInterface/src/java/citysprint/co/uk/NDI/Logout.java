package citysprint.co.uk.NDI;

/*
 * Logout.java
 *
 * Created on 14 June 2008, 14:57
 */

import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.util.Enumeration;
/**
 *
 * @author ramyas
 * @version
 */
public class Logout extends CommonClass {
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       
        if(isLogin(request))
        {
            //initialize config values
            initializeConfigValues(request,response);

            //Read session values
            //getSessionValues(request);
            clearSessionTables(request,response);

            HttpSession validSession = request.getSession(false);
            
            if(validSession!=null)
            {
                try
                {

                    Enumeration attributeNames=validSession.getAttributeNames();
                    while(attributeNames.hasMoreElements())
                    {
                        String sAttribute=attributeNames.nextElement().toString();
                        validSession.removeAttribute(sAttribute);
                    }
                }
                catch(Exception e) { }
            }
            
            validSession.invalidate();

            request.setAttribute("bFlag", true);
            getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
        }
        else
        {
            request.setAttribute("bFlag", true);
            getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     * @return
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
