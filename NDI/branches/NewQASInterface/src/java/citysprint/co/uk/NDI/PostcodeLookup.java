package citysprint.co.uk.NDI;

/*
 * PostcodeLookup.java
 *
 * Created on 5 September 2007, 14:36
 */

import citysprint.co.uk.NDI.QAS.QASAddress;
import citysprint.co.uk.NDI.QAS.QASConcrete;
import citysprint.co.uk.NDI.QAS.QASRequest;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;
import com.qas.proweb.servlet.*;
import com.qas.proweb.*;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author ramyas
 * @version
 */
public class PostcodeLookup extends CommonClass {
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    
    protected String[] asMonikers = null;	
    /**
     *
     */
    protected String[] asText = null;
    /**
     *
     */
    protected String[] asPostcodes = null;
    /**
     *
     */
    protected String[] asLines = null;
    /**
     *
     */
    protected String[] asLabels = null;
    /**
     *
     */
    protected String VerifyInfo="";
    /**
     *
     */
    protected String RouteInfo="";

    protected String sContact = "", sPhone = "", sCompany = "", sAddress = "", sAddress1 = "", sTown = "", sMobile = "", sPostcode = "", sCountry = "", sEmail = "", sAddress2 = "", sCounty = "", sInstruct="";
    
    protected String sMessage="", sLatitude="", sLongitude="";
   
    protected String sPage="";
  
    String sAction_sub = "", sUCompany = "";
    //New QAS Parameters
    List<QASAddress> qasOutput = new ArrayList<QASAddress>();
    int iStatusCode, iKey=0;
    String sQASMessage = "";
    String sKey = "";

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (isLogin(request)) {
            String sAction = request.getParameter(Constants.SACTION) == null ? "" : request.getParameter(Constants.SACTION).toString();

            //initialize config values
            initializeConfigValues(request, response);
                    
            if (sAction.equals("Confirm")) {
                Confirm(request, response);
            } else {
                validateAddress(request, response);
            }
        } else{
                writeOutputLog("Session expired redirecting to index page");
                response.sendRedirect("index.jsp");
            }
    }
    
    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void LoadPage(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
    {
        ServletOutputStream strmResponse=response.getOutputStream();//response.getOutputStream();
        response.setContentType("text/html");

        String sAction_sub=request.getParameter(Constants.SACTION_SUB)==null?"":request.getParameter(Constants.SACTION_SUB).toString();
        String sType=request.getParameter(Constants.STYPE)==null?"":request.getParameter(Constants.STYPE).toString();
        
        
        if(qasOutput.size()==1)
              sMessage="Your address has been validated. Please click confirm to select the validated address";
        else
            if(qasOutput.size()>1 && (!sAction_sub.equals("ShowAddress")))
                sMessage="Please select an address from the list.";
            else
                if(qasOutput.size()==0)
                    sMessage="Your address cannot be validated " + sQASMessage +". Please modify the address fields and try again.";
        
        strmResponse.println("<html>");
        strmResponse.println("<head>");
        strmResponse.println("<title>CitySprint - Postcode Lookup</title>");
        strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
        strmResponse.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
        strmResponse.println(" <style type=\"text/css\">    ");
        strmResponse.println("    .pg-normal {");
        strmResponse.println("        color: #0059AD;");
        strmResponse.println("        font-weight: normal;");
        strmResponse.println("        text-decoration: none; ");
        strmResponse.println("        cursor: pointer;   ");
        strmResponse.println("    }");
        strmResponse.println("    .pg-selected {");
        strmResponse.println("        color: #0059AD;");
        strmResponse.println("         font-weight: bold;      ");
        strmResponse.println("        text-decoration: underline;");
        strmResponse.println("        cursor: pointer;");
        strmResponse.println("    }");
        strmResponse.println("html { overflow-x: hidden;overflow-y: auto;  }");
        strmResponse.println("</style>");
        strmResponse.println("<script type=\"text/javascript\" src=\"JS/paging.js\"></script>");
        strmResponse.println("</head>");
        strmResponse.println("<body style=\"width:375px;\">");
        strmResponse.println("<br>");
        
        if(sMessage==null)
             strmResponse.println("<br>");
        else
        {
            strmResponse.println("&nbsp;&nbsp;&nbsp;&nbsp;");
            strmResponse.println("<label id=\"lblMessage\" style=\"color:green;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;\">"+sMessage+"</label><br>");
            strmResponse.println("<br>");
        }
        
        if(qasOutput.size()>1 && !sAction_sub.equals("ShowAddress"))
        {
            //if picklist is available for search results
            int size;
            
            if(qasOutput.size()>150)
                size=150;
            else
                size=qasOutput.size();

             if(size>15)
                 strmResponse.println("<div id=\"pageNavPosition\" style=\"padding-left:20px\"></div><br>");
             else
                 strmResponse.println("<div id=\"pageNavPosition\" style=\"padding-left:20px;display:none;\"></div><br>");

             strmResponse.println("<table id=\"picklistTable\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"99%\" align=\"center\">");
             strmResponse.println("<tr id=\"trheading\"><td id=\"boxtitle\">Address</td><td id=\"boxtitle\">Postcode</td></tr>");
             
            for(int i=0;i<size;i++)
            {

                 try
                    {

                             sAddress1=getQASDisplayAddress(qasOutput.get(i)).toUpperCase();

                            if(sAddress1.length()>0)
                            {
                                strmResponse.println("<tr><td><a href='javascript:QASlist_Click(\""+qasOutput.get(i).getsKey()+"\");'>"+sAddress1+"</a>&nbsp;</td><td >");
                                strmResponse.println(qasOutput.get(i).getsPostcode());
                                strmResponse.println("</td></tr>");
                            }

                            else
                            {
                                if(asText.length==1)
                                {
                                    strmResponse.println("<tr><td colspan=\"2\" style=\"color:green;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;\">No Matches Found.  Please try entering building number without the street name, and the postcode");
                                    strmResponse.println("</td></tr>");

                                    strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">document.getElementById(\"pageNavPosition\").style.display='none';" +
                                            "document.getElementById(\"lblMessage\").innerHTML=\"\";" +
                                            "document.getElementById(\"trheading\").style.display='none';</script>");
                                }
                            }



                    }
                    catch (Exception e)
                    {
//                            System.err.println("~~~~~~~~~ Controller:command exception ~~~~~~~~~~");
//                            e.printStackTrace();
//                            System.err.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//                            throw new ServletException(e);
                        vShowErrorPage(response, e);
                    }


                 
            }
             
             //strmResponse.println("<tr><td><input type=\"button\" name=\"btnCancel\" value=\"New Search\" id=\"btn\" onclick=\"javascript:cancel_click();\"></td></tr>");
             strmResponse.println("</table>");
            
        }
        
        if((qasOutput.size()==1) || (sAction_sub.equals("ShowAddress") && sKey.length()>0))
             strmResponse.println("<table id=entryTable style=\"border:1px solid #666666;padding:5px 5px 5px 5px;background-color:#EFEFEF;\" cellpadding=2 width=\"99%\" align=\"center\">");
        else
            strmResponse.println("<table id=entryTable style=\"display:none;border:1px solid #666666;padding:5px 5px 5px 5px;background-color:#EFEFEF;\" cellpadding=2 width=\"99%\" align=\"center\">");
        
         strmResponse.println("</tr>");
        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Name *</td><td>");
        
        if(qasOutput.size()>0)
            strmResponse.println("<input type=\"text\" name=\"txtCompany\" id=\"txtCompany\" size=\"50\" value='"+sCompany+"' disabled=\"disabled\">");
        else
            strmResponse.println("<input type=\"text\" name=\"txtCompany\" id=\"txtCompany\" size=\"50\" value='"+sCompany+"'>");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Address1 *</td><td>");
        if(qasOutput.size()>0)
            strmResponse.println("<input type=\"text\" name=\"txtAddress1\" id=\"txtAddress1\" size=\"50\" value='"+sAddress+"' disabled=\"disabled\">");
        else
            strmResponse.println("<input type=\"text\" name=\"txtAddress1\" id=\"txtAddress1\" size=\"50\" value='"+sAddress+"'>");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Address2</td><td>");
        if(qasOutput.size()>0)
            strmResponse.println("<input type=\"text\" name=\"txtAddress2\" id=\"txtAddress2\" size=\"50\" value='"+sAddress2+"' disabled=\"disabled\">");
        else
            strmResponse.println("<input type=\"text\" name=\"txtAddress2\" id=\"txtAddress2\" size=\"50\" value='"+sAddress2+"'>");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Town *</td><td>");
        if(qasOutput.size()>0)
            strmResponse.println("<input type=\"text\" name=\"txtTown\" id=\"txtTown\" size=\"40\" value='"+sTown+"' disabled=\"disabled\">");
        else
            strmResponse.println("<input type=\"text\" name=\"txtTown\" id=\"txtTown\" size=\"40\" value='"+sTown+"'>");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">County *</td><td>");
        if(qasOutput.size()>0)
            strmResponse.println("<input type=\"text\" name=\"txtCounty\" id=\"txtCounty\" size=\"40\" value='"+sCounty+"' disabled=\"disabled\">");
        else
            strmResponse.println("<input type=\"text\" name=\"txtCounty\" id=\"txtCounty\" size=\"40\" value='"+sCounty+"'>");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Postcode *</td><td>");
        if(qasOutput.size()>0)
            strmResponse.println("<input type=\"text\" name=\"txtPostcode\" id=\"txtPostcode\" size=\"20\" value='"+sPostcode+"' disabled=\"disabled\">");
        else
            strmResponse.println("<input type=\"text\" name=\"txtPostcode\" id=\"txtPostcode\" size=\"20\" value='"+sPostcode+"'> &nbsp; <input name=\"btnPostcodeLookup\" type=\"button\" id=\"btn\" value=\"Postcode Lookup\" onclick=\"javascript:QASPostcodeLookup();\">");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Country</td><td>");
        strmResponse.println(""+Constants.COUNTRY_VALUE+"");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");
        strmResponse.println("<tr>");
        strmResponse.println("<td>");
        strmResponse.println("</td>");
        strmResponse.println("<td>");
        
        if(qasOutput.size()==1 || sAction_sub.equals("ShowAddress"))
        {
            strmResponse.println("<input type=\"button\" name=btnEntry value=\"Confirm\" id=\"btn\" onclick=\"javascript:ConfirmAddress();\">");
            strmResponse.println("&nbsp;&nbsp;&nbsp;&nbsp;");
         
        }
        
       
        strmResponse.println("</td>");
        strmResponse.println("</tr>");
        strmResponse.println("</table>");
        strmResponse.println("<br><br><center><a href=\"javascript:window.close();\">Return To Booking Screen</a>");
        if(VerifyInfo.equals(SearchResult.InteractionRequired) || VerifyInfo.equals(SearchResult.Verified))
            strmResponse.println("&nbsp;&nbsp;<a href=\"javascript:history.back(1);\">Return To Search Results</a></center>");

        strmResponse.println("<script type=\"text/javascript\">");
        strmResponse.println("var pager = new Pager('picklistTable', 15);");
        strmResponse.println("pager.init(); ");
        strmResponse.println("pager.showPageNav('pager', 'pageNavPosition'); ");
        strmResponse.println("pager.showPage(1);");
        strmResponse.println("</script>");

        strmResponse.println("</body>");
        
        strmResponse.println("<script language=javascript>");
        
        //QAS Pick List Form
        
        strmResponse.println("function QASlist_Click(iKey)");
        strmResponse.println("{");
        //strmResponse.println("alert('QASlist');");
        strmResponse.println("document.PickListForm.iKey.value=iKey;");
        strmResponse.println("document.PickListForm.submit();");
        strmResponse.println("}");
        
        strmResponse.println("function ConfirmAddress()"); 
        strmResponse.println("{");
        strmResponse.println("var txtCompany=document.getElementById(\"txtCompany\");");
        strmResponse.println("var txtAddress1=document.getElementById(\"txtAddress1\");");
        strmResponse.println("var txtAddress2=document.getElementById(\"txtAddress2\");");
        strmResponse.println("var txtTown=document.getElementById(\"txtTown\");");
        strmResponse.println("var txtCounty=document.getElementById(\"txtCounty\");");
        strmResponse.println("var txtPostcode=document.getElementById(\"txtPostcode\");");
        
        strmResponse.println("var sCompany=txtCompany.value;");
        strmResponse.println("var sAddress1=txtAddress1.value;");
        strmResponse.println("var sAddress2=txtAddress2.value;");
        strmResponse.println("var sTown=txtTown.value;");
        strmResponse.println("var sCounty=txtCounty.value");
        strmResponse.println("var sPostcode=txtPostcode.value;");
        
        strmResponse.println("document.ConfirmForm.sCompanyName.value=sCompany");
        strmResponse.println("document.ConfirmForm.sAddress1.value=sAddress1");
        strmResponse.println("document.ConfirmForm.sAddress2.value=sAddress2");
        strmResponse.println("document.ConfirmForm.sTown.value=sTown");
        strmResponse.println("document.ConfirmForm.sCounty.value=sCounty");
        strmResponse.println("document.ConfirmForm.sPostcode.value=sPostcode");
        strmResponse.println("document.ConfirmForm.submit();");
        strmResponse.println("}");
        
        strmResponse.println("</script>");

       

        //QAS Pick List Form 
        
        strmResponse.println("<form name=\"PickListForm\" method=\"POST\" action=\""+Constants.srvPostcodeLookup+"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.SACTION_SUB+"\" value=\"ShowAddress\">");
        strmResponse.println("<input type=hidden name=\""+Constants.VERIFY_INFO+"\" value=\""+SearchResult.InteractionRequired+"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.COMMAND+"\" value=\""+VerifyFormatAddress.NAME+"\">");
        strmResponse.println("<input type=hidden name=\"iKey\" value=\"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.SACTION+"\" value=\"Booking\">");
        strmResponse.println("<input type=hidden name=\"sType\" value="+sType+">");
        strmResponse.println("</form>");
        
        //Confirm address form
        
        strmResponse.println("<form name=\"ConfirmForm\" method=\"POST\" action=\""+Constants.srvPostcodeLookup+"\">");
        strmResponse.println("<input type=hidden name=\"sCompanyName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sAddress1\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sAddress2\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sTown\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sCounty\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sPostcode\" value=\"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.SACTION+"\" value=\"Confirm\">");
        strmResponse.println("<input type=hidden name=\"sType\" value="+sType+">");
        strmResponse.println("</form>");
        
        strmResponse.println("</html>");
        
    }
    
    
    /**
     *
     * @param arrInput
     * @param arrLabels
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void readArray(String[] arrInput,String[] arrLabels, HttpServletResponse res) throws ServletException, IOException
    {
        try{
         sCompany = ""; sAddress = ""; sTown = ""; sPostcode = ""; sCountry = ""; sAddress2 = ""; sCounty = "";
         for(int i=0;i<arrInput.length;i++)
         {
                switch(i)
                {
                    case 0:
                        if(arrLabels[i].equals("Organisation"))
                        sCompany = arrInput[i];
                        break;
                    case 1:
                        if(arrLabels[i].equals(""))
                        sAddress = arrInput[i];
                        break;
                    case 2:
                        if(arrLabels[i].equals(""))
                            sAddress2=arrInput[i];
                        else
                        {
                            if(arrLabels[i].equals("Town"))
                            {
                                sTown=arrInput[i];
                                sAddress2="";
                            }
                        }
                        break;
                    case 3:
                        if(arrLabels[i].equals("Town"))
                        sTown = arrInput[i];
                        else
                        {
                            if(arrLabels[i].equals("County"))
                                sCounty=arrInput[i];
                        }
                        break;
                    case 4:
                        if(arrLabels[i].equals("County"))
                            sCounty=arrInput[i];
                        else
                        {
                             if(arrLabels[i].equals("Postcode"))
                                 sPostcode = arrInput[i];
                        }
                        break;
                    case 5:
                        if(arrLabels[i].equals("Postcode"))
                        sPostcode = arrInput[i];
                        break;
                    case 6:
                        if(arrLabels[i].equals("Country"))
                        {
                            sCountry="GBR";
                        }
                        break;
                    case 7:
                        if(arrLabels[i].equals("100m Latitude"))
                        {
                            sLatitude=arrInput[i];
                        }
                       break;
                    case 8:
                        if(arrLabels[i].equals("100m Longitude"))
                        {
                            sLongitude=arrInput[i];
                        }
                        break;
                }
            }
        }
        catch(Exception ex)
        {
            vShowErrorPage(res,ex);
        }
          
   }
    
    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void Confirm(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
    {
        ServletOutputStream strmResponse=response.getOutputStream();
        response.setContentType("text/html");

        String sCompany=request.getParameter("sCompanyName")==null?"":request.getParameter("sCompanyName").toString();
        String sAddress=request.getParameter("sAddress1")==null?"":request.getParameter("sAddress1").toString();
        String sAddress2=request.getParameter("sAddress2")==null?"":request.getParameter("sAddress2").toString();
        String sTown=request.getParameter("sTown")==null?"":request.getParameter("sTown").toString();
        String sPostcode=request.getParameter("sPostcode")==null?"":request.getParameter("sPostcode").toString();
        String sType=request.getParameter(Constants.STYPE)==null?"":request.getParameter(Constants.STYPE).toString();

        strmResponse.println("<html>");
        strmResponse.println("<head></head>");
        strmResponse.println("<body>");

            if(sType.equals("C"))
            {
                strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");

                strmResponse.println("var txtCCompany = window.opener.document.getElementById(\"txtCCompany\");");
                strmResponse.println("var txtCAddress1 = window.opener.document.getElementById(\"txtCAddress1\");");
                strmResponse.println("var txtCAddress2 = window.opener.document.getElementById(\"txtCAddress2\");");
                strmResponse.println("var txtCTown = window.opener.document.getElementById(\"txtCTown\");");
                strmResponse.println("var txtCCounty = window.opener.document.getElementById(\"txtCCounty\");");
                strmResponse.println("var txtCPostcode = window.opener.document.getElementById(\"txtCPostcode\");");
                strmResponse.println("var txtCLatitude = window.opener.document.getElementById(\"txtCLatitude\");");
                //strmResponse.println("alert(txtCLatitude);");
                strmResponse.println("var txtCLongitude = window.opener.document.getElementById(\"txtCLongitude\");");

                if(sCompany.length()!=0)
                    strmResponse.println("txtCCompany.value = \"" + sCompany + "\";");
                strmResponse.println("txtCAddress1.value = \"" + sAddress + "\";");
                strmResponse.println("txtCAddress2.value = \"" + sAddress2 + "\";");
                strmResponse.println("txtCTown.value = \"" + sTown + "\";");
                strmResponse.println("txtCCounty.value = \"" + sCounty + "\";");
                strmResponse.println("txtCPostcode.value = \"" + sPostcode + "\";");
                strmResponse.println("txtCLatitude.value = \"" + sLatitude + "\";");
                strmResponse.println("txtCLongitude.value = \"" + sLongitude + "\";");

                if(sCountry.equals(Constants.COUNTRY_CODE))
                {
                    strmResponse.println("txtCTown.disabled=true;");
                    strmResponse.println("txtCPostcode.disabled=true;");
                }
                else
                {
                    strmResponse.println("txtCTown.disabled=false;");
                    strmResponse.println("txtCPostcode.disabled=false;");
                }

                strmResponse.println("window.close();");
                strmResponse.println("</script>");
            }
            else
            {
                strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");

                strmResponse.println("var txtDCompany = window.opener.document.getElementById(\"txtDCompany\");");
                strmResponse.println("var txtDAddress1 = window.opener.document.getElementById(\"txtDAddress1\");");
                strmResponse.println("var txtDAddress2 = window.opener.document.getElementById(\"txtDAddress2\");");
                strmResponse.println("var txtDTown = window.opener.document.getElementById(\"txtDTown\");");
                strmResponse.println("var txtDCounty = window.opener.document.getElementById(\"txtDCounty\");");
                strmResponse.println("var txtDPostcode = window.opener.document.getElementById(\"txtDPostcode\");");
                strmResponse.println("var txtDLatitude = window.opener.document.getElementById(\"txtDLatitude\");");
                strmResponse.println("var txtDLongitude = window.opener.document.getElementById(\"txtDLongitude\");");
                strmResponse.println("var selDCountry = window.opener.document.getElementById(\"selDCountry\");");
                strmResponse.println("var lblDPostcode = window.opener.document.getElementById(\"lblDPostcode\");");

                if(sCompany.length()!=0)
                    strmResponse.println("txtDCompany.value = \"" + sCompany + "\";");
                strmResponse.println("txtDAddress1.value = \"" + sAddress + "\";");
                strmResponse.println("txtDAddress2.value = \"" + sAddress2 + "\";");
                strmResponse.println("txtDTown.value = \"" + sTown + "\";");
                strmResponse.println("txtDCounty.value = \"" + sCounty + "\";");
                strmResponse.println("txtDPostcode.value = \"" + sPostcode + "\";");
                strmResponse.println("txtDLatitude.value = \"" + sLatitude + "\";");
                strmResponse.println("txtDLongitude.value = \"" + sLongitude + "\";");

                if(sCountry.equals(Constants.COUNTRY_CODE))
                {
                    strmResponse.println("txtDTown.disabled=true;");
                    strmResponse.println("txtDPostcode.disabled=true;");
                    strmResponse.println("selDCountry.disabled=true;");
                    strmResponse.println("lblDPostcode.innerHTML=\"Postcode *\";");
                }
                else
                {
                    strmResponse.println("txtDTown.disabled=false;");
                    strmResponse.println("txtDPostcode.disabled=false;");
                    strmResponse.println("selDCountry.disabled=false;");
                    strmResponse.println("lblDPostcode.innerHTML=\"Postcode\";");
                }

                strmResponse.println("window.close();");
                
                strmResponse.println("</script>");
            }

        strmResponse.println("</body>");
        strmResponse.println("</html>");
    }

    /**
     *
     * @param asAddress
     * @return
     */
    public String getDisplayAddress(String[] asAddress)
     {
         String sAddress="";

         for(int i=0;i<asAddress.length-4;i++)
         {
             if(asAddress[i].length()>0)
                sAddress=sAddress + asAddress[i]+ ",";
         }

         if(sAddress.length()>0)
             sAddress=sAddress.substring(0,sAddress.length()-1);

         return sAddress;
     }

     /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void validateAddress(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
    {
        ServletOutputStream strmResponse=response.getOutputStream();//response.getOutputStream();
        response.setContentType("text/html");
        
        sAction_sub=request.getParameter(Constants.SACTION_SUB)==null?"":request.getParameter(Constants.SACTION_SUB).toString();
        sCompany=request.getParameter("sCompany")==null?"":request.getParameter("sCompany").toString();
        sAddress=request.getParameter("sAddress1")==null?"":request.getParameter("sAddress1").toString();
        sAddress2=request.getParameter("sAddress2")==null?"":request.getParameter("sAddress2").toString();
        sTown=request.getParameter("sTown")==null?"":request.getParameter("sTown").toString();
        sCounty=request.getParameter("sCounty")==null?"":request.getParameter("sCounty").toString();
        sPostcode=request.getParameter("sPostcode")==null?"":request.getParameter("sPostcode").toString();
        sCountry=request.getParameter("sCountry")==null?"":request.getParameter("sCountry").toString();
        sUCompany=request.getParameter("sUCompany")==null?"":request.getParameter("sUCompany").toString();

        if(sAction_sub.equals("ShowAddress"))
        {
                qasOutput = request.getSession(false).getAttribute("qasOutput")==null?qasOutput:(List<QASAddress>)request.getSession(false).getAttribute("qasOutput");
                sKey = request.getParameter("iKey")==null?"":request.getParameter("iKey");

                if(sKey.length()>0)
                    iKey = Integer.parseInt(sKey);

                sCompany = qasOutput.get(iKey).getsCompany().equals("")?sUCompany:qasOutput.get(iKey).getsCompany();
                sAddress = qasOutput.get(iKey).getsAddress1();
                sAddress2= qasOutput.get(iKey).getsAddress2();
                sTown = qasOutput.get(iKey).getsTown();
                sCounty= qasOutput.get(iKey).getsCounty();
                sPostcode = qasOutput.get(iKey).getsPostcode();
                sLongitude = qasOutput.get(iKey).getsLongitude();
                sLatitude = qasOutput.get(iKey).getsLatitude();
                sCountry = "GBR";
         }
        else
        {
             QASAddress qasInput = new QASAddress();
            qasInput.setsCompany(sCompany);
            qasInput.setsAddress1(sAddress);
            qasInput.setsAddress2(sAddress2);
            qasInput.setsTown(sTown);
            qasInput.setsCounty(sCounty);
            qasInput.setsPostcode(sPostcode);
            qasInput.setsCountry(sCountry);

            request.setAttribute("qasInput", qasInput);
            request.setAttribute("QASOnDemandURL", bdl.getString("QASOnDemandURL"));
            QASRequest qasRequest = new QASRequest();
            QASConcrete qasConcrete = new QASConcrete(qasRequest);
            qasConcrete.execute(request, response);

            iStatusCode = Integer.parseInt(request.getAttribute("statuscode")==null?"0":request.getAttribute("statuscode").toString());
            sQASMessage = request.getAttribute("message")==null?"":request.getAttribute("message").toString();
            qasOutput = request.getAttribute("qasOutput")==null?qasOutput:(List<QASAddress>)request.getAttribute("qasOutput");
            if(request.getSession(false).getAttribute("qasOutput")==null)
                    request.getSession(false).setAttribute("qasOutput", qasOutput);
            else
            {
                request.getSession(false).removeAttribute("qasOutput");
                request.getSession(false).setAttribute("qasOutput", qasOutput);
            }
        }
        

          if(qasOutput.size()==1)
          {
                sCompany = qasOutput.get(0).getsCompany().equals("")?sUCompany:qasOutput.get(0).getsCompany();
                sAddress = qasOutput.get(0).getsAddress1();
                sAddress2= qasOutput.get(0).getsAddress2();
                sTown = qasOutput.get(0).getsTown();
                sCounty= qasOutput.get(0).getsCounty();
                sPostcode = qasOutput.get(0).getsPostcode();
                sLongitude = qasOutput.get(0).getsLongitude();
                sLatitude = qasOutput.get(0).getsLatitude();
                sCountry = "GBR";
         }
//       else
//       {
//            sCompany = "";
//            sAddress = "";
//            sAddress2="";
//            sTown = "";
//            sCounty="";
//            sPostcode = "";
//            sCountry = "";
//            sLongitude="";
//            sLatitude="";
//
//       }
       LoadPage(request,response);
    }

    public String getQASDisplayAddress(QASAddress qasOutput)
     {
         String sAddress="";

         if(qasOutput.getsCompany().length()>0)
                sAddress=sAddress + qasOutput.getsCompany() + ",";
         if(qasOutput.getsAddress1().length()>0)
                sAddress=sAddress + qasOutput.getsAddress1() + ",";
         if(qasOutput.getsTown().length()>0)
                sAddress=sAddress + qasOutput.getsTown() + ",";
         if(qasOutput.getsCounty().length()>0)
                sAddress=sAddress + qasOutput.getsCounty();

         return sAddress;
     }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     * @return
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
