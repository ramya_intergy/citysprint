/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citysprint.co.uk.NDI.QAS;

/**
 *
 * @author ramyas
 */
public class QASAddress {
    String sCompany = "", sAddress1 = "", sAddress2 = "", sAddress3 = "", sTown = "", sPostcode = "",
            sCounty = "", sLongitude = "", sLatitude = "", sCountry = "", sKey = "";

    public String getsKey() {
        return sKey;
    }

    public void setsKey(String sKey) {
        this.sKey = sKey;
    }

    public String getsAddress1() {
        return sAddress1;
    }

    public String getsAddress3() {
        return sAddress3;
    }

    public void setsAddress3(String sAddress3) {
        this.sAddress3 = sAddress3;
    }

    public String getsCountry() {
        return sCountry;
    }

    public void setsCountry(String sCountry) {
        this.sCountry = sCountry;
    }

    public void setsAddress1(String sAddress1) {
        this.sAddress1 = sAddress1;
    }

    public String getsAddress2() {
        return sAddress2;
    }

    public void setsAddress2(String sAddress2) {
        this.sAddress2 = sAddress2;
    }

    public String getsCompany() {
        return sCompany;
    }

    public void setsCompany(String sCompany) {
        this.sCompany = sCompany;
    }

    public String getsCounty() {
        return sCounty;
    }

    public void setsCounty(String sCounty) {
        this.sCounty = sCounty;
    }

    public String getsLatitude() {
        return sLatitude;
    }

    public void setsLatitude(String sLatitude) {
        this.sLatitude = sLatitude;
    }

    public String getsLongitude() {
        return sLongitude;
    }

    public void setsLongitude(String sLongitude) {
        this.sLongitude = sLongitude;
    }

    public String getsPostcode() {
        return sPostcode;
    }

    public void setsPostcode(String sPostcode) {
        this.sPostcode = sPostcode;
    }

    public String getsTown() {
        return sTown;
    }

    public void setsTown(String sTown) {
        this.sTown = sTown;
    }    

}
