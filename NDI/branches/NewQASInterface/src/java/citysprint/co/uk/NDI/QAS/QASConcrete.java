/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citysprint.co.uk.NDI.QAS;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ramyas
 */
public class QASConcrete implements QASInterface{
    private QASRequest qasRequest;

    public QASConcrete(QASRequest qasRequest)
    {
        this.qasRequest = qasRequest;
    }

    public void execute(HttpServletRequest request, HttpServletResponse response)
    {
        qasRequest.validateAddress(request, response);
    }
     
}
