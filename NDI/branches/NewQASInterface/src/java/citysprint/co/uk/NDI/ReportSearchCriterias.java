/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citysprint.co.uk.NDI;

/**
 *
 * @author ramyas
 */
public class ReportSearchCriterias {

    private String jno ="",date = "",dcountry="",dcomp = "", cont="",servlevel = "",items="",weight="",pod ="",totcharge="";
    private String fmdate="",todate="",urref="",status="",status1="",status2="",incl="",sortby="", urdept="", rdStatus="";
    private String uref2="",ucaller="";

    private String joberror = "";

    public String getJoberror() {
        return joberror;
    }

    public void setJoberror(String joberror) {
        this.joberror = joberror;
    }

    public String getRdStatus() {
        return rdStatus;
    }

    public void setRdStatus(String rdStatus) {
        this.rdStatus = rdStatus;
    }

    public String getCont() {
        return cont;
    }

    public void setCont(String cont) {
        this.cont = cont;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDcomp() {
        return dcomp;
    }

    public void setDcomp(String dcomp) {
        this.dcomp = dcomp;
    }

    public String getDcountry() {
        return dcountry;
    }

    public void setDcountry(String dcountry) {
        this.dcountry = dcountry;
    }

    public String getFmdate() {
        return fmdate;
    }

    public void setFmdate(String fmdate) {
        this.fmdate = fmdate;
    }

    public String getIncl() {
        return incl;
    }

    public void setIncl(String incl) {
        this.incl = incl;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getJno() {
        return jno;
    }

    public void setJno(String jno) {
        this.jno = jno;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getServlevel() {
        return servlevel;
    }

    public void setServlevel(String servlevel) {
        this.servlevel = servlevel;
    }

    public String getSortby() {
        return sortby;
    }

    public void setSortby(String sortby) {
        this.sortby = sortby;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus1() {
        return status1;
    }

    public void setStatus1(String status1) {
        this.status1 = status1;
    }

    public String getStatus2() {
        return status2;
    }

    public void setStatus2(String status2) {
        this.status2 = status2;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getTotcharge() {
        return totcharge;
    }

    public void setTotcharge(String totcharge) {
        this.totcharge = totcharge;
    }

    public String getUrdept() {
        return urdept;
    }

    public void setUrdept(String urdept) {
        this.urdept = urdept;
    }

    public String getUrref() {
        return urref;
    }

    public void setUrref(String urref) {
        this.urref = urref;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getUcaller() {
        return ucaller;
    }

    public void setUcaller(String ucaller) {
        this.ucaller = ucaller;
    }

    public String getUref2() {
        return uref2;
    }

    public void setUref2(String uref2) {
        this.uref2 = uref2;
    }
}
