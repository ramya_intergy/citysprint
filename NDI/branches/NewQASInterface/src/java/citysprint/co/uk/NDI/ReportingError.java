/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citysprint.co.uk.NDI;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import citysprint.co.uk.NDI.CommonClass;

/**
 *
 * @author ramyas
 */
public class ReportingError extends CommonClass {

    /**
     *
     * @param rspOutput
     * @param Error
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void displayError(HttpServletResponse rspOutput, Exception Error)throws ServletException, IOException{

        String sErrorMessage="";

        String NEW_LINE = System.getProperty("line.separator");
        sErrorMessage=Error.getMessage()+NEW_LINE+getCustomStackTrace(Error)+NEW_LINE+Error.getClass().toString();
        writeOutputLog(sErrorMessage);

        rspOutput.setContentType("text/html");
        PrintWriter strmResponse =  rspOutput.getWriter();

        strmResponse.println("<HTML>");
        strmResponse.println("<HEAD>");
        strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
        strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");
        strmResponse.println("function enableErrorDiv()");
        strmResponse.println("{document.getElementById(\"divError\").style.display='';document.getElementById(\"aTechical\").style.display='none';}");
        strmResponse.println("</script>");
        strmResponse.println("</HEAD>");
        strmResponse.println("<BODY>");
        strmResponse.println("<table cellpadding=\"4\" height=\"100%\"><tr><td height=\"30%\">");
        strmResponse.println("<H5>CitySprint's NextDay & International Reporting/Tracking system is currently unavailable, please contact your local CitySprint ServiceCentre to check about your booking.</H5><BR>");
        strmResponse.println("</td></tr><tr><td height=\"70%\">");
        strmResponse.println("<div style=\"display:none\" id=\"divError\">");
        strmResponse.println(Error.getMessage()+"<BR>");
        strmResponse.println(getCustomStackTrace(Error)+"<BR>");
        strmResponse.println(Error.getClass().toString()+"<BR>");
        strmResponse.println("</div>");
        strmResponse.println("<div>");
        strmResponse.println("<a href=\"javascript:enableErrorDiv()\" id=\"aTechical\">Technical Information</a>");
        strmResponse.println("</div>");
        strmResponse.println("</td></tr>");
        strmResponse.println("</BODY></HTML>");
        strmResponse.close();
    }


}
