/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package citysprint.co.uk.NDI;

import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import javax.sql.DataSource;
import com.qas.proweb.servlet.*;
import com.qas.proweb.*;
import java.util.Vector;
/**
 *
 * @author ramyas
 */
public class Tracking extends CommonClass {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    int ndi_totpieces = 0;
    String ndi_consignno = "";
    String ndi_jobno = "";
    String sErrorMessage = "";
    HttpServletResponse res=null;

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        res=response;
        //String sAction = request.getParameter("sAction") == null ? "" : request.getParameter("sAction");
        try {
            if (isLogin(request)) {

                writeOutputLog("Tracking for Account No: "+request.getSession(false).getAttribute("acc_id").toString()+
                    " SessionId: "+request.getSession(false).getAttribute("sessionid").toString());

                //initialize config values
                initializeConfigValues(request, response);

                clearSessionAddress(request, response);
                LoadPage(request, response);


            } else {
                writeOutputLog("Session expired redirecting to index page");
                response.sendRedirect("index.jsp");
            }
        } catch (Exception ex) {
            vShowErrorPageReporting(response, ex);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void LoadPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletOutputStream strmResponse = response.getOutputStream();

        String sAction = request.getParameter("sAction") == null ? "" : request.getParameter("sAction");

        response.setContentType("text/html");


        strmResponse.println("<html>");
        strmResponse.println("<head>");
        strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
        strmResponse.println("<script type=\"text/javascript\">");
        strmResponse.println("preload1 = new Image();");
        strmResponse.println("preload2 = new Image();");
        strmResponse.println("preload3 = new Image();");
        strmResponse.println("preload4 = new Image();");
        strmResponse.println("preload5 = new Image();");
        strmResponse.println("preload6 = new Image();");

        strmResponse.println("preload1.src = \"images/booking_active.gif\";");
        strmResponse.println("preload2.src = \"images/booking_default.gif\";");
        strmResponse.println("preload3.src = \"images/reporting_active.gif\";");
        strmResponse.println("preload4.src = \"images/reporting_default.gif\";");
        strmResponse.println("preload5.src = \"images/tracking_active_410.gif\";");
        strmResponse.println("preload6.src = \"images/tracking_default_410.gif\";");
        strmResponse.println("</script>");
        //strmResponse.println("<script language=\"javascript\" src=\"JS/xp_progress.js\">");
        strmResponse.println("<script type=\"text/javascript\">");
        strmResponse.println("function showDetails(jobNo){");
        //strmResponse.println("alert('inside fn');");
        strmResponse.println("document.formShowDetails.chk_HawbNo.value=jobNo;");
        strmResponse.println("document.formShowDetails.submit();");
        strmResponse.println("}");
        strmResponse.println("</script>");
        strmResponse.println("</head>");
        strmResponse.println("<body>");
        strmResponse.println("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"padding-left:4px;padding-right:4px;\" >");
        strmResponse.println("<tr><td colspan=\"2\">");
        strmResponse.println("<img src=\"images/booking_default.gif\" id=\"imgTab1\" onclick=\"javascript:window.location='EnterConsignment?t="+new java.util.Date()+"';\" style=\"padding-bottom:0px;padding-top:1pxpadding-left:0px;cursor:pointer\"><img src=\"images/reporting_default.gif\" id=\"imgTab2\" onclick=\"javascript:window.location='Reporting.jsp?t="+new java.util.Date()+"';\" style=\"padding-bottom:0px;padding-top:1pxpadding-left:0px;cursor:pointer\"><img src=\"images/tracking_active_410.gif\" id=\"imgTab3\" style=\"padding-bottom:0px;padding-top:1pxpadding-left:0px;\">");
        strmResponse.println("</td></tr><tr><td>");
        strmResponse.println("<form name=\"formTracking\" method=\"post\" action=\"Tracking\">");
        strmResponse.println("<input type=\"hidden\" name=\"sAction\" value=\"getJobDetails\"><br>");
        strmResponse.println("<table border=\"0\" id=\"box\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding-left:4px;padding-right:4px\">");
        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"sectionheader\" style=\"padding-left:4px\">");
        strmResponse.println("Track your consignment<br>");
        strmResponse.println("</td>");
        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Consignment No"); 
        strmResponse.println("</td>");
        strmResponse.println("<td><input type=\"text\" name=\"cgno\" id=\"cgno\" size=\"20\" tabindex=\"1\" value=\""+(request.getParameter("cgno")==null?"":request.getParameter("cgno").toString())+"\" >");
        strmResponse.println("</td>");
        strmResponse.println("<td><input type=\"submit\" value=\"Get Job Details\">");
        strmResponse.println("</tr>");
        strmResponse.println("</table>");
        strmResponse.println("</form>");
        strmResponse.println("</td></tr></table>");
        //strmResponse.println("<table width=\"100%\"><tr><td>");
//        strmResponse.println("<table id=\"tblProgress\"><tr><td style=\"font-size:14px;font-weight:bold;padding-left:80px;\" align=\"center\"><br><br><br><br><br><br><br><br><br><br>Please wait while your request is being processed</td></tr>" +
//                    "<tr><td style=\"font-size:14px;font-weight:bold;padding-left:80px;\"><br><br><script type=\"text/javascript\">" +
//                    "var bar1= createBar(300,15,'white',1,'black','#0059AD',85,7,3,\"\");</script></td></tr></table>");

        if (sAction.equals("getJobDetails")) {


            Vector sJobNo = getJobdetails(request, response);

            if (sJobNo.size() > 0) {

                for (int i = 0; i < sJobNo.size(); i++) {

                    if (i == 0) {
                        strmResponse.println("<iframe id=\"ifJobDetails\" src=\"JobDetails.jsp?sAction=Tracking&chk_HawbNo=" + sJobNo.elementAt(i) + ",\" width=\"100%\" height=\"500px\" frameborder=\"0\" scrolling=\"auto\"></iframe>");
                    } else {
                        if (i == 1) {

                            strmResponse.println("<form name=\"formShowDetails\" method=\"post\" action=\"Tracking\">");
                            strmResponse.println("<input type=hidden name=\"sAction\" value=\"showDetails\">");
                            strmResponse.println("<input type=hidden name=\"chk_HawbNo\" value=\"\">");
                            strmResponse.println("<input type=hidden name=\"cgno\" value=\""+(request.getParameter("cgno")==null?"":request.getParameter("cgno").toString())+"\">");
                            strmResponse.println("</form></td></tr><tr><td colspan=\"2\">");
                            strmResponse.println("<table><tr><td><br>");
                            strmResponse.println("The Consignment Number you searched has duplicates. Please click on the Job Number link below to view another Consignment Note");
                            strmResponse.println("</td></tr>");

                        }

                        strmResponse.println("<tr><td><a href=\"javascript:showDetails('"+sJobNo.elementAt(i)+"')\">" + sJobNo.elementAt(i) + "</a></td></tr>");

                    }
                }

                request.getSession(false).setAttribute("jobnoVector", sJobNo);
                if(sJobNo.size()>1)
                strmResponse.println("</table>");
            } else {
                strmResponse.println("<br><br><font color=\"Red\">"+sErrorMessage+"</font>");
            }

        }
        else if(sAction.equals("showDetails"))
        {
            strmResponse.println("<iframe id=\"ifJobDetails\" src=\"JobDetails.jsp?sAction=Tracking&chk_HawbNo=" +(request.getParameter("chk_HawbNo")==null?"":request.getParameter("chk_HawbNo").toString())+ ",\" width=\"100%\" height=\"500px\" frameborder=\"0\" scrolling=\"auto\"></iframe>");
            Vector jobNo=request.getSession(false).getAttribute("jobnoVector")==null?new Vector():(Vector)request.getSession(false).getAttribute("jobnoVector");

            for (int j = 0; j < jobNo.size(); j++) {
                if (j == 0) {

                    strmResponse.println("<form name=\"formShowDetails\" method=\"post\" action=\"Tracking\">");
                    strmResponse.println("<input type=hidden name=\"sAction\" value=\"showDetails\">");
                    strmResponse.println("<input type=hidden name=\"chk_HawbNo\" value=\"\">");
                    strmResponse.println("<input type=hidden name=\"cgno\" value=\"" + (request.getParameter("cgno") == null ? "" : request.getParameter("cgno").toString()) + "\">");
                    strmResponse.println("</form></td></tr><tr><td colspan=\"2\">");
                    strmResponse.println("<table><tr><td><br>");
                    strmResponse.println("The Consignment Number you searched has duplicates. Please click on the Job Number link below to view another Consignment Note");
                    strmResponse.println("</td></tr>");

                }

                if(!jobNo.elementAt(j).equals((String)(request.getParameter("chk_HawbNo"))))
                    strmResponse.println("<tr><td><a href=\"javascript:showDetails('" + jobNo.elementAt(j) + "')\">" + jobNo.elementAt(j) + "</a></td></tr>");
            }

            strmResponse.println("</tr></table>");
        }

        //strmResponse.println("</td></tr></table>");
        strmResponse.println("</body>");
        strmResponse.println("</html>");

    }

    /**
     *
     * @param req
     * @param res
     * @return
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public Vector getJobdetails(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        String getJobdetailsArray[][];
        System.out.println("Inside getJobdetails=" + req + "  " + res);
        String sJobDetailError = "";
        java.util.Date d = new java.util.Date();
        String timeStamp = d.toString();
        String sHAWBNo = req.getParameter("cgno") == null ? "" : req.getParameter("cgno").toString();
        Vector jobNo=new Vector();
        String session_ID="";
        String acc_id="";

        URL urlApi = null;

        if (sHAWBNo.length() > 0) {

            try {
                StringBuffer jobdetails = new StringBuffer();

                //API Input

                jobdetails.append("CK=" + encStr(req.getSession(false).getAttribute("acc_id").toString()));                                    //Customer Key or Account No
                jobdetails.append("&PW=" + encStr(req.getSession(false).getAttribute("pwd").toString()));                                      //Password
                jobdetails.append("&TS=" + encStr(timeStamp));
                jobdetails.append("&HN=" + encStr(sHAWBNo));                                       //JobNumber
                jobdetails.append("&SD=");                                    //FromDate
                jobdetails.append("&ED=");                                    //ToDate

                if (environ.equals("PROD")) {
                    urlApi = new URL(scriptsurl + bdl.getString("jobDetailsScript") + "?" + jobdetails.toString());
                } else {
                    urlApi = new URL(bdl.getString("devhost") + "NDIJobDetails.txt");
                }


                writeOutputLog("Input to ndi_details:" + urlApi.toString());

                URLConnection con = urlApi.openConnection();
                System.out.println(con);
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setUseCaches(false);

                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String s;
                String outputLines = "";
                while ((s = br.readLine()) != null) {
                    outputLines = outputLines + s;
                }

                writeOutputLog("Output from ndi_details:" + outputLines);

                br.close();
                outputLines = outputLines + "^ZZ";

                getJobdetailsArray = parseOutput(outputLines);
                if (getJobdetailsArray == null) {
                    vShowErrorPage(res, "Error in NDI_Details no output returned from API");
                } else {

                    deleteJobdetails(req.getSession(false).getAttribute("acc_id").toString(), req.getSession(false).getAttribute("sessionid").toString());

                    parseJobdetails(getJobdetailsArray, req.getSession(false).getAttribute("acc_id").toString(), req.getSession(false).getAttribute("sessionid").toString());

                    session_ID=req.getSession(false).getAttribute("sessionid").toString();
                    acc_id=req.getSession(false).getAttribute("acc_id").toString();

                    Connection conJobNo=cOpenConnection();
                    Statement stmtJobDetails=conJobNo.createStatement();;
                    ResultSet rstTracking =null;
                    String sqlaudit_Delete = "delete from ndi_audit where ndi_sessionid='" + session_ID + "' and ndi_accno='" + acc_id + "' ";
                    //Product
                    String sqlprod_Delete = "delete from audit_prod where prod_sessionid='" + session_ID + "' and prod_accno='" + acc_id + "' ";
                    //Commodity
                    String sqlcommodity_Delete = "delete from audit_commodity where com_sessionid='" + session_ID + "' and com_accno='" + acc_id + "' ";
                    //Trial
                    String sqltrial_Delete = "delete from audit_trial where audit_sessionid='" + session_ID + "' and audit_accno='" + acc_id + "' ";
                    //Sequence
                    String sqlseq_Delete = "delete from audit_seq where seq_sessionid='" + session_ID + "' and seq_accno='" + acc_id + "' ";
                    //SecondSequence
                    String sqlseqsecond_Delete = "delete from auditseq_second where sequence_sessionid='" + session_ID + "' and sequence_accno='" + acc_id + "' ";


                    deleteAuditdetails(sqlaudit_Delete);
                    deleteAuditdetails(sqlprod_Delete);
                    deleteAuditdetails(sqlcommodity_Delete);
                    deleteAuditdetails(sqltrial_Delete);
                    deleteAuditdetails(sqlseq_Delete);
                    deleteAuditdetails(sqlseqsecond_Delete);

                    String sql="Select det_jno from ndi_details where det_hawbno='"+sHAWBNo+"' and det_sessionid='"+req.getSession(false).getAttribute("sessionid").toString()+"'";

                    rstTracking  = stmtJobDetails.executeQuery(sql);

                    while(rstTracking.next()){

                        jobNo.add(rstTracking.getString("det_jno"));
                        getAuditDetails(req, res, rstTracking.getString("det_jno"));

                        seqInsert(session_ID, acc_id);

                    }

                    return jobNo;
                }
            } catch (Exception ex) {
                vShowErrorPageReporting(res, ex);
            }

        }

        return jobNo;
    }

    /**
     *
     * @param JobdetailsArray
     * @param acc_id
     * @param session_ID
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void parseJobdetails(String[][] JobdetailsArray,String acc_id,String session_ID)throws ServletException, IOException
   {
       String tempstr="";
       String tag ="";
       String sSQL="";
       String sSQLStamp="";
       String sComDescrip="";
       String empty_String="''";
       Connection condetails=null;
       String sqlHeader = "insert into ndi_details (det_sessionid,det_accid,det_server,det_environ,det_jno,det_hawbno,det_readyon,det_readyat,det_servcode,det_ref,det_price,det_surcharge,det_vat,det_liability,det_dcomp,det_dcountry,det_ptype,det_totpieces,det_totweight,det_pod,det_jstatus,det_descrip)" +
              "values('"+session_ID+"','"+acc_id+"','"+server+"','"+environment+"', ";

       int i,itemp=0;
       //int j=14;
       int k = JobdetailsArray.length;
       System.out.println(k);
       int sqllen;
       for( i=0;i<JobdetailsArray.length-1;i++){

           tag = JobdetailsArray[i][0];

           //Tag equals Transaction

           if (tag.equals("TS")) {

               tempstr = JobdetailsArray[i][1].trim();

           }

           //Tag equals JobNumber
           if (tag.equals("JN")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";

               } else {
                   //String sqlJobNo="''";
                   sSQL = sSQL + empty_String + ",";
               }

           }

           //Tag equals Product Type
           if (tag.equals("TP")) {


               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {
                   // MySql Date Format
                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

               } else {
                   //String sqlProd="''";
                   sSQL = sSQL + empty_String + ",";
               }

           }
           //Tag equals Ready on
           if (tag.equals("RO")) {


               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {
                   // MySql Date Format
                   sSQL = sSQL + "DATE_FORMAT('" + tempstr + "','%d/%m/%y'),";

               } else {
                   //String sqlReadyOn="''";
                   sSQL = sSQL + empty_String + ",";
               }

           }
           //Tag Equals ReadyAt
           if (tag.equals("RA")) {


               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {
                   // MySql Date Format
                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

               } else {
                   // String sqlReadyAt="''";
                   sSQL = sSQL + empty_String + ",";
               }

           }


           //Tag equals Reference
           if (tag.equals("RE")) {
               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {
                   //MySql Query

                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";


               } else {
                   //String Ref="''";
                   sSQL = sSQL + empty_String + ",";
               }

           }



           //Tag equals Delivery Country
           if (tag.equals("DC")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
               } else {
                   //String sqlCountry="''";
                   sSQL = sSQL + empty_String + ",";
               }
           }

           //Tag equals Delivery Company
           if (tag.equals("DY")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
               } else {
                   //String sqlCompany="''";
                   sSQL = sSQL + empty_String + ",";
               }
           }



           //Tag equals Service Level

           if (tag.equals("SV")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
               } else {
                   // String sqlService="''";
                   sSQL = sSQL + empty_String + ",";
               }

           }

           //Tag equals total no of Pieces
           if (tag.equals("PN")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + tempstr + ",";
               } else {
                   // String sqlItem="''";
                   sSQL = sSQL + itemp + ",";
               }

           }

           //Tag equals Weight

           if (tag.equals("TW")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + tempstr + ",";
               } else {
                   // String sqlWeight="''";
                   sSQL = sSQL + itemp + ",";
               }

           }

           //Tag equals POD details
           if (tag.equals("PO")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
               } else {
                   //String sqlPod="''";
                   sSQL = sSQL + empty_String + ",";
               }

           }



           //Tag equals Hawbno
           if (tag.equals("HN")) {

               tempstr = JobdetailsArray[i][1].trim();
               String tempHawbno = "";
               String tempHawbno1 = "";
               String tempHawbno2 = "";

               if (tempstr.length() > 0) {

                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

               } else {

                   sSQL = sSQL + empty_String + ",";
               }

           }

           //Tag equals Basic Price

           if (tag.equals("BP")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + tempstr + ",";
               } else {
                   // String sqlPrice ="''";
                   sSQL = sSQL + itemp + ",";
               }

           }

           //Tag equals SurCharges
           if (tag.equals("SC")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + tempstr + ",";
               } else {
                   //  String sqlSurcharge="''";
                   sSQL = sSQL + itemp + ",";
               }

           }

           //Tag equals VAT
           if (tag.equals("GS")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + tempstr + ",";
               } else {
                   //String sqlGst="''";
                   sSQL = sSQL + itemp + ",";
               }

           }

           //Tag equals Liability
           if (tag.equals("LP")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + tempstr + ",";
               } else {
                   // String sqlLiability="''";
                   sSQL = sSQL + itemp + ",";
               }

           }


           //Tag equals JobStatus
           if (tag.equals("JS")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
               } else {
                   //String sqlJobstatus="''";
                   sSQL = sSQL + empty_String + ",";
               }
           }

           int czvalue = 0;
           if (tag.equals("CZ")) {

               tempstr = JobdetailsArray[i][1].trim();
               czvalue = Integer.parseInt(tempstr);

               if (tempstr.equals("0")) {
                   String sqlTest = "''";
                   sSQL = sSQL + sqlTest;
               }
           } else {


               for (int c = 0; c <= czvalue; c++) {
                   //Tag Equals Commodity Code

                   if (tag.equals("CC")) {
                       tempstr = JobdetailsArray[i][1].trim();
                   }


                   //Tag Equals Commodity Description

                   if (tag.equals("CD")) {

                       tempstr = JobdetailsArray[i][1].trim();

                       if (tempstr.length() > 0) {

                           //sComDescrip= sComDescrip + tempstr;
                           sComDescrip = sComDescrip + sReplaceChars(tempstr,"'","''");
                       } else {
                           //String sqldescrip="''";
                           sComDescrip = sComDescrip + empty_String;
                       // sSQL = sSQL+"'" + sComDescrip +"'" ;
                       }


                   }

                   //Tag Equals Commodity Quantity

                   if (tag.equals("CQ")) {

                       tempstr = JobdetailsArray[i][1].trim();


                   }

                   //Tag Equals Commodity Value
                   if (tag.equals("CV")) {

                       tempstr = JobdetailsArray[i][1].trim();

                   }

                   // Tag Equals Commodity Indemnity
                   if (tag.equals("CI")) {

                       tempstr = JobdetailsArray[i][1].trim();

                       sSQL = sSQL + "'" + sComDescrip + "'";
                       sComDescrip = "";
                   }


               }



           }


           //End Tag for Each Job set

           if (tag.equals("XX")) {

               tempstr = JobdetailsArray[i][1].trim();

               // sSQL=sSQL+"'"+tempstr+"'";

               sSQL = sqlHeader + sSQL + ")";

               try {

                   condetails = cOpenConnection();
                   Statement stmtdetails = condetails.createStatement();
System.out.println("Tracking: Line# 730: sSQL ==" + sSQL);
                   stmtdetails.executeUpdate(sSQL);//,Statement.RETURN_GENERATED_KEYS);
                   condetails.close();
                }
                catch(Exception e){
                    vShowErrorPageReporting(res, e);
                }
                sSQL="";
            }

           if(tag.equals("ER")){
               sErrorMessage = JobdetailsArray[i][1].trim().substring(3,JobdetailsArray[i][1].length());
           }

       }
    }

   /**
    *
    * @param acc_id
    * @param session_ID
    * @throws javax.servlet.ServletException
    * @throws java.io.IOException
    */
   public void deleteJobdetails(String acc_id, String session_ID) throws ServletException, IOException{
        try {
            Connection conDelete = cOpenConnection();
            String sqlDelete = "delete from ndi_details where det_sessionid='" + session_ID + "' and det_accid='" + acc_id + "' ";
            Statement stmtDelete = conDelete.createStatement();
            stmtDelete.executeUpdate(sqlDelete);
            conDelete.close();
        } catch (Exception e) {
            vShowErrorPageReporting(res, e);
        }
    }

    /**
     *
     * @param req
     * @param res
     * @param NewHawbno
     * @return
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public boolean getAuditDetails(HttpServletRequest req, HttpServletResponse res, String NewHawbno) throws ServletException, IOException {

        System.out.println("Inside getAuditDetails=" + req + "  " + res + " " + NewHawbno);
        String sServiceAPIError = "";
        String[][] getAuditDetailsArray=null;

        URL urlApi = null;


        java.util.Date d = new java.util.Date();
        String timeStamp = d.toString();

        StringBuffer auditDetails = new StringBuffer();


        //API Input

        auditDetails.append("CK=" + encStr(req.getSession(false).getAttribute("acc_id").toString()));                                      //Customer Key or Account No
        auditDetails.append("&PW=" + encStr(req.getSession(false).getAttribute("pwd").toString()));
        // auditDetails.append("&PW="+encStr(pwd));                                        //Password
        auditDetails.append("&TS=" + encStr(timeStamp));                                  //timeStamp
        auditDetails.append("&JN=" + encStr(NewHawbno));                                  //HawbNo



        // URL urlApi = new URL("http://ijb.citysprint.co.uk/dmsndi/bin/ndi_Audit?"+auditDetails.toString());
        //URL urlApi = new URL("http://localhost:8080/ndi_Reports/AuditDetails.txt");
        if (environ.equals("PROD")) {
            urlApi = new URL(scriptsurl + bdl.getString("jobAuditScript") + "?" + auditDetails.toString());
        }/* else if (NewHawbno.equals("280545")) {
            urlApi = new URL(bdl.getString("devhost") + "AuditDetails.txt");
        } */else  {
            urlApi = new URL(bdl.getString("devhost") + "NDI_jobno_14644319_not_working.txt");
        }
//         else if(NewHawbno.equals("280536"))
//           urlApi = new URL(bdl.getString("devhost")+ "AuditDetails.txt");
//         else if(NewHawbno.equals("280533"))
//          urlApi = new URL(bdl.getString("devhost")+ "AuditDetails.txt");
//
//         else




        writeOutputLog("Input to NDI_Audit:" + urlApi.toString());

        URLConnection con = urlApi.openConnection();
        con.setDoInput(true);
        con.setDoOutput(true);
        con.setUseCaches(false);

        BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String s;
        String outputLines = "";
        while ((s = br.readLine()) != null) {
            outputLines = outputLines + s;
        }

        writeOutputLog("Output from NDI_Audit:" + outputLines);

        br.close();
        outputLines = outputLines + "^ZZ";
        getAuditDetailsArray = parseOutput(outputLines);
        if (getAuditDetailsArray == null) {
            return false;
        } else {
            parseAuditDetails(getAuditDetailsArray, req.getSession(false).getAttribute("sessionid").toString(), req.getSession(false).getAttribute("acc_id").toString());
        }
        System.out.println(sServiceAPIError);
        System.out.println("Output=" + outputLines);
        return true;

    //}
    }

    /**
     *
     * @param AuditDetailsArray
     * @param session_ID
     * @param acc_id
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void parseAuditDetails(String[][] AuditDetailsArray, String session_ID, String acc_id) throws ServletException, IOException{
        String tempstr = "";
        String tag = "";
        String sSQL = "";
        String sqlAuditHeader = "";
        String sqlCommodityHeader = "";
        String sqlProductHeader = "";
        String sqlAuditTrialHeader = "";
        String sSQLAuditTrial = "";
        String sSQLProduct = "";
        String sSQLCommodity = "";
        //String SQLProduct[]=new String[0];
        String sql_Product = "";
        String sql_product = "";
        String sql_commodity = "";
        String sql_Commodity = "";
        String sql_AuditTrial = "";
        String sql_audittrial = "";
        String emptyString = "''";
        String sProductType="";
        int sqllen = 0;
        int sqllen1 = 0;
        int sqllen2 = 0;
        String sJobNo = "";
        int nzvalue = 0;
        int czvalue = 0;

        Connection conn = null;
        Statement stmtLabel = null;
        ResultSet rstLabel = null;    //ShowAudit Connection
        Connection conShowAudit = null;
        Statement stmtShowAudit = null;
        ResultSet rstShowAudit = null;    //For Jasper PDF
        Connection conAudit = null;
        Statement stmtAudit = null;
        ResultSet rstAudit = null;
        ResultSet rstSubLabel = null;
        Vector v = new Vector();
        Vector sqlcom = new Vector();
        Vector sqlaudit = new Vector();

//            sqlAuditHeader = "insert into ndi_audit(ndi_sessionid,ndi_accno,ndi_jobno,ndi_consignno,ndi_readyon,ndi_readyat,ndi_depart,ndi_ref,ndi_caller,ndi_service,ndi_vechicle,ndi_phone,ndi_booking," +
//                            "ndi_price,ndi_gstvat,ndi_surcharge,ndi_liability,ndi_paddress3,ndi_ptownkey,ndi_pcomp,ndi_paddress,ndi_paddress2,ndi_ptown,ndi_pcounty,ndi_ppostcode,ndi_pcountrycode," +
//                            "ndi_plongitu,ndi_platitu,ndi_pinstruct,ndi_pcontact,ndi_pphone,ndi_pcountry,ndi_dcomp,ndi_daddress,ndi_daddress2,ndi_daddress3,ndi_dtownkey,ndi_dtown,ndi_dcounty,ndi_dpostcode,ndi_dcountrycode," +
//                            "ndi_dphone,ndi_dvatno,ndi_dcountry,ndi_dlongitu,ndi_dlatitu,ndi_dinstruct,ndi_dcontact,ndi_prodtype,ndi_totpieces,ndi_totweight) " +
//                            "values('"+session_ID+"','"+acc_id+"',";

        sqlAuditHeader = "insert into ndi_audit(ndi_sessionid,ndi_accno,ndi_jobno,ndi_consignno,ndi_readyon,ndi_readyat,ndi_depart,ndi_ref,ndi_caller,ndi_service,ndi_vechicle,ndi_phone,ndi_booking," +
                "ndi_price,ndi_surcharge,ndi_gstvat,ndi_liability,ndi_pcomp,ndi_paddress,ndi_paddress2,ndi_paddress3,ndi_ptownkey,ndi_ptown,ndi_pcounty,ndi_ppostcode,ndi_pcountrycode," +
                "ndi_presidentialflag, "+
                 "ndi_pcountry,ndi_plongitu,ndi_platitu,ndi_pinstruct,ndi_pcontact,ndi_pphone,ndi_dcomp,ndi_daddress,ndi_daddress2,ndi_daddress3,ndi_dtownkey,ndi_dtown,ndi_dcounty,ndi_dpostcode,ndi_dcountrycode," +
                "ndi_dresidentialflag, " +
                 "ndi_dcountry,ndi_dlongitu,ndi_dlatitu,ndi_dinstruct,ndi_dcontact,ndi_dphone,ndi_dvatno,ndi_ddate,ndi_dtime, ndi_scname, ndi_scaddress, ndi_scaddress2, ndi_sctown, ndi_sccounty, ndi_scpostcode,ndi_sccountrycode," +
                "ndi_sclongitude, ndi_sclatitude, ndi_carriername, ndi_routingcode, ndi_destination_label,  ndi_prodtype, ndi_totpieces,ndi_totweight) " +
                "values('" + session_ID + "','" + acc_id + "',";



        sqlCommodityHeader = "insert into audit_commodity(com_sessionid,com_accno,com_code,com_description,com_quantity,com_value,com_indemnity,com_jobno)values('" + session_ID + "','" + acc_id + "',";

        sqlProductHeader = "insert into audit_prod(prod_sessionid,prod_accno,prod_noitems, "//pro_iweight, pro_ptype, "
                + "prod_height,prod_length,prod_depth,prod_jobno)" +
                "values('" + session_ID + "','" + acc_id + "',";
//        sqlProductHeader = "insert into audit_prod(prod_sessionid,prod_accno,prod_noitems,prod_height,prod_length,prod_depth,prod_jobno)" +
//                "values('" + session_ID + "','" + acc_id + "',";

        sqlAuditTrialHeader = "insert into audit_trial(audit_sessionid,audit_accno,audit_date,audit_time,audit_descrip,audit_jobno)values('" + session_ID + "','" + acc_id + "',";

        int i;
        //Needs to assign values for j
        // int j1=49;
        int k = AuditDetailsArray.length;
        System.out.println(k);

        for (i = 0; i < AuditDetailsArray.length - 1; i++) {


            System.out.println(AuditDetailsArray.length);

            //tag declaration
            tag = AuditDetailsArray[i][0];

            //Tag equals Transaction Stamp 1
            if (tag.equals("TS")) {

                tempstr = AuditDetailsArray[i][1].trim();

            }


            //ndi_jobno
            if (tag.equals("JN")) {

                tempstr = AuditDetailsArray[i][1].trim();
                ndi_jobno = tempstr;
                if (tempstr.length() > 0) {
                    sJobNo = tempstr;
                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlJobNo="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }


            //ndi_depart
            if (tag.equals("DT")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlDepart="''";
                    sSQL = sSQL + emptyString + ",";
                }

            }

            //ndi_ref
            if (tag.equals("RE")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlRef="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //4  ndi_caller
            if (tag.equals("CA")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlCaller="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }


            if (tag.equals("SV")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlService="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //6
            if (tag.equals("VT")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlVechicle="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }


            if (tag.equals("PH")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlPhone="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }


            if (tag.equals("AB")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlBooking="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }



            if (tag.equals("RO")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "DATE_FORMAT('" + tempstr + "','%d/%m/%y'),";

                } else {
                    //String sqlReadyon="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            if (tag.equals("RA")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlReadyat="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            int itemp = 0;
            //11
            if (tag.equals("BP")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + tempstr + ",";

                } else {
                    // String sqlPrice="''";

                    sSQL = sSQL + itemp + ",";
                }
            }


            if (tag.equals("GS")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + tempstr + ",";

                } else {
                    //String sqlGS="''";
                    sSQL = sSQL + itemp + ",";
                }

            }
            //13
            if (tag.equals("SC")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + tempstr + ",";

                } else {
                    // String sqlSurcharge="''";
                    sSQL = sSQL + itemp + ",";
                }
            }

            //14
            if (tag.equals("LP")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + tempstr + ",";

                } else {
                    //String sqlLiability="''";
                    sSQL = sSQL + itemp + ",";
                }
            }



            if (tag.equals("PC")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlPComp="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }


            if (tag.equals("PA")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlPAddr="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //17
            if (tag.equals("P2")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlPAddr2="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }
            //18
            if (tag.equals("P3")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlPAddr3="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }
            //19
            if (tag.equals("PK")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + tempstr + ",";

                } else {

                    //sSQL = sSQL +  emptyString +",";
                    sSQL = sSQL + itemp + ",";
                }
            }

            //20
            if (tag.equals("PS")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlPTown="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //21
            if (tag.equals("PO")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlPCounty="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //22
            if (tag.equals("PP")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlPostCode="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //23
            if (tag.equals("PY")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlCCode="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }


            //23 + 1 onwards
            if (tag.equals("PF")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlCCode="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            
            //24
            if (tag.equals("PG")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlPLong="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //25
            if (tag.equals("PU")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlPLatitu="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //26
            if (tag.equals("PI")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlInstruct="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //27
            if (tag.equals("PX")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlInstruct="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }
            //28PH

            if (tag.equals("PE")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlPickPhone="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //29PR


            if (tag.equals("PR")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlPCountr="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }
            //30
            if (tag.equals("DC")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlDComp="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //31
            if (tag.equals("DA")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlDAddr="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //32
            if (tag.equals("D2")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlDAddr2="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }
            // 33
            if (tag.equals("D3")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlDAddr3="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //  34
            if (tag.equals("DK")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + sReplaceChars(tempstr,"'","''") + ",";

                } else {
                    // String sqlDTownKey="''";
                    // sSQL = sSQL +  emptyString +",";
                    sSQL = sSQL + itemp + ",";
                }
            }

            //35
            if (tag.equals("DS")) {
                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlDTown="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //36
            if (tag.equals("DO")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlDCounty="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //37
            if (tag.equals("DP")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlDPost="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }


            //38
            if (tag.equals("DY")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlDCode="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }
            
            
            //38 + 2 onwards
            if (tag.equals("DF")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlDCode="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }
            
            //39
            if (tag.equals("DH")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlDPhone="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }
            //40
            if (tag.equals("DV")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlDVat="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //Delivery Date ndi_ddate
            if (tag.equals("DD")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlDVat="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //Delivery Time ndi_dtime
            if (tag.equals("DM")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlDVat="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //41
            if (tag.equals("DL")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //  String sqlDLong="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //42
            if (tag.equals("DU")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlDLati="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //43
            if (tag.equals("DI")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlDInstruct="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //44
            if (tag.equals("DX")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlDContact="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //46DR
            if (tag.equals("DR")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlDCountry="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }


            //47
            if (tag.equals("TP")) {

                tempstr = AuditDetailsArray[i][1].trim();
                sProductType = tempstr;

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlProd="''";
                    sSQL = sSQL + emptyString + ",";
                }
            }

            //48
            if (tag.equals("PN")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    // sSQL=sSQL +"'"+ tempstr +"'" + "," ;
                    sSQL = sSQL + tempstr + ",";

                } else {
                    // String sqlItem="''";
                    sSQL = sSQL + emptyString + ",";
                }

                ndi_totpieces = Integer.parseInt(tempstr);

                //sJobNoPieces.addElement(ndi_jobno+","+ndi_totpieces);

            }


            if (tag.equals("AD")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQLAuditTrial = sSQLAuditTrial + "DATE_FORMAT('" + tempstr + "','%d/%m/%y'),";

                } else {
                    //String sqlAuditDate="''";
                    sSQLAuditTrial = sSQLAuditTrial + emptyString + ",";
                }
            }
            //51
            if (tag.equals("AT")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQLAuditTrial = sSQLAuditTrial + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlAuditTime="''";
                    sSQLAuditTrial = sSQLAuditTrial + emptyString + ",";
                }
            }
            //52
            if (tag.equals("AE")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQLAuditTrial = sSQLAuditTrial + "'" + sReplaceChars(tempstr,"'","''") + "'";

                } else {
                    //  String sqlAuditBooked="''";
                    sSQLAuditTrial = sSQLAuditTrial + emptyString;
                }

                sql_AuditTrial = sqlAuditTrialHeader + sSQLAuditTrial + ",'" + sJobNo + "')";
                try {
                    Connection conAuditTrial = cOpenConnection();
                    Statement stmtAuditTrial = conAuditTrial.createStatement();
                    stmtAuditTrial.executeUpdate(sql_AuditTrial);
                    vCloseConnection(conAuditTrial);
                } catch (Exception e) {
                    vShowErrorPageReporting(res, e);
                }
//               sqlaudit.addElement(sql_AuditTrial);
//               sqllen2=sqlaudit.size();
                sSQLAuditTrial = "";


            }


//40       insert into Audit_Product table

            if (tag.equals("NZ")) {

                tempstr = AuditDetailsArray[i][1].trim();
                //nzvalue=tempstr.length();
                nzvalue = Integer.parseInt(tempstr);
            }


            //53
            if (tag.equals("NI")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQLProduct = sSQLProduct + tempstr + ",";

                } else {
                    //String sqlNoItems="''";
                    sSQLProduct = sSQLProduct + itemp + ",";
                }
            }

            
//             // Added by Adnan against 
//                if (tag.equals("WI")) {
//
//                    tempstr = AuditDetailsArray[i][1].trim();
//
//                    if (tempstr.length() > 0) {
//
//                        sSQLProduct = sSQLProduct + tempstr + ",";
//
//                    } else {
//                        //String sqlNoItems="''";
//                        sSQLProduct = sSQLProduct + itemp + ",";
//                    }
//                }
//                
//               if (tag.equals("PT")) {
//
//                    tempstr = AuditDetailsArray[i][1].trim();
//
//                    if (tempstr.length() > 0) {
//
//                        sSQLProduct = sSQLProduct +"'" + tempstr + "',";
//
//                    } else {
//                        //String sqlNoItems="''";
//                        sSQLProduct = sSQLProduct +"'"+ itemp + "',";
//                    }
//                }

            //54
            if (tag.equals("HE")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQLProduct = sSQLProduct + tempstr + ",";

                } else {
                    // String sqlHeight="''";
                    sSQLProduct = sSQLProduct + itemp + ",";
                }
            }

            //55
            if (tag.equals("PL")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQLProduct = sSQLProduct + tempstr + ",";

                } else {
                    //String sqlLength="''";
                    sSQLProduct = sSQLProduct + itemp + ",";
                }
            }


            //56

            if (tag.equals("PD")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQLProduct = sSQLProduct + tempstr;
                } else {
                    // String sqlDepth="''";
                    sSQLProduct = sSQLProduct + itemp + ",";
                }

                //Assign values in to Product Array

                sql_product = sqlProductHeader + sSQLProduct + ",'" + sJobNo + "')";
                try {

                    Connection conProd = cOpenConnection();
                    Statement stmtProd = conProd.createStatement();
                    stmtProd.executeUpdate(sql_product);

                    //sql_Product="";

                    conProd.close();
                } catch (Exception e) {
                    vShowErrorPageReporting(res, e);
                }

//               v.addElement(sql_product);
//               sqllen = v.size();
                sSQLProduct = "";


            }
            //  }
            // int czvalue=0;
            if (tag.equals("CZ")) {

                tempstr = AuditDetailsArray[i][1].trim();
                czvalue = Integer.parseInt(tempstr);
            }
            // for(int c=0;c<=czvalue;c++){
            //57
            if (tag.equals("CC")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQLCommodity = sSQLCommodity + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    //String sqlComcode="''";
                    sSQLCommodity = sSQLCommodity + emptyString + ",";
                }
            }

            //58
            if (tag.equals("CD")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQLCommodity = sSQLCommodity + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {
                    // String sqlComdescrip="''";
                    sSQLCommodity = sSQLCommodity + emptyString + ",";
                }
            }


            //int itemp=0;

            //59
            if (tag.equals("CQ")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQLCommodity = sSQLCommodity + tempstr + ",";

                } else {
                    // String sqlQuant="''";

                    sSQLCommodity = sSQLCommodity + itemp + ",";
                }
            }

            //60
            if (tag.equals("CV")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQLCommodity = sSQLCommodity + tempstr + ",";

                } else {
                    // String sqlComvalue="''";
                    sSQLCommodity = sSQLCommodity + itemp + ",";
                }
            }

            //61
            if (tag.equals("CI")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQLCommodity = sSQLCommodity + "'" + sReplaceChars(tempstr,"'","''") + "'";

                } else {
                    //String sqlComIndem="''";
                    sSQLCommodity = sSQLCommodity + emptyString + ",";
                }
                //Assign values in to Commodity Array
                sql_commodity = sqlCommodityHeader + sSQLCommodity + ",'" + sJobNo + "')";

                try {
                    Connection conCom = cOpenConnection();
                    Statement stmtCom = conCom.createStatement();
                    stmtCom.executeUpdate(sql_commodity);
                    //sql_Commodity="";
                    conCom.close();
                } catch (Exception e) {
                    vShowErrorPageReporting(res, e);
                }
//               sqlcom.addElement(sql_commodity);
//               sqllen1 = sqlcom.size();
                sSQLCommodity = "";


            }

            //  62
            if (tag.equals("HN")) {

                tempstr = AuditDetailsArray[i][1].trim();
                ndi_consignno = tempstr;
                String tempHawbno = "";
                String tempHawbno1 = "";
                String tempHawbno2 = "";

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {

                    sSQL = sSQL + emptyString + ",";
                }
            }

             if (tag.equals("SN")) {

                tempstr = AuditDetailsArray[i][1].trim();
                ndi_consignno = tempstr;
                String tempHawbno = "";
                String tempHawbno1 = "";
                String tempHawbno2 = "";

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {

                    sSQL = sSQL + emptyString + ",";
                }
            }

            if (tag.equals("SA")) {

                tempstr = AuditDetailsArray[i][1].trim();
                ndi_consignno = tempstr;
                String tempHawbno = "";
                String tempHawbno1 = "";
                String tempHawbno2 = "";

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {

                    sSQL = sSQL + emptyString + ",";
                }
            }

            if (tag.equals("S2")) {

                tempstr = AuditDetailsArray[i][1].trim();
                ndi_consignno = tempstr;
                String tempHawbno = "";
                String tempHawbno1 = "";
                String tempHawbno2 = "";

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {

                    sSQL = sSQL + emptyString + ",";
                }
            }

            if (tag.equals("SS")) {

                tempstr = AuditDetailsArray[i][1].trim();
                ndi_consignno = tempstr;
                String tempHawbno = "";
                String tempHawbno1 = "";
                String tempHawbno2 = "";

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {

                    sSQL = sSQL + emptyString + ",";
                }
            }

            if (tag.equals("SO")) {

                tempstr = AuditDetailsArray[i][1].trim();
                ndi_consignno = tempstr;
                String tempHawbno = "";
                String tempHawbno1 = "";
                String tempHawbno2 = "";

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {

                    sSQL = sSQL + emptyString + ",";
                }
            }

            if (tag.equals("SP")) {

                tempstr = AuditDetailsArray[i][1].trim();
                ndi_consignno = tempstr;
                String tempHawbno = "";
                String tempHawbno1 = "";
                String tempHawbno2 = "";

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {

                    sSQL = sSQL + emptyString + ",";
                }
            }

            if (tag.equals("SY")) {

                tempstr = AuditDetailsArray[i][1].trim();
                ndi_consignno = tempstr;
                String tempHawbno = "";
                String tempHawbno1 = "";
                String tempHawbno2 = "";

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {

                    sSQL = sSQL + emptyString + ",";
                }
            }

            if (tag.equals("SG")) {

                tempstr = AuditDetailsArray[i][1].trim();
                ndi_consignno = tempstr;
                String tempHawbno = "";
                String tempHawbno1 = "";
                String tempHawbno2 = "";

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

                } else {

                    sSQL = sSQL + emptyString + ",";
                }
            }

            if (tag.equals("SU")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'"+ ",";

                } else {

                    sSQL = sSQL + emptyString+ ",";
                }
            }

             /**
                 * Need to add two new tags here for Carrier Name & Routing Code
                 */
                
                if (tag.equals("SI")) {

                    tempstr = AuditDetailsArray[i][1].trim();

                    if (tempstr.length() > 0) {

                        sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";

                    } else {

                        sSQL = sSQL + emptyString + ",";
                    }
                }
                
                if (tag.equals("SJ")) {

                    tempstr = AuditDetailsArray[i][1].trim();

                    if (tempstr.length() > 0) {

                        sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";

                    } else {

                        sSQL = sSQL + emptyString + ",";
                    }
                }
                
                 if (tag.equals("SK")) {

                    tempstr = AuditDetailsArray[i][1].trim();

                    if (tempstr.length() > 0) {

                        sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";

                    } else {

                        sSQL = sSQL + emptyString + ",";
                    }
                }
                // END of adding 2 new tags
                
            if (tag.equals("TW")) {

                tempstr = AuditDetailsArray[i][1].trim();

                if (tempstr.length() > 0) {

                    sSQL = sSQL + tempstr ;

                } else {
                    //String sqlWeight="''";
                    //int itemp=0;
                    sSQL = sSQL + itemp ;
                }


             //  62


                sSQL = sqlAuditHeader + sSQL + ")";

                try {
                    conAudit = cOpenConnection();
                    stmtAudit = conAudit.createStatement();
                    System.out.println("sqlAudit:"+sSQL);
                    stmtAudit.executeUpdate(sSQL);

                    if(sProductType.equals("DOX"))
                    {
                        String sqlCommodity = "insert into audit_commodity(com_sessionid,com_accno,com_code,com_description,com_quantity,com_value,com_indemnity,com_jobno)" +
                                "values('" + session_ID + "','" + acc_id + "','DOX','DOCUMENTS','1','0','N','"+sJobNo+"')";
                        stmtAudit.executeUpdate(sqlCommodity);
                    }
                    // sSQL="";
                    conAudit.close();
                } catch (Exception e) {
                    vShowErrorPageReporting(res, e);
                }

                sSQL = "";
                sql_Product = "";
                sql_Commodity = "";
                sql_audittrial = "";

            }


        }
    //}
    }

    /**
     *
     * @param session_ID
     * @param acc_id
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void seqInsert(String session_ID, String acc_id) throws ServletException, IOException{

        Connection conSeq = null;
        Statement stmtSeq = null;//
        String sql_SeqFirst="";
        String sql_SeqSecond="";

        for (int s = 1; s <= ndi_totpieces; s++) {


            sql_SeqFirst = "insert into audit_seq(seq_sessionid,seq_accno,seq_no,seq_jobno,seq_consignno) values('" + session_ID + "','" + acc_id + "', " + s + ",'" + ndi_jobno + "','" + ndi_consignno + "')";
            sql_SeqSecond = "insert into auditseq_second(sequence_sessionid,sequence_accno,sequence_no,sequence_jobno,sequence_consignno) values('" + session_ID + "' ,'" + acc_id + "'," + s + ",'" + ndi_jobno + "','" + ndi_consignno + "')";

            try {

                if (s == 1) {
                    for (int j = 0; j < 2; j++) {
                        conSeq = cOpenConnection();
                        stmtSeq = conSeq.createStatement();
                        stmtSeq.executeUpdate(sql_SeqFirst);
                    }

                    for (int j = 0; j < 2; j++) {
                        conSeq = cOpenConnection();
                        stmtSeq = conSeq.createStatement();
                        stmtSeq.executeUpdate(sql_SeqSecond);
                    }
                } else {
                    if (s % 2 == 0) {
                        conSeq = cOpenConnection();
                        stmtSeq = conSeq.createStatement();
                        stmtSeq.executeUpdate(sql_SeqFirst);
                    } else {
                        conSeq = cOpenConnection();
                        stmtSeq = conSeq.createStatement();
                        stmtSeq.executeUpdate(sql_SeqSecond);
                    }

                }

                conSeq.close();

            } catch (Exception e) {
                vShowErrorPageReporting(res, e);
            }


        }

    }

    /**
     *
     * @param sDelete
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void deleteAuditdetails(String sDelete) throws ServletException, IOException{
        try {
            Connection conDelete = cOpenConnection();
            Statement stmtDelete = conDelete.createStatement();
            stmtDelete.executeUpdate(sDelete);
            System.out.println("Delete=" + sDelete);
            conDelete.close();
        } catch (Exception e) {
            vShowErrorPageReporting(res, e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
