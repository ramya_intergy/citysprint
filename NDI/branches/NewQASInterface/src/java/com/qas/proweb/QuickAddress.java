/* ----------------------------------------------------------------------------
 * QuickAddress Pro Web > (c) QAS Ltd > www.qas.com
 * 
 * Common Classes > QuickAddress.java
 * Main searching functionality
 * ----------------------------------------------------------------------------
 */
package com.qas.proweb;

// generated SOAP client stubs
import com.qas.proweb.soap.*;
// Axis exception
import org.apache.axis.AxisFault;
// The Axis exception is sometimes thrown as its superclass
import java.rmi.RemoteException;
import java.io.Serializable;
// URL used to connect to service
import java.net.URL;
import java.net.MalformedURLException;

/**
 * This class is a facade into ProWeb and provides the main functionality
 * of the package.  It uses the com.qas.proweb.soap package in a stateless manner,
 * but some state is maintained between construction and the "business" call to
 * the soap package - this is to allow for optional settings (e.g.timeout).
 * The instance of this class is not intended to be preserved across (for example) JSPs.
 * That is, the intended usage idiom is
 * "construct instance - set optional settings - call main method (e.g. search) - discard instance"
 *
 */
public class QuickAddress implements Serializable
{
	// ------------------------------------------------------------------------
	// public constants
	// ------------------------------------------------------------------------

	/* Constants for referring to search engine types */
	public static final String SINGLELINE = EngineEnumType._Singleline;
	public static final String TYPEDOWN = EngineEnumType._Typedown;
	public static final String VERIFICATION = EngineEnumType._Verification;

	/* Constants for referring to engine search intensity */
	public static final String EXACT = EngineIntensityType._Exact;
	public static final String CLOSE = EngineIntensityType._Close;
	public static final String EXTENSIVE = EngineIntensityType._Extensive;

	/* This is defined in the server */
	private static final String LINE_SEPARATOR = "|";

	// ------------------------------------------------------------------------
	// private data
	// ------------------------------------------------------------------------

	private QAPortType m_Port = null;

	/** optional settings */
	private EngineType          m_EngineType    = null;
	private ThresholdType       m_Threshold     = null;
	private TimeoutType         m_Timeout       = null;
	private Boolean             m_ToFlatten     = null;
	private EngineIntensityType m_Intensity     = null;
	private QAConfigType        m_Config        = null;

	// ------------------------------------------------------------------------
	// public methods - construction, settings
	// ------------------------------------------------------------------------

	/** disallow default constructor */
	private QuickAddress()
	{
	}

	/** Construct an instance with ProWeb service endpoint URL.
	 * @param sEndpointURL     <code>String</code> specifying the URL of the ProWeb SOAP service
	 * @throws ServiceException if <code>ProWebLocator.getPortType</code> failed
	 * @throws MalformedURLException
	 */
	public QuickAddress(String sEndpointURL)
	throws javax.xml.rpc.ServiceException, MalformedURLException
	{
		ProWebLocator service = new ProWebLocator();
		URL endpointURL = new URL(sEndpointURL);
		m_Port = service.getQAPortType(endpointURL);
		m_Config = new QAConfigType();
	}

	/** Sets engine type to be used in the initial search (SINGLELINE, TYPEDOWN or VERIFICATION).
	 *  If this is not called before <code>search</code> the default value is used (<code>SINGLELINE</code>).
	 *  @param s	<code>String</code> value corresponding to one of the constants: SINGLELINE..VERIFICATION
	 */
	public void setEngineType(String s)
	{
		m_EngineType = new EngineType(s);
	}

	/** Sets engine intensity, i.e. how hard the search engine will work to obtain a match.
	 * Higher intensity values may yield more results but will also result in longer search times.
	 * If this is not called before <code>search</code> the default server value is used (usually CLOSE).
	 *  @param s	<code>String</code> value corresponding to one of the constants: EXACT..EXTENSIVE
	 */
	public void setEngineIntensity(String s)
	{
		m_Intensity = EngineIntensityType.fromString(s);
	}

	/** Sets the threshold that is used to decide whether results will be returned in the picklist,
	 * or whether an informational picklist result will be returned advising more refinement.
	 * Due to the algorithms used with returning result picklists, this value is only used as an approximation.
	 * It should not be assumed that a larger number of picklist items will never be returned,
	 * or a lesser number of results will never be treated as if they exceeded the threshold.
	 * If this is not called before <code>refine</code> or <code>stepIn</code> the default server value is used.
	 *  @param i	<code>int</code> value for the threshold.
	 */
	public void setThreshold(int i)
	{
		String sThreshold = (new Integer(i)).toString();
		m_Threshold = new ThresholdType(sThreshold);
	}

	/** Sets the timeout period, i.e. the time threshold that will cause a search to abort if exceeded.
	 * If this is not called before <code>search</code>, <code>stepIn</code> or <code>refine</code> the
	 * default server value is used (usually 10000 milliseconds).
	 *  @param i        <code>int</code> value for timeout period in milliseconds (0 signifies that searches will not timeout).
	 */
	public void setTimeout(int i)
	{
		String sTimeout = (new Integer(i)).toString();
		m_Timeout = new TimeoutType(sTimeout);
	}

	/** This defines whether the search results will be �flattened� to a single picklist of deliverable results,
	 * or potentially multiple hierarchical picklists of results that can be stepped into.
	 * Flattened picklists are simpler for untrained users to navigate (i.e. website users),
	 * although can potentially lead to larger number of results.
	 * Hierarchical picklists are more suited to users that are trained in using the product
	 * (i.e. intranet users), and produce smaller picklists with results that can be stepped into.
	 * If this is not called before <code>search</code> the default value <code>false</code> is used, which corresponds to hierarchical.
	 * @param b	<code>boolean</code> indicating whether the picklist is to contain only leaf addresses or is to be hierarchical.
	 */
	public void setFlatten(boolean b)
	{
		m_ToFlatten = new Boolean(b);
	}

	/** Sets the server configuration file from which to read in settings, to vary it from the default (qawserve.ini).
	 *  The same configuration settings should generally used for all SOAP actions for consistent behaviour.
	 *  @param s	<code>String</code> configuration file name
	 */
	public void setConfigFile(String s)
	{
		m_Config.setIniFile(s);
	}

	/** Sets the configuration file section from which to read in settings, to vary it from the [QADefault] section.
	 *  The same configuration settings should generally used for all SOAP actions for consistent behaviour.
	 *  @param s	<code>String</code> configuration section name
	 */
	public void setConfigSection(String s)
	{
		m_Config.setIniSection(s);
	}

	// ------------------------------------------------------------------------
	// public methods - searching operations
	// ------------------------------------------------------------------------

	/** This method checks that the combination of country, engine and layout are valid to search upon.
	 * Some data sets are only available for the single-line engine and not the verification engine.
	 * Layouts can also be defined for specific data sets, and so may not always be valid for other sets.
	 * @param   sDataID  <code>String</code> ID of the dataset to be searched on
	 * @param   sLayoutName  <code>String</code> Layout to be used by the final address
	 * @return  boolean
	 */
	public CanSearch canSearch(String sDataId, String sLayoutName)
	throws QasException
	{
		CanSearch result = null;

		// set up the parameter for the SOAP call
		QACanSearch param = new QACanSearch();

		param.setCountry(new ISOType(sDataId));
		param.setEngine(getEngine());
		param.setLayout(sLayoutName);
		param.setQAConfig(m_Config);

		try
		{
			// make the call to the server
			QASearchOk ok = getServer().doCanSearch(param);

			result = new CanSearch(ok);
		}
		catch(RemoteException x)
		{
			mapRemoteExeption(x);
		}
		return result;
	}

	/** This method is used to submit an initial search to the server.
	 *  A search must be performed against a specific dataset and engine.
 	 *  A search will produce either:
 	 *		- A picklist of results, with match information
	 *		- A formatted final address (<code>VERIFICATION</code> only)
	 *		- and the verification level
	 *  The behaviour of a search depends upon the choice of engine and engine options
	 *  Since the <code>Verification</code> engine can result in a <code>FormattedAddress</code>, the layout must be specified.
	 *  All other engine types produce only a <code>Picklist</code> in the <code>SearchResult</code>.
	 * @param   sDataId  <code>String</code> ID of the dataset to be searched on
	 * @param   asSearch    <code>String[]</code> array of search terms
	 * @param   sPromptSet <code>String</code> name of the prompt set used for these search terms
	 * @param   sLayout       <code>String</code> name of the layout, with which to format the address (optional)
	 * @return SearchResult
	 */
	public SearchResult search(String sDataId, String[] asSearch, String sPromptSet, String sLayout)
	throws QasException
	{
		// concatenate search terms
		StringBuffer sSearch = new StringBuffer(asSearch[0]);
		for (int i=1; i < asSearch.length; i++)
		{
			sSearch.append(LINE_SEPARATOR);
			sSearch.append(asSearch[i]);
		}
		
		return search(sDataId, sSearch.toString(), sPromptSet, sLayout);
	}

	/** Method overload: provides the search function without the optional sLayout argument
	 * @param   sDataId  <code>String</code> ID of the dataset to be searched on
	 * @param   asSearch    <code>String[]</code> array of search terms
	 * @param   sPromptSet <code>String</code> name of the prompt set used for these search terms
	 * @return SearchResult
	 */
	public SearchResult search(String sDataId, String[] asSearch, String sPromptSet)
	throws QasException
	{
		return search(sDataId, asSearch, sPromptSet, null);
	}

	/** Method overload: provides the search function with search term as a single string
	 * @param   sDataId  <code>String</code> ID of the dataset to be searched on
	 * @param   sSearch    <code>String</code> search terms
	 * @param   sPromptSet <code>String</code> name of the prompt set used for these search terms
	 * @param   sLayout       <code>String</code> name of the layout, with which to format the address (optional)
	 * @return SearchResult
	 */
	public SearchResult search(String sDataId, String sSearch, String sPromptSet, String sLayout)
	throws QasException
	{
		SearchResult result = null;

		// create engine parameter
		EngineType engine = getEngine();
		engine.setPromptSet(PromptSetType.fromString(sPromptSet));

		// default to flatten = false unless it has been set
		boolean bFlatten = (m_ToFlatten == null ? false : m_ToFlatten.booleanValue());
		engine.setFlatten(bFlatten);
		engine.setTimeout(m_Timeout);
		engine.setThreshold(m_Threshold);

		// set up the parameter for the SOAP call
		QASearch param = new QASearch();
		param.setCountry(new ISOType(sDataId));
		param.setEngine(engine);
		if (sLayout != null)
		{
			param.setLayout(sLayout);
		}
		param.setQAConfig(m_Config);
		param.setSearch(sSearch);

		try
		{
			// make the call to the server
			QASearchResult searchResult = getServer().doSearch(param);

			result = new SearchResult(searchResult);
		}
		catch(RemoteException x)
		{
			mapRemoteExeption(x);
		}
		return result;
	}

	/** Method overload: provides the search function with search term string, without sLayout argument
	 * @param   sDataId  <code>String</code> ID of the dataset to be searched on
	 * @param   sSearch    <code>String</code> search terms
	 * @param   sPromptSet <code>String</code> name of the prompt set used for these search terms
	 * @return SearchResult
	 */
	public SearchResult search(String sDataId, String sSearch, String sPromptSet)
	throws QasException
	{
		return search(sDataId, sSearch, sPromptSet, null);
	}

	/** Method to perform a refinement.
	 * This is used after an initial search has been performed,
	 * when the user enters text to be used to filter upon the picklist,
	 * creating a smaller set of picklist results.
	 * Refinement and stepIn delegate to the same low-level function.
	 * @param   sRefinementText <code>String</code> the refinement text
	 * @param   sMoniker    <code>String</code> the search point moniker of the picklist (item) being refined.
	 * @return Picklist containing the results of the refinement
	 */
	public Picklist refine(String sMoniker, String sRefinementText)
	throws QasException
	{
		Picklist result = null;

		// set up the parameter for the SOAP call
		QARefine param = new QARefine();

		param.setTimeout(m_Timeout);
		param.setThreshold(m_Threshold);
		param.setMoniker(sMoniker);
		param.setRefinement(sRefinementText);
		param.setQAConfig(m_Config);

		try
		{
			// make the call to the server
			QAPicklistType picklist = getServer().doRefine(param).getQAPicklist();

			result = new Picklist(picklist);
		}
		catch(RemoteException x)
		{
			mapRemoteExeption(x);
		}
		return result;
	}

	/** Method to perform a step-in.
	 * This is used after an initial search has been performed, when the user selects an entry that can be
	 * expanded into elements �beneath� it. For example, a picklist entry that represents a street can
	 * often be stepped into so that a picklist of premises beneath the street are displayed.
	 *  Refinement and stepin delegate to the same low-level function.
	 * @param   sMoniker    <code>String</code> the search point moniker of the picklist (item) being stepped into
	 * @return Picklist  containing the results of the stepin.
	 */
	public Picklist stepIn(String sMoniker)
	throws QasException
	{
		return refine(sMoniker, "");
	}

	/** Method to retrieve the final formatted address, formatting a picklist entry.
	 * Typically the user selects a <code>PicklistItem</code> for which <code>isFullAddress()</code>
	 * returns <code>true</code>.
	 * Address formatting is performed using the picklist item moniker and the specified layout.
	 * @param   sLayoutName     <code>String</code> layout name (specifies how the address should be formatted)
	 * @param   sMoniker        <code>String</code> search point moniker that represents the address
	 * @return  FormattedAddress relating to the search point moniker and layout.
	 */
	public FormattedAddress getFormattedAddress(String sLayoutName, String sMoniker)
	throws QasException
	{
		FormattedAddress result = null;

		// set up the parameter for the SOAP call
		QAGetAddress param = new QAGetAddress();
		param.setLayout(sLayoutName);
		param.setMoniker(sMoniker);
		param.setQAConfig(m_Config);

		try
		{
			// make the call to the server
			QAAddressType address = getServer().doGetAddress(param).getQAAddress();
			result = new FormattedAddress(address);
		}
		catch(RemoteException x)
		{
			mapRemoteExeption(x);
		}

		return result;
	}

	// ------------------------------------------------------------------------
	// public methods - status operations
	// ------------------------------------------------------------------------

	/** Method to retrieve a list of available data sets that can be searched against with ProWeb.
	 * Specifically, this will return an array of <code>DataSet</code> objects that are valid to
	 * be passed to the searching methods.
	 * For a list of installed data files and their respective license status, the
	 * <code>getLicenseInfo</code> method can be used instead.
	 * @return  DataSet[]
	 */
	public DataSet[] getAllDataSets()
	throws QasException
	{
		DataSet[] results = null;

		try
		{
			// make the call to the server
			QADataSet[] aDataSets = getServer().doGetData().getDataSet();
			results = DataSet.createArray(aDataSets);
		}
		catch(RemoteException x)
		{
			mapRemoteExeption(x);
		}

		return results;
	}

	/** Method to retrieve a list of layouts that have been configured within
	 * the server configuration file, and can be used to format address results.
	 * A list of layouts is useful for situations where the integration would give
	 * the user a choice of which layout to use to format an address result.
	 * If only one layout is ever used within an integration, it would be more
	 * common to explicitly code the layout name.
	 * @param   sDataId  <code>String</code> ID of the dataset whose layouts are required
	 * @return  Layout[]
	 */
	public Layout[] getAllLayouts(String sDataId)
	throws QasException
	{
		Layout[] results = null;

		// set up the parameter for the SOAP call
		QAGetLayouts param = new QAGetLayouts();
		param.setCountry(new ISOType(sDataId));
		param.setQAConfig(m_Config);

		try
		{
			// make the call to the server
			QALayout[] aLayouts = getServer().doGetLayouts(param).getLayout();
			results = Layout.createArray(aLayouts);
		}
		catch(RemoteException x)
		{
			mapRemoteExeption(x);
		}

		return results;
	}

	/** Method to return an array of fully formatted example addresses. This may commonly be used
	 * to preview a given layout with a set of addresses.
	 * @param   sDataId      <code>String</code> ID of the dataset for which examples are required
	 * @param   sLayoutName     <code>String</code> name of the layout for formatting
	 * @return  ExampleAddress[]
	 */
	public ExampleAddress[] getExampleAddresses(String sDataId, String sLayoutName)
	throws QasException
	{
		ExampleAddress[] results = null;

		// set up the parameter for the SOAP call
		QAGetExampleAddresses param = new QAGetExampleAddresses();
		param.setLayout(sLayoutName);
		param.setCountry(new ISOType(sDataId));
		param.setQAConfig(m_Config);

		try
		{
			// make the call to the server
			QAExampleAddress[] aAddresses = getServer().doGetExampleAddresses(param).getExampleAddress();
			results = ExampleAddress.createArray(aAddresses);
		}
		catch(RemoteException x)
		{
			mapRemoteExeption(x);
		}

		return results;
	}

	/** Method to obtain a list of installed data, and any relevant licensing information such as
	 * days until data expiry, and data versions. Unlike the <code>getAllDataSets</code> method,
	 * this will return information about all data sets, including DataPlus files.
	 * This method is designed for the integrator to access information regarding the data files,
	 * it is not suitable for display to web users.  If you wish to obtain a list of datasets that
	 * can be searched against, then the <code>getAllDataSets</code> method should be used instead.
	 * @return  LicensedSet[] array of objects detailing the licence state.
	 */
	public LicensedSet[] getLicenceInfo()
	throws QasException
	{
		LicensedSet[] results = null;

		try
		{
			// make the call to the server
			QALicenceInfo info = getServer().doGetLicenseInfo();
			results = LicensedSet.createArray(info);
		}
		catch(RemoteException x)
		{
			mapRemoteExeption(x);
		}

		return results;
	}

	/** Method to retrieve a prompt set for a given data set.
	 * This is used to obtain information regarding a prompt set, such as the number of lines and suggested input.
	 * When searching with the verification engine, the default prompt set should be always used. The reported number
	 * of input lines will be set if an input line restriction is specified within the configuration file using the
	 * setting InputLineCount. If there is no restriction applied, there is no defined number of input lines and the
	 * integrator can create the input fields in what ever manner they wish.
	 * For all other prompt sets, a prompt set may have a different number of input lines depending upon the
	 * data set being searched upon.
	 * @param   sDataId  <code>String</code> ID of the dataset whose prompt set is required
	 * @param   sPromptSetName   <code>String</code> identifies the type of promt set: one of the <code>PromptSet</code> class.
	 * @return  PromptSet  which wraps an array of prompt lines identified by the name and country.
	 */
	public PromptSet getPromptSet(String sDataId, String sPromptSetName)
	throws QasException
	{
		PromptSet result = null;

		// set up the parameter for the SOAP call
		QAGetPromptSet param = new QAGetPromptSet();
		param.setCountry(new ISOType(sDataId));
		param.setEngine(getEngine());
		param.setPromptSet(PromptSetType.fromString(sPromptSetName));
		param.setQAConfig(m_Config);

		try
		{
			// make the call to the server
			QAPromptSet promptSet = getServer().doGetPromptSet(param);
			result = new PromptSet(promptSet);
		}
		catch(RemoteException x)
		{
			mapRemoteExeption(x);
		}

		return result;
	}

	/** Method to get textual system (diagnostic) information from the server.
	 * @return  String[] each string in the array is a (tab separated) name/value pair of system info.
	 */
	public String[] getSystemInfo()
	throws QasException
	{
		String[] results = null;

		try
		{
			// make the call to the server
			QASystemInfo info = getServer().doGetSystemInfo();
			results = info.getSystemInfo();
		}
		catch(RemoteException x)
		{
			mapRemoteExeption(x);
		}

		return results;
	}

	// ------------------------------------------------------------------------
	// private methods
	// ------------------------------------------------------------------------

	/** helper to return the port */
	private QAPortType getServer()
	{
		return m_Port;
	}

	/** helper */
	private EngineType getEngine()
	{
		if (m_EngineType == null)
		{
			setEngineType(SINGLELINE);
		}
		return m_EngineType;
	}

	/** helper that converts a remote exception (potentially thrown from the
	 *  com.qas.proweb.soap package) into a QasException.
	 */
	private void mapRemoteExeption(RemoteException x)
	throws QasException
	{
		if (x instanceof AxisFault)
		{
			// downcast
			AxisFault fault = (AxisFault)x;
			QasException exception = QasException.createFrom(fault);
			throw exception;
		}
		else
		{
			throw new QasException("Caught RemoteException which is not an AxisFault!",
									QasException.UNKNOWN_COMMS_ERROR);
		}
	}
}
