/* ----------------------------------------------------------------------------
 * QuickAddress Pro Web > (c) QAS Ltd > www.qas.com
 *
 * Web > Verification > VerifyRefine.java
 * Prompt for additional (premise) address details, and check for suitability
 * ----------------------------------------------------------------------------
 */
package com.qas.proweb.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qas.proweb.*;
/**
 * Encapsulates logic for the refinement page in the verification scenario.  This is only called
 * if an unresolvable range, incomplete address or phantom primary point (AUS) is selected,
 * as these need <code>refine</code> called in order to pass additional info for the final address.
 * Input params: DataId, CountryName, RefineInput, Moniker (also set as output attributes)
 * Output atributes: IsPhantom, RefineLine, RefineAddress, RefineFail; Route, ErrorInfo (if an exception is caught)
 *
 */
public class VerifyRefineAddress implements Command
{
	public static final String NAME = "VerifyRefineAddress";

	// Code-to-page field constants
	public static final String IS_PHANTOM = "IsPhantom";
	public static final String REFINE_FAIL = "RefineFailed";
	public static final String REFINE_LINE = "RefineLine";
	public static final String REFINE_ADDRESS = "RefineAddress";

	/**
	 * @see com.qas.proweb.servlet.Command#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public String execute(HttpServletRequest request,
						  HttpServletResponse response)
	{
		// Parameters to pass through
		HttpHelper.passThrough(request, Constants.DATA_ID);
		HttpHelper.passThrough(request, Constants.COUNTRY_NAME);
		HttpHelper.passThroughArray(request, Constants.USER_INPUT);

		String sMoniker = HttpHelper.passThrough(request, Constants.MONIKER);
		String sRefineInput = HttpHelper.passThrough(request, Constants.REFINE_INPUT);

		boolean bGoFormatPage = false;

		try
		{
			QuickAddress searchService = new QuickAddress(Constants.ENDPOINT);

			// Perform the refinement
			Picklist picklist = searchService.refine(sMoniker, sRefineInput);

			// If the refined search produces no results, recreate the picklist and update the message
			if ( picklist.getItems() == null )
			{
				// No acceptable address match - recreate without using refinement text
				picklist = searchService.refine(sMoniker, "");
			}

			PicklistItem item = picklist.getItems()[0];
			request.setAttribute(REFINE_LINE, item.getText());
			request.setAttribute(REFINE_ADDRESS, item.getPartialAddress());

			if (sRefineInput.equals(""))
			{
				// First time through - simply display appropriate message
				if (item.isPhantomPrimaryPoint())
				{
					request.setAttribute(IS_PHANTOM, Boolean.TRUE.toString());
				}
			}
			else if (item.isUnresolvableRange())
			{
				// Redisplay with explanation as to how the refinement text failed to match
				request.setAttribute(REFINE_FAIL, Boolean.TRUE.toString());
			}
			else
			{
				// Accept the address (or force accept in the Phantom case)
				request.setAttribute(Constants.MONIKER, item.getMoniker());
				// No more refinement necessary - now go straight to the format address command
				bGoFormatPage = true;
			}
		}
		catch (Exception e)
		{
			// Dump the exception to error stream
			System.err.println("~~~~~~~~~ Caught exception in VerifyRefine command ~~~~~~~~~~");
			e.printStackTrace();
			System.err.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

			request.setAttribute(Constants.ROUTE, Constants.ROUTE_FAILED);
			request.setAttribute(Constants.ERROR_INFO, e.toString());
			bGoFormatPage = true;
		}

		if (bGoFormatPage)
		{
			// Go straight to format address
			Command formatAddress = new VerifyFormatAddress();
			return formatAddress.execute(request, response);
		}
		
		return "/verifyRefine.jsp";
	}

}
