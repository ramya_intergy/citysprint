/**
 * AddressLineType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class AddressLineType  implements java.io.Serializable {
    private java.lang.String label;
    private java.lang.String line;
    private com.qas.proweb.soap.LineContentType lineContent;  // attribute
    private boolean overflow;  // attribute
    private boolean truncated;  // attribute

    public AddressLineType() {
    }

    public java.lang.String getLabel() {
        return label;
    }

    public void setLabel(java.lang.String label) {
        this.label = label;
    }

    public java.lang.String getLine() {
        return line;
    }

    public void setLine(java.lang.String line) {
        this.line = line;
    }

    public com.qas.proweb.soap.LineContentType getLineContent() {
        return lineContent;
    }

    public void setLineContent(com.qas.proweb.soap.LineContentType lineContent) {
        this.lineContent = lineContent;
    }

    public boolean isOverflow() {
        return overflow;
    }

    public void setOverflow(boolean overflow) {
        this.overflow = overflow;
    }

    public boolean isTruncated() {
        return truncated;
    }

    public void setTruncated(boolean truncated) {
        this.truncated = truncated;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddressLineType)) return false;
        AddressLineType other = (AddressLineType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((label==null && other.getLabel()==null) || 
             (label!=null &&
              label.equals(other.getLabel()))) &&
            ((line==null && other.getLine()==null) || 
             (line!=null &&
              line.equals(other.getLine()))) &&
            ((lineContent==null && other.getLineContent()==null) || 
             (lineContent!=null &&
              lineContent.equals(other.getLineContent()))) &&
            overflow == other.isOverflow() &&
            truncated == other.isTruncated();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLabel() != null) {
            _hashCode += getLabel().hashCode();
        }
        if (getLine() != null) {
            _hashCode += getLine().hashCode();
        }
        if (getLineContent() != null) {
            _hashCode += getLineContent().hashCode();
        }
        _hashCode += new Boolean(isOverflow()).hashCode();
        _hashCode += new Boolean(isTruncated()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddressLineType.class);

    static {
        org.apache.axis.description.FieldDesc field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("lineContent");
        field.setXmlName(new javax.xml.namespace.QName("", "LineContent"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "LineContentType"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("overflow");
        field.setXmlName(new javax.xml.namespace.QName("", "Overflow"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("truncated");
        field.setXmlName(new javax.xml.namespace.QName("", "Truncated"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("label");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Label"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("line");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Line"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
