/**
 * PicklistEntryType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class PicklistEntryType  implements java.io.Serializable {
    private java.lang.String moniker;
    private java.lang.String partialAddress;
    private java.lang.String picklist;
    private java.lang.String postcode;
    private org.apache.axis.types.NonNegativeInteger score;
    private boolean fullAddress;  // attribute
    private boolean multiples;  // attribute
    private boolean canStep;  // attribute
    private boolean aliasMatch;  // attribute
    private boolean postcodeRecoded;  // attribute
    private boolean crossBorderMatch;  // attribute
    private boolean dummyPOBox;  // attribute
    private boolean name;  // attribute
    private boolean information;  // attribute
    private boolean warnInformation;  // attribute
    private boolean incompleteAddr;  // attribute
    private boolean unresolvableRange;  // attribute
    private boolean phantomPrimaryPoint;  // attribute

    public PicklistEntryType() {
    }

    public java.lang.String getMoniker() {
        return moniker;
    }

    public void setMoniker(java.lang.String moniker) {
        this.moniker = moniker;
    }

    public java.lang.String getPartialAddress() {
        return partialAddress;
    }

    public void setPartialAddress(java.lang.String partialAddress) {
        this.partialAddress = partialAddress;
    }

    public java.lang.String getPicklist() {
        return picklist;
    }

    public void setPicklist(java.lang.String picklist) {
        this.picklist = picklist;
    }

    public java.lang.String getPostcode() {
        return postcode;
    }

    public void setPostcode(java.lang.String postcode) {
        this.postcode = postcode;
    }

    public org.apache.axis.types.NonNegativeInteger getScore() {
        return score;
    }

    public void setScore(org.apache.axis.types.NonNegativeInteger score) {
        this.score = score;
    }

    public boolean isFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(boolean fullAddress) {
        this.fullAddress = fullAddress;
    }

    public boolean isMultiples() {
        return multiples;
    }

    public void setMultiples(boolean multiples) {
        this.multiples = multiples;
    }

    public boolean isCanStep() {
        return canStep;
    }

    public void setCanStep(boolean canStep) {
        this.canStep = canStep;
    }

    public boolean isAliasMatch() {
        return aliasMatch;
    }

    public void setAliasMatch(boolean aliasMatch) {
        this.aliasMatch = aliasMatch;
    }

    public boolean isPostcodeRecoded() {
        return postcodeRecoded;
    }

    public void setPostcodeRecoded(boolean postcodeRecoded) {
        this.postcodeRecoded = postcodeRecoded;
    }

    public boolean isCrossBorderMatch() {
        return crossBorderMatch;
    }

    public void setCrossBorderMatch(boolean crossBorderMatch) {
        this.crossBorderMatch = crossBorderMatch;
    }

    public boolean isDummyPOBox() {
        return dummyPOBox;
    }

    public void setDummyPOBox(boolean dummyPOBox) {
        this.dummyPOBox = dummyPOBox;
    }

    public boolean isName() {
        return name;
    }

    public void setName(boolean name) {
        this.name = name;
    }

    public boolean isInformation() {
        return information;
    }

    public void setInformation(boolean information) {
        this.information = information;
    }

    public boolean isWarnInformation() {
        return warnInformation;
    }

    public void setWarnInformation(boolean warnInformation) {
        this.warnInformation = warnInformation;
    }

    public boolean isIncompleteAddr() {
        return incompleteAddr;
    }

    public void setIncompleteAddr(boolean incompleteAddr) {
        this.incompleteAddr = incompleteAddr;
    }

    public boolean isUnresolvableRange() {
        return unresolvableRange;
    }

    public void setUnresolvableRange(boolean unresolvableRange) {
        this.unresolvableRange = unresolvableRange;
    }

    public boolean isPhantomPrimaryPoint() {
        return phantomPrimaryPoint;
    }

    public void setPhantomPrimaryPoint(boolean phantomPrimaryPoint) {
        this.phantomPrimaryPoint = phantomPrimaryPoint;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PicklistEntryType)) return false;
        PicklistEntryType other = (PicklistEntryType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((moniker==null && other.getMoniker()==null) || 
             (moniker!=null &&
              moniker.equals(other.getMoniker()))) &&
            ((partialAddress==null && other.getPartialAddress()==null) || 
             (partialAddress!=null &&
              partialAddress.equals(other.getPartialAddress()))) &&
            ((picklist==null && other.getPicklist()==null) || 
             (picklist!=null &&
              picklist.equals(other.getPicklist()))) &&
            ((postcode==null && other.getPostcode()==null) || 
             (postcode!=null &&
              postcode.equals(other.getPostcode()))) &&
            ((score==null && other.getScore()==null) || 
             (score!=null &&
              score.equals(other.getScore()))) &&
            fullAddress == other.isFullAddress() &&
            multiples == other.isMultiples() &&
            canStep == other.isCanStep() &&
            aliasMatch == other.isAliasMatch() &&
            postcodeRecoded == other.isPostcodeRecoded() &&
            crossBorderMatch == other.isCrossBorderMatch() &&
            dummyPOBox == other.isDummyPOBox() &&
            name == other.isName() &&
            information == other.isInformation() &&
            warnInformation == other.isWarnInformation() &&
            incompleteAddr == other.isIncompleteAddr() &&
            unresolvableRange == other.isUnresolvableRange() &&
            phantomPrimaryPoint == other.isPhantomPrimaryPoint();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMoniker() != null) {
            _hashCode += getMoniker().hashCode();
        }
        if (getPartialAddress() != null) {
            _hashCode += getPartialAddress().hashCode();
        }
        if (getPicklist() != null) {
            _hashCode += getPicklist().hashCode();
        }
        if (getPostcode() != null) {
            _hashCode += getPostcode().hashCode();
        }
        if (getScore() != null) {
            _hashCode += getScore().hashCode();
        }
        _hashCode += new Boolean(isFullAddress()).hashCode();
        _hashCode += new Boolean(isMultiples()).hashCode();
        _hashCode += new Boolean(isCanStep()).hashCode();
        _hashCode += new Boolean(isAliasMatch()).hashCode();
        _hashCode += new Boolean(isPostcodeRecoded()).hashCode();
        _hashCode += new Boolean(isCrossBorderMatch()).hashCode();
        _hashCode += new Boolean(isDummyPOBox()).hashCode();
        _hashCode += new Boolean(isName()).hashCode();
        _hashCode += new Boolean(isInformation()).hashCode();
        _hashCode += new Boolean(isWarnInformation()).hashCode();
        _hashCode += new Boolean(isIncompleteAddr()).hashCode();
        _hashCode += new Boolean(isUnresolvableRange()).hashCode();
        _hashCode += new Boolean(isPhantomPrimaryPoint()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PicklistEntryType.class);

    static {
        org.apache.axis.description.FieldDesc field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("fullAddress");
        field.setXmlName(new javax.xml.namespace.QName("", "FullAddress"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("multiples");
        field.setXmlName(new javax.xml.namespace.QName("", "Multiples"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("canStep");
        field.setXmlName(new javax.xml.namespace.QName("", "CanStep"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("aliasMatch");
        field.setXmlName(new javax.xml.namespace.QName("", "AliasMatch"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("postcodeRecoded");
        field.setXmlName(new javax.xml.namespace.QName("", "PostcodeRecoded"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("crossBorderMatch");
        field.setXmlName(new javax.xml.namespace.QName("", "CrossBorderMatch"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("dummyPOBox");
        field.setXmlName(new javax.xml.namespace.QName("", "DummyPOBox"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("name");
        field.setXmlName(new javax.xml.namespace.QName("", "Name"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("information");
        field.setXmlName(new javax.xml.namespace.QName("", "Information"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("warnInformation");
        field.setXmlName(new javax.xml.namespace.QName("", "WarnInformation"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("incompleteAddr");
        field.setXmlName(new javax.xml.namespace.QName("", "IncompleteAddr"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("unresolvableRange");
        field.setXmlName(new javax.xml.namespace.QName("", "UnresolvableRange"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("phantomPrimaryPoint");
        field.setXmlName(new javax.xml.namespace.QName("", "PhantomPrimaryPoint"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("moniker");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Moniker"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("partialAddress");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "PartialAddress"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("picklist");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Picklist"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("postcode");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Postcode"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("score");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Score"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "nonNegativeInteger"));
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
