/**
 * QAPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public interface QAPortType extends java.rmi.Remote {
    public com.qas.proweb.soap.QASearchResult doSearch(com.qas.proweb.soap.QASearch body) throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault;
    public com.qas.proweb.soap.Picklist doRefine(com.qas.proweb.soap.QARefine body) throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault;
    public com.qas.proweb.soap.Address doGetAddress(com.qas.proweb.soap.QAGetAddress body) throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault;
    public com.qas.proweb.soap.QAData doGetData() throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault;
    public com.qas.proweb.soap.QALicenceInfo doGetLicenseInfo() throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault;
    public com.qas.proweb.soap.QASystemInfo doGetSystemInfo() throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault;
    public com.qas.proweb.soap.QAExampleAddresses doGetExampleAddresses(com.qas.proweb.soap.QAGetExampleAddresses body) throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault;
    public com.qas.proweb.soap.QALayouts doGetLayouts(com.qas.proweb.soap.QAGetLayouts body) throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault;
    public com.qas.proweb.soap.QAPromptSet doGetPromptSet(com.qas.proweb.soap.QAGetPromptSet body) throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault;
    public com.qas.proweb.soap.QASearchOk doCanSearch(com.qas.proweb.soap.QACanSearch body) throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault;
}
