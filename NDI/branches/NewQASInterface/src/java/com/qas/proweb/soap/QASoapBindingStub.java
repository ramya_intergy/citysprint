/**
 * QASoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class QASoapBindingStub extends org.apache.axis.client.Stub implements com.qas.proweb.soap.QAPortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    public QASoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public QASoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public QASoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "LineContentType");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.LineContentType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QALicenceInfo");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QALicenceInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAExampleAddress");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QAExampleAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAData");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QAData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "EngineIntensityType");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.EngineIntensityType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QASystemInfo");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QASystemInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "VerifyLevelType");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.VerifyLevelType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAGetAddress");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QAGetAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QADataSet");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QADataSet.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "ThresholdType");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.ThresholdType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAAddressType");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QAAddressType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QALayouts");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QALayouts.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QALayout");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QALayout.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "PromptLine");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.PromptLine.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QASearchResult");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QASearchResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAPicklistType");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QAPicklistType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "ISOType");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.ISOType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QALicensedSet");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QALicensedSet.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "EngineEnumType");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.EngineEnumType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAExampleAddresses");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QAExampleAddresses.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "EngineType");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.EngineType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "AddressLineType");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.AddressLineType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAGetLayouts");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QAGetLayouts.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAPromptSet");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QAPromptSet.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QACanSearch");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QACanSearch.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "LicenceWarningLevel");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.LicenceWarningLevel.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "TimeoutType");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.TimeoutType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(simplesf);
            cachedDeserFactories.add(simpledf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "PromptSetType");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.PromptSetType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QARefine");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QARefine.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAGetPromptSet");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QAGetPromptSet.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">Address");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.Address.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QASearch");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QASearch.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">Picklist");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.Picklist.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAFault");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QAFault.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QASearchOk");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QASearchOk.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAGetExampleAddresses");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QAGetExampleAddresses.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAConfigType");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.QAConfigType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "PicklistEntryType");
            cachedSerQNames.add(qName);
            cls = com.qas.proweb.soap.PicklistEntryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    private org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call =
                    (org.apache.axis.client.Call) super.service.createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                if(_call.isPropertySupported(key))
                    _call.setProperty(key, super.cachedProperties.get(key));
                else
                    _call.setScopedProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                        java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                        _call.registerTypeMapping(cls, qName, sf, df, false);
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", t);
        }
    }

    public com.qas.proweb.soap.QASearchResult doSearch(com.qas.proweb.soap.QASearch body) throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.addParameter(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QASearch"), new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QASearch"), com.qas.proweb.soap.QASearch.class, javax.xml.rpc.ParameterMode.IN);
        _call.setReturnType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QASearchResult"), com.qas.proweb.soap.QASearchResult.class);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.qas.com/web-2005-02/DoSearch");
        _call.setEncodingStyle(null);
        _call.setScopedProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setScopedProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setOperationStyle("document");
        _call.setOperationName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QASearch"));

        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            try {
                return (com.qas.proweb.soap.QASearchResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.qas.proweb.soap.QASearchResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.qas.proweb.soap.QASearchResult.class);
            }
        }
    }

    public com.qas.proweb.soap.Picklist doRefine(com.qas.proweb.soap.QARefine body) throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.addParameter(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QARefine"), new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QARefine"), com.qas.proweb.soap.QARefine.class, javax.xml.rpc.ParameterMode.IN);
        _call.setReturnType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">Picklist"), com.qas.proweb.soap.Picklist.class);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.qas.com/web-2005-02/DoRefine");
        _call.setEncodingStyle(null);
        _call.setScopedProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setScopedProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setOperationStyle("document");
        _call.setOperationName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QARefine"));

        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            try {
                return (com.qas.proweb.soap.Picklist) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.qas.proweb.soap.Picklist) org.apache.axis.utils.JavaUtils.convert(_resp, com.qas.proweb.soap.Picklist.class);
            }
        }
    }

    public com.qas.proweb.soap.Address doGetAddress(com.qas.proweb.soap.QAGetAddress body) throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.addParameter(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAGetAddress"), new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAGetAddress"), com.qas.proweb.soap.QAGetAddress.class, javax.xml.rpc.ParameterMode.IN);
        _call.setReturnType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">Address"), com.qas.proweb.soap.Address.class);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.qas.com/web-2005-02/DoGetAddress");
        _call.setEncodingStyle(null);
        _call.setScopedProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setScopedProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setOperationStyle("document");
        _call.setOperationName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAGetAddress"));

        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            try {
                return (com.qas.proweb.soap.Address) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.qas.proweb.soap.Address) org.apache.axis.utils.JavaUtils.convert(_resp, com.qas.proweb.soap.Address.class);
            }
        }
    }

    public com.qas.proweb.soap.QAData doGetData() throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setReturnType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAData"), com.qas.proweb.soap.QAData.class);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.qas.com/web-2005-02/DoGetData");
        _call.setEncodingStyle(null);
        _call.setScopedProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setScopedProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setOperationStyle("document");
        _call.setOperationName(new javax.xml.namespace.QName("", "DoGetData"));

        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            try {
                return (com.qas.proweb.soap.QAData) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.qas.proweb.soap.QAData) org.apache.axis.utils.JavaUtils.convert(_resp, com.qas.proweb.soap.QAData.class);
            }
        }
    }

    public com.qas.proweb.soap.QALicenceInfo doGetLicenseInfo() throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setReturnType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QALicenceInfo"), com.qas.proweb.soap.QALicenceInfo.class);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.qas.com/web-2005-02/DoGetLicenseInfo");
        _call.setEncodingStyle(null);
        _call.setScopedProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setScopedProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setOperationStyle("document");
        _call.setOperationName(new javax.xml.namespace.QName("", "DoGetLicenseInfo"));

        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            try {
                return (com.qas.proweb.soap.QALicenceInfo) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.qas.proweb.soap.QALicenceInfo) org.apache.axis.utils.JavaUtils.convert(_resp, com.qas.proweb.soap.QALicenceInfo.class);
            }
        }
    }

    public com.qas.proweb.soap.QASystemInfo doGetSystemInfo() throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setReturnType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QASystemInfo"), com.qas.proweb.soap.QASystemInfo.class);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.qas.com/web-2005-02/DoGetSystemInfo");
        _call.setEncodingStyle(null);
        _call.setScopedProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setScopedProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setOperationStyle("document");
        _call.setOperationName(new javax.xml.namespace.QName("", "DoGetSystemInfo"));

        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            try {
                return (com.qas.proweb.soap.QASystemInfo) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.qas.proweb.soap.QASystemInfo) org.apache.axis.utils.JavaUtils.convert(_resp, com.qas.proweb.soap.QASystemInfo.class);
            }
        }
    }

    public com.qas.proweb.soap.QAExampleAddresses doGetExampleAddresses(com.qas.proweb.soap.QAGetExampleAddresses body) throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.addParameter(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAGetExampleAddresses"), new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAGetExampleAddresses"), com.qas.proweb.soap.QAGetExampleAddresses.class, javax.xml.rpc.ParameterMode.IN);
        _call.setReturnType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAExampleAddresses"), com.qas.proweb.soap.QAExampleAddresses.class);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.qas.com/web-2005-02/DoGetExampleAddresses");
        _call.setEncodingStyle(null);
        _call.setScopedProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setScopedProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setOperationStyle("document");
        _call.setOperationName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAGetExampleAddresses"));

        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            try {
                return (com.qas.proweb.soap.QAExampleAddresses) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.qas.proweb.soap.QAExampleAddresses) org.apache.axis.utils.JavaUtils.convert(_resp, com.qas.proweb.soap.QAExampleAddresses.class);
            }
        }
    }

    public com.qas.proweb.soap.QALayouts doGetLayouts(com.qas.proweb.soap.QAGetLayouts body) throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.addParameter(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAGetLayouts"), new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAGetLayouts"), com.qas.proweb.soap.QAGetLayouts.class, javax.xml.rpc.ParameterMode.IN);
        _call.setReturnType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QALayouts"), com.qas.proweb.soap.QALayouts.class);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.qas.com/web-2005-02/DoGetLayouts");
        _call.setEncodingStyle(null);
        _call.setScopedProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setScopedProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setOperationStyle("document");
        _call.setOperationName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAGetLayouts"));

        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            try {
                return (com.qas.proweb.soap.QALayouts) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.qas.proweb.soap.QALayouts) org.apache.axis.utils.JavaUtils.convert(_resp, com.qas.proweb.soap.QALayouts.class);
            }
        }
    }

    public com.qas.proweb.soap.QAPromptSet doGetPromptSet(com.qas.proweb.soap.QAGetPromptSet body) throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.addParameter(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAGetPromptSet"), new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAGetPromptSet"), com.qas.proweb.soap.QAGetPromptSet.class, javax.xml.rpc.ParameterMode.IN);
        _call.setReturnType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QAPromptSet"), com.qas.proweb.soap.QAPromptSet.class);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.qas.com/web-2005-02/DoGetPromptSet");
        _call.setEncodingStyle(null);
        _call.setScopedProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setScopedProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setOperationStyle("document");
        _call.setOperationName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAGetPromptSet"));

        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            try {
                return (com.qas.proweb.soap.QAPromptSet) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.qas.proweb.soap.QAPromptSet) org.apache.axis.utils.JavaUtils.convert(_resp, com.qas.proweb.soap.QAPromptSet.class);
            }
        }
    }

    public com.qas.proweb.soap.QASearchOk doCanSearch(com.qas.proweb.soap.QACanSearch body) throws java.rmi.RemoteException, com.qas.proweb.soap.QAFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.addParameter(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QACanSearch"), new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QACanSearch"), com.qas.proweb.soap.QACanSearch.class, javax.xml.rpc.ParameterMode.IN);
        _call.setReturnType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", ">QASearchOk"), com.qas.proweb.soap.QASearchOk.class);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.qas.com/web-2005-02/DoCanSearch");
        _call.setEncodingStyle(null);
        _call.setScopedProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setScopedProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setOperationStyle("document");
        _call.setOperationName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QACanSearch"));

        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            try {
                return (com.qas.proweb.soap.QASearchOk) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.qas.proweb.soap.QASearchOk) org.apache.axis.utils.JavaUtils.convert(_resp, com.qas.proweb.soap.QASearchOk.class);
            }
        }
    }

}
