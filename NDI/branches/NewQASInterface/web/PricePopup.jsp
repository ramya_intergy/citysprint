<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="com.qas.proweb.servlet.*" %>
<%
    String sPrice=request.getParameter("sPrice")==null?"0.00":request.getParameter("sPrice").toString();

%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <title>CitySprint - Price Details</title>
        <link media="screen" href="CSS/NextDayCss.css" type="text/css" rel="stylesheet">
    </head>
    <body style="padding-left:14px">

    <table border="0" cellpadding="2" cellspacing="0" id="box" >
      <br>
            
            <tr>
                <td><b> 
                    <%=sPrice %></b>
                </td><br>
            </tr>
            <tr><td><center> The price quoted is based on the information provided and may be subject to change </center></td>
            </tr>
            <tr>
                <td><br><center><a href="javascript:self.close();">Close</a></center></td>
            </tr>
           </table>  
            
          
 
    
    <%--
    This example uses JSTL, uncomment the taglib directive above.
    To test, display the page like this: index.jsp?sayHello=true&name=Murphy
    --%>
    <%--
    <c:if test="${param.sayHello}">
        <!-- Let's welcome the user ${param.name} -->
        Hello ${param.name}!
    </c:if>
    --%>
    
    </body>
</html>
