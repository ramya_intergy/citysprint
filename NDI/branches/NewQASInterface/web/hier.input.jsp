<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page contentType="text/html" %>
<%@ page import="com.qas.proweb.servlet.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%
	/** Intranet, Input page - country selection and search term **/
	
	/** Get query parameters **/
	// Data ID
	String sDataId = (String) request.getAttribute(Constants.DATA_ID);
	// Search term (HTML escape quotes)
	String sUserInputEnc = StringEscapeUtils.escapeHtml((String) request.getAttribute(Constants.USER_INPUT));
%>

<html>
	<head>
		<link rel="stylesheet" href="CSS/style.css" type="text/css"/>
		<title>QuickAddress Pro Web - Rapid Addressing - Single Line</title>
		<script type="text/javascript">
			// Set the Country combobox to the value indicated by dataID
			function loadCountrySelector(dataID)
			{
				if (dataID != "")
				{
					document.formInput.<%= Constants.DATA_ID %>.value = dataID;
				}
				document.formInput.<%= Constants.DATA_ID %>.focus();
			}
			
			// Validate the form and decide where to go next
			function validate(theForm)
			{
				// Validate country selected
				var iCountrySelection = theForm.<%= Constants.DATA_ID %>.selectedIndex;
				if (!iCountrySelection)
				{
					theForm.<%= Constants.DATA_ID %>.focus();
					alert ("Please select a country.");
					return false;
				}
				
				theForm.<%= Constants.COUNTRY_NAME %>.value = theForm.<%= Constants.DATA_ID %>.options[iCountrySelection].text;
				// Send to manual address page if country unsupported (value is blank)
				if (!theForm.<%= Constants.DATA_ID %>.options[iCountrySelection].value)
				{
					theForm.<%= Constants.COMMAND %>.value = "<%= HierAddress.NAME %>";
					theForm.<%= Constants.ROUTE %>.value = "<%= Constants.ROUTE_UNSUPPORTED_COUNTRY %>";
					return true;
				}
				
				// Validate search term
				var sUserInput = theForm.<%= Constants.USER_INPUT %>.value;
				if (!sUserInput)
				{
					theForm.<%= Constants.USER_INPUT %>.focus();
					alert ('Please enter a search.');
					return false;
				}
				
				// Initiate search
				return true;
			}
		</script>
	</head>

	<body onLoad="loadCountrySelector('<%= sDataId %>');">
		<table width="800" class="nomargin">
			<tr>
				<th class="banner">
					<a href="index.htm"><h1>QuickAddress Pro Web</h1><h2>JSP Samples</h2></a>
				</th>
			</tr>
			<tr>
				<td class="nomargin">
					<h1>Rapid Addressing &#8211; Single Line</h1>
					<h3>Enter Your Search</h3>
					
					<form method="post" name="formInput" action="QasController" onSubmit="return validate(this);">
						Please select the country from the list below, and enter your search terms.<br/><br/>
						
						<table>
							<tr>
								<td><label for="country">Country</label></td>
								<td width="10"></td>
								<td>
									<select name="<%= Constants.DATA_ID %>" id="country">
<%	/* If the VALUE is blank then QuickAddress searching isn't attempted */
	/* DEBUG: adjust 'countries.ok.inc' to suit your set up */
%>										<option value="" selected>Please select&#8230;</option>
										<option value="">Other</option>
										<%@ include file="countries.ok.inc" %>
									</select>
								</td>
							</tr>
							
							<tr>
								<td><label for="SearchText">Search</label></td>
								<td width="10"></td>
								<td>
									<input type="text" name="<%= Constants.USER_INPUT %>" id="SearchText" size="40" value="<%= sUserInputEnc %>"/>&nbsp;
									<input type="submit" value="Search"/>
								</td>
							</tr>
						</table>
						
						<input type="hidden" name="<%= Constants.COUNTRY_NAME %>"/>
						<input type="hidden" name="<%= Constants.COMMAND %>" value="<%= HierSearch.NAME %>"/>
						<input type="hidden" name="<%= Constants.ROUTE %>" value="<%= Constants.ROUTE_NORMAL %>"/>
						
						<br/>The next step is to drill-down to the right address.<br/><br/>
					</form>
					
					<br/><br/><hr/><br/>
					<a href="index.htm">Click here to return to the JSP Samples home page.</a>
				</td>
			</tr>
		</table>
	</body>
</html>
