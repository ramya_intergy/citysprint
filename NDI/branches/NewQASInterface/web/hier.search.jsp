<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page contentType="text/html" %>
<%@ page import="com.qas.proweb.servlet.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%
	/** Intranet, Search page - search, picklist and refinement **/
	
	/** Input attributes **/
	String   sPrompt = (String) request.getAttribute(Constants.PROMPT);
	String   sPartial = (String) request.getAttribute(Constants.PARTIAL);
	String   sUserRefine = (String) request.getAttribute(Constants.REFINE_INPUT);
	String[] asMonikerHistory = (String[]) request.getAttribute(Constants.MONIKER_HISTORY);
	String[] asPartialHistory = (String[]) request.getAttribute(Constants.PARTIAL_HISTORY);
	String[] asPostcodeHistory = (String[]) request.getAttribute(Constants.POSTCODE_HISTORY);
	String[] asScoreHistory = (String[]) request.getAttribute(Constants.SCORE_HISTORY);
	// HTML escapes 
	String   sUserInputEnc = StringEscapeUtils.escapeHtml((String) request.getAttribute(Constants.USER_INPUT));
	
	/** Display warning about last step-in, depending on the Pick_Warning **/
	String sPicklistWarning = (String) request.getAttribute(Constants.STEPIN_WARNING);
        String sType = HttpHelper.passThrough(request,Constants.STYPE);

	String sWarning = "";
	if (Constants.WARN_STEPPEDPASTCLOSE.equals(sPicklistWarning))
	{
		sWarning = "<tr><td colspan=\"5\"><img src=\"img/warning.gif\" class=\"icon\"><b>There are also close matches available - click <a href=\"javascript:goBack();\">here</a> to see them</b><br />&nbsp;</td></tr>";
	}
	else if (Constants.WARN_CROSSBORDER.equals(sPicklistWarning))
	{
		sWarning = "<tr><td colspan=\"5\"><img src=\"img/warning.gif\" class=\"icon\"><b>Address selected is outside of the entered locality</b><br />&nbsp;</td></tr>";
	}
	else if (Constants.WARN_POSTCODERECODE.equals(sPicklistWarning))
	{
		sWarning = "<tr><td colspan=\"5\"><img src=\"img/warning.gif\" class=\"icon\"><b>Postal code has been updated by the Postal Authority</b><br />&nbsp;</td></tr>";
	}
	
	/** Picklist contents **/
	String[] asPickFunctions = (String[]) request.getAttribute(Constants.PICK_FUNCTIONS);
	String[] asPickMonikers = (String[]) request.getAttribute(Constants.PICK_MONIKERS);
	String[] asPickTexts = (String[]) request.getAttribute(Constants.PICK_TEXTS);
	String[] asPickPostcodes = (String[]) request.getAttribute(Constants.PICK_POSTCODES);
	String[] asPickScores = (String[]) request.getAttribute(Constants.PICK_SCORES);
	String[] asPickPartials = (String[]) request.getAttribute(Constants.PICK_PARTIALS);
	String[] asPickIsInfos = (String[]) request.getAttribute(Constants.PICK_ISINFOS);
	String[] asPickIsAlias = (String[]) request.getAttribute(Constants.PICK_ISALIAS);
	String[] asPickWarnings = (String[]) request.getAttribute(Constants.PICK_WARNINGS);




%>

<html>
	<head>
		   <title>CitySprint - Postcode Lookup</title>
                   <link media="screen" href="CSS/style.css" type="text/css" rel="stylesheet">
                   <link media="screen" href="CSS/NextDayCss.css" type="text/css" rel="stylesheet">
		<script type="text/javascript">
			function setFocus()
			{
				document.formPicklist.<%= Constants.COMMAND %>.value = "<%= HierSearch.NAME %>";
				document.formPicklist.<%= Constants.REFINE_INPUT %>.focus();
				document.formPicklist.<%= Constants.REFINE_INPUT %>.select();
				document.formPicklist.<%= Constants.MONIKER %>.value = "<%= request.getAttribute(Constants.MONIKER) %>";
				document.formPicklist.<%= Constants.PARTIAL %>.value = "<%= StringEscapeUtils.escapeHtml(sPartial) %>";
			}
			
			function newSearch()
			{
				document.formPicklist.<%= Constants.COMMAND %>.value = "<%= HierInit.NAME %>";
				document.formPicklist.<%= Constants.USER_INPUT %>.value = "";
				document.formPicklist.submit();
			}
			
			function goBack()
			{
				document.formPicklist.<%= Constants.COMMAND %>.value = "<%= request.getAttribute(Constants.BACK_COMMAND) %>";
				document.formPicklist.<%= Constants.ROUTE %>.value = "<%= Constants.ROUTE_BACK %>";
				document.formPicklist.submit();
			}
			
			function <%= Constants.OP_FORMAT %>(iIndex)
			{
				document.formPicklist.<%= Constants.COMMAND %>.value = "<%= HierAddress.NAME %>";
				document.formPicklist.<%= Constants.MONIKER %>.value = document.formPrivateData.Monikers[iIndex].value;
				document.formPicklist.<%= Constants.STEPIN_WARNING %>.value = document.formPrivateData.Warnings[iIndex].value;
				document.formPicklist.<%= Constants.ROUTE %>.value = "<%= Constants.ROUTE_NORMAL %>";
				document.formPicklist.submit();
			}
			
			function <%= Constants.OP_STEP_IN %>(iIndex)
			{
				document.formPicklist.<%= Constants.COMMAND %>.value = "<%= HierSearch.NAME %>";
				document.formPicklist.<%= Constants.MONIKER %>.value = document.formPrivateData.Monikers[iIndex].value;
				document.formPicklist.<%= Constants.PARTIAL %>.value = document.formPrivateData.Partials[iIndex].value;
				document.formPicklist.<%= Constants.REFINE_INPUT %>.value = "";
				document.formPicklist.<%= Constants.POSTCODE %>.value = document.formPrivateData.Postcodes[iIndex].value;
				document.formPicklist.<%= Constants.SCORE %>.value = document.formPrivateData.Scores[iIndex].value;
				document.formPicklist.<%= Constants.STEPIN_WARNING %>.value = document.formPrivateData.Warnings[iIndex].value;
				document.formPicklist.<%= Constants.ROUTE %>.value = "<%= Constants.ROUTE_NORMAL %>";
				document.formPicklist.submit();
			}
			
			function <%= Constants.OP_HALT_RANGE %>(iIndex)
			{
				document.formPicklist.<%= Constants.REFINE_INPUT %>.focus();
				// display some on-screen warning
				alert ("Please enter a value within the range.");
			}

			function <%= Constants.OP_HALT_INCOMPLETE %>(iIndex)
			{
				document.formPicklist.<%= Constants.REFINE_INPUT %>.focus();
				// display some on-screen warning
				alert ("Please enter the premise details.");
			}
		</script>
	</head>
	
	<body onLoad="setFocus();">
		<table width="800" class="nomargin">
			<tr>
				
			</tr>
			<tr>
				<td class="nomargin" style="padding-left:5px">
					
					<h3>Select from Address Picklist</h3>
					
					<form name="formPicklist" method="post" action="QasController">
						<table>
							<tr>
								Enter text to refine your search, or select one of the addresses below.						
							</tr>
							<tr><br /></tr>
							<tr>
								<td nowrap>&nbsp;<label for="refine"><%= sPrompt %>:</label></td>
								<td><input type="text" name="<%= Constants.REFINE_INPUT %>" id="refine" size="15" 
									value="<%= sUserRefine %>" onKeyDown="if(event.keyCode==13) submit();" />&nbsp;</td>
								<td><input type="submit" value="Search" id="btn"/></td>
							</tr>
						</table>
						
						<table class="pickle">
							<%= sWarning %>
<%	String sIndent = "";
	for (int i=0; i < asPartialHistory.length; i++)
	{
		String sScore = "";
		if (!asScoreHistory[i].equals("0"))
		{
			sScore = asScoreHistory[i] +  "%";
		}
%>							<tr>
								<td class="pre"><%= sIndent %><%= StringEscapeUtils.escapeHtml(asPartialHistory[i]) %></td>
								<td width="20"></td>
								<td nowrap><%= asPostcodeHistory[i] %></td>
								<td width="15"></td>
								<td><%= sScore %></td>
							</tr>
<%		/* Add spacers for every loop except the last */
		if (i < asPartialHistory.length - 1)
		{
			sIndent += "&nbsp;&nbsp;&nbsp;";
		}
	}
%>							<tr>
								<td colspan="5"><hr /></td>
							</tr>
<%	
	for (int i=0; i < asPickMonikers.length; i++)
	{
		/* Set the icon depending on the function */
		String sIcon;
		if (asPickFunctions[i].equals(Constants.OP_STEP_IN))
		{
			if ( asPickIsInfos[i].equals(Boolean.TRUE.toString()) )
			{
				sIcon = "img/iInfoStep.gif";
			}
			else if ( asPickIsAlias[i].equals(Boolean.TRUE.toString()) )
			{
				sIcon = "img/iAliasStep.gif";			
			}
			else
			{
				sIcon = "img/iPicklist.gif";			
			}
		}
		else if ( asPickIsInfos[i].equals(Boolean.TRUE.toString()) )
		{
			sIcon = "img/iInfo.gif";
		}
		else if (asPickFunctions[i].equals(Constants.OP_FORMAT))
		{
		    if ( asPickIsAlias[i].equals(Boolean.TRUE.toString()) )
		    {
		        sIcon = "img/iAlias.gif";
		    }
		    else
		    {
			    sIcon = "img/iFinal.gif";
			}
		}
		else /* OP_HALT_RANGE or OP_HALT_INCOMPLETE */
		{
			sIcon = "img/iNogo.gif";
		}
		
		String sFunction = asPickFunctions[i] + "('" + Integer.toString(i) + "');";
		boolean bLink = !asPickFunctions[i].equals(Constants.OP_NONE);
		
		String sScore = "";
		if (!asPickScores[i].equals("0"))
		{
			sScore = asPickScores[i] + "%";
		} 
%>							<tr>
								<td class="pickle"><%= sIndent %><% if (bLink) { %><a href="javascript:<%= sFunction %>" class="pre"><% } %><img class="icon" src="<%= sIcon %>" 
								alt="<%= StringEscapeUtils.escapeHtml(asPickPartials[i]) %>"><%= StringEscapeUtils.escapeHtml(asPickTexts[i]) %><% if (bLink) { %></a> <% } %></td>
								<td></td>
								<td nowrap><%= asPickPostcodes[i] %></td>
								<td></td>
								<td><%= sScore %></td>
							</tr>
<%	}
%>						
						
</table>

						<input type="hidden" name="<%= Constants.DATA_ID %>" value="<%= request.getAttribute(Constants.DATA_ID) %>" />
						<input type="hidden" name="<%= Constants.COUNTRY_NAME %>" value="<%= request.getAttribute(Constants.COUNTRY_NAME) %>" />
						<input type="hidden" name="<%= Constants.USER_INPUT %>" value="<%= sUserInputEnc %>" />

						<input type="hidden" name="<%= Constants.MONIKER %>" value="<%= request.getAttribute(Constants.MONIKER) %>" />
						<input type="hidden" name="<%= Constants.PARTIAL %>" value="<%= sPartial %>" />
						<input type="hidden" name="<%= Constants.STEPIN_WARNING %>" />
						<input type="hidden" name="<%= Constants.POSTCODE %>" />
						<input type="hidden" name="<%= Constants.SCORE %>" />

						<input type="hidden" name="<%= Constants.COMMAND %>" />
                                                <input type="hidden" name="<%= Constants.STYPE %>" value="<%= sType %>" />
                                                <input type="hidden" name="<%= Constants.SACTION %>" value="<%= request.getAttribute(Constants.SACTION) %>" />
                                                <input type="hidden" name="<%= Constants.SACTION_SUB %>" value="<%= request.getAttribute(Constants.SACTION_SUB) %>" />
						<input type="hidden" name="<%= Constants.ROUTE %>" value="<%= Constants.ROUTE_NORMAL %>" />
<%	/* History of monikers and partial addresses */
	for (int i=0; i < asMonikerHistory.length; i++)
	{
%>						<input type="hidden" name="<%= Constants.MONIKER_HISTORY %>" value="<%= asMonikerHistory[i] %>" />
						<input type="hidden" name="<%= Constants.PARTIAL_HISTORY %>" value="<%= StringEscapeUtils.escapeHtml(asPartialHistory[i]) %>" />
						<input type="hidden" name="<%= Constants.POSTCODE_HISTORY %>" value="<%= asPostcodeHistory[i] %>" />
						<input type="hidden" name="<%= Constants.SCORE_HISTORY %>" value="<%= asScoreHistory[i] %>" />
<%	}
%>						<br />
					</form>

					<form name="formPrivateData">
                                            <input type="hidden" name="<%= Constants.STYPE %>" value="<%= request.getAttribute(Constants.STYPE) %>" />
                                            <input type="hidden" name="<%= Constants.SACTION %>" value="<%= request.getAttribute(Constants.SACTION) %>" />
<%	for (int i=0; i < asPickMonikers.length; i++)
	{
		/* Store details about the picklist items here, accessible to the javascript functions */
%>						<input type="hidden" name="Monikers" value="<%= asPickMonikers[i] %>" />
						<input type="hidden" name="Partials" value="<%= StringEscapeUtils.escapeHtml(asPickTexts[i]) %>" />
						<input type="hidden" name="Warnings" value="<%= asPickWarnings[i] %>" />
						<input type="hidden" name="Postcodes" value="<%= asPickPostcodes[i] %>" />
						<input type="hidden" name="Scores" value="<%= asPickScores[i] %>" />
<%	}
	/* Add a dummy item to allow JavaScript array subscripting to work, when only length 1 */
	if (asPickMonikers.length == 1)
	{
%>						<input type="hidden" name="Monikers" />
						<input type="hidden" name="Partials" />
						<input type="hidden" name="Warnings" />
						<input type="hidden" name="Postcodes" />
						<input type="hidden" name="Scores" />
<%	}
%>					</form>

				</td>
                               
			</tr>
		</table>

                <%

                String sAction=request.getAttribute(Constants.SACTION)==null?"":(String)request.getAttribute(Constants.SACTION);

                if(sAction.equals(Constants.sBooking))
                {
                %>
                <table>
                    <tr>
                         <td><button onClick="Javascript:window.close();" accesskey="N" id="btn">Return to Booking Screen</button></td>
                         
                    </tr>
                </table>
                <%}else{%>
                <center><a href="javascript:history.back(1);">Back</a></center>
                <%}%>

	</body>
</html>
