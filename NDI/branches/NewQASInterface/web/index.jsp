<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%
boolean bFlag=request.getAttribute("bFlag")==null?false:true;
int mlength=(request.getAttribute("message")==null?"":request.getAttribute("message").toString()).length();
int acclength=(request.getSession(false).getAttribute("acc_id")==null?"":(String)request.getSession(false).getAttribute("acc_id")).length();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link media="screen" href="CSS/NextDayCss.css" type="text/css" rel="stylesheet">
        <title>CitySprint - Login</title>
        <script LANGUAGE="JAVASCRIPT">
            function bodyload()
            {
                if((<%=bFlag%>==true || <%=mlength%>>0) || <%=acclength%>==0)
                {
                    
                    document.getElementById("Username").focus();
                }
                else
                {
                    
                    document.FormLogout.submit();
                }
            }
        </script>
    </head>
    <body onload="javascript:bodyload();">

    <br><br>
<table cellpadding="0" cellspacing="0" height="398" width="698" align="center">
<tr><td align="center" background="images/citysprint_login1.gif">
	<br><br>
<form action="EnterConsignment" method="post" name="FormLogin" target="_self">
    <input type=hidden name="sAction" value="Login">
    

	<table cellpadding="5" cellspacing="0" border="0" align="center" class="">
	<tr><td colspan="2" height="200">&nbsp;</td></tr>
	<tr>
		<td id="label">Account Number</td>
		<td><input name="Username" id="Username" type="text" size="20" style="" value='<%=request.getAttribute("Username")==null?"":request.getAttribute("Username").toString()%>' /></td>
	</tr>
	<tr>
		<td id="label">Password</td>
		<td><input name="Password" id="Password" type="password" size="20"/></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" Value="LOGIN" id="btn"/></td>
	</tr>
        <tr>
            <td colspan=2><label id="lblMessage" style="color:red;"><%=request.getAttribute("message")==null?"":request.getAttribute("message").toString()%></label>
        </tr>
        </table>
</form>
</td></tr>
</table>
<form action="Logout" method="post" name="FormLogout" target="_self"></form>
    </body>
</html>