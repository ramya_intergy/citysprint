 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page contentType="text/html" %>
<%@ page import="com.qas.proweb.servlet.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%
	/** QuickAddress Pro Web > (c) QAS Ltd > www.qas.com **/
	/** Web > Verification > verifyInteraction.jsp **/
	/** Interaction page > Display original address along with found address or picklist of addresses **/
	
	/** Get attributes **/
	String sCountryName = (String) request.getAttribute(Constants.COUNTRY_NAME);
	String sDataId = (String) request.getAttribute(Constants.DATA_ID);
	String[] asUserInput = (String[]) request.getAttribute(Constants.USER_INPUT);	
	String[] asMonikers = (String[]) request.getAttribute(Constants.PICK_MONIKERS);	
	String[] asText = null;
	String[] asPostcodes = null;
	String[] asCommands = null;
	String[] asLines = (String[]) request.getAttribute(Constants.LINES);
	String[] asLabels = null;
	
	boolean bShowPicklist = false, bShowAddress = false;
	if (asMonikers != null)
	{
		/* Show a picklist */
		bShowPicklist = true;
		asText = (String[]) request.getAttribute(Constants.PICK_TEXTS);
		asPostcodes = (String[]) request.getAttribute(Constants.PICK_POSTCODES);
		asCommands = (String[]) request.getAttribute(Constants.PICK_FUNCTIONS);
	}
	else if (asLines != null)
	{
		/* Show an address (InteractionRequired case) */
		asLines = (String[]) request.getAttribute(Constants.LINES);
		asLabels = (String[]) request.getAttribute(Constants.LABELS);
		bShowAddress = true;
	}
	
	/* Tab index of controls */
	int iTab = 0;
%>

<html>
	<head>
		<title>City Sprint - Address Verification</title>
		<link rel="stylesheet" href="CSS/NextDayCss.css" type="text/css" />
		<script type="text/javascript">
			/* picklist item hyperlink has been clicked: pick item, command and submit */
			function submitCommand(iIndex)
			{
				document.getElementsByName("<%= Constants.MONIKER %>")[iIndex].checked = true;
				document.formPicklist.<%= Constants.MONIKER %>.selectedIndex = iIndex;
				document.formPicklist.<%= Constants.COMMAND %>.value = g_asCommands[iIndex];
				document.formPicklist.submit();
			}
			
			/* checkPicklist ensures that at least one picklist option button has been selected before submitting the form
			 * If no option buttons have been selected, the user is alerted, and the form is not submitted. */
			function checkPicklist()
			{
				var aItems = document.getElementsByName("<%= Constants.MONIKER %>");
				for (var iIndex=0; iIndex < aItems.length; iIndex++)
				{
					if (aItems[iIndex].checked == true)
					{
						document.formPicklist.<%= Constants.COMMAND %>.value = g_asCommands[iIndex];
						return true;
					}
				}
				alert ("Please select an address first.");
				return false;
			}
		</script>
	</head>
	
	<body onLoad="document.formUserInput.<%= Constants.USER_INPUT %>[0].focus();">
		<table width="800" class="nomargin">
			<tr>
				
			</tr>
			<tr>
				<td class="nomargin">
					<h1>Address Verification</h1>
					<h3>Confirm the Address Entered</h3>
					
					<form name="formUserInput" method="post" action="QasController">
						<table>
<%	for (int i = 0; i < asUserInput.length; i++)
	{
%>							<tr>
								<td width="50"></td>
								<td width="10"></td>
								<td><input type="text" name="<%= Constants.USER_INPUT %>" size="50" tabindex="<%= ++iTab %>" value="<%= StringEscapeUtils.escapeHtml(asUserInput[i]) %>" /></td>
								<input type="hidden" name="<%= Constants.ORIGINAL_INPUT %>" value="<%= StringEscapeUtils.escapeHtml(asUserInput[i]) %>" />
<%		if (i == 0)
		{
			/* Put the submit button next to the first line */
%>						  		<td width="20"></td>
								<td><input type="submit" value="    Accept    " tabindex="<%= iTab + asUserInput.length %>" /></td>
<%		}
%>							</tr>
<%	}
	/* Bump up tab index as we used one for the button */
	iTab++;
%>							<tr>
								<td>Country</td>
								<td width="10"></td>
								<td><input type="text" name="<%= Constants.COUNTRY_NAME %>" size="50" value="<%= sCountryName %>"
									readonly="readonly" style="background: #F0F0F0" tabindex="-1" /></td>
							</tr>
							<tr>
								<td colspan="5">
									<p class="debug"><img src="img/debug.gif" class="debug"/> Integrator information:
										address verification level was <%= request.getAttribute(Constants.VERIFY_INFO) %></p>
								</td>
							</tr>
						</table>
						<input type="hidden" name="<%= Constants.COMMAND %>" value="<%= Verify.NAME %>" />
						<input type="hidden" name="<%= Constants.DATA_ID %>" value="<%= sDataId %>" />
						<input type="hidden" name="<%= Constants.ROUTE %>" value="<%= Constants.ROUTE_ALREADY_VERIFIED %>" />
					</form>
<%
	
	
	/** Picklist case **/
	
	
	if (bShowPicklist)
	{
%>					<h3>Or Select the Address from the Picklist</h3>
					<form name="formPicklist" method="post" action="QasController" onSubmit="return checkPicklist();">
						<table border="0">
<%		/* Bump up tab index as we will use one for all the radio buttons */
		int iRadio = ++iTab;
		for (int i=0; i < asMonikers.length; i++)
		{
%>							<tr>
								<td width="30"></td>
								<td nowrap>
									<input type=radio name="<%= Constants.MONIKER %>" tabindex="<%= iRadio %>" value="<%= asMonikers[i] %>">
									<a tabindex="<%= ++iTab %>" href="javascript:submitCommand(<%= i %>);"><%= asText[i] %></a>
								</td>
								<td width="15"></td>
								<td nowrap><%= asPostcodes[i] %></td>
<%			if (i==0)
			{
				/* Put the submit button next to the first line */
%>						  		<td width="20"></td>
						  		<td><input type="submit" value="    Select    " tabindex="<%= iTab + asMonikers.length %>" /></td>
<%			}
%>							</tr>
<%		}
		/* Bump up tab index as we used one for the button */
		iTab++;
%>						</table>

						<input type="hidden" name="<%= Constants.COMMAND %>" />
						<input type="hidden" name="<%= Constants.COUNTRY_NAME %>" value="<%= sCountryName %>" />
<%		for (int i=0; i < asUserInput.length; i++)
		{
			/* Need to pass along the user input in case the next stage fails */
%>						<input type="hidden" name="<%= Constants.USER_INPUT %>" value="<%= asUserInput[i] %>" />
<%		}
		/* Javascript array holds the command appropriate to each link */
%>						<script type="text/javascript">
var g_asCommands = new Array('<%= HttpHelper.join(asCommands, "','") %>');
						</script>
					</form>
<%	}
	
	
	/** Address case **/
	
	
	else if (bShowAddress)
	{
%>					<h3>Confirm the Address Found</h3>
					<form name="formAddress" method="post" action="<%= Constants.FINAL_ADDRESS_PAGE %>">
						<table border="0">
<%		for (int i = 0; i < asLines.length; i++)
		{
%>							<tr>
								<td><%= asLabels[i] %></td>
								<td width="10"></td>
								<td><input type="text" name=<%= Constants.ADDRESS %> size="50" value="<%= StringEscapeUtils.escapeHtml(asLines[i]) %>" /></td>
<%			if (i == 0)
			{
				/* Put the submit button next to the first line */
%>						  		<td width="20"></td>
						  		<td><input type="submit" value="    Accept    " tabindex="<%= ++iTab %>" /></td>
<%			}
%>							</tr>
<%		}
%>							<tr>
								<td>Country</td>
								<td width=10></td>
								<td><input type=text name=<%= Constants.COUNTRY_NAME %> size=50 value="<%= sCountryName %>" readonly="readonly" style="background: #F0F0F0" tabindex="-1" /></td>
							</tr>
						</table>
					</form>
<%	}
%> 
					<br /><br /><hr /><br />
					
				</td>
			</tr>
		</table>
	</body>
</html>
