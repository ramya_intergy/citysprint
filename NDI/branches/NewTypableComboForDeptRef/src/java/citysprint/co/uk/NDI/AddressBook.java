package citysprint.co.uk.NDI;

/*
 * AddressBook.java - Manipulates Address Book Entries
 *
 * Created on 27 July 2007, 09:37
 */

import citysprint.co.uk.NDI.CountryInfo;
import citysprint.co.uk.NDI.CommonClass;
import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import javax.sql.DataSource;
import com.qas.proweb.servlet.*;
import com.qas.proweb.*;
/**
 *
 * @author ramyas
 * @version
 */
public class AddressBook extends CommonClass {
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    
        
         /**
         *
         * @param request
         * @param response
         * @throws javax.servlet.ServletException
         * @throws java.io.IOException
         * @throws java.sql.SQLException
         */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException,SQLException {
        response.setContentType("text/html;charset=UTF-8");

        if (isLogin(request)) {
            String sAction = request.getParameter(Constants.SACTION) == null ? "" : request.getParameter(Constants.SACTION).toString();
            String sType = request.getParameter(Constants.STYPE) == null ? "" : request.getParameter(Constants.STYPE).toString();
            String sBookingType = "";
            String sql = "";

            if (sAction.equals("AddressBook") || sAction.equals("Search")) {
                sBookingType = request.getParameter("sBookingType") == null ? "" : request.getParameter("sBookingType").toString();
            } else {
                sBookingType = request.getSession(false).getAttribute("sBookingType") == null ? "" : (String) request.getSession(false).getAttribute("sBookingType");
            }

            if (request.getSession(false) == null) {
            } else {
                String acc_id = request.getSession(false).getAttribute("acc_id").toString();

                //initialize config values
                initializeConfigValues(request, response);

                if (sAction.equals("Search")) {
                    Search(request, response, acc_id, sBookingType);
                } else if (sAction.equals("AddressBook")) {
                    request.getSession(false).setAttribute("sBookingType", sBookingType);
                    if (sBookingType.equals("C")) {
                        sql = "SELECT company,ID,postcode,town,county, residential_flag FROM address where collection=1 and owned_by='" + acc_id + "' and environment='" + environment + "' and server='" + host + "' order by company asc";
                    } else if (sBookingType.equals("D")) {
                        sql = "Select company,ID,postcode,town,county, residential_flag from address where delivery=1 and owned_by='" + acc_id + "' and environment='" + environment + "' and server='" + host + "' order by company asc";
                    } else {
                        sql = "Select company,ID,postcode,town,county, residential_flag from address where owned_by='" + acc_id + "' and environment='" + environment + "' and server='" + host + "' order by company asc";
                    }

                    LoadPage(request, response, sql, sBookingType);
                } else if (sAction.equals("ShowAddress")) {
                    String sCompany = request.getParameter("sCompanyName");
                    sql = "Select company,ID,postcode,town,county, residential_flag from address where company like '%" + sCompany + "%' and owned_by='" + acc_id + "' and environment='" + environment + "' and server='" + host + "' order by company asc";
                    LoadPage(request, response, sql, sBookingType);
                } else if (sAction.equals("Edit")) {
                    if (sBookingType.equals("C")) {
                        sql = "SELECT company,ID,postcode,town,county, residential_flag FROM address where collection=1 and owned_by='" + acc_id + "' and environment='" + environment + "' and server='" + host + "' order by company asc";
                    } else if (sBookingType.equals("D")) {
                        sql = "Select company,ID,postcode,town,county, residential_flag from address where delivery=1 and owned_by='" + acc_id + "' and environment='" + environment + "' and server='" + host + "' order by company asc";
                    } else {
                        sql = "Select company,ID,postcode,town,county, residential_flag from address where owned_by='" + acc_id + "' and environment='" + environment + "' and server='" + host + "' order by company asc";
                    }


                    LoadPage(request, response, sql, sBookingType);
                } else if (sAction.equals("QAS")) {
                    sql = "SELECT company,ID,postcode,town,county, residential_flag FROM address where owned_by='" + acc_id + "' and environment='" + environment + "' and server='" + host + "' order by company asc";
                    LoadPage(request, response, sql, sBookingType);
                } else if (sAction.equals("BulkDelete")) {
                    BulkDelete(request, response, acc_id, sBookingType);
                }
            }
        }
        else
            {
                writeOutputLog("Session expired redirecting to index page");
                response.sendRedirect("index.jsp");
            }
    }
    
    /**
     *
     * @param request
     * @param response
     * @param sql
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     */
    protected void LoadPage(HttpServletRequest request, HttpServletResponse response,String sql, String sBookingType)throws ServletException, IOException,SQLException
    {
        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");

        String sId=request.getParameter("sId")==null?"":request.getParameter("sId").toString();
        String sMessage=request.getParameter("sMessage");
        String sWidth="";
        String align="";
         /*QAS constants*/
        String RouteInfo="";
        String[] asMonikers = null;
        String[] asLines = null;
        String[] asLabels = null;
        String[] asText = null;
        String[] asPostcodes = null;
        String[] asCommands = null;
        String[] asClosesBy = null;
        String[] asUserInput = null;
        String[] asAddress = null;
        String VerifyInfo="";
        String sAction = request.getParameter(Constants.SACTION) == null ? "" : request.getParameter(Constants.SACTION).toString();
        String sType = request.getParameter(Constants.STYPE) == null ? "" : request.getParameter(Constants.STYPE).toString();
        String sContact = "", sPhone = "", sCompany = "", sAddress = "", sTown = "", sMobile = "",
               sPostcode = "", sCountry = "", sEmail = "", sAddress2 = "", sAddress3 = "",sCounty = "",
               sInstruct="",sLatitude="",sLongitude="", sClosesByHH="",sClosesByMM="",sClosesBy="",sUCompany="", sResidentialFlag="";

        HttpSession validSession = request.getSession(false);
        
        CountryInfo country=new CountryInfo();
        //Initialise QASAddress Objects
        QASCollectionAddress qCol = new QASCollectionAddress();

        if(validSession.getAttribute("getCountriesArray")!=null)
            country.parseCountries((String[][])validSession.getAttribute("getCountriesArray"));
               
        String sAction_sub="",sAddressline="";
        Boolean bCollection =false,bDelivery=false;
        
        strmResponse.println("<html>");
        strmResponse.println("<head>");
        strmResponse.println("<title>CitySprint - Address Book</title>");
        strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
        strmResponse.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
        strmResponse.println(" <style type=\"text/css\">    ");
        strmResponse.println("    .pg-normal {");
        strmResponse.println("        color: #0059AD;");
        strmResponse.println("        font-weight: normal;");
        strmResponse.println("        text-decoration: none; ");
        strmResponse.println("        cursor: pointer;   ");
        strmResponse.println("    }");
        strmResponse.println("    .pg-selected {");
        strmResponse.println("        color: #0059AD;");
        strmResponse.println("         font-weight: bold;      ");
        strmResponse.println("        text-decoration: underline;");
        strmResponse.println("        cursor: pointer;");
        strmResponse.println("    }");
        strmResponse.println("html { overflow-x: hidden;overflow-y: auto;  }");
        strmResponse.println("</style>");
        strmResponse.println("<script type=\"text/javascript\" src=\"JS/paging.js\"></script>");
        strmResponse.println("</head>");
        
//        if(ff)
//            strmResponse.println("<body width=\"90px\">");
//        else
            strmResponse.println("<body>");

        strmResponse.println("<table><tr><td width=\"100%\">");
       // strmResponse.println("<br>");
        try{

        if(sAction.equals("Edit"))
        {
            
            Connection conDB = null;
            Statement stmRead;
            ResultSet rstRead;
           
            String sSql="Select * from address where ID="+sId+"";

            try
            {
               
                conDB = cOpenConnection();
                stmRead = conDB.createStatement();
                rstRead = stmRead.executeQuery(sSql);
            
                while (rstRead.next())
                {
                    sContact = rstRead.getString("contact");
                    sPhone = rstRead.getString("phone");
                    sCompany = rstRead.getString("company");
                    sAddress = rstRead.getString("address1");
                    sAddress2=rstRead.getString("address2");
                    sAddress3=rstRead.getString("address3");
                    sTown = rstRead.getString("town");
                    sCounty=rstRead.getString("county");
                    sPostcode = rstRead.getString("postcode");
                    sCountry = rstRead.getString("country");
                    bCollection=rstRead.getBoolean("collection");
                    bDelivery=rstRead.getBoolean("delivery");
                    sInstruct=rstRead.getString("instructions");
                    sClosesBy=rstRead.getString("closesat");
                    asClosesBy=sClosesBy.split(":");
                    sLatitude=rstRead.getString("latitude");
                    sLongitude=rstRead.getString("longitude");

                    sResidentialFlag=rstRead.getString("residential_flag");
                    
                    System.out.println("######## AddressBook line# 225: Residential Flag = " + sResidentialFlag);
                    
                    if (sResidentialFlag == null || sResidentialFlag.trim().equals("")){
                        sResidentialFlag = "N";
                    }
                    if(sLatitude==null)
                        sLatitude="";

                    if(sLongitude==null)
                        sLongitude="";

                    if(asClosesBy.length>0)
                    {
                        sClosesByHH=asClosesBy[0];
                        sClosesByMM=asClosesBy[1];
                    }
                }

            }
            catch(Exception ex)
            {
                vShowErrorPage(response,ex);
            }
            finally
            {
                conDB.close();
            }
       }
       else
       if(sAction.equals("QAS"))
       {
            sAction_sub=request.getParameter(Constants.SACTION_SUB)==null?"":request.getParameter(Constants.SACTION_SUB).toString();
            sPhone=request.getParameter("sPhone")==null?"":request.getParameter("sPhone").toString();
            sInstruct=request.getParameter("sInstruct")==null?"":request.getParameter("sInstruct").toString();
            sContact=request.getParameter("sContact")==null?"":request.getParameter("sContact").toString();
            sCountry=request.getParameter("sCountry")==null?"":request.getParameter("sCountry").toString();
            sAddress3=request.getParameter("sAddress3")==null?"":request.getParameter("sAddress3").toString();
            sClosesByHH=request.getParameter("sClosesByHH")==null?"":request.getParameter("sClosesByHH").toString();
            sClosesByMM=request.getParameter("sClosesByMM")==null?"":request.getParameter("sClosesByMM").toString();
            sUCompany=request.getParameter("sUCompany")==null?"":request.getParameter("sUCompany").toString();

            sResidentialFlag =request.getParameter("sResidentialFlag")==null?"":request.getParameter("sResidentialFlag").toString();

System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< FRom AddressBook.java, LINE# 268, sResidentialFlag ==  " + sResidentialFlag);
            if(sAction_sub.equals("ShowAddress"))
            {
                //sAddressline = request.getParameter(Constants.PICK_TEXTS)==null?"":request.getParameter(Constants.PICK_TEXTS).toString();
                String[] sAddresslines=(String[]) request.getAttribute(Constants.ADDRESS);
                asLabels = (String[]) request.getAttribute(Constants.LABELS);
                
                int len=sAddresslines.length;
                
                if(len>0)
                {
                    readArray(sAddresslines,asLabels, qCol, sUCompany);
                  
                }
                
                        
                VerifyInfo=request.getParameter(Constants.VERIFY_INFO)==null?"None":request.getParameter(Constants.VERIFY_INFO).toString();
            }
            else
            {

                if(sAction_sub.equals(HierSearch.NAME))
                {
                    String sUserInput=request.getAttribute(Constants.USER_INPUT)==null?"":(String)request.getAttribute(Constants.USER_INPUT);
                    asLines = (String[]) request.getAttribute(Constants.LINES);
                    asLabels = (String[]) request.getAttribute(Constants.LABELS);
                }
                else
                {
                    asUserInput = (String[]) request.getAttribute(Constants.USER_INPUT);

                    //if result is a single address asLines would be returned
                    asLines = (String[]) request.getAttribute(Constants.LINES);
                    asLabels = (String[]) request.getAttribute(Constants.LABELS);
                    //if result is a picklist then asText,asPostcodes and asMonikers would be returned
                    asText = null;
                    asPostcodes = null;
                    asMonikers = (String[]) request.getAttribute(Constants.PICK_MONIKERS);
                    //if result is verified exactly then asAddress would be returned.
                    asAddress=(String[])request.getAttribute(Constants.ADDRESS);
                }

                VerifyInfo=request.getAttribute(Constants.VERIFY_INFO)==null?"None":(String)request.getAttribute(Constants.VERIFY_INFO);
                RouteInfo=request.getAttribute(Constants.ROUTE)==null?"":(String)request.getAttribute(Constants.ROUTE);
                
                
                String UserAddress="";
                
                if(VerifyInfo.equals(SearchResult.None) && !sAction_sub.equals(HierSearch.NAME))
                {
                   readArray(asUserInput,asLabels, qCol, sUCompany);
                }
                
                if(VerifyInfo.equals(SearchResult.Verified) && !sAction_sub.equals(HierSearch.NAME))
                {
                   readArray(asAddress,asLabels, qCol, sUCompany);
                }
                
                if(VerifyInfo.equals(SearchResult.InteractionRequired) || VerifyInfo.equals(SearchResult.Multiple) || VerifyInfo.equals(SearchResult.PremisesPartial) || VerifyInfo.equals(SearchResult.StreetPartial) || sAction_sub.equals(HierSearch.NAME))
                {
                    if(asLines==null)
                    {
                        asText = (String[]) request.getAttribute(Constants.PICK_TEXTS);
                        asPostcodes = (String[]) request.getAttribute(Constants.PICK_POSTCODES);
 
                    }
                    else
                    {
                       readArray(asLines,asLabels, qCol, sUCompany);
                       
                    }
                }
                                            
            }


            sCompany = qCol.getSCCompany();
            sAddress = qCol.getSCAddress1();
            sAddress2= qCol.getSCAddress2();
            sTown = qCol.getSCTown();
            sCounty= qCol.getSCCounty();
            sPostcode = qCol.getSCPostcode();
            sLongitude = qCol.getSCLongitude();
            sLatitude = qCol.getSCLatitude();

                
                
       }
       else
       {
            sContact = "";
            sPhone = "";
            sCompany = "";
            sAddress = "";
            sAddress2="";
            sAddress3="";
            sTown = "";
            sCounty="";
            sPostcode = "";
            sCountry = "";
            sInstruct="";
            sLongitude="";
            sLatitude="";
             
       }
       
        if(sType.equals("C") || (sBookingType.equals("C") && sAction.equals("AddressBook")))
                bCollection=true;

        if(sType.equals("D") || (sBookingType.equals("D") && sAction.equals("AddressBook")))
             bDelivery=true;

        if(sType.equals("CD"))
        {
             bDelivery=true;
             bCollection=true;
        }

        if(sAction.equals("QAS") && (VerifyInfo.equals(SearchResult.InteractionRequired) || VerifyInfo.equals(SearchResult.Verified)))
        {
            if(sAction_sub.equals("ShowAddress"))
                    sMessage="Your address has been validated. Please click confirm to add the address or Click New Search to start another search or ";
            else
                    sMessage="Your address has been validated. Please click confirm to add the address or Click New Search to start another search.";

        }
        else
            if(sAction.equals("QAS") && (VerifyInfo.equals(SearchResult.Multiple) || VerifyInfo.equals(SearchResult.PremisesPartial) || VerifyInfo.equals(SearchResult.StreetPartial)))
                sMessage="Please select an address from the list or Click New Search to start another search.";
            else
                if(sAction.equals("QAS") && (VerifyInfo.equals(SearchResult.None)))
                    sMessage="Your address can not be validated " + RouteInfo +". Please modify the address fields and try again.";
               
               
        
        if(sMessage==null)
        {
             strmResponse.println("<label id=\"lblMessage\" name=\"lblMessage\" style=\"color:green;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;padding-left:15px\"></label>");
             strmResponse.println("<br>");
        }
        else
        {
            if(sAction_sub.equals("ShowAddress") || sAction_sub.equals(HierSearch.NAME))
                strmResponse.println("<label id=\"lblMessage\" name=\"lblMessage\"  style=\"color:green;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;padding-left:15px\">"+sMessage+"</label><a href=\"javascript:history.back(1);\">Return to Search Results</a><br>");
            else
                strmResponse.println("<label id=\"lblMessage\" name=\"lblMessage\" style=\"color:green;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;padding-left:15px\">"+sMessage+"</label><br>");
            strmResponse.println("<br>");
        }
        
       
        //Address book entry table starts
        if(sAction.equals("Edit") || sAction.equals("QAS") && !VerifyInfo.equals(SearchResult.Multiple) && !VerifyInfo.equals(SearchResult.PremisesPartial) && !VerifyInfo.equals(SearchResult.StreetPartial))
            strmResponse.println("<table id=entryTable style=\"border:1px solid #666666;padding:5px 5px 5px 5px;background-color:#EFEFEF;\" cellpadding=2 width=\"95%\" align=\"center\">");
        else
            strmResponse.println("<table id=entryTable style=\"display:none;border:1px solid #666666;padding:5px 5px 5px 5px;background-color:#EFEFEF;\" cellpadding=2 width=\"95%\" align=\"center\">");
       
        strmResponse.println("<tr><br>");

        if(sAction.equals("Edit"))
            strmResponse.println("<td id=\"boxtitle\" colspan=\"7\">EDIT ADDRESS BOOK ENTRY</td>");
        else
            strmResponse.println("<td id=\"boxtitle\" colspan=\"7\">NEW ADDRESS BOOK ENTRY</td>");

        strmResponse.println("</tr>");
        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Name *</td><td>");
        strmResponse.println("<input type=\"text\" name=txtCompany id=txtCompany maxlength=\"60\" size=\"50\" value=\""+sCompany+"\" style=\"text-transform:uppercase\">");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Address1 *</td><td>");
        strmResponse.println("<input type=\"text\" name=txtAddress1 id=txtAddress1 size=\"50\" maxlength=\"60\" value=\""+sAddress+"\" style=\"text-transform:uppercase\">");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Address2</td><td>");
        strmResponse.println("<input type=\"text\" name=txtAddress2 id=txtAddress2 size=\"50\" maxlength=\"60\" value=\""+sAddress2+"\" style=\"text-transform:uppercase\">");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");
        
        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Address3</td><td>");
        strmResponse.println("<input type=\"text\" name=txtAddress3 id=txtAddress3 size=\"50\" maxlength=\"60\" value=\""+sAddress3+"\" style=\"text-transform:uppercase\">");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");
        
        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Town *</td><td>");
        if(sAction.equals("QAS") && !VerifyInfo.equals(SearchResult.None) || (!sLatitude.equals("") && !sLongitude.equals("")))
            strmResponse.println("<input type=\"text\" name=txtTown id=txtTown size=\"40\" maxlength=\"60\" value=\""+sTown+"\" disabled=\"disabled\" style=\"text-transform:uppercase\">");
        else
            strmResponse.println("<input type=\"text\" name=txtTown id=txtTown size=\"40\" maxlength=\"60\" value=\""+sTown+"\" style=\"text-transform:uppercase\">");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">County </td><td>");
        strmResponse.println("<input type=\"text\" name=txtCounty id=txtCounty size=\"40\" maxlength=\"60\" value=\""+sCounty+"\" style=\"text-transform:uppercase\">");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\"><label id=\"lblPostcode\">Postcode *</label></td><td>");
        if(sAction.equals("QAS") && !VerifyInfo.equals(SearchResult.None) || (!sLatitude.equals("") && !sLongitude.equals("")))
            strmResponse.println("<input type=\"text\" maxlength=\"20\" name=txtPostcode id=txtPostcode size=\"20\" value=\""+sPostcode+"\" disabled=\"disabled\" style=\"text-transform:uppercase\"> &nbsp; " +
                    "<input name=\"btnPostcodeLookup\" id=\"btnPostcodeLookup\" style=\"display:none;font-size:10px\" type=\"button\" class=\"btn\" value=\"UK Postcode Lookup\" onclick=\"javascript:QASPostcodeLookup();\">");
        else
            strmResponse.println("<input type=\"text\" maxlength=\"20\" name=txtPostcode id=txtPostcode size=\"20\" value=\""+sPostcode+"\" style=\"text-transform:uppercase\"> &nbsp; <input name=\"btnPostcodeLookup\" id=\"btnPostcodeLookup\" type=\"button\" style=\"font-size:10px\" class=\"btn\" value=\"UK Postcode Lookup\" onclick=\"javascript:QASPostcodeLookup();\">");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");
        
        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Country</td>");
        strmResponse.println("<td>");
        
       //populate country values
           
        if(country.getCnVector().size()>0)
        {
            if(sAction.equals("QAS") && !VerifyInfo.equals(SearchResult.None) || (!sLatitude.equals("") && !sLongitude.equals("")))
                strmResponse.println("<select id=\"selCountry\" onchange=\"JavaScript:dCountryChange(this.value);\" disabled>");
            else
                strmResponse.println("<select id=\"selCountry\" onchange=\"JavaScript:dCountryChange(this.value);\">");
            
            for(int i=0;i<=country.getCnVector().size()-1;i++)
            {
                if(sCountry.equals(country.getCyVector().elementAt(i)) || (sCountry.equals("") && country.getCyVector().elementAt(i).equals("GBR")))
                    strmResponse.println("<option value="+country.getCyVector().elementAt(i)+" selected>"+country.getCnVector().elementAt(i)+"</option>");
                else
                    strmResponse.println("<option value="+country.getCyVector().elementAt(i)+">"+country.getCnVector().elementAt(i)+"</option>");
            }

            strmResponse.println("</select>&nbsp;&nbsp;");
        }
        else
            strmResponse.println("<input type=\"text\" size=\"40\" name=\"selCountry\" id=\"selCountry\" style=\"text-transform:uppercase\">");
        
        strmResponse.println("</td>");
        strmResponse.println("</tr>");
        
        // Following Residential Address is added by Adnan on 26 Aug 2013 against Residential Flag changes
        strmResponse.println("<tr>");
//        strmResponse.println("<td id=\"label\">Residential Flag</td><td>");
        strmResponse.println("<td id=\"label\">Residential</td><td>");
        
       /*
         strmResponse.println("<select id=\"selResidentialFlag\" >");
         */
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< FRom AddressBook.java, sResidentialFlag ==  " + sResidentialFlag);
          if ("Y".equalsIgnoreCase(sResidentialFlag)){
//                      strmResponse.println("<option value='"+sResidentialFlag+"' selected >"+sResidentialFlag+"</option>");
              strmResponse.println("<input type=\"checkbox\" name=\"chkResidentialFlag\" id=\"chkResidentialFlag\" onclick=\"chkResFlag_Click()\" checked>");
            }
//            else {
//                strmResponse.println("<option value='Y'>Y</option>");
//            }
            
//             if ("N".equalsIgnoreCase(sResidentialFlag)){
          else{
//                      strmResponse.println("<option value='"+sResidentialFlag+"' selected >"+sResidentialFlag+"</option>");
                 strmResponse.println("<input type=\"checkbox\" name=\"chkResidentialFlag\" id=\"chkResidentialFlag\" onclick=\"chkResFlag_Click()\" >");
            }
          /*
            else if (!"Y".equalsIgnoreCase(sResidentialFlag)){
//                strmResponse.println("<option value='N' selected>N</option>");
            }else {
//                strmResponse.println("<option value='N' >N</option>");
            }
         */
        
        
    
                
//         strmResponse.println("<option value='N' selected>N</option>");
//         strmResponse.println("<option value='Y' >Y</option>");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");
        
        
        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Contact</td><td>");
        strmResponse.println("<input type=\"text\" name=txtContact id=txtContact maxlength=\"20\" size=\"50\" value=\""+sContact+"\" style=\"text-transform:uppercase\">");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("<tr>");
        if(bDelivery){
            strmResponse.println("<td id=\"label\"><label id=\"lblPhone\">Phone *</label></td><td>");
            strmResponse.println("<input type=\"text\" name=txtPhone id=txtPhone maxlength=\"20\" size=\"20\" value=\""+sPhone+"\" style=\"text-transform:uppercase\">");
        }
        else{
            strmResponse.println("<td id=\"label\"><label id=\"lblPhone\">Phone</label></td><td>");
            strmResponse.println("<input type=\"text\" name=txtPhone id=txtPhone maxlength=\"20\" size=\"20\" value=\""+sPhone+"\" style=\"text-transform:uppercase\">");
        }
        
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Instruct</td><td>");
        strmResponse.println("<input type=\"text\" name=txtInstruct id=txtInstruct maxlength=\"50\" size=\"50\" value=\""+sInstruct+"\" style=\"text-transform:uppercase\">");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");
        
        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"label\">Closes At</td><td>");
        strmResponse.println("<select id=\"selClosesByHH\" >");
            
        String sI = "";
        for(int i=0;i<24;i++)
        {
            if (i<10)
                sI="0"+i;
            else
                sI=""+i;

            if(sClosesByHH.equals(sI))
                strmResponse.print("<option value=\""+sI+"\" selected>"+sI+"</option>");
            else
                strmResponse.print("<option value=\""+sI+"\">"+sI+"</option>");
        }


        strmResponse.println("</select>:<select id=\"selClosesByMM\">");
        
        if(sClosesByMM.equals("00"))
            strmResponse.println("<option value=\"00\" selected>00</option>");
        else
            strmResponse.println("<option value=\"00\">00</option>");
        
        if(sClosesByMM.equals("15"))
            strmResponse.println("<option value=\"15\" selected>15</option>");
        else
            strmResponse.println("<option value=\"15\">15</option>");

        if(sClosesByMM.equals("30"))
            strmResponse.println("<option value=\"30\" selected>30</option>");
        else
            strmResponse.println("<option value=\"30\">30</option>");

        if(sClosesByMM.equals("45"))
            strmResponse.println("<option value=\"45\" selected>45</option>");
        else
            strmResponse.println("<option value=\"45\">45</option>");

        strmResponse.println("</select>");
        
        //strmResponse.println("<select id=\"selClosesByHH\"><option value=\"16\">16</option><option value=\"18\">18</option><option value=\"20\">20</option></select>&nbsp;");
        //strmResponse.println("<select id=\"selClosesByMM\"><option value=\"15\">15</option><option value=\"30\">30</option><option value=\"45\">45</option></select>&nbsp;");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("<tr>");
        strmResponse.println("<td>");
        strmResponse.println("</td>");
        strmResponse.println("<td>");

        if(bCollection)
                strmResponse.println("<input type=\"checkbox\" name=\"chkCollection\" id=\"chkCollection\" checked>Collection &nbsp;&nbsp;");
        
        else
                strmResponse.println("<input type=\"checkbox\" name=\"chkCollection\" id=\"chkCollection\">Collection &nbsp;&nbsp;");
      

        if(bDelivery)
                strmResponse.println("<input type=\"checkbox\" name=\"chkDelivery\" id=\"chkDelivery\" onclick=\"chkDelivery_Click()\" checked>Delivery &nbsp;&nbsp;");
 
        else
    
                strmResponse.println("<input type=\"checkbox\" name=\"chkDelivery\" id=\"chkDelivery\" onclick=\"chkDelivery_Click()\">Delivery &nbsp;&nbsp;");
    
        strmResponse.println("</td>");
        strmResponse.println("</tr>");
        strmResponse.println("<tr>");
        strmResponse.println("<td>");
        strmResponse.println("<input type=\"hidden\" name=\"hdLatitude\" id=\"hdLatitude\" value='"+sLatitude+"'>");
        strmResponse.println("<input type=\"hidden\" name=\"hdLongitude\" id=\"hdLongitude\" value='"+sLongitude+"'>");
        strmResponse.println("</td>");
        strmResponse.println("<td>");
        
        if(sAction.equals("QAS") && (VerifyInfo.equals(SearchResult.InteractionRequired) || VerifyInfo.equals(SearchResult.Verified)))
        {
            strmResponse.println("<input type=\"button\" name=btnEntry id=btnEntry value=\"Confirm\" class=\"btn\" onclick=\"javascript:addAddress();\">");
            strmResponse.println("&nbsp;&nbsp;&nbsp;&nbsp;<br><br>");
            strmResponse.println("<input type=\"button\" name=\"btnCancel\" id=\"btnCancel\" value=\"New Search\" class=\"btn\" onclick=\"javascript:cancel_click('New');\">");
        }
        else
        {
            if(!sLatitude.equals("") && !sLongitude.equals(""))
                strmResponse.println("<input type=\"button\" name=btnEntry id=btnEntry value=\"Save Address\" class=\"btn\" onclick=\"javascript:addAddress();\">");
            else
                strmResponse.println("<input type=\"button\" name=btnEntry id=btnEntry value=\"Validate|Save Address\" class=\"btn\" onclick=\"javascript:addAddress();\">");
            
            if(!sLatitude.equals("") && !sLongitude.equals(""))
            {
                strmResponse.println("&nbsp;");
                strmResponse.println("<input type=\"button\" name=\"btnEdit\" id=\"btnEdit\" value=\"Edit Address\" class=\"btn\" onclick=\"javascript:editAddress();\">");
                strmResponse.println("<br><br>");
            }

            if(sLatitude.equals("") && sLongitude.equals(""))
                strmResponse.println("&nbsp;");
            strmResponse.println("<input type=\"button\" name=\"btnClear\" id=\"btnClear\" value=\"Clear Address\" class=\"btn\" onclick=\"javascript:clearAddress();\">");
            strmResponse.println("&nbsp;");
            strmResponse.println("<input type=\"button\" name=\"btnCancel\" id=\"btnCancel\" value=\"Cancel\" class=\"btn\" onclick=\"javascript:cancel_click('Cancel');\">");
            
        }
        }
        catch(Exception ex){
            vShowErrorPage(response, ex);
        }
        
        
        strmResponse.println("</td>");
        strmResponse.println("</tr>");
        strmResponse.println("</table>");
        //end of Address book entry table.
        strmResponse.println("</td></tr><tr><td>");
       
        
        if(sAction.equals("QAS") && (VerifyInfo.equals(SearchResult.Multiple) || VerifyInfo.equals(SearchResult.PremisesPartial) || VerifyInfo.equals(SearchResult.StreetPartial)) && (!sAction_sub.equals("ShowAddress")))
        {
            //if picklist is available for search results
            int size;
            
            if(asText.length>150)
                size=150;
            else
                size=asText.length;
             
             if(size>15)
                 strmResponse.println("<div id=\"pageNavPosition\" style=\"padding-left:20px\"></div><br>");
             else
                 strmResponse.println("<div id=\"pageNavPosition\" style=\"padding-left:20px;display:none;\"></div><br>");

             strmResponse.println("<table id=\"picklistTable\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"box\" width=\"95%\" align=\"center\">");
             strmResponse.println("<tr id=\"trHeading\" name=\"trHeading\"><td id=\"boxtitle\">Address</td><td id=\"boxtitle\">Postcode</td></tr>");
             
            for(int i=0;i<size;i++)
            {
               
                   try
                    {

                            request.setAttribute("Variation","Multiple");
                            request.setAttribute(Constants.COMMAND, VerifyFormatAddress.NAME);
                            request.setAttribute(Constants.MONIKER, asMonikers[i]);
                            // The helper extract the Command name from the request and creates the relevant object.
                            Command cmd = HttpHelper.getCommand(request);

                            // perform custom operation
                            String sPage = cmd.execute(request, response);

                            String[] asAddress1=(String[]) request.getAttribute(Constants.ADDRESS);
                            String[] asLabels1=(String[])request.getAttribute(Constants.LABELS);
                            //readArray(asAddress1,asLabels1, qCol, sUCompany);


                            String sAddress1=getDisplayAddress(asAddress1).toUpperCase();

                            if(sAddress1.length()>0)
                            {
                                strmResponse.println("<tr><td><a href='javascript:QASlist_Click(\""+asMonikers[i]+"\");'>"+sAddress1+"</a>&nbsp;</td><td >");
                                strmResponse.println(asPostcodes[i]);
                                strmResponse.println("</td></tr>");
                            }
                            else
                            {
                                if(asText.length==1)
                                {
                                    strmResponse.println("<tr><td colspan=\"2\" style=\"color:green;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;\">No Matches Found.  Please try entering building number without the street name, and the postcode");
                                    strmResponse.println("</td></tr>");

                                    strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">document.getElementById(\"pageNavPosition\").style.display='none';");
                                    strmResponse.println("if(document.all){");
                                    strmResponse.println("lblMessage.innerText = \"\";");
                                    strmResponse.println("}else{");
                                    strmResponse.println("lblMessage.textContent = \"\";}");
                                    strmResponse.println("document.getElementById(\"trheading\").style.display='none';</script>");
                                }
                            }



                    }
                    catch (Exception e)
                    {
//                            System.err.println("~~~~~~~~~ Controller:command exception ~~~~~~~~~~");
//                            e.printStackTrace();
//                            System.err.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//                            throw new ServletException(e);
                        vShowErrorPage(response, e);
                    }


            }
             
             strmResponse.println("<tr><td><input type=\"button\" name=\"btnCancel\" id=\"btnCancel\" value=\"New Search\" class=\"btn\" onclick=\"javascript:cancel_click('New');\"></td></tr>");
             strmResponse.println("</table>");
            
        }
        
        
            strmResponse.println("</td></tr><tr><td>");
            strmResponse.println("<br>");
            
            if(!sAction.equals("QAS") && !sAction.equals("Edit"))
                strmResponse.println("<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"tblSearch\" width=\"95%\" align=\"center\" name=\"tblSearch\" class=\"box\">");
            else
                strmResponse.println("<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"tblSearch\" width=\"95%\" align=\"center\" name=\"tblSearch\" class=\"box\">");
            
            strmResponse.println("<tr><td id=\"boxtitle\" colspan=\"8\">SEARCH ADDRESS BOOK</td></tr>");
            strmResponse.println("<tr>");
            strmResponse.println("<td style=\"text-align:left;\">Company</td>");
            strmResponse.println("<td><input type=\"text\" size=\"30\" name=\"txtSrchCompany\" id=\"txtSrchCompany\"></td><td><input type=\"button\" id=\"btn\" value=\"Search\" onclick=\"javascript:Search();\"></td><td><input type=\"button\" id=\"btn\" value=\"Add\" onclick=\"javascript:showEntryForm();\"></td>");
            strmResponse.println("</tr>");
            strmResponse.println("</table>");
            
            strmResponse.println("</td></tr><tr><td>");
            
            //Dispaly the existing address for the current logged on account.
            Connection conDB = null;
            Statement stmRead;
            ResultSet rstRead;
            String sCompanyName="";
            String sPcode="";
           
            String sClass="odd";
            int Id;

            try
            {
                 
                conDB = cOpenConnection();
                stmRead = conDB.createStatement();
                rstRead = stmRead.executeQuery(sql);
                if(rstRead.isBeforeFirst())
                {
                    if(!sAction.equals("QAS") && !sAction.equals("Edit"))
                        strmResponse.println("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"tblResults\" width=\"95%\" align=\"center\" style=\"border:1px solid #666666;padding:5px 5px 5px 5px;background-color:#EFEFEF;\">");
                    else
                        strmResponse.println("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"tblResults\" width=\"95%\" align=\"center\" style=\"display:none;border:1px solid #666666;padding:5px 5px 5px 5px;background-color:#EFEFEF;\">");
                    
                    strmResponse.println("<tr><td id=\"boxtitle\"></td><td id=\"boxtitle\"></td><td id=\"boxtitle\"></td><td id=\"boxtitle\"></td><td id=\"boxtitle\">Company</td><td id=\"boxtitle\">Town</td><td id=\"boxtitle\">County</td><td id=\"boxtitle\">Postcode</td></tr>");
                
                
                    while (rstRead.next())
                    {

                        sCompanyName = rstRead.getString("company").toUpperCase();
                        sPcode=rstRead.getString("postcode").toUpperCase();
                        sTown=rstRead.getString("town").toUpperCase();
                        sCounty=rstRead.getString("county").toUpperCase();

                        Id=rstRead.getInt("ID");
                        strmResponse.println("<tr id=\"odd\">");
    //                    strmResponse.println("<td width=\"3%\"><input type=\"checkbox\" name=\"chkSelect\" value=\"\"></td><td width=\"5%\"><a href=\"AddressBook?"+Constants.SACTION+"=Edit&sType="+sType+"&sId="+Id+"\"><img src=\"images/iedit.jpg\"></a></td><td width=\"6%\">");
    //                    strmResponse.println("<a href=\"AddAddress?"+Constants.SACTION+"=Delete&sType="+sType+"&sId="+Id+"\">Delete</a></td><td width=\"6%\">");
    //                    strmResponse.println("<a href=\"AddAddress?"+Constants.SACTION+"=display&sType="+sType+"&sId="+Id+"\">Select</a></td><td width=\"40%\">");
    //                    strmResponse.println("<a href=\"AddAddress?"+Constants.SACTION+"=display&sType="+sType+"&sId="+Id+"\">"+sCompanyName+"</a></td><td width=\"10%\">");

                        strmResponse.println("<td width=\"3%\"><input type=\"checkbox\" name=\"chkSelect\" id=\"chkSelect\" value="+Id+"></td>" +
                                "<td width=\"3%\"><a href=\"AddressBook?"+Constants.SACTION+"=Edit&sBookingType="+sBookingType+"&sType="+sType+"&sId="+Id+"\"><img src=\"images/iedit_t.gif\" border=\"0\"></a></td><td width=\"3%\">");
                        strmResponse.println("<a href=\"AddAddress?"+Constants.SACTION+"=Delete&sBookingType="+sBookingType+"&sType="+sType+"&sId="+Id+"\"><img src=\"images/idelete_t.gif\" border=\"0\"></a></td><td width=\"3%\">");
                        strmResponse.println("<a href=\"AddAddress?"+Constants.SACTION+"=display&sBookingType="+sBookingType+"&sType="+sType+"&sId="+Id+"\"><img src=\"images/sel_t.gif\" border=\"0\"></a></td><td width=\"60%\">");
                        strmResponse.println("<a href=\"AddAddress?"+Constants.SACTION+"=display&sBookingType="+sBookingType+"&sType="+sType+"&sId="+Id+"\">"+sCompanyName+"</a></td><td width=\"10%\">");
                        strmResponse.println(sTown+"</td><td width=\"10%\">");
                        strmResponse.println(sCounty+"</td><td width=\"10%\">");
                        strmResponse.println(sPcode);
                        strmResponse.println("</td></tr>");

                        if(sClass=="odd")
                        {
                            sClass="even";
                        }
                        else if(sClass=="even")
                        {
                            sClass="odd";
                        }
                    }
                    
                    strmResponse.println("</table>");
                    strmResponse.println("</td></tr><tr><td>");
                    if(!sAction.equals("QAS") && !sAction.equals("Edit"))
                        strmResponse.println("<table width=\"95%\" align=\"center\" id=\"tblButtons\">");
                    else
                        strmResponse.println("<table width=\"95%\" align=\"center\" id=\"tblButtons\" style=\"display:none\">");
                    
                    strmResponse.println("<tr><td width=\"10%\"><br><input type=\"button\" id=\"btn\" value=\"Delete\" onclick=\"javascript:bDelete();\"></td>");
                    strmResponse.println("<td width=\"10%\"><br><input type=\"button\" id=\"btn\" value=\"Import\" onclick=\"javascript:window.location='ImportStepOne1.html'\"></td>");
                    strmResponse.println("<td width=\"10%\"><br><input type=\"button\" id=\"btn\" value=\"Close\" onclick=\"javascript:window.close();\"></td>");
                    strmResponse.println("<td width=\"65%\"></td></tr></table>");
                }
                else
                {
                    strmResponse.println("</td></tr><tr><td>");
                    strmResponse.println("<table width=\"95%\" align=\"center\" id=\"tblButtons\">");
                    strmResponse.println("<tr><td width=\"10%\" style=\"display:none\"><br><input type=\"button\" id=\"btn\" value=\"Delete\" onclick=\"javascript:bDelete();\"></td>");
                    strmResponse.println("<td width=\"10%\"><br><input type=\"button\" id=\"btn\" value=\"Import\" onclick=\"javascript:window.location='ImportStepOne1.html'\"></td>");
                    strmResponse.println("<td width=\"10%\"><br><input type=\"button\" id=\"btn\" value=\"Close\" onclick=\"javascript:window.close();\"></td>");
                    strmResponse.println("<td width=\"65%\"></td></tr></table>");
                }
                
                  rstRead.close();
            }
            catch (Exception errOther)
            {
              strmResponse.println("<FONT SIZE=\"-0\" face=\"Verdana,Tahoma,Arial,Helvetica,sans-serif\" COLOR=\"\" + sFontColor+ \">"+errOther.toString()+"</font>");
            }
            finally
            { 
              conDB.close();
            }

        strmResponse.println("</td></tr></table>");
        strmResponse.println("<script type=\"text/javascript\">");
        strmResponse.println("var pager = new Pager('picklistTable', 15);");
        strmResponse.println("pager.init(); ");
        strmResponse.println("pager.showPageNav('pager', 'pageNavPosition'); ");
        strmResponse.println("pager.showPage(1);");
        strmResponse.println("</script>");

      //  strmResponse.println("<br><br>");
      //  strmResponse.println("<center><a href=\"javascript:window.close();\">Close</a></center>");
        strmResponse.println("</body>");
        strmResponse.println("<script language=javascript>");

        strmResponse.println("function chkDelivery_Click()");
        strmResponse.println("{");
        strmResponse.println("var chkDelivery=document.getElementById(\"chkDelivery\");");
        strmResponse.println("if(chkDelivery.checked){");
        strmResponse.println("document.getElementById(\"lblPhone\").innerHTML = \"Phone *\";");
//        strmResponse.println("  document.getElementById(\"lblPhone\").style.color='#CC0000';");
//        strmResponse.println("  document.getElementById(\"txtPhone\").style.border='3px solid #7F9DB9';");
        strmResponse.println("}else{");
        strmResponse.println("document.getElementById(\"lblPhone\").innerHTML = \"Phone\";");
//        strmResponse.println("  document.getElementById(\"lblPhone\").style.color='#666666';");
//        strmResponse.println("  document.getElementById(\"txtPhone\").style.border='1px solid #7F9DB9';");
        strmResponse.println("}");
        strmResponse.println("}");
        
        strmResponse.println("function chkResFlag_Click()");
        strmResponse.println("{");
        strmResponse.println("var chkResidential=document.getElementById(\"chkResidentialFlag\");");
        strmResponse.println("if(chkResidential.checked){");
        strmResponse.println("sResidentialFlag.value = \"Y\";");
//        strmResponse.println("  document.getElementById(\"lblPhone\").style.color='#CC0000';");
//        strmResponse.println("  document.getElementById(\"txtPhone\").style.border='3px solid #7F9DB9';");
        strmResponse.println("}else{");
        strmResponse.println("sResidentialFlag"
                + ".value = \"N\";");
//        strmResponse.println("  document.getElementById(\"lblPhone\").style.color='#666666';");
//        strmResponse.println("  document.getElementById(\"txtPhone\").style.border='1px solid #7F9DB9';");
        strmResponse.println("}");
        strmResponse.println("}");
        
        //Displays the New entry Form
        strmResponse.println("function showEntryForm()");
        strmResponse.println("{");
        strmResponse.println("var table = document.getElementById('entryTable');");
        strmResponse.println("var tblSearch=document.getElementById('tblSearch');");
        strmResponse.println("var tblButtons=document.getElementById('tblButtons');");
        strmResponse.println("var tblResults=document.getElementById('tblResults');");
        //strmResponse.println("alert(tblSearch);");
        strmResponse.println("if(table.style.display == 'none')");
        strmResponse.println("{");
        strmResponse.println("table.style.display = '';");
        strmResponse.println("tblSearch.style.display = 'none';");
        strmResponse.println("tblButtons.style.display = 'none'");
        strmResponse.println("tblResults.style.display = 'none'");
        strmResponse.println("var txtCompany=document.getElementById(\"txtCompany\");");
        strmResponse.println("var txtContact=document.getElementById(\"txtContact\");"); 
        strmResponse.println("var txtAddress1=document.getElementById(\"txtAddress1\");");
        strmResponse.println("var txtAddress2=document.getElementById(\"txtAddress2\");");
        strmResponse.println("var txtAddress3=document.getElementById(\"txtAddress3\");");
        strmResponse.println("var txtPhone=document.getElementById(\"txtPhone\");");
        strmResponse.println("var txtTown=document.getElementById(\"txtTown\");");
        strmResponse.println("var txtCounty=document.getElementById(\"txtCounty\");");
        strmResponse.println("var txtPostcode=document.getElementById(\"txtPostcode\");");
        strmResponse.println("var chkCollection=document.getElementById(\"chkCollection\");");
        strmResponse.println("var chkDelivery=document.getElementById(\"chkDelivery\");");
        strmResponse.println("var txtInstruct=document.getElementById(\"txtInstruct\");");
        strmResponse.println("var btnEntry=document.getElementById(\"btnEntry\");");
        strmResponse.println("var picklistTable=document.getElementById(\"picklistTable\");");
        
        
        strmResponse.println("txtCompany.disabled=false;");
        strmResponse.println("txtCompany.value=\"\";");
        strmResponse.println("txtContact.disabled=false;");
        strmResponse.println("txtContact.value=\"\";");
        strmResponse.println("txtAddress1.disabled=false;");
        strmResponse.println("txtAddress1.value=\"\";");
        strmResponse.println("txtAddress2.disabled=false;");
        strmResponse.println("txtAddress2.value=\"\";");
        strmResponse.println("txtAddress3.disabled=false;");
        strmResponse.println("txtAddress3.value=\"\";");
        strmResponse.println("txtPhone.disabled=false;");
        strmResponse.println("txtPhone.value=\"\";");
        strmResponse.println("txtTown.disabled=false;");
        strmResponse.println("txtTown.value=\"\";");
        strmResponse.println("txtCounty.disabled=false;");
        strmResponse.println("txtCounty.value=\"\";");
        strmResponse.println("txtPostcode.disabled=false;");
        strmResponse.println("txtPostcode.value=\"\";");
        strmResponse.println("txtInstruct.disabled=false;");
        strmResponse.println("txtInstruct.value=\"\";");
        strmResponse.println("chkCollection.disabled=false;");
        strmResponse.println("chkDelivery.disabled=false;");
        strmResponse.println("btnEntry.value=\"Validate|Save Address\";");
        strmResponse.println("btnCancel.value=\"Cancel\";");
        strmResponse.println("btnPostcodeLookup.style.display='';");
        strmResponse.println("picklistTable.style.display='none';");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("table.style.display = 'none';");
        strmResponse.println("}");
        strmResponse.println("}");
        
        //Cancel click hide entry table
        strmResponse.println("function cancel_click(sType)");
        strmResponse.println("{");
        //strmResponse.println("alert('inside fn');");
        strmResponse.println("var lblMessage=document.getElementById(\"lblMessage\");");
        strmResponse.println("var btnCancel=document.getElementById(\"btnCancel\");");
        strmResponse.println("var tblSearch=document.getElementById('tblSearch');");
        strmResponse.println("var tblResults=document.getElementById('tblResults');");
        strmResponse.println("var btnPostcodeLookup=document.getElementById('btnPostcodeLookup');");
        strmResponse.println("var tblButtons=document.getElementById('tblButtons');");
        //strmResponse.println("lblMessage.innerText=\"\";");
        //strmResponse.println("alert(sType);");
        strmResponse.println("if(sType==\"Cancel\")");
        strmResponse.println("{");
        strmResponse.println("var table = document.getElementById('entryTable');");
        //strmResponse.println("alert(tblResults);");
        strmResponse.println("table.style.display = 'none';");
        strmResponse.println("tblSearch.style.display = '';");
        strmResponse.println("if(tblResults==null){}");
        strmResponse.println("else");
        strmResponse.println("tblResults.style.display = '';");
        //strmResponse.println("alert('test');");
        //strmResponse.println("alert(tblButtons);");
        //strmResponse.println("alert(tblButtons.style.display);");
        strmResponse.println("tblButtons.style.display = '';");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("var table = document.getElementById('entryTable');");
        strmResponse.println("table.style.display = '';");
        strmResponse.println("var txtCompany=document.getElementById(\"txtCompany\");");
        strmResponse.println("var txtContact=document.getElementById(\"txtContact\");"); 
        strmResponse.println("var txtAddress1=document.getElementById(\"txtAddress1\");");
        strmResponse.println("var txtAddress2=document.getElementById(\"txtAddress2\");");
        strmResponse.println("var txtAddress3=document.getElementById(\"txtAddress3\");");
        strmResponse.println("var txtPhone=document.getElementById(\"txtPhone\");");
        strmResponse.println("var txtTown=document.getElementById(\"txtTown\");");
        strmResponse.println("var txtCounty=document.getElementById(\"txtCounty\");");
        strmResponse.println("var txtPostcode=document.getElementById(\"txtPostcode\");");
        strmResponse.println("var chkCollection=document.getElementById(\"chkCollection\");");
        strmResponse.println("var chkDelivery=document.getElementById(\"chkDelivery\");");
        strmResponse.println("var txtInstruct=document.getElementById(\"txtInstruct\");");
        strmResponse.println("var btnEntry=document.getElementById(\"btnEntry\");");
        strmResponse.println("var picklistTable=document.getElementById(\"picklistTable\");");
        strmResponse.println("var selCountry=document.getElementById(\"selCountry\");");

        strmResponse.println("txtCompany.disabled=false;");
        strmResponse.println("txtCompany.value=\"\";");
        strmResponse.println("txtContact.disabled=false;");
        strmResponse.println("txtContact.value=\"\";");
        strmResponse.println("txtAddress1.disabled=false;");
        strmResponse.println("txtAddress1.value=\"\";");
        strmResponse.println("txtAddress2.disabled=false;");
        strmResponse.println("txtAddress2.value=\"\";");
        strmResponse.println("txtAddress3.disabled=false;");
        strmResponse.println("txtAddress3.value=\"\";");
        strmResponse.println("txtPhone.disabled=false;");
        strmResponse.println("txtPhone.value=\"\";");
        strmResponse.println("txtTown.disabled=false;");
        strmResponse.println("txtTown.value=\"\";");
        strmResponse.println("txtCounty.disabled=false;");
        strmResponse.println("txtCounty.value=\"\";");
        strmResponse.println("txtPostcode.disabled=false;");
        strmResponse.println("txtPostcode.value=\"\";");
        strmResponse.println("txtInstruct.disabled=false;");
        strmResponse.println("txtInstruct.value=\"\";");
        strmResponse.println("chkCollection.disabled=false;");
        strmResponse.println("chkDelivery.disabled=false;");
        strmResponse.println("btnEntry.value=\"Validate|Save Address\";");
        strmResponse.println("btnCancel.value=\"Cancel\";");
        strmResponse.println("btnPostcodeLookup.style.display='';");
        strmResponse.println("lblMessage.innerText=\"\";");
        strmResponse.println("lblMessage.textContent=\"\";");
        strmResponse.println("if(picklistTable==null){}else{");
        strmResponse.println("picklistTable.style.display='none';");
        strmResponse.println("selCountry.value=\""+Constants.COUNTRY_CODE+"\";");
        strmResponse.println("selCounty.disabled=false;");
        strmResponse.println("document.getElementById(\"pageNavPosition\").style.display='none';");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("}");
        
        //Posts the add entry to AddAddress servlet
        strmResponse.println("function addAddress()"); 
        strmResponse.println("{");
        strmResponse.println("var sMsg1=\"\";");
        strmResponse.println("var sMsg2=\"\";");
        strmResponse.println("var error=false;");
       
        strmResponse.println("var txtCompany=document.getElementById(\"txtCompany\");");
        strmResponse.println("var txtContact=document.getElementById(\"txtContact\");");
        strmResponse.println("var txtAddress1=document.getElementById(\"txtAddress1\");");
        strmResponse.println("var txtAddress2=document.getElementById(\"txtAddress2\");");
        strmResponse.println("var txtAddress3=document.getElementById(\"txtAddress3\");");
        strmResponse.println("var txtPhone=document.getElementById(\"txtPhone\");");
        strmResponse.println("var txtTown=document.getElementById(\"txtTown\");");
        strmResponse.println("var txtCounty=document.getElementById(\"txtCounty\");");
        strmResponse.println("var txtPostcode=document.getElementById(\"txtPostcode\");");
        strmResponse.println("var selCountry=document.getElementById(\"selCountry\");");
        strmResponse.println("var chkCollection=document.getElementById(\"chkCollection\");");
        strmResponse.println("var chkDelivery=document.getElementById(\"chkDelivery\");");
        strmResponse.println("var txtInstruct=document.getElementById(\"txtInstruct\");");
        strmResponse.println("var selClosesByHH=document.getElementById(\"selClosesByHH\");");
        strmResponse.println("var selClosesByMM=document.getElementById(\"selClosesByMM\");");
        strmResponse.println("var btnEntry=document.getElementById(\"btnEntry\");");
        strmResponse.println("var hdLatitude=document.getElementById(\"hdLatitude\");");
        strmResponse.println("var hdLongitude=document.getElementById(\"hdLongitude\");");
        strmResponse.println("var sCompany=txtCompany.value;");
        strmResponse.println("var sContact=txtContact.value;");
        strmResponse.println("var sAddress1=txtAddress1.value;");
        strmResponse.println("var sAddress2=txtAddress2.value;");
        strmResponse.println("var sAddress3=txtAddress3.value;");
        strmResponse.println("var sPhone=txtPhone.value;");
        strmResponse.println("var sTown=txtTown.value;");
        strmResponse.println("var sCounty=txtCounty.value");
        
        // added by Adnan on 26 Aug 2013 against Residential Flag change
        strmResponse.println("var selResidentialFlag=document.getElementById(\"chkResidentialFlag\");");
         strmResponse.println("var sResidentialFlag=chkResidentialFlag.checked");
         
         strmResponse.println("if (sResidentialFlag == true){ sResidentialFlag = \"Y\"; }else { sResidentialFlag = \"N\";} ");
//        strmResponse.println("alert(\"sResidentialFlag value=\" + sResidentialFlag);");
        strmResponse.println("var sPostcode=txtPostcode.value;");
        strmResponse.println("var sCountry=selCountry.value;");
        strmResponse.println("var sInstruct=txtInstruct.value;");
        //strmResponse.println("alert(\"after value\");");

        strmResponse.println("if(sCountry==\"GBR\" && (sCompany==\"\" || sAddress1==\"\" || sPostcode==\"\" || sTown==\"\"))");
        strmResponse.println("{");
        strmResponse.println("sMsg2=\"\\nPlease enter data in the mandatory fields marked(*).\";");
        strmResponse.println("error=true;");
        strmResponse.println("}");
        strmResponse.println("else if(sCompany==\"\" || sAddress1==\"\" || sTown==\"\")");
        strmResponse.println("{");
        strmResponse.println("sMsg2=\"\\nPlease enter data in the mandatory fields marked(*).\";");
        strmResponse.println("error=true;");
        strmResponse.println("}");
        //strmResponse.println("alert(\"before if\");");
        strmResponse.println("if(sPhone.indexOf(\"'\")!=-1 || sPostcode.indexOf(\"'\")!=-1)");
        strmResponse.println("{");
        strmResponse.println("sMsg1=sMsg1+\"\\nInvalid characters found in Phone and Postcode fields. Please correct.\";");
        strmResponse.println("error=true;");
        strmResponse.println("}");
        strmResponse.println("if(!chkCollection.checked && !chkDelivery.checked)");
        strmResponse.println("{");
        strmResponse.println("sMsg1=sMsg1+\"\\nPlease select collection or delivery type\";");
        strmResponse.println("error=true;");
        strmResponse.println("}");
        strmResponse.println("if(chkDelivery.checked && sPhone.length==0)");
        strmResponse.println("{");
        strmResponse.println("document.getElementById(\"lblPhone\").innerHTML = \"Phone *\";");
        strmResponse.println("sMsg1=sMsg1+\"\\nPhone number is mandatory for delivery address.\";");
        strmResponse.println("error=true;");
        strmResponse.println("}");
        //strmResponse.println("alert(\"after if\");");
        
        strmResponse.println("if(error==false)");
        strmResponse.println("{");
        //strmResponse.println("alert(sCountry);");
        strmResponse.println("if(sCountry==\""+Constants.COUNTRY_CODE+"\" && hdLatitude.value==\"\" && hdLongitude.value==\"\")");
        strmResponse.println("{");
       // strmResponse.println("alert(sCompany);");
        strmResponse.println("document.VerifyForm."+Constants.USER_INPUT+".value=sCompany +\"^\" + sAddress1+\"^\"+sAddress2+\"^\"+sTown+ \"^\"+sCounty+\"^\"+sPostcode");
        strmResponse.println("document.VerifyForm.sPhone.value=sPhone");
       // strmResponse.println("alert(sCompany);");
        strmResponse.println("document.VerifyForm.sInstruct.value=sInstruct");
        strmResponse.println("document.VerifyForm.sAddress3.value=sAddress3");
        strmResponse.println("document.VerifyForm.sCountry.value=sCountry");
        strmResponse.println("if(chkCollection.checked && chkDelivery.checked)");
        strmResponse.println("document.VerifyForm.sType.value=\"CD\"");
        strmResponse.println("if(chkCollection.checked && !chkDelivery.checked)");
        strmResponse.println("document.VerifyForm.sType.value=\"C\"");
        strmResponse.println("if(!chkCollection.checked && chkDelivery.checked)");
        strmResponse.println("document.VerifyForm.sType.value=\"D\"");
       // strmResponse.println("alert(sCompany);");
        strmResponse.println("document.VerifyForm.sContact.value=sContact");
      //  strmResponse.println("alert(sCompany);");
        strmResponse.println("document.VerifyForm.sId.value=\""+sId+"\";");
        //strmResponse.println("alert(selClosesByHH.value);");
        strmResponse.println("document.VerifyForm.sClosesByHH.value=selClosesByHH.value;");
        //strmResponse.println("alert(selClosesByMM.value);");
        strmResponse.println("document.VerifyForm.sClosesByMM.value=selClosesByMM.value;");
        //strmResponse.println("alert(sCompany);");
        strmResponse.println("document.VerifyForm.sUCompany.value=sCompany;");
        
         // added by Adnan on 26 Aug 2013 against Residential Flag change
        strmResponse.println("document.VerifyForm.sResidentialFlag.value=sResidentialFlag");
        
        strmResponse.println("document.VerifyForm.submit();");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        // Assign the values to all the hidden fields.  
        strmResponse.println("document.AddAddressForm.sCompanyName.value=sCompany");
        strmResponse.println("document.AddAddressForm.sContactName.value=sContact");
        strmResponse.println("document.AddAddressForm.sAddress1.value=sAddress1");
        strmResponse.println("document.AddAddressForm.sAddress2.value=sAddress2");
        strmResponse.println("document.AddAddressForm.sAddress3.value=sAddress3");
       // strmResponse.println("document.AddAddressForm.sEmail.value=sEmail");
        strmResponse.println("document.AddAddressForm.sPhone.value=sPhone");
       // strmResponse.println("document.AddAddressForm.sMobile.value=sMobile");
        strmResponse.println("document.AddAddressForm.sTown.value=sTown");
        strmResponse.println("document.AddAddressForm.sCounty.value=sCounty");
        
        strmResponse.println("document.AddAddressForm.sPostcode.value=sPostcode");
        strmResponse.println("document.AddAddressForm.sCountry.value=sCountry");
        
        // added by Adnan on 26 Aug 2013 against Residential Flag change
        strmResponse.println("document.AddAddressForm.sResidentialFlag.value=sResidentialFlag");
        strmResponse.println("document.AddAddressForm.sInstruct.value=sInstruct");
        strmResponse.println("document.AddAddressForm.sClosesByHH.value=selClosesByHH.value");
        strmResponse.println("document.AddAddressForm.sClosesByMM.value=selClosesByMM.value");
        strmResponse.println("if(chkCollection.checked && chkDelivery.checked)");
        strmResponse.println("document.AddAddressForm.sType.value=\"CD\"");
        strmResponse.println("if(chkCollection.checked && !chkDelivery.checked)");
        strmResponse.println("document.AddAddressForm.sType.value=\"C\"");
        strmResponse.println("if(!chkCollection.checked && chkDelivery.checked)");
        strmResponse.println("document.AddAddressForm.sType.value=\"D\"");
        strmResponse.println("document.AddAddressForm.sId.value=\""+sId+"\";");
        strmResponse.println("document.AddAddressForm.sLatitude.value=hdLatitude.value;");
        strmResponse.println("document.AddAddressForm.sLongitude.value=hdLongitude.value;");
        strmResponse.println("document.AddAddressForm.submit();");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("alert(sMsg1+sMsg2);");
        strmResponse.println("}");
        
        //submit form to search addresses.
        strmResponse.println("function Search()");
        strmResponse.println("{");
        strmResponse.println("var sCompany=document.getElementById(\"txtSrchCompany\");");
        strmResponse.println("document.SearchForm.sCompanyName.value=sCompany.value;");
        strmResponse.println("document.SearchForm.sType.value='C'");
        strmResponse.println("document.SearchForm.submit();");
        strmResponse.println("}");
        
        //QAS Pick List Form
        
        strmResponse.println("function QASlist_Click(asMonikers)");
        strmResponse.println("{");
        strmResponse.println("var txtCompany=document.getElementById(\"txtCompany\");");
        //strmResponse.println("alert('QASlist');");
        strmResponse.println("document.PickListForm."+Constants.MONIKER+".value=asMonikers;");
        strmResponse.println("document.PickListForm.sId.value=\""+sId+"\";");
        strmResponse.println("document.PickListForm.sUCompany.value=txtCompany.value");
        strmResponse.println("document.PickListForm.submit();");
        strmResponse.println("}");
        
        //QAS Postcode Lookup
        
        strmResponse.println("function QASPostcodeLookup()");
        strmResponse.println("{");

        strmResponse.println("var txtCompany=document.getElementById(\"txtCompany\");");
        strmResponse.println("var txtAddress1=document.getElementById(\"txtAddress1\");");
        strmResponse.println("var txtAddress3=document.getElementById(\"txtAddress3\");");
        strmResponse.println("var txtPhone=document.getElementById(\"txtPhone\");");
        strmResponse.println("var txtTown=document.getElementById(\"txtTown\");");
        strmResponse.println("var txtCounty=document.getElementById(\"txtCounty\");");
        strmResponse.println("var txtPostcode=document.getElementById(\"txtPostcode\");");
        strmResponse.println("var selCountry=document.getElementById(\"selCountry\");");
        strmResponse.println("var chkCollection=document.getElementById(\"chkCollection\");");
        strmResponse.println("var chkDelivery=document.getElementById(\"chkDelivery\");");
        strmResponse.println("var txtInstruct=document.getElementById(\"txtInstruct\");");
        strmResponse.println("var selClosesByHH=document.getElementById(\"selClosesByHH\");");
        strmResponse.println("var selClosesByMM=document.getElementById(\"selClosesByMM\");");
        strmResponse.println("var lblMessage=document.getElementById(\"lblMessage\");");

        strmResponse.println("var sPhone=txtPhone.value;");
        strmResponse.println("var sContact=txtContact.value;");
        strmResponse.println("var sInstruct=txtInstruct.value;");
        strmResponse.println("var sCountry=selCountry.value;");
        strmResponse.println("var sAddress3=txtAddress3.value;");
        strmResponse.println("var error=false;");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = \"\";");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = \"\";");
        
//        strmResponse.println("if(txtAddress1.value==\"\")");
//        strmResponse.println("lblMessage.innerText=\"Please enter Building No in the Address1 field to continue to Postcode Lookup\";");
//        strmResponse.println("else");
        strmResponse.println("if(txtPostcode.value==\"\")");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = \"Please enter postcode to continue to Postcode Lookup\";");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = \"Please enter postcode to continue to Postcode Lookup\";");
        strmResponse.println("  error=true;");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("if(selCountry.value!=\""+Constants.COUNTRY_CODE+"\")");
        strmResponse.println("{");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText = \"Postcode Lookup applies for UK postcodes only. Please enter UK postcode and select country as United Kingdom to continue to Postcode Lookup\";");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = \"Postcode Lookup applies for UK postcodes only. Please enter UK postcode and select country as United Kingdom to continue to Postcode Lookup\";");
        strmResponse.println("  error=true;");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("if(error==false)");
        strmResponse.println("{");
        strmResponse.println("if(txtAddress1.value==\"\")");
        strmResponse.println("{");
        strmResponse.println("document.HierLookup."+Constants.USER_INPUT+".value=txtPostcode.value;");
        strmResponse.println("document.HierLookup.sId.value=\""+sId+"\";");
        strmResponse.println("document.HierLookup.sPhone.value=sPhone;");
        strmResponse.println("document.HierLookup.sInstruct.value=sInstruct;");
        strmResponse.println("document.HierLookup.sContact.value=sContact;");
        strmResponse.println("document.HierLookup.sAddress3.value=sAddress3");
        strmResponse.println("document.HierLookup.sUCompany.value=txtCompany.value");
        strmResponse.println("document.HierLookup.sCountry.value=sCountry;");
        strmResponse.println("document.HierLookup.sClosesByHH.value=selClosesByHH.value");
        strmResponse.println("document.HierLookup.sClosesByMM.value=selClosesByMM.value");
        strmResponse.println("if(chkCollection.checked && chkDelivery.checked)");
        strmResponse.println("document.HierLookup.sType.value=\"CD\";");
        strmResponse.println("if(chkCollection.checked && !chkDelivery.checked)");
        strmResponse.println("document.HierLookup.sType.value=\"C\";");
        strmResponse.println("if(!chkCollection.checked && chkDelivery.checked)");
        strmResponse.println("document.HierLookup.sType.value=\"D\";");
        strmResponse.println("document.HierLookup.submit();");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("document.PostcodeLookup."+Constants.USER_INPUT+".value=txtPostcode.value+\"^\"+txtAddress1.value;");
        strmResponse.println("document.PostcodeLookup.sId.value=\""+sId+"\";");
        strmResponse.println("document.PostcodeLookup.sPhone.value=sPhone;");
        strmResponse.println("document.PostcodeLookup.sInstruct.value=sInstruct;");
        strmResponse.println("document.PostcodeLookup.sContact.value=sContact;");
        strmResponse.println("document.PostcodeLookup.sAddress3.value=sAddress3");
        strmResponse.println("document.PostcodeLookup.sUCompany.value=txtCompany.value");
        strmResponse.println("document.PostcodeLookup.sCountry.value=sCountry;");
        strmResponse.println("document.PostcodeLookup.sClosesByHH.value=selClosesByHH.value");
        strmResponse.println("document.PostcodeLookup.sClosesByMM.value=selClosesByMM.value");
        strmResponse.println("if(chkCollection.checked && chkDelivery.checked)");
        strmResponse.println("document.PostcodeLookup.sType.value=\"CD\";");
        strmResponse.println("if(chkCollection.checked && !chkDelivery.checked)");
        strmResponse.println("document.PostcodeLookup.sType.value=\"C\";");
        strmResponse.println("if(!chkCollection.checked && chkDelivery.checked)");
        strmResponse.println("document.PostcodeLookup.sType.value=\"D\";");
        strmResponse.println("document.PostcodeLookup.submit();");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("}");
        
        //Bulk Delete click
        strmResponse.println("function bDelete()");
        strmResponse.println("{");
        strmResponse.println("var sId=\"\";");
        strmResponse.println("var sDeleteIds=\"\";");
        strmResponse.println("var CInputBooking=document.getElementsByTagName('input');");
        strmResponse.println("for (var i=0,thisInput; thisInput=CInputBooking[i]; i++) {");
        strmResponse.println("if(thisInput.type==\"checkbox\" && thisInput.name==\"chkSelect\")");
        strmResponse.println("{");
        strmResponse.println("if(thisInput.checked==true)");
        strmResponse.println("sId=sId+thisInput.value+\",\";");
        strmResponse.println("}");
        strmResponse.println("}");
        strmResponse.println("sDeleteIds=sId.replace(\",\",\"\");");
        //strmResponse.println("alert(sDeleteIds);");
        strmResponse.println("if(sDeleteIds.length>0)");
        strmResponse.println("{");
        strmResponse.println("document.BulkDeleteForm.sId.value=sId;");
        strmResponse.println("document.BulkDeleteForm.submit();");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("alert('Please select the addresses to be deleted');");
        strmResponse.println("}");

        //Clearing Address fields
        strmResponse.println("function clearAddress()");
        strmResponse.println("{");
        strmResponse.println("var tblEntry=document.getElementById(\"entryTable\");");
        strmResponse.println("var btnEntry=document.getElementById(\"btnEntry\");");
        //Identify all the input types first
        strmResponse.println("var CInputAddress=tblEntry.getElementsByTagName('input');");
        //Clear all the input fields in the Collection Address section.
        strmResponse.println("for (var i=0,thisInput; thisInput=CInputAddress[i]; i++) {");
        strmResponse.println("if(thisInput.type==\"text\")");
        strmResponse.println("thisInput.value='';");
        strmResponse.println("}");
        strmResponse.println("var selResidentialFlag=document.getElementById(\"chkResidentialFlag\");");
        strmResponse.println("selResidentialFlag.checked=\"false\";");
        
        strmResponse.println("var selCountry=document.getElementById(\"selCountry\");");
        strmResponse.println("selCountry.value=\""+Constants.COUNTRY_CODE+"\";");
        
        strmResponse.println("var selClosesByHH=document.getElementById(\"selClosesByHH\");");
        strmResponse.println("selClosesByHH.value=\"01\";");
        strmResponse.println("var selClosesByMM=document.getElementById(\"selClosesByMM\");");
        strmResponse.println("selClosesByMM.value=\"00\";");
        strmResponse.println("var hdLatitude=document.getElementById(\"hdLatitude\");");
        strmResponse.println("var hdLongitude=document.getElementById(\"hdLongitude\");");
        strmResponse.println("hdLatitude.value=\"\";");
        strmResponse.println("hdLongitude.value=\"\";");
        strmResponse.println("document.getElementById(\"txtTown\").disabled=false;");
        strmResponse.println("document.getElementById(\"txtPostcode\").disabled=false;");
        strmResponse.println("document.getElementById(\"selCountry\").disabled=false;");
        strmResponse.println("btnEntry.value=\"Validate|Save Address\";");
        strmResponse.println("}");

        //Editing Address fields
        strmResponse.println("function editAddress()");
        strmResponse.println("{");
        strmResponse.println("var hdLatitude=document.getElementById(\"hdLatitude\");");
        strmResponse.println("var hdLongitude=document.getElementById(\"hdLongitude\");");
        strmResponse.println("var btnPostcodeLookup=document.getElementById('btnPostcodeLookup');");
        strmResponse.println("var selCountry=document.getElementById(\"selCountry\");");
        strmResponse.println("hdLatitude.value=\"\";");
        strmResponse.println("hdLongitude.value=\"\";");
        strmResponse.println("document.getElementById(\"txtTown\").disabled=false;");
        strmResponse.println("document.getElementById(\"txtPostcode\").disabled=false;");
        strmResponse.println("document.getElementById(\"selCountry\").disabled=false;");
        strmResponse.println("var btnEntry=document.getElementById(\"btnEntry\");");
        strmResponse.println("btnEntry.value=\"Validate|Save Address\";");
        strmResponse.println("btnPostcodeLookup.style.display='';");
        strmResponse.println("selCountry.disabled=false;");
        strmResponse.println("}");

        //Country change
        strmResponse.println("function dCountryChange(dCountry)");
        strmResponse.println("{");
        //strmResponse.println("alert(dCountry);");
        strmResponse.println("if(dCountry==\"GBR\")");
        strmResponse.println("{");
        strmResponse.println("  document.getElementById(\"lblPostcode\").innerHTML=\"Postcode *\";");
//        strmResponse.println("  document.getElementById(\"lblPostcode\").style.color='#CC0000';");
//        strmResponse.println("  document.getElementById(\"txtPostcode\").style.border='3px solid #7F9DB9';");
        strmResponse.println("  document.getElementById(\"btnPostcodeLookup\").style.display='';");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("  document.getElementById(\"lblPostcode\").innerHTML=\"Postcode\";");
//        strmResponse.println("  document.getElementById(\"lblPostcode\").style.color='#666666';");
//        strmResponse.println("  document.getElementById(\"txtPostcode\").style.border='1px solid #7F9DB9';");
        strmResponse.println("  document.getElementById(\"btnPostcodeLookup\").style.display='none';");
        strmResponse.println("}");
        strmResponse.println("}");

        strmResponse.println("</script>");
        
        //AddAddress form to process new address entry
        strmResponse.println("<form name=\"AddAddressForm\" method=\"POST\" action=\""+Constants.srvAddAddress+"\">");
        strmResponse.println("<input type=hidden name=\"sCompanyName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sContactName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sAddress1\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sAddress2\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sAddress3\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sTown\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sCounty\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sPostcode\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sCountry\" value=\"\">");
        
        strmResponse.println("<input type=hidden name=\"sResidentialFlag\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sInstruct\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sClosesByHH\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sClosesByMM\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sType\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sBookingType\" value=\""+sBookingType+"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.SACTION+"\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sId\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sLatitude\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sLongitude\" value=\"\">");
        strmResponse.println("</form>");
        
        //QAS form
        strmResponse.println("<form name=\"VerifyForm\" method=\"POST\" action=\""+Constants.srvQasController+"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.COMMAND+"\" value=\""+Verify.NAME+"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.USER_INPUT+"\" value=\"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.COUNTRY_NAME+"\" value=\""+Constants.COUNTRY_VALUE+"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.DATA_ID+"\" value=\""+Constants.COUNTRY_CODE+"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.SACTION+"\" value=\"QAS\">");
        strmResponse.println("<input type=hidden name=\"sInstruct\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sClosesByHH\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sClosesByMM\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sType\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sBookingType\" value=\""+sBookingType+"\">");
        strmResponse.println("<input type=hidden name=\"sPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sContact\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sAddress3\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sCountry\" value=\"\">");
        
        strmResponse.println("<input type=hidden name=\"sResidentialFlag\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sUCompany\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sId\" value=\"\">");
        strmResponse.println("</form>");
        
        //QAS Pick List Form 
        
        strmResponse.println("<form name=\"PickListForm\" method=\"POST\" action=\""+Constants.srvQasController+"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.SACTION+"\" value=\"QAS\">");
        strmResponse.println("<input type=hidden name=\"sInstruct\" value=\""+sInstruct+"\">");
        strmResponse.println("<input type=hidden name=\"sType\" value=\""+sType+"\">");
        strmResponse.println("<input type=hidden name=\"sBookingType\" value=\""+sBookingType+"\">");
        strmResponse.println("<input type=hidden name=\"sPhone\" value=\""+sPhone+"\">");
        strmResponse.println("<input type=hidden name=\"sContact\" value=\""+sContact+"\">");
        strmResponse.println("<input type=hidden name=\"sAddress3\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sCountry\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sClosesByHH\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sClosesByMM\" value=\"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.SACTION_SUB+"\" value=\"ShowAddress\">");
        strmResponse.println("<input type=hidden name=\""+Constants.VERIFY_INFO+"\" value=\""+SearchResult.InteractionRequired+"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.COMMAND+"\" value=\""+VerifyFormatAddress.NAME+"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.MONIKER+"\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sUCompany\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sId\" value=\"\">");
        strmResponse.println("</form>");
        
        //SearchForm to process address search
        strmResponse.println("<form name=\"SearchForm\" method=\"POST\" action=\""+Constants.srvAddressBook+"\">");
        strmResponse.println("<input type=hidden name=\"sCompanyName\" value=\"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.SACTION+"\" value=\"ShowAddress\">");
        strmResponse.println("<input type=hidden name=\"sType\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sBookingType\" value=\""+sBookingType+"\">");
        strmResponse.println("</form>");
        
        //Postcode lookup form
        strmResponse.println("<form name=\"PostcodeLookup\" method=\"POST\" action=\""+Constants.srvQasController+"\">");
        strmResponse.println("<input type=\"hidden\" name=DataId value=\""+Constants.COUNTRY_CODE+"\">");
        strmResponse.println("<input type=\"hidden\" name="+Constants.COUNTRY_NAME+" value="+Constants.COUNTRY_VALUE+">");
        strmResponse.println("<input type=\"hidden\" name="+Constants.PROMPT_SET+" value=\"Optimal\">");
        strmResponse.println("<input type=\"hidden\" name="+Constants.COMMAND+" value=\""+FlatSearch.NAME+"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.USER_INPUT+"\" value=\"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.SACTION+"\" value=\"QAS\">");
        strmResponse.println("<input type=hidden name=\""+Constants.SACTION_SUB+"\" value=\""+FlatSearch.NAME+"\">");
        strmResponse.println("<input type=hidden name=\"sId\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sInstruct\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sClosesByHH\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sClosesByMM\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sType\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sBookingType\" value=\""+sBookingType+"\">");
        strmResponse.println("<input type=hidden name=\"sPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sContact\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sAddress3\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sCountry\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sUCompany\" value=\"\">");
        strmResponse.println("</form>");

         //HierSearch lookup form
        strmResponse.println("<form name=\"HierLookup\" method=\"POST\" action=\""+Constants.srvQasController+"\">");
        strmResponse.println("<input type=\"hidden\" name=DataId value=\""+Constants.COUNTRY_CODE+"\">");
        strmResponse.println("<input type=\"hidden\" name=CountryName value=\""+Constants.COUNTRY_NAME+"\">");
        strmResponse.println("<input type=\"hidden\" name=PromptSet value=\"Optimal\">");
        strmResponse.println("<input type=\"hidden\" name=Command value=\""+HierSearch.NAME+"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.USER_INPUT+"\" value=\"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.SACTION+"\" value=\"QAS\">");
        strmResponse.println("<input type=hidden name=\""+Constants.SACTION_SUB+"\" value=\""+HierSearch.NAME+"\">");
        strmResponse.println("<input type=hidden name=\"sId\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sInstruct\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sClosesByHH\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sClosesByMM\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sType\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sBookingType\" value=\""+sBookingType+"\">");
        strmResponse.println("<input type=hidden name=\"sPhone\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sContact\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sAddress3\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sCountry\" value=\"\">");
        strmResponse.println("<input type=hidden name=\"sUCompany\" value=\"\">");
        strmResponse.println("</form>");

        //Bulk Delete form
        strmResponse.println("<form name=\"BulkDeleteForm\" method=\"POST\" action=\""+Constants.srvAddressBook+"\">");
        strmResponse.println("<input type=hidden name=\"sId\" value=\"\">");
        strmResponse.println("<input type=hidden name=\""+Constants.SACTION+"\" value=\"BulkDelete\">");
        strmResponse.println("<input type=hidden name=\""+Constants.STYPE+"\" value="+sType+">");
        strmResponse.println("<input type=hidden name=\"sBookingType\" value=\""+sBookingType+"\">");
        strmResponse.println("</form>");
        
        strmResponse.println("</html>");
    
    }
    
    /**
     *
     * @param request
     * @param response
     * @param acc_id
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     */
    protected void Search(HttpServletRequest request, HttpServletResponse response,String acc_id, String sBookingType)throws ServletException, IOException, SQLException
    {
        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");
        String sContact = "", sPhone = "", sCompany = "", sAddress = "", sTown = "",
               sPostcode = "", sCountry = "", sEmail = "", sAddress2 = "", sAddress3 = "",
               sCounty = "", sInstruct="",sLatitude="",sLongitude="", sClosesByHH="",
               sClosesByMM="",sClosesBy="",sUCompany="", sResidentialFlag = "";
        String[] asClosesBy = null;
        String sSrchCompany=sReplaceChars(request.getParameter("sCompanyName"),"'","''");

        strmResponse.println("<html>");
        strmResponse.println("<head>");
        strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
        strmResponse.println("</head>");
        strmResponse.println("<body>");

        if(sSrchCompany.length()==0)
            strmResponse.println("<div style=\"color:green;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:14px;padding-left:150px\"><br><br><br><br><br><br><br>Enter company name / part of company name and try again.<br><br><a href=\"#\" onClick=\"Javascript:window.close();\" >Return to Booking Screen</a></div>");
        else
        {
            int iCount = 0;
            int iCount1 = 0;

            String sqlexact = "Select * from address where company='" + sSrchCompany + "' and owned_by='"+acc_id+"' and environment='"+environment+"' and server='"+host+"'";
            String sql = "Select * from address where (company like '%" + sSrchCompany + "%') and owned_by='"+acc_id+"' and environment='"+environment+"' and server='"+host+"' order by company asc";
            
            Connection conDB = null;
            Statement stmRead;
            ResultSet rstRead;
            try
            {
            
            conDB = cOpenConnection();
            stmRead = conDB.createStatement();
            rstRead = stmRead.executeQuery(sqlexact);
            
            while (rstRead.next())
            {
                iCount1 = iCount1 + 1;
            }
          
            if (iCount1 > 0)
            {
                if (iCount1 == 1)
                {
                    rstRead.first();
                    
                    sContact = rstRead.getString("contact");
                    sPhone = rstRead.getString("phone");
                    sCompany = rstRead.getString("company");
                    sAddress = rstRead.getString("address1");
                    sAddress2 = rstRead.getString("address2");
                    sAddress3 = rstRead.getString("address3");
                    sTown = rstRead.getString("town");
                    sCounty=rstRead.getString("county");
                    sInstruct=rstRead.getString("instructions");
                    sPostcode = rstRead.getString("postcode");
                    sCountry = rstRead.getString("country");
                    sLatitude = rstRead.getString("latitude");
                    sLongitude = rstRead.getString("longitude");
                    
                    sResidentialFlag = rstRead.getString("residential_flag");
                    
                    sClosesBy=rstRead.getString("closesat");
                    sClosesBy=rstRead.getString("closesat");
                    asClosesBy=sClosesBy.split(":");

                    if(sLatitude==null)
                        sLatitude="";

                    if(sLongitude==null)
                        sLongitude="";

                    if(asClosesBy.length>0)
                    {
                        sClosesByHH=asClosesBy[0]==""?"18":asClosesBy[0];
                        sClosesByMM=asClosesBy[1]==""?"00":asClosesBy[1];
                    }


                    if(sBookingType.equals("C"))
                    {
                        strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");

                        strmResponse.println("var txtCContact = window.opener.document.getElementById(\"txtCContact\");");
                        strmResponse.println("var txtCPhone = window.opener.document.getElementById(\"txtCPhone\");");
                        strmResponse.println("var txtCCompany = window.opener.document.getElementById(\"txtCCompany\");");
                        strmResponse.println("var txtCAddress1 = window.opener.document.getElementById(\"txtCAddress1\");");
                        strmResponse.println("var txtCAddress2 = window.opener.document.getElementById(\"txtCAddress2\");");
                        strmResponse.println("var txtCAddress3 = window.opener.document.getElementById(\"txtCAddress3\");");
                        strmResponse.println("var txtCTown = window.opener.document.getElementById(\"txtCTown\");");
                        strmResponse.println("var txtCCounty = window.opener.document.getElementById(\"txtCCounty\");");
                        strmResponse.println("var txtCInstruct = window.opener.document.getElementById(\"txtCInstruct\");");
                        strmResponse.println("var txtCPostcode = window.opener.document.getElementById(\"txtCPostcode\");");
                        
                        strmResponse.println("var selCResidentialFlag = window.opener.document.getElementById(\"chkCResidentialFlag\");");
                        
                         strmResponse.println( " if ('Y' == '" + sResidentialFlag + "' || 'y' == '" + sResidentialFlag + "'){");
                        strmResponse.println( " selCResidentialFlag.checked= true; " );
                        strmResponse.println( " } else { " );
                        strmResponse.println( " selCResidentialFlag.checked= false ; }" );
                        
                        //strmResponse.println("var selCCountry = window.opener.document.getElementById(\"selCCountry\");");
                        strmResponse.println("var selHHPickupBefore = window.opener.document.getElementById(\"selHHPickupBefore\");");
                        strmResponse.println("var selMMPickupBefore = window.opener.document.getElementById(\"selMMPickupBefore\");");
                        
                        strmResponse.println("txtCContact.value = \"" + sContact + "\";");
                        strmResponse.println("txtCPhone.value = \"" + sPhone + "\";");
                        strmResponse.println("txtCCompany.value = \"" + sCompany + "\";");
                        strmResponse.println("txtCAddress1.value = \"" + sAddress + "\";");
                        strmResponse.println("txtCAddress2.value = \"" + sAddress2 + "\";");
                        strmResponse.println("txtCAddress3.value = \"" + sAddress3 + "\";");
                        strmResponse.println("txtCTown.value = \"" + sTown + "\";");
                        strmResponse.println("txtCCounty.value = \"" + sCounty + "\";");
                        strmResponse.println("txtCInstruct.value = \"" + sInstruct + "\";");
                        strmResponse.println("txtCPostcode.value = \"" + sPostcode + "\";");
                        
//                         strmResponse.println("selCResidentialFlag.value = \"" + sResidentialFlag + "\";");
                         
                        //strmResponse.println("selCCountry.value = '" + sCountry + "';");
                        strmResponse.println("txtCInstruct.style.fontStyle='normal';");
                        strmResponse.println("selHHPickupBefore.value = '"+ sClosesByHH + "'");
                        strmResponse.println("selMMPickupBefore.value = '"+ sClosesByMM + "'");

                        if(sCountry.equals(Constants.COUNTRY_CODE) && !sLatitude.equals("") && !sLongitude.equals(""))
                        {
                            strmResponse.println("txtCTown.disabled=true;");
                            strmResponse.println("txtCPostcode.disabled=true;");
                            

                        }
                        else
                        {
                            strmResponse.println("txtCTown.disabled=false;");
                            strmResponse.println("txtCPostcode.disabled=false;");
                            

                        }

                        strmResponse.println("window.close();");
                        strmResponse.println("</script>");
                    }
                    else
                    {
                        strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");

                        strmResponse.println("var txtDContact = window.opener.document.getElementById(\"txtDContact\");");
                        strmResponse.println("var txtDPhone = window.opener.document.getElementById(\"txtDPhone\");");
                        strmResponse.println("var txtDCompany = window.opener.document.getElementById(\"txtDCompany\");");
                        strmResponse.println("var txtDAddress1 = window.opener.document.getElementById(\"txtDAddress1\");");
                        strmResponse.println("var txtDAddress2 = window.opener.document.getElementById(\"txtDAddress2\");");
                        strmResponse.println("var txtDAddress3 = window.opener.document.getElementById(\"txtDAddress3\");");
                        strmResponse.println("var txtDTown = window.opener.document.getElementById(\"txtDTown\");");
                        strmResponse.println("var txtDCounty = window.opener.document.getElementById(\"txtDCounty\");");
                        strmResponse.println("var txtDInstruct = window.opener.document.getElementById(\"txtDInstruct\");");
                        strmResponse.println("var txtDPostcode = window.opener.document.getElementById(\"txtDPostcode\");");
                        strmResponse.println("var selDCountry = window.opener.document.getElementById(\"selDCountry\");");
                        strmResponse.println("var lblDPostcode = window.opener.document.getElementById(\"lblDPostcode\");");

                        strmResponse.println("var selDResidentialFlag = window.opener.document.getElementById(\"chkDResidentialFlag\");");
                        
                         strmResponse.println( " if ('Y' == '" + sResidentialFlag + "' || 'y' == '" + sResidentialFlag + "'){");
                        strmResponse.println( " selDResidentialFlag.checked= true; " );
                        strmResponse.println( " } else { " );
                        strmResponse.println( " selDResidentialFlag.checked= false ; }" );
                        
                        strmResponse.println("txtDContact.value = \"" + sContact + "\";");
                        strmResponse.println("txtDPhone.value = \"" + sPhone + "\";");
                        strmResponse.println("txtDCompany.value = \"" + sCompany + "\";");
                        strmResponse.println("txtDAddress1.value = \"" + sAddress + "\";");
                        strmResponse.println("txtDAddress2.value = \"" + sAddress2 + "\";");
                        strmResponse.println("txtDAddress3.value = \"" + sAddress3 + "\";");
                        strmResponse.println("txtDTown.value = \"" + sTown + "\";");
                        strmResponse.println("txtDCounty.value = \"" + sCounty + "\";");
                        strmResponse.println("txtDInstruct.value = \"" + sInstruct + "\";");
                        strmResponse.println("txtDPostcode.value = \"" + sPostcode + "\";");
                        strmResponse.println("selDCountry.value = \"" + sCountry + "\";");
                        
//                        strmResponse.println("selDResidentialFlag.value = \"" + sResidentialFlag + "\";");
                        
                        strmResponse.println("txtDInstruct.style.fontStyle='normal';");

                        if(sCountry.equals(Constants.COUNTRY_CODE) && !sLatitude.equals("") && !sLongitude.equals(""))
                        {
                            strmResponse.println("txtDTown.disabled=true;");
                            strmResponse.println("txtDPostcode.disabled=true;");
                            strmResponse.println("selDCountry.disabled=true;");
                            strmResponse.println("lblDPostcode.innerHTML=\"Postcode *\";");
                        }
                        else
                        {
                            strmResponse.println("txtDTown.disabled=false;");
                            strmResponse.println("txtDPostcode.disabled=false;");
                            strmResponse.println("selDCountry.disabled=false;");
                            strmResponse.println("lblDPostcode.innerHTML=\"Postcode\";");
                        }

                        strmResponse.println("window.close();");
                        strmResponse.println("</script>");
                    }
                }
                if (iCount1 > 1)
                {
                    String sSelect="Select * from address where company='"+sSrchCompany+"' and owned_by='"+acc_id+"' and environment='"+environment+"' and server='"+host+"' limit 0,5";
                    LoadPage(request,response,sSelect, sBookingType);
                }

            }
            else
            {
                rstRead.close();
                stmRead.close();
                stmRead = conDB.createStatement();
                rstRead = stmRead.executeQuery(sql);

                while (rstRead.next())
                {
                    iCount = iCount + 1;
                }
                
                if (iCount > 1)
                {
                    String sSelect="Select * from address where (company like '%" + sSrchCompany + "%') and owned_by='"+acc_id+"' and environment='"+environment+"' and server='"+host+"' order by company asc limit 0,5";
                    LoadPage(request,response,sSelect, sBookingType);
                }
                else if (iCount == 1)
                {
                    rstRead.first();

//                    sContact = sReplaceChars(rstRead.getString("contact"),"'","&apos;");
//                    sPhone = sReplaceChars(rstRead.getString("phone"),"'","&apos;");
//                    sCompany = sReplaceChars(rstRead.getString("company"),"'","&apos;");
//                    sAddress = sReplaceChars(rstRead.getString("address1"),"'","&apos;");
//                    sAddress2 = sReplaceChars(rstRead.getString("address2"),"'","&apos;");
//                    sAddress3 = sReplaceChars(rstRead.getString("address3"),"'","&apos;");
//                    sTown = sReplaceChars(rstRead.getString("town"),"'","&apos;");
//                    sCounty= sReplaceChars(rstRead.getString("county"),"'","&apos;");
//                    sInstruct= sReplaceChars(rstRead.getString("instructions"),"'","&apos;");
//                    sPostcode = sReplaceChars(rstRead.getString("postcode"),"'","&apos;");
//                    sCountry = sReplaceChars(rstRead.getString("country"),"'","&apos;");
//                    sLatitude = sReplaceChars(rstRead.getString("latitude"),"'","&apos;");
//                    sLongitude = sReplaceChars(rstRead.getString("longitude"),"'","&apos;");
                    sContact = rstRead.getString("contact");
                    sPhone = rstRead.getString("phone");
                    sCompany = rstRead.getString("company");
                    sAddress = rstRead.getString("address1");
                    sAddress2 = rstRead.getString("address2");
                    sAddress3 = rstRead.getString("address3");
                    sTown = rstRead.getString("town");
                    sCounty=rstRead.getString("county");
                    sInstruct=rstRead.getString("instructions");
                    sPostcode = rstRead.getString("postcode");
                    sCountry = rstRead.getString("country");
                    sLatitude = rstRead.getString("latitude");
                    sLongitude = rstRead.getString("longitude");
                    
                    sResidentialFlag = rstRead.getString("residential_flag");
                    
                    sClosesBy=rstRead.getString("closesat");
                    asClosesBy=sClosesBy.split(":");

                    if(sLatitude==null)
                        sLatitude="";

                    if(sLongitude==null)
                        sLongitude="";

                    if(asClosesBy.length>0)
                    {
                        sClosesByHH=asClosesBy[0]==""?"18":asClosesBy[0];
                        sClosesByMM=asClosesBy[1]==""?"00":asClosesBy[1];
                    }
                    
                    if(sBookingType.equals("C"))
                    {
                        strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");

                        strmResponse.println("var txtCContact = window.opener.document.getElementById(\"txtCContact\");");
                        strmResponse.println("var txtCPhone = window.opener.document.getElementById(\"txtCPhone\");");
                        strmResponse.println("var txtCCompany = window.opener.document.getElementById(\"txtCCompany\");");
                        strmResponse.println("var txtCAddress1 = window.opener.document.getElementById(\"txtCAddress1\");");
                        strmResponse.println("var txtCAddress2 = window.opener.document.getElementById(\"txtCAddress2\");");
                        strmResponse.println("var txtCAddress3 = window.opener.document.getElementById(\"txtCAddress3\");");
                        strmResponse.println("var txtCTown = window.opener.document.getElementById(\"txtCTown\");");
                        strmResponse.println("var txtCCounty = window.opener.document.getElementById(\"txtCCounty\");");
                        strmResponse.println("var txtCInstruct = window.opener.document.getElementById(\"txtCInstruct\");");
                        strmResponse.println("var txtCPostcode = window.opener.document.getElementById(\"txtCPostcode\");");
                        strmResponse.println("var txtCLatitude = window.opener.document.getElementById(\"txtCLatitude\");");
                        strmResponse.println("var txtCLongitude = window.opener.document.getElementById(\"txtCLongitude\");");
                        //strmResponse.println("var selCCountry = window.opener.document.getElementById(\"selCCountry\");");
                        
                        strmResponse.println("var selCResidentialFlag = window.opener.document.getElementById(\"selCResidentialFlag\");");
                        
                        strmResponse.println("var selHHPickupBefore = window.opener.document.getElementById(\"selHHPickupBefore\");");
                        strmResponse.println("var selMMPickupBefore = window.opener.document.getElementById(\"selMMPickupBefore\");");
                        
                        
                        strmResponse.println("selCResidentialFlag.value = \"" + sResidentialFlag + "\";");
    
                        strmResponse.println("txtCContact.value = \"" + sContact + "\";");
                        strmResponse.println("txtCPhone.value = \"" + sPhone + "\";");
                        strmResponse.println("txtCCompany.value = \"" + sCompany + "\";");
                        strmResponse.println("txtCAddress1.value = \"" + sAddress + "\";");
                        strmResponse.println("txtCAddress2.value = \"" + sAddress2 + "\";");
                        strmResponse.println("txtCAddress3.value = \"" + sAddress3 + "\";");
                        strmResponse.println("txtCTown.value = \"" + sTown + "\";");
                        strmResponse.println("txtCCounty.value = \"" + sCounty + "\";");
                        strmResponse.println("txtCInstruct.value = \"" + sInstruct + "\";");
                        strmResponse.println("txtCPostcode.value = \"" + sPostcode + "\";");
                        strmResponse.println("txtCLatitude.value = \"" + sLatitude + "\";");
                        strmResponse.println("txtCLongitude.value = \"" + sLongitude + "\";");
                        //strmResponse.println("selCCountry.value = '" + sCountry + "';");
                        strmResponse.println("txtCInstruct.style.fontStyle='normal';");
                        strmResponse.println("selHHPickupBefore.value = '"+ sClosesByHH + "'");
                        strmResponse.println("selMMPickupBefore.value = '"+ sClosesByMM + "'");

                        if(sCountry.equals(Constants.COUNTRY_CODE) && !sLatitude.equals("") && !sLongitude.equals(""))
                        {
                            strmResponse.println("txtCTown.disabled=true;");
                            strmResponse.println("txtCPostcode.disabled=true;");
                            
                        }
                        else
                        {
                            strmResponse.println("txtCTown.disabled=false;");
                            strmResponse.println("txtCPostcode.disabled=false;");
                            
                        }

                        strmResponse.println("window.close();");
                        strmResponse.println("</script>");
                    }
                    else
                    {
                        strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");

                        strmResponse.println("var txtDContact = window.opener.document.getElementById(\"txtDContact\");");
                        strmResponse.println("var txtDPhone = window.opener.document.getElementById(\"txtDPhone\");");
                        strmResponse.println("var txtDCompany = window.opener.document.getElementById(\"txtDCompany\");");
                        strmResponse.println("var txtDAddress1 = window.opener.document.getElementById(\"txtDAddress1\");");
                        strmResponse.println("var txtDAddress2 = window.opener.document.getElementById(\"txtDAddress2\");");
                        strmResponse.println("var txtDAddress3 = window.opener.document.getElementById(\"txtDAddress3\");");
                        strmResponse.println("var txtDTown = window.opener.document.getElementById(\"txtDTown\");");
                        strmResponse.println("var txtDCounty = window.opener.document.getElementById(\"txtDCounty\");");
                        strmResponse.println("var txtDInstruct = window.opener.document.getElementById(\"txtDInstruct\");");
                        strmResponse.println("var txtDPostcode = window.opener.document.getElementById(\"txtDPostcode\");");
                        strmResponse.println("var selDCountry = window.opener.document.getElementById(\"selDCountry\");");
                        strmResponse.println("var txtDLatitude = window.opener.document.getElementById(\"txtDLatitude\");");
                        strmResponse.println("var txtDLongitude = window.opener.document.getElementById(\"txtDLongitude\");");
                        strmResponse.println("var lblDPostcode = window.opener.document.getElementById(\"lblDPostcode\");");
                        
                        strmResponse.println("var selDResidentialFlag = window.opener.document.getElementById(\"selDResidentialFlag\");");
	
                        strmResponse.println("selDResidentialFlag.value = \"" + sResidentialFlag + "\";");
        
                        strmResponse.println("txtDContact.value = \"" + sContact + "\";");
                        strmResponse.println("txtDPhone.value = \"" + sPhone + "\";");
                        strmResponse.println("txtDCompany.value = \"" + sCompany + "\";");
                        strmResponse.println("txtDAddress1.value = \"" + sAddress + "\";");
                        strmResponse.println("txtDAddress2.value = \"" + sAddress2 + "\";");
                        strmResponse.println("txtDAddress3.value = \"" + sAddress3 + "\";");
                        strmResponse.println("txtDTown.value = \"" + sTown + "\";");
                        strmResponse.println("txtDCounty.value = \"" + sCounty + "\";");
                        strmResponse.println("txtDInstruct.value = \"" + sInstruct + "\";");
                        strmResponse.println("txtDPostcode.value = \"" + sPostcode + "\";");
                        strmResponse.println("selDCountry.value = \"" + sCountry + "\";");
                        strmResponse.println("txtDInstruct.style.fontStyle='normal';");
                        strmResponse.println("txtDLatitude.value = \"" + sLatitude + "\";");
                        strmResponse.println("txtDLongitude.value = \"" + sLongitude + "\";");

                        if(sCountry.equals(Constants.COUNTRY_CODE) && !sLatitude.equals("") && !sLongitude.equals(""))
                        {
                            strmResponse.println("txtDTown.disabled=true;");
                            strmResponse.println("txtDPostcode.disabled=true;");
                            strmResponse.println("selDCountry.disabled=true;");
                            strmResponse.println("lblDPostcode.innerHTML=\"Postcode *\";");
                        }
                        else
                        {
                            strmResponse.println("txtDTown.disabled=false;");
                            strmResponse.println("txtDPostcode.disabled=false;");
                            strmResponse.println("selDCountry.disabled=false;");
                            strmResponse.println("lblDPostcode.innerHTML=\"Postcode\";");
                        }

                        strmResponse.println("window.close();");
                        strmResponse.println("</script>");
                    }
                }
                else if (iCount == 0)
                {
                   strmResponse.println("<div style=\"color:green;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:14px;padding-left:150px\"><br><br><br><br><br><br><br>No matches found, please check spelling and try again.<br><br><a href=\"#\" onClick=\"Javascript:window.close();\" >Return to Booking Screen</a></div>");
                }
            }
        
            }
            catch(Exception ex)
            {
                vShowErrorPage(response,ex);
            }
            finally
            {
                conDB.close();
            }
        }
        strmResponse.println("</body>");
        strmResponse.println("</html>");
    }
    
    /**
     *
     * @param request
     * @param response
     * @param acc_id
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     */
    public void BulkDelete(HttpServletRequest request, HttpServletResponse response,String acc_id, String sBookingType)throws ServletException, IOException, SQLException
    {
        Connection conDB = null;
        try
        {
            String sId=request.getParameter("sId")==null?"0":request.getParameter("sId").toString();
            sId=sId.substring(0,sId.length()-1);
           
            String sql="";
         
            
            Statement stmDelete;
            
            conDB = cOpenConnection();
                       
            String sDeleteSql="Delete from address where ID in ("+sId+")";

            stmDelete= conDB.createStatement();
            stmDelete.executeUpdate(sDeleteSql);	
           
            
            if(sBookingType.equals("C"))
            {
                sql="SELECT company,ID,postcode,town,county FROM address where collection=1 and owned_by='"+acc_id+"' and environment='"+environment+"' and server='"+host+"' order by company asc";
            }
            else
            {
                sql="SELECT company,ID,postcode,town,county FROM address where delivery=1 and owned_by='"+acc_id+"' and environment='"+environment+"' and server='"+host+"' order by company asc";
            }
            
            LoadPage(request,response,sql, sBookingType);
            
        }
        catch(Exception ex)
        {
            vShowErrorPage(response,ex);
        }
        finally
        {
            conDB.close();
        }
        
    }
    /**
     *
     * @param arrInput
     * @param arrLabels
     */
    public void readArray(String[] arrInput,String[] arrLabels, QASCollectionAddress qColR, String sUCompany)
    {

        switch(arrInput.length)
        {
            case 1:
                qColR.setSCAddress1("");
                qColR.setSCPostcode(arrInput[0]);
                qColR.setSCCompany("");
                qColR.setSCAddress2("");
                qColR.setSCTown("");
                qColR.setSCCounty("");
                break;
            case 2:
                qColR.setSCAddress1(arrInput[1]);
                qColR.setSCPostcode(arrInput[0]);
                qColR.setSCCompany("");
                qColR.setSCAddress2("");
                qColR.setSCTown("");
                qColR.setSCCounty("");
                break;
            default:
                for(int i=0;i<arrInput.length;i++)
                {
                    switch(i)
                    {
                        case 0:
                            if(arrLabels[i].equals("Organisation"))
                            {
                                if(arrInput[i].equals(""))
                                   qColR.setSCCompany(sUCompany);
                                else
                                    qColR.setSCCompany(arrInput[i]);
                            }
                            break;
                        case 1:
                            if(arrLabels[i].equals(""))
                            qColR.setSCAddress1(arrInput[i]);
                            break;
                        case 2:
                            if(arrLabels[i].equals(""))
                                qColR.setSCAddress2(arrInput[i]);
                            else
                            {
                                if(arrLabels[i].equals("Town"))
                                {
                                    qColR.setSCTown(arrInput[i]);
                                    qColR.setSCAddress2("");
                                }
                            }
                            break;
                        case 3:
                            if(arrLabels[i].equals("Town"))
                            qColR.setSCTown(arrInput[i]);
                            else
                            {
                                if(arrLabels[i].equals("County"))
                                    qColR.setSCCounty(arrInput[i]);
                            }
                            break;
                        case 4:
                            if(arrLabels[i].equals("County"))
                               qColR.setSCCounty(arrInput[i]);
                            else
                            {
                                 if(arrLabels[i].equals("Postcode"))
                                     qColR.setSCPostcode(arrInput[i]);
                            }
                            break;
                        case 5:
                            if(arrLabels[i].equals("Postcode"))
                            qColR.setSCPostcode(arrInput[i]);
                            break;
                        case 6:
                            if(arrLabels[i].equals("Country"))
                            {

                            }
                            break;
                        case 7:
                            if(arrLabels[i].equals("100m Latitude"))
                              qColR.setSCLatitude(arrInput[i]);
                            break;
                        case 8:
                            if(arrLabels[i].equals("100m Longitude"))
                              qColR.setSCLongitude(arrInput[i]);
                            break;
                    }
                }
            }
          
    }

    /**
     *
     * @param asAddress
     * @return
     */
    public String getDisplayAddress(String[] asAddress)
     {
         String sAddress="";

         for(int i=0;i<asAddress.length-4;i++)
         {
             if(asAddress[i].length()>0)
                sAddress=sAddress + asAddress[i]+ ",";
         }

         if(sAddress.length()>0)
             sAddress=sAddress.substring(0,sAddress.length()-1);

         return sAddress;
     }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try
        {
        processRequest(request, response);
        }
        catch(Exception ex)
        {
            
        }
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         try
        {
        processRequest(request, response);
        }
        catch(Exception ex)
        {
            
        }
    }
    
    /** Returns a short description of the servlet.
     * @return
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
