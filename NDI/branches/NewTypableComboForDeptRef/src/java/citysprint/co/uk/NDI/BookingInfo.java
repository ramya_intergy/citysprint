package citysprint.co.uk.NDI;

/*
 * BookingInfo.java
 *
 * Created on 27 March 2008, 15:42
 */

import citysprint.co.uk.NDI.*;
import citysprint.co.uk.NDI.LoginInfo;
import citysprint.co.uk.NDI.CommonClass;
import java.beans.*;
import java.io.Serializable;
import javax.servlet.http.*;
import javax.servlet.*;
import java.sql.*;
import java.io.*;
import com.qas.proweb.servlet.Constants;
/**
 * @author ramyas
 */
public class BookingInfo extends CommonClass implements Serializable {
    
    /**
     *
     */
        
    private String sBDept="",sBCaller="",sBPhone="",sBRef="",sBHAWBNO="";
    private String sRdCollect="",sCollectDate="",sCollectHH="",sCollectMM="",sChkPickupBefore="",sPickupBeforeHH="",sPickupBeforeMM="";
    private String sProductType="",sTotalPieces="",sTotalWeight="",sTotalVolWeight="",sAction="",sShipperVAT="",sDCountry="";
    private String sNTotalPieces="",sNTotalVolWeight="";
//    private String sParcelName = "", sParcelNameId = "";
//
//    public String getsParcelNameId() {
//        return sParcelNameId;
//    }
// 
//    
//    public String getsParcelName() {
//        return sParcelName;
//    }
  
    private int itemsRowCount = 0;

    public int getItemsRowCount() {
        return itemsRowCount;
    }
    
    /**
     *
     */
    public BookingInfo() {
        
    }
    
    //Booking Person's Dept
    /**
     *
     * @return
     */
    public String getBDept() {
         return sBDept;
    }
    //Booking Person's name
    /**
     *
     * @return
     */
    public String getBCaller() {
         return sBCaller;
    }
    //Booking Person's Phone
    /**
     *
     * @return
     */
    public String getBPhone() {
         return sBPhone;
    }
    //Booking Person's Reference
    /**
     *
     * @return
     */
    public String getBRef() {
         return sBRef;
    }
    //Booking Person's HawbNo
    /**
     *
     * @return
     */
    public String getBHAWBNO() {
         return sBHAWBNO;
    }
    //Collect Date Option
    /**
     *
     * @return
     */
    public String getRdCollect() {
         return sRdCollect;
    }
    //Collection Date
    /**
     *
     * @return
     */
    public String getCollectDate() {
         return sCollectDate;
    }
    //CollectBy Hour
    /**
     *
     * @return
     */
    public String getCollectHH() {
         return sCollectHH;
    }
    //CollectBy minutes
    /**
     *
     * @return
     */
    public String getCollectMM() {
         return sCollectMM;
    }
    //CollectBefore option
    /**
     *
     * @return
     */
    public String getChkPickupBefore() {
         return sChkPickupBefore;
    }
    //CollectBefore hour
    /**
     *
     * @return
     */
    public String getPickupBeforeHH() {
         return sPickupBeforeHH;
    }
    //CollectBefore minutes
    /**
     *
     * @return
     */
    public String getPickupBeforeMM() {
         return sPickupBeforeMM;
    }
    //ProductType
    /**
     *
     * @return
     */
    public String getProductType() {
         return sProductType;
    }
    //Total Pieces of the product
    /**
     *
     * @return
     */
    public String getTotalPieces() {
         
         return sTotalPieces;
    }
    //TotalWeight
    /**
     *
     * @return
     */
    public String getTotalWeight() {
         return sTotalWeight;
    }
    //TotalVolWeight
    /**
     *
     * @return
     */
    public String getTotalVolWeight() {
         return sTotalVolWeight;
    }
     //ShipperVAT
    /**
     *
     * @return
     */
    public String getShipperVAT() {
         return sShipperVAT;
    }
    
     //Total Pieces of the product
    /**
     *
     * @return
     */
    public String getNTotalPieces() {
         
         return sNTotalPieces;
    }
     //TotalVolWeight
    /**
     *
     * @return
     */
    public String getNTotalVolWeight() {
         return sNTotalVolWeight;
    }
     
    /**
     *
     * @param request
     * @param response
     * @param login
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void setBookingInfo(HttpServletRequest request,HttpServletResponse response,LoginInfo login) throws ServletException,IOException
    {
        
        //initialize config values
        initializeConfigValues(request,response);

        //read session values
        //getSessionValues(request);

        //parseLogin(loginArray);
        
        sAction=request.getParameter("sAction")==null?"":request.getParameter("sAction").toString();
        sBDept=request.getParameter("BDept")==null?"":request.getParameter("BDept").toString();
        sBCaller=request.getParameter("BCaller")==null?"":request.getParameter("BCaller").toString();
        sBPhone=request.getParameter("BPhone")==null?login.getCus_phone():request.getParameter("BPhone").toString();
        sBRef=request.getParameter("BRef")==null?"":request.getParameter("BRef").toString();
        sBHAWBNO=request.getParameter("BHAWBNO")==null?"":request.getParameter("BHAWBNO").toString();
        sRdCollect=request.getParameter("rdCollect")==null?"":request.getParameter("rdCollect").toString();
        sCollectDate=request.getParameter("CollectDate")==null?"":request.getParameter("CollectDate").toString();
        sCollectHH=request.getParameter("CollectHH")==null?"":request.getParameter("CollectHH").toString();
        sCollectMM=request.getParameter("CollectMM")==null?"":request.getParameter("CollectMM").toString();
        sChkPickupBefore=request.getParameter("chkPickupBefore")==null?"":request.getParameter("chkPickupBefore").toString();
        sPickupBeforeHH=request.getParameter("PickupBeforeHH")==null?"":request.getParameter("PickupBeforeHH").toString();
        sPickupBeforeMM=request.getParameter("PickupBeforeMM")==null?"":request.getParameter("PickupBeforeMM").toString();
        sProductType=request.getParameter("ProductType")==null?"":request.getParameter("ProductType").toString();
        sTotalWeight=request.getParameter("TotalWeight")==null?"":request.getParameter ("TotalWeight").toString();
        sShipperVAT=request.getParameter("ShipperVAT")==null?"":request.getParameter("ShipperVAT").toString();
        sDCountry=request.getParameter("DCountry")==null?"":request.getParameter("DCountry").toString();
        //sTotalVolWeight=request.getParameter("TotalVolWeight")==null?"":request.getParameter("TotalVolWeight").toString();
//        sParcelName = request.getParameter("ParcelName")==null?"":request.getParameter("ParcelName").toString();
//        sParcelNameId = request.getParameter("ParcelNameId")==null?"":request.getParameter("ParcelNameId").toString();
//                
//        if (sParcelName == null || sParcelName.trim().equalsIgnoreCase("undefined")){
//            sParcelName ="";
//            sParcelNameId = "SELECT";
//        }
        
        String itemsCount = request.getParameter("itemsRowCount")==null?"0":request.getParameter("itemsRowCount").toString();
        try{
            itemsRowCount = Integer.parseInt(itemsCount);
        }catch(Exception ex){}
        
        String sItemWeightCount = "0";

        sItemWeightCount = request.getParameter("itemWeightCount") == null ? "0" : request.getParameter("itemWeightCount").toString();
        
        System.out.append("########################### From BookingInfo, line# 239, sItemWeightCount = " + sItemWeightCount + ", itemsCount == " + itemsCount);// + ", sParcelName ==" + sParcelName);
            
        String sTempTotalWeight = sumItemWeight4TotalWeight(request,response);
        if (sTempTotalWeight != null && !sTempTotalWeight.trim().equals("") && !sTempTotalWeight.trim().equals("0"))  
            sTotalWeight=sTempTotalWeight;
        
        if(sProductType.equals("DOX"))
        {
            sTotalPieces=request.getParameter("TotalPieces")==null?"":request.getParameter("TotalPieces").toString();
            sTotalVolWeight="0";
            System.out.append("########################### From BookingInfo, line# 276, sTotalPieces = " + sTotalPieces);
            if (sTotalPieces != null && (sTotalPieces.trim().equals("") || sTotalPieces.trim().equals("0")))
                totalCalculate(request,response,login,"DOX");
        }
        else
        {
            if(sAction.equals("Clear"))
            {
                sTotalWeight="";
                sTotalPieces="";
                sTotalVolWeight="";
            }
            else
            {
                totalCalculate(request,response,login,"NDOX");                
            }
        }
    }
    
     public String sumItemWeight4TotalWeight(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException
     {
         String  sTempTotalWeight = "0";
         try
            {
        String sSQL="select SUM(pro_iweight) as totIWeight from pro_product where pro_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";
        
        
        Connection conDB = null;
                java.sql.Statement stmRead;
                ResultSet rstRead;

                conDB = cOpenConnection();
                stmRead = conDB.createStatement();
                rstRead = stmRead.executeQuery(sSQL);

                while(rstRead.next())
                {
                   
                      sTempTotalWeight=rstRead.getString("totIWeight")==null?"0":rstRead.getString("totIWeight");
                     
                }

                vCloseConnection(conDB);
            }
            catch(Exception ex)
            {
                vShowErrorPage(response,ex);
            }
         
         return sTempTotalWeight;
     }
    
    /**
     *
     * @param request
     * @param response
     * @param login
     * @param sType
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * calculates Volumetric Weight and Total Pieces
     */
    public void totalCalculate(HttpServletRequest request,HttpServletResponse response,LoginInfo login,String sType) throws ServletException,IOException
    {
        
            //initialize config values
            initializeConfigValues(request,response);
        
            try
            {

                String sSQL="";

                // dc - 23/11/2009 - Changed formula according to Kerry's instructions
                // dc - 24/11/2009 - changed again according to David Morgan's email

                //if(login.getSCubingFactor().length()>0 && !login.getSCubingFactor().equals("0"))
                //    sSQL="select SUM(CEILING((pro_width*pro_height*pro_length*pro_NoItems)/"+login.getSCubingFactor()+")) as volWeight, SUM(pro_NoItems) as totPieces from pro_product where pro_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";
                //else
                //    sSQL="select SUM(CEILING((pro_width*pro_height*pro_length*pro_NoItems)/6000)) as volWeight, SUM(pro_NoItems) as totPieces from pro_product where pro_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";


                if(login.getSCubingFactor().length()>0 && !login.getSCubingFactor().equals("0"))
                    // sSQL="select CAST(SUM(CEILING((pro_width*pro_height*pro_length*pro_NoItems*10)/"+login.getSCubingFactor()+")/10) AS DECIMAL(16,1)) as volWeight, SUM(pro_NoItems) as totPieces from pro_product where pro_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";
                    sSQL="select CAST(CEILING(SUM((pro_width*pro_height*pro_length*pro_NoItems)/" + login.getSCubingFactor() + ")*10)/10 as DECIMAL(16,1)) as volWeight, SUM(pro_NoItems) as totPieces from pro_product where pro_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";
                else
                    // sSQL="select CAST(SUM(CEILING((pro_width*pro_height*pro_length*pro_NoItems*10)/6000)/10) AS DECIMAL(16,1)) as volWeight, SUM(pro_NoItems) as totPieces from pro_product where pro_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";
                    sSQL="select CAST(CEILING(SUM((pro_width*pro_height*pro_length*pro_NoItems)/6000)*10)/10 as DECIMAL(16,1)) as volWeight, SUM(pro_NoItems) as totPieces from pro_product where pro_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";




                Connection conDB = null;
                java.sql.Statement stmRead;
                ResultSet rstRead;

                conDB = cOpenConnection();
                stmRead = conDB.createStatement();
                rstRead = stmRead.executeQuery(sSQL);

                while(rstRead.next())
                {
                    if(sType.equals("NDOX") /* || ( sParcelName != null && !sParcelName.trim().equals("")) */)
                    {
                        sTotalVolWeight=rstRead.getString("volWeight")==null?"0":rstRead.getString("volWeight");
                        sTotalPieces=rstRead.getString("totPieces")==null?"0":rstRead.getString("totPieces");
                    }else{
                        sTotalPieces=rstRead.getString("totPieces")==null?"0":rstRead.getString("totPieces");
                    }
                        sNTotalVolWeight=rstRead.getString("volWeight")==null?"0":rstRead.getString("volWeight");
                        sNTotalPieces=rstRead.getString("totPieces")==null?"0":rstRead.getString("totPieces");
                }

                vCloseConnection(conDB);
            }
            catch(Exception ex)
            {
                vShowErrorPage(response,ex);
            }
    }
    
    public String sumNoOfPieces(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException
     {
         String  sTempTotalWeight = "0";
         try
            {
        String sSQL="select SUM(pro_NoItems) as totPieces from pro_product where pro_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";
        
        
        Connection conDB = null;
                java.sql.Statement stmRead;
                ResultSet rstRead;

                conDB = cOpenConnection();
                stmRead = conDB.createStatement();
                rstRead = stmRead.executeQuery(sSQL);

                while(rstRead.next())
                {
                   
                      sTempTotalWeight=rstRead.getString("totPieces")==null?"0":rstRead.getString("totPieces");
                     
                }

                vCloseConnection(conDB);
            }
            catch(Exception ex)
            {
                vShowErrorPage(response,ex);
            }
         
         return sTempTotalWeight;
     }
}
