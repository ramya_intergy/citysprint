package citysprint.co.uk.NDI;

/*
 * CommodityInfo.java
 *
 * Created on 13 August 2008, 10:57
 */

import java.beans.*;
import java.io.Serializable;
import java.util.Vector;

/**
 * @author ramyas
 */
public class CommodityInfo extends Object implements Serializable {
    
    //Commodity API output
    private Vector ccVector = new Vector();           //Commodity code
    private Vector csVector = new Vector();           //Commodity Description
    private Vector ifVector = new Vector();           //Indemnity Apply Flag
    
    /**
     * Getter for property ccVector .
     * @return Value of property ccVector .
     */
    public Vector getCcVector () {
        return this.ccVector ;
    }

     /**
     * Getter for property csVector .
     * @return Value of property csVector .
     */
    public Vector getCsVector () {
        return this.csVector ;
    }
    
    /**
     * Getter for property ifVector .
     * @return Value of property ifVector .
     */
    public Vector getIfVector () {
        return this.ifVector ;
    }
    
    
   //parse commodity details array
    /**
     *
     * @param commodityArray
     */
    public void parseCommodity(String[][] commodityArray)
   {
       String tempstr = "";
       String tag = "";
       
      //Product API output  ^TS Transaction Stamp ^CC Commodity Code ^CD Commodity Description ^IF Indemnity Flag
       ccVector.clear();
       csVector.clear();
       ifVector.clear();
       
       for(int i=0;i<commodityArray.length-1;i++)
       {
           tag=commodityArray[i][0];
           
           //Commodity Code
           if(tag.equals("CC"))
           {
               tempstr=commodityArray[i][1].trim();
               
               ccVector.addElement(tempstr);    
           }
           
                    
           //Commodity Description
           if(tag.equals("CD"))
           {
               tempstr=commodityArray[i][1].trim();
               
               csVector.addElement(tempstr);
           }
           
           //Indemnity Apply Flag
           if(tag.equals("IF"))
           {
               tempstr=commodityArray[i][1].trim();
               
               ifVector.addElement(tempstr);
           }
           
      }

     
   }
    
}
