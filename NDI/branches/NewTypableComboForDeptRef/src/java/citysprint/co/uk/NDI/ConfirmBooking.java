package citysprint.co.uk.NDI;

/*
 * ConfirmBooking.java
 *
 * Created on 28 September 2007, 15:34
 */

import citysprint.co.uk.NDI.BookingInfo;
import citysprint.co.uk.NDI.CountryInfo;
import citysprint.co.uk.NDI.CreditCardDetails;
import citysprint.co.uk.NDI.LoginInfo;
import citysprint.co.uk.NDI.CommonClass;
import citysprint.co.uk.NDI.ServiceInfo;
import citysprint.co.uk.NDI.SessionAddresses;
import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import javax.sql.DataSource;
import java.lang.*;
import java.text.*;
import java.util.*;
import com.qas.proweb.*;
import com.qas.proweb.servlet.*;

/**
 *
 * @author ramyas
 * @version
 */
public class ConfirmBooking extends CommonClass {
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    
            
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        try
        {
            if(isLogin(request))
            {
                
                //initialize config values
                initializeConfigValues(request,response);
  
                LoadPage(request,response);
            }
            else
                {
                writeOutputLog("Session expired redirecting to index page");
                response.sendRedirect("index.jsp");
            }
        }
        catch(Exception ex)
        {
            vShowErrorPage(response,ex);
        }
    }
    
    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void LoadPage(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException
    {
        StringBuffer sbHeader=new StringBuffer();
        sbHeader=build_Header();
        String sHeader=new String();
        sHeader=sbHeader.substring(0,sbHeader.length());
        HttpSession validSession = request.getSession(false);

        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");

//        System.out.println("################# ConfirmBooking: LoadPage(): Line#87: parcelName= "+ request.getParameter("ParcelName"));
        
        
        LoginInfo login=new LoginInfo();
        BookingInfo booking=new BookingInfo();
        CreditCardDetails ccdetails= new CreditCardDetails();
        SessionAddresses srsaddress=new SessionAddresses();
        ServiceInfo service=new ServiceInfo();
        CountryInfo country=new CountryInfo();
        String[][] getServicesArray=null;
        
        //set login,collection,delivery address fields and other booking info fields by processing the request object
        login.parseLogin((String[][])validSession.getAttribute("loginArray"), request);
        booking.setBookingInfo(request,response,login);
        ccdetails.setCreditCardDetails(request);
        srsaddress.setSessionAddresses(request,response);
        country.parseCountries((String[][])validSession.getAttribute("getCountriesArray"));
       
        
        strmResponse.println("<html>");
        strmResponse.println("<head>");
        strmResponse.println("<META HTTP-EQUIV=\"CACHE-CONTROL\" CONTENT=\"NO-CACHE\">");
        strmResponse.println("  <title>CitySprint</title>");
        strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
        strmResponse.println("<script type=\"text/javascript\">");
        strmResponse.println("preload1 = new Image();");
        strmResponse.println("preload2 = new Image();");
        strmResponse.println("preload3 = new Image();");
        strmResponse.println("preload4 = new Image();");
        strmResponse.println("preload5 = new Image();");
        strmResponse.println("preload6 = new Image();");
        strmResponse.println("preload1.src = \"images/booking_active.gif\";");
        strmResponse.println("preload2.src = \"images/booking_default.gif\";");
        strmResponse.println("preload3.src = \"images/reporting_active.gif\";");
        strmResponse.println("preload4.src = \"images/reporting_default.gif\";");
        strmResponse.println("preload5.src = \"images/tracking_active_410.gif\";");
        strmResponse.println("preload6.src = \"images/tracking_default_410.gif\";");
        strmResponse.println("</script>");
        strmResponse.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
        strmResponse.println("<style>");
        strmResponse.println("html { overflow-x: hidden;overflow-y: auto;  }");
        strmResponse.println("</style>");
        strmResponse.println("</head>");
        strmResponse.println("<body background=\"#BCBCBC\" style=\"width:895px;\">");
        
        strmResponse.println("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding-left:4px;padding-right:4px\">");
        //strmResponse.println(sHeader);
        strmResponse.println("<tr>");
        strmResponse.println("<td colspan=\"2\" >");
        strmResponse.println("<img src=\"images/booking_active.gif\" id=\"imgTab1\" style=\"padding-bottom:0px;padding-top:1pxpadding-left:0px;\"><img src=\"images/reporting_default.gif\" id=\"imgTab2\" onclick=\"javascript:if(confirm('Your booking information will be lost if you navigate away from this page. Are you sure?')){window.location='Reporting.jsp?t="+new java.util.Date()+"';}\" style=\"padding-bottom:0px;padding-top:1pxpadding-left:0px;cursor:pointer\"><img src=\"images/tracking_default_410.gif\" id=\"imgTab3\" onclick=\"javascript:if(confirm('Your booking information will be lost if you navigate away from this page. Are you sure?')){window.location='Tracking?t="+new java.util.Date()+"';}\" style=\"padding-bottom:0px;padding-top:1pxpadding-left:0px;cursor:pointer\">");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");
        strmResponse.println("<tr id=\"trBooking\">");
        strmResponse.println("<td width=\"100%\" valign=\"top\" colspan=\"2\">");
            
        strmResponse.println("    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"box\" width=\"100%\">");
            
          
        strmResponse.println("    <tr style=\"padding-top:4px;padding-bottom:4px;\"><td id=\"boxtitle\" colspan=\"6\">BOOKING DETAILS</td></tr>");
        strmResponse.println("    <tr>");
        strmResponse.println("     <td id=\"label\">");
        if (login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("V")) {
            if (login.isEYFlag()) {
                strmResponse.println(login.getEy_Department());
            } else {
                strmResponse.println("Department");
            }
        }
        strmResponse.println("      </td>");
        strmResponse.println("      <td>" );
        if (login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("V")) {
            strmResponse.println(booking.getBDept().toUpperCase() + "");
        }
        strmResponse.println("      </td> ");
        strmResponse.println("      <td id=\"label\">");

        if (login.getRefFlag().equals("R") || login.getRefFlag().equals("Y") || login.getRefFlag().equals("V")) {
            if (login.isEYFlag()) {
                strmResponse.println(login.getEy_Reference());
            } else {
                strmResponse.println("Reference");
            }
        }
        strmResponse.println("      </td>");
        strmResponse.println("      <td>");
        if(login.getRefFlag().equals("R") || login.getRefFlag().equals("Y") || login.getRefFlag().equals("V")){
            strmResponse.println(booking.getBRef().toUpperCase()+"");
        }
        strmResponse.println("      </td> ");
        strmResponse.println("      <td></td>");
        strmResponse.println("   </tr>");
        strmResponse.println("    <tr>");
                
        strmResponse.println("      <td id=\"label\">");
        if (login.isEYFlag()) {
            strmResponse.println(login.getEy_YourName());
        } else {
            strmResponse.println("Caller");
        }
        strmResponse.println("      </td>");
        strmResponse.println("      <td>"+booking.getBCaller().toUpperCase()+"");   
        strmResponse.println("      </td> ");
        strmResponse.println("      <td id=\"label\">");
        if (login.isEYFlag()) {
            strmResponse.println(login.getEy_Phone());
        } else {
            strmResponse.println("Phone");
        }
        strmResponse.println("      </td>");
        strmResponse.println("      <td>"+booking.getBPhone().toUpperCase()+""); 
        strmResponse.println("      </td> ");
        strmResponse.println("      <td id=\"label\">");
        strmResponse.println("          Consignment Number");
        strmResponse.println("      </td>");
        strmResponse.println("      <td>");
        strmResponse.println("    "+booking.getBHAWBNO().toUpperCase()+"");            
        strmResponse.println("    </td>");
        strmResponse.println("    </tr>");
        strmResponse.println("    </table>");
            
        strmResponse.println("</td></tr>");
        strmResponse.println("<tr >");
        strmResponse.println("      <td width=\"50%\">");
        strmResponse.println("          <br> ");
        strmResponse.println("          <table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"99%\">");
        strmResponse.println("              <tr><td id=\"boxtitle\" colspan=\"6\">COLLECTION ADDRESS</td></tr>");
        strmResponse.println("              <tr>");
//        if(ff)
//            strmResponse.println("           <td id=\"label\" width=\"23%\">Name</td>");
//        else
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Name</td>");
        strmResponse.println("                  <td width=\"25%\" >"+srsaddress.getCCompany().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Address 1</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getCCAddress1().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width\"25%\">Address 2</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getCAddress2().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Address 3</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getCAddress3().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td width=\"25%\" id=\"label\">Town</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getCTown().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td width=\"25%\" id=\"label\">County</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getCCounty().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
                          
        strmResponse.println("                  <td width=\"25%\" id=\"label\">Postcode</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getCPostcode().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Country</td>");
        strmResponse.println("                  <td width=\"25%\">"+(country.getCnVector().elementAt(country.getCyVector().indexOf(srsaddress.getCCountry().toUpperCase())).toString())+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Contact</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getCContact().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Phone</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getCPhone().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
                      
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Instructions</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getCInstruct().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        
        // added by Adnan on 29 Aug 2013 against Residential Flag change
        strmResponse.println("              <tr>");
//        strmResponse.println("                  <td id=\"label\" width=\"25%\">Residential Flag</td>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Residential</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getCResidentialFlag().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        
        strmResponse.println("          </table>");
        strmResponse.println("      </td>");
        strmResponse.println("      <td width=\"50%\">");
        strmResponse.println("          <br>");
        strmResponse.println("          <table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"99%\">");
        strmResponse.println("              <tr><td id=\"boxtitle\" colspan=\"6\">DELIVERY ADDRESS</td></tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Name</td>");
        strmResponse.println("                  <td width=\"25%\" >"+srsaddress.getDCompany().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Address 1</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getDAddress1().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Address 2</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getDAddress2().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Address 3</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getDAddress3().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td width=\"25%\" id=\"label\">Town</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getDTown().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td width=\"25%\" id=\"label\">County</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getDCounty().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
                          
        strmResponse.println("                  <td width=\"25%\" id=\"label\">Postcode</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getDPostcode().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Country</td>");
        strmResponse.println("                  <td width=\"25%\">"+(country.getCnVector().elementAt(country.getCyVector().indexOf(srsaddress.getDCountry().toUpperCase())).toString())+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Contact</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getDContact().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Phone</td>");
        strmResponse.println("                 <td width=\"25%\">"+srsaddress.getDPhone().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("     ");         
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Instructions</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getDInstruct().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        
        // added by Adnan on 29 Aug 2013 against Residential Flag change
        strmResponse.println("              <tr>");
//        strmResponse.println("                  <td id=\"label\" width=\"25%\">Residential Flag</td>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Residential</td>");
        strmResponse.println("                  <td width=\"25%\">"+srsaddress.getDResidentialFlag().toUpperCase()+"</td>");
        strmResponse.println("              </tr>");
        
        strmResponse.println("          </table>");
        strmResponse.println("      </td>");
        strmResponse.println("  </tr>");
        strmResponse.println(" <tr>");

         
             
             
        strmResponse.println("<tr id=\"trCollectTime\">");
        strmResponse.println("<td width=\"50%\">");
        strmResponse.println("<br>");
        strmResponse.println("<table id=\"box\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"99%\">");
        strmResponse.println("<tr>");
        strmResponse.println("<td  id=\"boxtitle\" colspan=\"6\">");
        strmResponse.println("CONSIGNMENT DETAILS");
        strmResponse.println("</td></tr>");
        strmResponse.println("<tr><td id=\"label\" width=\"15%\">");
        strmResponse.println("Product Type");
//        if(ff)
//            strmResponse.println("</td><td width=\"23%\">");
//        else
            strmResponse.println("</td><td width=\"25%\">");
        strmResponse.println(booking.getProductType().toUpperCase());
        strmResponse.println("</td></tr>");
        
//         strmResponse.println("<tr><td id=\"label\" width=\"15%\">");
//        strmResponse.println("Parcel Type");
////        if(ff)
////            strmResponse.println("</td><td width=\"23%\">");
////        else
//            strmResponse.println("</td><td width=\"25%\">");
//        
//        if (booking.getsParcelName() != null){
//            strmResponse.println(booking.getsParcelName().toUpperCase());
//        }else{
//            strmResponse.println("SELECT");
//        }
//        strmResponse.println("</td></tr>");
        
        strmResponse.println("<tr><td id=\"label\" width=\"25%\">");
        strmResponse.println("Total No of Pieces");
        strmResponse.println("</td><td width=\"25%\">");
        strmResponse.println(booking.getTotalPieces());
        strmResponse.println("</td></tr>");
        strmResponse.println("<tr><td id=\"label\" width=\"25%\">");
        strmResponse.println("Total Weight");
        strmResponse.println("</td><td width=\"25%\">");
        strmResponse.println(booking.getTotalWeight());
        strmResponse.println("</td></tr>");
        strmResponse.println("<tr><td id=\"label\" width=\"25%\">");
        strmResponse.println("Total Volumetric Weight");
        strmResponse.println("</td><td width=\"25%\">");
        strmResponse.println(booking.getTotalVolWeight());
        strmResponse.println("</td></tr>");
        strmResponse.println("<tr><td width=\"100%\" colspan=\"2\">");
        if(booking.getProductType().equals("NDOX") )//|| booking.getsParcelName()!= null )
        {
            strmResponse.println("<TABLE border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"100%\" align=\"center\">  ");  
            strmResponse.println("    <tr>");
            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:16px\">No Of Items</td>");
//            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:18px\">Parcel Type</td>");
//            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:18px\">Item Weight</td>");
            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:18px\">Length(cm)</td>");
            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:18px\">Width(cm)</td>");
            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:18px\">Height(cm)</td>");
            strmResponse.println("    </tr>");

            try
            {
                Connection conDB = null;
                Statement stmRead;
                ResultSet rstRead;

                String sSql="Select "///*pro_ptype,*/ "
                        + "pro_noItems,pro_length,pro_width,pro_height "
//                        + ", pro_iweight "
                        + "from pro_product where pro_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";

                try
                {

                    conDB = cOpenConnection();
                    stmRead = conDB.createStatement();
                    rstRead = stmRead.executeQuery(sSql);

                    while (rstRead.next())
                    {
                        strmResponse.println("    <tr style=\"text-align:right\">");
                        strmResponse.println("        <td style=\"text-align:center\">"+rstRead.getInt("pro_noItems")+"</td>");
//                        strmResponse.println("        <td style=\"text-align:center\">"+rstRead.getString("pro_ptype")+"</td>");
//                        strmResponse.println("        <td style=\"text-align:center\">"+rstRead.getFloat("pro_iweight")+"</td>");
                        strmResponse.println("        <td style=\"text-align:center\">"+rstRead.getInt("pro_length")+"</td>");
                        strmResponse.println("        <td style=\"text-align:center\">"+rstRead.getInt("pro_width")+"</td>");
                        strmResponse.println("        <td style=\"text-align:center\">"+rstRead.getInt("pro_height")+"</td>");
                        strmResponse.println("    </tr>");

                    }
                } 
                finally
                {
                   conDB.close();
                }
            }
            catch(Exception ex)
            {
                vShowErrorPage(response,ex);
            }

            strmResponse.println("</table>  ");
        }
            strmResponse.println("</td>");
            strmResponse.println("</tr>");
        
       
        strmResponse.println("</td></tr></table>");
        strmResponse.println("</td>");
        strmResponse.println("<td width=\"50%\"><br>");
        if(booking.getProductType().equals("NDOX"))
        {
        strmResponse.println("<table id=\"box\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"99%\">");
        strmResponse.println("<tr>");
        strmResponse.println("<td id=\"boxtitle\" width=\"50%\" colspan=\"5\">");
        strmResponse.println("CONTENTS");
        strmResponse.println("</td></tr>");
        strmResponse.println("<tr><td width=\"100%\" colspan=\"2\">");
        strmResponse.println("<TABLE border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"100%\" align=\"center\">    ");
        strmResponse.println("    <tr>");
        strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:2px;text-align:left\">Commodity</td>");
        strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:2px;text-align:right\">Quantity</td>");
        strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:2px;text-align:right\">Total Value</td>");
        strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:2px;text-align:right\">Indemnity</td>");
        strmResponse.println("    </tr>");
        
        Connection conDB = null;
        Statement stmRead;
        ResultSet rstRead;
           
        String sSql="Select com_description,com_quantity,com_value,com_indemnity from com_commodity where com_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"'";

        try
        {
           
            conDB = cOpenConnection();
            stmRead = conDB.createStatement();
            rstRead = stmRead.executeQuery(sSql);

            while (rstRead.next())
            {
                strmResponse.println("    <tr style=\"text-align:right\">");
                strmResponse.println("        <td style=\"text-align:left\">"+rstRead.getString("com_description").toUpperCase()+"</td>");
                strmResponse.println("        <td style=\"text-align:right\">"+rstRead.getInt("com_quantity")+"</td>");
                strmResponse.println("        <td style=\"text-align:right\">"+rstRead.getDouble("com_value")+"</td>");
                strmResponse.println("        <td style=\"text-align:right\">"+(rstRead.getString("com_indemnity").equals("0")?"NO":"YES")+"</td>");
                strmResponse.println("    </tr>");
            }

             vCloseConnection(conDB);
        }
        catch(Exception ex)
        {
            vShowErrorPage(response,ex);
        }
        
        strmResponse.println("</table>    ");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("</td></tr></table>");
        }
        strmResponse.println("</td></tr>");

          
        strmResponse.println("  <tr>");
        strmResponse.println("      <td>");
        strmResponse.println("          <br>");
        strmResponse.println("          <table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"99%\">");
        strmResponse.println("              <tr><td id=\"boxtitle\" colspan=\"6\">SERVICE DETAILS</td></tr>");
        
        
        if(request.isRequestedSessionIdValid())
        {
            getServicesArray=(String[][])validSession.getAttribute("getServicesArray");
        }

        if(getServicesArray!=null)
        {
            service.parseServices(getServicesArray); 
            
            for(int i=0; i<service.getSvVector().size();i++)
            {
                //if(svcodeVector.elementAt(i).toString().trim().equals("U9"))
                if(ccdetails.getServices().equals(service.getSvcodeVector().elementAt(i)))
                {
                    strmResponse.println("              <tr>");
                    strmResponse.println("                  <td id=\"label\" width=\"25%\">Service Option</td>");
                    strmResponse.println("                  <td width=\"25%\">"+service.getSvVector().elementAt(i)+"</td>");

                    strmResponse.println("              </tr>");
                    strmResponse.println("              <tr> ");
                    strmResponse.println("<td id=\"label\">Collection By<br>Date/Time</td>");
                    strmResponse.println("                  <td >"+service.getCdVector().elementAt(i)+" "+service.getCmVector().elementAt(i)+"</td>");
                    strmResponse.println("              </tr>");
                    strmResponse.println("              <tr>");
                    strmResponse.println("<td id=\"label\">Scheduled Delivery By<br>Date/Time</td>");
                    strmResponse.println("                  <td >"+service.getDdVector().elementAt(i)+" "+service.getDmVector().elementAt(i)+"</td>");
                    strmResponse.println("              </tr>");
                }
            }
        }
        
        if(login.isEmailAccountFlag() || login.isSmsAccountFlag())
        {
            strmResponse.println("              <tr><td id=\"boxtitle\" colspan=\"2\">TRACKING NOTIFICATIONS</td></tr>");
            if(login.isEmailAccountFlag())
                strmResponse.println("              <tr><td id=\"label\" width=\"25%\">Email Notification</td><td >"+getTrackingDetails(request,response,"E","T").toUpperCase()+"</td></tr>");
            strmResponse.println("              <tr><td id=\"label\" width=\"25%\"></td><td></td></tr>");
            if(login.isSmsAccountFlag())
                strmResponse.println("              <tr><td id=\"label\" width=\"25%\">SMS Notification</td><td >"+getTrackingDetails(request,response,"S","T").toUpperCase()+"</td></tr>");
        }

        strmResponse.println("          </table>");
                  
        strmResponse.println("      </td>");
        strmResponse.println("      <td>");
        strmResponse.println("            <br>");
        
        if(login.getCreditCardFlag().equals("F"))
        {
            strmResponse.println("      <table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"99%\">");
            strmResponse.println("          <tr><td id=\"boxtitle\" colspan=\"6\">PAYMENT DETAILS</td></tr>");
            strmResponse.println("          <tr>");
            strmResponse.println("                  <td id=\"label\" width=\"25%\">Credit Card Type</td>");
            strmResponse.println("                  <td width=\"25%\" colspan=\"2\">");
            strmResponse.println("                      "+ccdetails.getCCType().toUpperCase()+"");
            strmResponse.println("                  </td>");
            strmResponse.println("              </tr>");
            strmResponse.println("             <tr>");
            strmResponse.println("                  <td id=\"label\">Credit Card Number</td>");
            strmResponse.println("                  <td colspan=\"2\">"+ccdetails.getCardNo()+"</td>");
            strmResponse.println("              </tr>");
            strmResponse.println("              <tr>");
            strmResponse.println("                  <td id=\"label\">Card Holders Name</td>");
            strmResponse.println("                  <td colspan=\"2\">"+ccdetails.getCCName().toUpperCase()+"</td>");
            strmResponse.println("              </tr>");
            strmResponse.println("              <tr>");
            strmResponse.println("                  <td id=\"label\">Expiry Date (MM/YY)</td>");
            strmResponse.println("                  <td colspan=\"2\">"+ccdetails.getExpiryMonth()+"/"+ccdetails.getExpiryYear()+"");
            strmResponse.println("                  </td>");
            strmResponse.println("              </tr>");
            strmResponse.println("              <tr>");
            strmResponse.println("                  <td id=\"label\">Start Date (Switch/Solo)</td>");
            if(ccdetails.getStartDay().length()>0 && ccdetails.getStartMonth().length()>0)
                strmResponse.println("              <td colspan=\"2\">"+ccdetails.getStartDay()+"/"+ccdetails.getStartMonth()+"");
            else
                strmResponse.println("              <td colspan=\"2\">");

            strmResponse.println("                  </td>");
            strmResponse.println("              </tr>");
            strmResponse.println("              <tr>");
            strmResponse.println("                  <td id=\"label\">Issue Number (Switch/Solo)</td>");
            strmResponse.println("                  <td colspan=\"2\">"+ccdetails.getIssueNo()+"</td>");
            strmResponse.println("              </tr>");

            if(login.isAvsAddressMandatoryFlag())
            {
                strmResponse.println("              <tr>");
                strmResponse.println("                  <td id=\"label\">Address1</td>");
                strmResponse.println("                  <td colspan=\"2\">"+ccdetails.getsAVSAddress1().toUpperCase()+"</td>");
                strmResponse.println("              </tr>");
                strmResponse.println("              <tr>");
                strmResponse.println("                  <td id=\"label\">Address2</td>");
                strmResponse.println("                  <td colspan=\"2\">"+ccdetails.getsAVSAddress2().toUpperCase()+"</td>");
                strmResponse.println("              </tr>");
                strmResponse.println("              <tr>");
                strmResponse.println("                  <td id=\"label\">Address3</td>");
                strmResponse.println("                  <td colspan=\"2\">"+ccdetails.getsAVSAddress3().toUpperCase()+"</td>");
                strmResponse.println("              </tr>");
                strmResponse.println("              <tr>");
                strmResponse.println("                  <td id=\"label\">Address4</td>");
                strmResponse.println("                  <td colspan=\"2\">"+ccdetails.getsAVSAddress4().toUpperCase()+"</td>");
                strmResponse.println("              </tr>");
            }
            
            strmResponse.println("              <tr>");
            strmResponse.println("                  <td id=\"label\">Postcode</td>");
            strmResponse.println("                  <td colspan=\"2\">"+ccdetails.getPostcode().toUpperCase()+"</td>");
            strmResponse.println("              </tr>");
            strmResponse.println("              <tr>");
            strmResponse.println("                  <td id=\"label\">Security Code</td>");
            strmResponse.println("                  <td colspan=\"2\">"+ccdetails.getSecurityCode().toUpperCase()+"</td>");
            strmResponse.println("              </tr>");
            strmResponse.println("              <tr>");
            strmResponse.println("                  <td id=\"label\">Email Address</td>");
            strmResponse.println("                  <td colspan=\"2\">"+ccdetails.getCCEmail().toUpperCase()+"</td>");
            strmResponse.println("              </tr>");
            strmResponse.println("      </table>");
             
        }
        strmResponse.println("      </td>");
        strmResponse.println("  </tr>");

       
        strmResponse.println(" <tr>");
        strmResponse.println("     <td colspan=\"2\">");
        strmResponse.println("         <br>");
        strmResponse.println("        <table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\">");
        strmResponse.println("        <tr>");
        
        if(login.isEmailAccountFlag() || login.isSmsAccountFlag())
            strmResponse.println("            <td style=\"padding-left:15px\"><a href=\"#\" onclick=\"javascript:trackingClick();\">Tracking Notification Details</a></td>");
        else
            strmResponse.println("            <td></td>");
        
        // dc 8/12/2009 - disable confirm button after press
        //strmResponse.println("        <td style=\"text-align:right;padding-right:15px\"><input type=\"button\" id=\"btn\" value=\"Amend Details\" onclick=\"document.AmendForm.submit();\"> &nbsp; &nbsp; &nbsp;<input type=\"button\" value=\"Confirm\" id=\"btn\" onclick=\"document.ConfirmBookingForm.submit();\"></td>");
        strmResponse.println("        <td style=\"text-align:right;padding-right:15px\"><input type=\"button\" id=\"btn\" value=\"Amend Details\" onclick=\"document.AmendForm.submit();\"> &nbsp; &nbsp; &nbsp;<input type=\"button\" value=\"Confirm\" id=\"btn\" onclick=\"this.disabled=true;document.ConfirmBookingForm.submit();\"></td>");


        strmResponse.println("        </tr>");
        strmResponse.println("        </table>		<!-- CONTENT END -->");
        strmResponse.println("    </td>");
        strmResponse.println("</tr>");
        strmResponse.println("</table>");
        
        
        strmResponse.println("<form name=\"AmendForm\" method=\"post\" action=\""+Constants.srvEnterConsignment+"\">");
        strmResponse.println("<input type=hidden name=\"CCompanyName\" value=\""+srsaddress.getCCompany().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"CAddress1\" value=\""+srsaddress.getCCAddress1().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"CAddress2\" value=\""+srsaddress.getCAddress2().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"CAddress3\" value=\""+srsaddress.getCAddress3().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"CTown\" value=\""+srsaddress.getCTown().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"CCounty\" value=\""+srsaddress.getCCounty().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"CPostcode\" value=\""+srsaddress.getCPostcode().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"CCountry\" value=\""+srsaddress.getCCountry().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"CContactName\" value=\""+srsaddress.getCContact().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"CPhone\" value=\""+srsaddress.getCPhone().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"CInstruct\" value=\""+srsaddress.getCInstruct().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"CLatitude\" value=\""+srsaddress.getCLatitude()+"\">");
        strmResponse.println("<input type=hidden name=\"CLongitude\" value=\""+srsaddress.getCLongitude()+"\">");
        strmResponse.println("<input type=hidden name=\"DCompanyName\" value=\""+srsaddress.getDCompany().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"DAddress1\" value=\""+srsaddress.getDAddress1().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"DAddress2\" value=\""+srsaddress.getDAddress2().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"DAddress3\" value=\""+srsaddress.getDAddress3().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"DTown\" value=\""+srsaddress.getDTown().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"DCounty\" value=\""+srsaddress.getDCounty().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"DPostcode\" value=\""+srsaddress.getDPostcode().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"DCountry\" value=\""+srsaddress.getDCountry().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"DContactName\" value=\""+srsaddress.getDContact().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"DPhone\" value=\""+srsaddress.getDPhone().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"DInstruct\" value=\""+srsaddress.getDInstruct().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"DLatitude\" value=\""+srsaddress.getDLatitude()+"\">");
        strmResponse.println("<input type=hidden name=\"DLongitude\" value=\""+srsaddress.getDLongitude()+"\">");

        strmResponse.println("<input type=hidden name=\"BDept\" value=\""+booking.getBDept()+"\">");
        strmResponse.println("<input type=hidden name=\"BCaller\" value=\""+booking.getBCaller()+"\">");
        strmResponse.println("<input type=hidden name=\"BPhone\" value=\""+booking.getBPhone()+"\">");
        strmResponse.println("<input type=hidden name=\"BRef\" value=\""+booking.getBRef()+"\">");
        strmResponse.println("<input type=hidden name=\"BHAWBNO\" value=\""+booking.getBHAWBNO()+"\">");

        strmResponse.println("<input type=hidden name=\"rdCollect\" value=\""+booking.getRdCollect()+"\">");
        strmResponse.println("<input type=hidden name=\"CollectDate\" value=\""+booking.getCollectDate()+"\">");
        strmResponse.println("<input type=hidden name=\"CollectHH\" value=\""+booking.getCollectHH()+"\">");
        strmResponse.println("<input type=hidden name=\"CollectMM\" value=\""+booking.getCollectMM()+"\">");
        strmResponse.println("<input type=hidden name=\"chkPickupBefore\" value=\""+booking.getChkPickupBefore()+"\">");
        strmResponse.println("<input type=hidden name=\"PickupBeforeHH\" value=\""+booking.getPickupBeforeHH()+"\">");
        strmResponse.println("<input type=hidden name=\"PickupBeforeMM\" value=\""+booking.getPickupBeforeMM()+"\">");

//        strmResponse.println("<input type=hidden name=\"ParcelNameId\" value=\""+booking.getsParcelNameId()+"\">");
//        strmResponse.println("<input type=hidden name=\"ParcelName\" value=\""+booking.getsParcelName()+"\">");
        strmResponse.println("<input type=hidden id=\"itemsRowCount\" name=\"itemsRowCount\" value=\"" + booking.getItemsRowCount() + "\">");
        
        strmResponse.println("<input type=hidden name=\"ProductType\" value=\""+booking.getProductType()+"\">");
        strmResponse.println("<input type=hidden name=\"TotalPieces\" value=\""+booking.getTotalPieces()+"\">");
        strmResponse.println("<input type=hidden name=\"TotalWeight\" value=\""+booking.getTotalWeight()+"\">");
        strmResponse.println("<input type=hidden name=\"TotalVolWeight\" value=\""+booking.getTotalVolWeight()+"\">");
        strmResponse.println("</form>");
        
        strmResponse.println("<form name=\"ConfirmBookingForm\" method=\"post\" action=\""+Constants.srvThankYou+"\">");
        strmResponse.println("<input type=\"hidden\" name=\"CCType\" value=\""+ccdetails.getCCType()+"\">");
        strmResponse.println("<input type=\"hidden\" name=\"CCNumber\" value=\""+ccdetails.getCardNo()+"\">");
        strmResponse.println("<input type=\"hidden\" name=\"CCName\" value=\""+ccdetails.getCCName()+"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sServices\" value=\""+ccdetails.getServices()+"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sExpiryMonth\" value=\""+ccdetails.getExpiryMonth()+"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sExpiryYear\" value=\""+ccdetails.getExpiryYear()+"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sStartDay\" value=\""+ccdetails.getStartDay()+"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sStartMonth\" vale=\""+ccdetails.getStartMonth()+"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sIssueNo\" value=\""+ccdetails.getIssueNo()+"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sSecurityCode\" value=\""+ccdetails.getSecurityCode()+"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sAVSAddress1\" value=\"" + ccdetails.getsAVSAddress1() + "\">");
        strmResponse.println("<input type=\"hidden\" name=\"sAVSAddress2\" value=\"" + ccdetails.getsAVSAddress2() + "\">");
        strmResponse.println("<input type=\"hidden\" name=\"sAVSAddress3\" value=\"" + ccdetails.getsAVSAddress3() + "\">");
        strmResponse.println("<input type=\"hidden\" name=\"sAVSAddress4\" value=\"" + ccdetails.getsAVSAddress4() + "\">");
        strmResponse.println("<input type=\"hidden\" name=\"sPostcode\" value=\""+ccdetails.getPostcode()+"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sCCEmail\" value=\""+ccdetails.getCCEmail()+"\">");

        strmResponse.println("<input type=hidden name=\"BDept\" value=\""+booking.getBDept()+"\">");
        strmResponse.println("<input type=hidden name=\"BCaller\" value=\""+booking.getBCaller()+"\">");
        strmResponse.println("<input type=hidden name=\"BPhone\" value=\""+booking.getBPhone()+"\">");
        strmResponse.println("<input type=hidden name=\"BRef\" value=\""+booking.getBRef()+"\">");
        strmResponse.println("<input type=hidden name=\"BHAWBNO\" value=\""+booking.getBHAWBNO()+"\">");

        strmResponse.println("<input type=hidden name=\"rdCollect\" value=\""+booking.getRdCollect()+"\">");
        strmResponse.println("<input type=hidden name=\"CollectDate\" value=\""+booking.getCollectDate()+"\">");
        strmResponse.println("<input type=hidden name=\"CollectHH\" value=\""+booking.getCollectHH()+"\">");
        strmResponse.println("<input type=hidden name=\"CollectMM\" value=\""+booking.getCollectMM()+"\">");
        strmResponse.println("<input type=hidden name=\"chkPickupBefore\" value=\""+booking.getChkPickupBefore()+"\">");
        strmResponse.println("<input type=hidden name=\"PickupBeforeHH\" value=\""+booking.getPickupBeforeHH()+"\">");
        strmResponse.println("<input type=hidden name=\"PickupBeforeMM\" value=\""+booking.getPickupBeforeMM()+"\">");

        strmResponse.println("<input type=hidden name=\"ProductType\" value=\""+booking.getProductType()+"\">");
        strmResponse.println("<input type=hidden name=\"TotalPieces\" value=\""+booking.getTotalPieces()+"\">");
        strmResponse.println("<input type=hidden name=\"TotalWeight\" value=\""+booking.getTotalWeight()+"\">");
        strmResponse.println("<input type=hidden name=\"TotalVolWeight\" value=\""+booking.getTotalVolWeight()+"\">");
        
//        strmResponse.println("<input type=hidden name=\"ParcelNameId\" value=\""+booking.getsParcelNameId()+"\">");
//        strmResponse.println("<input type=hidden name=\"ParcelName\" value=\""+booking.getsParcelName()+"\">");
        strmResponse.println("<input type=hidden id=\"itemsRowCount\" name=\"itemsRowCount\" value=\"" + booking.getItemsRowCount() + "\">");
        strmResponse.println("</form>");
        
        //Tracking Notifications Form 
        strmResponse.println("<form name=\"TrackingForm\" target=\"TrackingForm\" method=\"post\" action=\""+Constants.srvTrackingNotifications+"\">");        
        strmResponse.println("<input type=\"hidden\" name=\"sAction\" value=\""+Constants.sConfirm+"\">");
        strmResponse.println("</form>");
        
        
        strmResponse.println("<script language=javascript>");
        strmResponse.println("function trackingClick()");
        strmResponse.println("{");
        //strmResponse.println("alert('in tracking');");
        strmResponse.println("	 somewin=window.open('blank.html','TrackingForm','resizable=no,scrollbars=yes,width=800,height=500');");
        strmResponse.println("document.TrackingForm.submit();");
        strmResponse.println("}");
        
        strmResponse.println("</script>");
        strmResponse.println("</body>");
        strmResponse.println("</html>");

    }
    
//    public void getSessionAddresses(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException
//    {
//            Connection conDB = null;
//            Statement stmRead;
//            ResultSet rstRead;
//           
//            String sSql="Select * from srs_address where srs_sessionid='"+sSessionID+"'";
//                
//            try
//            {
//                
//                conDB = cOpenConnection();
//                stmRead = conDB.createStatement();
//                rstRead = stmRead.executeQuery(sSql);
//            
//                while (rstRead.next())
//                {
//                    if(rstRead.getBoolean("srs_collection"))
//                    {
//                        sCContact = rstRead.getString("srs_contact");
//                        sCPhone = rstRead.getString("srs_phone");
//                        sCCompany = rstRead.getString("srs_company");
//                        sCAddress1 = rstRead.getString("srs_address1");
//                        sCAddress2=rstRead.getString("srs_address2");
//                        sCAddress3=rstRead.getString("srs_address3");
//                        sCTown = rstRead.getString("srs_town");
//                        sCCounty=rstRead.getString("srs_county");
//                        sCPostcode = rstRead.getString("srs_postcode");
//                        sCCountry = rstRead.getString("srs_country");
//                        sCInstruct=rstRead.getString("srs_instructions");
//                    }
//                    else
//                    {
//                        sDContact = rstRead.getString("srs_contact");
//                        sDPhone = rstRead.getString("srs_phone");
//                        sDCompany = rstRead.getString("srs_company");
//                        sDAddress1 = rstRead.getString("srs_address1");
//                        sDAddress2=rstRead.getString("srs_address2");
//                        sDAddress3=rstRead.getString("srs_address3");
//                        sDTown = rstRead.getString("srs_town");
//                        sDCounty=rstRead.getString("srs_county");
//                        sDPostcode = rstRead.getString("srs_postcode");
//                        sDCountry = rstRead.getString("srs_country");
//                        sDInstruct=rstRead.getString("srs_instructions");
//                    }
//                }
//            }
//            catch(Exception ex)
//            {
//                vShowErrorPage(response,ex);
//            }
//    }
    
    
      
    /**
     *
     * @param request
     * @param response
     * @param type
     * @param action
     * @return
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public String getTrackingDetails(HttpServletRequest request,HttpServletResponse response,String type,String action) throws ServletException, IOException
    {
        String[][] sTrackingEmail1=new String[5][5];
        String[][] sTrackingSMS1=new String[5][5];
        String[][] sEmpty=new String[1][1];
        sEmpty[0][0]="";
        String sTrackingEmail="",sTrackingSMS="",sBTrackingEmail="",sBTrackingSMS="";
        //sEmpty[0][1]="";
        String result="";
        
        String temp="";
        try
        {
            HttpSession validSession=request.getSession(false);
            
            if(validSession==null)
            {
            }
            else
            {
                
                if(validSession.getAttribute("sTrackingEmail")!=null)
                {
                    sTrackingEmail1=(String[][])validSession.getAttribute("sTrackingEmail");
                    
                    for(int i=0;i<5;i++)
                    {
                        temp=sTrackingEmail1[i][0]==null?"":sTrackingEmail1[i][0];
                        if(temp.length()>0)
                        {
                            sTrackingEmail = sTrackingEmail + temp + ",<br>";
                            sBTrackingEmail = sBTrackingEmail + "&EM=" + temp + "|" + sTrackingEmail1[i][1];
                        }
                    }
                    
                     if(sTrackingEmail.length()>0 && sTrackingEmail.lastIndexOf(',')!=-1)
                        sTrackingEmail=sTrackingEmail.substring(0,sTrackingEmail.lastIndexOf(','))+sTrackingEmail.substring(sTrackingEmail.lastIndexOf(',')+1,sTrackingEmail.length());
                }
                
                if(validSession.getAttribute("sTrackingSMS")!=null)
                {
                    sTrackingSMS1=(String[][])validSession.getAttribute("sTrackingSMS");
                    
                    for(int i=0;i<5;i++)
                    {
                        temp=sTrackingSMS1[i][0]==null?"":sTrackingSMS1[i][0];
                        if(temp.length()>0)
                        {
                            sTrackingSMS = sTrackingSMS + temp + ",<br>";
                            sBTrackingSMS = sBTrackingSMS + "&SM=" + temp + "|" + sTrackingSMS1[i][1];
                        }
                    }
                    
                    if(sTrackingSMS.length()>0 && sTrackingSMS.lastIndexOf(',')!=-1)
                        sTrackingSMS=sTrackingSMS.substring(0,sTrackingSMS.lastIndexOf(','))+sTrackingSMS.substring(sTrackingSMS.lastIndexOf(',')+1,sTrackingSMS.length());
                }
                
            }
        }
        catch(Exception ex)
        {
            vShowErrorPage(response,ex);
        }
        
         if(type.equals("E") && action.equals("T"))
            result=sTrackingEmail;
         if(type.equals("S") && action.equals("T"))
            result=sTrackingSMS;
         if(type.equals("E") && action.equals("B"))
            result=sBTrackingEmail;
         if(type.equals("S") && action.equals("B"))
            result=sBTrackingSMS;
        
        return result;
     }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
//        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     * @return
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
