package citysprint.co.uk.NDI;

/*
 * DeliveryAddress.java
 *
 * Created on 27 March 2008, 15:31
 */

import java.beans.*;
import java.io.Serializable;
import javax.servlet.http.*;
/**
 * @author ramyas
 */
public class DeliveryAddress extends Object implements Serializable {
    
    /**
     *
     */
    public static final String PROP_SAMPLE_PROPERTY = "sampleProperty";
    
    private String sDCompany="",sDContact="",sDAddress1="",sDAddress2="",sDAddress3="",sDTown="",sDPostcode="",sDPhone="",sDCounty="",sDInstruct="",sDCountry="";
    private String sDLatitude="",sDLongitude="", sDResidentialFlag = "N";

    public String getDResidentialFlag() {
        return sDResidentialFlag;
    }
    private PropertyChangeSupport propertySupport;
    
    /**
     *
     */
    public DeliveryAddress() {
        propertySupport = new PropertyChangeSupport(this);
    }
    
   //Company Name
    /**
     *
     * @return
     */
    public String getDCompany() {
         return sDCompany;
    }
    
    //Contact
    /**
     *
     * @return
     */
    public String getDContact() {
         return sDContact;
    }
    
    //Address Line 1
    /**
     *
     * @return
     */
    public String getDAddress1() {
         return sDAddress1;
    }
    
    //Address Line 2
    /**
     *
     * @return
     */
    public String getDAddress2() {
         return sDAddress2;
    }
    
    //Address Line 3
    /**
     *
     * @return
     */
    public String getDAddress3() {
         return sDAddress3;
    }
    
    //Town
    /**
     *
     * @return
     */
    public String getDTown() {
         return sDTown;
    }
    
    //County
    /**
     *
     * @return
     */
    public String getDCounty() {
         return sDCounty;
    }
    
    //Postcode
    /**
     *
     * @return
     */
    public String getDPostcode() {
         return sDPostcode;
    }
    
     //Country
    /**
     *
     * @return
     */
    public String getDCountry() {
         return sDCountry;
    }
    
    //Instruct
    /**
     *
     * @return
     */
    public String getDInstruct() {
         return sDInstruct;
    }

     //Instruct
    /**
     *
     * @param instruct
     */
    public void setDInstruct(String instruct) {
         this.sDInstruct=instruct;
    }

    //Phone
    /**
     *
     * @return
     */
    public String getDPhone() {
         return sDPhone;
    }

     //Latitude
    /**
     *
     * @return
     */
    public String getDLatitude() {
         return sDLatitude;
    }

    //Longitude
    /**
     *
     * @return
     */
    public String getDLongitude() {
         return sDLongitude;
    }

    /**
     *
     * @param request
     * populates delivery address feilds
     */
    public void setDeliveryAddress(HttpServletRequest request)
    {
        sDCompany=request.getParameter("DCompanyName")==null?"":request.getParameter("DCompanyName").toString();
        sDAddress1=request.getParameter("DAddress1")==null?"":request.getParameter("DAddress1").toString();
        sDAddress2=request.getParameter("DAddress2")==null?"":request.getParameter("DAddress2").toString();
        sDAddress3=request.getParameter("DAddress3")==null?"":request.getParameter("DAddress3").toString();
        sDTown=request.getParameter("DTown")==null?"":request.getParameter("DTown").toString();
        sDCounty=request.getParameter("DCounty")==null?"":request.getParameter("DCounty").toString();
        sDPostcode=request.getParameter("DPostcode")==null?"":request.getParameter("DPostcode").toString();
        sDCountry=request.getParameter("DCountry")==null?"GBR":request.getParameter("DCountry").toString();
        sDContact=request.getParameter("DContactName")==null?"":request.getParameter("DContactName").toString();
        sDPhone=request.getParameter("DPhone")==null?"":request.getParameter("DPhone").toString();
        sDInstruct=request.getParameter("DInstruct")==null?"":request.getParameter("DInstruct").toString();
        sDLatitude=request.getParameter("DLatitude")==null?"":request.getParameter("DLatitude").toString();
        sDLongitude=request.getParameter("DLongitude")==null?"":request.getParameter("DLongitude").toString();
        
        sDResidentialFlag =request.getParameter("DResidentialFlag")==null?"":request.getParameter("DResidentialFlag").toString();
    }
    
}
