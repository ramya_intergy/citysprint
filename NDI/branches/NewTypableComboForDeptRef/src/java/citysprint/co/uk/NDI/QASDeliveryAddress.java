/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citysprint.co.uk.NDI;

/**
 *
 * @author ramyas
 */
public class QASDeliveryAddress {

    String sDCompany = "", sDContact = "", sDAddress1 = "", sDAddress2 = "", sDTown = "", sDPostcode = "", sDCounty = "", sDLongitude = "", sDLatitude = "", sDTownKey = "";

    public String getSDAddress1() {
        return sDAddress1;
    }

    public void setSDAddress1(String sDAddress1) {
        this.sDAddress1 = sDAddress1;
    }

    public String getSDAddress2() {
        return sDAddress2;
    }

    public void setSDAddress2(String sDAddress2) {
        this.sDAddress2 = sDAddress2;
    }

    public String getSDCompany() {
        return sDCompany;
    }

    public void setSDCompany(String sDCompany) {
        this.sDCompany = sDCompany;
    }

    public String getSDContact() {
        return sDContact;
    }

    public void setSDContact(String sDContact) {
        this.sDContact = sDContact;
    }

    public String getSDCounty() {
        return sDCounty;
    }

    public void setSDCounty(String sDCounty) {
        this.sDCounty = sDCounty;
    }

    public String getSDLatitude() {
        return sDLatitude;
    }

    public void setSDLatitude(String sDLatitude) {
        this.sDLatitude = sDLatitude;
    }

    public String getSDLongitude() {
        return sDLongitude;
    }

    public void setSDLongitude(String sDLongitude) {
        this.sDLongitude = sDLongitude;
    }

    public String getSDPostcode() {
        return sDPostcode;
    }

    public void setSDPostcode(String sDPostcode) {
        this.sDPostcode = sDPostcode;
    }

    public String getSDTown() {
        return sDTown;
    }

    public void setSDTown(String sDTown) {
        this.sDTown = sDTown;
    }

    public String getSDTownKey() {
        return sDTownKey;
    }

    public void setSDTownKey(String sDTownKey) {
        this.sDTownKey = sDTownKey;
    }
}
