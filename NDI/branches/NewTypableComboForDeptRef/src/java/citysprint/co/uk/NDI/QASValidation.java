package citysprint.co.uk.NDI;

/*
 * QASValidation.java
 *
 * Created on 6 September 2007, 15:03
 */
import citysprint.co.uk.NDI.CollectionAddress;
import citysprint.co.uk.NDI.DeliveryAddress;
import citysprint.co.uk.NDI.CommodityInfo;
import citysprint.co.uk.NDI.LoginInfo;
import citysprint.co.uk.NDI.TownKeyInfo;
import citysprint.co.uk.NDI.BookingInfo;
import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.qas.proweb.*;
import com.qas.proweb.servlet.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.sql.*;

/**
 *
 * @author ramyas
 * @version
 */
public class QASValidation extends CommonClass {

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        //sServiceAPIError = "";


        HttpSession validSession = request.getSession(false);
        //read session values
        //getSessionValues(request);

        try {
            if (isLogin(request)) {
                //initialize config values
                initializeConfigValues(request, response);

                String sAction = request.getParameter(Constants.SACTION) == null ? "" : request.getParameter(Constants.SACTION);

                LoginInfo login = new LoginInfo();
                CollectionAddress collection = new CollectionAddress();
                DeliveryAddress delivery = new DeliveryAddress();
                BookingInfo booking = new BookingInfo();

                //Read request parameters
                login.parseLogin((String[][]) validSession.getAttribute("loginArray"), request);
                collection.setCollectionAddress(request);
                delivery.setDeliveryAddress(request);
                booking.setBookingInfo(request, response, login);


                if (sAction.equals("Booking")) {
                    LoadPage(request, response, booking, collection, delivery);
                } else {
                    Confirm(request, response, booking, collection, delivery);
                }
            } else {
                writeOutputLog("Session expired redirecting to index page");
                response.sendRedirect("index.jsp");
            }
        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param booking
     * @param collection
     * @param delivery
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void LoadPage(HttpServletRequest request, HttpServletResponse response,
            BookingInfo booking, CollectionAddress collection, DeliveryAddress delivery) throws ServletException, IOException {

        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");

        String result = "";
        String[][] getTownArray = null;
        String RouteInfo = "";
        String RouteInfo1 = "";
        String ErrorInfo = "";
        String ErrorInfo1 = "";
        String[] asMonikers = null;
        String[] asLines = null;
        String[] asLabels = null;
        String[] asText = null;
        String[] asPostcodes = null;
        String[] asMonikers1 = null;
        String[] asLines1 = null;
        String[] asLabels1 = null;
        String[] asText1 = null;
        String[] asPostcodes1 = null;
        String[] asUserInput = null;
        String[] asUserInput1 = null;
        String[] asAddress = null;
        String[] asAddress1 = null;
        String sPage = "";
        String VerifyInfo = "";
        String VerifyInfo1 = "";

        String sMessage = "";
        String sMessage1 = "", sAction = "";
        String sItemTags = "";
        String sCommodityTags = "";
        String sTownKeyError = "";

        HttpSession validSession = request.getSession(false);

        sAction = request.getParameter(Constants.SACTION) == null ? "" : request.getParameter(Constants.SACTION);

        //Inialising QAS constants
        String sEndpoint = getServletContext().getInitParameter("QasEndpoint");
        String sLayout = getServletContext().getInitParameter("QasLayout");

        Constants.ENDPOINT = sEndpoint;
        Constants.LAYOUT = sLayout;

        //Initialise QASAddress Objects
        QASCollectionAddress qCol = new QASCollectionAddress();
        QASDeliveryAddress qDel = new QASDeliveryAddress();

        TownKeyInfo tk = new TownKeyInfo();

        if (sAction.equals("Booking")) {

            try {
                //Validate the collection address first.
                // The helper extract the Command name from the request and creates the relevant object.
                Command cmd = HttpHelper.getCommand(request);

                if (collection.getCLatitude().equals("") || collection.getCLongitude().equals("")) {

                    // perform custom operation
                    sPage = cmd.execute(request, response);

                    //retrieve all collection address validation details
                    asUserInput = (String[]) request.getAttribute(Constants.USER_INPUT);
                    //if result is a single address asLines would be returned
                    asLines = (String[]) request.getAttribute(Constants.LINES);
                    asLabels = (String[]) request.getAttribute(Constants.LABELS);
                    //if result is a picklist then asText,asPostcodes and asMonikers would be returned
                    asText = (String[]) request.getAttribute(Constants.PICK_TEXTS);
                    asPostcodes = (String[]) request.getAttribute(Constants.PICK_POSTCODES);
                    asMonikers = (String[]) request.getAttribute(Constants.PICK_MONIKERS);
                    //if result is verified exactly then asAddress would be returned.
                    asAddress = (String[]) request.getAttribute(Constants.ADDRESS);
                    VerifyInfo = request.getAttribute(Constants.VERIFY_INFO) == null ? "None" : (String) request.getAttribute(Constants.VERIFY_INFO);
                    RouteInfo = (String) request.getAttribute(Constants.ROUTE);
                    ErrorInfo = request.getAttribute(Constants.ERROR_INFO) == null ? "" : (String) request.getAttribute(Constants.ERROR_INFO);
                    qCol.setSCTownKey("0");
                } else {
                    //If UK address and already validated
                    asAddress = new String[9];
                    asAddress[0] = collection.getCCompany();
                    asAddress[1] = collection.getCCAddress1();
                    asAddress[2] = collection.getCAddress2();
                    asAddress[3] = collection.getCTown();
                    asAddress[4] = collection.getCCounty();
                    asAddress[5] = collection.getCPostcode();
                    asAddress[6] = "";
                    asAddress[7] = collection.getCLatitude();
                    asAddress[8] = collection.getCLongitude();

                    asLabels = new String[9];
                    asLabels[0] = "Organisation";
                    asLabels[1] = "";
                    asLabels[2] = "";
                    asLabels[3] = "Town";
                    asLabels[4] = "County";
                    asLabels[5] = "Postcode";
                    asLabels[6] = "";
                    asLabels[7] = "100m Latitude";
                    asLabels[8] = "100m Longitude";
                    VerifyInfo = SearchResult.Verified;
                    qCol.setSCTownKey("0");
                }

                if (delivery.getDCountry().equals(Constants.COUNTRY_CODE)) {

                    if (delivery.getDLatitude().equals("") || delivery.getDLongitude().equals("")) {
                        //Validate the delivery address and retrieve the validation details.
                        request.setAttribute(Constants.STYPE, Constants.USER_INPUT1);
                        sPage = cmd.execute(request, response);

                        asUserInput1 = (String[]) request.getAttribute(Constants.USER_INPUT);
                        //if result is a single address asLines would be returned
                        asLines1 = (String[]) request.getAttribute(Constants.LINES);
                        asLabels1 = (String[]) request.getAttribute(Constants.LABELS);
                        //if result is a picklist then asText,asPostcodes and asMonikers would be returned
                        asText1 = (String[]) request.getAttribute(Constants.PICK_TEXTS);
                        asPostcodes1 = (String[]) request.getAttribute(Constants.PICK_POSTCODES);
                        asMonikers1 = (String[]) request.getAttribute(Constants.PICK_MONIKERS);
                        //if result is verified exactly then asAddress would be returned.
                        asAddress1 = (String[]) request.getAttribute(Constants.ADDRESS);
                        VerifyInfo1 = request.getAttribute(Constants.VERIFY_INFO) == null ? "None" : (String) request.getAttribute(Constants.VERIFY_INFO);
                        RouteInfo1 = (String) request.getAttribute(Constants.ROUTE);
                        ErrorInfo1 = request.getAttribute(Constants.ERROR_INFO) == null ? "" : (String) request.getAttribute(Constants.ERROR_INFO);
                        //IF country is UK TownKey should be set to zero
                        qDel.setSDTownKey("0");
                    } else {
                        //IF UK address and already validated
                        asAddress1 = new String[9];
                        asAddress1[0] = delivery.getDCompany();
                        asAddress1[1] = delivery.getDAddress1();
                        asAddress1[2] = delivery.getDAddress2();
                        asAddress1[3] = delivery.getDTown();
                        asAddress1[4] = delivery.getDCounty();
                        asAddress1[5] = delivery.getDPostcode();
                        asAddress1[6] = "";
                        asAddress1[7] = delivery.getDLatitude();
                        asAddress1[8] = delivery.getDLongitude();
                        VerifyInfo1 = SearchResult.Verified;
                        //IF country is UK TownKey should be set to zero
                        qDel.setSDTownKey("0");

                        asLabels1 = new String[9];
                        asLabels1[0] = "Organisation";
                        asLabels1[1] = "";
                        asLabels1[2] = "";
                        asLabels1[3] = "Town";
                        asLabels1[4] = "County";
                        asLabels1[5] = "Postcode";
                        asLabels1[6] = "";
                        asLabels1[7] = "100m Latitude";
                        asLabels1[8] = "100m Longitude";
                    }
                } else {

                    getTownArray = getTownKey(request, response, delivery);

                    if (getTownArray != null) {

                        tk = parseValidTown(getTownArray);
                        qDel.setSDTownKey(tk.getSTownKey());

                        if (qDel.getSDTownKey().length() != 0) {
                            VerifyInfo1 = SearchResult.Verified;
                        } else {
                            VerifyInfo1 = SearchResult.None;
                        }
                    } else {
                        VerifyInfo1 = SearchResult.None;
                    }

                    if (VerifyInfo1.equals(SearchResult.Verified)) {
                        asAddress1 = new String[9];
                        asAddress1[0] = delivery.getDCompany();
                        asAddress1[1] = delivery.getDAddress1();
                        asAddress1[2] = delivery.getDAddress2();
                        asAddress1[3] = delivery.getDTown();
                        asAddress1[4] = delivery.getDCounty();
                        asAddress1[5] = delivery.getDPostcode();
                        asAddress1[6] = "";
                        asAddress1[7] = "";
                        asAddress1[8] = "";
                    } else {
                        asUserInput1 = new String[9];
                        asUserInput1[0] = delivery.getDCompany();
                        asUserInput1[1] = delivery.getDAddress1();
                        asUserInput1[2] = delivery.getDAddress2();
                        asUserInput1[3] = delivery.getDTown();
                        asUserInput1[4] = delivery.getDCounty();
                        asUserInput1[5] = delivery.getDPostcode();
                        asUserInput1[6] = "";
                        asUserInput1[7] = "";
                        asUserInput1[8] = "";
                        if (tk.getSTownKeyError().equals("") && qDel.getSDTownKey().equals("0")) {
                            ErrorInfo1 = "Provided Town and Country combination is not valid";
                        } else {
                            ErrorInfo1 = sTownKeyError;
                        }
                    }

                    asLabels1 = new String[9];
                    asLabels1[0] = "Organisation";
                    asLabels1[1] = "";
                    asLabels1[2] = "";
                    asLabels1[3] = "Town";
                    asLabels1[4] = "County";
                    asLabels1[5] = "Postcode";
                    asLabels1[6] = "";
                    asLabels1[7] = "100m Latitude";
                    asLabels1[8] = "100m Longitude";

                }

            } catch (Exception e) {
                vShowErrorPage(response, e);
            }
        }

        strmResponse.println("<html>");
        strmResponse.println("<head>");
        strmResponse.println("<title>CitySprint - Address Validation</title>");
        strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
        strmResponse.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
        strmResponse.println("</head>");
        strmResponse.println("<body style=\"width:75px;height:75px;\">");
        strmResponse.println("<br>");

        if (ff) {
            strmResponse.println("<table width=\"100%\"><tr><td>");
        }

        if (VerifyInfo.equals(SearchResult.Verified) && VerifyInfo1.equals(SearchResult.Verified)) {


            try {

                readArray(asAddress, asLabels, qCol);
                readArray(asAddress1, asLabels1, qDel);

                strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");
                strmResponse.println("var txtCCompany = window.opener.document.getElementById(\"txtCCompany\");");
                strmResponse.println("var txtCAddress1 = window.opener.document.getElementById(\"txtCAddress1\");");
                strmResponse.println("var txtCAddress2 = window.opener.document.getElementById(\"txtCAddress2\");");
                strmResponse.println("var txtCTown = window.opener.document.getElementById(\"txtCTown\");");
                strmResponse.println("var txtCCounty = window.opener.document.getElementById(\"txtCCounty\");");
                strmResponse.println("var txtCPostcode = window.opener.document.getElementById(\"txtCPostcode\");");
                strmResponse.println("var txtCLatitude = window.opener.document.getElementById(\"txtCLatitude\");");
                strmResponse.println("var txtCLongitude = window.opener.document.getElementById(\"txtCLongitude\");");
                strmResponse.println("var txtDCompany = window.opener.document.getElementById(\"txtDCompany\");");
                strmResponse.println("var txtDAddress1 = window.opener.document.getElementById(\"txtDAddress1\");");
                strmResponse.println("var txtDAddress2 = window.opener.document.getElementById(\"txtDAddress2\");");
                strmResponse.println("var txtDTown = window.opener.document.getElementById(\"txtDTown\");");
                strmResponse.println("var txtDCounty = window.opener.document.getElementById(\"txtDCounty\");");
                strmResponse.println("var txtDPostcode = window.opener.document.getElementById(\"txtDPostcode\");");
                strmResponse.println("var txtDLatitude = window.opener.document.getElementById(\"txtDLatitude\");");
                strmResponse.println("var txtDLongitude = window.opener.document.getElementById(\"txtDLongitude\");");

                strmResponse.println("txtCCompany.value = \"" + qCol.getSCCompany() + "\";");
                strmResponse.println("txtCAddress1.value = \"" + qCol.getSCAddress1() + "\";");
                strmResponse.println("txtCAddress2.value = \"" + qCol.getSCAddress2() + "\";");
                strmResponse.println("txtCTown.value = \"" + qCol.getSCTown() + "\";");

                strmResponse.println("txtCCounty.value = \"" + qCol.getSCCounty() + "\";");
                strmResponse.println("txtCPostcode.value = \"" + qCol.getSCPostcode() + "\";");

                strmResponse.println("txtCLatitude.value = \"" + qCol.getSCLatitude() + "\";");
                strmResponse.println("txtCLongitude.value = \"" + qCol.getSCLongitude() + "\";");
                strmResponse.println("txtDCompany.value = \"" + qDel.getSDCompany() + "\";");
                strmResponse.println("txtDAddress1.value = \"" + qDel.getSDAddress1() + "\";");
                strmResponse.println("txtDAddress2.value = \"" + qDel.getSDAddress2() + "\";");
                strmResponse.println("txtDTown.value = \"" + qDel.getSDTown() + "\";");
                strmResponse.println("txtDCounty.value = \"" + qDel.getSDCounty() + "\";");
                strmResponse.println("txtDPostcode.value = \"" + qDel.getSDPostcode() + "\";");
                strmResponse.println("txtDLatitude.value = \"" + qDel.getSDLatitude() + "\";");
                strmResponse.println("txtDLongitude.value = \"" + qDel.getSDLongitude() + "\";");
                strmResponse.println("</script>");

                //write the confirmed addresses into the session addresses table for later retrieval.
                //delete any addresses in the srs_address table for this account and session
                deleteSessionAddress(request, response);
                writeSessionAddress(validSession.getAttribute("acc_id").toString(), validSession.getAttribute("sessionid").toString(), (qCol.getSCCompany().length()>60?qCol.getSCCompany().substring(0,60):qCol.getSCCompany()), qCol.getSCAddress1(), qCol.getSCAddress2(), collection.getCAddress3(), qCol.getSCTown(), qCol.getSCCounty(), qCol.getSCPostcode(), collection.getCCountry(), collection.getCContact(), collection.getCPhone(), collection.getCInstruct(), qCol.getSCLongitude(), qCol.getSCLatitude(), "C", qCol.getSCTownKey(), collection.getCResidentialFlag());
                writeSessionAddress(validSession.getAttribute("acc_id").toString(), validSession.getAttribute("sessionid").toString(), (qDel.getSDCompany().length()>60?qDel.getSDCompany().substring(0,60):qDel.getSDCompany()), qDel.getSDAddress1(), qDel.getSDAddress2(), delivery.getDAddress3(), qDel.getSDTown(), qDel.getSDCounty(), qDel.getSDPostcode(), delivery.getDCountry(), delivery.getDContact(), delivery.getDPhone(), delivery.getDInstruct(), qDel.getSDLongitude(), qDel.getSDLatitude(), "D", qDel.getSDTownKey(), delivery.getDResidentialFlag());

                if (booking.getProductType().equals("NDOX")) {

                    sItemTags = request.getParameter("itemTags") == null ? "" : request.getParameter("itemTags").toString();
                    sCommodityTags = request.getParameter("commodityTags") == null ? "" : request.getParameter("commodityTags").toString();

                    if (sItemTags.length() > 1 && !sItemTags.equals(validSession.getAttribute("itemTags"))) {
                        validSession.setAttribute("itemTags", sItemTags);
                    } else {
                        validSession.removeAttribute("itemTags");
                    }


                    if (sCommodityTags.length() > 1 && !sCommodityTags.equals(validSession.getAttribute("commodityTags"))) {
                        validSession.setAttribute("commodityTags", sCommodityTags);
                    } else {
                        validSession.removeAttribute("commodityTags");
                    }
//
//                    //insert Item and Commodity Grid rows into database
                    sItemTags = validSession.getAttribute("itemTags") == null ? "" : validSession.getAttribute("itemTags").toString();
                    sCommodityTags = validSession.getAttribute("commodityTags") == null ? "" : validSession.getAttribute("commodityTags").toString();

                    if (sItemTags.length() > 0) {
                        addItemGridRow(sItemTags, validSession, response);
                    }

                    if (sCommodityTags.length() > 0) {
                        result = addCommodityGridRow(sCommodityTags, validSession, response);
                    }
                }
                
//                sItemTags = request.getParameter("itemTags") == null ? "" : request.getParameter("itemTags").toString();
//                if (sItemTags.length() > 1 && !sItemTags.equals(validSession.getAttribute("itemTags"))) {
//                        validSession.setAttribute("itemTags", sItemTags);
//                    } else {
//                        validSession.removeAttribute("itemTags");
//                    }
//
//                
//                    //insert Item and Commodity Grid rows into database
//                    sItemTags = validSession.getAttribute("itemTags") == null ? "" : validSession.getAttribute("itemTags").toString();
//                    
//                    if (sItemTags.length() > 0) {
//                        addItemGridRow(sItemTags, validSession, response);
//                    }
                    
                if (result.length() == 0) {
                    //get valid services for user requirement
                    result = getServices(request, response, booking, collection, delivery, qCol, qDel);
                }

            } catch (Exception ex) {
                vShowErrorPage(response, ex);
            }

            if (result.length() == 0) {
                strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");
                strmResponse.println("opener.Services('" + SearchResult.Verified + "');");
                strmResponse.println("window.close();");
                strmResponse.println("</script>");
            } else {
                strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");
                strmResponse.println("var lblMessage = window.opener.document.getElementById(\"lblMessage\");");
                //strmResponse.println("var selDept = window.opener.document.getElementById(\"selDept\");");
                if (ff) {
                    strmResponse.println("lblMessage.textContent=\"" + result + "\"");
                } else {
                    strmResponse.println("lblMessage.innerText=\"" + result + "\"");
                }
                strmResponse.println("opener.Services('Error');");
                strmResponse.println("window.close();");
                strmResponse.println("</script>");
            }

        } else {
            if (VerifyInfo.equals(SearchResult.None)) {
                readArray(asUserInput, asLabels, qCol);
            }

            if (VerifyInfo.equals(SearchResult.Verified)) {
                readArray(asAddress, asLabels, qCol);
            }


            if (VerifyInfo.equals(SearchResult.InteractionRequired)) {
                readArray(asLines, asLabels, qCol);
            }

            if (VerifyInfo1.equals(SearchResult.None)) {
                readArray(asUserInput1, asLabels1, qDel);
            }

            if (VerifyInfo1.equals(SearchResult.Verified)) {
                readArray(asAddress1, asLabels1, qDel);
            }


            if (VerifyInfo1.equals(SearchResult.InteractionRequired)) {
                readArray(asLines1, asLabels1, qDel);
            }

            if (VerifyInfo.equals(SearchResult.InteractionRequired) || VerifyInfo.equals(SearchResult.Verified)) {
                sMessage = "Your collection address has been validated.";
            } else if (VerifyInfo.equals(SearchResult.Multiple) || VerifyInfo.equals(SearchResult.PremisesPartial) || VerifyInfo.equals(SearchResult.StreetPartial)) {
                sMessage = "Please select a collection address from the list.";
            } else if (VerifyInfo.equals(SearchResult.None)) {
                sMessage = "Your collection address cannot be validated (" + ErrorInfo + "). Please modify the address fields and try again.";
            }

            if (VerifyInfo1.equals(SearchResult.InteractionRequired) || VerifyInfo1.equals(SearchResult.Verified)) {
                sMessage1 = "Your delivery address has been validated.";
            } else if (VerifyInfo1.equals(SearchResult.Multiple) || VerifyInfo1.equals(SearchResult.PremisesPartial) || VerifyInfo1.equals(SearchResult.StreetPartial)) {
                sMessage1 = "Please select a delivery address from the list.";
            } else if (VerifyInfo1.equals(SearchResult.None)) {
                sMessage1 = "Your delivery address cannot be validated (" + ErrorInfo1 + "). Please modify the address fields and try again.";
            }

            if (!VerifyInfo.equals(SearchResult.None) && !VerifyInfo1.equals(SearchResult.None)) {
                sMessage = sMessage + " Please select the address and click Confirm";
                sMessage1 = sMessage1 + " Please select the address and click Confirm";
            }


            if (sMessage == null) {
                strmResponse.println("<br>");
            } else {
                strmResponse.println("&nbsp;&nbsp;&nbsp;&nbsp;");

                if (ff) {
                    strmResponse.println("<table><tr><td width=\"100%\">");
                }

                strmResponse.println("<label id=\"lblMessage\" style=\"color:green;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;\" width=\"100%\">" + sMessage + "</label><br>");
                strmResponse.println("<br>");

                if (ff) {
                    strmResponse.println("</td></tr></table>");
                }
            }


            strmResponse.println("<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"95%\" align=\"center\">");
            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"boxtitle\">FROM</td></tr>");
            strmResponse.println("<tr><td>");
            strmResponse.println("<select name=\"selCollection\" id=\"selCollection\" size=10 width=\"95%\">");


            if (VerifyInfo.equals(SearchResult.InteractionRequired) || VerifyInfo.equals(SearchResult.Verified)) {
                strmResponse.println("<option value=" + SearchResult.Verified + ">" + getDisplayAddress(qCol.getSCCompany(), qCol.getSCAddress1(), qCol.getSCAddress2(), qCol.getSCTown(), qCol.getSCCounty(), qCol.getSCPostcode()) + "</option>");
            }

            if (VerifyInfo.equals(SearchResult.Multiple) || VerifyInfo.equals(SearchResult.PremisesPartial) || VerifyInfo.equals(SearchResult.StreetPartial)) {
                //if picklist is available for search results
                int size;

                if (asText.length > 10) {
                    size = 10;
                } else {
                    size = asText.length;
                }

                for (int i = 0; i < size; i++) {
                    strmResponse.println("<option value=" + asMonikers[i] + ">" + asText[i] + "," + asPostcodes[i] + "</option>");
                }
            }
            strmResponse.println("</select>");
            strmResponse.println("</td></tr>");
            strmResponse.println("</table>");

            strmResponse.println("<br><br>");

            if (sMessage1 == null) {
                strmResponse.println("<br>");
            } else {
                strmResponse.println("&nbsp;&nbsp;&nbsp;&nbsp;");

                if (ff) {
                    strmResponse.println("<table><tr><td width=\"100%\">");
                }

                strmResponse.println("<label id=\"lblMessage1\" style=\"color:green;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;\" width=\"100%\">" + sMessage1 + "</label><br>");
                strmResponse.println("<br>");

                if (ff) {
                    strmResponse.println("</td></tr></table>");
                }
            }


            strmResponse.println("<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"95%\" align=\"center\">");
            strmResponse.println("<tr><td id=\"boxtitle\">TO</td></tr>");
            strmResponse.println("<tr><td>");
            strmResponse.println("<select name=\"selDelivery\" id=\"selDelivery\" size=10 width=\"95%\">");


            if (VerifyInfo1.equals(SearchResult.InteractionRequired) || VerifyInfo1.equals(SearchResult.Verified)) {
                strmResponse.println("<option value=" + SearchResult.Verified + ">" + getDisplayAddress(qDel.getSDCompany(), qDel.getSDAddress1(), qDel.getSDAddress2(), qDel.getSDTown(), qDel.getSDCounty(), qDel.getSDPostcode()) + "</option>");
            }

            if (VerifyInfo1.equals(SearchResult.Multiple) || VerifyInfo1.equals(SearchResult.PremisesPartial) || VerifyInfo1.equals(SearchResult.StreetPartial)) {
                //if picklist is available for search results
                int size;

                if (asText1.length > 10) {
                    size = 10;
                } else {
                    size = asText1.length;
                }

                for (int i = 0; i < size; i++) {
                    strmResponse.println("<option value=" + asMonikers1[i] + ">" + asText1[i] + "," + asPostcodes1[i] + "</option>");
                }
            }

            strmResponse.println("</select>");
            strmResponse.println("</td></tr>");
            strmResponse.println("</table>");


            strmResponse.println("<tr><td>");
            strmResponse.println("<br><br>");
            strmResponse.println("&nbsp;&nbsp;&nbsp;&nbsp;");
            if (!VerifyInfo.equals(SearchResult.None) && !VerifyInfo1.equals(SearchResult.None)) {
                strmResponse.println("<input type=\"button\" name=btnEntry value=\"Confirm\" id=\"btn\" onclick=\"javascript:confirm_click();\">");
            }

            strmResponse.println("&nbsp;&nbsp;&nbsp;&nbsp;");
            strmResponse.println("<input type=\"button\" name=btnCancel value=\"Return To Booking Screen\" id=\"btn\" onclick=\"javascript:window.close();\">");
            strmResponse.println("</td></tr>");
            strmResponse.println("</table>");

            if (ff) {
                strmResponse.println("</td></tr></table>");
            }

            strmResponse.println("</body>");
            strmResponse.println("<script language=javascript>");

            strmResponse.println("function confirm_click()");
            strmResponse.println("{");
            //strmResponse.println("alert('inside');");
            strmResponse.println("var selCollection=document.getElementById(\"selCollection\");");
            strmResponse.println("var selDelivery=document.getElementById(\"selDelivery\");");
            strmResponse.println("var lblMessage=document.getElementById(\"lblMessage\");");
            strmResponse.println("var lblMessage1=document.getElementById(\"lblMessage1\");");
            strmResponse.println("var error=false;");
            strmResponse.println("var sCollection=selCollection.value;");
            strmResponse.println("var sDelivery=selDelivery.value;");

            //strmResponse.println("alert(sCollection);");
            strmResponse.println("if(sCollection==\"\" && selCollection.options.length > 0)");
            strmResponse.println("{");
            //strmResponse.println("alert(sDelivery);");
            strmResponse.println("lblMessage.style.color='red';");
            strmResponse.println("if(document.all)");
            strmResponse.println("lblMessage.innerText= \"Please select a Collection Address.\";");
            strmResponse.println("else");
            strmResponse.println("lblMessage.textContent = \"Please select a Collection Address.\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");
            strmResponse.println("if(sDelivery==\"\" && selDelivery.options.length > 0)");
            strmResponse.println("{");
            strmResponse.println("lblMessage1.style.color='red';");
            strmResponse.println("if(document.all)");
            strmResponse.println("lblMessage.innerText= \"Please select a Delivery Address.\";");
            strmResponse.println("else");
            strmResponse.println("lblMessage.textContent = \"Please select a Delivery Address.\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");
            strmResponse.println("if(error==false)");
            strmResponse.println("{");
            strmResponse.println("if(sCollection==\"" + SearchResult.Verified + "\"  && sDelivery==\"" + SearchResult.Verified + "\")");
            strmResponse.println("{");
            //strmResponse.println("alert('1');");
            strmResponse.println("document.ConfirmForm." + Constants.SACTION_SUB + ".value=\"" + SearchResult.Verified + "\";");
            strmResponse.println("document.ConfirmForm.submit();");
            strmResponse.println("}");
            strmResponse.println("if((sCollection==\"" + SearchResult.Verified + "\" && selDelivery.options.length==0) && (sDelivery==\"" + SearchResult.Verified + "\" && selCollection.options.length == 0))");
            strmResponse.println("{");
            //strmResponse.println("alert('2');");
            strmResponse.println("document.ConfirmForm.submit();");
            strmResponse.println("}");
            strmResponse.println("if((sCollection==\"" + SearchResult.Verified + "\" || selCollection.options.length == 0) && (sDelivery!=\"" + SearchResult.Verified + "\" && selDelivery.options.length >0))");
            strmResponse.println("{");
            //strmResponse.println("alert('3');");
            strmResponse.println("document.PickListForm.sCompanyName.value=\"" + qCol.getSCCompany() + "\";");
            strmResponse.println("document.PickListForm.sAddress1.value=\"" + qCol.getSCAddress1() + "\";");
            strmResponse.println("document.PickListForm.sAddress2.value=\"" + qCol.getSCAddress2() + "\";");
            strmResponse.println("document.PickListForm.sTown.value=\"" + qCol.getSCTown() + "\";");
            strmResponse.println("document.PickListForm.sCounty.value=\"" + qCol.getSCCounty() + "\";");
            strmResponse.println("document.PickListForm.sPostcode.value=\"" + qCol.getSCPostcode() + "\";");
            strmResponse.println("document.PickListForm.sLatitude.value=\"" + qCol.getSCLatitude() + "\";");
            strmResponse.println("document.PickListForm.sLongitude.value=\"" + qCol.getSCLongitude() + "\";");
            strmResponse.println("document.PickListForm.sType.value=\"C\";");
            strmResponse.println("document.PickListForm." + Constants.MONIKER + ".value=sDelivery;");
            strmResponse.println("document.PickListForm.submit();");
            strmResponse.println("}");
            strmResponse.println("if((sCollection!=\"" + SearchResult.Verified + "\" && selCollection.options.length >0) && (sDelivery==\"" + SearchResult.Verified + "\" || selDelivery.options.length==0))");
            strmResponse.println("{");
            //strmResponse.println("alert('4');");
            strmResponse.println("document.PickListForm.sCompanyName.value=\"" + qDel.getSDCompany() + "\";");
            strmResponse.println("document.PickListForm.sAddress1.value=\"" + qDel.getSDAddress1() + "\";");
            strmResponse.println("document.PickListForm.sAddress2.value=\"" + qDel.getSDAddress2() + "\";");
            strmResponse.println("document.PickListForm.sTown.value=\"" + qDel.getSDTown() + "\";");
            strmResponse.println("document.PickListForm.sCounty.value=\"" + qDel.getSDCounty() + "\";");
            strmResponse.println("document.PickListForm.sPostcode.value=\"" + qDel.getSDPostcode() + "\";");
            strmResponse.println("document.PickListForm.sLatitude.value=\"" + qDel.getSDLatitude() + "\";");
            strmResponse.println("document.PickListForm.sLongitude.value=\"" + qDel.getSDLongitude() + "\";");
            strmResponse.println("document.PickListForm.sType.value=\"D\";");
            strmResponse.println("document.PickListForm." + Constants.MONIKER + ".value=sCollection;");
            strmResponse.println("document.PickListForm.submit();");
            strmResponse.println("}");
            strmResponse.println("if((sCollection!=\"" + SearchResult.Verified + "\" && selCollection.options.length >0) && (sDelivery!=\"" + SearchResult.Verified + "\" && selDelivery.options.length >0))");
            strmResponse.println("{");
            //strmResponse.println("alert('5');");
            strmResponse.println("document.PickListForm." + Constants.MONIKER + ".value=sCollection;");
            strmResponse.println("document.PickListForm." + Constants.MONIKER1 + ".value=sDelivery;");
            strmResponse.println("document.PickListForm.sType.value=\"CD\";");
            strmResponse.println("document.PickListForm.submit();");
            strmResponse.println("}");
            strmResponse.println("}");
            strmResponse.println("}");

            strmResponse.println("</script>");
            //Confirm address form

            strmResponse.println("<form name=\"ConfirmForm\" method=\"POST\" action=\"" + Constants.srvQASValidation + "\">");
            strmResponse.println("<input type=hidden name=\"sCCompanyName\" value=\"" + qCol.getSCCompany() + "\">");
            strmResponse.println("<input type=hidden name=\"sCAddress1\" value=\"" + qCol.getSCAddress1() + "\">");
            strmResponse.println("<input type=hidden name=\"sCAddress2\" value=\"" + qCol.getSCAddress2() + "\">");
            strmResponse.println("<input type=hidden name=\"sCTown\" value=\"" + qCol.getSCTown() + "\">");
            strmResponse.println("<input type=hidden name=\"sCCounty\" value=\"" + qCol.getSCCounty() + "\">");
            strmResponse.println("<input type=hidden name=\"sCPostcode\" value=\"" + qCol.getSCPostcode() + "\">");
            strmResponse.println("<input type=hidden name=\"CCountry\" value=\"" + collection.getCCountry() + "\">");
            strmResponse.println("<input type=hidden name=\"sCLatitude\" value=\"" + qCol.getSCLatitude() + "\">");
            strmResponse.println("<input type=hidden name=\"sCLongitude\" value=\"" + qCol.getSCLongitude() + "\">");
            strmResponse.println("<input type=hidden name=\"sDCompanyName\" value=\"" + qDel.getSDCompany() + "\">");
            strmResponse.println("<input type=hidden name=\"sDAddress1\" value=\"" + qDel.getSDAddress1() + "\">");
            strmResponse.println("<input type=hidden name=\"sDAddress2\" value=\"" + qDel.getSDAddress2() + "\">");
            strmResponse.println("<input type=hidden name=\"sDTown\" value=\"" + qDel.getSDTown() + "\">");
            strmResponse.println("<input type=hidden name=\"sDCounty\" value=\"" + qDel.getSDCounty() + "\">");
            strmResponse.println("<input type=hidden name=\"sDPostcode\" value=\"" + qDel.getSDPostcode() + "\">");
            strmResponse.println("<input type=hidden name=\"DCountry\" value=\"" + delivery.getDCountry() + "\">");
            strmResponse.println("<input type=hidden name=\"sDLatitude\" value=\"" + qDel.getSDLatitude() + "\">");
            strmResponse.println("<input type=hidden name=\"sDLongitude\" value=\"" + qDel.getSDLongitude() + "\">");
            strmResponse.println("<input type=hidden name=\"BDept\" value=\"" + booking.getBDept() + "\">");
            strmResponse.println("<input type=hidden name=\"BCaller\" value=\"" + booking.getBCaller() + "\">");
            strmResponse.println("<input type=hidden name=\"BPhone\" value=\"" + booking.getBPhone() + "\">");
            strmResponse.println("<input type=hidden name=\"BRef\" value=\"" + booking.getBRef() + "\">");
            strmResponse.println("<input type=hidden name=\"BHAWBNO\" value=\"" + booking.getBHAWBNO() + "\">");

            strmResponse.println("<input type=hidden name=\"rdCollect\" value=\"" + booking.getRdCollect() + "\">");
            strmResponse.println("<input type=hidden name=\"CollectDate\" value=\"" + booking.getCollectDate() + "\">");
            strmResponse.println("<input type=hidden name=\"CollectHH\" value=\"" + booking.getCollectHH() + "\">");
            strmResponse.println("<input type=hidden name=\"CollectMM\" value=\"" + booking.getCollectMM() + "\">");
            strmResponse.println("<input type=hidden name=\"chkPickupBefore\" value=\"" + booking.getChkPickupBefore() + "\">");
            strmResponse.println("<input type=hidden name=\"PickupBeforeHH\" value=\"" + booking.getPickupBeforeHH() + "\">");
            strmResponse.println("<input type=hidden name=\"PickupBeforeMM\" value=\"" + booking.getPickupBeforeMM() + "\">");
            strmResponse.println("<input type=hidden name=\"ProductType\" value=\"" + booking.getProductType() + "\">");
            strmResponse.println("<input type=hidden name=\"TotalPieces\" value=\"" + booking.getTotalPieces() + "\">");
            strmResponse.println("<input type=hidden name=\"TotalWeight\" value=\"" + booking.getTotalWeight() + "\">");
            strmResponse.println("<input type=hidden name=\"TotalVolWeight\" value=\"" + booking.getTotalVolWeight() + "\">");
            strmResponse.println("<input type=hidden name=\"" + Constants.SACTION + "\" value=\"Confirm\">");
            strmResponse.println("<input type=hidden name=\"" + Constants.SACTION_SUB + "\" value=\"\">");
            
            strmResponse.println("<input type=hidden name=\"itemsRowCount\" value=\"" + booking.getItemsRowCount() + "\">");
//            strmResponse.println("<input type=hidden name=\"ParcelName\" value=\"" + booking.getsParcelName() + "\">");
//         strmResponse.println("<input type=hidden name=\"ParcelNameId\" value=\"" + booking.getsParcelNameId() + "\">");
         
            strmResponse.println("</form>");



            //QAS Pick List Form

            strmResponse.println("<form name=\"PickListForm\" method=\"POST\" action=\"" + Constants.srvQASValidation + "\">");
            strmResponse.println("<input type=hidden name=\"" + Constants.COMMAND + "\" value=\"" + VerifyFormatAddress.NAME + "\">");
            strmResponse.println("<input type=hidden name=\"" + Constants.MONIKER + "\" value=\"\">");
            strmResponse.println("<input type=hidden name=\"" + Constants.MONIKER1 + "\" value=\"\">");
            strmResponse.println("<input type=hidden name=\"" + Constants.SACTION + "\" value=" + VerifyFormatAddress.NAME + ">");
            strmResponse.println("<input type=hidden name=\"sCompanyName\" value=\"\">");
            strmResponse.println("<input type=hidden name=\"sAddress1\" value=\"\">");
            strmResponse.println("<input type=hidden name=\"sAddress2\" value=\"\">");
            strmResponse.println("<input type=hidden name=\"sTown\" value=\"\">");
            strmResponse.println("<input type=hidden name=\"sCounty\" value=\"\">");
            strmResponse.println("<input type=hidden name=\"sPostcode\" value=\"\">");
            strmResponse.println("<input type=hidden name=\"sLatitude\" value=\"\">");
            strmResponse.println("<input type=hidden name=\"sLongitude\" value=\"\">");
            strmResponse.println("<input type=hidden name=\"sType\" value=\"\">");
            strmResponse.println("<input type=hidden name=\"CCountry\" value=\"" + collection.getCCountry() + "\">");
            strmResponse.println("<input type=hidden name=\"DCountry\" value=\"" + delivery.getDCountry() + "\">");
            strmResponse.println("<input type=hidden name=\"BDept\" value=\"" + booking.getBDept() + "\">");
            strmResponse.println("<input type=hidden name=\"BCaller\" value=\"" + booking.getBCaller() + "\">");
            strmResponse.println("<input type=hidden name=\"BPhone\" value=\"" + booking.getBPhone() + "\">");
            strmResponse.println("<input type=hidden name=\"BRef\" value=\"" + booking.getBRef() + "\">");
            strmResponse.println("<input type=hidden name=\"BHAWBNO\" value=\"" + booking.getBHAWBNO() + "\">");

            strmResponse.println("<input type=hidden name=\"rdCollect\" value=\"" + booking.getRdCollect() + "\">");
            strmResponse.println("<input type=hidden name=\"CollectDate\" value=\"" + booking.getCollectDate() + "\">");
            strmResponse.println("<input type=hidden name=\"CollectHH\" value=\"" + booking.getCollectHH() + "\">");
            strmResponse.println("<input type=hidden name=\"CollectMM\" value=\"" + booking.getCollectMM() + "\">");
            strmResponse.println("<input type=hidden name=\"chkPickupBefore\" value=\"" + booking.getChkPickupBefore() + "\">");
            strmResponse.println("<input type=hidden name=\"PickupBeforeHH\" value=\"" + booking.getPickupBeforeHH() + "\">");
            strmResponse.println("<input type=hidden name=\"PickupBeforeMM\" value=\"" + booking.getPickupBeforeMM() + "\">");
            strmResponse.println("<input type=hidden name=\"ProductType\" value=\"" + booking.getProductType() + "\">");
            strmResponse.println("<input type=hidden name=\"TotalPieces\" value=\"" + booking.getTotalPieces() + "\">");
            strmResponse.println("<input type=hidden name=\"TotalWeight\" value=\"" + booking.getTotalWeight() + "\">");
            strmResponse.println("<input type=hidden name=\"TotalVolWeight\" value=\"" + booking.getTotalVolWeight() + "\">");
            strmResponse.println("</form>");

            strmResponse.println("</html>");
        }
    }

    protected void readArray(String[] arrInputC, String[] arrLabelsC, QASCollectionAddress qColR) {

        for (int i = 0; i < arrInputC.length; i++) {
            switch (i) {
                case 0:
                    if (arrLabelsC[i].equals("Organisation")) {
                        qColR.setSCCompany(arrInputC[i]);
                    }
                    break;
                case 1:
                    if (arrLabelsC[i].equals("")) {
                        qColR.setSCAddress1(arrInputC[i]);
                    }
                    break;
                case 2:
                    if (arrLabelsC[i].equals("")) {
                        qColR.setSCAddress2(arrInputC[i]);
                    } else {
                        if (arrLabelsC[i].equals("Town")) {
                            qColR.setSCTown(arrInputC[i]);

                            qColR.setSCAddress2("");
                        }
                    }

                    break;
                case 3:
                    if (arrLabelsC[i].equals("Town")) {
                        qColR.setSCTown(arrInputC[i]);
                    } else {
                        if (arrLabelsC[i].equals("County")) {
                            qColR.setSCCounty(arrInputC[i]);
                        }
                    }
                    break;
                case 4:
                    if (arrLabelsC[i].equals("County")) {
                        qColR.setSCCounty(arrInputC[i]);
                    } else {
                        if (arrLabelsC[i].equals("Postcode")) {
                            qColR.setSCPostcode(arrInputC[i]);
                        }
                    }
                    break;
                case 5:
                    if (arrLabelsC[i].equals("Postcode")) {
                        qColR.setSCPostcode(arrInputC[i]);
                    }
                    break;
                case 6:
                    if (arrLabelsC[i].equals("Country")) {
                    }
                    break;
                case 7:
                    if (arrLabelsC[i].equals("100m Latitude")) {
                        qColR.setSCLatitude(arrInputC[i]);
                    }
                    break;
                case 8:
                    if (arrLabelsC[i].equals("100m Longitude")) {
                        qColR.setSCLongitude(arrInputC[i]);
                    }
                    break;

            }

        }

    }

    protected void readArray(String[] arrInputD, String[] arrLabelsD, QASDeliveryAddress qDelR) {

        for (int i = 0; i < arrInputD.length; i++) {
            switch (i) {
                case 0:
                    if (arrLabelsD[i].equals("Organisation")) {
                        qDelR.setSDCompany(arrInputD[i]);
                    }
                    break;
                case 1:
                    if (arrLabelsD[i].equals("")) {
                        qDelR.setSDAddress1(arrInputD[i]);
                    }
                    break;
                case 2:
                    if (arrLabelsD[i].equals("")) {
                        qDelR.setSDAddress2(arrInputD[i]);
                    } else {
                        if (arrLabelsD[i].equals("Town")) {
                            qDelR.setSDTown(arrInputD[i]);

                            qDelR.setSDAddress2("");
                        }
                    }

                    break;
                case 3:
                    if (arrLabelsD[i].equals("Town")) {
                        qDelR.setSDTown(arrInputD[i]);
                    } else {
                        if (arrLabelsD[i].equals("County")) {
                            qDelR.setSDCounty(arrInputD[i]);
                        }
                    }
                    break;
                case 4:
                    if (arrLabelsD[i].equals("County")) {
                        qDelR.setSDCounty(arrInputD[i]);
                    } else {
                        if (arrLabelsD[i].equals("Postcode")) {
                            qDelR.setSDPostcode(arrInputD[i]);
                        }
                    }
                    break;
                case 5:
                    if (arrLabelsD[i].equals("Postcode")) {
                        qDelR.setSDPostcode(arrInputD[i]);
                    }
                    break;
                case 6:
                    if (arrLabelsD[i].equals("Country")) {
                    }
                    break;
                case 7:
                    if (arrLabelsD[i].equals("100m Latitude")) {
                        qDelR.setSDLatitude(arrInputD[i]);
                    }
                    break;
                case 8:
                    if (arrLabelsD[i].equals("100m Longitude")) {
                        qDelR.setSDLongitude(arrInputD[i]);
                    }
                    break;

            }

        }

    }

    /**
     *
     * @param request
     * @param response
     * @param booking
     * @param collection
     * @param delivery
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void Confirm(HttpServletRequest request, HttpServletResponse response, BookingInfo booking, CollectionAddress collection, DeliveryAddress delivery) throws ServletException, IOException {
        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");
        String sAction = request.getParameter(Constants.SACTION) == null ? "" : request.getParameter(Constants.SACTION).toString();
        String sAction_sub = request.getParameter(Constants.SACTION_SUB) == null ? "" : request.getParameter(Constants.SACTION_SUB).toString();
        String sCCompany = "", sCContact = "", sCAddress1 = "", sCAddress2 = "", sCTown = "", sCPostcode = "", sCCounty = "", sCLongitude = "", sCLatitude = "", sCTownKey = "";
        String sDCompany = "", sDContact = "", sDAddress1 = "", sDAddress2 = "", sDTown = "", sDPostcode = "", sDCounty = "", sDLongitude = "", sDLatitude = "", sDTownKey = "";

        String[] asLabels = null;
        String[] asAddress = null;
        String[] asLabels1 = null;
        String[] asAddress1 = null;
        String sItemTags = "";
        String sCommodityTags = "";
        String sPage = "";
        //Inialising QAS constants
        String sEndpoint = getServletContext().getInitParameter("QasEndpoint");
        String sLayout = getServletContext().getInitParameter("QasLayout");

        Constants.ENDPOINT = sEndpoint;
        Constants.LAYOUT = sLayout;

        //Initialise QASAddress Objects
        QASCollectionAddress qCol = new QASCollectionAddress();
        QASDeliveryAddress qDel = new QASDeliveryAddress();

        try {

            if (sAction.equals("Confirm")) {

                qCol.setSCCompany(request.getParameter("sCCompanyName") == null ? "" : request.getParameter("sCCompanyName").toString());
                qCol.setSCAddress1(request.getParameter("sCAddress1") == null ? "" : request.getParameter("sCAddress1").toString());
                qCol.setSCAddress2(request.getParameter("sCAddress2") == null ? "" : request.getParameter("sCAddress2").toString());
                qCol.setSCTown(request.getParameter("sCTown") == null ? "" : request.getParameter("sCTown").toString());
                qCol.setSCPostcode(request.getParameter("sCPostcode") == null ? "" : request.getParameter("sCPostcode").toString());
                qCol.setSCLatitude(request.getParameter("sCLatitude") == null ? "" : request.getParameter("sCLatitude").toString());
                qCol.setSCLongitude(request.getParameter("sCLongitude") == null ? "" : request.getParameter("sCLongitude").toString());
                qDel.setSDCompany(request.getParameter("sDCompanyName") == null ? "" : request.getParameter("sDCompanyName").toString());
                qDel.setSDAddress1(request.getParameter("sDAddress1") == null ? "" : request.getParameter("sDAddress1").toString());
                qDel.setSDAddress2(request.getParameter("sDAddress2") == null ? "" : request.getParameter("sDAddress2").toString());
                qDel.setSDTown(request.getParameter("sDTown") == null ? "" : request.getParameter("sDTown").toString());
                qDel.setSDPostcode(request.getParameter("sDPostcode") == null ? "" : request.getParameter("sDPostcode").toString());
                qDel.setSDLatitude(request.getParameter("sDLatitude") == null ? "" : request.getParameter("sDLatitude").toString());
                qDel.setSDLongitude(request.getParameter("sDLongitude") == null ? "" : request.getParameter("sDLongitude").toString());


            } else {
                String sType = request.getParameter(Constants.STYPE) == null ? "" : request.getParameter(Constants.STYPE).toString();


                if (sType.equals("C")) {
                    //Collection address is already validated delivery address needs to be formated by calling QAS.

                    qCol.setSCCompany(request.getParameter("sCompanyName") == null ? "" : request.getParameter("sCompanyName").toString());
                    qCol.setSCAddress1(request.getParameter("sAddress1") == null ? "" : request.getParameter("sAddress1").toString());
                    qCol.setSCAddress2(request.getParameter("sAddress2") == null ? "" : request.getParameter("sAddress2").toString());
                    qCol.setSCTown(request.getParameter("sTown") == null ? "" : request.getParameter("sTown").toString());
                    qCol.setSCPostcode(request.getParameter("sPostcode") == null ? "" : request.getParameter("sPostcode").toString());
                    qCol.setSCLatitude(request.getParameter("sLatitude") == null ? "" : request.getParameter("sLatitude").toString());
                    qCol.setSCLongitude(request.getParameter("sLongitude") == null ? "" : request.getParameter("sLongitude").toString());


                    try {
                        // The helper extract the Command name from the request and creates the relevant object.
                        Command cmd = HttpHelper.getCommand(request);

                        // perform custom operation
                        sPage = cmd.execute(request, response);

                        asAddress = (String[]) request.getAttribute(Constants.ADDRESS);
                        asLabels = (String[]) request.getAttribute(Constants.LABELS);
                        readArray(asAddress, asLabels, qDel);

                    } catch (Exception e) {
                        vShowErrorPage(response, e);
                    }
                } else if (sType.equals("D")) {

                    //Delivery address is already validated Collection address needs to be formated by calling QAS.

                    qDel.setSDCompany(request.getParameter("sCompanyName") == null ? "" : request.getParameter("sCompanyName").toString());
                    qDel.setSDAddress1(request.getParameter("sAddress1") == null ? "" : request.getParameter("sAddress1").toString());
                    qDel.setSDAddress2(request.getParameter("sAddress2") == null ? "" : request.getParameter("sAddress2").toString());
                    qDel.setSDTown(request.getParameter("sTown") == null ? "" : request.getParameter("sTown").toString());
                    qDel.setSDPostcode(request.getParameter("sPostcode") == null ? "" : request.getParameter("sPostcode").toString());
                    qDel.setSDLatitude(request.getParameter("sLatitude") == null ? "" : request.getParameter("sLatitude").toString());
                    qDel.setSDLongitude(request.getParameter("sLongitude") == null ? "" : request.getParameter("sLongitude").toString());

                    try {
                        // The helper extract the Command name from the request and creates the relevant object.
                        Command cmd = HttpHelper.getCommand(request);

                        // perform custom operation
                        sPage = cmd.execute(request, response);

                        asAddress = (String[]) request.getAttribute(Constants.ADDRESS);
                        asLabels = (String[]) request.getAttribute(Constants.LABELS);
                        readArray(asAddress, asLabels, qCol);

                    } catch (Exception e) {
                        vShowErrorPage(response, e);
                    }

                } else if (sType.equals("CD")) {
                    try {
                        //both collection and Delivery addresses needs to be formatted calling QAS.

                        // The helper extract the Command name from the request and creates the relevant object.
                        Command cmd = HttpHelper.getCommand(request);

                        // perform custom operation
                        sPage = cmd.execute(request, response);

                        asAddress = (String[]) request.getAttribute(Constants.ADDRESS);
                        asLabels = (String[]) request.getAttribute(Constants.LABELS);
                        readArray(asAddress, asLabels, qCol);

                        request.setAttribute(Constants.STYPE, Constants.MONIKER1);
                        // The helper extract the Command name from the request and creates the relevant object.
                        cmd = HttpHelper.getCommand(request);

                        // perform custom operation
                        sPage = cmd.execute(request, response);

                        asAddress1 = (String[]) request.getAttribute(Constants.ADDRESS);
                        asLabels1 = (String[]) request.getAttribute(Constants.LABELS);
                        readArray(asAddress1, asLabels1, qDel);

                    } catch (Exception e) {
                        vShowErrorPage(response, e);
                    }
                }
            }

            strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");

            strmResponse.println("var txtCCompany = window.opener.document.getElementById(\"txtCCompany\");");
            strmResponse.println("var txtCAddress1 = window.opener.document.getElementById(\"txtCAddress1\");");
            strmResponse.println("var txtCAddress2 = window.opener.document.getElementById(\"txtCAddress2\");");
            strmResponse.println("var txtCTown = window.opener.document.getElementById(\"txtCTown\");");
            strmResponse.println("var txtCCounty = window.opener.document.getElementById(\"txtCCounty\");");
            strmResponse.println("var txtCPostcode = window.opener.document.getElementById(\"txtCPostcode\");");
            strmResponse.println("var txtCLatitude = window.opener.document.getElementById(\"txtCLatitude\");");
            strmResponse.println("var txtCLongitude = window.opener.document.getElementById(\"txtCLongitude\");");
            strmResponse.println("var txtDCompany = window.opener.document.getElementById(\"txtDCompany\");");
            strmResponse.println("var txtDAddress1 = window.opener.document.getElementById(\"txtDAddress1\");");
            strmResponse.println("var txtDAddress2 = window.opener.document.getElementById(\"txtDAddress2\");");
            strmResponse.println("var txtDTown = window.opener.document.getElementById(\"txtDTown\");");
            strmResponse.println("var txtDCounty = window.opener.document.getElementById(\"txtDCounty\");");
            strmResponse.println("var txtDPostcode = window.opener.document.getElementById(\"txtDPostcode\");");
            strmResponse.println("var txtDLatitude = window.opener.document.getElementById(\"txtDLatitude\");");
            strmResponse.println("var txtDLongitude = window.opener.document.getElementById(\"txtDLongitude\");");
            strmResponse.println("var selDCountry = window.opener.document.getElementById(\"selDCountry\");");

            strmResponse.println("txtCCompany.value = \"" + qCol.getSCCompany() + "\";");
            strmResponse.println("txtCAddress1.value = \"" + qCol.getSCAddress1() + "\";");
            strmResponse.println("txtCAddress2.value = \"" + qCol.getSCAddress2() + "\";");
            strmResponse.println("txtCTown.value = \"" + qCol.getSCTown() + "\";");
            strmResponse.println("txtCCounty.value = \"" + qCol.getSCCounty() + "\";");
            strmResponse.println("txtCPostcode.value = \"" + qCol.getSCPostcode() + "\";");
            strmResponse.println("txtCLatitude.value = \"" + qCol.getSCLatitude() + "\";");
            strmResponse.println("txtCLongitude.value = \"" + qCol.getSCLongitude() + "\";");
            strmResponse.println("txtDCompany.value = \"" + qDel.getSDCompany() + "\";");
            strmResponse.println("txtDAddress1.value = \"" + qDel.getSDAddress1() + "\";");
            strmResponse.println("txtDAddress2.value = \"" + qDel.getSDAddress2() + "\";");
            strmResponse.println("txtDTown.value = \"" + qDel.getSDTown() + "\";");
            strmResponse.println("txtDCounty.value = \"" + qDel.getSDCounty() + "\";");
            strmResponse.println("txtDPostcode.value = \"" + qDel.getSDPostcode() + "\";");
            strmResponse.println("txtDLatitude.value = \"" + qDel.getSDLatitude() + "\";");
            strmResponse.println("txtDLongitude.value = \"" + qDel.getSDLongitude() + "\";");
            strmResponse.println("txtDTown.disabled=true;");
            strmResponse.println("txtDPostcode.disabled=true;");
            strmResponse.println("selDCountry.disabled=true;");
            strmResponse.println("txtCTown.disabled=true;");
            strmResponse.println("txtCPostcode.disabled=true;");

            String result = "";
            HttpSession validSession = request.getSession(false);

            if (sAction_sub.equals(SearchResult.Verified)) {

                try {

                    //write the confirmed addresses into the session addresses table for later retrieval.
                    //delete any addresses in the srs_address table for this account and session
                    deleteSessionAddress(request, response);
                    writeSessionAddress(validSession.getAttribute("acc_id").toString(), validSession.getAttribute("sessionid").toString(), (qCol.getSCCompany().length()>60?qCol.getSCCompany().substring(0,60):qCol.getSCCompany()), qCol.getSCAddress1(), qCol.getSCAddress2(), collection.getCAddress3(), qCol.getSCTown(), qCol.getSCCounty(), qCol.getSCPostcode(), collection.getCCountry(), collection.getCContact(), collection.getCPhone(), collection.getCInstruct(), qCol.getSCLongitude(), qCol.getSCLatitude(), "C", qCol.getSCTownKey(), collection.getCResidentialFlag());
                    writeSessionAddress(validSession.getAttribute("acc_id").toString(), validSession.getAttribute("sessionid").toString(), (qDel.getSDCompany().length()>60?qDel.getSDCompany().substring(0,60):qDel.getSDCompany()), qDel.getSDAddress1(), qDel.getSDAddress2(), delivery.getDAddress3(), qDel.getSDTown(), qDel.getSDCounty(), qDel.getSDPostcode(), delivery.getDCountry(), delivery.getDContact(), delivery.getDPhone(), delivery.getDInstruct(), qDel.getSDLongitude(), qDel.getSDLatitude(), "D", qDel.getSDTownKey(), delivery.getDResidentialFlag());

                    if (booking.getProductType().equals("NDOX")) {
                        sItemTags = request.getParameter("itemTags") == null ? "" : request.getParameter("itemTags").toString();
                        sCommodityTags = request.getParameter("commodityTags") == null ? "" : request.getParameter("commodityTags").toString();

                        if (sItemTags.length() > 1 && !sItemTags.equals(validSession.getAttribute("itemTags"))) {
                            validSession.setAttribute("itemTags", sItemTags);
                        } else {
                            validSession.removeAttribute("itemTags");
                        }


                        if (sCommodityTags.length() > 1 && !sCommodityTags.equals(validSession.getAttribute("commodityTags"))) {
                            validSession.setAttribute("commodityTags", sCommodityTags);
                        } else {
                            validSession.removeAttribute("commodityTags");
                        }
//                        //insert Item and Commodity Grid rows into database
//                        sItemTags = validSession.getAttribute("itemTags") == null ? "" : validSession.getAttribute("itemTags").toString();
//                        sCommodityTags = validSession.getAttribute("commodityTags") == null ? "" : validSession.getAttribute("commodityTags").toString();
//
//                        if (sItemTags.length() > 0) {
//                            addItemGridRow(sItemTags, validSession, response);
//                        }
//
//                        if (sCommodityTags.length() > 0) {
//                            result = addCommodityGridRow(sCommodityTags, validSession, response);
//                        }
                    }
                    
                     //insert Item and Commodity Grid rows into database
                        sItemTags = validSession.getAttribute("itemTags") == null ? "" : validSession.getAttribute("itemTags").toString();
                        sCommodityTags = validSession.getAttribute("commodityTags") == null ? "" : validSession.getAttribute("commodityTags").toString();

                        if (sItemTags.length() > 0) {
                            addItemGridRow(sItemTags, validSession, response);
                        }

                        if (sCommodityTags.length() > 0) {
                            result = addCommodityGridRow(sCommodityTags, validSession, response);
                        }

                    if (result.length() == 0) {
                        //get valid services for user requirement
                        result = getServices(request, response, booking, collection, delivery, qCol, qDel);
                    }

                } catch (Exception ex) {
                    vShowErrorPage(response, ex);
                }

                if (result.length() == 0) {
                    //strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");
                    strmResponse.println("opener.Services('" + SearchResult.Verified + "');");
                    strmResponse.println("window.close();");
                    strmResponse.println("</script>");
                } else {
                    //strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");
                    strmResponse.println("var lblMessage = window.opener.document.getElementById(\"lblMessage\");");
                    strmResponse.println("var selDept = window.opener.document.getElementById(\"selDept\");");
                    if (ff) {
                        strmResponse.println("lblMessage.textContent=\"" + result + "\"");
                    } else {
                        strmResponse.println("lblMessage.innerText=\"" + result + "\"");
                    }
                    strmResponse.println("opener.Services('Error');");
                    strmResponse.println("window.close();");
                    strmResponse.println("</script>");
                }


            } else {
                strmResponse.println("window.close();");
                strmResponse.println("</script>");
            }
        } catch (Exception ex) {
            vShowErrorPageReporting(response, ex);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param booking
     * @param collection
     * @param delivery
     * @return
     * @throws java.net.UnknownHostException
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     */
    public String getServices(HttpServletRequest request, HttpServletResponse response,
            BookingInfo booking, CollectionAddress collection, DeliveryAddress delivery, QASCollectionAddress qColF, QASDeliveryAddress qDelF) throws UnknownHostException, IOException, ServletException {

        java.util.Date d = new java.util.Date();
        String timeStamp = d.toString();

        HttpSession validSession = request.getSession(false);
        
        String[][] getServicesArray = null;
        String[][] tempServices = null;

        String sServiceAPIError = "";

        //Build services API input parameters
        StringBuffer sInput = new StringBuffer();

        sInput.append("CK=" + encStr(request.getSession(false).getAttribute("acc_id").toString()));                                            // Customer Key
        sInput.append("&PW=" + encStr(request.getSession(false).getAttribute("pwd").toString()));                                              // Password
        sInput.append("&TS=" + encStr(timeStamp));                                        // Transaction Stamp
//        sInput.append("&DT="+encStr(booking.getBDept()));                               // Department client selected
        sInput.append("&AB=" + encStr(booking.getRdCollect().equals("Now") ? "N" : "Y"));     // Indicates if a booking made ahead of time
        sInput.append("&TP=" + encStr(booking.getProductType()));                         // Product Type
        
//        System.out.println("############################################################################################################");
//        System.out.println("########################### From QASValidation class, line# 1213, sParcelName ==" + booking.getsParcelName());
//        
//        System.out.println("############################################################################################################");
//        System.out.println("########################### From QASValidation class, line# 1217, ParcelName from request object ==" + request.getParameter("ParcelName"));
////        System.out.println("########################### From QASValidation class, line# 1218, selParcel from request object ==" + request.getParameter("selParcel"));
////        System.out.println("########################### From QASValidation class, line# 1219, sParcelName from request object ==" + request.getParameter("sParcelName"));
//        System.out.println("############################################################################################################");
//        
////         ParcelInfo parcelInfo = new ParcelInfo();
////            parcelInfo.parseParcel((String[][]) validSession.getAttribute("getNDIParcelArray"));
//            String sParcelIndx = booking.getsParcelName();
////            int iParcelIndx = -1;
//            if ( sParcelIndx == null){
//                sParcelIndx = (String) request.getParameter("ParcelName");
//            }
//            
//            if (sParcelIndx != null && sParcelIndx.trim().equalsIgnoreCase("undefined")){
//                sParcelIndx = null;
//            }
////            try{
////                iParcelIndx = Integer.parseInt(sParcelIndx);
////            }catch(NumberFormatException nbfe){
////                nbfe.printStackTrace();
////            }
////            String parcelName = "";
////            if (iParcelIndx != -1){
////                parcelName = (String)parcelInfo.getPnVector().get(iParcelIndx);
////            }
//            
//             String parcelName4NDIService = "";
//            if (sParcelIndx != null)
//                parcelName4NDIService = sParcelIndx;
////         sData = (String) parcelInfo.getPnVector().elementAt(iParcelIndx);
//        sInput.append("&PT=" + encStr(parcelName4NDIService));//booking.getsParcelName()));                         // Product Type
//        if (booking.getProductType().equals("NDOX") ) {// && sParcelIndx == null) {
            sInput.append("&PN=" + encStr(totalCalculate(request, response)));
//        } else {
//            sInput.append("&PN=" + encStr(booking.getTotalPieces()));
//        }                     // Total No.of.Pieces
        sInput.append("&TW=" + encStr(booking.getTotalWeight()));                         // Total Weight

//         sInput.append(getItems(request, response));//, sParcelIndx!=null?true:false));        
        if (booking.getProductType().equals("NDOX")) {
            sInput.append(getItems(request, response));//, sParcelIndx!=null?true:false));                                      // get Item tags ^NI ^WI ^PT ^HE ^PL ^PD
            sInput.append(getCommodityValues(request, response));                            // get Commodity tags ^CC ^CD ^CQ ^CV ^CI
        } 
//        else if (sParcelIndx != null){
//            sInput.append(getItems(request, response, true)); 
//        }

        SimpleDateFormat sDateFormat = new SimpleDateFormat("dd/MM/yy");
        SimpleDateFormat sTimeFormat = new SimpleDateFormat("HH:mm");
        String sDate = sDateFormat.format(d);
        String sTime = sTimeFormat.format(d);

        if (booking.getRdCollect().equals("Now")) {
            sInput.append("&RO=" + encStr(sDate));                                        // Pickup Date
            sInput.append("&RA=" + encStr(sTime));                                        // Pickup Time
        } else {
            String sDate1 = booking.getCollectDate().equals("") ? sDate : booking.getCollectDate();
            sInput.append("&RO=" + encStr(sDate1));                                               // Pickup Date
            sInput.append("&RA=" + encStr(booking.getCollectHH() + ":" + booking.getCollectMM()));    // Pickup Time
        }

        if (booking.getChkPickupBefore().equals("1")) //check if closesat checkbox ticked
        {
            sInput.append("&RC=" + (booking.getPickupBeforeHH() + ":" + booking.getPickupBeforeMM()));
        } else {
            sInput.append("&RC=");
        }                                                              // Collect By Time

//        sInput.append("&PA="+encStr(sCAddress1));                                       // Pickup Street Address
//        sInput.append("&P2="+encStr(sCAddress2));                                       // Pickup Street Address 2
        sInput.append("&PK=" + encStr(qColF.getSCTownKey()));                                           // Pickup Town Key
//        sInput.append("&PO="+encStr(sCCounty));                                         // Pickup County
        sInput.append("&PP=" + encStr(qColF.getSCPostcode()));                                       // Pickup Postcode
        
        // added by Adnan on 27 Aug 2013 against Residential Flag change
        System.out.println("@@@@@@@@@@@@@@@ QASValidation: Line # 1252: collection.getCResidentialFlag() = " + collection.getCResidentialFlag());
        sInput.append("&PF=" + encStr(collection.getCResidentialFlag()));                                       // Pickup Residential Address Flag
        
        sInput.append("&PY=" + encStr(collection.getCCountry()));                         // Pickup Country Code
//        sInput.append("&PG="+encStr(sCLongitude));                                      // Pickup Longitude
//        sInput.append("&PU="+encStr(sCLatitude));                                       // Pickup Latitude

//        sInput.append("&DA="+encStr(sDAddress1));                                       // Delivery Street Address
//        sInput.append("&D2="+encStr(sDAddress2));                                       // Delivery Street Address 2
        sInput.append("&DK=" + encStr(qDelF.getSDTownKey()));                                           // Delivery Town Key
//        sInput.append("&DO="+encStr(sDCounty));                                         // Delivery County
        sInput.append("&DP=" + encStr(qDelF.getSDPostcode()));                                       // Delivery Postcode
        
        System.out.println("@@@@@@@@@@@@@@@ QASValidation: Line # 1264: delivery.getDResidentialFlag() = " + delivery.getDResidentialFlag());
        // added by Adnan on 27 Aug 2013 against Residential Flag change
        sInput.append("&DF=" + encStr(delivery.getDResidentialFlag()));                                       // Delivery Residential Address Flag
        
        sInput.append("&DY=" + encStr(delivery.getDCountry()));                           // Delivery Country Code
//        sInput.append("&DL="+encStr(sDLongitude));                                      // Delivery Longitude
//        sInput.append("&DU="+encStr(sDLatitude));                                       // Delivery Latitude

        System.out.println("#########Line#: 1273, QASValidation: sInput for NDI_Services :" + sInput);
        if (booking.getProductType().length() == 0 
                || collection.getCCountry().length() == 0 
                || delivery.getDCountry().length() == 0 
                || (Integer.parseInt(totalCalculate(request, response)) == 0 && booking.getProductType().equals("NDOX")) 
                || ((booking.getTotalPieces().equals("0") || booking.getTotalPieces().equals("")) && booking.getProductType().equals("DOX"))) {
            sServiceAPIError = "Invalid booking parameters. Please re-login and try again";

            writeOutputLog("booking.getProductType().length():"+booking.getProductType().length());
            writeOutputLog("collection.getCCountry().length():"+collection.getCCountry().length());
            writeOutputLog("delivery.getDCountry().length():"+delivery.getDCountry().length());
            writeOutputLog("Integer.parseInt(totalCalculate(request, response)):"+Integer.parseInt(totalCalculate(request, response)));
            writeOutputLog("booking.getTotalPieces():"+booking.getTotalPieces());
            writeOutputLog("booking.getProductType():"+booking.getProductType());
            writeOutputLog("Invalid booking parameters. Please re-login and try again");
        } else {
            URL url;

            if (environ.equals("PROD")) {
                url = new URL(scriptsurl + bdl.getString("serviceScript") + "?" + sInput.toString());
            } else {
                url = new URL(bdl.getString("devhost") + "services.txt");
            }


            writeOutputLog("Input to NDI_Service:" + url.toString());

            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setUseCaches(false);
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String s = "";
            String outputLines = "";

            while ((s = br.readLine()) != null) {
                outputLines = outputLines + s;
            }

            writeOutputLog("Ouput from NDI_Service:" + outputLines);

            br.close();
            outputLines = outputLines + "^ZZ";
            getServicesArray = parseOutput(outputLines);

            if (getServicesArray == null) {
                sServiceAPIError = "Error in NDI_Service no output returned from API";
            } else {

                for (int i = 0; i < getServicesArray.length - 1; i++) {
                    if (getServicesArray[i][0].equals("ER")) {
                        sServiceAPIError = getServicesArray[i][1].substring(3, getServicesArray[i][1].length());
                        break;
                    }
                }


            }

            if (sServiceAPIError.length() == 0) {
                if (request.isRequestedSessionIdValid()) {
                    tempServices = (String[][]) request.getSession(false).getAttribute("getServicesArray");
                    if (tempServices != null) //clear the existing service details and set the new services array
                    {
                        request.getSession(false).setAttribute("getServicesArray", null);
                    }

                    request.getSession(false).setAttribute("getServicesArray", getServicesArray);

                }
            }
        }
        return sServiceAPIError;
    }

    /**
     *
     * @param request
     * @param response
     * @return
     * @throws java.net.UnknownHostException
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     */
    public String getItems(HttpServletRequest request, HttpServletResponse response/*, boolean isParcelType*/) throws UnknownHostException, IOException, ServletException {

        String sql = "Select pro_noitems,pro_height,pro_length,pro_width "//, "//pro_ptype, "
//                + "pro_iweight  "
                + "from pro_product where pro_sessionid='" + (request.getSession(false).getAttribute("sessionid").toString()) + "'";
        String sItemTags = "";

        Connection conDB = null;
        Statement stmRead;
        ResultSet rstRead;
        try {

            conDB = cOpenConnection();
            stmRead = conDB.createStatement();
            rstRead = stmRead.executeQuery(sql);

            while (rstRead.next()) {
                sItemTags = sItemTags + "&NI=" + rstRead.getInt("pro_noitems");
                
//                if (!isParcelType)
                /**
                 * Following line is commented by Adnan on 1-DEC-2104 against CR-Remove Item weight
                 */    
//                sItemTags = sItemTags + "&WI=" + rstRead.getFloat("pro_iweight");
//                else
//                    sItemTags = sItemTags + "&WI=0" ;
                
//                String parcelType = rstRead.getString("pro_ptype");
//                System.out.println("PARCEL TYPE in QASValidation b4 validation == " + parcelType);
//                if (parcelType != null && !parcelType.trim().equalsIgnoreCase("0") && !parcelType.trim().equalsIgnoreCase("SELECT")){
//                    System.out.println("PARCEL TYPE in QASValidation b4 appending to API call == " + parcelType);
//                    sItemTags = sItemTags + "&PT=" + encStr(parcelType);
//                }
                sItemTags = sItemTags + "&HE=" + rstRead.getInt("pro_height");
                sItemTags = sItemTags + "&PL=" + rstRead.getInt("pro_length");
                sItemTags = sItemTags + "&PD=" + rstRead.getInt("pro_width");
            }
            vCloseConnection(conDB);
        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }

        if (sItemTags.length() == 0) {
            sItemTags = "&NI=&HE=&PL=&PD=";
        }

        return sItemTags;
    }

    /**
     *
     * @param request
     * @param response
     * @return
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public String totalCalculate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sTotalPieces = "";

        try {

            String sSQL = "";

            sSQL = "select SUM(pro_NoItems) as totPieces from pro_product where pro_sessionid='" + (request.getSession(false).getAttribute("sessionid").toString()) + "'";

            Connection conDB = null;
            java.sql.Statement stmRead;
            ResultSet rstRead;

            conDB = cOpenConnection();
            stmRead = conDB.createStatement();
            rstRead = stmRead.executeQuery(sSQL);

            while (rstRead.next()) {
                sTotalPieces = rstRead.getString("totPieces") == null ? "0" : rstRead.getString("totPieces");
            }

            vCloseConnection(conDB);
        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }

        return sTotalPieces;
    }

    /**
     *
     * @param request
     * @param response
     * @return
     * @throws java.net.UnknownHostException
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     */
    public String getCommodityValues(HttpServletRequest request, HttpServletResponse response) throws UnknownHostException, IOException, ServletException {

        String sql = "Select com_code, com_description, com_quantity, com_value, com_indemnity  from com_commodity where com_sessionid='" + (request.getSession(false).getAttribute("sessionid").toString()) + "'";
        String sCommodityTags = "";

        Connection conDB = null;
        Statement stmRead;
        ResultSet rstRead;
        try {

            conDB = cOpenConnection();
            stmRead = conDB.createStatement();
            rstRead = stmRead.executeQuery(sql);

            while (rstRead.next()) {
                sCommodityTags = sCommodityTags + "&CC=" + encStr(rstRead.getString("com_code"));
                sCommodityTags = sCommodityTags + "&CD=" + encStr(rstRead.getString("com_description"));
                sCommodityTags = sCommodityTags + "&CQ=" + rstRead.getDouble("com_quantity");
                sCommodityTags = sCommodityTags + "&CV=" + rstRead.getInt("com_value");

                if (rstRead.getInt("com_indemnity") == 0) {
                    sCommodityTags = sCommodityTags + "&CI=N";
                } else {
                    sCommodityTags = sCommodityTags + "&CI=Y";
                }
            }

            vCloseConnection(conDB);
        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }

        if (sCommodityTags.length() == 0) {
            sCommodityTags = "&CC=&CD=&CQ=&CV=CI=";
        }

        return sCommodityTags;
    }

    /**
     *
     * @param sCompany
     * @param sAddress1
     * @param sAddress2
     * @param sTown
     * @param sCounty
     * @param sPostcode
     * @return
     */
    public String getDisplayAddress(String sCompany, String sAddress1, String sAddress2, String sTown, String sCounty, String sPostcode) {
        String sAddress = "";

        if (sCompany.length() > 0) {
            sAddress = sAddress + sCompany + ",";
        }

        if (sAddress1.length() > 0) {
            sAddress = sAddress + sAddress1 + ",";
        }

        if (sAddress2.length() > 0) {
            sAddress = sAddress + sAddress2 + ",";
        }

        if (sTown.length() > 0) {
            sAddress = sAddress + sTown + ",";
        }

        if (sCounty.length() > 0) {
            sAddress = sAddress + sCounty + ",";
        }

        if (sPostcode.length() > 0) {
            sAddress = sAddress + sPostcode + ",";
        }

        if (sAddress.length() > 0) {
            sAddress = sAddress.substring(0, sAddress.length() - 1);
        }

        return sAddress;
    }

    /**
     *
     * @param request
     * @param response
     * @param delivery
     * @return
     * @throws java.net.UnknownHostException
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     */
    public String[][] getTownKey(HttpServletRequest request, HttpServletResponse response, DeliveryAddress delivery) throws UnknownHostException, IOException, ServletException {
        java.util.Date d = new java.util.Date();
        String timeStamp = d.toString();
        boolean status = true;
        String[][] getTownArray = null;

        //Build TownValidation API input parameters
        StringBuffer sInput = new StringBuffer();

        //Town Name-TN, Postcode-TO, Country-TC, Transaction Stamp-TS

        sInput.append("TN=" + encStr(delivery.getDTown()));
        sInput.append("&TO=" + encStr(delivery.getDPostcode()));
        sInput.append("&TC=" + encStr(delivery.getDCountry()));
        sInput.append("&TS=" + encStr(timeStamp));

        URL url;

        if (environ.equals("PROD")) {
            url = new URL(scriptsurl + bdl.getString("validtownScript") + "?" + sInput.toString());
        } else {
            url = new URL(bdl.getString("devhost") + "validtown.txt");
        }


        writeOutputLog("Input to NDI_ValidTown:" + url.toString());

        URLConnection con = url.openConnection();
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setUseCaches(false);
        BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String s = "";
        String outputLines = "";

        while ((s = br.readLine()) != null) {
            outputLines = outputLines + s;
        }

        writeOutputLog("Ouput from NDI_ValidTown:" + outputLines);

        br.close();
        outputLines = outputLines + "^ZZ";
        getTownArray = parseOutput(outputLines);

        return getTownArray;

    }
    //parse town array
    /**
     *
     * @param townArray
     * @return
     */
    public TownKeyInfo parseValidTown(String[][] townArray) {
        String tempstr = "";
        String tag = "";
        String sTKey = "";
        //ValidTown API output  ^TS Transaction Stamp ^TK Town Key

        TownKeyInfo tkInfo = new TownKeyInfo();
        for (int i = 0; i < townArray.length - 1; i++) {
            tag = townArray[i][0];

            if (tag.equals("TK")) {
                tempstr = townArray[i][1].trim();
                tkInfo.setSTownKey(tempstr);
            }

            if (tag.equals("ER")) {
                tkInfo.setSTownKeyError(townArray[i][1].trim().substring(3, townArray[i][1].length()));
            }

        }

        return tkInfo;
    }

    /**
     *
     * @param sItemTags
     * @param validSession
     * @param rspOutput
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void addItemGridRow(String sItemTags, HttpSession validSession, HttpServletResponse rspOutput) throws ServletException, IOException {
        boolean bResult = false;

System.out.println("$$$$$$$$$$ QASValidation --> addItemGridRow ==> sItemsTags == " + sItemTags);
        String sPNoofItems = "";
//        String sPWeight = "";
        String sPLength = "";
        String sPWidth = "";
        String sPHeight = "";

        String[] sEditedRow;

        sEditedRow = sItemTags.split(",");

        try {

            if (sEditedRow[0] == null || sEditedRow[0].length() == 0) {
                sPNoofItems = "0";
            } else {
                sPNoofItems = sReplaceChars(sEditedRow[0], "'", "''");
            }

//            if (sEditedRow.length > 1) {
//                if (sEditedRow[1] == null || sEditedRow[1].length() == 0) {
//                    sPWeight = "0";
//                } else {
//                    sPWeight = sReplaceChars(sEditedRow[1], "'", "''");
//                }
//            } else {
//                sPWeight = "0";
//            }

            
            if (sEditedRow.length > 1) {
                if (sEditedRow[1] == null || sEditedRow[1].length() == 0) {
                    sPLength = "0";
                } else {
                    sPLength = sReplaceChars(sEditedRow[1], "'", "''");
                }
            } else {
                sPLength = "0";
            }
            
            
            if (sEditedRow.length > 2) {
                if (sEditedRow[2] == null || sEditedRow[2].length() == 0) {
                    sPWidth = "0";
                } else {
                    sPWidth = sReplaceChars(sEditedRow[2], "'", "''");
                }
            } else {
                sPWidth = "0";
            }

            if (sEditedRow.length > 3) {
                if (sEditedRow[3] == null || sEditedRow[3].length() == 0) {
                    sPHeight = "0";
                } else {
                    sPHeight = sReplaceChars(sEditedRow[3], "'", "''");
                }

            } else {
                sPHeight = "0";
            }

            // Insert a new row
            Connection conProduct = null;
            conProduct = cOpenConnection();
            Statement stmInsert = conProduct.createStatement();

            // stmInsert.executeUpdate("Insert into pro_Product (pro_Type,pro_Description,pro_Value,pro_NoItems,pro_Length,pro_Width,pro_Height,pro_Weight,pro_cubic,pro_Indemnity,pro_SessionId)values("+sPType+","+sPDescription+","+sPValue+","+sPNoofItems+","+sPLength+","+sPWidth+","+sPHeight+","+sPWeight+","+sPCubic+","+sPIndemnity+",'"+sSessionID+"')");
            stmInsert.executeUpdate("Insert into pro_product (pro_noitems,pro_length,pro_width,pro_height, "//pro_iweight ,"
                    + "pro_sessionid)values(" + sPNoofItems + "," + sPLength + "," + sPWidth + "," + sPHeight + "," 
//                    + sPWeight + ",'" 
                    + "'"
                    + (validSession.getAttribute("sessionid").toString()) + "')");
            stmInsert.close();

            vCloseConnection(conProduct);

        } catch (Exception errInsert) {
            sErrorMessage = "Error inserting record - " + errInsert.toString();
            vShowErrorPage(rspOutput, errInsert);
        }
    }

    /**
     *
     * @param sCommodityTags
     * @param validSession
     * @param rspOutput
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public String addCommodityGridRow(String sCommodityTags, HttpSession validSession, HttpServletResponse rspOutput) throws ServletException, IOException {

        String sPIndemnity = "";
        String sPCode = "";
        String sPDescription = "D";
        String sPValue = "";
        String sPQuantity = "";
        String[] sEditedRow1;
        String sError = "";
        String sTempIndemnity = "";

        sEditedRow1 = sCommodityTags.split(",");

        CommodityInfo commodity = new CommodityInfo();
        commodity.parseCommodity((String[][]) validSession.getAttribute("getCommodityArray"));

        try {


            if (sEditedRow1[0] == null) {
                sPDescription = "''";
            } else {

                //sPDescription = sEditedRow1[0];

          // dc - 9/12/2009 - strip commas and single quotes
          //      sPDescription =sEditedRow1[0];
                  sPDescription = sReplaceChars(sEditedRow1[0], "'", "");
                  sPDescription = sReplaceChars(sPDescription, ",", ";");

            }

            if (sEditedRow1[1] == null || sEditedRow1[1].length() == 0) {
                sPQuantity = "0";
            } else {
                sPQuantity = sReplaceChars(sEditedRow1[1], "'", "''");
            }

            if (sEditedRow1[2] == null || sEditedRow1[2].length() == 0) {
                sPValue = "0.0";
            } else {
                sPValue = sReplaceChars(sEditedRow1[2], "'", "''");
            }

            if (sEditedRow1[3] == null) {
                sPIndemnity = "0";
            } else {
                if (sEditedRow1[3].equals("YES")) {
                    sPIndemnity = "1";
                } else {
                    sPIndemnity = "0";
                }
            }

            if ((commodity.getCsVector().indexOf(sPDescription.toUpperCase())) != -1) {
                sPCode = "'" + commodity.getCcVector().elementAt(commodity.getCsVector().indexOf(sPDescription.toUpperCase())).toString() + "'";
                sTempIndemnity = commodity.getIfVector().elementAt(commodity.getCsVector().indexOf(sPDescription.toUpperCase())).toString();
            } else {
                sPCode = "''";
            }


            sPDescription = "'" + sReplaceChars(sPDescription, "'", "''") + "'";

            if (sPCode == "''" && sPIndemnity == "1" || (sTempIndemnity.equals("N") && sPIndemnity.equals("1"))) {
                sError = "Indemnity cannot be applied for "+sPDescription+". Please contact your local CitySprint ServiceCentre to complete your booking";
            } else {
                Connection conCommodity = null;
                conCommodity = cOpenConnection();
                Statement stmInsert = conCommodity.createStatement();
                String sInsert = "";
                // stmInsert.executeUpdate("Insert into pro_Product (pro_Type,pro_Description,pro_Value,pro_NoItems,pro_Length,pro_Width,pro_Height,pro_Weight,pro_cubic,pro_Indemnity,pro_SessionId)values("+sPType+","+sPDescription+","+sPValue+","+sPNoofItems+","+sPLength+","+sPWidth+","+sPHeight+","+sPWeight+","+sPCubic+","+sPIndemnity+",'"+sSessionID+"')");
                stmInsert.executeUpdate("Insert into com_commodity (com_code,com_description,com_quantity,com_value,com_indemnity,com_sessionid)values(" + sPCode + "," + sPDescription + "," + sPQuantity + "," + sPValue + "," + sPIndemnity + ",'" + (validSession.getAttribute("sessionid").toString()) + "')");
                stmInsert.close();
                vCloseConnection(conCommodity);
            }

        } catch (Exception errInsert) {
            sErrorMessage = "Error inserting record - " + errInsert.toString();

            vShowErrorPage(rspOutput, errInsert);
        }

        return sError;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     * @return
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
