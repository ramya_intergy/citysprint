package citysprint.co.uk.NDI;

/*
 * TrackingNotifications.java
 *
 * Created on 2 October 2007, 09:10
 */
import citysprint.co.uk.NDI.LoginInfo;
import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;
import com.qas.proweb.servlet.*;

/**
 *
 * @author ramyas
 * @version
 */
public class TrackingNotifications extends CommonClass {

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            if (isLogin(request)) {


                //Common Variables
                String sAction = "", sType = "";

                String[][] sTrackingEmail = new String[5][5];
                String[][] sTrackingSMS = new String[5][5];
                String sParam_Email = "Email";
                String sParam_SMS = "SMS";
                String emailArray[][] = new String[5][5];                   //Tracking email notifications
                String smsArray[][] = new String[5][5];                     //tracking SMs notifications

                sAction = request.getParameter(Constants.SACTION) == null ? "" : request.getParameter(Constants.SACTION).toString();
                HttpSession validSession = request.getSession(false);

                //initialize config values
                initializeConfigValues(request, response);

                //Read session values
                //getSessionValues(request);
                //parseLogin(loginArray);

                if (sAction.equals(Constants.sBooking) || sAction.equals(Constants.sConfirm)) {
                    String sTrackingEmail1 = request.getParameter("sTrackingEmail") == null ? "" : request.getParameter("sTrackingEmail").toString();
                    String sTrackingSMS1 = request.getParameter("sTrackingSMS") == null ? "" : request.getParameter("sTrackingSMS").toString();
                    String sEmailNotify1 = request.getParameter("sEmailNotify") == null ? "" : request.getParameter("sEmailNotify").toString();
                    String sSMSNotify1 = request.getParameter("sSMSNotify") == null ? "" : request.getParameter("sSMSNotify").toString();
                    sType = request.getParameter("sType") == null ? "" : request.getParameter("sType").toString();

                    emailArray = null;
                    emailArray = validSession.getAttribute("sTrackingEmail") == null ? null : (String[][]) validSession.getAttribute("sTrackingEmail");
                    smsArray = null;
                    smsArray = validSession.getAttribute("sTrackingSMS") == null ? null : (String[][]) validSession.getAttribute("sTrackingSMS");

                    if (emailArray != null) {
                        for (int i = 0; i < 5; i++) {
                            if (i == 0 && !sAction.equals(Constants.sConfirm)) {
                                sTrackingEmail[i][0] = sTrackingEmail1;
                                sTrackingEmail[i][1] = sEmailNotify1;
                            } else {
                                sTrackingEmail[i][0] = emailArray[i][0] == null ? "" : emailArray[i][0];
                                sTrackingEmail[i][1] = emailArray[i][1] == null ? "" : emailArray[i][1];
                            }
                        }
                    } else {
                        for (int i = 0; i < 5; i++) {
                            if (i == 0 && !sAction.equals(Constants.sConfirm)) {
                                sTrackingEmail[i][0] = sTrackingEmail1;
                                sTrackingEmail[i][1] = sEmailNotify1;
                            } else {
                                sTrackingEmail[i][0] = "";
                                sTrackingEmail[i][1] = "";
                            }
                        }
                    }
                    if (smsArray != null) {
                        for (int i = 0; i < 5; i++) {
                            if (i == 0 && !sAction.equals(Constants.sConfirm)) {
                                sTrackingSMS[i][0] = sTrackingSMS1;
                                sTrackingSMS[i][1] = sSMSNotify1;
                            } else {
                                sTrackingSMS[i][0] = smsArray[i][0] == null ? "" : smsArray[i][0];
                                sTrackingSMS[i][1] = smsArray[i][1] == null ? "" : smsArray[i][1];
                            }
                        }
                    } else {
                        for (int i = 0; i < 5; i++) {
                            if (i == 0 && !sAction.equals(Constants.sConfirm)) {
                                sTrackingSMS[i][0] = sTrackingSMS1;
                                sTrackingSMS[i][1] = sSMSNotify1;
                            } else {
                                sTrackingSMS[i][0] = "";
                                sTrackingSMS[i][1] = "";
                            }
                        }

                    }

                    LoadPage(request, response, sTrackingEmail, sTrackingSMS, sAction, sType);

                    //set the checkboxes
                    for (int i = 0; i < 5; i++) {
                        String sI = "" + (i + 1);
                        setCheckboxes(sTrackingEmail[i][1], sParam_Email, sI, response);
                        setCheckboxes(sTrackingSMS[i][1], sParam_SMS, sI, response);
                    }

                }

                if (sAction.equals(Constants.sSave)) {
                    saveNotifications(request, response);
                }

                if (sAction.equals(Constants.sConfirm)) {
                    disableInputBoxes(request, response);
                }

            } else {
                writeOutputLog("Session expired redirecting to index page");
                response.sendRedirect("index.jsp");
            }

        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void LoadPage(HttpServletRequest request, HttpServletResponse response, String[][] sTrackingEmail, String[][] sTrackingSMS, String sAction, String sType) throws ServletException, IOException {

        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");

        HttpSession validSession = request.getSession(false);
        LoginInfo login = new LoginInfo();
        login.parseLogin((String[][]) validSession.getAttribute("loginArray"), request);

        strmResponse.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
        strmResponse.println("<html>");
        strmResponse.println("    <head>");
        strmResponse.println("      <title>CitySprint - Multiple Tracking Notifications</title>");
        strmResponse.println("      <link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
        strmResponse.println("      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");

        strmResponse.println("<script language=javascript>");

        strmResponse.println("function saveNotifs()");
        strmResponse.println("{");
        //strmResponse.println("alert('inside fn');");

        if (login.isEmailAccountFlag()) {
            strmResponse.println("var txtEmail1=document.getElementById(\"txtEmail1\");");
            strmResponse.println("var txtEmail2=document.getElementById(\"txtEmail2\");");
            strmResponse.println("var txtEmail3=document.getElementById(\"txtEmail3\");");
            strmResponse.println("var txtEmail4=document.getElementById(\"txtEmail4\");");
            strmResponse.println("var txtEmail5=document.getElementById(\"txtEmail5\");");
            strmResponse.println("var chkBookingEmail1=document.getElementById(\"chkBookingEmail1\");");
            strmResponse.println("var chkBookingEmail2=document.getElementById(\"chkBookingEmail2\");");
            strmResponse.println("var chkBookingEmail3=document.getElementById(\"chkBookingEmail3\");");
            strmResponse.println("var chkBookingEmail4=document.getElementById(\"chkBookingEmail4\");");
            strmResponse.println("var chkBookingEmail5=document.getElementById(\"chkBookingEmail5\");");
            strmResponse.println("var chkCollectionEmail1=document.getElementById(\"chkCollectionEmail1\");");
            strmResponse.println("var chkCollectionEmail2=document.getElementById(\"chkCollectionEmail2\");");
            strmResponse.println("var chkCollectionEmail3=document.getElementById(\"chkCollectionEmail3\");");
            strmResponse.println("var chkCollectionEmail4=document.getElementById(\"chkCollectionEmail4\");");
            strmResponse.println("var chkCollectionEmail5=document.getElementById(\"chkCollectionEmail5\");");
            strmResponse.println("var chkDeliveryEmail1=document.getElementById(\"chkDeliveryEmail1\");");
            strmResponse.println("var chkDeliveryEmail2=document.getElementById(\"chkDeliveryEmail2\");");
            strmResponse.println("var chkDeliveryEmail3=document.getElementById(\"chkDeliveryEmail3\");");
            strmResponse.println("var chkDeliveryEmail4=document.getElementById(\"chkDeliveryEmail4\");");
            strmResponse.println("var chkDeliveryEmail5=document.getElementById(\"chkDeliveryEmail5\");");
        }

        if (login.isSmsAccountFlag()) {
            strmResponse.println("var txtSMS1=document.getElementById(\"txtSMS1\");");
            strmResponse.println("var txtSMS2=document.getElementById(\"txtSMS2\");");
            strmResponse.println("var txtSMS3=document.getElementById(\"txtSMS3\");");
            strmResponse.println("var txtSMS4=document.getElementById(\"txtSMS4\");");
            strmResponse.println("var txtSMS5=document.getElementById(\"txtSMS5\");");
            strmResponse.println("var chkBookingSMS1=document.getElementById(\"chkBookingSMS1\");");
            strmResponse.println("var chkBookingSMS2=document.getElementById(\"chkBookingSMS2\");");
            strmResponse.println("var chkBookingSMS3=document.getElementById(\"chkBookingSMS3\");");
            strmResponse.println("var chkBookingSMS4=document.getElementById(\"chkBookingSMS4\");");
            strmResponse.println("var chkBookingSMS5=document.getElementById(\"chkBookingSMS5\");");
            strmResponse.println("var chkCollectionSMS1=document.getElementById(\"chkCollectionSMS1\");");
            strmResponse.println("var chkCollectionSMS2=document.getElementById(\"chkCollectionSMS2\");");
            strmResponse.println("var chkCollectionSMS3=document.getElementById(\"chkCollectionSMS3\");");
            strmResponse.println("var chkCollectionSMS4=document.getElementById(\"chkCollectionSMS4\");");
            strmResponse.println("var chkCollectionSMS5=document.getElementById(\"chkCollectionSMS5\");");
            strmResponse.println("var chkDeliverySMS1=document.getElementById(\"chkDeliverySMS1\");");
            strmResponse.println("var chkDeliverySMS2=document.getElementById(\"chkDeliverySMS2\");");
            strmResponse.println("var chkDeliverySMS3=document.getElementById(\"chkDeliverySMS3\");");
            strmResponse.println("var chkDeliverySMS4=document.getElementById(\"chkDeliverySMS4\");");
            strmResponse.println("var chkDeliverySMS5=document.getElementById(\"chkDeliverySMS5\");");
        }
//        strmResponse.println("var chkDelayedEmail1=document.getElementById(\"chkDelayedEmail1\");");
//        strmResponse.println("var chkDelayedEmail2=document.getElementById(\"chkDelayedEmail2\");");
//        strmResponse.println("var chkDelayedEmail3=document.getElementById(\"chkDelayedEmail3\");");
//        strmResponse.println("var chkDelayedEmail4=document.getElementById(\"chkDelayedEmail4\");");
//        strmResponse.println("var chkDelayedEmail5=document.getElementById(\"chkDelayedEmail5\");");
        strmResponse.println("var lblMessage=document.getElementById(\"lblMessage\");");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText=\"\";");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = \"\";");
        strmResponse.println("var error;");
        strmResponse.println("var message=\"\";");

        if (login.isEmailAccountFlag()) {
            strmResponse.println("if(chkBookingEmail2.checked || chkCollectionEmail2.checked || chkDeliveryEmail2.checked )");
            strmResponse.println("{");
            strmResponse.println("if(!validateEmail(txtEmail2.value))");
            strmResponse.println("{");
            strmResponse.println("message=\"Tracking Email2 is Missing or Invalid.\\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");
            strmResponse.println("}");
            strmResponse.println("else");
            strmResponse.println("if((txtEmail2.value).length>0)");
            strmResponse.println("{");
            strmResponse.println("if(!chkBookingEmail2.checked && !chkCollectionEmail2.checked && !chkDeliveryEmail2.checked )");
            strmResponse.println("{");
            strmResponse.println("	message=\"Please tick any one of the Tracking Notification Type (Booking|Collection|Delivery) of Email2.\\n\";");
            strmResponse.println("	error=true; ");
            strmResponse.println("}");
            strmResponse.println("}");

            strmResponse.println("if(chkBookingEmail3.checked || chkCollectionEmail3.checked || chkDeliveryEmail3.checked)");
            strmResponse.println("{");
            strmResponse.println("if(!validateEmail(txtEmail3.value))");
            strmResponse.println("{");
            strmResponse.println("message=\"Tracking Email3 is Missing or Invalid \\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");
            strmResponse.println("}");
            strmResponse.println("else");
            strmResponse.println("if((txtEmail3.value).length>0)");
            strmResponse.println("{");
            strmResponse.println("if(!chkBookingEmail3.checked && !chkCollectionEmail3.checked && !chkDeliveryEmail3.checked)");
            strmResponse.println("{");
            strmResponse.println("	message=\"Please tick any one of the Tracking Notification Type (Booking|Collection|Delivery) of Email3 \\n\";");
            strmResponse.println("	error=true; ");
            strmResponse.println("}");
            strmResponse.println("}");

            strmResponse.println("if(chkBookingEmail4.checked || chkCollectionEmail4.checked || chkDeliveryEmail4.checked)");
            strmResponse.println("{");
            strmResponse.println("if(!validateEmail(txtEmail4.value))");
            strmResponse.println("{");
            strmResponse.println("message=\"Tracking Email4 is Missing or Invalid \\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");
            strmResponse.println("}");
            strmResponse.println("else");
            strmResponse.println("if((txtEmail4.value).length>0)");
            strmResponse.println("{");
            strmResponse.println("if(!chkBookingEmail4.checked && !chkCollectionEmail4.checked && !chkDeliveryEmail4.checked)");
            strmResponse.println("{");
            strmResponse.println("	message=\"Please tick any one of the Tracking Notification Type (Booking|Collection|Delivery) of Email4 \\n\";");
            strmResponse.println("	error=true; ");
            strmResponse.println("}");
            strmResponse.println("}");

            strmResponse.println("if(chkBookingEmail5.checked || chkCollectionEmail5.checked || chkDeliveryEmail5.checked)");
            strmResponse.println("{");
            strmResponse.println("if(!validateEmail(txtEmail5.value))");
            strmResponse.println("{");
            strmResponse.println("message=\"Tracking Email5 is Missing or Invalid \\n\";");
            strmResponse.println("error=true;");
            strmResponse.println("}");
            strmResponse.println("}");
            strmResponse.println("else");
            strmResponse.println("if((txtEmail5.value).length>0)");
            strmResponse.println("{");
            strmResponse.println("if(!chkBookingEmail5.checked && !chkCollectionEmail5.checked && !chkDeliveryEmail5.checked)");
            strmResponse.println("{");
            strmResponse.println("	message=\"Please tick any one of the Tracking Notification Type (Booking|Collection|Delivery) of Email5 \\n\";");
            strmResponse.println("	error=true; ");
            strmResponse.println("}");
            strmResponse.println("}");
        }

        if (login.isSmsAccountFlag()) {
            strmResponse.println("if(chkBookingSMS2.checked || chkCollectionSMS2.checked || chkDeliverySMS2.checked)");
            strmResponse.println("{");
//            strmResponse.println("if(!validateMobNo(txtSMS2.value))");
//            strmResponse.println("{");
//            strmResponse.println("message=\"Tracking SMS2 is Missing or Invalid \\n\";");
//            strmResponse.println("error=true;");
//            strmResponse.println("}");
            strmResponse.println("}");
            strmResponse.println("else");
            strmResponse.println("if((txtSMS2.value).length>0)");
            strmResponse.println("{");
            strmResponse.println("if(!chkBookingSMS2.checked && !chkCollectionSMS2.checked && !chkDeliverySMS2.checked )");
            strmResponse.println("{");
            strmResponse.println("	message=\"Please tick any one of the Tracking Notification Type (Booking|Collection|Delivery) of SMS2\\n\";");
            strmResponse.println("	error=true; ");
            strmResponse.println("}");
            strmResponse.println("}");

            strmResponse.println("if(chkBookingSMS3.checked || chkCollectionSMS3.checked || chkDeliverySMS3.checked )");
            strmResponse.println("{");
//            strmResponse.println("if(!validateMobNo(txtSMS3.value))");
//            strmResponse.println("{");
//            strmResponse.println("message=\"Tracking SMS3 is Missing or Invalid \\n\";");
//            strmResponse.println("error=true;");
//            strmResponse.println("}");
            strmResponse.println("}");
            strmResponse.println("else");
            strmResponse.println("if((txtSMS3.value).length>0)");
            strmResponse.println("{");
            strmResponse.println("if(!chkBookingSMS3.checked && !chkCollectionSMS3.checked && !chkDeliverySMS3.checked )");
            strmResponse.println("{");
            strmResponse.println("	message=\"Please tick any one of the Tracking Notification Type (Booking|Collection|Delivery) of SMS3\\n\";");
            strmResponse.println("	error=true; ");
            strmResponse.println("}");
            strmResponse.println("}");

            strmResponse.println("if(chkBookingSMS4.checked || chkCollectionSMS4.checked || chkDeliverySMS4.checked )");
            strmResponse.println("{");
//            strmResponse.println("if(!validateMobNo(txtSMS4.value))");
//            strmResponse.println("{");
//            strmResponse.println("message=\"Tracking SMS4 is Missing or Invalid \\n\";");
//            strmResponse.println("error=true;");
//            strmResponse.println("}");
            strmResponse.println("}");
            strmResponse.println("else");
            strmResponse.println("if((txtSMS4.value).length>0)");
            strmResponse.println("{");
            strmResponse.println("if(!chkBookingSMS4.checked && !chkCollectionSMS4.checked && !chkDeliverySMS4.checked)");
            strmResponse.println("{");
            strmResponse.println("	message=\"Please tick any one of the Tracking Notification Type (Booking|Collection|Delivery) of SMS4\\n\";");
            strmResponse.println("	error=true; ");
            strmResponse.println("}");
            strmResponse.println("}");

            strmResponse.println("if(chkBookingSMS5.checked || chkCollectionSMS5.checked || chkDeliverySMS5.checked)");
            strmResponse.println("{");
//            strmResponse.println("if(!validateMobNo(txtSMS5.value))");
//            strmResponse.println("{");
//            strmResponse.println("message=\"Tracking SMS5 is Missing or Invalid \\n\";");
//            strmResponse.println("error=true;");
//            strmResponse.println("}");
            strmResponse.println("}");
            strmResponse.println("else");
            strmResponse.println("if((txtSMS5.value).length>0)");
            strmResponse.println("{");
            strmResponse.println("if(!chkBookingSMS5.checked && !chkCollectionSMS5.checked && !chkDeliverySMS5.checked)");
            strmResponse.println("{");
            strmResponse.println("	message=\"Please tick any one of the Tracking Notification Type (Booking|Collection|Delivery) of SMS5\\n\";");
            strmResponse.println("	error=true; ");
            strmResponse.println("}");
            strmResponse.println("}");
        }

        strmResponse.println("if(error==true){");
        strmResponse.println("if(document.all)");
        strmResponse.println("lblMessage.innerText=message;");
        strmResponse.println("else");
        strmResponse.println("lblMessage.textContent = message;}");
        strmResponse.println("else");
        strmResponse.println("{");

        if (login.isEmailAccountFlag()) {
            strmResponse.println("document.TrackingForm.sEmail1.value=txtEmail1.value;");
            strmResponse.println("document.TrackingForm.sEmail2.value=txtEmail2.value;");
            strmResponse.println("document.TrackingForm.sEmail3.value=txtEmail3.value;");
            strmResponse.println("document.TrackingForm.sEmail4.value=txtEmail4.value;");
            strmResponse.println("document.TrackingForm.sEmail5.value=txtEmail5.value;");
            strmResponse.println("document.TrackingForm.sEmailNotify1.value=getNotifyType('Email1');");
            strmResponse.println("document.TrackingForm.sEmailNotify2.value=getNotifyType('Email2');");
            strmResponse.println("document.TrackingForm.sEmailNotify3.value=getNotifyType('Email3');");
            strmResponse.println("document.TrackingForm.sEmailNotify4.value=getNotifyType('Email4');");
            strmResponse.println("document.TrackingForm.sEmailNotify5.value=getNotifyType('Email5');");
        }

        if (login.isSmsAccountFlag()) {
            strmResponse.println("document.TrackingForm.sSMS1.value=txtSMS1.value;");
            strmResponse.println("document.TrackingForm.sSMS2.value=txtSMS2.value;");
            strmResponse.println("document.TrackingForm.sSMS3.value=txtSMS3.value;");
            strmResponse.println("document.TrackingForm.sSMS4.value=txtSMS4.value;");
            strmResponse.println("document.TrackingForm.sSMS5.value=txtSMS5.value;");

            strmResponse.println("document.TrackingForm.sSMSNotify1.value=getNotifyType('SMS1');");
            strmResponse.println("document.TrackingForm.sSMSNotify2.value=getNotifyType('SMS2');");
            strmResponse.println("document.TrackingForm.sSMSNotify3.value=getNotifyType('SMS3');");
            strmResponse.println("document.TrackingForm.sSMSNotify4.value=getNotifyType('SMS4');");
            strmResponse.println("document.TrackingForm.sSMSNotify5.value=getNotifyType('SMS5');");
        }

        strmResponse.println("document.TrackingForm.submit();");
        strmResponse.println("}");
        strmResponse.println("}");

        //form the Notify type based on checkboxes selected.
        strmResponse.println("function getNotifyType(sType)");
        strmResponse.println("{");
        strmResponse.println("var sNotify=\"\";");
        strmResponse.println("var chkNotify=document.getElementById(\"chkBooking\"+sType);");
        strmResponse.println("if(chkNotify.checked)");
        strmResponse.println("sNotify=\"B\";");
        strmResponse.println("chkNotify=document.getElementById(\"chkCollection\"+sType);");
        strmResponse.println("if(chkNotify.checked)");
        strmResponse.println("sNotify=sNotify+\"C\";");
        strmResponse.println("chkNotify=document.getElementById(\"chkDelivery\"+sType);");
        strmResponse.println("if(chkNotify.checked)");
        strmResponse.println("sNotify=sNotify+\"D\";");
//        strmResponse.println("chkNotify=document.getElementById(\"chkDelayed\"+sType);");
//        strmResponse.println("if(chkNotify.checked)");
//        strmResponse.println("sNotify=sNotify+\"E\";");
        strmResponse.println("return sNotify;");
        strmResponse.println("}");

        //Validate email address.
        strmResponse.println("function validateEmail(sEmail)");
        strmResponse.println("{");
        strmResponse.println("var result=false;");
        strmResponse.println("var nIndex = sEmail.indexOf('@');");
        strmResponse.println("var nIndexDot = sEmail.indexOf('.');");
        strmResponse.println("if(nIndex == -1 || nIndexDot == -1)");
        strmResponse.println("result=false;");
        strmResponse.println("else");
        strmResponse.println("result=true;");
        strmResponse.println("return result;");
        strmResponse.println("}");

        //Validate Mobile number
        strmResponse.println("function validateMobNo(tempstr)");
        strmResponse.println("{");
        //strmResponse.println("alert('inside fn');");
        strmResponse.println("var result=false;");
        strmResponse.println("if((tempstr.length==11) && (tempstr.substring(0,3)==\"077\" || tempstr.substring(0,3)==\"078\" || tempstr.substring(0,3)==\"079\"))");
        strmResponse.println("{");
        strmResponse.println("  result=true; ");
        strmResponse.println("}");
        strmResponse.println("else");
        //  strmResponse.println("alert(tempstr.substring(0,3));");
        strmResponse.println("if(tempstr.length==12 && tempstr.substring(0,4)==\"+614\")");
        strmResponse.println("{");
        strmResponse.println(" result=true;");
        strmResponse.println("}");
        strmResponse.println("return result;");
        strmResponse.println("}");

        strmResponse.println("</script>");

        strmResponse.println("    </head>");
        strmResponse.println("    <body >");
        strmResponse.println("    <br>");
        if (ff) {
            strmResponse.println("    <table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"80%\" align=\"left\" id=\"tblTracking\">");
        } else {
            strmResponse.println("    <table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" align=\"center\" id=\"tblTracking\">");
        }

        strmResponse.println("          <tr>");
        strmResponse.println("              <td>");
        strmResponse.println("                  <label id=\"lblMessage\" style=\"color:red;\">");
        strmResponse.println("              </td></tr>");
        strmResponse.println("              <tr><td>");

        if (login.isEmailAccountFlag()) {
            strmResponse.println("                  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"box\" width=\"100%\" align=\"center\">");
            strmResponse.println("                      <tr>");
            strmResponse.println("                          <td id=\"boxtitle\" colspan=\"5\">");
            strmResponse.println("                              Email Notifications");
            strmResponse.println("                          </td>");
            strmResponse.println("");
            strmResponse.println("                      </tr>");
            strmResponse.println("                      <tr>");
            strmResponse.println("                          <td>");

            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <b>Booking</b> ");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <b>Collection</b> ");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <b>Delivery</b> ");
            strmResponse.println("                          </td>");
            //        strmResponse.println("                          <td><b>Delayed</b>");
            //        strmResponse.println("                          </td>");
            strmResponse.println("                      </tr>");
            strmResponse.println("                      <tr>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              1. Email Address : <input type=\"text\" style=\"text-transform:uppercase;\" size=\"40\" name=\"txtEmail1\" id=\"txtEmail1\" value=\"" + sTrackingEmail[0][0] + "\" disabled>");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkBookingEmail1\" id=\"chkBookingEmail1\" disabled>");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkCollectionEmail1\" id=\"chkCollectionEmail1\" disabled>");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkDeliveryEmail1\" id=\"chkDeliveryEmail1\" disabled>");
            strmResponse.println("                          </td>");
            //        strmResponse.println("                          <td>");
            //        strmResponse.println("                              <input type=\"checkbox\" name=\"chkDelayedEmail1\" disabled>");
            //        strmResponse.println("                          </td>");
            strmResponse.println("                      </tr>");
            strmResponse.println("                      <tr>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              2. Email Address : <input type=\"text\" style=\"text-transform:uppercase;\" size=\"40\" name=\"txtEmail2\" id=\"txtEmail2\" value=\"" + sTrackingEmail[1][0] + "\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkBookingEmail2\" id=\"chkBookingEmail2\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkCollectionEmail2\" id=\"chkCollectionEmail2\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkDeliveryEmail2\" id=\"chkDeliveryEmail2\">");
            strmResponse.println("                          </td>");
            //        strmResponse.println("                          <td>");
            //        strmResponse.println("                              <input type=\"checkbox\" name=\"chkDelayedEmail2\">");
            //        strmResponse.println("                          </td>");
            strmResponse.println("                      </tr>");
            strmResponse.println("                      <tr>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              3. Email Address : <input type=\"text\" style=\"text-transform:uppercase;\" size=\"40\" name=\"txtEmail3\" id=\"txtEmail3\" value=\"" + sTrackingEmail[2][0] + "\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkBookingEmail3\" id=\"chkBookingEmail3\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkCollectionEmail3\" id=\"chkCollectionEmail3\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkDeliveryEmail3\" id=\"chkDeliveryEmail3\">");
            strmResponse.println("                          </td>");
            //        strmResponse.println("                          <td>");
            //        strmResponse.println("                              <input type=\"checkbox\" name=\"chkDelayedEmail3\">");
            //        strmResponse.println("                          </td>");
            strmResponse.println("                      </tr>");
            strmResponse.println("                      <tr>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              4. Email Address : <input type=\"text\" style=\"text-transform:uppercase;\" size=\"40\" name=\"txtEmail4\" id=\"txtEmail4\" value=\"" + sTrackingEmail[3][0] + "\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkBookingEmail4\" id=\"chkBookingEmail4\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkCollectionEmail4\" id=\"chkCollectionEmail4\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkDeliveryEmail4\" id=\"chkDeliveryEmail4\">");
            strmResponse.println("                          </td>");
            //        strmResponse.println("                          <td>");
            //        strmResponse.println("                              <input type=\"checkbox\" name=\"chkDelayedEmail4\">");
            //        strmResponse.println("                          </td>");
            strmResponse.println("                      </tr>");
            strmResponse.println("                      <tr>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              5. Email Address : <input type=\"text\" style=\"text-transform:uppercase;\" size=\"40\" name=\"txtEmail5\" id=\"txtEmail5\" value=\"" + sTrackingEmail[4][0] + "\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkBookingEmail5\" id=\"chkBookingEmail5\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkCollectionEmail5\" id=\"chkCollectionEmail5\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkDeliveryEmail5\" id=\"chkDeliveryEmail5\">");
            strmResponse.println("                          </td>");
            //        strmResponse.println("                          <td>");
            //        strmResponse.println("                              <input type=\"checkbox\" name=\"chkDelayedEmail5\">");
            //        strmResponse.println("                          </td>");
            strmResponse.println("                      </tr>");
            strmResponse.println("                  </table>");
        }

        strmResponse.println("              </td>");
        strmResponse.println("          </tr>");
        strmResponse.println("          <tr>");
        strmResponse.println("              <td> <br>");

        if (login.isSmsAccountFlag()) {
            strmResponse.println("                 <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"box\" width=\"100%\" align=\"center\">");
            strmResponse.println("                      <tr>");
            strmResponse.println("                          <td id=\"boxtitle\" colspan=\"5\">");
            strmResponse.println("                              SMS Notifications");
            strmResponse.println("                          </td>");
            strmResponse.println("                      </tr>");
            strmResponse.println("                      <tr>");
            strmResponse.println("                          <td>");
            strmResponse.println("");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <b>Booking</b> ");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <b>Collection</b> ");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <b>Delivery</b> ");
            strmResponse.println("                          </td>");
            //        strmResponse.println("                          <td><b>Delayed</b>");
            //        strmResponse.println("                          </td>");
            strmResponse.println("                      </tr>");
            strmResponse.println("                      <tr>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              1. Mobile Number : <input type=\"text\" size=\"40\" style=\"text-transform:uppercase;\" name=\"txtSMS1\" id=\"txtSMS1\" value=\"" + sTrackingSMS[0][0] + "\" disabled>");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkBookingSMS1\" id=\"chkBookingSMS1\" disabled>");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkCollectionSMS1\" id=\"chkCollectionSMS1\" disabled>");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkDeliverySMS1\" id=\"chkDeliverySMS1\" disabled>");
            strmResponse.println("                          </td>");
            //        strmResponse.println("                          <td>");
            //        strmResponse.println("                              <input type=\"checkbox\" name=\"chkDelayedSMS1\" disabled>");
            //        strmResponse.println("                          </td>");
            strmResponse.println("                      </tr>");
            strmResponse.println("                      <tr>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              2. Mobile Number : <input type=\"text\" size=\"40\" style=\"text-transform:uppercase;\" name=\"txtSMS2\" id=\"txtSMS2\" value=\"" + sTrackingSMS[1][0] + "\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkBookingSMS2\" id=\"chkBookingSMS2\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkCollectionSMS2\" id=\"chkCollectionSMS2\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkDeliverySMS2\" id=\"chkDeliverySMS2\">");
            strmResponse.println("                          </td>");
            //        strmResponse.println("                          <td>");
            //        strmResponse.println("                              <input type=\"checkbox\" name=\"chkDelayedSMS2\">");
            //        strmResponse.println("                          </td>");
            strmResponse.println("                      </tr>");
            strmResponse.println("                      <tr>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              3. Mobile Number : <input type=\"text\" size=\"40\" style=\"text-transform:uppercase;\" name=\"txtSMS3\" id=\"txtSMS3\" value=\"" + sTrackingSMS[2][0] + "\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkBookingSMS3\" id=\"chkBookingSMS3\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkCollectionSMS3\" id=\"chkCollectionSMS3\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkDeliverySMS3\" id=\"chkDeliverySMS3\">");
            strmResponse.println("                          </td>");
            //        strmResponse.println("                          <td>");
            //        strmResponse.println("                              <input type=\"checkbox\" name=\"chkDelayedSMS3\">");
            //        strmResponse.println("                          </td>");
            strmResponse.println("                      </tr>");
            strmResponse.println("                      <tr>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              4. Mobile Number : <input type=\"text\" size=\"40\" style=\"text-transform:uppercase;\" name=\"txtSMS4\" id=\"txtSMS4\" value=\"" + sTrackingSMS[3][0] + "\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkBookingSMS4\" id=\"chkBookingSMS4\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkCollectionSMS4\" id=\"chkCollectionSMS4\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkDeliverySMS4\" id=\"chkDeliverySMS4\">");
            strmResponse.println("                          </td>");
            //        strmResponse.println("                          <td>");
            //        strmResponse.println("                              <input type=\"checkbox\" name=\"chkDelayedSMS4\">");
            //        strmResponse.println("                          </td>");
            strmResponse.println("                      </tr>");
            strmResponse.println("                      <tr>");
            strmResponse.println("                         <td>");
            strmResponse.println("                              5. Mobile Number : <input type=\"text\" size=\"40\" style=\"text-transform:uppercase;\" name=\"txtSMS5\" id=\"txtSMS5\" value=\"" + sTrackingSMS[4][0] + "\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkBookingSMS5\" id=\"chkBookingSMS5\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkCollectionSMS5\" id=\"chkCollectionSMS5\">");
            strmResponse.println("                          </td>");
            strmResponse.println("                          <td>");
            strmResponse.println("                              <input type=\"checkbox\" name=\"chkDeliverySMS5\" id=\"chkDeliverySMS5\">");
            strmResponse.println("                          </td>");
            //        strmResponse.println("                          <td>");
            //        strmResponse.println("                              <input type=\"checkbox\" name=\"chkDelayedSMS5\">");
            //        strmResponse.println("                           </td>");
            strmResponse.println("                      </tr>");
            strmResponse.println("                  </table>");
        }
        strmResponse.println("              </td>");
        strmResponse.println("          </tr>");
        strmResponse.println("          <tr>");

        if (!sAction.equals(Constants.sConfirm)) {
            strmResponse.println("              <td><br><input type=\"button\" id=\"btn\" value=\"Save & Return to Booking Screen\" onclick=\"javascript:saveNotifs();\"> &nbsp; " +
                    "<input type=\"button\" id=\"btn\" value=\"Cancel\" onclick=\"window.close();\"</td>");
        }

        strmResponse.println("          </tr>");
        strmResponse.println("</table>");
        strmResponse.println("      </body>");

        strmResponse.println("<form name=\"TrackingForm\" method=\"post\" action=\"" + Constants.srvTrackingNotifications + "\">");
        strmResponse.println("<input type=\"hidden\" name=\"sEmail1\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sEmail2\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sEmail3\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sEmail4\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sEmail5\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sSMS1\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sSMS2\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sSMS3\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sSMS4\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sSMS5\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sEmailNotify1\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sEmailNotify2\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sEmailNotify3\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sEmailNotify4\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sEmailNotify5\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sSMSNotify1\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sSMSNotify2\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sSMSNotify3\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sSMSNotify4\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"sSMSNotify5\" value=\"\">");
        strmResponse.println("<input type=\"hidden\" name=\"" + Constants.SACTION + "\" value=\"" + Constants.sSave + "\"");
        strmResponse.println("</form>");
        strmResponse.println("</html>");

    }

    /**
     *
     * @param sInput
     * @param sType
     * @param sI
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void setCheckboxes(String sInput, String sType, String sI, HttpServletResponse response) throws ServletException, IOException {
        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");

        strmResponse.println("<html>");
        strmResponse.println("<head></head>");
        strmResponse.println("<body>");

        if (sInput.length() > 0) {
            for (int i = 0; i < sInput.length(); i++) {
                if (sInput.charAt(i) == 'B') {
                    strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">var chkNotify=document.getElementById(\"chkBooking" + sType + sI + "\");");
                    strmResponse.println("chkNotify.checked=true;</script>");
                }

                if (sInput.charAt(i) == 'C') {
                    strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">var chkNotify=document.getElementById(\"chkCollection" + sType + sI + "\");");
                    strmResponse.println("chkNotify.checked=true;</script>");
                }
                if (sInput.charAt(i) == 'D') {
                    strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">var chkNotify=document.getElementById(\"chkDelivery" + sType + sI + "\");");
                    strmResponse.println("chkNotify.checked=true;</script>");
                }
//              if(sInput.charAt(i)=='E')
//              {
//                  strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">var chkNotify=document.getElementById(\"chkDelayed"+sType+sI+"\");");
//                  strmResponse.println("chkNotify.checked=true;</script>");
//              }
            }
        }

        strmResponse.println("</body>");
        strmResponse.println("</html>");

    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void saveNotifications(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");
        String[][] sTrackingEmail = new String[5][5];
        String[][] sTrackingSMS = new String[5][5];

        try {
            for (int i = 0; i < 5; i++) {
                sTrackingEmail[i][0] = request.getParameter("sEmail" + (i + 1)) == null ? "" : request.getParameter("sEmail" + (i + 1));
                sTrackingSMS[i][0] = request.getParameter("sSMS" + (i + 1)) == null ? "" : request.getParameter("sSMS" + (i + 1));
                sTrackingEmail[i][1] = request.getParameter("sEmailNotify" + (i + 1)) == null ? "" : request.getParameter("sEmailNotify" + (i + 1)).toString();
                sTrackingSMS[i][1] = request.getParameter("sSMSNotify" + (i + 1)) == null ? "" : request.getParameter("sSMSNotify" + (i + 1)).toString();
            }


            HttpSession validSession = request.getSession(false);

            validSession.setAttribute("sTrackingEmail", sTrackingEmail);
            validSession.setAttribute("sTrackingSMS", sTrackingSMS);

            strmResponse.println("<html>");
            strmResponse.println("<head></head>");
            strmResponse.println("<body>");
            strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">window.close();</script>");
            strmResponse.println("</body>");
            strmResponse.println("</html>");
        } catch (Exception ex) {
            sErrorMessage = "Error in saveNotifications:" + ex.getMessage();
            vShowErrorPage(response);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void disableInputBoxes(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");

        strmResponse.println("<html>");
        strmResponse.println("<head></head>");
        strmResponse.println("<body>");
        strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">");
        strmResponse.println("var tblBooking=document.getElementById(\"tblTracking\");");

        //Identify all the input types first
        strmResponse.println("var CInputBooking=tblBooking.getElementsByTagName('input');");

        //Disable all the input fields in the booking section.   
        strmResponse.println("for (var i=0,thisInput; thisInput=CInputBooking[i]; i++) {");
        strmResponse.println("thisInput.disabled=true;");
        strmResponse.println("}");

        strmResponse.println("</script>");
        strmResponse.println("</body>");
        strmResponse.println("</html>");
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     * @return
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
