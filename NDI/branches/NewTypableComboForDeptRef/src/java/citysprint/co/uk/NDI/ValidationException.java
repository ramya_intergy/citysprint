package citysprint.co.uk.NDI;

/**
 *
 * @author ramyas
 */
public class ValidationException extends Exception
{

    /**
     *
     * @param sMessage
     */
    public ValidationException(String sMessage)
    {
        super(sMessage);
    }

    /**
     *
     */
    public ValidationException()
    {
        super();
    }
}