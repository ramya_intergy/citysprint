/* ----------------------------------------------------------------------------
 * QuickAddress Pro Web > (c) QAS Ltd > www.qas.com
 *
 * Web > Verification > VerifyFormatAddress.java
 * Format the address selected from the picklist
 * ----------------------------------------------------------------------------
 */
package com.qas.proweb.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qas.proweb.*;

/** 
 * Command to encapsulate formatting addresses in the interaction case of the verification scenario.
 * Input params: CountryName (also set as output attribute), Moniker, UserInput[] (if error this is also set as output attribute)
 * Output attributes: Verified, Address[], ErrorInfo (if exception caught)
 */
public class VerifyFormatAddress implements Command
{
    public static final String NAME = "VerifyFormatAddress";

	/* (non-Javadoc)
	 * @see com.qas.proweb.servlet.Command#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public String execute(
		HttpServletRequest request,
		HttpServletResponse response)
	{
        HttpHelper.passThrough(request, Constants.COUNTRY_NAME);
        String sMoniker="";
        String sType=HttpHelper.passThrough(request, Constants.STYPE);
        
        //Ramya added logic to format two address simultaneously
        if(sType.equals(Constants.MONIKER1))
            sMoniker=HttpHelper.getValue(request,Constants.MONIKER1);
        else
            sMoniker = HttpHelper.getValue(request, Constants.MONIKER);
        
        // get final address 
        try
        {
            QuickAddress searchService = new QuickAddress(Constants.ENDPOINT);
            FormattedAddress address = searchService.getFormattedAddress(Constants.LAYOUT, sMoniker);
            AddressLine[] lines = address.getAddressLines();
            String[] asLabels=new String[lines.length];
            // set up output attributes
            String[] asAddress = new String[lines.length];
            for (int i=0; i < lines.length; i++)
            {
                asAddress[i] = lines[i].getLine();
                asLabels[i]=lines[i].getLabel();
            }
            request.setAttribute(Constants.ADDRESS, asAddress);
            request.setAttribute(Constants.LABELS,asLabels); 
        }
        catch (Exception e)
        {
            // dump the exception to error stream
            System.err.println("~~~~~~ Caught exception in VerifyFromatAddress command ~~~~~~~");
            e.printStackTrace();
            System.err.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

            // use user input as address in final screen
            request.setAttribute(Constants.ADDRESS, HttpHelper.getArrayValue(request, Constants.USER_INPUT) );
            request.setAttribute(Constants.VERIFY_INFO, "address verification " + Constants.ROUTE_FAILED + ", so the entered address has been used");
            request.setAttribute(Constants.ERROR_INFO, e.toString());
        }

		return "/" + Constants.FINAL_ADDRESS_PAGE;
	}

}
