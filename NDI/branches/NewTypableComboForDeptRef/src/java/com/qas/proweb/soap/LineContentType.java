/**
 * LineContentType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class LineContentType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected LineContentType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _None = "None";
    public static final java.lang.String _Address = "Address";
    public static final java.lang.String _Name = "Name";
    public static final java.lang.String _Ancillary = "Ancillary";
    public static final java.lang.String _DataPlus = "DataPlus";
    public static final LineContentType None = new LineContentType(_None);
    public static final LineContentType Address = new LineContentType(_Address);
    public static final LineContentType Name = new LineContentType(_Name);
    public static final LineContentType Ancillary = new LineContentType(_Ancillary);
    public static final LineContentType DataPlus = new LineContentType(_DataPlus);
    public java.lang.String getValue() { return _value_;}
    public static LineContentType fromValue(java.lang.String value)
          throws java.lang.IllegalStateException {
        LineContentType enum1 = (LineContentType)
            _table_.get(value);
        if (enum1==null) throw new java.lang.IllegalStateException();
        return enum1;
    }
    public static LineContentType fromString(java.lang.String value)
          throws java.lang.IllegalStateException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
}
