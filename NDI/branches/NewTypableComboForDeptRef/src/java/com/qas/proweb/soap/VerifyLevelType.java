/**
 * VerifyLevelType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class VerifyLevelType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected VerifyLevelType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _None = "None";
    public static final java.lang.String _Verified = "Verified";
    public static final java.lang.String _InteractionRequired = "InteractionRequired";
    public static final java.lang.String _PremisesPartial = "PremisesPartial";
    public static final java.lang.String _StreetPartial = "StreetPartial";
    public static final java.lang.String _Multiple = "Multiple";
    public static final VerifyLevelType None = new VerifyLevelType(_None);
    public static final VerifyLevelType Verified = new VerifyLevelType(_Verified);
    public static final VerifyLevelType InteractionRequired = new VerifyLevelType(_InteractionRequired);
    public static final VerifyLevelType PremisesPartial = new VerifyLevelType(_PremisesPartial);
    public static final VerifyLevelType StreetPartial = new VerifyLevelType(_StreetPartial);
    public static final VerifyLevelType Multiple = new VerifyLevelType(_Multiple);
    public java.lang.String getValue() { return _value_;}
    public static VerifyLevelType fromValue(java.lang.String value)
          throws java.lang.IllegalStateException {
        VerifyLevelType enum1 = (VerifyLevelType)
            _table_.get(value);
        if (enum1==null) throw new java.lang.IllegalStateException();
        return enum1;
    }
    public static VerifyLevelType fromString(java.lang.String value)
          throws java.lang.IllegalStateException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
}
