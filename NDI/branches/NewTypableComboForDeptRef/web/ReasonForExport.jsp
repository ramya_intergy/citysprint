<%-- 
    Document   : ReasonForExport
    Created on : 3/08/2009, 10:34:47
    Author     : ramyas
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.sql.*" %>
<%@ page import ="java.util.*" %>
<%@ page import ="java.net.URL"%>
<%@ page import="java.net.URLDecoder"%>
<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%!
/*
    public String getClassName() throws FileNotFoundException, IOException {
        String thisClassName;

        //Build a string with executing class's name
        thisClassName = this.getClass().getName();
        thisClassName = thisClassName.substring(thisClassName.lastIndexOf(".") + 1, thisClassName.length());
        thisClassName += ".class";  //this is the name of the bytecode file that is executing

        //writeOutputLog(thisClassName);
        return thisClassName;
    }

    public String getLocalDirName() throws FileNotFoundException, IOException {
        String localDirName;

        //Use that name to get a URL to the directory we are executing in
        java.net.URL myURL = this.getClass().getResource(getClassName());  //Open a URL to the our .class file

        //Clean up the URL and make a String with absolute path name
        localDirName = myURL.getPath();  //Strip path to URL object out

        localDirName = myURL.getPath().replaceAll("%20", " ");  //change %20 chars to spaces

        //Get the current execution directory
        localDirName = localDirName.substring(0, localDirName.lastIndexOf("/"));  //clean off the file name

        //writeOutputLog(localDirName);

        return localDirName;
    }

    public void writeOutputLog(String aContents) {

        java.util.Date d = new java.util.Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");


        File file;
        FileWriter fw;
        final String NEW_LINE = System.getProperty("line.separator");
        try {
            File baseDir = new File(getLocalDirName());
            File logDir = new File(baseDir, "NDILogs");
            boolean bDirExists = logDir.mkdirs();
            int len = d.toString().length();

            String sFileName = "console_" + d.toString().substring(0, 10) + "_" + d.toString().substring(len - 4, len) + ".txt";


            file = new File(logDir, sFileName);

            if (!file.exists()) {
                // a new file was created
                boolean fCreated = file.createNewFile();
                Writer output = new BufferedWriter(new FileWriter(file));

                //FileWriter always assumes default encoding is OK!
                output.write(format.format(d));
                output.write(NEW_LINE);
                output.write(aContents);
                output.close();
            } else {
                Writer output = new BufferedWriter(new FileWriter(file, true));

                //FileWriter always assumes default encoding is OK!
                output.write(NEW_LINE);
                output.write(NEW_LINE);
                output.write(format.format(d));
                output.write(NEW_LINE);
                output.write(NEW_LINE + aContents);
                output.close();
            }
        } catch (Exception e) {
            //throw new Error("unable to create log file "
              //      + "\n" + e.toString());
        }


    }
*/
%>
<html>
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>CitySprint - Reason For Export</title>
        <link media="screen" href="CSS/NextDayCss.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" >

        //typable combobox script
        var fActiveMenu = false;
        var oOverMenu = false;
        function mouseSelect(e)
        {
        if (fActiveMenu)
        {
        if (oOverMenu == false)
        {
        oOverMenu = false;
        document.getElementById(fActiveMenu).style.display = "none";
        fActiveMenu = false;
        return false;
        }
        return false;
        }
        return true;
        }
        function menuActivate(idEdit, idMenu, idSel)
        {
        if (fActiveMenu) return mouseSelect(0);
        oMenu = document.getElementById(idMenu);
        oEdit = document.getElementById(idEdit);
        nTop = oEdit.offsetTop + oEdit.offsetHeight;
        nLeft = oEdit.offsetLeft;
        while (oEdit.offsetParent != document.body)
        {
        oEdit = oEdit.offsetParent;
        nTop += oEdit.offsetTop;
        nLeft += oEdit.offsetLeft;
        }
        oMenu.style.left = nLeft;
        oMenu.style.top = nTop;
        oMenu.style.display = "";
        fActiveMenu = idMenu;
        document.getElementById(idSel).focus();
        return false;
        }
        function textSet(idEdit, text)
        {
            //alert (text);
		if (text == null || text == ''){
		//	alert('text is null or empty goign to return');
			return;
		}
			
        
		
		//var cmb = document.forms
		document.getElementById(idEdit).value = text;
        //document.getElementById(idEdit).value = text;
//        var valueSplit=value.split("^\
//        if(valueSplit.length>0)
//        {
//        var selIndemnity=document.getElementById(""+sParam_EditCell1+"3\
//        if(valueSplit[1]=="N")
//        {
//        selIndemnity.value="NO";
//        selIndemnity.disabled=true;
//        }
//        else
//        selIndemnity.disabled=false
//        }
        oOverMenu = false;
        mouseSelect(0);
        document.getElementById(idEdit).focus();
        }
        function comboKey(idEdit, text)
        {
        if (window.event.keyCode == 13 || window.event.keyCode == 32)
        textSet(idEdit,text.value);
        else if (window.event.keyCode == 27)
        {
        mouseSelect(0);
        document.getElementById(idEdit).focus();
        }
        }
        document.onmousedown = mouseSelect;
        </script>
    </head>
    <body style="padding-left:10px" width="350px" height="250px">
        <form id="frmReason" name="frmReason" target="" action="JobAudit">
            <div class="label"> <br>
                Reason for Export<br>
                <input type="text" id="txtReason" name="txtReason" size="40" style="width:200;text-transform:uppercase;">
                <input type="button" hidefocus="1" value="&#9660;" style='height:19; width:18; font-family: Verdana;' onclick="JavaScript:menuActivate('txtReason', 'divReason', 'selReason')">
                <div id="divReason" style="position:absolute; display:none; top:0px;left:0px;z-index:10000" onmouseover="javascript:oOverMenu='divTrackingSMS';" onmouseout="javascript:oOverMenu=false;">
                    <select size="5" id="selReason" style="width: 200; border-style: none" onclick="JavaScript:textSet('txtReason',this.value);" onkeypress="JavaScript:comboKey('txtReason', this);">

                        <%

                            Connection conDB = null;
                            Statement stmSelect;
                            String sqlReason = "Select rfe_description from rfe_reasonforexport";
                            ResultSet rstReason = null;


                            try {
                                String localDirName;

                                localDirName = getServletContext().getRealPath("") + "/WEB-INF/classes/citysprint/co/uk/NDI";

                                ResourceBundle bdl = new PropertyResourceBundle(new FileInputStream(localDirName + "/Config.properties"));

                                Class.forName(bdl.getString("sJDBCDriver")).newInstance();
                                conDB = DriverManager.getConnection(bdl.getString("sJDBCurl") + "user=" + bdl.getString("sMySqlUser") + "&password=" + bdl.getString("sMySqlPassword"));
                                stmSelect = conDB.createStatement();
                                rstReason = stmSelect.executeQuery(sqlReason);

                                while (rstReason.next()) {

                        %>

                        <option value="<%=rstReason.getString(1)%>" onclick="JavaScript:textSet('txtReason',this.value);"><%=rstReason.getString(1)%></option>
                        <%
                                }

                            } catch (Exception ex) {
            //                    citysprint.co.uk.NDI.CommonClass commObj = new citysprint.co.uk.NDI.CommonClass();
            //                    commObj.writeOutputLog(ex.getMessage());                               
                                System.out.println("Exception inside the ReasonForExport jsp file, Excep message =" + ex);
                                ex.printStackTrace();
                            } finally {
                                conDB.close();
                            }

                        %>
                    </select>
                </div>&nbsp;
                <input type="hidden" name="chkid" value='<%=(request.getParameter("chkid") == null ? "" : request.getParameter("chkid").toString())%>'>
                <input type="hidden" name="sAction" value='<%=(request.getParameter("sAction") == null ? "PrintInvoice" : request.getParameter("sAction").toString())%>'>
                <br><br>
                <font color="red">*Please insert company letterhead paper</font><br><br>
                <input type=submit value="Print Commercial Invoice">&nbsp;<input type=button value="Close" onclick="javascript:self.close()">
            </div>
        </form>

    </body>
</html>
