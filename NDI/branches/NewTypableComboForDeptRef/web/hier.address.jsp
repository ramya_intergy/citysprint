<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page contentType="text/html" %>
<%@ page import="com.qas.proweb.servlet.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%
	/** Intranet, Address page - final address display and confirmation **/
	
	/** Get attributes **/
	String   sRoute = (String) request.getAttribute(Constants.ROUTE);
	String[] asLabels = (String[]) request.getAttribute(Constants.LABELS);
	String[] asLines = (String[]) request.getAttribute(Constants.LINES);
	
	String[] asMonikerHistory = (String[]) request.getAttribute(Constants.MONIKER_HISTORY);
	String[] asPartialHistory = (String[]) request.getAttribute(Constants.PARTIAL_HISTORY);
	String[] asPostcodeHistory = (String[]) request.getAttribute(Constants.POSTCODE_HISTORY);
	String[] asScoreHistory = (String[]) request.getAttribute(Constants.SCORE_HISTORY);
	// Search term (HTML escape quotes)
	String sUserInputEnc = StringEscapeUtils.escapeHtml((String) request.getAttribute(Constants.USER_INPUT));
	String sType1 = StringEscapeUtils.escapeHtml((String) request.getAttribute(Constants.STYPE));
	/** Display a message depending on the Route. The possible values are:
		- Constants.ROUTE_NORMAL
		- Constants.ROUTE_FAILED
		- Constants.ROUTE_TOO_MANY
		- Constants.ROUTE_NO_MATCHES
		- Constants.ROUTE_TIMEOUT
		- Constants.ROUTE_PRE_SEARCH_FAILED
		- Constants.ROUTE_UNSUPPORTED_COUNTRY **/
	String sMessage;
	if (Constants.ROUTE_NORMAL.equals(sRoute))
	{
		sMessage = "Please confirm your address below.";
	}
	else if (Constants.ROUTE_TOO_MANY.equals(sRoute)
		|| Constants.ROUTE_NO_MATCHES.equals(sRoute)
		|| Constants.ROUTE_TIMEOUT.equals(sRoute))
	{
		sMessage = "Automatic address capture did not succeed.<br /><br />Please search again or enter your address below.";
	}
	else
	{
		sMessage = "Automatic address capture is not available.<br /><br />Please enter your address below.";
	}
	
	/** Display warning about last step-in, depending on the Pick_Warning **/
	String sPicklistWarning = ((String) request.getAttribute(Constants.STEPIN_WARNING));
	String sWarning = "";
	if (Constants.WARN_STEPPEDPASTCLOSE.equals(sPicklistWarning))
	{
		sWarning = "<img src=\"img/warning.gif\" class=\"icon\"><b>There are also close matches available - click <a href=\"javascript:goBack();\">back</a> to see them.</b><br />";
	}
	else if (Constants.WARN_CROSSBORDER.equals(sPicklistWarning))
	{
		sWarning = "<img src=\"img/warning.gif\" class=\"icon\"><b>Address selected is outside of the entered locality.</b><br />";
	}
	else if (Constants.WARN_POSTCODERECODE.equals(sPicklistWarning))
	{
		sWarning = "<img src=\"img/warning.gif\" class=\"icon\"><b>Postal code has been updated by the Postal Authority.</b><br />";
	}
%>

<html>
	<head>
		  <title>CitySprint - Postcode Lookup</title>
                   <link media="screen" href="CSS/style.css" type="text/css" rel="stylesheet">
                   <link media="screen" href="CSS/NextDayCss.css" type="text/css" rel="stylesheet">
		<script type="text/javascript">
			function newSearch()
			{
				document.formAddress.action = "QasController";
				document.formAddress.<%= Constants.COMMAND %>.value = "<%= HierInit.NAME %>";
				document.formAddress.<%= Constants.USER_INPUT %>.value = "";
				document.formAddress.submit();
			}
			
			function goBack()
			{
				document.formAddress.action = "QasController";
				document.formAddress.<%= Constants.COMMAND %>.value = "<%= request.getAttribute(Constants.BACK_COMMAND) %>";
				document.formAddress.submit();
			}
			
			// Store the country selected in the CountryName field (only when using an unsupported country)
			function storeCountryName()
			{
<%	if (Constants.ROUTE_UNSUPPORTED_COUNTRY.equals(sRoute))
	{
%>				var iSelected = document.formAddress.countrySelector.selectedIndex;
				document.formAddress.<%= Constants.COUNTRY_NAME %>.value = document.formAddress.countrySelector.options[iSelected].text;
<%	}
%>			}

    function Confirm()
    {
        //alert("inside confirm");
        var sCompany="<%= asLines[0]%>";
        var sAddress="<%= asLines[1]%>";
        var sAddress2="<%= asLines[2]%>";
        var sTown="<%= StringEscapeUtils.escapeHtml(asLines[3])%>";
        var sCounty="<%= StringEscapeUtils.escapeHtml(asLines[4])%>";
        var sPostcode="<%= StringEscapeUtils.escapeHtml(asLines[5])%>";
        var sLatitude="<%= StringEscapeUtils.escapeHtml(asLines[7])%>";
        var sLongitude="<%= StringEscapeUtils.escapeHtml(asLines[8])%>";
        
        var sType="<%= sType1%>";
        //alert("Type:"+sType);

            if(sType=="C")
            {

                var txtCCompany = window.opener.document.getElementById("txtCCompany");
                var txtCAddress1 = window.opener.document.getElementById("txtCAddress1");
                var txtCAddress2 = window.opener.document.getElementById("txtCAddress2");
                var txtCTown = window.opener.document.getElementById("txtCTown");
                var txtCCounty = window.opener.document.getElementById("txtCCounty");
                var txtCPostcode = window.opener.document.getElementById("txtCPostcode");
                var txtCLatitude = window.opener.document.getElementById("txtCLatitude");
                //alert(txtCLatitude);
                var txtCLongitude = window.opener.document.getElementById("txtCLongitude");

                if(sCompany==""){

                }
                else{
                    txtCCompany.value =  sCompany;
                }

                txtCAddress1.value = sAddress;
                txtCAddress2.value = sAddress2;
                txtCTown.value = sTown;
                txtCCounty.value = sCounty;
                txtCPostcode.value = sPostcode;
                txtCLatitude.value = sLatitude;
                txtCLongitude.value = sLongitude;

                txtCTown.disabled=true;
                txtCPostcode.disabled=true;


                window.close();

            }
            else
            {

                var txtDCompany = window.opener.document.getElementById("txtDCompany");
                var txtDAddress1 = window.opener.document.getElementById("txtDAddress1");
                var txtDAddress2 = window.opener.document.getElementById("txtDAddress2");
                var txtDTown = window.opener.document.getElementById("txtDTown");
                var txtDCounty = window.opener.document.getElementById("txtDCounty");
                var txtDPostcode = window.opener.document.getElementById("txtDPostcode");
                var txtDLatitude = window.opener.document.getElementById("txtDLatitude");
                var txtDLongitude = window.opener.document.getElementById("txtDLongitude");
                var selDCountry = window.opener.document.getElementById("selDCountry");
                var lblDPostcode = window.opener.document.getElementById("lblDPostcode");

                if(sCompany==""){

                }
                else{
                    txtDCompany.value = sCompany;
                }

                txtDAddress1.value = sAddress;
                txtDAddress2.value = sAddress2;
                txtDTown.value = sTown;
                txtDCounty.value = sCounty;
                txtDPostcode.value = sPostcode;
                txtDLatitude.value = sLatitude;
                txtDLongitude.value = sLongitude;

                txtDTown.disabled=true;
                txtDPostcode.disabled=true;
                selDCountry.disabled=true;
                lblDPostcode.innerHTML="Postcode *";


                window.close();


            }
       

    }


	</script>
	</head>
	
	<body >
		<table width="400" cellpadding=2 id="box">
			<tr>
				
			</tr>
			<tr>
				<td class="nomargin" style="padding-left:5px">
					
					<h3>Confirm Final Address</h3>
					
					<form name="formAddress" method="post" action="<%= Constants.FINAL_ADDRESS_PAGE %>" onSubmit="storeCountryName();">
 						<%= sMessage %><br /><br />
 						<%= sWarning %>
						
						<table>
<%	for (int i = 0; i < asLines.length-3; i++)
	{
%>							<tr>
                                                                <% if(i==1){ %>
                                                                <td>Address 1</td>
                                                                <% }else if (i==2){%>
                                                                <td>Address 2</td>
                                                                <% }else{%>
								<td> <%= asLabels[i]%></td>
                                                                <%}%>
								<td width="10"></td>
								<td><input type="text" name="<%= Constants.ADDRESS %>" size="50" 
									value="<%= StringEscapeUtils.escapeHtml(asLines[i]) %>" disabled=disabled/></td>
							</tr>
<%	}
	if (Constants.ROUTE_UNSUPPORTED_COUNTRY.equals(sRoute))
	{
%>							<!--<tr>
								<td>Country</td>
								<td width="10"></td>
								<td><select name="countrySelector"><%@ include file="countries.all.inc" %></select>
								</td>
							</tr>-->
							<input type="hidden" name="<%= Constants.COUNTRY_NAME %>" value="" />
<%	}
	else
	{
%>							<!--<tr>
								<td>Country</td>
								<td width="10"></td>
								<td><input type="text" name="<%= Constants.COUNTRY_NAME %>" size="50"
									value="<%= request.getAttribute(Constants.COUNTRY_NAME) %>"
									readonly="readonly" style="background: #F0F0F0" tabindex="-1" /></td>
							</tr>-->
<%	}
	if (!Constants.ROUTE_NORMAL.equals(sRoute))
	{
%>							<tr class="debug">
								<td colspan="3" class="debug">
									<p class="debug"><img src="img/debug.gif" align="left" />&nbsp;Integrator information: search result was
<%		out.print(sRoute);
		if (request.getAttribute(Constants.ERROR_INFO) != null)
		{
			out.print("<br />&nbsp;" + request.getAttribute(Constants.ERROR_INFO));
		}
%>
									</p>
								</td>
							</tr>
<%	}
%>						</table>

						<input type="hidden" name="<%= Constants.DATA_ID %>" value="<%= request.getAttribute(Constants.DATA_ID) %>" />
						<input type="hidden" name="<%= Constants.USER_INPUT %>" value="<%= sUserInputEnc %>" />
						<input type="hidden" name="<%= Constants.STYPE %>" value="<%= sType1 %>" />
						<input type="hidden" name="<%= Constants.COMMAND %>" value="<%= request.getAttribute(Constants.BACK_COMMAND) %>" />
						<input type="hidden" name="<%= Constants.ROUTE %>" value="<%= Constants.ROUTE_RECREATE %>" />
<%	/* History of monikers and partial addresses */
	for (int i=0; i < asMonikerHistory.length; i++)
	{
%>						<input type="hidden" name="<%= Constants.MONIKER_HISTORY %>" value="<%= asMonikerHistory[i] %>" />
						<input type="hidden" name="<%= Constants.PARTIAL_HISTORY %>"
							value="<%= StringEscapeUtils.escapeHtml(asPartialHistory[i]) %>" />
						<input type="hidden" name="<%= Constants.POSTCODE_HISTORY %>" value="<%= asPostcodeHistory[i] %>" />
						<input type="hidden" name="<%= Constants.SCORE_HISTORY %>" value="<%= asScoreHistory[i] %>" />
<%	}
%>
						<br />
						This completes the address capture process.

                                                
					</form>
                                        <center>
					<button id="btn" onClick="Javascript:Confirm()">Confirm</button> &nbsp;<br /><br />
                                        </center>
				</td>
			</tr>
		</table>
                <table>
                    <tr>
                        <td>
                            
                            <center>

                                <a href="#" onClick="Javascript:window.close();" >Return to Booking Screen</a>&nbsp;
                                <a href="#" onClick="goBack()" accesskey="B">Return to Search Results</a>
                            </center>
                        </td>
                    </  tr>
                </table>
	</body>
</html>
