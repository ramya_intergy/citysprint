/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citysprint.co.uk.NDI;

import citysprint.co.uk.NDI.CommodityInfo;
import citysprint.co.uk.NDI.ValidationException;
import citysprint.co.uk.NDI.BookingDetails;
import com.sun.net.httpserver.HttpContext;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import javax.sql.DataSource;


/**
 *
 * @author ramyas
 */
public class EnterConsignment extends BookingDetails  {


    //String sSessionID;
    //Connection conDB=null;

     // Local constants for ItemsGrid
    final String  sColumnHeaders[]    = {"No.of.items*", /*"Parcel Type", "Item Weight",*/ "Length(cm)", "Width(cm)","Height(cm)"," "};
    final int     nColumnWidths[]     = {90, /*90, 90,*/ 90, 90, 90, 50};
    final int     nColumnTextWidths[] = {8, /*8, 8,*/ 8, 8, 8, 8};
    final int     nColumnMaxLengths[]  = {9,/*9,9,*/9,9,9,9};
    final boolean bColumnEditable[]   = {true, /*true, true,*/ true, true, true, false};

     // Local constants for CommodityGrid
    final String  sColumnHeaders1[]    = {"Commodity*", "Quantity", "Total Value*","Indemnity*"," "};
    final int     nColumnWidths1[]     = {100,70, 90, 90, 50};
    final int     nColumnTextWidths1[] = {8, 8, 8, 8, 8};
    final int     nColumnMaxLengths1[]  = {40,9,15,9,9};
    final boolean bColumnEditable1[]   = {true, true, true, true, false};


    String sPType="";
    String sPDescription="D";
    String sPValue="";
    String sPQuantity="";
    String sPNoofItems="";
    String sPLength="";
    String sPWidth="";
    String sPHeight="";
    String sPWeight="";
    String sPInstructions="";
    String sPCubic="";
    String sPIndemnity="";
    //String sErrorMessage="";
    String sPCode="";


    protected boolean bEditAllowed(int nColumn)
    {
        return (bColumnEditable[nColumn]);
    }

    protected boolean bEditAllowed1(int nColumn)
    {
        return (bColumnEditable1[nColumn]);
    }

    // Headers for columns
    protected String sGetColumnHeader(int nColumnNo)
    {
        String header = sColumnHeaders[nColumnNo];
        if (nColumnNo < sColumnHeaders.length)
            return(sColumnHeaders[nColumnNo]);
        else
            return Integer.toString(nColumnNo);
    }

     // Headers for columns
    protected String sGetColumnHeader1(int nColumnNo)
    {
        String header = sColumnHeaders1[nColumnNo];
        if (nColumnNo < sColumnHeaders1.length)
            return(sColumnHeaders1[nColumnNo]);
        else
            return Integer.toString(nColumnNo);
    }

    // Headers for columns
    protected int nGetColumnWidth(int nColumnNo)
    {
        if (nColumnNo < nColumnWidths.length)
            return(nColumnWidths[nColumnNo]);
        else
            return 100;
    }

    // Headers for columns
    protected int nGetColumnWidth1(int nColumnNo)
    {
        if (nColumnNo < nColumnWidths1.length)
            return(nColumnWidths1[nColumnNo]);
        else
            return 100;
    }

    // Headers for columns
    protected int nGetColumnTextWidth(int nColumnNo)
    {
        if (nColumnNo < nColumnTextWidths.length)
            return(nColumnTextWidths[nColumnNo]);
        else
            return 100;
    }

   // Headers for columns
    protected int nGetColumnTextWidth1(int nColumnNo)
    {
        if (nColumnNo < nColumnTextWidths1.length)
            return(nColumnTextWidths1[nColumnNo]);
        else
            return 100;
    }

     // Max Lengths for columns
    protected int nGetColumnMaxLengths(int nColumnNo)
    {
        if (nColumnNo < nColumnMaxLengths.length)
            return(nColumnMaxLengths[nColumnNo]);
        else
            return 10;
    }

    // Max Lengths for columns
    protected int nGetColumnMaxLengths1(int nColumnNo)
    {
        if (nColumnNo < nColumnMaxLengths1.length)
            return(nColumnMaxLengths1[nColumnNo]);
        else
            return 10;
    }

    protected boolean bAddAllowed() {return true;}
    protected boolean bAddAllowed1() {return true;}

    protected boolean bAddRow(String sSessionID)
    {
        boolean bResult  = false;

        String sPType="";
        String sPDescription="D";
        String sPValue="";
        String sPQuantity="";
        String sPNoofItems="";
        String sPLength="";
        String sPWidth="";
        String sPHeight="";
        String sPWeight="";
        String sPInstructions="";
        String sPCubic="";
        String sPIndemnity="";
        //String sErrorMessage="";
        String sPCode="";
        sErrorMessage = "";
        
        String sParcelType = "";
        String sItemWeight = "";
        
        try
        {
//            if (sEditedRow[1] == null)
//                sPType = "''";
//            else
//                sPType = "'" + sReplaceChars(sEditedRow[1], "'", "''") + "'";
//
//            if (sEditedRow[2] == null)
//                sPDescription = "''";
//            else
//                sPDescription = "'" + sReplaceChars(sEditedRow[2], "'", "''") + "'";


//            if(sEditedRow[1]==null)
//                sPWeight="0";
//            else
//                sPWeight=sReplaceChars(sEditedRow[1], "'", "''");

            if(sEditedRow[0]==null || sEditedRow[0].length()==0)
                sPNoofItems="0";
            else
                sPNoofItems=sReplaceChars(sEditedRow[0], "'", "''");

//            if(sEditedRow[1]==null || sEditedRow[1].length()==0)
//                sParcelType="0";
//            else
//                sParcelType=sReplaceChars(sEditedRow[1], "'", "''");
            
//            if(sEditedRow[1]==null || sEditedRow[1].length()==0)
//                sItemWeight="0";
//            else
//                sItemWeight=sReplaceChars(sEditedRow[1], "'", "''");
            
            if(sEditedRow[1]==null || sEditedRow[1].length()==0)
                sPLength="0";
            else
                sPLength=sReplaceChars(sEditedRow[1], "'", "''");

            if(sEditedRow[2]==null || sEditedRow[2].length()==0)
                sPWidth="0";
            else
                sPWidth=sReplaceChars(sEditedRow[2], "'", "''");

            if(sEditedRow[3]==null || sEditedRow[3].length()==0)
                sPHeight="0";
            else
                sPHeight=sReplaceChars(sEditedRow[3], "'", "''");



//            double dLength=(Double.parseDouble(sPLength))/100;
//            double dWidth=(Double.parseDouble(sPWidth))/100;
//            double dHeight=(Double.parseDouble(sPHeight))/100;
//
//            double cubic=dLength*dWidth*dHeight;
//
//
//            sPCubic="'"+cubic+"'";

//            if (sEditedRow[9] == null)
//                sPValue = "0";
//            else
//                sPValue=sReplaceChars(sEditedRow[9], "'", "''");
//
//            if(sEditedRow[10]==null)
//                sPIndemnity="0";
//            else
//                sPIndemnity="1";


            // Insert a new row
            //long lRowID = lGetSequence("stf_Staff");
            Connection conProduct=null;
            conProduct=cOpenConnection();
            Statement stmInsert = conProduct.createStatement();
            String sInsert="";
           // stmInsert.executeUpdate("Insert into pro_Product (pro_Type,pro_Description,pro_Value,pro_NoItems,pro_Length,pro_Width,pro_Height,pro_Weight,pro_cubic,pro_Indemnity,pro_SessionId)values("+sPType+","+sPDescription+","+sPValue+","+sPNoofItems+","+sPLength+","+sPWidth+","+sPHeight+","+sPWeight+","+sPCubic+","+sPIndemnity+",'"+sSessionID+"')");
            //stmInsert.executeUpdate("Insert into pro_product (pro_noitems,pro_length,pro_width,pro_height,pro_sessionid, /* pro_ptype,*/ pro_iweight)values("+sPNoofItems+","+sPLength+","+sPWidth+","+sPHeight+",'"+sSessionID+"','"+sParcelType+"',"+sItemWeight+")");
            stmInsert.executeUpdate("Insert into pro_product (pro_noitems,pro_length,pro_width,pro_height,pro_sessionid"
//                    + ", pro_iweight"
                    + ")values("+sPNoofItems+","+sPLength+","+sPWidth+","+sPHeight+",'"+sSessionID+"'"
//                    + ","+sItemWeight
                    +")");
            stmInsert.close();

            vCloseConnection(conProduct);
            bResult = true;
        }
        catch (Exception errInsert)
        {
            sErrorMessage = "Error inserting record - " + errInsert.toString();
        }
        return(bResult);
    }

    protected boolean bAddRow1(HttpSession validSession)
    {
        boolean bResult  = false;

        String sPType="";
        String sPDescription="D";
        String sPValue="";
        String sPQuantity="";
        String sPNoofItems="";
        String sPLength="";
        String sPWidth="";
        String sPHeight="";
        String sPWeight="";
        String sPInstructions="";
        String sPCubic="";
        String sPIndemnity="";
        //String sErrorMessage="";
        String sPCode="";
        String sTempIndemnity = "";
         sErrorMessage = "";
        CommodityInfo commodity=new CommodityInfo();
        commodity.parseCommodity((String[][])validSession.getAttribute("getCommodityArray"));

        try
        {
//            if (sEditedRow[1] == null)
//                sPType = "''";
//            else
//                sPType = "'" + sReplaceChars(sEditedRow[1], "'", "''") + "'";
//
//            if (sEditedRow[2] == null)
//                sPDescription = "''";
//            else
//                sPDescription = "'" + sReplaceChars(sEditedRow[2], "'", "''") + "'";


//            if(sEditedRow[1]==null)
//                sPWeight="0";
//            else
//                sPWeight=sReplaceChars(sEditedRow[1], "'", "''");

            if (sEditedRow1[0] == null)
                sPDescription = "''";
            else
                // dc - 9/12/2009 - strip commas and single quotes
          //      sPDescription =sEditedRow1[0];
                  sPDescription = sReplaceChars(sEditedRow1[0], "'", "");
                  sPDescription = sReplaceChars(sPDescription, ",", ";");

            if(sEditedRow1[1]==null || sEditedRow1[1].length()==0)
                sPQuantity="0";
            else
                sPQuantity=sReplaceChars(sEditedRow1[1], "'", "''");

            if(sEditedRow1[2]==null || sEditedRow1[2].length()==0)
                sPValue="0.0";
            else
                sPValue=sReplaceChars(sEditedRow1[2], "'", "''");

            if(sEditedRow1[3]==null)
            {
                sPIndemnity="0";
            }
            else
            {
                if(sEditedRow1[3].equals("YES"))
                    sPIndemnity="1";
                else
                    sPIndemnity="0";
            }

            if((commodity.getCsVector().indexOf(sPDescription.toUpperCase()))!=-1)
            {
                sPCode="'"+commodity.getCcVector().elementAt(commodity.getCsVector().indexOf(sPDescription.toUpperCase())).toString()+"'";
                if(commodity.getIfVector().elementAt(commodity.getCsVector().indexOf(sPDescription.toUpperCase())).equals("Y") && sPIndemnity.equals("1"))
                    validSession.setAttribute("bIndemnity",false);
                sTempIndemnity = commodity.getIfVector().elementAt(commodity.getCsVector().indexOf(sPDescription.toUpperCase())).toString();
            }
            else
                sPCode="''";


            sPDescription="'"+sReplaceChars(sPDescription,"'","''")+"'";
//            double dLength=(Double.parseDouble(sPLength))/100;
//            double dWidth=(Double.parseDouble(sPWidth))/100;
//            double dHeight=(Double.parseDouble(sPHeight))/100;
//
//            double cubic=dLength*dWidth*dHeight;
//
//
//            sPCubic="'"+cubic+"'";

//            if (sEditedRow[9] == null)
//                sPValue = "0";
//            else
//                sPValue=sReplaceChars(sEditedRow[9], "'", "''");
//
//            if(sEditedRow[10]==null)
//                sPIndemnity="0";
//            else
//                sPIndemnity="1";


            // Insert a new row
            //long lRowID = lGetSequence("stf_Staff");
            if ((sPCode == "''" && sPIndemnity == "1") || (sPCode != "''" && sTempIndemnity.equals("N") && sPIndemnity.equals("1"))) {
                sErrorMessage = "Indemnity cannot be applied for "+sPDescription+". Please contact your local CitySprint ServiceCentre to complete your booking";
            } else {
                Connection conCommodity = null;
                conCommodity = cOpenConnection();
                Statement stmInsert = conCommodity.createStatement();
                String sInsert = "";
                // stmInsert.executeUpdate("Insert into pro_Product (pro_Type,pro_Description,pro_Value,pro_NoItems,pro_Length,pro_Width,pro_Height,pro_Weight,pro_cubic,pro_Indemnity,pro_SessionId)values("+sPType+","+sPDescription+","+sPValue+","+sPNoofItems+","+sPLength+","+sPWidth+","+sPHeight+","+sPWeight+","+sPCubic+","+sPIndemnity+",'"+sSessionID+"')");
                stmInsert.executeUpdate("Insert into com_commodity (com_code,com_description,com_quantity,com_value,com_indemnity,com_sessionid)values(" + sPCode + "," + sPDescription + "," + sPQuantity + "," + sPValue + "," + sPIndemnity + ",'" + (validSession.getAttribute("sessionid").toString()) + "')");
                stmInsert.close();
                vCloseConnection(conCommodity);
                bResult = true;
            }



        }
        catch (Exception errInsert)
        {
            sErrorMessage = "Error inserting record - " + errInsert.toString();
        }
        return(bResult);
    }

    protected boolean bEditRow(long lRowID,String sSessionID)
    {
        boolean bResult  = false;
        String sPType="";
        String sPDescription="D";
        String sPValue="";
        String sPQuantity="";
        String sPNoofItems="";
        String sPLength="";
        String sPWidth="";
        String sPHeight="";
        String sPWeight="";
        String sPInstructions="";
        String sPCubic="";
        String sPIndemnity="";
        //String sErrorMessage="";
        String sPCode="";
        sErrorMessage = "";
        
        String sParcelType = "";
        String sItemWeight = "";
        
        try
        {
//             if (sEditedRow[1] == null)
//                sPType = "''";
//            else
//                sPType = "'" + sReplaceChars(sEditedRow[1], "'", "''") + "'";
//
//            if (sEditedRow[2] == null)
//                sPDescription = "''";
//            else
//                sPDescription = "'" + sReplaceChars(sEditedRow[2], "'", "''") + "'";

//
//            if(sEditedRow[1]==null)
//                sPWeight="0";
//            else
//                sPWeight=sReplaceChars(sEditedRow[1], "'", "''");

            /*
            if(sEditedRow[0]==null || sEditedRow[0].length()==0)
                sPNoofItems="0";
            else
                sPNoofItems=sReplaceChars(sEditedRow[0], "'", "''");

            if(sEditedRow[1]==null || sEditedRow[1].length()==0)
                sPLength="0";
            else
                sPLength=sReplaceChars(sEditedRow[1], "'", "''");

            if(sEditedRow[2]==null || sEditedRow[2].length()==0)
                sPWidth="0";
            else
                sPWidth=sReplaceChars(sEditedRow[2], "'", "''");

            if(sEditedRow[3]==null || sEditedRow[3].length()==0)
                sPHeight="0";
            else
                sPHeight=sReplaceChars(sEditedRow[3], "'", "''");
*/
            
            if(sEditedRow[0]==null || sEditedRow[0].length()==0)
                sPNoofItems="0";
            else
                sPNoofItems=sReplaceChars(sEditedRow[0], "'", "''");

//            if(sEditedRow[1]==null || sEditedRow[1].length()==0)
//                sParcelType="0";
//            else
//                sParcelType=sReplaceChars(sEditedRow[1], "'", "''");
            
//            if(sEditedRow[1]==null || sEditedRow[1].length()==0)
//                sItemWeight="0";
//            else
//                sItemWeight=sReplaceChars(sEditedRow[1], "'", "''");
            
            if(sEditedRow[1]==null || sEditedRow[1].length()==0)
                sPLength="0";
            else
                sPLength=sReplaceChars(sEditedRow[1], "'", "''");

            if(sEditedRow[2]==null || sEditedRow[2].length()==0)
                sPWidth="0";
            else
                sPWidth=sReplaceChars(sEditedRow[2], "'", "''");

            if(sEditedRow[3]==null || sEditedRow[3].length()==0)
                sPHeight="0";
            else
                sPHeight=sReplaceChars(sEditedRow[3], "'", "''");	
            

//
//            double dLength=(Double.parseDouble(sPLength))/100;
//            double dWidth=(Double.parseDouble(sPWidth))/100;
//            double dHeight=(Double.parseDouble(sPHeight))/100;
//
//            double cubic=dLength*dWidth*dHeight;
//
//            sPCubic="'"+cubic+"'";

//            if (sEditedRow[9] == null)
//                sPValue = "0";
//            else
//                sPValue=sReplaceChars(sEditedRow[9], "'", "''");
//
//            if(sEditedRow[10]==null)
//                sPIndemnity="0";
//            else
//                sPIndemnity="1";


            // Update the row
           
                Connection conProUpdate = null;
                conProUpdate = cOpenConnection();
                Statement stmUpdate = conProUpdate.createStatement();
                //  stmUpdate.executeUpdate("UPDATE pro_Product set pro_Type="+sPType+",pro_Description="+sPDescription+",pro_Value="+sPValue+",pro_NoItems="+sPNoofItems+",pro_Length="+sPLength+",pro_Width="+sPWidth+",pro_Height="+sPHeight+",pro_Weight="+sPWeight+",pro_cubic="+sPCubic+",pro_Indemnity="+sPIndemnity+",pro_SessionId='"+sSessionID+"'"+
                //                             "WHERE pro_ID=" + Long.toString(lRowID));
System.out.println("UPDATE pro_product set pro_noitems=" + sPNoofItems + ",pro_length=" + sPLength + ",pro_width=" + sPWidth 
                        + ",pro_height=" + sPHeight + ",pro_sessionid='" + sSessionID + "'"
//                        + ",pro_iweight=" + sItemWeight /*+ ",pro_ptype='" + sParcelType + "'"*/
                        + " WHERE pro_ID=" + Long.toString(lRowID));

                stmUpdate.executeUpdate("UPDATE pro_product set pro_noitems=" + sPNoofItems + ",pro_length=" + sPLength + ",pro_width=" + sPWidth 
                        + ",pro_height=" + sPHeight + ",pro_sessionid='" + sSessionID + "'"
//                        + ",pro_iweight=" + sItemWeight  /* + ",pro_ptype='" + sParcelType + "' " */
                        + " WHERE pro_ID=" + Long.toString(lRowID));

                stmUpdate.close();
                conProUpdate.close();

                bResult = true;
            
        }
        catch (Exception errUpdate)
        {
            sErrorMessage = "Error updating record - " + errUpdate.toString();
            errUpdate.printStackTrace();
        }
        return(bResult);
    }

    protected boolean bEditRow1(long lRowID,HttpSession validSession)
    {
        boolean bResult  = false;
        String sPType="";
        String sPDescription="D";
        String sPValue="";
        String sPQuantity="";
        String sPNoofItems="";
        String sPLength="";
        String sPWidth="";
        String sPHeight="";
        String sPWeight="";
        String sPInstructions="";
        String sPCubic="";
        String sPIndemnity="";
        String sTempIndemnity = "";
        //String sErrorMessage="";
        String sPCode="";
        sErrorMessage = "";
        CommodityInfo commodity=new CommodityInfo();
        commodity.parseCommodity((String[][])validSession.getAttribute("getCommodityArray"));

        try
        {

            if (sEditedRow1[0] == null)
                sPDescription = "''";
            else
                sPDescription =sEditedRow1[0];

            if(sEditedRow1[1]==null || sEditedRow1[1].length()==0)
                sPQuantity="0";
            else
                sPQuantity=sReplaceChars(sEditedRow1[1], "'", "''");

            if(sEditedRow1[2]==null || sEditedRow1[2].length()==0)
                sPValue="0.0";
            else
                sPValue=sReplaceChars(sEditedRow1[2], "'", "''");

            if(sEditedRow1[3]==null)
            {
                sPIndemnity="0";
            }
            else
            {
                if(sEditedRow1[3].equals("YES"))
                    sPIndemnity="1";
                else
                    sPIndemnity="0";
            }

            if((commodity.getCsVector().indexOf(sPDescription.toUpperCase()))!=-1)
            {
                sPCode="'"+commodity.getCcVector().elementAt(commodity.getCsVector().indexOf(sPDescription.toUpperCase())).toString()+"'";
                if(commodity.getIfVector().elementAt(commodity.getCsVector().indexOf(sPDescription.toUpperCase())).equals("Y") && sPIndemnity.equals("1"))
                    validSession.setAttribute("bIndemnity",false);
                 sTempIndemnity = commodity.getIfVector().elementAt(commodity.getCsVector().indexOf(sPDescription.toUpperCase())).toString();
            }
            else
                sPCode="''";


            sPDescription="'"+sReplaceChars(sPDescription,"'","''")+"'";

            // Update the row
            if (sPCode == "''" && sPIndemnity == "1" || (sPCode != "''" && sTempIndemnity.equals("N") && sPIndemnity.equals("1"))) {
                sErrorMessage = "Indemnity cannot be applied for "+sPDescription+". Please contact your local CitySprint ServiceCentre to complete your booking";
            } else {
                Connection conComUpdate = null;
                conComUpdate = cOpenConnection();
                Statement stmUpdate = conComUpdate.createStatement();
                //  stmUpdate.executeUpdate("UPDATE pro_Product set pro_Type="+sPType+",pro_Description="+sPDescription+",pro_Value="+sPValue+",pro_NoItems="+sPNoofItems+",pro_Length="+sPLength+",pro_Width="+sPWidth+",pro_Height="+sPHeight+",pro_Weight="+sPWeight+",pro_cubic="+sPCubic+",pro_Indemnity="+sPIndemnity+",pro_SessionId='"+sSessionID+"'"+
                //                             "WHERE pro_ID=" + Long.toString(lRowID));

                stmUpdate.executeUpdate("UPDATE com_commodity set com_code=" + sPCode + ", com_description=" + sPDescription + ",com_quantity=" + sPQuantity + ",com_value=" + sPValue + ",com_indemnity=" + sPIndemnity + ",com_sessionid='" + (validSession.getAttribute("sessionid").toString()) + "'"
                        + "WHERE com_ID=" + Long.toString(lRowID));

                stmUpdate.close();
                conComUpdate.close();
                bResult = true;
            }
        }
        catch (Exception errUpdate)
        {
            sErrorMessage = "Error updating record - " + errUpdate.toString();
        }
        return(bResult);
    }

    protected boolean bDeleteRow(long lRowID)
    {
        boolean bResult = false;
        sErrorMessage = "";
        try
        {
            // Delete the row with the id in column 0
            Connection conProDelete=null;
            conProDelete=cOpenConnection();
            Statement stmDelete = conProDelete .createStatement();
            stmDelete.execute("DELETE FROM pro_product " +
                              "WHERE  pro_ID=" + Long.toString(lRowID));
            stmDelete.close();
            conProDelete.close();
            bResult = true;
        }
        catch (Exception errDelete)
        {
            sErrorMessage = "Error deleting product record - " + errDelete.toString();
        }
        return(bResult);
    }

     protected boolean bDeleteRow1(long lRowID)
    {
        boolean bResult = false;
        sErrorMessage = "";
        try
        {
            // Delete the row with the id in column 0
            Connection conComDelete=null;
            conComDelete=cOpenConnection();
            Statement stmDelete = conComDelete .createStatement();
            stmDelete.execute("DELETE FROM com_commodity " +
                              "WHERE  com_ID=" + Long.toString(lRowID));
            stmDelete.close();
            conComDelete.close();
            bResult = true;
        }
        catch (Exception errDelete)
        {
            sErrorMessage = "Error deleting commodity record - " + errDelete.toString();
        }
        return(bResult);
    }

     protected boolean bDeleteAllowed(long lRowID) throws IllegalArgumentException
    {
        boolean bResult = true;
        return bResult;
    }

     protected boolean bDeleteAllowed1(long lRowID) throws IllegalArgumentException
    {
        boolean bResult = true;
        return bResult;
    }

    protected void vValidateSubmittedData() throws ValidationException
    {
    }

    protected String sServletName() {return "EnterConsignment";}

    protected String sBuildCellEditor(int nColumn, String sData)
                                      throws IllegalArgumentException
    {
//        if (nColumn==1)
//            return "<SELECT NAME=\"" + sParam_EditCell +
//                  Integer.toString(nColumn) + "\">\n" +
//                  "<OPTION VALUE=Documents" +
//                  (sData.equals("Documents")?" SELECTED":"") + ">Documents\n" +
//                  "<OPTION VALUE=\"Non Documents\"" +
//                  (sData.equals("Non Documents")?" SELECTED":"") + ">Non Documents\n" +
//                  "</SELECT>";
//        else
//
//         if(nColumn==2)
//            return "<input type=\"text\" id=\""+sParam_EditCell+Integer.toString(nColumn) +"\" name=\""+sParam_EditCell+Integer.toString(nColumn) +"\" size=\"15\" style=\"width:100\" value="+sData+">" +
//                    "<input type=\"button\" hidefocus=\"1\" value=\"&#9660;\" style=" +
//                    "'height:19; width:18; font-family: Verdana;' onclick=\"JavaScript:menuActivate('"+sParam_EditCell+Integer.toString(nColumn) +"', 'combodiv', 'combosel')\">"+
//                    "<div id=\"combodiv\" style=\"position:absolute; display:none; top:0px;left:0px;z-index:10000\" onmouseover=\"javascript:oOverMenu='combodiv';\""+
//                    "onmouseout=\"javascript:oOverMenu=false;\">"+
//                    "<select size=\"5\" id=\"combosel\" style=\"width: 150; border-style: none\""+
//                    "onclick=\"JavaScript:textSet('"+sParam_EditCell+Integer.toString(nColumn) +"',this.value);\""+
//                    "onkeypress=\"JavaScript:comboKey('"+sParam_EditCell+Integer.toString(nColumn) +"', this);\">"+
//                    "<option value=\"Heavy\">Heavy</option>"+
//                    "<option value=\"Light\">Light</option>"+
//                    "</select></div>";
//         else
//             if(nColumn==10)
//                 return "<input type=\"checkbox\" name=\""+sParam_EditCell+Integer.toString(nColumn) +"\" id=\""+sParam_EditCell+Integer.toString(nColumn) +"\" onclick=\"javascript:window.open('Indemnity.html','Calendar','width=250,height=130','toolbar=no','resizable=no')\">";
//
//        else      
            return super.sBuildCellEditor(nColumn, sData);
    }

      protected String sBuildCellEditor1(int nColumn, String sData, HttpSession validSession)
                                      throws IllegalArgumentException
    {

          String sOptions="";

          CommodityInfo commodity=new CommodityInfo();
          commodity.parseCommodity((String[][])validSession.getAttribute("getCommodityArray"));

//          for(int i=0;i<commodity.getCsVector().size();i++)
//          {
//              sOptions=sOptions+"<option value=\""+commodity.getCsVector().elementAt(i)+"^"+commodity.getIfVector().elementAt(i)+"\">"+commodity.getCsVector().elementAt(i)+"</option>";
//          }


         if(nColumn==0)
             //Ramya 16/11/2008 Typable Combo Box
//            return "<input type=\"text\" id=\""+sParam_EditCell1+Integer.toString(nColumn) +"\" name=\""+sParam_EditCell1+Integer.toString(nColumn) +"\" size=\"15\" onKeyPress=\"JavaScript:disableIndemnity();\" maxlength=\"40\" style=\"width:100\" value="+sData+" >" +
//                    "<input type=\"button\" hidefocus=\"1\" value=\"&#9660;\" style=" +
//                    "'height:19; width:18; font-family: Verdana;' onclick=\"JavaScript:menuActivate('"+sParam_EditCell1+Integer.toString(nColumn) +"', 'combodiv', 'combosel')\">"+
//                    "<div id=\"combodiv\" style=\"position:absolute; display:none; top:0px;left:0px;z-index:10000\" onmouseover=\"javascript:oOverMenu='combodiv';\""+
//                    "onmouseout=\"javascript:oOverMenu=false;\">"+
//                    "<select size=\"5\" id=\"combosel\" style=\"width: 150; border-style: none\""+
//                    "onclick=\"JavaScript:textSet('"+sParam_EditCell1+Integer.toString(nColumn) +"',this.options[this.selectedIndex].text,this.value);\""+
//                    "onkeypress=\"JavaScript:comboKey('"+sParam_EditCell1+Integer.toString(nColumn) +"', this.options[this.selectedIndex].text,this.value);\">"+
//                    sOptions+
//                    "</select></div>";
             
             //Ramya replacing with autocomplete textbox
             return "<div id=\"comAutoComplete\" class=\"yui-skin-sam\">"+
                    "<input type=\"text\" id=\""+sParam_EditCell1+Integer.toString(nColumn) +"\" name=\""+sParam_EditCell1+Integer.toString(nColumn) +"\" style=\"width:90px;\" maxlength=\"40\" value="+sData+" >"+
                    "<br><div id=\"comContainer\"></div>"+
                    "</div>";
                    //disableIndemnity(oSelf._elTextbox.value); function is called in autocomplete-min.js

//              return "<div id=\"myAutoComplete\">"+
//                     "<input id=\"myInput\" type=\"text\">"+
//                     "<div id=\"myContainer\"></div>"+
//                     "</div>";

         else
             if(nColumn==3)
                 return "<SELECT NAME=\"" + sParam_EditCell1 +
                  Integer.toString(nColumn) + "\" id=\"" + sParam_EditCell1 +
                  //Integer.toString(nColumn) + "\" onchange=\"JavaScript:indemnityClick('"+sParam_EditCell1+Integer.toString(nColumn) +"');\" disabled>\n" +
                  Integer.toString(nColumn) + "\" onchange=\"JavaScript:indemnityClick('"+sParam_EditCell1+Integer.toString(nColumn) +"');\" >\n" +
                  "<OPTION VALUE=SELECT" +
                  (sData.equals("SELECT")?" SELECTED":"") + ">SELECT\n" +
                  "<OPTION VALUE=\"YES\"" +
                  (sData.equals("1")?" SELECTED":"") + ">YES\n" +
                  "<OPTION VALUE=\"NO\"" +
                  (sData.equals("0")?" SELECTED":"") + ">NO\n" +
                  "</SELECT>";
                // return "<input type=\"checkbox\" name=\""+sParam_EditCell+Integer.toString(nColumn) +"\" id=\""+sParam_EditCell+Integer.toString(nColumn) +"\" onclick=\"javascript:window.open('Indemnity.html','Calendar','width=250,height=130','toolbar=no','resizable=no')\">";

        else
            return super.sBuildCellEditor1(nColumn, sData,validSession);
    }

    protected void vGetMasterData(HttpServletRequest reqInput)
    {
//        nColumnCount = 6;
        nColumnCount = 5;
        nColumnCount1 = 5;
    }

   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
//         try {
//            if (isLogin(request)) {
//
//
//            } else{
//                writeOutputLog("Session expired redirecting to index page");
//                response.sendRedirect("index.jsp");
//            }
//        } catch (Exception ex) {
//            vShowErrorPage(response, ex);
//        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
