package citysprint.co.uk.NDI;

/*
 * ImportCSV.java
 *
 * Created on 26 March 2008, 10:41
 */
import au.com.bytecode.opencsv.CSVReader;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

import com.qas.proweb.*;
import com.qas.proweb.servlet.*;

import javax.swing.*;

import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.FilenameUtils;
import java.util.*;
import java.io.File;
import java.lang.Exception;

/**
 *
 * @author ramyas
 * @version
 */
public class ImportCSV extends CommonClass {

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    String FileUpload = "";
    String rdAddressBook = "";
    String sDirName = "";
    String sError = "";
    String[] asUserInput = null;
    String sPage = "";
    String VerifyInfo = "";
    String ErrorInfo = "";
    String[] asAddress = null;
    String[] resultaddress = null;
    JProgressBar pb;
    //Timer timer;
    ProgressBar pbar;
    boolean invalidcountry;

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");

        if (isLogin(request)) {
            //initialize config values
            initializeConfigValues(request, response);

            //Read session values
            //getSessionValues(request);



            try {
                //Get the Multipart form data

                if (parseRequestData(request, response)) {
                    clearImportAddress(request.getSession(false).getAttribute("sessionid").toString());
                    LoadPage(request, response);
                } else {
                    strmResponse.println("<html>");
                    strmResponse.println("<head>");
                    strmResponse.println("<title>CitySprint - Import Wizard</title>");
                    strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
                    strmResponse.println("</head>");
                    strmResponse.println("<body>");
                    strmResponse.println("<br>");
                    strmResponse.println("<table border=\"0\" cellpadding=\"5\" cellspacing=\"2\" id=\"box\" width=\"95%\" align=\"center\">");
                    strmResponse.println("<tr>");
                    strmResponse.println("<td style=\"color:red;font-weight:bold;font-size:14px\"><br>");
                    strmResponse.println("The file selected for upload does not exist in the specified path or is invalid");
                    strmResponse.println("</td>");
                    strmResponse.println("</tr>");
                    strmResponse.println("<tr>");
                    strmResponse.println("<td><br>");
                    strmResponse.println("<a href=\"AddressBook?sAction=AddressBook&sType=C\" target=\"_self\">Return to Address Book</a>");
                    strmResponse.println("</td>");
                    strmResponse.println("</tr>");
                    strmResponse.println("</table>");
                    strmResponse.println("</body>");
                    strmResponse.println("</html>");
                }



            } catch (Exception ex) {
                vShowErrorPage(response, ex);
            }



        } else {
            writeOutputLog("Session expired redirecting to index page");
            response.sendRedirect("index.jsp");
        }

    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void LoadPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");

        HttpSession validSession = request.getSession(false);
        String sSessionID = validSession.getAttribute("sessionid").toString();
        String acc_id = validSession.getAttribute("acc_id").toString();

        CountryInfo country = new CountryInfo();

        Connection conDB = null;
        Statement stmSelect;
        ResultSet rstRead;
        int totalRecords = 0, j = 0;
        int ilen = 0;
        int imported = 0;
        int failureRecords, successRecords = 0;
        int i = 0;

        //get and parse country details
        //getCountries(request,response);
        if (validSession.getAttribute("getCountriesArray") != null) {
            country.parseCountries((String[][]) validSession.getAttribute("getCountriesArray"));
        }

        try {


//            if (UploadFile(sSessionID)) {
            if (parseImportedCSV(sSessionID, acc_id)) {

                //Get total records imported
                String sCountSql = "Select count(*) as totalrecords from import_address where sessionid='" + sSessionID + "'";

                conDB = cOpenConnection();
                stmSelect = conDB.createStatement();
                rstRead = stmSelect.executeQuery(sCountSql);

                while (rstRead.next()) {
                    totalRecords = rstRead.getInt("totalrecords");
                }

                vCloseConnection(conDB);
                //Set the progress bar length
                // pbar = new ProgressBar(totalRecords);
                //pbar.progresslength=totalRecords;
                // writeOutputLog("Progressbar"+pbar.i);

                // If replace option is choosen first delete all the addresses belonging to the logged in user.
                if (rdAddressBook.equals("1")) {
                    deleteExistingAddresses(acc_id);
                }

                String sSQL = "Select * from import_address where sessionid='" + sSessionID + "'";


                conDB = cOpenConnection();
                stmSelect = conDB.createStatement();
                rstRead = stmSelect.executeQuery(sSQL);

                while (rstRead.next()) {
                    invalidcountry = false;
//                    pbar.pb.setValue(i);
//                    pbar.timer.start();
//                    pbar.setAlwaysOnTop(true);
//                    pbar.pb.setFocusable(true);
//                    pbar.setLocation(300,300);
                    i = i + 1;
                    //writeOutputLog("Progressbar"+pbar.i);
                    //strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">var lblMessage=document.getElementById(\"lblMessage\");alert(lblMessage);lblMessage.innerHTML=\"Please wait while the file is been uploaded ...\n "+i+" record(s) processed\"</script>");


                    asUserInput = new String[7];

                    asUserInput[0] = rstRead.getString("company");
                    asUserInput[1] = rstRead.getString("address1");
                    asUserInput[2] = rstRead.getString("address2");
                    asUserInput[3] = rstRead.getString("town");
                    asUserInput[4] = rstRead.getString("county");
                    asUserInput[5] = rstRead.getString("postcode");
                    asUserInput[6] = rstRead.getString("country");

                    //only UK addresses should be passed through QAS Validation
                    if (rstRead.getString("country").equalsIgnoreCase(Constants.COUNTRY_VALUE)) {
                        request.setAttribute(Constants.USER_INPUT, asUserInput);
                        request.setAttribute(Constants.COMMAND, Verify.NAME);
                        request.setAttribute(Constants.COUNTRY_NAME, Constants.COUNTRY_VALUE);
                        request.setAttribute(Constants.DATA_ID, Constants.COUNTRY_CODE);

                        //Inialising QAS constants
                        String sEndpoint = getServletContext().getInitParameter("QasEndpoint");
                        String sLayout = getServletContext().getInitParameter("QasLayout");

                        Constants.ENDPOINT = sEndpoint;
                        Constants.LAYOUT = sLayout;

                        Command cmd = HttpHelper.getCommand(request);
                        System.out.println("<<<<<<<<<<<<<< Before calling QAS, Command ==>" + cmd);
                        // perform custom operation
                        sPage = cmd.execute(request, response);
                        System.out.println("<<<<<<<<<<<<<< After calling QAS, the output ==>" + sPage);
                        //if result is verified exactly then asAddress would be returned.
                        asAddress = (String[]) request.getAttribute(Constants.ADDRESS);
                        VerifyInfo = request.getAttribute(Constants.VERIFY_INFO) == null ? "None" : (String) request.getAttribute(Constants.VERIFY_INFO);
                        ErrorInfo = (String) request.getAttribute(Constants.ERROR_INFO);

                        if (VerifyInfo.equals(SearchResult.Verified)) {
                            asAddress[6] = "GBR";
                        }
                    } else {
                        if (country.getCnVector().contains(rstRead.getString("country").toUpperCase())) {
                            //For International addresses set address as verified and assign 
                            //user input to asAddress array.                            
                            asAddress = new String[9];
                            asAddress[0] = asUserInput[0];
                            asAddress[1] = asUserInput[1];
                            asAddress[2] = asUserInput[2];
                            asAddress[3] = asUserInput[3];
                            asAddress[4] = asUserInput[4];
                            asAddress[5] = asUserInput[5];
                            asAddress[6] = country.getCyVector().elementAt(country.getCnVector().indexOf(rstRead.getString("country").toUpperCase())).toString();
                            asAddress[7] = "";
                            asAddress[8] = "";
                            VerifyInfo = SearchResult.Verified;
                            invalidcountry = false;
                        } else {
                            VerifyInfo = SearchResult.None;
                            invalidcountry = true;
                        }
                    }

                    if (VerifyInfo.equals(SearchResult.Verified)) {

                        ilen = asAddress.length;
                        resultaddress = new String[ilen + 6];
                        System.arraycopy(asAddress, 0, resultaddress, 0, ilen);


                        resultaddress[ilen++] = rstRead.getString("address3");
                        resultaddress[ilen++] = rstRead.getString("contact");
                        resultaddress[ilen++] = rstRead.getString("phone");
                        resultaddress[ilen++] = rstRead.getString("instructions");
                        resultaddress[ilen++] = rstRead.getString("indicator");
                        resultaddress[ilen++] = (rstRead.getString("closesat") == null ? "" : rstRead.getString("closesat"));

                        if (ImportAddress(resultaddress, acc_id)) {
                            imported = 1;
                            setStatus(rstRead.getInt("ID"), imported, "Success");
                        } else {
                            imported = 0;
                            setStatus(rstRead.getInt("ID"), imported, sError);
                        }

                    } else {
                        if (invalidcountry) {
                            sError = "Invalid Country";
                        } else {
                            sError = "Address did not validate in QAS " + VerifyInfo;
                        }

                        imported = 0;
                        if (ErrorInfo != null) {
                            sError = sError + " " + ErrorInfo;
                        }

                        setStatus(rstRead.getInt("ID"), imported, sError);
                    }
                }
                vCloseConnection(conDB);
                //Show status table

                strmResponse.println("<html>");
                strmResponse.println("<head>");
                strmResponse.println("<title>CitySprint - Import Wizard</title>");
                strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
                strmResponse.println("</head>");
                strmResponse.println("<body>");
                strmResponse.println("<br>");
                strmResponse.println("<table border=\"0\" cellpadding=\"5\" cellspacing=\"2\" id=\"box\" width=\"95%\" align=\"center\">");
                strmResponse.println("<tr>");
                strmResponse.println("<td><br>");
                strmResponse.println(totalRecords + " record(s) in CSV file.");
                strmResponse.println("</td>");
                strmResponse.println("</tr>");

                String sCountSql1 = "Select count(*) as failure from import_address where imported=0 and sessionid='" + sSessionID + "'";
                String sSql = "Select company,postcode,errormsg from import_address where imported=0 and sessionid='" + sSessionID + "'";

                try {

                    conDB = cOpenConnection();
                    stmSelect = conDB.createStatement();
                    rstRead = stmSelect.executeQuery(sCountSql1);

                    while (rstRead.next()) {
                        failureRecords = rstRead.getInt("failure");
                        successRecords = totalRecords - failureRecords;

                        if (failureRecords > 0) {
                            strmResponse.println("<tr>");
                            strmResponse.println("<td><br>");
                            strmResponse.println(successRecords + " record(s) imported successfully");
                            strmResponse.println("</td>");
                            strmResponse.println("</tr>");
                            strmResponse.println("<tr>");
                            strmResponse.println("<td colspan=\"4\"><br>");
                            strmResponse.println(failureRecords + " record(s) were not imported for the following reasons<br>");
                            strmResponse.println("</td>");
                            strmResponse.println("</tr>");

                            rstRead.close();

                            rstRead = stmSelect.executeQuery(sSql);

                            strmResponse.println("<tr>");
                            //strmResponse.println("<td id=\"boxtitle\">Record</td>");
                            strmResponse.println("<td id=\"boxtitle\">Company</td>");
                            strmResponse.println("<td id=\"boxtitle\">Postcode</td>");
                            strmResponse.println("<td id=\"boxtitle\">Reason</td>");
                            strmResponse.println("</tr>");

                            while (rstRead.next()) {
                                strmResponse.println("<tr>");
                                strmResponse.println("<td>" + rstRead.getString("company") + "</td>");
                                strmResponse.println("<td>" + rstRead.getString("postcode") + "</td>");
                                strmResponse.println("<td>" + rstRead.getString("errormsg") + "</td>");
                                strmResponse.println("</tr>");
                            }
                        } else {
                            if (totalRecords == successRecords) {
                                strmResponse.println("<tr>");
                                strmResponse.println("<td>");
                                strmResponse.println(successRecords + " record(s) imported successfully");
                                strmResponse.println("</td>");
                                strmResponse.println("</tr>");
                            }
                        }
                    }

                    vCloseConnection(conDB);
                } catch (Exception ex) {
                    vShowErrorPage(response, ex);
                }

                strmResponse.println("<tr>");
                strmResponse.println("<td>");
                strmResponse.println("<br><a href=\"AddressBook?sAction=AddressBook&sType=C\" target=\"_self\">Return to Address Book</a><br>");
                strmResponse.println("</td>");
                strmResponse.println("</tr>");
                strmResponse.println("</table>");
                strmResponse.println("</body>");
                strmResponse.println("</html>");

            } else {
                //vShowErrorPage(response,sError);
                strmResponse.println("<html>");
                strmResponse.println("<head>");
                strmResponse.println("<title>CitySprint - Import Wizard</title>");
                strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");
                strmResponse.println("</head>");
                strmResponse.println("<body>");
                strmResponse.println("<br>");
                strmResponse.println("<table border=\"0\" cellpadding=\"5\" cellspacing=\"2\" id=\"box\" width=\"95%\" align=\"center\">");
                strmResponse.println("<tr>");
                strmResponse.println("<td style=\"color:red;font-weight:bold;font-size:14px\"><br>");
                strmResponse.println(sError);
                strmResponse.println("</td>");
                strmResponse.println("</tr>");
                strmResponse.println("<tr>");
                strmResponse.println("<td><br>");
                strmResponse.println("<a href=\"AddressBook?sAction=AddressBook&sType=C\" target=\"_self\">Return to Address Book</a>");
                strmResponse.println("</td>");
                strmResponse.println("</tr>");
                strmResponse.println("</table>");
                strmResponse.println("</body>");
                strmResponse.println("</html>");
            }
        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }

        //pbar.timer.stop();
        //pbar.frame.dispose();




    }

    /**
     *
     * @param sSessionID
     * @return
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
//    protected boolean UploadFile(String sSessionID) throws SQLException, IOException {
////sDirName
//        // Following line is being added by Adnan against CSV Import process change
//        String sDirName4MySQL = bdl.getString("sTempLocationForMySQL");//.replace("\\", "\\\\");
//
//        String sImportSql = "LOAD DATA INFILE '" + sDirName4MySQL + FileUpload + "' INTO TABLE import_address "
//                + "FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' (company , address1, address2, address3, town, county, postcode, country, contact, phone, instructions, closesat, indicator) "
//                + "SET sessionid='" + sSessionID + "'";
//        
//        Connection conDB = null;
//        Statement stmInsert;
//
//
//        try {
//
//            conDB = cOpenConnection();
//            stmInsert = conDB.createStatement();
//            stmInsert.executeUpdate(sImportSql);
//            return true;
//        } catch (Exception ex) {
//            sError = "Invalid CSV format<br>";
//            sError = sError + "File upload failed due to : " + ex.getMessage();
//            return false;
//        } finally {
//            conDB.close();
//        }
//    }

    /**
     *
     * @param asAddress
     * @param acc_id
     * @return
     * @throws java.sql.SQLException
     */
    protected boolean ImportAddress(String[] asAddress, String acc_id) throws SQLException {
        String sImportSql = "";
        String sUpdate = "";
        boolean bAddressExists;
        boolean bInsert = false;
        boolean bResult = false;
        int collection = 0;
        int delivery = 0;
        String sClosesBy = asAddress[14];
        String[] asClosesBy = null;
        String sClosesByHH = "", sClosesByMM = "";

        if (sClosesBy.length() != 0) {
            asClosesBy = sClosesBy.split(":");

            if (asClosesBy.length > 1) {
                sClosesByHH = asClosesBy[0] == "" ? "00" : asClosesBy[0];
                sClosesByMM = asClosesBy[1] == "" ? "00" : asClosesBy[1];
            }
        } else {
            sClosesByHH = "00";
            sClosesByMM = "00";
        }


        int iClosesByMM = Integer.parseInt(sClosesByMM);
        iClosesByMM = iClosesByMM / 15;
        iClosesByMM = 15 * iClosesByMM;
        sClosesByMM = "" + iClosesByMM;

        if (sClosesByMM.length() == 1) {
            sClosesByMM = "0" + sClosesByMM;
        }

        if (asAddress[13].equals("C")) {
            collection = 1;
        } else if (asAddress[13].equals("D")) {
            delivery = 1;
        } else if (asAddress[13].equals("B")) {
            collection = 1;
            delivery = 1;
        }

        if (rdAddressBook.equals("1")) {
            bInsert = true;
        } else if (rdAddressBook.equals("2")) {
            bAddressExists = checkAddressExists(asAddress[0], asAddress[3], acc_id);
            if (bAddressExists) {
                bInsert = false;
                sError = "Address already exists";
            } else {
                bInsert = true;
            }
        } else if (rdAddressBook.equals("3")) {
            bAddressExists = checkAddressExists(asAddress[0], asAddress[3], acc_id);

            if (bAddressExists) {
                bInsert = false;
            } else {
                bInsert = true;
            }
        }

        if (bInsert) {
            sImportSql = "Insert into address (owned_by , server, environment, company, address1 ,  address2 , town, county, postcode, country, address3, contact , phone, instructions , collection,  delivery, closesat, latitude, longitude) values"
                    + "('" + acc_id + "','" + host + "','" + environment + "','" + sReplaceChars(asAddress[0], "'", "''") + "','" + sReplaceChars(asAddress[1], "'", "''") + "','" + sReplaceChars(asAddress[2], "'", "''") + "','" + sReplaceChars(asAddress[3], "'", "''") + "','" + sReplaceChars(asAddress[4], "'", "''") + "','" + sReplaceChars(asAddress[5], "'", "''") + "','" + sReplaceChars(asAddress[6], "'", "''") + "','" + sReplaceChars(asAddress[9], "'", "''") + "','" + sReplaceChars(asAddress[10], "'", "''") + "','" + sReplaceChars(asAddress[11], "'", "''") + "',"
                    + "'" + asAddress[12] + "'," + collection + "," + delivery + ",'" + sClosesByHH + ":" + sClosesByMM + "', '" + asAddress[7] + "', '" + asAddress[8] + "')";
            Connection conDB = null;
            Statement stmInsert;


            try {

                conDB = cOpenConnection();
                stmInsert = conDB.createStatement();
                stmInsert.executeUpdate(sImportSql);
                bResult = true;
            } catch (Exception ex) {
                sError = "Import address failed due to : " + ex.getMessage();
                bResult = false;
            } finally {
                conDB.close();
            }

        } else if (rdAddressBook.equals("3")) {
            int iID = getAddressID(asAddress[0], asAddress[3], acc_id);

            if (iID > 0) {
                sUpdate = "Update address set owned_by='" + acc_id + "', server='" + host + "', environment='" + environment + "', company='" + sReplaceChars(asAddress[0], "'", "''") + "', address1='" + sReplaceChars(asAddress[1], "'", "''") + "', "
                        + "address2='" + sReplaceChars(asAddress[2], "'", "''") + "', town='" + sReplaceChars(asAddress[3], "'", "''") + "', county='" + sReplaceChars(asAddress[4], "'", "''") + "', postcode='" + sReplaceChars(asAddress[5], "'", "''") + "', country='" + sReplaceChars(asAddress[6], "'", "''") + "', "
                        + "address3='" + sReplaceChars(asAddress[9], "'", "''") + "', contact='" + sReplaceChars(asAddress[10], "'", "''") + "', phone='" + sReplaceChars(asAddress[11], "'", "''") + "', instructions='" + sReplaceChars(asAddress[12], "'", "''") + "', collection=" + collection + ", "
                        + "delivery=" + delivery + ", closesat='" + sClosesByHH + ":" + sClosesByMM + "', latitude='" + asAddress[7] + "', longitude='" + asAddress[8] + "' where ID=" + iID;

                Connection conDB = null;
                Statement stmInsert;


                try {

                    conDB = cOpenConnection();
                    stmInsert = conDB.createStatement();
                    stmInsert.executeUpdate(sUpdate);
                    bResult = true;
                } catch (Exception ex) {
                    sError = "Update address failed due to : " + ex.getMessage();
                    bResult = false;
                } finally {
                    conDB.close();
                }
            }
        }

        return bResult;
    }

    /**
     *
     * @param sId
     * @param status
     * @param message
     * @return
     * @throws java.sql.SQLException
     */
    protected boolean setStatus(int sId, int status, String message) throws SQLException {
        String sSql = "Update import_address set imported=" + status + ", errormsg='" + message + "' where ID=" + sId;
        Connection conDB = null;
        Statement stmUpdate;


        try {

            conDB = cOpenConnection();
            stmUpdate = conDB.createStatement();
            stmUpdate.executeUpdate(sSql);
            return true;
        } catch (Exception ex) {
            return false;
        } finally {
            conDB.close();
        }
    }

    /**
     *
     * @param sCompany
     * @param sTown
     * @param acc_id
     * @return
     * @throws java.sql.SQLException
     */
    protected boolean checkAddressExists(String sCompany, String sTown, String acc_id) throws SQLException {

        String sSql = "Select 1 as result from address where UCASE(company)='" + sCompany.toUpperCase() + "' and UCASE(town)='" + sTown.toUpperCase() + "' and owned_by='" + acc_id + "' and server='" + host + "' and environment='" + environment + "'";
        Connection conDB = null;
        Statement stmSelect;
        ResultSet rstRead;
        boolean bResult = false;

        try {

            conDB = cOpenConnection();
            stmSelect = conDB.createStatement();
            rstRead = stmSelect.executeQuery(sSql);

            while (rstRead.next()) {
                int result = rstRead.getInt("result");

                if (result == 1) {
                    bResult = true;
                } else {
                    bResult = false;
                }
            }
            vCloseConnection(conDB);
        } catch (Exception ex) {
            throw new SQLException("<B>Error in checkAddressExists</B><BR>"
                    + ex.toString());
        }

        return bResult;
    }

    /**
     *
     * @param sCompany
     * @param sTown
     * @param acc_id
     * @return
     * @throws java.sql.SQLException
     */
    protected int getAddressID(String sCompany, String sTown, String acc_id) throws SQLException {

        String sSql = "Select ID from address where UCASE(company)='" + sCompany.toUpperCase() + "' and UCASE(town)='" + sTown.toUpperCase() + "' and owned_by='" + acc_id + "' and server='" + host + "' and environment='" + environment + "'";
        Connection conDB = null;
        Statement stmSelect;
        ResultSet rstRead;
        int result = 0;

        try {

            conDB = cOpenConnection();
            stmSelect = conDB.createStatement();
            rstRead = stmSelect.executeQuery(sSql);

            while (rstRead.next()) {
                result = rstRead.getInt("ID");

            }
            vCloseConnection(conDB);

        } catch (Exception ex) {
            throw new SQLException("<B>Error in getAddressID</B><BR>"
                    + ex.toString());
        }

        return result;
    }

    /**
     *
     * @param acc_id
     * @throws java.sql.SQLException
     */
    protected void deleteExistingAddresses(String acc_id) throws SQLException {
        String sDelete = "Delete from address where owned_by='" + acc_id + "' and server='" + host + "' and environment='" + environment + "'";

        Connection conDB = null;
        Statement stmDelete;

        try {

            conDB = cOpenConnection();
            stmDelete = conDB.createStatement();
            stmDelete.executeUpdate(sDelete);
            vCloseConnection(conDB);

        } catch (Exception ex) {
            throw new SQLException("<B>Error in deleteExistingAddresses</B><BR>"
                    + ex.toString());
        }
    }

    /**
     *
     * @param sSessionID
     * @throws java.sql.SQLException
     */
    protected void clearImportAddress(String sSessionID) throws SQLException {
        String sDelete = "Delete from import_address where sessionid='" + sSessionID + "'";

        Connection conDB = null;
        Statement stmDelete;

        try {

            conDB = cOpenConnection();
            stmDelete = conDB.createStatement();
            stmDelete.executeUpdate(sDelete);
            vCloseConnection(conDB);

        } catch (Exception ex) {
            throw new SQLException("<B>Error in clearImportAddress</B><BR>"
                    + ex.toString());
        }
    }

    /**
     *
     * @param request
     * @param response
     * @return
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.apache.commons.fileupload.FileUploadException
     */
    protected boolean parseRequestData(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, FileUploadException {

        boolean result = true;

        if (ServletFileUpload.isMultipartContent(request)) {

            ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
            java.util.List fileItemsList = servletFileUpload.parseRequest(request);

            String optionalFileName = "";
            FileItem fileItem = null;

            Iterator it = fileItemsList.iterator();
            while (it.hasNext()) {
                FileItem fileItemTemp = (FileItem) it.next();
                if (fileItemTemp.isFormField()) {
                    if (fileItemTemp.getFieldName().equals("rdAddressBook")) {
                        rdAddressBook = fileItemTemp.getString();
                    }
                } else {
                    fileItem = fileItemTemp;
                }
            }

            if (fileItem != null) {
                String fileName = fileItem.getName();

                /* Save the uploaded file if its size is greater than 0. */
                String sType = fileItem.getContentType();
                if (fileItem.getSize() > 0) {
                    if (optionalFileName.trim().equals("")) {
                        FileUpload = FilenameUtils.getName(fileName);
                    } else {
                        FileUpload = optionalFileName;
                    }


                    sDirName = bdl.getString("sTempLocation").replace("\\", "\\\\");
                    sDirName = sDirName.replace("//", "////");

                    File saveTo = new File(sDirName + FileUpload);
                    try {
                        fileItem.write(saveTo);
//                        String scpUser = bdl.getString("scpUser");
//                        String scpPass = bdl.getString("scpPass");
//                        String remoteDir = bdl.getString("sTempLocationForMySQL").replace("\\", "\\\\");
//                        remoteDir = remoteDir.replace("//", "////");
//                        
//                        this.sftpFile(saveTo, scpUser, scpPass, bdl.getString("remoteHost"), remoteDir);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    result = false;
                }
            } else {
                result = false;
            }
        }

        return result;
    }

    protected boolean uploadFile(PreparedStatement stmInsert, String[] strLine, String sSessionID, String acc_id) throws SQLException, IOException {
//sDirName
        // Following line is being added by Adnan against CSV Import process change
        String sDirName4MySQL = bdl.getString("sTempLocationForMySQL");//.replace("\\", "\\\\");

//        String sImportSql = "LOAD DATA INFILE '" + sDirName4MySQL + FileUpload + "' INTO TABLE import_address "
//                + "FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' (company , address1, address2, address3, town, county, postcode, country, contact, phone, instructions, closesat, indicator) "
//                + "SET sessionid='" + sSessionID + "'";
        
        try{
            int indx = 0;
            for (; indx < strLine.length ; indx++){
                System.out.println("\n index [" + (indx+1) + "] value ="+ strLine[indx]);
                stmInsert.setString((indx+1), strLine[indx]);
            }
            
            System.out.println("\n index [" + (indx+1) + "] value ="+ sSessionID);          
            stmInsert.setString(++indx, sSessionID);
            System.out.println("\n index [" + (indx+1) + "] value ="+ acc_id);
            stmInsert.setString(++indx, acc_id);
            System.out.println("\n index [" + (indx+1) + "] value ="+ host);
            stmInsert.setString(++indx, host);
            stmInsert.addBatch();
//            stmInsert.executeUpdate(sImportSql);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            sError = "Invalid CSV format<br>";
            sError = sError + "File upload failed due to : " + ex.getMessage();
            return false;
        } 
    }
    
    private boolean parseImportedCSV(String sSessionID, String acc_id) {
//        String[] strArr= null; 
//        PreparedStatement pstmt;
         String sImportSql = "INSERT INTO import_address "
                + "(company , address1, address2, address3, town, county, postcode, country, contact, phone, instructions, closesat, indicator, sessionid, owned_by , server) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ;";
        Connection conDB = null;
//        Statement stmInsert;
        PreparedStatement stmInsert = null;


        try {

            conDB = cOpenConnection();
//            stmInsert = conDB.createStatement();
            stmInsert = conDB.prepareStatement(sImportSql);
            
        
//        File saveTo = new File(sDirName + FileUpload);
            // Open the file that is the first 
            // command line parameter
//            FileInputStream fstream = new FileInputStream(sDirName + FileUpload);
            // Get the object of DataInputStream
            CSVReader reader = new CSVReader(new FileReader(sDirName + FileUpload));
            
            String [] nextLine;
             while ((nextLine = reader.readNext()) != null) {
                 boolean result = this.uploadFile( stmInsert, nextLine, sSessionID, acc_id);
                 System.out.println(nextLine + ", \n and result =" + result);
             }
//            DataInputStream in = new DataInputStream(fstream);
//            BufferedReader br = new BufferedReader(new InputStreamReader(in));
//            String strLine;
            //Read File Line By Line
//            while ((strLine = br.readLine()) != null) {
//                // Print the content on the console
//                strArr = strLine.split(",");
//                boolean result = this.uploadFile( stmInsert, strArr, sSessionID, acc_id);
//                System.out.println(strLine + ", \n and result =" + result);
//            }
            stmInsert.executeBatch();
            //Close the input stream
//            in.close();
            return true;
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
            return false;
        }finally {
             try{
                 if (stmInsert != null)
                    stmInsert.close();
            }catch(SQLException sqle){}
             
            try{
                if (conDB != null)
                    conDB.close();
            }catch(SQLException sqle){}
        }
}
    
    private String cleanImportedCSVLineFrmInvalidChars(String line){
        String result = line;
        line.replaceAll("\"", "");
        line.replaceAll("&", "");
        line.replaceAll(";", "");
        line.replaceAll("<", "");
        line.replaceAll(">", "");
//        line.replaceAll("&", "");
        
        return result;    
    }
/**
 * 
 * @param localFile
 * @param username
 * @param password
 * @param hostname
 * @param remoteDirectory
 * @throws JSchException
 * @throws SftpException
 * @throws FileNotFoundException 
 */
private static void sftpFile(File localFile, String username, String password, String hostname, String remoteDirectory)
            throws JSchException, SftpException, FileNotFoundException {
 
        JSch jsch = new JSch();
        String filename = localFile.getName();
        Session session = jsch.getSession(username, hostname, 22);
        session.setPassword(password);
//        session.setUserInfo(new HardcodedUserInfo(password));
        Properties config = new Properties();
        config.setProperty("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.connect();
        ChannelSftp channel = (ChannelSftp)session.openChannel("sftp");
        channel.connect();
        channel.cd(remoteDirectory);
        channel.put(new FileInputStream(localFile), filename);
        channel.disconnect();
        session.disconnect();
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     * @return
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
