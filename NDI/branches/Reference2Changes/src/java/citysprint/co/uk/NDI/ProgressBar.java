package citysprint.co.uk.NDI;

/*
 * ProgressBar.java
 *
 * Created on 16 April 2008, 14:49
 */

import java.io.*;
import java.net.*;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.html.*;
/**
 *
 * @author ramyas
 * @version
 */
public class ProgressBar extends JFrame 
{
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    final static int interval = 1000;
    int i;
    JLabel label;
    JProgressBar pb;
    Timer timer;
    JButton button;
    JFrame frame;
    //int progresslength=1000;
    
    /**
     *
     * @param progresslength
     */
    public ProgressBar(int progresslength)
    {
        frame = new JFrame("Upload Progress Bar");
       // button = new JButton("Start");
       // button.addActionListener(new ButtonListener());
        
        label = new JLabel("");
         
       
            
        pb = new JProgressBar(0, progresslength);
        pb.setValue(0);
        pb.setStringPainted(true);

            
        JPanel panel = new JPanel();
        //panel.add(button);
        panel.add(pb);

        JPanel panel1 = new JPanel();
        panel1.setLayout(new BorderLayout());
        panel1.add(panel, BorderLayout.NORTH);
        panel1.add(label, BorderLayout.CENTER);
        panel1.setBorder(BorderFactory.createEmptyBorder(40, 40, 40, 40));
        frame.setContentPane(panel1);
        
        frame.pack();
        frame.setLocation(150,150);
        frame.setFocusable(true);
        frame.setVisible(true);
        
             
          
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create a timer.
        timer = new Timer(interval, new ActionListener()
        {
            public void actionPerformed(ActionEvent evt) {
//            if (i == 2000){
//            Toolkit.getDefaultToolkit().beep();
//            timer.stop();
//            button.setEnabled(true);
//            pb.setValue(0);
//            String str = "<html>" + "<font color=\"#FF0000\">" + "<b>" + 
//            "Downloading completed." + "</b>" + "</font>" + "</html>";
//            label.setText(str);
//        }
        }});
        
        i = 0;
        String str = "<html>" + "<font color=\"#008000\">" + "<b>" + 
        "Uploading is in process......." + "</b>" + "</font>" + "</html>";
        label.setText(str);
        
    }

    class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent ae) {
            button.setEnabled(false);
        i = 0;
        String str = "<html>" + "<font color=\"#008000\">" + "<b>" + 
        "Uploading is in process......." + "</b>" + "</font>" + "</html>";
        label.setText(str);
            timer.start();
        }
    }
    
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        //ProgressBar spb = new ProgressBar();
    }
}
    
    
  

