/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citysprint.co.uk.NDI;

/**
 *
 * @author ramyas
 */
public class ReportSQLs {

    private String sql = "", sqlCSV = "";

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public String getSqlCSV() {
        return sqlCSV;
    }

    public void setSqlCSV(String sqlCSV) {
        this.sqlCSV = sqlCSV;
    }

}
