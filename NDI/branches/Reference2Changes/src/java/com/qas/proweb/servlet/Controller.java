/* ----------------------------------------------------------------------------
 * QuickAddress Pro Web
 * (c) 2004 QAS Ltd. All rights reserved.
 * File: Controller.java
 * Created: 19-Apr-2004
 * ----------------------------------------------------------------------------
 */
package com.qas.proweb.servlet;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Servlet that acts as the Command Controller.
 *
 * Uses HttpHelper as a Command factory.
 *
 */
public class Controller extends HttpServlet
{
	/**  Initializes the servlet - gets the init params from the servlet context and sets them in the Constants class */
	public void init(ServletConfig config)
		throws ServletException
	{

            super.init(config);

		String sEndpoint = getServletContext().getInitParameter("QasEndpoint");
		String sLayout = getServletContext().getInitParameter("QasLayout");

		Constants.ENDPOINT = sEndpoint;
		Constants.LAYOUT = sLayout;
	}

	/** nothing to do here */
	public void destroy()
	{
	}

	/** Processes requests for both HTTP
	 * <code>GET</code> and <code>POST</code> methods.
	 * 1) constructs the relevant <code>Command</code> object (by delegating to <code>HttpHelper</code>.),
	 * 2) calls <code>execute</code> on the new <code>Command</code> object
	 * 3) dispatches to the page returned by the <code>Command</code> object.
	 * @param request servlet request
	 * @param response servlet response
	 */
	protected void processRequest(HttpServletRequest request,
									HttpServletResponse response)
		throws ServletException, java.io.IOException
	{
		String sPage;
                String sAction="";

		try
		{
                        sAction=HttpHelper.passThrough(request, Constants.SACTION);

			// The helper extract the Command name from the request and creates the relevant object.
			Command cmd = HttpHelper.getCommand(request);
                         
			// perform custom operation
                        
			sPage = cmd.execute(request, response);
		}
		catch (Throwable e)
		{
			System.err.println("~~~~~~~~~ Controller:command exception ~~~~~~~~~~");
			e.printStackTrace();
			System.err.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			throw new ServletException(e);
		}
		// dispatch control to view
                if(sAction==null)
                    dispatch(request, response, sPage,"");
                else
                    dispatch(request, response, sPage,sAction);
	}

	/** Handles the HTTP <code>GET</code> method - delegates to <code>processRequest</code>
	 * @param request servlet request
	 * @param response servlet response
	 */
	protected void doGet(HttpServletRequest request,
						 HttpServletResponse response)
		throws ServletException, java.io.IOException
	{
		processRequest(request, response); 
	}

	/** Handles the HTTP <code>POST</code> method - delegates to <code>processRequest</code>
	 * @param request servlet request
	 * @param response servlet response
	 */
	protected void doPost(HttpServletRequest request,
						  HttpServletResponse response)
		throws ServletException, java.io.IOException
	{
		processRequest(request, response);
	}

	/** delegates the <code>forward</code> to the servlet context's <code>RequestDispatcher</code> */
	protected void dispatch(HttpServletRequest request,
							HttpServletResponse response,
							String sPage,String sAction)
		throws  javax.servlet.ServletException,
				java.io.IOException
	{
            RequestDispatcher dispatcher;
            String sAction_sub=request.getParameter(Constants.SACTION_SUB)==null?"":request.getParameter(Constants.SACTION_SUB).toString();
                        
            if(sAction.equals("QAS"))
            {
                if(sAction_sub.equals(HierSearch.NAME))
                {
                    dispatcher = getServletContext().getRequestDispatcher(sPage);
                    dispatcher.forward(request,response);
                }
                else
                {
                    dispatcher = getServletContext().getRequestDispatcher(Constants.ADDRESS_BOOK_PAGE);
                    dispatcher.forward(request, response);
                }
            }
            else if(sAction.equals(Constants.sBooking))
            {
                if(sAction_sub.equals(HierSearch.NAME))
                {
                    dispatcher = getServletContext().getRequestDispatcher(sPage);
                    dispatcher.forward(request,response);
                }
                else if(sAction_sub.equals(FlatSearch.NAME))
                {
                    dispatcher = getServletContext().getRequestDispatcher(Constants.POSTCODELOOKUP_PAGE);
                    dispatcher.forward(request,response);
                }
                else
                {
                    dispatcher = getServletContext().getRequestDispatcher(Constants.POSTCODELOOKUP_PAGE);
                    dispatcher.forward(request,response);
                }
                
            }
            else
            {
                dispatcher = getServletContext().getRequestDispatcher(sPage);
                dispatcher.forward(request,response);
            }
               
		
	}
}
