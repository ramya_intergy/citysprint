/* ----------------------------------------------------------------------------
 * QuickAddress Pro Web > (c) QAS Ltd > www.qas.com
 *
 * Web > Verification > Verify.java
 * Verify the address entered > Perfect match, best match, picklist of matches  
 * ----------------------------------------------------------------------------
 */
package com.qas.proweb.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import citysprint.co.uk.NDI.CommonClass;
import com.qas.proweb.*;

/* Command to encapsulate verify functionality where minimal workflow changes are required in the web
 * application.
 * Input params DataId, CountryName, Route and UserInput[]
 * (all set as output attributes).
 * Output attributes Address[], CountryName, Route and Verified.
 * ErrorInfo (if an exception is caught)
 * Address will contain the verified (cleaned) address if this was done to the "Verified" level,
 * and the final address page will be shown.
 * If displaying a picklist, PickMonikers[], PickTexts[], PickPostcodes[] and PickScores[] will be set.
 * If interaction with an address is required, Labels[] and Lines[] will be set.
 * Otherwise, interaction with no address or picklist is required provided this is the first time through
 * (indicated by the Route).
 */ 
public class Verify extends CommonClass implements Command
{
	public static final String NAME = "Verify";

	/** these data members are not intended to live across invocations of execute */
	private String m_sDataId = null;
	private String[] m_asUserInput = null;
	private SearchResult m_Result = null;
	private HttpServletRequest m_Request = null;
      
	/**
	 * @see com.qas.proweb.servlet.Command#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public String execute(HttpServletRequest request,
						  HttpServletResponse response)
	{
		m_Request = request;
                            
                        
                String sType=HttpHelper.passThrough(m_Request, Constants.STYPE);
		// input attributes/params
                //Ramya checking sType request parameter to validate Delivery address.
                if(sType.equals(Constants.USER_INPUT1))
                    m_asUserInput = HttpHelper.getArrayValue(m_Request, Constants.USER_INPUT1);
                else
                    m_asUserInput = HttpHelper.getArrayValue(m_Request, Constants.USER_INPUT);
                
		String [] asOriginalInput = HttpHelper.getArrayValue(m_Request, Constants.ORIGINAL_INPUT);

		// input & output
		String sCountryName = HttpHelper.passThrough(m_Request, Constants.COUNTRY_NAME);
		boolean bAlreadyVerified = HttpHelper.passThrough(m_Request, Constants.ROUTE).equals(Constants.ROUTE_ALREADY_VERIFIED);
		m_sDataId = HttpHelper.passThrough(m_Request, Constants.DATA_ID);
                
               // Layout[] sLayouts=getAllLayouts(m_sDataId);
                  
		if ( bAlreadyVerified && Arrays.equals(m_asUserInput, asOriginalInput) )
		{
			return noInteraction("");
		}

		// output attributes
		String sRoute = Constants.ROUTE_NORMAL;
		String[] asAddress = null;
		try
		{
			QuickAddress searchService = new QuickAddress(Constants.ENDPOINT);
			searchService.setEngineType(QuickAddress.VERIFICATION);
                        searchService.setFlatten(true);
                        Layout[] sLayouts=searchService.getAllLayouts(m_sDataId);
                        
			if (!bAlreadyVerified)
			{
				CanSearch canSearch = searchService.canSearch(m_sDataId, Constants.LAYOUT);
				if (!canSearch.isOk())
				{
					sRoute = Constants.ROUTE_PRE_SEARCH_FAILED;
					m_Request.setAttribute(Constants.ERROR_INFO, canSearch.getMessage());
                                        m_Request.setAttribute(Constants.VERIFY_INFO,SearchResult.None);
				}
			}
			
			if (sRoute.equals(Constants.ROUTE_NORMAL))
			{
				m_Result = searchService.search(m_sDataId, m_asUserInput, PromptSet.DEFAULT, Constants.LAYOUT);

				if (m_Result.getVerifyLevel().equals(SearchResult.Verified))
				{
					// Fully verified result, so formulate the output params (Address[] and Verify).
					AddressLine[] lines = m_Result.getAddress().getAddressLines();
					asAddress = new String[lines.length];
                                        String[] asLabels = new String[lines.length];
                                        
					for (int i=0; i < lines.length; i++)
					{
						asAddress[i] = lines[i].getLine();
                                                asLabels[i] = lines[i].getLabel();
					}

					// set the output attributes and return the final page
					m_Request.setAttribute(Constants.VERIFY_INFO, SearchResult.Verified);
					m_Request.setAttribute(Constants.ADDRESS, asAddress);
                                        m_Request.setAttribute(Constants.LABELS, asLabels);
                                       
                                       
					return "/" + Constants.FINAL_ADDRESS_PAGE;
				}
				else
				{
					/* If you require no interaction at all, replace with: return noInteraction(sVerifyLevel); */
					if (bAlreadyVerified)
					{
						// If we've been once through already then we want no interaction
						return noInteraction(m_Result.getVerifyLevel());
					}
					else
					{
						return processResultWithInteraction(m_Result.getVerifyLevel());
					}
				}
			}
		}
		catch (Exception e)
		{
			// dump the exception to error stream
//			System.err.println("~~~~~~~~~~~ Caught exception in Verify command ~~~~~~~~~~~~");
//			e.printStackTrace();
//			System.err.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
             try{
            vShowErrorPage(response, e);
            }
            catch(Exception ex){

            }
			sRoute = Constants.ROUTE_FAILED;
			m_Request.setAttribute(Constants.ERROR_INFO, e.toString());
                        
		}
                
                String[] asLabels = new String[m_asUserInput.length];
                                        
                for (int i=0; i < m_asUserInput.length; i++)
                {
                     switch(i)
                    {
                        case 0:
                            asLabels[i]="";
                            break;
                        case 1:
                            asLabels[i]="";
                        case 2:
                            asLabels[i]="";
                            break;
                        case 3:
                            asLabels[i]="Town";
                            break;
                        case 4:
                            asLabels[i]="County";
                            break;
                        case 5:
                            asLabels[i]="Postcode";
                            break;
                    }
                }
                
                m_Request.setAttribute(Constants.LABELS,asLabels);
		m_Request.setAttribute(Constants.USER_INPUT, m_asUserInput);
                m_Request.setAttribute(Constants.VERIFY_INFO,SearchResult.None);
                m_Request.setAttribute(Constants.ROUTE,sRoute);
		//m_Request.setAttribute(Constants.VERIFY_INFO, "address verification " + sRoute + ", so the entered address has been used");
		return "/" + Constants.FINAL_ADDRESS_PAGE;
	}

	/** Offer best match, or picklist of matches, when the verification level is too low */
	private String processResultWithInteraction(String sVerifyLevel)
	{
		// Set UserInput as an output attribute (used in all cases below)
		m_Request.setAttribute(Constants.USER_INPUT, m_asUserInput);

		// Three different cases - formatted address, picklist, neither:
		// if picklist is present we show the items
		// (we know there are no informationals as we are operating in flattened mode).
		// if address, we show the address
		// otherwise, we show
		Picklist picklist =  m_Result.getPicklist();
		if ( picklist == null )
		{   // in this (address) case we populate Lines and Labels output params
			AddressLine[] lines = m_Result.getAddress().getAddressLines();
			String[] asLines = new String[lines.length];
			String[] asLabels = new String[lines.length];
			for (int i=0; i < lines.length; i++)
			{
				asLines[i] = lines[i].getLine();
				asLabels[i] = lines[i].getLabel();
			}
			m_Request.setAttribute(Constants.LABELS, asLabels);
			m_Request.setAttribute(Constants.LINES, asLines);
		}
		else
		{   // picklist case, we populate PickMonikers, PickPreviews, PickScores
			PicklistItem[] items = picklist.getItems();
			if (items != null)
			{
				String[] asMonikers = new String[items.length];
				String[] asText = new String[items.length];
				String[] asPostcodes = new String[items.length];
				String[] asCommands = new String[items.length];
				for (int i=0; i < items.length; i++)
				{
					asMonikers[i] = items[i].getMoniker();
					asText[i] = items[i].getText();
					asPostcodes[i] = items[i].getPostcode();
					if (mustRefine(items[i]))
					{
						asCommands[i] = VerifyRefineAddress.NAME;
					}
					else
					{
						asCommands[i] = VerifyFormatAddress.NAME;
					}
				}
				m_Request.setAttribute(Constants.PICK_MONIKERS, asMonikers);
				m_Request.setAttribute(Constants.PICK_TEXTS, asText);
				m_Request.setAttribute(Constants.PICK_POSTCODES, asPostcodes);
				m_Request.setAttribute(Constants.PICK_FUNCTIONS, asCommands);
			}
			else
			{
				// no items  - no matches
                                String[] asLabels = new String[m_asUserInput.length];
                                        
				for (int i=0; i < m_asUserInput.length; i++)
                                {
                                     switch(i)
                                    {
                                        case 0:
                                            asLabels[i]="";
                                            break;
                                        case 1:
                                            asLabels[i]="";
                                        case 2:
                                            asLabels[i]="";
                                            break;
                                        case 3:
                                            asLabels[i]="Town";
                                            break;
                                        case 4:
                                            asLabels[i]="County";
                                            break;
                                        case 5:
                                            asLabels[i]="Postcode";
                                            break;
                                    }
                                }
				m_Request.setAttribute(Constants.ADDRESS, m_asUserInput);
                                m_Request.setAttribute(Constants.LABELS,asLabels);
			}
		}
		m_Request.setAttribute(Constants.VERIFY_INFO, sVerifyLevel);
		return "/verifyInteraction.jsp";
	}

	/** Accept originally entered address, when the verification level is too low **/ 
	private String noInteraction(String sVerifyLevel)
	{
		if (sVerifyLevel.equals(""))
		{
			sVerifyLevel = "address accepted unchanged";
		}
		else
		{
			sVerifyLevel = "address verification level was " + sVerifyLevel;
		}
		String sVerify = sVerifyLevel + ", so the entered address has been used";

		m_Request.setAttribute(Constants.VERIFY_INFO, sVerify);
		m_Request.setAttribute(Constants.ADDRESS, m_asUserInput);

		return "/" + Constants.FINAL_ADDRESS_PAGE;
	}

	/** Must the picklist item be refined (text added) to form a final address? **/
	protected boolean mustRefine(PicklistItem item)
	{
		return ( item.isIncompleteAddress() || item.isUnresolvableRange() || item.isPhantomPrimaryPoint() );
	}

        
     
}
