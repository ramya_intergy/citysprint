/**
 * ProWebLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class ProWebLocator extends org.apache.axis.client.Service implements com.qas.proweb.soap.ProWeb {

    // Use to get a proxy class for QAPortType
    private final java.lang.String QAPortType_address = "http://Nigelt:2022/";

    public java.lang.String getQAPortTypeAddress() {
        return QAPortType_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String QAPortTypeWSDDServiceName = "QAPortType";

    public java.lang.String getQAPortTypeWSDDServiceName() {
        return QAPortTypeWSDDServiceName;
    }

    public void setQAPortTypeWSDDServiceName(java.lang.String name) {
        QAPortTypeWSDDServiceName = name;
    }

    public com.qas.proweb.soap.QAPortType getQAPortType() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(QAPortType_address);
        }
        catch (java.net.MalformedURLException e) {
            return null; // unlikely as URL was validated in WSDL2Java
        }
        return getQAPortType(endpoint);
    }

    public com.qas.proweb.soap.QAPortType getQAPortType(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.qas.proweb.soap.QASoapBindingStub _stub = new com.qas.proweb.soap.QASoapBindingStub(portAddress, this);
            _stub.setPortName(getQAPortTypeWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.qas.proweb.soap.QAPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.qas.proweb.soap.QASoapBindingStub _stub = new com.qas.proweb.soap.QASoapBindingStub(new java.net.URL(QAPortType_address), this);
                _stub.setPortName(getQAPortTypeWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        java.rmi.Remote _stub = getPort(serviceEndpointInterface);
        ((org.apache.axis.client.Stub) _stub).setPortName(portName);
        return _stub;
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "ProWeb");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("QAPortType"));
        }
        return ports.iterator();
    }

}
