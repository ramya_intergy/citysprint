/**
 * QAGetAddress.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class QAGetAddress  implements java.io.Serializable {
    private java.lang.String layout;
    private java.lang.String moniker;
    private com.qas.proweb.soap.QAConfigType QAConfig;

    public QAGetAddress() {
    }

    public java.lang.String getLayout() {
        return layout;
    }

    public void setLayout(java.lang.String layout) {
        this.layout = layout;
    }

    public java.lang.String getMoniker() {
        return moniker;
    }

    public void setMoniker(java.lang.String moniker) {
        this.moniker = moniker;
    }

    public com.qas.proweb.soap.QAConfigType getQAConfig() {
        return QAConfig;
    }

    public void setQAConfig(com.qas.proweb.soap.QAConfigType QAConfig) {
        this.QAConfig = QAConfig;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QAGetAddress)) return false;
        QAGetAddress other = (QAGetAddress) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((layout==null && other.getLayout()==null) || 
             (layout!=null &&
              layout.equals(other.getLayout()))) &&
            ((moniker==null && other.getMoniker()==null) || 
             (moniker!=null &&
              moniker.equals(other.getMoniker()))) &&
            ((QAConfig==null && other.getQAConfig()==null) || 
             (QAConfig!=null &&
              QAConfig.equals(other.getQAConfig())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLayout() != null) {
            _hashCode += getLayout().hashCode();
        }
        if (getMoniker() != null) {
            _hashCode += getMoniker().hashCode();
        }
        if (getQAConfig() != null) {
            _hashCode += getQAConfig().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QAGetAddress.class);

    static {
        org.apache.axis.description.FieldDesc field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("layout");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Layout"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("moniker");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Moniker"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("QAConfig");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAConfig"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAConfigType"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
