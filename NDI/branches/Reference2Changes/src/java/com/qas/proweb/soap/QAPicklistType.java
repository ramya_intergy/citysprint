/**
 * QAPicklistType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class QAPicklistType  implements java.io.Serializable {
    private java.lang.String fullPicklistMoniker;
    private com.qas.proweb.soap.PicklistEntryType[] picklistEntry;
    private java.lang.String prompt;
    private org.apache.axis.types.NonNegativeInteger total;
    private boolean autoFormatSafe;  // attribute
    private boolean autoFormatPastClose;  // attribute
    private boolean autoStepinSafe;  // attribute
    private boolean autoStepinPastClose;  // attribute
    private boolean largePotential;  // attribute
    private boolean maxMatches;  // attribute
    private boolean moreOtherMatches;  // attribute
    private boolean overThreshold;  // attribute
    private boolean timeout;  // attribute

    public QAPicklistType() {
    }

    public java.lang.String getFullPicklistMoniker() {
        return fullPicklistMoniker;
    }

    public void setFullPicklistMoniker(java.lang.String fullPicklistMoniker) {
        this.fullPicklistMoniker = fullPicklistMoniker;
    }

    public com.qas.proweb.soap.PicklistEntryType[] getPicklistEntry() {
        return picklistEntry;
    }

    public void setPicklistEntry(com.qas.proweb.soap.PicklistEntryType[] picklistEntry) {
        this.picklistEntry = picklistEntry;
    }

    public com.qas.proweb.soap.PicklistEntryType getPicklistEntry(int i) {
        return picklistEntry[i];
    }

    public void setPicklistEntry(int i, com.qas.proweb.soap.PicklistEntryType value) {
        this.picklistEntry[i] = value;
    }

    public java.lang.String getPrompt() {
        return prompt;
    }

    public void setPrompt(java.lang.String prompt) {
        this.prompt = prompt;
    }

    public org.apache.axis.types.NonNegativeInteger getTotal() {
        return total;
    }

    public void setTotal(org.apache.axis.types.NonNegativeInteger total) {
        this.total = total;
    }

    public boolean isAutoFormatSafe() {
        return autoFormatSafe;
    }

    public void setAutoFormatSafe(boolean autoFormatSafe) {
        this.autoFormatSafe = autoFormatSafe;
    }

    public boolean isAutoFormatPastClose() {
        return autoFormatPastClose;
    }

    public void setAutoFormatPastClose(boolean autoFormatPastClose) {
        this.autoFormatPastClose = autoFormatPastClose;
    }

    public boolean isAutoStepinSafe() {
        return autoStepinSafe;
    }

    public void setAutoStepinSafe(boolean autoStepinSafe) {
        this.autoStepinSafe = autoStepinSafe;
    }

    public boolean isAutoStepinPastClose() {
        return autoStepinPastClose;
    }

    public void setAutoStepinPastClose(boolean autoStepinPastClose) {
        this.autoStepinPastClose = autoStepinPastClose;
    }

    public boolean isLargePotential() {
        return largePotential;
    }

    public void setLargePotential(boolean largePotential) {
        this.largePotential = largePotential;
    }

    public boolean isMaxMatches() {
        return maxMatches;
    }

    public void setMaxMatches(boolean maxMatches) {
          this.maxMatches = maxMatches;
    }

    public boolean isMoreOtherMatches() {
        return moreOtherMatches;
    }

    public void setMoreOtherMatches(boolean moreOtherMatches) {
        this.moreOtherMatches = moreOtherMatches;
    }

    public boolean isOverThreshold() {
        return overThreshold;
    }

    public void setOverThreshold(boolean overThreshold) {
        this.overThreshold = overThreshold;
    }

    public boolean isTimeout() {
        return timeout;
    }

    public void setTimeout(boolean timeout) {
        this.timeout = timeout;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QAPicklistType)) return false;
        QAPicklistType other = (QAPicklistType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((fullPicklistMoniker==null && other.getFullPicklistMoniker()==null) || 
             (fullPicklistMoniker!=null &&
              fullPicklistMoniker.equals(other.getFullPicklistMoniker()))) &&
            ((picklistEntry==null && other.getPicklistEntry()==null) || 
             (picklistEntry!=null &&
              java.util.Arrays.equals(picklistEntry, other.getPicklistEntry()))) &&
            ((prompt==null && other.getPrompt()==null) || 
             (prompt!=null &&
              prompt.equals(other.getPrompt()))) &&
            ((total==null && other.getTotal()==null) || 
             (total!=null &&
              total.equals(other.getTotal()))) &&
            autoFormatSafe == other.isAutoFormatSafe() &&
            autoFormatPastClose == other.isAutoFormatPastClose() &&
            autoStepinSafe == other.isAutoStepinSafe() &&
            autoStepinPastClose == other.isAutoStepinPastClose() &&
            largePotential == other.isLargePotential() &&
            maxMatches == other.isMaxMatches() &&
            moreOtherMatches == other.isMoreOtherMatches() &&
            overThreshold == other.isOverThreshold() &&
            timeout == other.isTimeout();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFullPicklistMoniker() != null) {
            _hashCode += getFullPicklistMoniker().hashCode();
        }
        if (getPicklistEntry() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPicklistEntry());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPicklistEntry(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPrompt() != null) {
            _hashCode += getPrompt().hashCode();
        }
        if (getTotal() != null) {
            _hashCode += getTotal().hashCode();
        }
        _hashCode += new Boolean(isAutoFormatSafe()).hashCode();
        _hashCode += new Boolean(isAutoFormatPastClose()).hashCode();
        _hashCode += new Boolean(isAutoStepinSafe()).hashCode();
        _hashCode += new Boolean(isAutoStepinPastClose()).hashCode();
        _hashCode += new Boolean(isLargePotential()).hashCode();
        _hashCode += new Boolean(isMaxMatches()).hashCode();
        _hashCode += new Boolean(isMoreOtherMatches()).hashCode();
        _hashCode += new Boolean(isOverThreshold()).hashCode();
        _hashCode += new Boolean(isTimeout()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QAPicklistType.class);

    static {
        org.apache.axis.description.FieldDesc field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("autoFormatSafe");
        field.setXmlName(new javax.xml.namespace.QName("", "AutoFormatSafe"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("autoFormatPastClose");
        field.setXmlName(new javax.xml.namespace.QName("", "AutoFormatPastClose"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("autoStepinSafe");
        field.setXmlName(new javax.xml.namespace.QName("", "AutoStepinSafe"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("autoStepinPastClose");
        field.setXmlName(new javax.xml.namespace.QName("", "AutoStepinPastClose"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("largePotential");
        field.setXmlName(new javax.xml.namespace.QName("", "LargePotential"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("maxMatches");
        field.setXmlName(new javax.xml.namespace.QName("", "MaxMatches"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("moreOtherMatches");
        field.setXmlName(new javax.xml.namespace.QName("", "MoreOtherMatches"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("overThreshold");
        field.setXmlName(new javax.xml.namespace.QName("", "OverThreshold"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("timeout");
        field.setXmlName(new javax.xml.namespace.QName("", "Timeout"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("fullPicklistMoniker");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "FullPicklistMoniker"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("picklistEntry");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "PicklistEntry"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("prompt");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Prompt"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("total");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Total"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "nonNegativeInteger"));
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
