/**
 * QASearchResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class QASearchResult  implements java.io.Serializable {
    private com.qas.proweb.soap.QAPicklistType QAPicklist;
    private com.qas.proweb.soap.QAAddressType QAAddress;
    private com.qas.proweb.soap.VerifyLevelType verifyLevel;  // attribute

    public QASearchResult() {
    }

    public com.qas.proweb.soap.QAPicklistType getQAPicklist() {
        return QAPicklist;
    }

    public void setQAPicklist(com.qas.proweb.soap.QAPicklistType QAPicklist) {
        this.QAPicklist = QAPicklist;
    }

    public com.qas.proweb.soap.QAAddressType getQAAddress() {
        return QAAddress;
    }

    public void setQAAddress(com.qas.proweb.soap.QAAddressType QAAddress) {
        this.QAAddress = QAAddress;
    }

    public com.qas.proweb.soap.VerifyLevelType getVerifyLevel() {
        return verifyLevel;
    }

    public void setVerifyLevel(com.qas.proweb.soap.VerifyLevelType verifyLevel) {
        this.verifyLevel = verifyLevel;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QASearchResult)) return false;
        QASearchResult other = (QASearchResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((QAPicklist==null && other.getQAPicklist()==null) || 
             (QAPicklist!=null &&
              QAPicklist.equals(other.getQAPicklist()))) &&
            ((QAAddress==null && other.getQAAddress()==null) || 
             (QAAddress!=null &&
              QAAddress.equals(other.getQAAddress()))) &&
            ((verifyLevel==null && other.getVerifyLevel()==null) || 
             (verifyLevel!=null &&
              verifyLevel.equals(other.getVerifyLevel())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getQAPicklist() != null) {
            _hashCode += getQAPicklist().hashCode();
        }
        if (getQAAddress() != null) {
            _hashCode += getQAAddress().hashCode();
        }
        if (getVerifyLevel() != null) {
            _hashCode += getVerifyLevel().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QASearchResult.class);

    static {
        org.apache.axis.description.FieldDesc field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("verifyLevel");
        field.setXmlName(new javax.xml.namespace.QName("", "VerifyLevel"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "VerifyLevelType"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("QAPicklist");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAPicklist"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAPicklistType"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("QAAddress");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAAddress"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAAddressType"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
