<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.io.*" %>

<%@ page import="org.apache.commons.fileupload.*,
org.apache.commons.fileupload.servlet.ServletFileUpload, 
org.apache.commons.fileupload.disk.DiskFileItemFactory, 
org.apache.commons.io.FilenameUtils, java.util.*, java.io.File, java.lang.Exception" %>
<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
      <% //response.setContentType("application/vnd.wap.xhtml+xml"); %>
  
    <%
       if(ServletFileUpload.isMultipartContent(request)){
           
           ServletFileUpload servletFileUpload  = new ServletFileUpload(new DiskFileItemFactory());
           List fileItemsList = servletFileUpload .parseRequest(request);
           
           String optionalFileName = "";
            FileItem fileItem = null;

            Iterator it = fileItemsList.iterator();
             while (it.hasNext()){
                     FileItem fileItemTemp = (FileItem)it.next();
             if (fileItemTemp.isFormField()){

           
           
           
      // }
    
    %>
        
      <%-- <b>Name-value Pair Info:</b><br/>
        Field name: <%= fileItemTemp.getFieldName() %><br/>
        Field value: <%= fileItemTemp.getString() %><br/><br/>--%>
 
    <%
      if (fileItemTemp.getFieldName().equals("filename"))
        optionalFileName = fileItemTemp.getString();
    }
    else
      fileItem = fileItemTemp;
  }

  if (fileItem!=null){
    String fileName = fileItem.getName();
%>

 <%--<b>Uploaded File Info:</b><br/>
Content type: <%= fileItem.getContentType() %><br/>
Field name: <%= fileItem.getFieldName() %><br/>
File name: <%= fileName %><br/>
File size: <%= fileItem.getSize() %><br/><br/>--%>


<%
    /* Save the uploaded file if its size is greater than 0. */
    if (fileItem.getSize() > 0){
      if (optionalFileName.trim().equals(""))
       fileName = FilenameUtils.getName(fileName);
        
      else
       fileName = optionalFileName;
        
      //String dirName = "/file_uploads/";
      String localDirName;
           
      localDirName=getServletContext( ).getRealPath( "" )+ "/WEB-INF/classes";
        
     ResourceBundle bdl =  new PropertyResourceBundle(new FileInputStream(localDirName + "/Config.properties"));
    
      //String dirName = "C:\\\\NDI";
      //File saveTo = new File(dirName + fileName);
      File saveTo = new File(localDirName + fileName);
      try {
        fileItem.write(saveTo);
      
        
%>
   
  <b>The uploaded file has been saved successfully.</b>
<%
      }
      catch (Exception e){
%>
<b>An error occurred when we tried to save the uploaded file.</b>

<%
      }
    }
  }
}
%>

 
    
    </body>
</html>
