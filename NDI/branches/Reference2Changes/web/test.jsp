<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>City Sprint - Address Book</title>
    <link media="screen" href="CSS/NextDayCss.css" type="text/css" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body style="width:75px;height:75px;">
<br>
<label id="lblMessage" style="color:green;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px;"></label><br>
<br>
<table id=entryTable style="display:none;border:1px solid #666666;padding:5px 5px 5px 5px;background-color:#EFEFEF;" cellpadding=2 width="95%" align="center">
    <tr><br>
        <td id="boxtitle" colspan="7">NEW ADDRESS BOOK ENTRY</td>
    </tr>
    <tr>
    <td id="label">Name *</td><td>
        <input type="text" name=txtCompany size="50" value=''>
    </td>
    </tr>
    <tr>
         <td id="label">Address1 *</td><td>
        <input type="text" name=txtAddress1 size="50" value=''>
    </td>
    </tr>
    <tr>
        <td id="label">Address2</td><td>
        <input type="text" name=txtAddress2 size="50" value=''>
        </td>
    </tr>
    <tr>
        <td id="label">Town *</td><td>
        <input type="text" name=txtTown size="40" value=''>
        </td>
    </tr>
    <tr>
        <td id="label">County *</td><td>
         <input type="text" name=txtCounty size="40" value=''>
        </td>
    </tr>
    <tr>
        <td id="label">Postcode *</td><td>
        <input type="text" name=txtPostcode size="20" value=''>
        </td>
    </tr>
    <tr>
        <td id="label">Country</td><td>
         UK
        </td>
    </tr>
    <tr>
        <td id="label">Contact *</td><td>
        <input type="text" name=txtContact size="50" value=''>
        </td>
    </tr>
    <tr>
        <td id="label">Phone</td><td>
        <input type="text" name=txtPhone size="20" value=>
        </td>
    </tr>
    <tr>
        <td id="label">Instruct</td><td>
        <input type="text" name=txtInstruct size="50" value=>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
        <input type="checkbox" name="chkCollection" checked>Collection &nbsp;&nbsp;
        <input type="checkbox" name="chkDelivery" >Delivery &nbsp;&nbsp;
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
        <input type="button" name=btnEntry value="Validate Address" id="btn" onclick="javascript:addAddress();">
        &nbsp;&nbsp;&nbsp;&nbsp;
        <input type="button" name="btnCancel" value="Cancel" id="btn" onclick="javascript:cancel_click();">
        </td>
    </tr>
</table>
<br>
<table border="0" cellpadding="2" cellspacing="0" id="box" width="95%" align="center">
    <tr><td id="boxtitle" colspan="2">SEARCH ADDRESS BOOK</td></tr>
<tr>
    <td id= style="text-align:left;">Company</td>
    <td><input type="text" size="40" name="txtSrchCompany">&nbsp;<input type="button" id="btn" value="Search" onclick="javascript:Search();">&nbsp;
        <input type="button" id="btn" value="Add" onclick="javascript:showEntryForm();"></td>
</tr>
<tr>    <td id="boxtitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Company</td><td id="boxtitle">Postcode</td></tr>
<tr id=odd>
    <td ><a href="AddressBook?sAction=Edit&sType=C&sId=1" id="smallbtn">Edit</a>&nbsp;
    <a href="AddAddress?sAction=Delete&sType=C&sId=1" id="smallbtn">Delete</a>&nbsp;
    <a href="AddAddress?sAction=display&sType=C&id=1">Intergy</a>&nbsp;</td><td >
    2065
    </td>
</tr>
<tr id=even>
<td ><a href="AddressBook?sAction=Edit&sType=C&sId=2" id="smallbtn">Edit</a>&nbsp;
<a href="AddAddress?sAction=Delete&sType=C&sId=2" id="smallbtn">Delete</a>&nbsp;
<a href="AddAddress?sAction=display&sType=C&id=2">Dancrai</a>&nbsp;</td><td >
2065
</td></tr>
<tr id=odd>
<td ><a href="AddressBook?sAction=Edit&sType=C&sId=3" id="smallbtn">Edit</a>&nbsp;
<a href="AddAddress?sAction=Delete&sType=C&sId=3" id="smallbtn">Delete</a>&nbsp;
<a href="AddAddress?sAction=display&sType=C&id=3">Intech</a>&nbsp;</td><td >
2100
</td></tr>
<tr id=even>
<td ><a href="AddressBook?sAction=Edit&sType=C&sId=5" id="smallbtn">Edit</a>&nbsp;
<a href="AddAddress?sAction=Delete&sType=C&sId=5" id="smallbtn">Delete</a>&nbsp;
<a href="AddAddress?sAction=display&sType=C&id=5">test</a>&nbsp;</td><td >
2156
</td></tr>
<tr id=odd>
<td ><a href="AddressBook?sAction=Edit&sType=C&sId=6" id="smallbtn">Edit</a>&nbsp;
<a href="AddAddress?sAction=Delete&sType=C&sId=6" id="smallbtn">Delete</a>&nbsp;
<a href="AddAddress?sAction=display&sType=C&id=6">ramya</a>&nbsp;</td><td >
3424
</td></tr>
<tr id=even>
<td ><a href="AddressBook?sAction=Edit&sType=C&sId=7" id="smallbtn">Edit</a>&nbsp;
<a href="AddAddress?sAction=Delete&sType=C&sId=7" id="smallbtn">Delete</a>&nbsp;
<a href="AddAddress?sAction=display&sType=C&id=7">test2</a>&nbsp;</td><td >
2145
</td></tr>
<tr id=odd>
<td ><a href="AddressBook?sAction=Edit&sType=C&sId=8" id="smallbtn">Edit</a>&nbsp;
<a href="AddAddress?sAction=Delete&sType=C&sId=8" id="smallbtn">Delete</a>&nbsp;
<a href="AddAddress?sAction=display&sType=C&id=8">Townshend Landscape Architects</a>&nbsp;</td><td >
EC2A 4HJ
</td></tr>
<tr id=even>
<td ><a href="AddressBook?sAction=Edit&sType=C&sId=9" id="smallbtn">Edit</a>&nbsp;
<a href="AddAddress?sAction=Delete&sType=C&sId=9" id="smallbtn">Delete</a>&nbsp;
<a href="AddAddress?sAction=display&sType=C&id=9">Charter & Law Solicitors</a>&nbsp;</td><td >
EC2A 4AA
</td></tr>
</table>
<br><br>
<center><a href="javascript:window.close();">Close</a></center>
</body>

<script language=javascript>
function showEntryForm()
{
    var table = document.getElementById('entryTable');
    alert(table);
    if(table.style.display == 'none')
    {
    table.style.display = 'block';
    }
    else
    {
    table.style.display = 'none';
    }
}

function cancel_click()
{
    var lblMessage=document.getElementById("lblMessage");
    var btnCancel=document.getElementById("btnCancel");
    lblMessage.innerText="";
    if(btnCancel.value=="Cancel")
    {
        var table = document.getElementById('entryTable');
        table.style.display = 'none';
    }
    else
    {
        var table = document.getElementById('entryTable');
        table.style.display = 'block';
        var txtCompany=document.getElementById("txtCompany");
        var txtContact=document.getElementById("txtContact");
        var txtAddress1=document.getElementById("txtAddress1");
        var txtAddress2=document.getElementById("txtAddress2");
        var txtPhone=document.getElementById("txtPhone");
        var txtTown=document.getElementById("txtTown");
        var txtCounty=document.getElementById("txtCounty");
        var txtPostcode=document.getElementById("txtPostcode");
        var chkCollection=document.getElementById("chkCollection");
        var chkDelivery=document.getElementById("chkDelivery");
        var txtInstruct=document.getElementById("txtInstruct");
        var btnEntry=document.getElementById("btnEntry");
        var picklistTable=document.getElementById("picklistTable");
        txtCompany.disabled=false;
        txtCompany.value="";
        txtContact.disabled=false;
        txtContact.value="";
        txtAddress1.disabled=false;
        txtAddress1.value="";
        txtAddress2.disabled=false;
        txtAddress2.value="";
        txtPhone.disabled=false;
        txtPhone.value="";
        txtTown.disabled=false;
        txtTown.value="";
        txtCounty.disabled=false;
        txtCounty.value="";
        txtPostcode.disabled=false;
        txtPostcode.value="";
        txtInstruct.disabled=false;
        txtInstruct.value="";
        chkCollection.disabled=false;
        chkDelivery.disabled=false;
        btnEntry.value="Validate Address";
        btnCancel.value="Cancel";
        picklistTable.style.display='none';
    }
}
function addAddress()
{
    var sMsg1="";
    var sMsg2="";
    var error=false;
    var txtCompany=document.getElementById("txtCompany");
    var txtContact=document.getElementById("txtContact");
    var txtAddress1=document.getElementById("txtAddress1");
    var txtAddress2=document.getElementById("txtAddress2");
    var txtPhone=document.getElementById("txtPhone");
    var txtTown=document.getElementById("txtTown");
    var txtCounty=document.getElementById("txtCounty");
    var txtPostcode=document.getElementById("txtPostcode");
    var chkCollection=document.getElementById("chkCollection");
    var chkDelivery=document.getElementById("chkDelivery");
    var txtInstruct=document.getElementById("txtInstruct");
    var btnEntry=document.getElementById("btnEntry");
    var sCompany=txtCompany.value;
    var sContact=txtContact.value;
    var sAddress1=txtAddress1.value;
    var sAddress2=txtAddress2.value;
    var sPhone=txtPhone.value;
    var sTown=txtTown.value;
    var sCounty=txtCounty.value
    alert("after county value");
    var sPostcode=txtPostcode.value;
    var sCountry="UK";
    var sInstruct=txtInstruct.value;
    alert("after value");
    if(sCompany=="" || sContact=="" || sPostcode=="" || sTown=="")
    {
    sMsg2="\nYou must enter data in the mandatory fields.";
    error=true;
    }
    alert("before if");
    if(sCompany.indexOf("'")!=-1 || sContact.indexOf("'")!=-1 || sPhone.indexOf("'")!=-1 || sAddress1.indexOf("'")!=-1 || sAddress2.indexOf("'")!=-1 || sTown.indexOf("'")!=-1 || sPostcode.indexOf("'")!=-1 || sCounty.indexOf("'")!=-1 || sInstruct.indexOf("'")!=-1)
    {
    sMsg1=sMsg1+"\nInvalid characters found in the provided data";
    error=true;
    }
    alert("after if");
    if(error==false)
    {
    alert(btnEntry.value);
        if(btnEntry.value=="Validate Address")
        {
            alert(sCompany);
            document.VerifyForm.UserInput.value=sCompany +"^" + sAddress1+"^"+sAddress2+"^"+sTown+ "^"+sCounty+"^"+sPostcode
            document.VerifyForm.sPhone.value=sPhone
            alert(sCompany);
            document.VerifyForm.sInstruct.value=sInstruct
            if(chkCollection.checked && chkDelivery.checked)
            document.VerifyForm.sType.value="CD"
            if(chkCollection.checked && !chkDelivery.checked)
            document.VerifyForm.sType.value="C"
            if(!chkCollection.checked && chkDelivery.checked)
            document.VerifyForm.sType.value="D"
            alert(sCompany);
            document.VerifyForm.sContact.value=sContact
            alert(sCompany);
            document.VerifyForm.sId.value=;
            document.VerifyForm.submit();
        }
        else
        {
            document.AddAddressForm.sCompanyName.value=sCompany
            document.AddAddressForm.sContactName.value=sContact
            document.AddAddressForm.sAddress1.value=sAddress1
            document.AddAddressForm.sAddress2.value=sAddress2
            document.AddAddressForm.sPhone.value=sPhone
            document.AddAddressForm.sTown.value=sTown
            document.AddAddressForm.sCounty.value=sCounty
            document.AddAddressForm.sPostcode.value=sPostcode
            document.AddAddressForm.sCountry.value=sCountry
            document.AddAddressForm.sInstruct.value=sInstruct
            if(chkCollection.checked && chkDelivery.checked)
            document.AddAddressForm.sType.value="CD"
            if(chkCollection.checked && !chkDelivery.checked)
            document.AddAddressForm.sType.value="C"
            if(!chkCollection.checked && chkDelivery.checked)
            document.AddAddressForm.sType.value="D"
            document.AddAddressForm.sId.value=;
            document.AddAddressForm.sAction='Add';
            document.AddAddressForm.submit();
        }
    }
    else
    alert(sMsg1+sMsg2);
}
function Search()
{
    var sCompany=document.getElementById("txtSrchCompany");
    document.SearchForm.sCompanyName.value=sCompany.value;
    document.SearchForm.sType.value='C'
    document.SearchForm.submit();
}
function QASlist_Click(asText,asPostcode,asMonikers)
{
    alert('QASlist');
    document.PickListForm.PickTexts.value=asText;
    document.PickListForm.PickPostcodes.value=asPostcode;
    document.PickListForm.Moniker.value=asMonikers;
    document.PickListForm.sId.value=;
    document.PickListForm.submit();
}
</script>
<form name="AddAddressForm" method="POST" action="AddAddress">
<input type=hidden name="sCompanyName" value="">
<input type=hidden name="sContactName" value="">
<input type=hidden name="sAddress1" value="">
<input type=hidden name="sAddress2" value="">
<input type=hidden name="sPhone" value="">
<input type=hidden name="sTown" value="">
<input type=hidden name="sCounty" value="">
<input type=hidden name="sPostcode" value="">
<input type=hidden name="sCountry" value="">
<input type=hidden name="sInstruct" value="">
<input type=hidden name="sType" value="">
<input type=hidden name="sAction" value="">
<input type=hidden name="sId" value="">
</form>
<form name="VerifyForm" method="POST" action="QasController">
<input type=hidden name="Command" value="Verify">
<input type=hidden name="UserInput" value="">
<input type=hidden name="CountryName" value="United Kingdom">
<input type=hidden name="DataId" value="GBR">
<input type=hidden name="sAction" value="QAS">
<input type=hidden name="sInstruct" value="">
<input type=hidden name="sType" value="">
<input type=hidden name="sPhone" value="">
<input type=hidden name="sContact" value="">
<input type=hidden name="sId" value="">
</form>
<form name="PickListForm" method="POST" action="QasController">
<input type=hidden name="sAction" value="QAS">
<input type=hidden name="sInstruct" value="">
<input type=hidden name="sType" value="C">
<input type=hidden name="sPhone" value="">
<input type=hidden name="sContact" value="">
<input type=hidden name=PickTexts value="">
<input type=hidden name=PickPostcodes value="">
<input type=hidden name="sAction_sub" value="ShowAddress">
<input type=hidden name="AddressInfo" value="InteractionRequired">
<input type=hidden name="Command" value="VerifyFormatAddress">
<input type=hidden name="Moniker" value="">
<input type=hidden name="sId" value="">
</form>
<form name="SearchForm" method="POST" action="AddressBook">
<input type=hidden name="sCompanyName" value="">
<input type=hidden name="sAction" value="ShowAddress">
<input type=hidden name="sType" value="">
</form>
</html>

