<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
       
        <title>JSP Page</title>
         <link rel="stylesheet" type="text/css" media="all" href="calendar-blue.css" title="win2k-cold-1" />
    <script type="text/javascript" src="calendar-en.js"></script>
    <script type="text/javascript"  src="calendar-setup.js"></script>
    <script type="text/javascript" src="calendar.js"></script>
    
    </head>
    <body>

    <h1>JSP Page</h1>
    
     <table>
        <tr><td>
              <img src="images/calender1.GIF" id="f_trigger_c" title="Date selector"/>
              
              <script type="text/javascript">
            Calendar.setup({
            inputField     :    "txtCollectDate",     
            ifFormat       :    "%d-%m-%Y",      
            button         :    "f_trigger_c",  
            align          :    "Tl",           
            singleClick    :    true
            });
           </script>
    </td></tr></table>
    
    </body>
</html>
