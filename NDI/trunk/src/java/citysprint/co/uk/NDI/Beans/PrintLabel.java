/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citysprint.co.uk.NDI.Beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ramyas
 */
public class PrintLabel extends HttpServlet
{

     /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException,SQLException  {


        try {
            File f = new File("C:\\backups\\W99969_2008_08_14_19_22_17.pdf");

            OutputStream oos = response.getOutputStream();
            response.setContentType("application/pdf");
            response.setContentLength((int) f.length());
            response.setHeader("Content-Disposition", "attachment;filename=W99969_2008_08_14_19_22_17.pdf");
            byte[] buf = new byte[8192];

            InputStream is = new FileInputStream(f);

            int c = 0;

            while ((c = is.read(buf, 0, buf.length)) > 0) {
                oos.write(buf, 0, c);
                oos.flush();
            }

            oos.close();
            System.out.println("stop");
            is.close();
        } catch (FileNotFoundException ex) {
        }
    }
}
