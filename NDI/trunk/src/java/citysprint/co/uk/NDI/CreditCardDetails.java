package citysprint.co.uk.NDI;

/*
 * CreditCardDetails.java
 *
 * Created on 27 March 2008, 16:25
 */

import java.beans.*;
import java.io.Serializable;
import javax.servlet.http.*;
/**
 * @author ramyas
 */
public class CreditCardDetails extends Object implements Serializable {
    
    /**
     *
     */
    public static final String PROP_SAMPLE_PROPERTY = "sampleProperty";
    
    private String sCCType="",sCardNo="",sCCName="",sServices="",sExpiryMonth="",sExpiryYear="",sStartDay="",sStartMonth="",sIssueNo="",sCCEmail="";
    private String sSecurityCode="",sPostcode="",sTrackingEmail="",sTrackingSMS="",sEmailNotify="",sSMSNotify="";
    private String sAVSAddress1="", sAVSAddress2="", sAVSAddress3="", sAVSAddress4="";

    public String getsAVSAddress1() {
        return sAVSAddress1;
    }

    public void setsAVSAddress1(String sAVSAddress1) {
        this.sAVSAddress1 = sAVSAddress1;
    }

    public String getsAVSAddress2() {
        return sAVSAddress2;
    }

    public void setsAVSAddress2(String sAVSAddress2) {
        this.sAVSAddress2 = sAVSAddress2;
    }

    public String getsAVSAddress3() {
        return sAVSAddress3;
    }

    public void setsAVSAddress3(String sAVSAddress3) {
        this.sAVSAddress3 = sAVSAddress3;
    }

    public String getsAVSAddress4() {
        return sAVSAddress4;
    }

    public void setsAVSAddress4(String sAVSAddress4) {
        this.sAVSAddress4 = sAVSAddress4;
    }
    private PropertyChangeSupport propertySupport;
    
    /**
     *
     */
    public CreditCardDetails() {
        propertySupport = new PropertyChangeSupport(this);
    }
    
     //Credit Card Type
    /**
     *
     * @return
     */
    public String getCCType() {
         return sCCType;
    }
    
     //Credit Card Number
    /**
     *
     * @return
     */
    public String getCardNo() {
         return sCardNo;
    }
     //Name on credit Card
    /**
     *
     * @return
     */
    public String getCCName() {
         return sCCName;
    }
     //Service Option
    /**
     *
     * @return
     */
    public String getServices() {
         return sServices;
    }
     //Expiry Month
    /**
     *
     * @return
     */
    public String getExpiryMonth() {
         return sExpiryMonth;
    }
     //ExpiryYear
    /**
     *
     * @return
     */
    public String getExpiryYear() {
         return sExpiryYear;
    }
     //StartDay
    /**
     *
     * @return
     */
    public String getStartDay() {
         return sStartDay;
    }
     //StartMonth
    /**
     *
     * @return
     */
    public String getStartMonth() {
         return sStartMonth;
    }
     //Issue Number
    /**
     *
     * @return
     */
    public String getIssueNo() {
         return sIssueNo;
    }
     //Credit Card Email
    /**
     *
     * @return
     */
    public String getCCEmail() {
         return sCCEmail;
    }
     //SecurityCode
    /**
     *
     * @return
     */
    public String getSecurityCode() {
         return sSecurityCode;
    }
     //Postcode
    /**
     *
     * @return
     */
    public String getPostcode() {
         return sPostcode;
    }
     //TrackingEmail
    /**
     *
     * @return
     */
    public String getTrackingEmail() {
         return sTrackingEmail;
    }
     //TrackingSMS
    /**
     *
     * @return
     */
    public String getTrackingSMS() {
         return sTrackingSMS;
    }
    
    /**
     *
     * @return
     */
    public String getEmailNotify(){
        return sEmailNotify;
    }
    
    /**
     *
     * @return
     */
    public String getSMSNotify() {
        return sSMSNotify;
    }
    /**
     *
     * @param request
     */
    public void setCreditCardDetails(HttpServletRequest request)
    {
         sCCType=request.getParameter("CCType")==null?"":request.getParameter("CCType").toString();
         sCardNo=request.getParameter("CCNumber")==null?"":request.getParameter("CCNumber").toString();
         sCCName=request.getParameter("CCName")==null?"":request.getParameter("CCName").toString();
         sServices=request.getParameter("sServices")==null?"":request.getParameter("sServices").toString();
         sExpiryMonth=request.getParameter("sExpiryMonth")==null?"":request.getParameter("sExpiryMonth").toString();
         sExpiryYear=request.getParameter("sExpiryYear")==null?"":request.getParameter("sExpiryYear").toString();
         sStartDay=request.getParameter("sStartDay")==null?"":request.getParameter("sStartDay").toString();
         sStartMonth=request.getParameter("sStartMonth")==null?"":request.getParameter("sStartMonth").toString();
         sIssueNo=request.getParameter("sIssueNo")==null?"":request.getParameter("sIssueNo").toString();
         sCCEmail=request.getParameter("sCCEmail")==null?"":request.getParameter("sCCEmail").toString();
         sSecurityCode=request.getParameter("sSecurityCode")==null?"":request.getParameter("sSecurityCode").toString();
         sPostcode=request.getParameter("sPostcode")==null?"":request.getParameter("sPostcode").toString();
         sTrackingEmail=request.getParameter("sTrackingEmail")==null?"":request.getParameter("sTrackingEmail").toString();
         sTrackingSMS=request.getParameter("sTrackingSMS")==null?"":request.getParameter("sTrackingSMS").toString();
         sEmailNotify=request.getParameter("sEmailNotify")==null?"":request.getParameter("sEmailNotify").toString();
         sSMSNotify=request.getParameter("sSMSNotify")==null?"":request.getParameter("sSMSNotify").toString();
         sAVSAddress1=request.getParameter("sAVSAddress1")==null?"":request.getParameter("sAVSAddress1").toString();
         sAVSAddress2=request.getParameter("sAVSAddress2")==null?"":request.getParameter("sAVSAddress2").toString();
         sAVSAddress3=request.getParameter("sAVSAddress3")==null?"":request.getParameter("sAVSAddress3").toString();
         sAVSAddress4=request.getParameter("sAVSAddress4")==null?"":request.getParameter("sAVSAddress4").toString();
   }
  
}
