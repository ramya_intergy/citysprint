package citysprint.co.uk.NDI;
/*
 * ExportCSV.java
 *
 * Created on 20 June 2008, 11:25
 */

import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author vanir
 * @version
 */
public class ExportCSV extends CommonClass {

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    /**Create Object for Connection
     */
    Connection conCSV = null;
    /**Create Object for Statement
     */
    Statement stmtCSV = null;
    /**Create Object for ResultSet
     */
    ResultSet rstCSV = null;

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {

            if (isLogin(request)) {

            initializeConfigValues(request, response);

            //String CSVlocation="C:\\\\NDI\\\\";
            String CSVlocation = bdl.getString("CSVPath");
            //CSVlocation=CSVlocation.replace("/","//");


            DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
            java.util.Date date = new java.util.Date();
            String dateStr = dateFormat.format(date);

            String CSVloginname = request.getSession(false).getAttribute("acc_id").toString() + "_" + dateStr;
            /**CSV File Location
             */
            String CSVfilename = CSVloginname + ".csv";
            String CSVfilename1 = CSVlocation + CSVfilename;


            /**Get Parameter(SQLQuery,Include fields)
             */
            String csvSQL = request.getParameter("sqlCSV") == null ? "" : request.getParameter("sqlCSV").toString();
//            String selIncludeFields = request.getParameter("selInclude1") == null ? "" : request.getParameter("selInclude1").toString();
            String selIncludeFields = request.getParameter("sIncludeWithoutQuote") == null ? "" : request.getParameter("sIncludeWithoutQuote").toString();

            /**Convert from Database into CSV File Format
             */
//            String csvCopy = "( select " + selIncludeFields + ")" + "UNION" + "(" + csvSQL + " into outfile '" + CSVfilename1 + "' fields terminated by ',' optionally enclosed by '\"' Lines terminated by '\n')";
            String csvCopy = csvSQL;

            //Get a Database Connection

//           Class.forName("com.mysql.jdbc.Driver").newInstance();
//           conCSV  = DriverManager.getConnection("jdbc:mysql://localhost:3306/cs_ndi?user=root&password=admin");

            conCSV = cOpenConnection();
            stmtCSV = conCSV.createStatement();
            rstCSV = stmtCSV.executeQuery(csvCopy);

            this.createCSVFileFromRS(CSVfilename1, rstCSV, selIncludeFields);
            vCloseConnection(conCSV);

//            response.reset();
//
//            response.resetBuffer();
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            /* Set the headers.*/
            response.setContentType("application/x-download");
            response.setHeader("Content-Disposition", "attachment; location=" + CSVlocation + " filename=" + CSVfilename);
            response.setHeader("Content-Disposition", "inline; filename=" + CSVfilename);

            /** Send the file.
             */
            returnFile(CSVfilename1, out);
//            return;
            } else {
                writeOutputLog("Session expired redirecting to index page");
                response.sendRedirect("index.jsp");
            }
        } catch (Exception e) {
            vShowErrorPageReporting(response, e);
        }finally {
            if (stmtCSV != null){
                try{
                    stmtCSV.close();
                }catch(SQLException sqle){}
                
            }
            
            if (rstCSV != null){
                try{
                    rstCSV.close();
                }catch(SQLException sqle){}
                
            }
        }
    }

    
    private void createCSVFileFromRS(String fileName, ResultSet rs, String headerFields){
        BufferedWriter out = null;
        File file = new File(fileName);
        try {
            String duration = null;
            // Create file 
            FileWriter fstream = new FileWriter(file);
            out = new BufferedWriter(fstream);
            out.write(headerFields);
            ResultSetMetaData rsmt = rs.getMetaData();
            int noOfCols = rsmt.getColumnCount();
            String value = null;
            int index = 1; 
            while (rs.next()){
                out.write("\n");
                index = 1; 
                for (; index <= noOfCols; index++){
                    value = null;
                    value = rs.getString(index);
                    if ( value == null | value.equalsIgnoreCase("null")){
                        value = "";
                    }
                    
                    if (value.endsWith("\"")){
                       value = "\"" + value ;
                   }
                    
                    if (value.endsWith("\"")){
                       value += "\"";
                   }
                    if (index < noOfCols){
                        out.write("\"" + value + "\",");
                    }else{
                        out.write("\"" + value + "\"");
                    }
                }
                
            }

        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        } finally {
            try {
                //Close the output stream
                out.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    
    }
    
    /**
     *
     * @param CSVfilename1
     * @param out1
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     */
    public static void returnFile(String CSVfilename1, Writer out1) throws FileNotFoundException, IOException {
        Reader in = null;

        try {

            in = new BufferedReader(new FileReader(CSVfilename1));
            char[] buf = new char[4 * 1024];  // 4K char buffer
            int charsRead;
            while ((charsRead = in.read(buf)) != -1) {
                out1.write(buf, 0, charsRead);
            }


        } finally {
            if (in != null) {
                in.close();
            }

        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     * @return
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
