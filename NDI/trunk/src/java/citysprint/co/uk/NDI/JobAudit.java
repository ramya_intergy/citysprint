package citysprint.co.uk.NDI;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperRunManager;
import org.apache.http.HttpResponse;
import org.json.*;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.*;
import org.apache.http.impl.client.DefaultHttpClient;


public class JobAudit
  extends CommonClass
{
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    try
    {
      String s_Action = "";
      s_Action = request.getParameter("sAction") == null ? "" : request.getParameter("sAction").toString();
      if ((isLogin(request)) || (s_Action.equals("QuikTrak")))
      {
        initializeConfigValues(request, response);
        String sqlLabel = "";
        String Hawbno = "";
        
        String acc_id = "";
        if (s_Action.equals("QuikTrak")) {
          acc_id = (String)request.getSession(false).getAttribute("qacc_id");
        } else {
          acc_id = (String)request.getSession(false).getAttribute("acc_id");
        }
        writeOutputLog("Job Audit for Account No: " + acc_id + " SessionId: " + request.getSession(false).getAttribute("sessionid").toString() + " Action: " + s_Action);
        

        generatePDF(request, response, request.getSession(false).getAttribute("sessionid").toString(), acc_id, s_Action);
      }
      else
      {
        writeOutputLog("Session expired redirecting to index page");
        response.sendRedirect("index.jsp");
      }
    }
    catch (Exception ex)
    {
      vShowErrorPageReporting(response, ex);
    }
  }
  
  public void generatePDF(HttpServletRequest request, HttpServletResponse response, String session_ID, String acc_id, String s_Action)
    throws ServletException, IOException, SQLException
  {
    String sql1 = "";
    Connection conn = null;
    Statement stmtLabel = null;
    ResultSet rstLabel = null;
    Connection conShowAudit = null;
    Statement stmtShowAudit = null;
    Statement stmtShowInvoice = null;
    ResultSet rstShowAudit = null;
    Connection conAudit = null;
    Statement stmtAudit = null;
    ResultSet rstAudit = null;
    ResultSet rstSubLabel = null;
    ResultSet rstInvoice = null;
    ResultSet rstQuikTrak = null;
    Statement stmtQuikTrak = null;
    
    Connection conSeq = null;
    Statement stmtSeq = null;
    String sqlShowAudit = "";
    String sqlQuikTrak = "";
    String sqlShowInvoice = "";
    String sqlShowLabel = "";
    JSONArray objLabel;

    try
    {
      String sqlaudit_Delete = "delete from ndi_audit where ndi_sessionid='" + session_ID + "' and ndi_accno='" + acc_id + "' ";
      
      String sqlprod_Delete = "delete from audit_prod where prod_sessionid='" + session_ID + "' and prod_accno='" + acc_id + "' ";
      
      String sqlcommodity_Delete = "delete from audit_commodity where com_sessionid='" + session_ID + "' and com_accno='" + acc_id + "' ";
      
      String sqltrial_Delete = "delete from audit_trial where audit_sessionid='" + session_ID + "' and audit_accno='" + acc_id + "' ";
      
      String sqlseq_Delete = "delete from audit_seq where seq_sessionid='" + session_ID + "' and seq_accno='" + acc_id + "' ";
      
      String sqlseqsecond_Delete = "delete from auditseq_second where sequence_sessionid='" + session_ID + "' and sequence_accno='" + acc_id + "' ";
      
      deleteAuditdetails(sqlaudit_Delete, response);
      deleteAuditdetails(sqlprod_Delete, response);
      deleteAuditdetails(sqlcommodity_Delete, response);
      deleteAuditdetails(sqltrial_Delete, response);
      deleteAuditdetails(sqlseq_Delete, response);
      deleteAuditdetails(sqlseqsecond_Delete, response);
      
      String chk_Hawbno = request.getParameter("chkid") == null ? "" : request.getParameter("chkid").toString();
      


      String[] chk_NewHawbno = chk_Hawbno.split(",");
      
      String quikTrackJobNConsNo = null;
      for (int i = 0; i < chk_NewHawbno.length; i++)
      {
        JobAuditInfo jAudit = getAuditDetails(request, response, chk_NewHawbno[i]);
        if (!jAudit.isResult())
        {
          if (s_Action.equals("QuikTrak")) {
            vShowErrorPage(response, "The account or job/consignment/air way bill number you have entered is incorrect.  Please go back and re enter the correct details");
          } else {
            vShowErrorPage(response, jAudit.getSServiceAPIError());
          }
        }
        else
        {
          JobAuditInfoParsed jAuditPar = parseAuditDetails(jAudit.getGetAuditDetailsArray(), session_ID, acc_id, response);
          
          seqInsert(session_ID, acc_id, response, jAuditPar);
          quikTrackJobNConsNo = jAuditPar.getNdi_jobno();
        }
      }
      String chk_HawbNoNew = null;
      if (s_Action.equals("QuikTrak")) {
        chk_HawbNoNew = quikTrackJobNConsNo;
      } else {
        chk_HawbNoNew = chk_Hawbno.substring(0, chk_Hawbno.length() - 1);
      }
      sqlShowAudit = "select *,TIME_FORMAT(ndi_readyat,'%l:%i%p') as nditime, seq_no from audit_seq,ndi_audit where seq_jobno=ndi_jobno and seq_sessionid=ndi_sessionid and seq_sessionid='" + session_ID + "' and seq_jobno in (" + chk_HawbNoNew + ");";
      sqlQuikTrak = "select *,TIME_FORMAT(ndi_readyat,'%l:%i%p') as nditime from ndi_audit where ndi_sessionid='" + session_ID + "' and ndi_jobno in (" + chk_HawbNoNew + ");";
      

      sqlShowInvoice = "select *,TIME_FORMAT(ndi_readyat,'%l:%i%p') as nditime from ndi_audit where ndi_sessionid='" + session_ID + "' and ndi_jobno in (" + chk_HawbNoNew + ");";
      sqlShowLabel = sqlLabel(session_ID, response, chk_HawbNoNew);
      

      ResourceBundle bdl = new PropertyResourceBundle(new FileInputStream(getLocalDirName() + "/Config.properties"));
      

      String PDFlocation = bdl.getString("CommonPDFPath") + "/" + bdl.getString("ReportsOutput") + "/";
      PDFlocation = PDFlocation.replace("/", "//");
      
      DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
      Date date = new Date();
      String dateStr = dateFormat.format(date);
      String PDFloginname = acc_id + "_" + dateStr;
      

      String PDFfilename = PDFloginname + ".pdf";
      String PDFfilename1 = PDFlocation + PDFfilename;
      

      String locationComPDF = bdl.getString("CommonPDFPath") + "/" + bdl.getString("ReportsFolder") + "/";
      locationComPDF = locationComPDF.replace("/", "//");
      String filenameLabel = bdl.getString("PDFLabelFormat");
      
      String fileLabel = locationComPDF + filenameLabel;
      

      String filenameSubLabel = bdl.getString("PDFSubLabelFormat");
      String fileSubLabel = locationComPDF + filenameSubLabel;
      


      String filenameInvoice = bdl.getString("PDFInvoice");
      String fileInvoice = locationComPDF + filenameInvoice;
      

      String filenameSubInvoice1 = bdl.getString("PDFSubInvoice");
      String fileSubInvoice = locationComPDF + filenameSubInvoice1;
      
      String filenameSubRptInvoice = bdl.getString("PDFSubRptInvoice");
      String fileSubRptInvoice = locationComPDF + filenameSubRptInvoice;
      

      String filenameConsign = bdl.getString("PDFConsignment");
      String fileConsign = locationComPDF + filenameConsign;
      
      String filenameSubConsign = bdl.getString("PDFSubConsign");
      String fileSubConsign = locationComPDF + filenameSubConsign;
      
      String filenameSubConsign1 = bdl.getString("PDFSubConsign1");
      String fileSubConsign1 = locationComPDF + filenameSubConsign1;
      
      String filenameSubConsign2 = bdl.getString("PDFSubConsign2");
      String fileSubConsign2 = locationComPDF + filenameSubConsign2;
      
      String filenameQuikTrak = bdl.getString("PDFQuikTrak");
      String fileQuikTrak = locationComPDF + filenameQuikTrak;
      
      String filenameQuikTrakSub = bdl.getString("PDFQuikTrakSub");
      String fileQuikTrakSub = locationComPDF + filenameQuikTrakSub;
      

      String filenameLogo = bdl.getString("LogoPath");
      String filenameArrow = bdl.getString("arrowImagePath");
      String fileLabelLogo = bdl.getString("LabelLogo");
      





      conAudit = cOpenConnection();
      stmtAudit = conAudit.createStatement();
      stmtLabel = conAudit.createStatement();
      stmtShowInvoice = conAudit.createStatement();
      stmtQuikTrak = conAudit.createStatement();
      if (sqlShowAudit.length() > 0) {
        rstAudit = stmtAudit.executeQuery(sqlShowAudit);
      }
      if (sqlShowLabel.length() > 0) {
        rstLabel = stmtLabel.executeQuery(sqlShowLabel);
      }
      if (sqlShowInvoice.length() > 0) {
        rstInvoice = stmtShowInvoice.executeQuery(sqlShowInvoice);
      }
      if (sqlQuikTrak.length() > 0) {
        rstQuikTrak = stmtQuikTrak.executeQuery(sqlQuikTrak);
      }
      if (s_Action.equals("PrintLabels"))
      {
          if (bdl.getString("PrintLabelFlag").equals("Y")) {
              PDFLabelInfo pdfLabel = new PDFLabelInfo();
              objLabel = pdfLabel.getJSONAuditDetails(session_ID, chk_HawbNoNew, request, response);
              String sInput = objLabel.toString();

              writeOutputLog("JSON PDF Label Info" + sInput);
              HttpResponse resOutput = null;
              String sUrl = "";
              if (environ.equals("PROD")) {
                  sUrl = bdl.getString("LabelWebServiceURL");
              } else {
                  sUrl = bdl.getString("devhost") + "PrintLabel";
              }

              HttpClient client = new DefaultHttpClient();
              StringEntity json = new StringEntity(sInput.toString());
              json.setContentType("application/json");
              HttpPost post = new HttpPost(sUrl);
              post.addHeader("Content-Type", "application/json");
              post.setEntity(json);
              resOutput = client.execute(post);

              InputStream is = resOutput.getEntity().getContent();
              FileOutputStream fos = new FileOutputStream(new File((PDFlocation + PDFfilename)));
              int inByte;
              while ((inByte = is.read()) != -1) {
                  fos.write(inByte);
              }
              is.close();
              fos.close();


          } else {
              Map map = new HashMap();
              map.put("subpdflabel", fileSubLabel);
              map.put("sqlconnection", conAudit);
              map.put("session_ID", session_ID);
              map.put("ndi_jobno", chk_HawbNoNew);
              map.put("LogoPath", fileLabelLogo);
              JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(rstLabel);
              JasperRunManager.runReportToPdfFile(fileLabel, PDFlocation + PDFfilename, map, resultSetDataSource);
          }
        }
      else if (s_Action.equals("PrintInvoice"))
      {
        Map map = new HashMap();
        

        map.put("SubReport_Name", fileSubInvoice);
        map.put("SQL_connection", conAudit);
        
        map.put("session_ID", session_ID);
        map.put("reasonforexport", request.getParameter("txtReason") == null ? "" : request.getParameter("txtReason").toString());
        System.out.println("Session=" + session_ID);
        System.out.println("Reason=" + (request.getParameter("txtReason") == null ? "" : request.getParameter("txtReason").toString()));
        
        JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(rstInvoice);
        JasperRunManager.runReportToPdfFile(fileInvoice, PDFlocation + PDFfilename, map, resultSetDataSource);
      }
      else if (s_Action.equals("PrintConsignment"))
      {
        Map map = new HashMap();
        map.put("ImagePath", filenameLogo);
        map.put("ArrowImagePath", filenameArrow);
        map.put("subrptname", fileSubConsign);
        map.put("subrptname1", fileSubConsign1);
        map.put("subrptname2", fileSubConsign2);
        map.put("sqlconnect", conAudit);
        map.put("session_ID", session_ID);
        JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(rstAudit);
        JasperRunManager.runReportToPdfFile(fileConsign, PDFlocation + PDFfilename, map, resultSetDataSource);
      }
      else if (s_Action.equals("QuikTrak"))
      {
        Map map = new HashMap();
        map.put("ImagePath", fileLabelLogo);
        map.put("ArrowImagePath", filenameArrow);
        map.put("subrptname", fileQuikTrakSub);
        

        map.put("sqlconnect", conAudit);
        map.put("session_ID", session_ID);
        JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(rstQuikTrak);
        JasperRunManager.runReportToPdfFile(fileQuikTrak, PDFlocation + PDFfilename, map, resultSetDataSource);
      }
      else if (s_Action.equals("DisplayJobDetails"))
      {
        sql1 = URLEncoder.encode(sqlShowAudit, "UTF-8");
        

        ServletOutputStream strmResponse = response.getOutputStream();
        request.getSession(false).setAttribute("sqlShowAudit", sql1);
        response.setContentType("text/html");
        
        strmResponse.println("<script LANGUAGE=\"JAVASCRIPT\">window.open(\"JobDetails.jsp?chk_HawbNo=" + chk_Hawbno + "&ndi_sessionid=" + session_ID + "\",'DisplayJob','resizable=yes,scrollbars=yes,width=950,height=650');</script>");
      }
      if ((s_Action.equals("QuikTrak")) && (request.getSession(false).getAttribute("acc_id") == null)) {
        request.getSession(false).invalidate();
      }
      if (!s_Action.equals("DisplayJobDetails"))
      {
        File f = new File(PDFlocation, PDFfilename);
        OutputStream outStream = response.getOutputStream();
        
        ServletOutputStream stream = response.getOutputStream();
        
        response.setContentType("application/pdf");
        response.setContentLength((int)f.length());
        response.setHeader("Content-Disposition", "attachment;filename=" + PDFfilename);
        
        byte[] buf = new byte[8192];
        FileInputStream inStream = new FileInputStream(f);
        int sizeRead = 0;
        while ((sizeRead = inStream.read(buf, 0, buf.length)) > 0) {
          outStream.write(buf, 0, sizeRead);
        }
        inStream.close();
        outStream.close();
      }
    }
    catch (Exception e)
    {
      vShowErrorPageReporting(response, e);
    }
    finally
    {
      if (rstAudit != null) {
        rstAudit.close();
      }
      if (stmtAudit != null) {
        stmtAudit.close();
      }
      if (rstLabel != null) {
        rstLabel.close();
      }
      if (stmtLabel != null) {
        stmtLabel.close();
      }
      if (conAudit != null) {
        conAudit.close();
      }
      sqlShowAudit = "";
      sqlShowLabel = "";
      sqlShowInvoice = "";
    }
  }
  
  public JobAuditInfo getAuditDetails(HttpServletRequest req, HttpServletResponse res, String NewHawbno)
    throws ServletException, IOException
  {
    System.out.println("Inside getAuditDetails=" + req + "  " + res + " " + NewHawbno);
    
    JobAuditInfo jAuditOutput = new JobAuditInfo();
    jAuditOutput.setResult(true);
    URL urlApi = null;
    

    Date d = new Date();
    String timeStamp = d.toString();
    
    StringBuffer auditDetails = new StringBuffer();
    

    String s_Action = req.getParameter("sAction") == null ? "" : req.getParameter("sAction").toString();
    if (s_Action.equals("QuikTrak"))
    {
      auditDetails.append("CK=" + encStr(req.getSession(false).getAttribute("qacc_id").toString()));
      auditDetails.append("&PW=");
    }
    else
    {
      auditDetails.append("CK=" + encStr(req.getSession(false).getAttribute("acc_id").toString()));
      auditDetails.append("&PW=" + encStr(req.getSession(false).getAttribute("pwd").toString()));
    }
    auditDetails.append("&TS=" + encStr(timeStamp));
    auditDetails.append("&JN=" + encStr(NewHawbno));
    if (this.environ.equals("PROD")) {
      urlApi = new URL(this.scriptsurl + this.bdl.getString("jobAuditScript") + "?" + auditDetails.toString());
    } else {
      urlApi = new URL(this.bdl.getString("devhost") + "AuditDetails.txt");
    }
    writeOutputLog("Input to NDI_Audit:" + urlApi.toString());
    
    URLConnection con = urlApi.openConnection();
    con.setDoInput(true);
    con.setDoOutput(true);
    con.setUseCaches(false);
    
    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
    
    String outputLines = "";
    String s;
    while ((s = br.readLine()) != null) {
      outputLines = outputLines + s;
    }
    writeOutputLog("Output from NDI_Audit:" + outputLines);
    
    br.close();
    outputLines = outputLines + "^ZZ";
    jAuditOutput.setGetAuditDetailsArray(parseOutput(outputLines));
    if (jAuditOutput.getGetAuditDetailsArray() == null)
    {
      jAuditOutput.setResult(false);
    }
    else
    {
      for (int i = 0; i < jAuditOutput.getGetAuditDetailsArray().length - 1; i++) {
        if (jAuditOutput.getGetAuditDetailsArray()[i][0].equals("ER"))
        {
          jAuditOutput.setSServiceAPIError(null);
          jAuditOutput.setSServiceAPIError(jAuditOutput.getGetAuditDetailsArray()[i][1].substring(3, jAuditOutput.getGetAuditDetailsArray()[i][1].length()));
          break;
        }
      }
      if (jAuditOutput.getSServiceAPIError().length() > 0) {
        jAuditOutput.setResult(false);
      }
    }
    return jAuditOutput;
  }
  
  public String[][] parseOutput(String outputLines)
  {
    String temp = "";
    outputLines = outputLines.trim();
    outputLines = outputLines.replace('^', '\n');
    String[] stringArray = outputLines.split("\n");
    _arrayLength = stringArray.length;
    String[][] parsedOutput = (String[][])null;
    parsedOutput = new String[_arrayLength + 1][2];
    for (int i = 0; i < _arrayLength; i++)
    {
      temp = stringArray[i];
      if (temp.length() > 0)
      {
        parsedOutput[i][0] = stringArray[i].substring(0, 2);
        parsedOutput[i][1] = stringArray[i].substring(2);
      }
      else
      {
        parsedOutput[i][0] = "ST";
        parsedOutput[i][1] = "START";
      }
    }
    return parsedOutput;
  }
  
  public JobAuditInfoParsed parseAuditDetails(String[][] AuditDetailsArray, String session_ID, String acc_id, HttpServletResponse res)
    throws ServletException, IOException
  {
    String tempstr = "";
    String tag = "";
    String sSQL = "";
    String sqlAuditHeader = "";
    String sqlCommodityHeader = "";
    String sqlProductHeader = "";
    String sqlAuditTrialHeader = "";
    String sSQLAuditTrial = "";
    String sSQLProduct = "";
    String sSQLCommodity = "";
    

    String sql_product = "";
    String sql_commodity = "";
    String sql_Commodity = "";
    String sql_AuditTrial = "";
    String sql_audittrial = "";
    String emptyString = "''";
    int sqllen = 0;
    int sqllen1 = 0;
    int sqllen2 = 0;
    String sJobNo = "";
    int nzvalue = 0;
    int czvalue = 0;
    String sProductType = "";
    Connection conAudit = null;
    Vector v = new Vector();
    Vector sqlcom = new Vector();
    Vector sqlaudit = new Vector();
    JobAuditInfoParsed jAuditParsed = new JobAuditInfoParsed();
    





    sqlAuditHeader = "insert into ndi_audit(ndi_sessionid,ndi_accno,ndi_jobno,ndi_consignno,ndi_readyon,ndi_readyat,ndi_depart,ndi_ref,ndi_caller,ndi_service,ndi_vechicle,ndi_phone,ndi_booking,ndi_price,ndi_surcharge,ndi_gstvat,ndi_liability,ndi_pcomp,ndi_paddress,ndi_paddress2,ndi_paddress3,ndi_ptownkey,ndi_ptown,ndi_pcounty,ndi_ppostcode,ndi_pcountrycode, ndi_presidentialflag, "
            + "ndi_pcountry,ndi_plongitu,ndi_platitu,ndi_pinstruct,ndi_pcontact,ndi_pphone,ndi_dcomp,ndi_daddress,ndi_daddress2,ndi_daddress3,ndi_dtownkey,ndi_dtown,ndi_dcounty,ndi_dpostcode,ndi_dcountrycode, ndi_dresidentialflag, "
            + "ndi_dcountry,ndi_dlongitu,ndi_dlatitu,ndi_dinstruct,ndi_dcontact,ndi_dphone,ndi_dvatno,ndi_ddate,ndi_dtime, ndi_scname, ndi_scaddress, ndi_scaddress2, ndi_sctown, ndi_sccounty, ndi_scpostcode,ndi_sccountrycode,ndi_sclongitude, ndi_sclatitude, ndi_carriername, ndi_routingcode, ndi_destination_label, ndi_prodtype,  ndi_totpieces,ndi_totweight) values('" + session_ID + "','" + acc_id + "',";
    







    sqlCommodityHeader = "insert into audit_commodity(com_sessionid,com_accno,com_code,com_description,com_quantity,com_value,com_indemnity,com_jobno)values('" + session_ID + "','" + acc_id + "',";
    
    sqlProductHeader = "insert into audit_prod(prod_sessionid,prod_accno,prod_noitems, "//pro_iweight, pro_ptype, "
            + "prod_height,prod_length,prod_depth,prod_jobno)values('" + session_ID + "','" + acc_id + "',";
    

    sqlAuditTrialHeader = "insert into audit_trial(audit_sessionid,audit_accno,audit_date,audit_time,audit_descrip,audit_jobno)values('" + session_ID + "','" + acc_id + "',";
    



    int k = AuditDetailsArray.length;
    System.out.println(k);
    try
    {
      for (int i = 0; i < AuditDetailsArray.length - 1; i++)
      {
        tag = AuditDetailsArray[i][0];
        if (tag.equals("TS")) {
          tempstr = AuditDetailsArray[i][1].trim();
        }
        if (tag.equals("JN"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          jAuditParsed.setNdi_jobno(tempstr);
          if (tempstr.length() > 0)
          {
            sJobNo = tempstr;
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          }
          else
          {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DT"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("RE"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("CA"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("SV"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("VT"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("PH"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("AB"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("RO"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "DATE_FORMAT('" + tempstr + "','%d/%m/%y'),";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("RA"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        int itemp = 0;
        if (tag.equals("BP"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + tempstr + ",";
          } else {
            sSQL = sSQL + itemp + ",";
          }
        }
        if (tag.equals("GS"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + tempstr + ",";
          } else {
            sSQL = sSQL + itemp + ",";
          }
        }
        if (tag.equals("SC"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + tempstr + ",";
          } else {
            sSQL = sSQL + itemp + ",";
          }
        }
        if (tag.equals("LP"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + tempstr + ",";
          } else {
            sSQL = sSQL + itemp + ",";
          }
        }
        if (tag.equals("PC"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("PA"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("P2"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("P3"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("PK"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + tempstr + ",";
          } else {
            sSQL = sSQL + itemp + ",";
          }
        }
        if (tag.equals("PS"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("PO"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("PP"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("PY"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("PF"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("PG"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("PU"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("PI"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("PX"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("PE"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("PR"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DC"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DA"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("D2"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("D3"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DK"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + sReplaceChars(tempstr, "'", "''") + ",";
          } else {
            sSQL = sSQL + itemp + ",";
          }
        }
        if (tag.equals("DS"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DO"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DP"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DY"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DF"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DH"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DV"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DD"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DM"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DL"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DU"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DI"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DX"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("DR"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("TP"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          sProductType = tempstr;
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("PN"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + tempstr + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
          jAuditParsed.setNdi_totpieces(Integer.parseInt(tempstr));
        }
        if (tag.equals("AD"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQLAuditTrial = sSQLAuditTrial + "DATE_FORMAT('" + tempstr + "','%d/%m/%y'),";
          } else {
            sSQLAuditTrial = sSQLAuditTrial + emptyString + ",";
          }
        }
        if (tag.equals("AT"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQLAuditTrial = sSQLAuditTrial + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQLAuditTrial = sSQLAuditTrial + emptyString + ",";
          }
        }
        if (tag.equals("AE"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQLAuditTrial = sSQLAuditTrial + "'" + sReplaceChars(tempstr, "'", "''") + "'";
          } else {
            sSQLAuditTrial = sSQLAuditTrial + emptyString;
          }
          sql_AuditTrial = sqlAuditTrialHeader + sSQLAuditTrial + ",'" + sJobNo + "')";
          
          Connection conAuditTrial = cOpenConnection();
          Statement stmtAuditTrial = conAuditTrial.createStatement();
          stmtAuditTrial.executeUpdate(sql_AuditTrial);
          vCloseConnection(conAuditTrial);
          
          sSQLAuditTrial = "";
        }
        if (tag.equals("NZ"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          
          nzvalue = Integer.parseInt(tempstr);
        }
        if (tag.equals("NI"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQLProduct = sSQLProduct + tempstr + ",";
          } else {
            sSQLProduct = sSQLProduct + itemp + ",";
          }
        }
//        if (tag.equals("WI"))
//        {
//          tempstr = AuditDetailsArray[i][1].trim();
//          if (tempstr.length() > 0) {
//            sSQLProduct = sSQLProduct + tempstr + ",";
//          } else {
//            sSQLProduct = sSQLProduct + itemp + ",";
//          }
//        }
//        if (tag.equals("PT"))
//        {
//          tempstr = AuditDetailsArray[i][1].trim();
//          if (tempstr.length() > 0) {
//            sSQLProduct = sSQLProduct + "'" + tempstr + "',";
//          } else {
//            sSQLProduct = sSQLProduct + "'" + itemp + "',";
//          }
//        }
        if (tag.equals("HE"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQLProduct = sSQLProduct + tempstr + ",";
          } else {
            sSQLProduct = sSQLProduct + itemp + ",";
          }
        }
        if (tag.equals("PL"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQLProduct = sSQLProduct + tempstr + ",";
          } else {
            sSQLProduct = sSQLProduct + itemp + ",";
          }
        }
        if (tag.equals("PD"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQLProduct = sSQLProduct + tempstr;
          } else {
            sSQLProduct = sSQLProduct + itemp + ",";
          }
          sql_product = sqlProductHeader + sSQLProduct + ",'" + sJobNo + "')";
          
          Connection conProd = cOpenConnection();
          Statement stmtProd = conProd.createStatement();
          
          System.out.println("JobAudit Line#1543 ==> sql query ==" + sql_product);
          stmtProd.executeUpdate(sql_product);
          conProd.close();
          
          sSQLProduct = "";
        }
        if (tag.equals("CZ"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          czvalue = Integer.parseInt(tempstr);
        }
        if (tag.equals("CC"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQLCommodity = sSQLCommodity + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQLCommodity = sSQLCommodity + emptyString + ",";
          }
        }
        if (tag.equals("CD"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQLCommodity = sSQLCommodity + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQLCommodity = sSQLCommodity + emptyString + ",";
          }
        }
        if (tag.equals("CQ"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQLCommodity = sSQLCommodity + tempstr + ",";
          } else {
            sSQLCommodity = sSQLCommodity + itemp + ",";
          }
        }
        if (tag.equals("CV"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQLCommodity = sSQLCommodity + tempstr + ",";
          } else {
            sSQLCommodity = sSQLCommodity + itemp + ",";
          }
        }
        if (tag.equals("CI"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQLCommodity = sSQLCommodity + "'" + sReplaceChars(tempstr, "'", "''") + "'";
          } else {
            sSQLCommodity = sSQLCommodity + emptyString + ",";
          }
          sql_commodity = sqlCommodityHeader + sSQLCommodity + ",'" + sJobNo + "')";
          
          Connection conCom = cOpenConnection();
          Statement stmtCom = conCom.createStatement();
          stmtCom.executeUpdate(sql_commodity);
          
          conCom.close();
          
          sSQLCommodity = "";
        }
        if (tag.equals("HN"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          jAuditParsed.setChk_HawbNo(tempstr);
          String tempHawbno = "";
          String tempHawbno1 = "";
          String tempHawbno2 = "";
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("SN"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("SA"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("S2"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("SS"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("SO"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("SP"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("SY"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("SG"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("SU"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("SI"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("SJ"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("SK"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";
          } else {
            sSQL = sSQL + emptyString + ",";
          }
        }
        if (tag.equals("TW"))
        {
          tempstr = AuditDetailsArray[i][1].trim();
          if (tempstr.length() > 0) {
            sSQL = sSQL + tempstr;
          } else {
            sSQL = sSQL + itemp;
          }
          sSQL = sqlAuditHeader + sSQL + ")";
          
          conAudit = cOpenConnection();
          Statement stmtAudit = conAudit.createStatement();
          System.out.println("JobAudit Line# 1849==> sqlAudit:" + sSQL);
          stmtAudit.executeUpdate(sSQL);
          if (sProductType.equals("DOX"))
          {
            String sqlCommodity = "insert into audit_commodity(com_sessionid,com_accno,com_code,com_description,com_quantity,com_value,com_indemnity,com_jobno)values('" + session_ID + "','" + acc_id + "','DOX','DOCUMENTS','1','0','N','" + sJobNo + "')";
            
            stmtAudit.executeUpdate(sqlCommodity);
          }
          conAudit.close();
          

          sSQL = "";
          
          sql_Commodity = "";
          sql_audittrial = "";
        }
      }
    }
    catch (Exception ex)
    {
      vShowErrorPageReporting(res, ex);
    }
    return jAuditParsed;
  }
  
  public void seqInsert(String session_ID, String acc_id, HttpServletResponse res, JobAuditInfoParsed jAudPar)
    throws ServletException, IOException, SQLException
  {
    Connection conSeq = null;
    Statement stmtSeq = null;
    try
    {
      conSeq = cOpenConnection();
      for (int s = 1; s <= jAudPar.getNdi_totpieces(); s++)
      {
        String sql_SeqFirst = "insert into audit_seq(seq_sessionid,seq_accno,seq_no,seq_jobno,seq_consignno) values('" + session_ID + "','" + acc_id + "', " + s + ",'" + jAudPar.getNdi_jobno() + "','" + jAudPar.getChk_HawbNo() + "')";
        String sql_SeqSecond = "insert into auditseq_second(sequence_sessionid,sequence_accno,sequence_no,sequence_jobno,sequence_consignno) values('" + session_ID + "' ,'" + acc_id + "'," + s + ",'" + jAudPar.getNdi_jobno() + "','" + jAudPar.getChk_HawbNo() + "')";
        if (s == 1)
        {
          for (int j = 0; j < 2; j++)
          {
            stmtSeq = conSeq.createStatement();
            stmtSeq.executeUpdate(sql_SeqFirst);
          }
          for (int j = 0; j < 2; j++)
          {
            stmtSeq = conSeq.createStatement();
            stmtSeq.executeUpdate(sql_SeqSecond);
          }
        }
        else if (s % 2 == 0)
        {
          stmtSeq = conSeq.createStatement();
          stmtSeq.executeUpdate(sql_SeqFirst);
        }
        else
        {
          stmtSeq = conSeq.createStatement();
          stmtSeq.executeUpdate(sql_SeqSecond);
        }
      }
    }
    catch (Exception e)
    {
      vShowErrorPageReporting(res, e);
    }
    finally
    {
      stmtSeq.close();
      conSeq.close();
    }
  }
  
  public void deleteAuditdetails(String sDelete, HttpServletResponse res)
    throws ServletException, IOException
  {
    try
    {
      Connection conDelete = cOpenConnection();
      Statement stmtDelete = conDelete.createStatement();
      stmtDelete.executeUpdate(sDelete);
      System.out.println("Delete=" + sDelete);
      conDelete.close();
    }
    catch (Exception e)
    {
      vShowErrorPageReporting(res, e);
    }
  }
  
  public String sqlLabel(String session_ID, HttpServletResponse res, String chkHawbNo)
    throws ServletException, IOException
  {
    Connection conShowLabel = null;
    Statement stmtShowLabel = null;
    
    String sqlShowLabel = "";
    int i = 0;
    try
    {
      String sSqlLabel = "Select ndi_jobno, ndi_totpieces from ndi_audit where ndi_jobno in (" + chkHawbNo + ") and ndi_sessionid='" + session_ID + "'";
      System.out.println("################### JobAudit: sSQLLabel == " + sSqlLabel);
      conShowLabel = cOpenConnection();
      stmtShowLabel = conShowLabel.createStatement();
      ResultSet rstRead = stmtShowLabel.executeQuery(sSqlLabel);
      while (rstRead.next())
      {
        i++;
        if (i > 1) {
          sqlShowLabel = sqlShowLabel + " union all ";
        }
        for (int s = 1; s <= rstRead.getInt("ndi_totpieces"); s++) {
          if (s == 1) {
            sqlShowLabel = sqlShowLabel + "select *, '" + s + "' as ndi_seq from ndi_audit where ndi_sessionid='" + session_ID + "' and ndi_jobno = " + rstRead.getString("ndi_jobno");
          } else {
            sqlShowLabel = sqlShowLabel + " union all select *, '" + s + "' as ndi_seq from ndi_audit where ndi_sessionid='" + session_ID + "' and ndi_jobno = " + rstRead.getString("ndi_jobno");
          }
        }
      }
    }
    catch (Exception ex)
    {
      vShowErrorPageReporting(res, ex);
    }
    System.out.println("sqlShowLabel:" + sqlShowLabel);
    
    return sqlShowLabel;
  }
  
  public static void returnFile(String filename1, Writer out1)
    throws FileNotFoundException, IOException
  {
    Reader in = null;
    try
    {
      in = new BufferedReader(new FileReader(filename1));
      char[] buf = new char[4096];
      int charsRead;
      while ((charsRead = in.read(buf)) != -1) {
        out1.write(buf, 0, charsRead);
      }
    }
    finally
    {
      if (in != null) {
        in.close();
      }
    }
  }
  
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    processRequest(request, response);
  }
  
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    processRequest(request, response);
  }
  
  public String getServletInfo()
  {
    return "Short description";
  }
}
