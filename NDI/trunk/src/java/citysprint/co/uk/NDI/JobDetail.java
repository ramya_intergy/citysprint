package citysprint.co.uk.NDI;

/*
 * Jobdetail.java
 *
 * Created on 20 June 2008, 11:53
 */

import citysprint.co.uk.NDI.CommonClass;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.*;
import javax.servlet.http.*;
import java.net.URL; 
import java.net.URLEncoder;
import java.util.*;


/**
 *
 * @author vanir
 * @version
 */

public class JobDetail extends CommonClass {
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    
     /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
   
    response.setContentType("text/html;charset=UTF-8");

    
    try{

        if (isLogin(request)) {

            writeOutputLog("Job Details for Account No: "+request.getSession(false).getAttribute("acc_id").toString()+
                    " SessionId: "+request.getSession(false).getAttribute("sessionid").toString());
            initializeConfigValues(request,response);

            ReportSearchCriterias reportSearch = new ReportSearchCriterias();
            reportSearch.setJno(request.getParameter("cgno")==null?"":request.getParameter("cgno").toString());
            reportSearch.setFmdate(request.getParameter("fmdate")==null?"":request.getParameter("fmdate").toString());
            reportSearch.setTodate(request.getParameter("todate")==null?"":request.getParameter("todate").toString());
            reportSearch.setDcountry(sReplaceChars((request.getParameter("countr")==null?"":request.getParameter("countr").toString()),"'","''"));
            reportSearch.setDcomp(sReplaceChars((request.getParameter("comp")==null?"":request.getParameter("comp").toString()),"'","''"));
            reportSearch.setUrref(sReplaceChars((request.getParameter("ref")==null?"":request.getParameter("ref").toString()),"'","''"));
            reportSearch.setUrdept(sReplaceChars((request.getParameter("dept")==null?"":request.getParameter("dept").toString()),"'","''"));

            //Reference 2 changes
            reportSearch.setUref2(sReplaceChars((request.getParameter("ref2")==null?"":request.getParameter("ref2").toString()),"'","''"));
            reportSearch.setUcaller(sReplaceChars((request.getParameter("caller")==null?"":request.getParameter("caller").toString()),"'","''"));

            reportSearch.setStatus(sReplaceChars((request.getParameter("chkStatus")==null?"":request.getParameter("chkStatus").toString()),"'","''"));
            reportSearch.setStatus1(sReplaceChars((request.getParameter("chkStatus1")==null?"":request.getParameter("chkStatus1").toString()),"'","''"));
            reportSearch.setStatus2(sReplaceChars((request.getParameter("chkStatus2")==null?"":request.getParameter("chkStatus2").toString()),"'","''"));
            reportSearch.setIncl(request.getParameter("cInclude")==null?"":request.getParameter("cInclude").toString());
            reportSearch.setSortby(request.getParameter("cSortby")==null?"":request.getParameter("cSortby").toString());
            reportSearch.setRdStatus(request.getParameter("group")==null?"":request.getParameter("group").toString());
            
             //Method declaration for Jobdetails
            JobDetailsParsed  jDetails=getJobdetails(request,response, reportSearch.getJno(), reportSearch.getFmdate(), reportSearch.getTodate());
            ReportSQLs rSQLs=null;

            if(jDetails.getSJobDetailError().length()==0 && jDetails.getGetJobdetailsArray()!=null){

                 //Method for Delete based on sessionid and userid
                 deleteJobdetails(request.getSession(false).getAttribute("acc_id").toString(),request.getSession(false).getAttribute("sessionid").toString(), response);

                 //Method declaration for Parse Jobdetails
                 parseJobdetails(jDetails.getGetJobdetailsArray(),request.getSession(false).getAttribute("acc_id").toString(),request.getSession(false).getAttribute("sessionid").toString(), response);

                 //Method declaration for Showdetails
                 rSQLs = showdetails(request.getSession(false).getAttribute("sessionid").toString(), request.getSession(false).getAttribute("acc_id").toString(), reportSearch, response);

                 request.getSession(false).setAttribute("sql1", rSQLs.getSql());
                 request.getSession(false).setAttribute("sqlCSV", rSQLs.getSqlCSV());
             }
            else
            {
                 request.getSession(false).setAttribute("sql1", "");
                 request.getSession(false).setAttribute("sqlCSV", "");
            }
             reportSearch.setJoberror(jDetails.getSJobDetailError());
             //response.sendRedirect("Reporting.jsp?&cInclude="+incl + "&cSortby="+sortby +"&cgno="+jno + "&fmdate="+fmdate + "&todate="+todate + "&countr="+dcountry + "&comp="+dcomp+ "&ref="+urref +"&dept="+urdept+"&chkStatus="+status +"&chkStatus1="+status1+"&chkStatus2="+status2 +"&group="+rdStatus + "&joberror="+sJobDetailError);
             request.setAttribute("searchCriterias", reportSearch);
             getServletConfig().getServletContext().getRequestDispatcher("/Reporting.jsp").forward(request, response);
            
            }
            else {
                writeOutputLog("Session expired redirecting to index page");
                response.sendRedirect("index.jsp");
            }
        
    }
    catch(Exception ex){
        vShowErrorPageReporting(response, ex);
    }
  }

    /**
     *
     * @param req
     * @param res
     * @return
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public JobDetailsParsed getJobdetails(HttpServletRequest req,HttpServletResponse res, String jno, String fmdate, String todate) throws ServletException,IOException
   {
      
       System.out.println("Inside getJobdetails="+ req +"  "+res);
       String sJobDetailError="";
       java.util.Date d = new java.util.Date();
       String timeStamp = d.toString();
     
       URL urlApi=null;
       JobDetailsParsed jDetails = new JobDetailsParsed();

       StringBuffer jobdetails = new StringBuffer();
          
        //API Input
           
        jobdetails.append("CK="+encStr(req.getSession(false).getAttribute("acc_id").toString()));                                    //Customer Key or Account No
        jobdetails.append("&PW="+encStr(req.getSession(false).getAttribute("pwd").toString()));                                      //Password
        jobdetails.append("&TS="+encStr(timeStamp));
        jobdetails.append("&HN="+encStr(jno));                                       //JobNumber
        jobdetails.append("&SD="+encStr(fmdate));                                    //FromDate
        jobdetails.append("&ED="+encStr(todate));                                    //ToDate

         if(environ.equals("PROD"))
             urlApi = new URL(scriptsurl+bdl.getString("jobDetailsScript")+"?"+jobdetails.toString());
         else
             urlApi = new URL(bdl.getString("devhost")+ "NDIJobDetails.txt");
           
   
        writeOutputLog("Input to ndi_details:"+urlApi.toString());

         URLConnection con = urlApi.openConnection();
         System.out.println(con);
         con.setDoInput(true);
         con.setDoOutput(true);
         con.setUseCaches(false);
         
         BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
         String s;
         String outputLines = "";
         while ((s = br.readLine()) != null) {
            outputLines = outputLines + s;
         }
       
         writeOutputLog("Output from ndi_details:"+outputLines);
       
        br.close();
        //outputLines = outputLines.replace("^XX", "^PHphone^CAcaller^XX") + "^ZZ";
        outputLines = outputLines + "^ZZ";

        jDetails.setGetJobdetailsArray(parseOutput(outputLines));

        if(jDetails.getGetJobdetailsArray() == null)
            sJobDetailError="Error in NDI_Details no output returned from API";
        else
        {
            for(int i=0;i<jDetails.getGetJobdetailsArray().length-1;i++)
            {
                if(jDetails.getGetJobdetailsArray()[i][0].equals("ER"))
                {
                    jDetails.setSJobDetailError(null);
                    jDetails.setSJobDetailError(jDetails.getGetJobdetailsArray()[i][1].substring(3, jDetails.getGetJobdetailsArray()[i][1].length()));
                    break;
                }
                
            }
            
              
       }
      
        return jDetails;
    }
     
     
    
   /**
    *
    * @param JobdetailsArray
    * @param acc_id
    * @param session_ID
    * @throws javax.servlet.ServletException
    * @throws java.io.IOException
    */
   public void parseJobdetails(String[][] JobdetailsArray,String acc_id,String session_ID, HttpServletResponse res) throws ServletException, IOException
   {
       String tempstr="";
       String tag ="";
       String sSQL="";
       String sSQLStamp="";
       String sComDescrip="";
       String empty_String="''";
       String server        = bdl.getString("dmsServer");
       String environment   = bdl.getString("environment");
       String sqlHeader = "insert into ndi_details (det_sessionid,det_accid,det_server,det_environ,det_jno,det_hawbno,det_readyon,det_readyat,det_servcode,det_ref,det_price,det_surcharge,det_vat,det_liability,det_dcomp,det_dcountry,det_ptype,det_totpieces,det_totweight,det_pod,det_jstatus,det_descrip, det_dept,det_ref2,det_caller)" +
              "values('"+session_ID+"','"+acc_id+"','"+server+"','"+environment+"', "; 
 
       int i,itemp=0;
       //int j=14;
       int k = JobdetailsArray.length;
       System.out.println(k);
       int sqllen;
       for( i=0;i<JobdetailsArray.length-1;i++){

           tag = JobdetailsArray[i][0];

           //Tag equals Transaction 

           if (tag.equals("TS")) {

               tempstr = JobdetailsArray[i][1].trim();

           }

           //Tag equals JobNumber 
           if (tag.equals("JN")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + "'" + sReplaceChars(tempstr, "'", "''") + "'" + ",";

               } else {
                   //String sqlJobNo="''";
                   sSQL = sSQL + empty_String + ",";
               }

           }

           //Tag equals Product Type 
           if (tag.equals("TP")) {


               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {
                   // MySql Date Format
                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

               } else {
                   //String sqlProd="''";
                   sSQL = sSQL + empty_String + ",";
               }

           }
           //Tag equals Ready on
           if (tag.equals("RO")) {


               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {
                   // MySql Date Format
                   sSQL = sSQL + "DATE_FORMAT('" + tempstr + "','%d/%m/%y'),";

               } else {
                   //String sqlReadyOn="''"; 
                   sSQL = sSQL + empty_String + ",";
               }

           }
           //Tag Equals ReadyAt
           if (tag.equals("RA")) {


               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {
                   // MySql Date Format
                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

               } else {
                   // String sqlReadyAt="''";
                   sSQL = sSQL + empty_String + ",";
               }

           }


           //Tag equals Reference 
           if (tag.equals("RE")) {
               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {
                   //MySql Query

                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";


               } else {
                   //String Ref="''";
                   sSQL = sSQL + empty_String + ",";
               }

           }



           //Tag equals Delivery Country
           if (tag.equals("DC")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
               } else {
                   //String sqlCountry="''";
                   sSQL = sSQL + empty_String + ",";
               }
           }

           //Tag equals Delivery Company 
           if (tag.equals("DY")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
               } else {
                   //String sqlCompany="''";
                   sSQL = sSQL + empty_String + ",";
               }
           }



           //Tag equals Service Level 

           if (tag.equals("SV")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
               } else {
                   // String sqlService="''";
                   sSQL = sSQL + empty_String + ",";
               }

           }

           //Tag equals total no of Pieces 
           if (tag.equals("PN")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + tempstr + ",";
               } else {
                   // String sqlItem="''";
                   sSQL = sSQL + itemp + ",";
               }

           }

           //Tag equals Weight

           if (tag.equals("TW")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + tempstr + ",";
               } else {
                   // String sqlWeight="''";
                   sSQL = sSQL + itemp + ",";
               }

           }

           //Tag equals POD details 
           if (tag.equals("PO")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
               } else {
                   //String sqlPod="''";
                   sSQL = sSQL + empty_String + ",";
               }

           }



           //Tag equals Hawbno
           if (tag.equals("HN")) {

               tempstr = JobdetailsArray[i][1].trim();
               String tempHawbno = "";
               String tempHawbno1 = "";
               String tempHawbno2 = "";

               if (tempstr.length() > 0) {

                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";

               } else {

                   sSQL = sSQL + empty_String + ",";
               }

           }

           //Tag equals Basic Price

           if (tag.equals("BP")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + tempstr + ",";
               } else {
                   // String sqlPrice ="''";
                   sSQL = sSQL + itemp + ",";
               }

           }
           
           //Tag equals SurCharges
           if (tag.equals("SC")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + tempstr + ",";
               } else {
                   //  String sqlSurcharge="''";
                   sSQL = sSQL + itemp + ",";
               }

           }

           //Tag equals VAT
           if (tag.equals("GS")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + tempstr + ",";
               } else {
                   //String sqlGst="''";
                   sSQL = sSQL + itemp + ",";
               }

           }

           //Tag equals Liability
           if (tag.equals("LP")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + tempstr + ",";
               } else {
                   // String sqlLiability="''";
                   sSQL = sSQL + itemp + ",";
               }

           }


           //Tag equals JobStatus
           if (tag.equals("JS")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + "'" + sReplaceChars(tempstr,"'","''") + "'" + ",";
               } else {
                   //String sqlJobstatus="''";
                   sSQL = sSQL + empty_String + ",";
               }
           }

           int czvalue = 0;
           if (tag.equals("CZ")) {

               tempstr = JobdetailsArray[i][1].trim();
               czvalue = Integer.parseInt(tempstr);

               if (tempstr.equals("0")) {
                   String sqlTest = "''";
                   sSQL = sSQL + sqlTest;
               }
               } else {


                   for (int c = 0; c <= czvalue; c++) {
                       //Tag Equals Commodity Code

                       if (tag.equals("CC")) {
                           tempstr = JobdetailsArray[i][1].trim();
                       }


                       //Tag Equals Commodity Description

                       if (tag.equals("CD")) {

                           tempstr = JobdetailsArray[i][1].trim();

                           if (tempstr.length() > 0) {

                               //sComDescrip= sComDescrip + tempstr;
                           sComDescrip = sComDescrip + sReplaceChars(tempstr,"'","''");
                           } else {
                               //String sqldescrip="''";
                               sComDescrip = sComDescrip + empty_String;
                           // sSQL = sSQL+"'" + sComDescrip +"'" ;
                           }


                       }

                       //Tag Equals Commodity Quantity

                       if (tag.equals("CQ")) {

                           tempstr = JobdetailsArray[i][1].trim();


                       }

                       //Tag Equals Commodity Value
                       if (tag.equals("CV")) {

                           tempstr = JobdetailsArray[i][1].trim();

                       }

                       // Tag Equals Commodity Indemnity
                       if (tag.equals("CI")) {

                           tempstr = JobdetailsArray[i][1].trim();

                       sSQL = sSQL + "'" + sComDescrip + "'";
                           sComDescrip = "";

                       }


                   }



               }
           

           //Tag equals Department
           if (tag.equals("DT")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + ",'" + sReplaceChars(tempstr,"'","''") + "'";
               } else {
                   sSQL = sSQL + "," + empty_String;
               }
           }

             //Tag equals Phone
           if (tag.equals("PH")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + ",'" + sReplaceChars(tempstr,"'","''") + "'";
               } else {
                   sSQL = sSQL + "," + empty_String;
               }
           }

             //Tag equals Department
           if (tag.equals("CA")) {

               tempstr = JobdetailsArray[i][1].trim();

               if (tempstr.length() > 0) {

                   sSQL = sSQL + ",'" + sReplaceChars(tempstr,"'","''") + "'";
               } else {
                   sSQL = sSQL + "," + empty_String;
               }
           }

           //End Tag for Each Job set

           if (tag.equals("XX")) {

               tempstr = JobdetailsArray[i][1].trim();

               // sSQL=sSQL+"'"+tempstr+"'";

               sSQL = sqlHeader + sSQL + ")";

               try {

                   Connection condetails = cOpenConnection();
                   Statement stmtdetails = condetails.createStatement();
                   writeOutputLog("JobDetails ==> sSQL == " + sSQL);
                   stmtdetails.executeUpdate(sSQL);//,Statement.RETURN_GENERATED_KEYS);
                   writeOutputLog("Warnings:"+ stmtdetails.getWarnings());
                   condetails.close();
                }
                catch(SQLException e){
                    vShowErrorPageReporting(res, e);
                }
                sSQL="";
            } 
        
       }       
    }
   
   /**
    *
    * @param acc_id
    * @param session_ID
    * @throws javax.servlet.ServletException
    * @throws java.io.IOException
    */
   public void deleteJobdetails(String acc_id,String session_ID, HttpServletResponse res)throws ServletException, IOException
   {
         try
         {
            Connection conDelete = cOpenConnection();
            String sqlDelete = "delete from ndi_details where det_sessionid='"+session_ID+"' and det_accid='"+acc_id+"' ";
            Statement stmtDelete = conDelete.createStatement();
            stmtDelete.executeUpdate(sqlDelete);
            conDelete.close();
         }
         catch(Exception e)
         {
             vShowErrorPageReporting(res, e);
         }
   }
  
   /**
    *
    * @param session_ID
    * @throws javax.servlet.ServletException
    * @throws java.io.IOException
    */
   public ReportSQLs showdetails(String session_ID, String acc_id, ReportSearchCriterias rsc, HttpServletResponse res) throws ServletException, IOException{

        String ss[];
        String newString = "";
        String sSQL1 = "";
        String sqlHeader1 = "";
        String sSQL2 = "";
        
        Connection conShow = null;
        ReportSQLs rSQL = new ReportSQLs();

        if (rsc.getIncl().length() > 0) //for remove comma using Substring for cInclude Fields
        {
            rsc.setIncl(rsc.getIncl().substring(0, rsc.getIncl().length() - 1));
        }
        ss = rsc.getIncl().split(",");
        for (int i = 0; i < ss.length; i++) {

            if (ss[i].equals("det_readyon")) {
                newString = newString + "DATE_FORMAT(" + ss[i] + ",'%d/%m/%y')" + ",";

            } else if (ss[i].equals("det_totcharge")) {
                //newString = newString + "(det_price+det_vat+det_surcharge+det_liability) as det_totcharge" + ",";
                newString = newString + "(det_price+det_surcharge+det_liability) as det_totcharge" + ",";
            } else {
                newString = newString + ss[i] + ",";

            }
        }

        newString = newString.substring(0, newString.length() - 1);

        //for remove comma using Substring for sSortby Fields
        if (rsc.getSortby().length() > 0) {
            rsc.setSortby(rsc.getSortby().substring(0, rsc.getSortby().length() - 1));
        }
        
        int dc = 0, cm = 0, re = 0, st = 0, st1 = 0, st2 = 0, sid = 0, de = 0, re2 = 0, ca = 0;
        cm = rsc.getDcomp().length();
        dc = rsc.getDcountry().length();
        re = rsc.getUrref().length();
        st = rsc.getStatus().length();
        st1 = rsc.getStatus1().length();
        st2 = rsc.getStatus2().length();
        sid = session_ID.length();
        de = rsc.getUrdept().length();
        re2 = rsc.getUref2().length();
        ca = rsc.getUcaller().length();

        try {

            conShow = cOpenConnection();


            if ((dc > 0 || cm > 0 || re > 0 || st > 0 || st1 > 0 || st2 > 0 || sid > 0 || de > 0 || re2 > 0 || ca > 0)) {

                sqlHeader1 = " where ";
                if (sid > 0) {
                    sSQL1 = sSQL1 + "det_sessionid='" + session_ID + "' ";
                }
                if (dc > 0) {
                    if (sSQL1.length() > 0) {
                        sSQL1 = sSQL1 + "and";
                    }
                    sSQL1 = sSQL1 + " UCASE(det_dcountry) like '%" + rsc.getDcountry().toUpperCase() + "%'  ";
                }
                if (cm > 0) {
                    if (sSQL1.length() > 0) {
                        sSQL1 = sSQL1 + " and ";
                    }

                    sSQL1 = sSQL1 + " UCASE(det_dcomp) like '%" + rsc.getDcomp().toUpperCase() + "%'  ";
                }
                if (re > 0) {
                    if (sSQL1.length() > 0) {
                        sSQL1 = sSQL1 + " and ";
                    }

                    sSQL1 = sSQL1 + " UCASE(det_ref) like '%" + rsc.getUrref().toUpperCase() + "%' ";
                }

                 if (de > 0) {
                    if (sSQL1.length() > 0) {
                        sSQL1 = sSQL1 + " and ";
                    }

                    sSQL1 = sSQL1 + " UCASE(det_dept) like '%" + rsc.getUrdept().toUpperCase() + "%' ";
                }

                if (re2 > 0) {
                    if (sSQL1.length() > 0) {
                        sSQL1 = sSQL1 + " and ";
                    }

                    sSQL1 = sSQL1 + " UCASE(det_ref2) like '%" + rsc.getUref2().toUpperCase() + "%' ";
                }

                 if (ca > 0) {
                    if (sSQL1.length() > 0) {
                        sSQL1 = sSQL1 + " and ";
                    }

                    sSQL1 = sSQL1 + " UCASE(det_caller) like '%" + rsc.getUcaller().toUpperCase() + "%' ";
                }

                if (st > 0) {

                    sSQL2 = sSQL2 + " UCASE(det_jstatus) ='" + rsc.getStatus().toUpperCase() + "' ";
                }
                if (st1 > 0) {
                    if (sSQL2.length() > 0) {
                        sSQL2 = sSQL2 + " or ";
                    }

                    sSQL2 = sSQL2 + " UCASE(det_jstatus) ='" + rsc.getStatus1().toUpperCase() + "' ";
                }
                if (st2 > 0) {
                    if (sSQL2.length() > 0) {
                        sSQL2 = sSQL2 + " or ";
                    }

                    sSQL2 = sSQL2 + " UCASE(det_jstatus) ='" + rsc.getStatus2().toUpperCase() + "' ";
                }

                if (sSQL2.length() > 0) {
                    sSQL1 = sqlHeader1 + sSQL1 + "and (" + sSQL2 + ")";
                } else {
                    sSQL1 = sqlHeader1 + sSQL1;
                }
            }

            if (sSQL1.length() > 0) {
                sSQL1 = sSQL1 + " and det_accid = '" + acc_id + "'";
            } else {
                sSQL1 = "where det_accid = '" + acc_id + "'";
            }

            //For PrintLabels

            if (sSQL1.length() > 0) //sql = "select hawbno,"  + newString + " from jobdet_brief "+sSQL1;
            {
                rSQL.setSql("select det_jno," + newString + " from ndi_details " + sSQL1);
            } // sql = "select det_hawbno,"  + newString + " from ndi_details"  + sSQL1;
            else //sql = "select hawbno,"  + newString + " from jobdet_brief ";
            {
                rSQL.setSql("select det_jno," + newString + "  from ndi_details");
            }
            //sql = "select det_hawbno,"  + newString + " from ndi_details "+sSQL1;


            if (rsc.getSortby().length() > 0) {
                rSQL.setSql(rSQL.getSql() + " order by " + rsc.getSortby() + " asc");
            }

            //For CSVExport

            if (sSQL1.length() > 0) //sqlCSV=" select " + newString + " from jobdet_brief" +sSQL1;
            {
                rSQL.setSqlCSV(" select " + newString + " from ndi_details" + sSQL1);
            } else //sqlCSV = " select "  + newString + " from jobdet_brief";
            {
                rSQL.setSqlCSV(" select " + newString + " from ndi_details");
            }

            writeOutputLog("Job details SQL for Account: "+acc_id+" SessionId: "+session_ID+" SQL: "+rSQL.getSql());
            
            conShow.close();
        } catch (Exception e) {
            vShowErrorPageReporting(res, e);
        }

        return rSQL;
    }
       
    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     * @return
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
   
   
    
}
