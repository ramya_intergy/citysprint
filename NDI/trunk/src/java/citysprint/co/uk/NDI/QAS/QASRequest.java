/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package citysprint.co.uk.NDI.QAS;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.*;

/**
 *
 * @author ramyas
 */
public class QASRequest {

    private QASAddress qasInput = new QASAddress();
    List<QASAddress> qasOutput = new ArrayList<QASAddress>();

    public void validateAddress(HttpServletRequest request, HttpServletResponse response) {
        HttpResponse resOutput = null;
        String sUrl = "";
        String sInput = "";
        int iStatusCode = 0;
        String sMessage = "";

        try {
            //In validate address
            qasInput = request.getAttribute("qasInput") == null ? qasInput : (QASAddress) request.getAttribute("qasInput");
            sUrl = request.getAttribute("QASOnDemandURL") == null ? "" : request.getAttribute("QASOnDemandURL").toString();
            String sOutput = getOutputString();
            sInput = getInputString(qasInput);

            URL url = new URL(sUrl + sInput);
            //Invoke Web Service
            //LIVE
            HttpClient client = new DefaultHttpClient();
            
            // if you need any parameters
            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            urlParameters.add(new BasicNameValuePair("company", qasInput.getsCompany()));
            urlParameters.add(new BasicNameValuePair("address1", qasInput.getsAddress1()));
            urlParameters.add(new BasicNameValuePair("address2", qasInput.getsAddress2()));
            urlParameters.add(new BasicNameValuePair("town", qasInput.getsTown()));
            urlParameters.add(new BasicNameValuePair("county", qasInput.getsCounty()));
            urlParameters.add(new BasicNameValuePair("postcode", qasInput.getsPostcode()));
            urlParameters.add(new BasicNameValuePair("threshold", "50"));
            
            String paramString = URLEncodedUtils.format(urlParameters, "utf-8");
            sUrl += paramString;

            HttpGet get = new HttpGet(sUrl);
            get.addHeader("accept", "application/json");
            resOutput = client.execute(get);
            iStatusCode = resOutput.getStatusLine().getStatusCode();
            BufferedReader rd = new BufferedReader(new InputStreamReader(resOutput.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            //LIVE

            //DEVELOPMENT
            //iStatusCode = 200;
            
            if (iStatusCode != 200) {
                //LIVE
                JSONObject error = new JSONObject(result.toString());
                sMessage = error.getString("message");
                request.setAttribute("qasOutput", qasOutput);
                request.setAttribute("message", sMessage);
                request.setAttribute("statuscode", iStatusCode);
            } else {
                //LIVE
                JSONArray jsonOutput = new JSONArray(result.toString());

                //DEVELOPMENT
                //JSONArray jsonOutput = new JSONArray(sOutput);

                for (int i = 0; i < jsonOutput.length(); i++) {
                    QASAddress qasAddress = new QASAddress();
                    JSONObject e = jsonOutput.getJSONObject(i);

                    qasAddress.setsKey(Integer.toString(i));
                    qasAddress.setsCompany(e.getString("company"));
                    qasAddress.setsAddress1(e.getString("address1"));
                    qasAddress.setsAddress2(e.getString("address2"));
                    qasAddress.setsTown(e.getString("town"));
                    qasAddress.setsCounty(e.getString("county"));
                    qasAddress.setsPostcode(e.getString("postcode"));
                    qasAddress.setsCountry(e.getString("country"));
                    qasAddress.setsLatitude(e.getString("latitude"));
                    qasAddress.setsLongitude(e.getString("longitude"));

                    qasOutput.add(qasAddress);
                }

                request.setAttribute("qasOutput", qasOutput);
                request.setAttribute("message", sMessage);
                request.setAttribute("statuscode", iStatusCode);
            }
        } catch (Exception ex) {
            sMessage = ex.getMessage();
            request.setAttribute("message", sMessage);
            request.setAttribute("statuscode", 0);
        }

    }

    public String getInputString(QASAddress qasInput) {
        String sInput = "";

        sInput = sInput + "company=" + qasInput.sCompany + "&";
        sInput = sInput + "address1=" + qasInput.sAddress1 + "&";
        sInput = sInput + "address2=" + qasInput.sAddress2 + "&";
        sInput = sInput + "address3=" + qasInput.sAddress3 + "&";
        sInput = sInput + "town=" + qasInput.sTown + "&";
        sInput = sInput + "county=" + qasInput.sCounty + "&";
        sInput = sInput + "postcode=" + qasInput.sPostcode + "&";
        sInput = sInput + "threshold=50";

        return sInput;
    }

    public String getOutputString(){
        String sOutput = "[{\"company\":\"Citysprint (UK) Ltd\"," +
                "\"address1\":\"58-62 Scrutton Street\","+
                "\"address2\":\"\",\"town\":\"LONDON\",\"county\":\"\",\"country\":\"GBR\","+
                "\"postcode\":\"EC2A 4PH\",\"longitude\":\"-0.081445\",\"latitude\":\"51.523132\","+
                "\"score\":100}"
                + ",{\"company\":\"Dispatch Management Services (UK) Ltd\",\"address1\":\"58-62 Scrutton Street\","+
                "\"address2\":\"\",\"town\":\"LONDON\",\"county\":\"\",\"country\":\"GBR\",\"postcode\":\"EC2A 4PH\","+
                "\"longitude\":\"-0.081445\",\"latitude\":\"51.523132\",\"score\":100},{\"company\":\"Westone Couriers\","+
                "\"address1\":\"58-62 Scrutton Street\",\"address2\":\"\",\"town\":\"LONDON\",\"county\":\"\","+
                "\"country\":\"GBR\",\"postcode\":\"EC2A 4XP\",\"longitude\":\"-0.081445\",\"latitude\":\"51.523132\","+
                "\"score\":100}]";

        return sOutput;
    }
}
