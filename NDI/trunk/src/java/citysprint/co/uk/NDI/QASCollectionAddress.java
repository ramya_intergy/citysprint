/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citysprint.co.uk.NDI;

/**
 *
 * @author ramyas
 */
public class QASCollectionAddress {
       String sCCompany = "", sCContact = "", sCAddress1 = "", sCAddress2 = "", sCTown = "", sCPostcode = "", sCCounty = "", sCLongitude = "", sCLatitude = "", sCTownKey = "";

    public String getSCAddress1() {
        return sCAddress1;
    }

    public void setSCAddress1(String sCAddress1) {
        this.sCAddress1 = sCAddress1;
    }

    public String getSCAddress2() {
        return sCAddress2;
    }

    public void setSCAddress2(String sCAddress2) {
        this.sCAddress2 = sCAddress2;
    }

    public String getSCCompany() {
        return sCCompany;
    }

    public void setSCCompany(String sCCompany) {
        this.sCCompany = sCCompany;
    }

    public String getSCContact() {
        return sCContact;
    }

    public void setSCContact(String sCContact) {
        this.sCContact = sCContact;
    }

    public String getSCCounty() {
        return sCCounty;
    }

    public void setSCCounty(String sCCounty) {
        this.sCCounty = sCCounty;
    }

    public String getSCLatitude() {
        return sCLatitude;
    }

    public void setSCLatitude(String sCLatitude) {
        this.sCLatitude = sCLatitude;
    }

    public String getSCLongitude() {
        return sCLongitude;
    }

    public void setSCLongitude(String sCLongitude) {
        this.sCLongitude = sCLongitude;
    }

    public String getSCPostcode() {
        return sCPostcode;
    }

    public void setSCPostcode(String sCPostcode) {
        this.sCPostcode = sCPostcode;
    }

    public String getSCTown() {
        return sCTown;
    }

    public void setSCTown(String sCTown) {
        this.sCTown = sCTown;
    }

    public String getSCTownKey() {
        return sCTownKey;
    }

    public void setSCTownKey(String sCTownKey) {
        this.sCTownKey = sCTownKey;
    }


}
