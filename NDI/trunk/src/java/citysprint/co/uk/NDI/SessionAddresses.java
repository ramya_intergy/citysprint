package citysprint.co.uk.NDI;

/*
 * SessionAddresses.java
 *
 * Created on 13 August 2008, 12:45
 */

import java.beans.*;
import java.io.Serializable;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.io.*;

/**
 * @author ramyas
 */
public class SessionAddresses extends CommonClass implements Serializable {
    
   private String sCCompany="",sCContact="",sCAddress1="",sCAddress2="",sCAddress3="",sCTown="",sCPostcode="",sCPhone="",sCCounty="",sCInstruct="",sCLongitude="",sCLatitude="",sCCountry="",sCTownKey="", sCResidentialFlag="";
   private String sDCompany="",sDContact="",sDAddress1="",sDAddress2="",sDAddress3="",sDTown="",sDPostcode="",sDPhone="",sDCounty="",sDInstruct="",sDLongitude="",sDLatitude="",sDCountry="",sDTownKey="", sDResidentialFlag="";    
   
   
   public String getCResidentialFlag(){
       return sCResidentialFlag;
   }
   
   public String getDResidentialFlag(){
       return sDResidentialFlag;
   }
    //Company Name
   /**
    *
    * @return
    */
   public String getCCompany() {
         return sCCompany;
    }
    
    //Contact
   /**
    *
    * @return
    */
   public String getCContact() {
         return sCContact;
    }
    
    //Address Line 1
   /**
    *
    * @return
    */
   public String getCCAddress1() {
         return sCAddress1;
    }
    
    //Address Line 2
    /**
     *
     * @return
     */
    public String getCAddress2() {
         return sCAddress2;
    }
    
    //Address Line 3
    /**
     *
     * @return
     */
    public String getCAddress3() {
         return sCAddress3;
    }
    
    //Town
    /**
     *
     * @return
     */
    public String getCTown() {
         return sCTown;
    }
    
    //County
    /**
     *
     * @return
     */
    public String getCCounty() {
         return sCCounty;
    }
    
    //Postcode
    /**
     *
     * @return
     */
    public String getCPostcode() {
         return sCPostcode;
    }
    
    //Country
    /**
     *
     * @return
     */
    public String getCCountry() {
         return sCCountry;
    }
    
    //Instruct
    /**
     *
     * @return
     */
    public String getCInstruct() {
         return sCInstruct;
    }
    
    //Phone
    /**
     *
     * @return
     */
    public String getCPhone() {
         return sCPhone;
    }
    
     //Longitude
    /**
     *
     * @return
     */
    public String getCLongitude() {
         return sCLongitude;
    }
    
     //Latitude
    /**
     *
     * @return
     */
    public String getCLatitude() {
         return sCLatitude;
    }
    
     //TownKey
    /**
     *
     * @return
     */
    public String getCTownKey() {
         return sCTownKey;
    }
    
    //Company Name
    /**
     *
     * @return
     */
    public String getDCompany() {
         return sDCompany;
    }
    
    //Contact
    /**
     *
     * @return
     */
    public String getDContact() {
         return sDContact;
    }
    
    //Address Line 1
    /**
     *
     * @return
     */
    public String getDAddress1() {
         return sDAddress1;
    }
    
    //Address Line 2
    /**
     *
     * @return
     */
    public String getDAddress2() {
         return sDAddress2;
    }
    
    //Address Line 3
    /**
     *
     * @return
     */
    public String getDAddress3() {
         return sDAddress3;
    }
    
    //Town
    /**
     *
     * @return
     */
    public String getDTown() {
         return sDTown;
    }
    
    //County
    /**
     *
     * @return
     */
    public String getDCounty() {
         return sDCounty;
    }
    
    //Postcode
    /**
     *
     * @return
     */
    public String getDPostcode() {
         return sDPostcode;
    }
    
     //Country
    /**
     *
     * @return
     */
    public String getDCountry() {
         return sDCountry;
    }
    
    //Instruct
    /**
     *
     * @return
     */
    public String getDInstruct() {
         return sDInstruct;
    }
    
    //Phone
    /**
     *
     * @return
     */
    public String getDPhone() {
         return sDPhone;
    }
    
    //Longitude
    /**
     *
     * @return
     */
    public String getDLongitude() {
         return sDLongitude;
    }
    
     //Latitude
    /**
     *
     * @return
     */
    public String getDLatitude() {
         return sDLatitude;
    }
    
     //TownKey
    /**
     *
     * @return
     */
    public String getDTownKey() {
         return sDTownKey;
    }
    
    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public void setSessionAddresses(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException
    {
            Connection conDB = null;
            java.sql.Statement stmRead;
            ResultSet rstRead;
           
            //initialize config values
            initializeConfigValues(request,response);
        
            String sSql="Select * from srs_address where srs_sessionid='"+(request.getSession(false).getAttribute("sessionid").toString())+"' and srs_owned_by='"+(request.getSession(false).getAttribute("acc_id").toString())+"'";
                
            try
            {
                
                conDB = cOpenConnection();
                stmRead = conDB.createStatement();
                rstRead = stmRead.executeQuery(sSql);
            
                while (rstRead.next())
                {
                    if(rstRead.getBoolean("srs_collection"))
                    {
                        sCContact = rstRead.getString("srs_contact");
                        sCPhone = rstRead.getString("srs_phone");
                        sCCompany = rstRead.getString("srs_company");
                        sCAddress1 = rstRead.getString("srs_address1");
                        sCAddress2=rstRead.getString("srs_address2");
                        sCAddress3=rstRead.getString("srs_address3");
                        sCTown = rstRead.getString("srs_town");
                        sCCounty=rstRead.getString("srs_county");
                        sCPostcode = rstRead.getString("srs_postcode");
                        sCCountry = rstRead.getString("srs_country");
                        sCInstruct=rstRead.getString("srs_instructions");
                        sCLatitude=rstRead.getString("srs_latitude");
                        sCLongitude=rstRead.getString("srs_longitude");
                        sCTownKey=rstRead.getString("srs_townkey");
                        sCResidentialFlag=rstRead.getString("srs_residential_flag");
                    }
                    else
                    {
                        sDContact = rstRead.getString("srs_contact");
                        sDPhone = rstRead.getString("srs_phone");
                        sDCompany = rstRead.getString("srs_company");
                        sDAddress1 = rstRead.getString("srs_address1");
                        sDAddress2=rstRead.getString("srs_address2");
                        sDAddress3=rstRead.getString("srs_address3");
                        sDTown = rstRead.getString("srs_town");
                        sDCounty=rstRead.getString("srs_county");
                        sDPostcode = rstRead.getString("srs_postcode");
                        sDCountry = rstRead.getString("srs_country");
                        sDInstruct=rstRead.getString("srs_instructions");
                        sDLatitude=rstRead.getString("srs_latitude");
                        sDLongitude=rstRead.getString("srs_longitude");
                        sDTownKey=rstRead.getString("srs_townkey");
                        sDResidentialFlag=rstRead.getString("srs_residential_flag");
                    }
                }

                vCloseConnection(conDB);
            }
            catch(Exception ex)
            {
                vShowErrorPage(response,ex);
            }
    }
}
