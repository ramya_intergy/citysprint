package citysprint.co.uk.NDI;

/*
 * ThankYouPage.java
 *
 * Created on 7 June 2008, 13:49
 */
import citysprint.co.uk.NDI.CountryInfo;
import citysprint.co.uk.NDI.ServiceInfo;
import citysprint.co.uk.NDI.CreditCardDetails;
import citysprint.co.uk.NDI.LoginInfo;
import citysprint.co.uk.NDI.JobInfo;
import citysprint.co.uk.NDI.BookingInfo;
import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import com.qas.proweb.*;
import com.qas.proweb.servlet.*;
import java.text.SimpleDateFormat;
import java.util.Vector;
import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author ramyas
 * @version
 */
public class ThankYouPage extends CommonClass implements Serializable {

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //ServletOutputStream strmResponse = response.getOutputStream();


        try {
            if (isLogin(request)) {

                //initialize config values
                initializeConfigValues(request, response);

                LoadPage(request, response);
            } else {
                writeOutputLog("Session expired redirecting to index page");
                response.sendRedirect("index.jsp");
            }
        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void LoadPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession validSession = request.getSession(false);


        LoginInfo login = new LoginInfo();
        BookingInfo booking = new BookingInfo();
        CreditCardDetails ccdetails = new CreditCardDetails();
        SessionAddresses srsaddress = new SessionAddresses();
        ServiceInfo service = new ServiceInfo();
        JobInfo job = new JobInfo();
        CountryInfo country = new CountryInfo();

        String[][] getServicesArray = null;
        String[][] getBookingArray = null;
        //String sJobNo="";

        //set login,collection,delivery address fields and other booking info fields by processing the request object
        login.parseLogin((String[][]) validSession.getAttribute("loginArray"), request);
        booking.setBookingInfo(request, response, login);
        ccdetails.setCreditCardDetails(request);
        srsaddress.setSessionAddresses(request, response);
        country.parseCountries((String[][]) validSession.getAttribute("getCountriesArray"));

        getBookingArray = makeJobBooking(request, response);

        if (getBookingArray == null) {
            job.setError("Invalid booking parameters. Please re-login and try again");
        } else {
            job.parseBooking(getBookingArray);
        }

        ServletOutputStream strmResponse = response.getOutputStream();
        response.setContentType("text/html");

        strmResponse.println("<html>");
        strmResponse.println("  <title>CitySprint</title>");
        strmResponse.println("<head>");
        strmResponse.println("<META HTTP-EQUIV=\"CACHE-CONTROL\" CONTENT=\"NO-CACHE\">");
        strmResponse.println("<link media=\"screen\" href=\"CSS/NextDayCss.css\" type=\"text/css\" rel=\"stylesheet\">");

        strmResponse.println("<script type=\"text/javascript\">");
        strmResponse.println("preload1 = new Image();");
        strmResponse.println("preload2 = new Image();");
        strmResponse.println("preload3 = new Image();");
        strmResponse.println("preload4 = new Image();");
        strmResponse.println("preload5 = new Image();");
        strmResponse.println("preload6 = new Image();");

        strmResponse.println("preload1.src = \"images/booking_active.gif\";");
        strmResponse.println("preload2.src = \"images/booking_default.gif\";");
        strmResponse.println("preload3.src = \"images/reporting_active.gif\";");
        strmResponse.println("preload4.src = \"images/reporting_default.gif\";");
        strmResponse.println("preload5.src = \"images/tracking_active_410.gif\";");
        strmResponse.println("preload6.src = \"images/tracking_default_410.gif\";");
        strmResponse.println("</script>");
        strmResponse.println("<style>");
        strmResponse.println("html { overflow-x: hidden;overflow-y: auto;  }");
        strmResponse.println("</style>");
        strmResponse.println("</head>");

        strmResponse.println("<body background=\"#BCBCBC\" style=\"width:895px;\">");

        strmResponse.println("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding-left:4px;padding-right:4px\">");
//        strmResponse.println("<tr>");
//        strmResponse.println("<td id=\"PageHeader\" style=\"Font-size:12px;\">");
//	strmResponse.println("<b>BOOKING : NextDay & International Courier<b>");
//        strmResponse.println("<label style=\"padding-left:450px;text-align:right;color:white;\">Help |  <a href=\"Logout\" style=\"color:white;font-weight:bold;\">Logout</a></label>");
//        strmResponse.println("</td>");
//        //strmResponse.println("<td style=\"text-align:right\" style=\"Font-size:12px;\"></td>");
//        strmResponse.println("</tr>");
        strmResponse.println("<tr>");
        strmResponse.println("<td colspan=\"2\" >");
        strmResponse.println("<img src=\"images/booking_active.gif\" id=\"imgTab1\" style=\"padding-bottom:0px;padding-top:1pxpadding-left:0px;\"><img src=\"images/reporting_default.gif\" id=\"imgTab2\" onclick=\"javascript:window.location='Reporting.jsp?t=" + new java.util.Date() + "';\" style=\"padding-bottom:0px;padding-top:1pxpadding-left:0px;cursor:pointer\"><img src=\"images/tracking_default_410.gif\" id=\"imgTab3\" onclick=\"javascript:window.location='Tracking?t=" + new java.util.Date() + "';\" style=\"padding-bottom:0px;padding-top:1pxpadding-left:0px;cursor:pointer\">");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");
        strmResponse.println("<tr>");
        strmResponse.println("<td width=\"100%\" valign=\"top\" colspan=\"2\"><br>");
        strmResponse.println("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">");
        strmResponse.println("<tr>");

        if (job.getError().length() == 0) {
            strmResponse.println("<td width=\"50%\" ><p style=\"font-size:14pt;color:#ad1421;line-height: 16pt;padding-left:40px\">Thank you for Booking with CitySprint.</p></td>");
        } else {
            strmResponse.println("<td width=\"50%\" ><p style=\"font-size:14pt;color:#ad1421;line-height: 16pt;padding-left:40px\">" + job.getError() + "</p></td>");
        }

        strmResponse.println("<td width=\"50%\" style=\"padding-left:160px;line-height:12pt;\"><input type=\"radio\" checked style=\"font-size:10pt;\" name=\"rdDetails\" id=\"rdSame\"> Same Details &nbsp;&nbsp;<input type=\"button\" value=\"New Booking\" id=\"btn\" onclick=\"javascript:newBookingClick();\"> <br> <input type=\"radio\" style=\"font-size:10pt;\" name=\"rdDetails\" id=\"rdNew\"> New Details </td>");
        strmResponse.println("</tr>");
        strmResponse.println("<tr>");

        if (job.getError().length() == 0) {
            strmResponse.println("<td width=\"50%\" ><p style=\"font-size:14pt;color:#191970;font-weight:bolder;line-height: 16pt;padding-left:40px\">Your Consignment Number is " + job.getSBHAWBNO() + "</p></td>");
        } else {
            strmResponse.println("<td width=\"50%\" ><p style=\"font-size:14pt;color:#191970;font-weight:bolder;line-height: 16pt;padding-left:40px\"></p></td>");
        }

        strmResponse.println("<td>");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");
        strmResponse.println("<tr>");

//        if(ff)
//        {
//            strmResponse.println("<td width=\"50%\">");
//            strmResponse.println("<br>");
//
//            if(job.getError().length()==0)
//            {
//                strmResponse.println("<input type=\"button\" value=\"Print Paper Consignment Note\" id=\"btnlarge\" onclick=\"javascript:printClick('PrintConsignment');\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
//                strmResponse.println("&nbsp;<input type=\"button\" value=\"Print Thermal Labels\" id=\"btnlarge\" onclick=\"javascript:printClick('PrintLabels');\"></td>");
//                strmResponse.println("<td width=\"50%\"><br><input type=\"button\" value=\"Print Commercial Invoice\" id=\"btnlarge\" onclick=\"javascript:printClick('PrintInvoice');\">");
//            }
//            strmResponse.println("</td>");
//        }
//        else if(ie)
//        {
        strmResponse.println("<td style=\"padding-left:40px;line-height:12pt;\" colspan=\"2\">");
        strmResponse.println("<br>");

        if (job.getError().length() == 0) {
            strmResponse.println("<input type=\"button\" value=\"Print Paper Consignment Note\" id=\"btnlarge\" onclick=\"javascript:printClick('PrintConsignment');\">&nbsp;");
            strmResponse.println("<input type=\"button\" value=\"Print Commercial Invoice\" id=\"btnlarge\" onclick=\"javascript:printClick('PrintInvoice');\">&nbsp;");
            strmResponse.println("<input type=\"button\" value=\"Print Thermal Labels\" id=\"btnlarge\" onclick=\"javascript:printClick('PrintLabels');\">");
        }
        strmResponse.println("</td>");
//        }
//        else
//        {
//             strmResponse.println("<td style=\"float:right;padding-left:40px\" colspan=\"2\">");
//            strmResponse.println("<br>");
//
//            if(job.getError().length()==0)
//            {
//                strmResponse.println("<input type=\"button\" value=\"Print Paper Consignment Note\" id=\"btnlarge\" onclick=\"javascript:printClick('PrintConsignment');\">&nbsp;");
//                strmResponse.println("<input type=\"button\" value=\"Print Commercial Invoice\" id=\"btnlarge\" onclick=\"javascript:printClick('PrintInvoice');\">&nbsp;");
//                strmResponse.println("<input type=\"button\" value=\"Print Thermal Labels\" id=\"btnlarge\" onclick=\"javascript:printClick('PrintLabels');\">");
//            }
//            strmResponse.println("</td>");
//        }

        strmResponse.println("</tr>");
        strmResponse.println("<tr>");
        strmResponse.println("  <td  style=\"padding-left:160px;line-height:12pt;\">&nbsp;</td>");
        strmResponse.println("  <td style=\"padding-left:160px;line-height:12pt;\">&nbsp;</td>");
        strmResponse.println("</tr>");

        strmResponse.println("<tr>");
        strmResponse.println("<br>");
        strmResponse.println("<td><br><p style=\"font-size:12pt;color:#005CAC;line-height: 12pt;\" >Your Booking Details</p><br><br></td>");
        strmResponse.println("<td style=\"padding-left:160px;line-height:12pt;\">&nbsp;</td>");
        strmResponse.println("</tr>");
        strmResponse.println("</td>");
        strmResponse.println("</tr>");
        strmResponse.println("<tr id=\"trBooking\">");
        strmResponse.println("<td colspan=\"2\">");

        strmResponse.println("    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"box\" width=\"100%\">");


        strmResponse.println("    <tr style=\"padding-top:4px;padding-bottom:4px;\"><td id=\"boxtitle\" colspan=\"6\">BOOKING DETAILS</td></tr>");
        strmResponse.println("    <tr>");
        strmResponse.println("     <td id=\"label\">");
        if (login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("V")) {
            if (login.isEYFlag()) {
                strmResponse.println(login.getEy_Department());
            } else {
                strmResponse.println("Department");
            }
        }
        strmResponse.println("      </td>");
        strmResponse.println("      <td>");
        if (login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("V")) {
            strmResponse.println(booking.getBDept().toUpperCase() + "");
        }
        strmResponse.println("      </td> ");
        strmResponse.println("      <td id=\"label\">");

        if (login.getRefFlag().equals("R") || login.getRefFlag().equals("Y") || login.getRefFlag().equals("V")) {
            if (login.isEYFlag()) {
                strmResponse.println(login.getEy_Reference());
            } else {
                strmResponse.println("Reference");
            }
        }
        strmResponse.println("      </td>");
        strmResponse.println("      <td>");
        if (login.getRefFlag().equals("R") || login.getRefFlag().equals("Y") || login.getRefFlag().equals("V")) {
            strmResponse.println(booking.getBRef().toUpperCase() + "");
        }
        strmResponse.println("      </td> ");
        strmResponse.println("      <td></td>");
        strmResponse.println("   </tr>");
        strmResponse.println("    <tr>");

        strmResponse.println("      <td id=\"label\">");
        if (login.isEYFlag()) {
            strmResponse.println(login.getEy_YourName());
        } else {
            strmResponse.println("Caller");
        }
        strmResponse.println("      </td>");
        strmResponse.println("      <td>" + booking.getBCaller().toUpperCase() + "");
        strmResponse.println("      </td> ");
        strmResponse.println("      <td id=\"label\">");
        if (login.isEYFlag()) {
            strmResponse.println(login.getEy_Phone());
        } else {
            strmResponse.println("Reference 2");
        }
        strmResponse.println("      </td>");
        strmResponse.println("      <td>" + booking.getBPhone() + "");
        strmResponse.println("      </td> ");
        strmResponse.println("      <td id=\"label\">");
        strmResponse.println("          Consignment Number");
        strmResponse.println("      </td>");
        strmResponse.println("      <td>");
        strmResponse.println("    " + job.getSBHAWBNO() + "");
        strmResponse.println("    </td>");
        strmResponse.println("    </tr>");
        strmResponse.println("    </table>");

        strmResponse.println("</td></tr>");
        strmResponse.println("<tr >");
        strmResponse.println("      <td width=\"50%\">");
        strmResponse.println("          <br> ");
        strmResponse.println("          <table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"99%\">");
        strmResponse.println("              <tr><td id=\"boxtitle\" colspan=\"6\">COLLECTION ADDRESS</td></tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Name</td>");
        strmResponse.println("                  <td width=\"25%\" >" + srsaddress.getCCompany().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Address 1</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getCCAddress1().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width\"25%\">Address 2</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getCAddress2().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Address 3</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getCAddress3().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td width=\"25%\" id=\"label\">Town</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getCTown().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td width=\"25%\" id=\"label\">County</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getCCounty().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");

        strmResponse.println("                  <td width=\"25%\" id=\"label\">Postcode</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getCPostcode().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Country</td>");
        strmResponse.println("                  <td width=\"25%\">" + (country.getCnVector().elementAt(country.getCyVector().indexOf(srsaddress.getCCountry().toUpperCase())).toString()) + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Contact</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getCContact().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Phone</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getCPhone().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");

        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Instructions</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getCInstruct().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");

        strmResponse.println("              <tr>");
//        strmResponse.println("                  <td id=\"label\" width=\"25%\">Residential Flag</td>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Residential</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getCResidentialFlag() + "</td>");
        strmResponse.println("              </tr>");


        strmResponse.println("          </table>");
        strmResponse.println("      </td>");
        strmResponse.println("      <td width=\"50%\">");
        strmResponse.println("          <br>");
        strmResponse.println("          <table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"99%\">");
        strmResponse.println("              <tr><td id=\"boxtitle\" colspan=\"6\">DELIVERY ADDRESS</td></tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Name</td>");
        strmResponse.println("                  <td width=\"25%\" >" + srsaddress.getDCompany().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Address 1</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getDAddress1().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Address 2</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getDAddress2().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Address 3</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getDAddress3().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td width=\"25%\" id=\"label\">Town</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getDTown().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td width=\"25%\" id=\"label\">County</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getDCounty().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");

        strmResponse.println("                  <td width=\"25%\" id=\"label\">Postcode</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getDPostcode().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Country</td>");
        strmResponse.println("                  <td width=\"25%\">" + (country.getCnVector().elementAt(country.getCyVector().indexOf(srsaddress.getDCountry().toUpperCase())).toString()) + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Contact</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getDContact().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Phone</td>");
        strmResponse.println("                 <td width=\"25%\">" + srsaddress.getDPhone().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");
        strmResponse.println("     ");
        strmResponse.println("              <tr>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Instructions</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getDInstruct().toUpperCase() + "</td>");
        strmResponse.println("              </tr>");

        strmResponse.println("              <tr>");
//        strmResponse.println("                  <td id=\"label\" width=\"25%\">Residential Flag</td>");
        strmResponse.println("                  <td id=\"label\" width=\"25%\">Residential</td>");
        strmResponse.println("                  <td width=\"25%\">" + srsaddress.getDResidentialFlag() + "</td>");
        strmResponse.println("              </tr>");

        strmResponse.println("          </table>");
        strmResponse.println("      </td>");
        strmResponse.println("  </tr>");
        strmResponse.println(" <tr>");




        strmResponse.println("<tr id=\"trCollectTime\">");
        strmResponse.println("<td width=\"50%\">");
        strmResponse.println("<br>");
        strmResponse.println("<table id=\"box\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"99%\">");
        strmResponse.println("<tr>");
        strmResponse.println("<td  id=\"boxtitle\" colspan=\"6\">");
        strmResponse.println("CONSIGNMENT DETAILS");
        strmResponse.println("</td></tr>");
        strmResponse.println("<tr><td id=\"label\" width=\"15%\">");
        strmResponse.println("Product Type");
        strmResponse.println("</td><td width=\"25%\">");
        strmResponse.println(booking.getProductType().toUpperCase());
        strmResponse.println("</td></tr>");



//        strmResponse.println("<tr><td id=\"label\" width=\"15%\">");
//        strmResponse.println("Parcel Type");
////        if(ff)
////            strmResponse.println("</td><td width=\"23%\">");
////        else
//        strmResponse.println("</td><td width=\"25%\">");
//        if (booking.getsParcelName() != null){
//            strmResponse.println(booking.getsParcelName().toUpperCase());
//        }else{
//            strmResponse.println("SELECT");
//        }
//        strmResponse.println("</td></tr>");


        strmResponse.println("<tr><td id=\"label\" width=\"25%\">");
        strmResponse.println("Total No of Pieces");
        strmResponse.println("</td><td width=\"25%\">");
        strmResponse.println(booking.getTotalPieces());
        strmResponse.println("</td></tr>");
        strmResponse.println("<tr><td id=\"label\" width=\"25%\">");
        strmResponse.println("Total Weight");
        strmResponse.println("</td><td width=\"25%\">");
        strmResponse.println(booking.getTotalWeight());
        strmResponse.println("</td></tr>");
        strmResponse.println("<tr><td id=\"label\" width=\"25%\">");
        strmResponse.println("Total Volumetric Weight");
        strmResponse.println("</td><td width=\"25%\">");
        strmResponse.println(booking.getTotalVolWeight());
        strmResponse.println("</td></tr>");
        strmResponse.println("<tr><td width=\"100%\" colspan=\"2\">");
        if (booking.getProductType().equals("NDOX") ){ //|| booking.getsParcelName()!= null ) {
            strmResponse.println("<TABLE border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"99%\" align=\"center\">  ");
            strmResponse.println("    <tr>");
            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:16px\">No Of Items</td>");

//            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:18px\">Parcel Type</td>");
//            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:18px\">Item Weight</td>");

            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:18px\">Length(cm)</td>");
            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:18px\">Width(cm)</td>");
            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:18px\">Height(cm)</td>");
            strmResponse.println("    </tr>");

            try {
                Connection conDB = null;
                Statement stmRead;
                ResultSet rstRead;

                String sSql = "Select pro_noitems,pro_length,pro_width,pro_height "//, "//pro_ptype, "
//                        + "pro_iweight  "
                        + "from pro_product where pro_sessionid='" + validSession.getAttribute("sessionid").toString() + "'";

                try {

                    conDB = cOpenConnection();
                    stmRead = conDB.createStatement();
                    rstRead = stmRead.executeQuery(sSql);

                    while (rstRead.next()) {
                        strmResponse.println("    <tr style=\"text-align:right\">");
                        strmResponse.println("        <td style=\"text-align:center\">" + rstRead.getInt("pro_noitems") + "</td>");
//                        strmResponse.println("        <td style=\"text-align:center\">" + rstRead.getString("pro_ptype") + "</td>");
//                        strmResponse.println("        <td style=\"text-align:center\">" + rstRead.getFloat("pro_iweight") + "</td>");

                        strmResponse.println("        <td style=\"text-align:center\">" + rstRead.getInt("pro_length") + "</td>");
                        strmResponse.println("        <td style=\"text-align:center\">" + rstRead.getInt("pro_width") + "</td>");
                        strmResponse.println("        <td style=\"text-align:center\">" + rstRead.getInt("pro_height") + "</td>");
                        strmResponse.println("    </tr>");

                    }
                } finally {
                    conDB.close();
                }
            } catch (Exception ex) {
                vShowErrorPage(response, ex);
            }

            strmResponse.println("</table>  ");
        }
        strmResponse.println("</td>");
        strmResponse.println("</tr>");

        strmResponse.println("</td></tr></table>");

        strmResponse.println("</td>");
        strmResponse.println("<td><br>");

        if (booking.getProductType().equals("NDOX")) {
            strmResponse.println("<table id=\"box\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"99%\">");
            strmResponse.println("<tr>");
            strmResponse.println("<td id=\"boxtitle\" colspan=\"2\">");
            strmResponse.println("CONTENTS");
            strmResponse.println("</td></tr>");
            strmResponse.println("<tr><td width=\"100%\" colspan=\"2\">");
            strmResponse.println("<TABLE border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"99%\" align=\"center\" style=\"margin-top:1px; margin-bottom:1px;\"> ");
            strmResponse.println("    <tr>");
            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:2px;text-align:left\">Commodity</td>");
            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:2px;text-align:right\">Quantity</td>");
            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:2px;text-align:right\">Total Value</td>");
            strmResponse.println("        <td id=\"boxtitle\" style=\"padding-left:2px;text-align:right\">Indemnity</td>");
            strmResponse.println("    </tr>");

            Connection conDB = null;
            Statement stmRead;
            ResultSet rstRead;

            String sSql = "Select com_description,com_quantity,com_value,com_indemnity from com_commodity where com_sessionid='" + validSession.getAttribute("sessionid").toString() + "'";

            try {

                conDB = cOpenConnection();
                stmRead = conDB.createStatement();
                rstRead = stmRead.executeQuery(sSql);

                while (rstRead.next()) {
                    strmResponse.println("    <tr style=\"text-align:right\">");
                    strmResponse.println("        <td style=\"text-align:left\">" + rstRead.getString("com_description").toUpperCase() + "</td>");
                    strmResponse.println("        <td style=\"text-align:right\">" + rstRead.getInt("com_quantity") + "</td>");
                    strmResponse.println("        <td style=\"text-align:right\">" + rstRead.getDouble("com_value") + "</td>");
                    strmResponse.println("        <td style=\"text-align:right\">" + (rstRead.getString("com_indemnity").equals("0") ? "NO" : "YES") + "</td>");
                    strmResponse.println("    </tr>");
                }

                vCloseConnection(conDB);
            } catch (Exception ex) {
                vShowErrorPage(response, ex);
            }

            strmResponse.println("</table>    ");
            strmResponse.println("</td>");
            strmResponse.println("</tr>");

            strmResponse.println("</td></tr></table>");
        }

        strmResponse.println("</td></tr>");


        strmResponse.println("  <tr>");
        strmResponse.println("      <td width=\"50%\">");
        strmResponse.println("          <br>");
        strmResponse.println("          <table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"99%\">");
        strmResponse.println("              <tr><td id=\"boxtitle\" colspan=\"6\">SERVICE DETAILS</td></tr>");


        getServicesArray = (String[][]) validSession.getAttribute("getServicesArray");

        if (getServicesArray != null) {
            service.parseServices(getServicesArray);

            for (int i = 0; i < service.getSvVector().size(); i++) {
                //if(svcodeVector.elementAt(i).toString().trim().equals("U9"))
                if (ccdetails.getServices().equals(service.getSvcodeVector().elementAt(i))) {
                    strmResponse.println("              <tr>");
                    strmResponse.println("                  <td id=\"label\" width=\"25%\">Service Option</td>");
                    strmResponse.println("                  <td width=\"25%\">" + service.getSvVector().elementAt(i) + "</td>");

                    strmResponse.println("              </tr>");
                    strmResponse.println("              <tr> ");
                    strmResponse.println("<td id=\"label\">Collection By<br>Date/Time</td>");
                    strmResponse.println("                  <td >" + service.getCdVector().elementAt(i) + " " + service.getCmVector().elementAt(i) + "</td>");
                    strmResponse.println("              </tr>");
                    strmResponse.println("              <tr>");
                    strmResponse.println("<td id=\"label\">Scheduled Delivery By<br>Date/Time</td>");
                    strmResponse.println("                  <td >" + service.getDdVector().elementAt(i) + " " + service.getDmVector().elementAt(i) + "</td>");
                    strmResponse.println("              </tr>");
                }
            }
        }

        if (login.isEmailAccountFlag() || login.isSmsAccountFlag()) {
            strmResponse.println("              <tr><td id=\"boxtitle\" colspan=\"2\">TRACKING NOTIFICATIONS</td></tr>");
            if (login.isEmailAccountFlag()) {
                strmResponse.println("              <tr><td id=\"label\" width=\"25%\">Email Notification</td><td >" + getTrackingDetails(request, response, "E", "T").toUpperCase() + "</td></tr>");
            }
            strmResponse.println("              <tr><td id=\"label\" width=\"25%\"></td><td></td></tr>");
            if (login.isSmsAccountFlag()) {
                strmResponse.println("              <tr><td id=\"label\" width=\"25%\">SMS Notification</td><td >" + getTrackingDetails(request, response, "S", "T").toUpperCase() + "</td></tr>");
            }
        }

        strmResponse.println("          </table>");

        strmResponse.println("      </td>");
        strmResponse.println("      <td width=\"50%\">");
        strmResponse.println("            <br>");

        if (login.getCreditCardFlag().equals("F")) {
            strmResponse.println("      <table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" id=\"box\" width=\"99%\">");
            strmResponse.println("          <tr><td id=\"boxtitle\" colspan=\"6\">PAYMENT DETAILS</td></tr>");
            strmResponse.println("          <tr>");
            strmResponse.println("                  <td id=\"label\" width=\"25%\">Credit Card Type</td>");
            strmResponse.println("                  <td width=\"25%\" colspan=\"2\">");
            strmResponse.println("                      " + ccdetails.getCCType().toUpperCase() + "");
            strmResponse.println("                  </td>");
            strmResponse.println("              </tr>");
            strmResponse.println("             <tr>");
            strmResponse.println("                  <td id=\"label\">Credit Card Number</td>");
            strmResponse.println("                  <td colspan=\"2\">" + ccdetails.getCardNo() + "</td>");
            strmResponse.println("              </tr>");
            strmResponse.println("              <tr>");
            strmResponse.println("                  <td id=\"label\">Card Holders Name</td>");
            strmResponse.println("                  <td colspan=\"2\">" + ccdetails.getCCName().toUpperCase() + "</td>");
            strmResponse.println("              </tr>");
            strmResponse.println("              <tr>");
            strmResponse.println("                  <td id=\"label\">Expiry Date (MM/YY)</td>");
            strmResponse.println("                  <td colspan=\"2\">" + ccdetails.getExpiryMonth() + "/" + ccdetails.getExpiryYear() + "");
            strmResponse.println("                  </td>");
            strmResponse.println("              </tr>");
            strmResponse.println("              <tr>");
            strmResponse.println("                  <td id=\"label\">Start Date (Switch/Solo)</td>");
            if (ccdetails.getStartDay().length() > 0 && ccdetails.getStartMonth().length() > 0) {
                strmResponse.println("              <td colspan=\"2\">" + ccdetails.getStartDay() + "/" + ccdetails.getStartMonth() + "");
            } else {
                strmResponse.println("              <td colspan=\"2\">");
            }

            strmResponse.println("                  </td>");
            strmResponse.println("              </tr>");
            strmResponse.println("              <tr>");
            strmResponse.println("                  <td id=\"label\">Issue Number (Switch/Solo)</td>");
            strmResponse.println("                  <td colspan=\"2\">" + ccdetails.getIssueNo() + "</td>");
            strmResponse.println("              </tr>");
            if (login.isAvsAddressMandatoryFlag()) {
                strmResponse.println("              <tr>");
                strmResponse.println("                  <td id=\"label\">Address1</td>");
                strmResponse.println("                  <td colspan=\"2\">" + ccdetails.getsAVSAddress1().toUpperCase() + "</td>");
                strmResponse.println("              </tr>");
                strmResponse.println("              <tr>");
                strmResponse.println("                  <td id=\"label\">Address2</td>");
                strmResponse.println("                  <td colspan=\"2\">" + ccdetails.getsAVSAddress2().toUpperCase() + "</td>");
                strmResponse.println("              </tr>");
                strmResponse.println("              <tr>");
                strmResponse.println("                  <td id=\"label\">Address3</td>");
                strmResponse.println("                  <td colspan=\"2\">" + ccdetails.getsAVSAddress3().toUpperCase() + "</td>");
                strmResponse.println("              </tr>");
                strmResponse.println("              <tr>");
                strmResponse.println("                  <td id=\"label\">Address4</td>");
                strmResponse.println("                  <td colspan=\"2\">" + ccdetails.getsAVSAddress4().toUpperCase() + "</td>");
                strmResponse.println("              </tr>");
            }
            strmResponse.println("              <tr>");
            strmResponse.println("                  <td id=\"label\">Postcode</td>");
            strmResponse.println("                  <td colspan=\"2\">" + ccdetails.getPostcode().toUpperCase() + "</td>");
            strmResponse.println("              </tr>");
            strmResponse.println("              <tr>");
            strmResponse.println("                  <td id=\"label\">Security Code</td>");
            strmResponse.println("                  <td colspan=\"2\">" + ccdetails.getSecurityCode().toUpperCase() + "</td>");
            strmResponse.println("              </tr>");
            strmResponse.println("              <tr>");
            strmResponse.println("                  <td id=\"label\">Email Address</td>");
            strmResponse.println("                  <td colspan=\"2\">" + ccdetails.getCCEmail().toUpperCase() + "</td>");
            strmResponse.println("              </tr>");
            strmResponse.println("      </table>");

        }
        strmResponse.println("      </td>");
        strmResponse.println("  </tr>");

        strmResponse.println("</table>");

        strmResponse.println("<form name=\"AmendForm\" method=\"post\" action=\"" + Constants.srvEnterConsignment + "\">");
        strmResponse.println("<input type=hidden name=\"CCompanyName\" value=\"" + srsaddress.getCCompany().toUpperCase() + "\">");
        strmResponse.println("<input type=hidden name=\"CAddress1\" value=\"" + srsaddress.getCCAddress1().toUpperCase() + "\">");
        strmResponse.println("<input type=hidden name=\"CAddress2\" value=\"" + srsaddress.getCAddress2().toUpperCase() + "\">");
        strmResponse.println("<input type=hidden name=\"CAddress3\" value=\"" + srsaddress.getCAddress3().toUpperCase() + "\">");
        strmResponse.println("<input type=hidden name=\"CTown\" value=\"" + srsaddress.getCTown().toUpperCase() + "\">");
        strmResponse.println("<input type=hidden name=\"CCounty\" value=\"" + srsaddress.getCCounty().toUpperCase() + "\">");
        strmResponse.println("<input type=hidden name=\"CPostcode\" value=\"" + srsaddress.getCPostcode().toUpperCase() + "\">");
        strmResponse.println("<input type=hidden name=\"CCountry\" value=\"" + srsaddress.getCCountry().toUpperCase() + "\">");
        strmResponse.println("<input type=hidden name=\"CContactName\" value=\"" + srsaddress.getCContact().toUpperCase() + "\">");
        strmResponse.println("<input type=hidden name=\"CPhone\" value=\"" + srsaddress.getCPhone().toUpperCase() + "\">");
        strmResponse.println("<input type=hidden name=\"CInstruct\" value=\"" + srsaddress.getCInstruct().toUpperCase() + "\">");
        strmResponse.println("<input type=hidden name=\"CLatitude\" value=\"" + srsaddress.getCLatitude() + "\">");
        strmResponse.println("<input type=hidden name=\"CLongitude\" value=\"" + srsaddress.getCLongitude() + "\">");

//        strmResponse.println("<input type=hidden name=\"DCompanyName\" value=\""+srsaddress.getDCompany().toUpperCase()+"\">");
//        strmResponse.println("<input type=hidden name=\"DAddress1\" value=\""+srsaddress.getDAddress1().toUpperCase()+"\">");
//        strmResponse.println("<input type=hidden name=\"DAddress2\" value=\""+srsaddress.getDAddress2().toUpperCase()+"\">");
//        strmResponse.println("<input type=hidden name=\"DAddress3\" value=\""+srsaddress.getDAddress3().toUpperCase()+"\">");
//        strmResponse.println("<input type=hidden name=\"DTown\" value=\""+srsaddress.getDTown().toUpperCase()+"\">");
//        strmResponse.println("<input type=hidden name=\"DCounty\" value=\""+srsaddress.getDCounty().toUpperCase()+"\">");
//        strmResponse.println("<input type=hidden name=\"DPostcode\" value=\""+srsaddress.getDPostcode().toUpperCase()+"\">");
//        strmResponse.println("<input type=hidden name=\"DCountry\" value=\""+srsaddress.getDCountry().toUpperCase()+"\">");
//        strmResponse.println("<input type=hidden name=\"DContactName\" value=\""+srsaddress.getDContact().toUpperCase()+"\">");
//        strmResponse.println("<input type=hidden name=\"DPhone\" value=\""+srsaddress.getDPhone().toUpperCase()+"\">");
//        strmResponse.println("<input type=hidden name=\"DInstruct\" value=\""+srsaddress.getDInstruct().toUpperCase()+"\">");
        strmResponse.println("<input type=hidden name=\"BDept\" value=\"" + booking.getBDept() + "\">");
        strmResponse.println("<input type=hidden name=\"BCaller\" value=\"" + booking.getBCaller() + "\">");
        strmResponse.println("<input type=hidden name=\"BPhone\" value=\"" + booking.getBPhone() + "\">");
        strmResponse.println("<input type=hidden name=\"BRef\" value=\"" + booking.getBRef() + "\">");
        // strmResponse.println("<input type=hidden name=\"BHAWBNO\" value=\""+booking.getBHAWBNO()+"\">");

        strmResponse.println("<input type=hidden name=\"rdCollect\" value=\"" + booking.getRdCollect() + "\">");
        strmResponse.println("<input type=hidden name=\"CollectDate\" value=\"" + booking.getCollectDate() + "\">");
        strmResponse.println("<input type=hidden name=\"CollectHH\" value=\"" + booking.getCollectHH() + "\">");
        strmResponse.println("<input type=hidden name=\"CollectMM\" value=\"" + booking.getCollectMM() + "\">");
        strmResponse.println("<input type=hidden name=\"chkPickupBefore\" value=\"" + booking.getChkPickupBefore() + "\">");
        strmResponse.println("<input type=hidden name=\"PickupBeforeHH\" value=\"" + booking.getPickupBeforeHH() + "\">");
        strmResponse.println("<input type=hidden name=\"PickupBeforeMM\" value=\"" + booking.getPickupBeforeMM() + "\">");

        strmResponse.println("<input type=hidden name=\"CResidentialFlag\" value=\"" + srsaddress.getCResidentialFlag() + "\">");
//        strmResponse.println("<input type=hidden id=\"ParcelNameId\" name=\"ParcelNameId\" value=\"" + booking.getsParcelNameId() + "\">");
//        strmResponse.println("<input type=hidden id=\"ParcelName\" name=\"ParcelName\" value=\"" + booking.getsParcelName() + "\">");
        strmResponse.println("<input type=hidden id=\"itemsRowCount\" name=\"itemsRowCount\" value=\"" + booking.getItemsRowCount() + "\">");

        strmResponse.println("<input type=hidden name=\"" + Constants.SACTION + "\" value=\"Clear\">");
        strmResponse.println("</form>");

        strmResponse.println("<form name=\"NewForm\" method=\"post\" action=\"" + Constants.srvEnterConsignment + "\">");
        strmResponse.println("<input type=hidden name=\"" + Constants.SACTION + "\" value=\"Clear\">");
        strmResponse.println("</form>");
        strmResponse.println("<form name=\"formlabels\" method=\"post\" action=\"JobAudit\">");
        strmResponse.println("<input type=\"hidden\" name=\"chkid\" value=\"" + job.getSJobNo() + ",\">");
        strmResponse.println("<input type=\"hidden\" name=\"sAction\" value=\"\">");
        strmResponse.println("</form>");
        strmResponse.println(" <form name=\"formInvoice\" method=\"post\" action=\"ReasonForExport.jsp\" target=\"popupwinInvoice\">");
        strmResponse.println(" <input type=\"hidden\" name=\"chkid\" value=\"" + job.getSJobNo() + ",\">");
        strmResponse.println(" <input type=\"hidden\" name=\"sAction\" value=\"PrintInvoice\">");
        strmResponse.println("</form>");

        strmResponse.println("<script language=javascript>");

        strmResponse.println("function newBookingClick()");
        strmResponse.println("{");
        strmResponse.println("var rdSame=document.getElementById(\"rdSame\");");
        strmResponse.println("var rdNew=document.getElementById(\"rdNew\");");
        //strmResponse.println("alert(rdSame.checked);");
        //strmResponse.println("alert(rdNew.checked);");
        strmResponse.println("if(rdSame.checked){");
        
//        strmResponse.println("document.getElementById(\"ParcelNameId\").value = '' ;");
//        strmResponse.println("document.getElementById(\"ParcelName\").value = '' ;");
        strmResponse.println("document.getElementById(\"itemsRowCount\").value = '0' ;");
        
        strmResponse.println("   document.AmendForm.submit();");
        strmResponse.println("}");
        strmResponse.println("if(rdNew.checked)");
        strmResponse.println("   document.NewForm.submit();");
        strmResponse.println("}");

        strmResponse.println("var winInvoice=null;");
        strmResponse.println("function printClick(sAction)");
        strmResponse.println("{");
        //strmResponse.println("alert(sAction);");
        strmResponse.println("if(sAction==\"PrintInvoice\")");
        strmResponse.println("{");
        strmResponse.println("if((winInvoice == null) || (winInvoice.closed == true))");
        strmResponse.println("    winInvoice=window.open('blank.html','popupwinInvoice','resizable=no,scrollbars=no,width=350,height=250');");
        strmResponse.println("else");
        strmResponse.println("    winInvoice.focus();");
        strmResponse.println("document.formInvoice.submit();");
        strmResponse.println("}");
        strmResponse.println("else");
        strmResponse.println("{");
        strmResponse.println("document.formlabels.sAction.value=sAction;");
        strmResponse.println("document.formlabels.submit();");
        strmResponse.println("}");
        strmResponse.println("}");

        strmResponse.println("</script>");
        strmResponse.println("</body>");
        strmResponse.println("</html>");

    }

//      public void getSessionAddresses(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException
//      {
//            Connection conDB = null;
//            Statement stmRead;
//            ResultSet rstRead;
//           
//            String sSql="Select * from srs_address where srs_sessionid='"+sSessionID+"'";
//
//            try
//            {
//                
//                conDB = cOpenConnection();
//                stmRead = conDB.createStatement();
//                rstRead = stmRead.executeQuery(sSql);
//            
//                while (rstRead.next())
//                {
//                    if(rstRead.getBoolean("srs_collection"))
//                    {
//                        sCContact = rstRead.getString("srs_contact");
//                        sCPhone = rstRead.getString("srs_phone");
//                        sCCompany = rstRead.getString("srs_company");
//                        sCAddress1 = rstRead.getString("srs_address1");
//                        sCAddress2=rstRead.getString("srs_address2");
//                        sCAddress3=rstRead.getString("srs_address3");
//                        sCTown = rstRead.getString("srs_town");
//                        sCCounty=rstRead.getString("srs_county");
//                        sCPostcode = rstRead.getString("srs_postcode");
//                        sCCountry = rstRead.getString("srs_country");
//                        sCInstruct=rstRead.getString("srs_instructions");
//                        sCLatitude=rstRead.getString("srs_latitude");
//                        sCLongitude=rstRead.getString("srs_longitude");
//                        sCTownKey=rstRead.getString("srs_townkey");
//                    }
//                    else
//                    {
//                        sDContact = rstRead.getString("srs_contact");
//                        sDPhone = rstRead.getString("srs_phone");
//                        sDCompany = rstRead.getString("srs_company");
//                        sDAddress1 = rstRead.getString("srs_address1");
//                        sDAddress2=rstRead.getString("srs_address2");
//                        sDAddress3=rstRead.getString("srs_address3");
//                        sDTown = rstRead.getString("srs_town");
//                        sDCounty=rstRead.getString("srs_county");
//                        sDPostcode = rstRead.getString("srs_postcode");
//                        sDCountry = rstRead.getString("srs_country");
//                        sDInstruct=rstRead.getString("srs_instructions");
//                        sDLatitude=rstRead.getString("srs_latitude");
//                        sDLongitude=rstRead.getString("srs_longitude");
//                        sDTownKey=rstRead.getString("srs_townkey");
//                    }
//                }
//            }
//            catch(Exception ex)
//            {
//                vShowErrorPage(response,ex);
//            }
//    }
    /**
     *
     * @param request
     * @param response
     * @param type
     * @param action
     * @return
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public String getTrackingDetails(HttpServletRequest request, HttpServletResponse response, String type, String action) throws ServletException, IOException {
        String[][] sTrackingEmail1 = new String[5][5];
        String[][] sTrackingSMS1 = new String[5][5];
        String[][] sEmpty = new String[1][1];
        sEmpty[0][0] = "";
        String sTrackingEmail = "", sTrackingSMS = "", sBTrackingEmail = "", sBTrackingSMS = "";
        //sEmpty[0][1]="";
        String result = "";

        String temp = "";
        try {
            HttpSession validSession = request.getSession(false);

            if (validSession == null) {
            } else {

                if (validSession.getAttribute("sTrackingEmail") != null) {
                    sTrackingEmail1 = (String[][]) validSession.getAttribute("sTrackingEmail");

                    for (int i = 0; i < 5; i++) {
                        temp = sTrackingEmail1[i][0] == null ? "" : sTrackingEmail1[i][0];
                        if (temp.length() > 0) {
                            sTrackingEmail = sTrackingEmail + temp + ",<br>";
                            sBTrackingEmail = sBTrackingEmail + "&EM=" + encStr(temp) + "|" + encStr(sTrackingEmail1[i][1]);
                        }
                    }

                    if (sTrackingEmail.length() > 0 && sTrackingEmail.lastIndexOf(',') != -1) {
                        sTrackingEmail = sTrackingEmail.substring(0, sTrackingEmail.lastIndexOf(',')) + sTrackingEmail.substring(sTrackingEmail.lastIndexOf(',') + 1, sTrackingEmail.length());
                    }
                }

                if (validSession.getAttribute("sTrackingSMS") != null) {
                    sTrackingSMS1 = (String[][]) validSession.getAttribute("sTrackingSMS");

                    for (int i = 0; i < 5; i++) {
                        temp = sTrackingSMS1[i][0] == null ? "" : sTrackingSMS1[i][0];
                        if (temp.length() > 0) {
                            sTrackingSMS = sTrackingSMS + temp + ",<br>";
                            sBTrackingSMS = sBTrackingSMS + "&SM=" + encStr(temp) + "|" + encStr(sTrackingSMS1[i][1]);
                        }
                    }

                    if (sTrackingSMS.length() > 0 && sTrackingSMS.lastIndexOf(',') != -1) {
                        sTrackingSMS = sTrackingSMS.substring(0, sTrackingSMS.lastIndexOf(',')) + sTrackingSMS.substring(sTrackingSMS.lastIndexOf(',') + 1, sTrackingSMS.length());
                    }
                }

            }
        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }

        if (type.equals("E") && action.equals("T")) {
            result = sTrackingEmail;
        }
        if (type.equals("S") && action.equals("T")) {
            result = sTrackingSMS;
        }
        if (type.equals("E") && action.equals("B")) {
            result = sBTrackingEmail;
        }
        if (type.equals("S") && action.equals("B")) {
            result = sBTrackingSMS;
        }

        return result;
    }

    /**
     *
     * @param request
     * @param response
     * @return
     * @throws java.net.UnknownHostException
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     */
    public String[][] makeJobBooking(HttpServletRequest request, HttpServletResponse response) throws UnknownHostException, IOException, ServletException {
        java.util.Date d = new java.util.Date();
        String timeStamp = d.toString();
        HttpSession validSession = request.getSession(false);

        LoginInfo login = new LoginInfo();
        BookingInfo booking = new BookingInfo();
        CreditCardDetails ccdetails = new CreditCardDetails();
        SessionAddresses srsaddress = new SessionAddresses();
        ServiceInfo service = new ServiceInfo();
        String[][] getServicesArray = null;
        String[][] getBookingArray = null;

        //set login,collection,delivery address fields and other booking info fields by processing the request object
        login.parseLogin((String[][]) validSession.getAttribute("loginArray"), request);
        booking.setBookingInfo(request, response, login);
        ccdetails.setCreditCardDetails(request);
        srsaddress.setSessionAddresses(request, response);

        //Build services API input parameters
        StringBuffer sInput = new StringBuffer();

        sInput.append("CK=" + encStr(request.getSession(false).getAttribute("acc_id").toString()));                                            // Customer Key                 
        sInput.append("&PW=" + encStr(request.getSession(false).getAttribute("pwd").toString()));                                              // Password                     
        sInput.append("&TS=" + encStr(timeStamp));                                        // Transaction Stamp            
        sInput.append("&DT=" + encStr(booking.getBDept()));                               // Department client selected   
        sInput.append("&RE=" + encStr(booking.getBRef()));                                // Client Reference Field
        sInput.append("&CA=" + encStr(booking.getBCaller()));                             // Clients Name
        sInput.append("&SV=" + encStr(ccdetails.getServices().trim()));                         // Service Option Code
        sInput.append("&PH=" + encStr(booking.getBPhone()));                              // Clients phone number
        sInput.append("&AB=" + encStr(booking.getRdCollect().equals("Now") ? "N" : "Y"));            // Indicates if a booking made ahead of time

        //User Input Collect Date & Time should not be passed to booking
        //Service guide date & time only should be passed
        //Ramya 29/01/2009 as per SP11423 Functional V1.18.doc reverting back to the below values.
        SimpleDateFormat sDateFormat = new SimpleDateFormat("dd/MM/yy");
        SimpleDateFormat sTimeFormat = new SimpleDateFormat("HH:mm");
        String sDate = sDateFormat.format(d);
        String sTime = sTimeFormat.format(d);

        if (booking.getRdCollect().equals("Now")) {
            sInput.append("&RO=" + encStr(sDate));                                        // Pickup Date
            sInput.append("&RA=" + encStr(sTime));                                        // Pickup Time
        } else {
            String sDate1 = (booking.getCollectDate().equals("") ? sDate : booking.getCollectDate());
            sInput.append("&RO=" + encStr(sDate1));                                       // Pickup Date
            sInput.append("&RA=" + encStr(booking.getCollectHH() + ":" + booking.getCollectMM()));    // Pickup Time
        }

        getServicesArray = (String[][]) validSession.getAttribute("getServicesArray");

        if (getServicesArray != null) {
            service.parseServices(getServicesArray);

            for (int i = 0; i < service.getSvVector().size(); i++) {

                if (ccdetails.getServices().equals(service.getSvcodeVector().elementAt(i))) {
                    //Ramya 29/01/2009 as per SP11423 Functional V1.18.doc
                    //Collection Date passed by the service guide should be sent back to booking in "CL" tag
                    //RO and RA will revert back to user entered values
//                    sInput.append("&RO="+encStr(service.getCdVector().elementAt(i).toString()));
//                    sInput.append("&RA="+encStr(service.getCmVector().elementAt(i).toString()));
                    sInput.append("&CL=" + encStr(service.getCdVector().elementAt(i).toString()));
                    sInput.append("&CB=" + encStr(service.getCmVector().elementAt(i).toString()));
                    sInput.append("&AG=" + encStr(service.getAgVector().elementAt(i).toString()));
                    sInput.append("&AT=" + encStr(service.getAtVector().elementAt(i).toString()));
                    sInput.append("&DD=" + encStr(service.getDdVector().elementAt(i).toString()));
                    sInput.append("&DM=" + encStr(service.getDmVector().elementAt(i).toString()));
                }
            }
        }

        if (booking.getChkPickupBefore().equals("1")) //check if closesat checkbox ticked
        {
            sInput.append("&CT=" + encStr((booking.getPickupBeforeHH() + ":" + booking.getPickupBeforeMM())));
        } else {
            sInput.append("&CT=");
        }

        sInput.append("&PC=" + encStr(srsaddress.getCCompany()));                                        // Pickup Company
        sInput.append("&PA=" + encStr(srsaddress.getCCAddress1()));                                      // Pickup Street Address
        sInput.append("&P2=" + encStr(srsaddress.getCAddress2()));                                       // Pickup Street Address 2
        sInput.append("&P3=" + encStr(srsaddress.getCAddress3()));                                       // Pickup Street Address 3
        sInput.append("&PK=" + encStr(srsaddress.getCTownKey()));                                        // Pickup Town Key
        sInput.append("&PS=" + encStr(srsaddress.getCTown()));                                           // Pickup Town	
        sInput.append("&PO=" + encStr(srsaddress.getCCounty()));                                         // Pickup County   
        sInput.append("&PP=" + encStr(srsaddress.getCPostcode()));                                       // Pickup Postcode  
        sInput.append("&PY=" + encStr(srsaddress.getCCountry()));                                        // Pickup Country Code

        // added by Adnan on 28 Aug 2013 against Residential Flag change
        sInput.append("&PF=" + encStr(srsaddress.getCResidentialFlag()));                                // Pickup Residential Flag

        sInput.append("&PG=" + encStr(srsaddress.getCLongitude()));                                      // Pickup Longitude
        sInput.append("&PU=" + encStr(srsaddress.getCLatitude()));                                       // Pickup Latitude
        sInput.append("&PX=" + encStr(srsaddress.getCContact()));                                        // Pickup Contact
        sInput.append("&PE=" + encStr(srsaddress.getCPhone()));                                          // Pickup Contact Phone
        sInput.append("&PI=" + encStr(srsaddress.getCInstruct()));                                       // Pickup Instrutions
        sInput.append("&TP=" + encStr(booking.getProductType()));                         // Product Type

//        String sParcelIndx = booking.getsParcelName();
////            int iParcelIndx = -1;
//        if (sParcelIndx == null) {
//            sParcelIndx = (String) request.getParameter("ParcelName");
//        }
//        String parcelName4NDIService = "";
//            if (sParcelIndx != null)
//                parcelName4NDIService = sParcelIndx;
//            
//        sInput.append("&PT=" + encStr(parcelName4NDIService));    
////        sInput.append("&PT=" + encStr(booking.getsParcelName()));

        if (booking.getProductType().equals("NDOX") ){//&& sParcelIndx == null) {
            sInput.append("&PN=" + encStr(totalCalculate(request, response)));
        } else {
            sInput.append("&PN=" + encStr(booking.getTotalPieces()));                     // Total No.of.Pieces                                 // Total No.of.Pieces
        }
        sInput.append("&TW=" + encStr(booking.getTotalWeight()));                                 // Total Weight


        if (booking.getProductType().equals("NDOX")) {
            sInput.append(getItems(request,response));                                      // get Item tags ^NI ^HE ^PL ^PD 
//            sInput.append(getItems(request, response, sParcelIndx != null ? true : false));
            sInput.append(getCommodityValues(request, response));                            // get Commodity tags ^CC ^CD ^CQ ^CV ^CI
        }
//        else if (sParcelIndx != null){
//            sInput.append(getItems(request, response, true)); 
//        }

        if (getTrackingDetails(request, response, "E", "B").length() > 0) {
            sInput.append("&ER=Y");                                                     // Email Required
            sInput.append(getTrackingDetails(request, response, "E", "B"));                // Email Address (Notification)
        }

        sInput.append(getTrackingDetails(request, response, "S", "B"));                           // SMS Notification
        sInput.append("&DC=" + encStr(srsaddress.getDCompany()));                                        // Delivery Company       
        sInput.append("&DA=" + encStr(srsaddress.getDAddress1()));                                       // Delivery Street Address
        sInput.append("&D2=" + encStr(srsaddress.getDAddress2()));                                       // Delivery Street Address 2
        sInput.append("&D3=" + encStr(srsaddress.getDAddress3()));                                       // Delivery Street Address 3
        sInput.append("&DK=" + encStr(srsaddress.getDTownKey()));                                        // Delivery Town	Key
        sInput.append("&DS=" + encStr(srsaddress.getDTown()));                                           // Delivery Town	
        sInput.append("&DO=" + encStr(srsaddress.getDCounty()));                                         // Delivery County  
        sInput.append("&DP=" + encStr(srsaddress.getDPostcode()));                                       // Delivery Postcode
        sInput.append("&DY=" + encStr(srsaddress.getDCountry()));                                        // Delivery Country Code        

        // added by Adnan on 28 Aug 2013 against Residential Flag change
        sInput.append("&DF=" + encStr(srsaddress.getDResidentialFlag()));                                // Delivery Residential Flag

        sInput.append("&DL=" + encStr(srsaddress.getDLongitude()));                                      // Delivery Longitude
        sInput.append("&DU=" + encStr(srsaddress.getDLatitude()));                                       // Delivery Latitude
        sInput.append("&DX=" + encStr(srsaddress.getDContact()));                                        // Delivery Contact
        sInput.append("&DH=" + encStr(srsaddress.getDPhone()));                                          // Delivery Contact Phone
        sInput.append("&DI=" + encStr(srsaddress.getDInstruct()));                                       // Delivery Instructions

        if (login.getCreditCardFlag().equals("F")) {
            sInput.append("&C1=" + encStrLower(ccdetails.getCCType()));                                // Credit Card Type
            //sInput.append("&C2="+encStrLower(encryptCreditCard(ccdetails.getCardNo())));                                // Credit Card Number
            sInput.append("&C2=" + encStrLower(encryptCreditCard(validSession.getAttribute("CreditCardNo").toString())));                                // Credit Card Number
            sInput.append("&C3=" + encStrLower(ccdetails.getCCName()));                                // Card Holders Name
            sInput.append("&C4=" + encStrLower(ccdetails.getExpiryMonth() + ccdetails.getExpiryYear()));   // Expiry Date

            if (ccdetails.getCCType().equals("Switch") || ccdetails.getCCType().equals("Solo")) {
                sInput.append("&C5=" + encStrLower(ccdetails.getStartDay() + ccdetails.getStartMonth()));      // Swtich Start Date
                sInput.append("&C6=" + encStrLower(ccdetails.getIssueNo()));                                     // Swtich Issue Number
            } else {
                sInput.append("&C5=" + encStrLower("    "));      // Swtich Start Date
                sInput.append("&C6=" + encStrLower("  "));
            }

            sInput.append("&C7=" + encStrLower(ccdetails.getPostcode()));                              // Credit Card Postcode
            sInput.append("&C8=" + encStrLower(ccdetails.getSecurityCode()));                          // Credit Card Security Code
            sInput.append("&C9=" + encStrLower(ccdetails.getCCEmail()));                       // Credit Card Email

            if (login.isAvsAddressMandatoryFlag()) {
                sInput.append("&A1=" + encStrLower(ccdetails.getsAVSAddress1()));
                sInput.append("&A2=" + encStrLower(ccdetails.getsAVSAddress2()));
                sInput.append("&A3=" + encStrLower(ccdetails.getsAVSAddress3()));
                sInput.append("&A4=" + encStrLower(ccdetails.getsAVSAddress4()));
            }
        }

        sInput.append("&HN=" + encStr(booking.getBHAWBNO()));                             // HAWB Number
        sInput.append("&DV=" + encStr(booking.getShipperVAT()));                                  // Shipper VAT

        if (booking.getProductType().length() == 0 || srsaddress.getCCountry().length() == 0 || srsaddress.getDCountry().length() == 0 || (Integer.parseInt(totalCalculate(request, response)) == 0 && booking.getProductType().equals("NDOX"))
                || ((booking.getTotalPieces().equals("0") || booking.getTotalPieces().equals("")) && booking.getProductType().equals("DOX"))) {
            getBookingArray = null;
        } else {
            URL url;

            if (environ.equals("PROD")) {
                url = new URL(scriptsurl + bdl.getString("bookScript") + "?" + sInput.toString());
            } else {
                url = new URL(bdl.getString("devhost") + "booking.txt");
            }
            //writeOutputLog("Input to BookingAPI:"+url.toString());

            System.out.println("Input to NDI_Booking:" + sInput);
            writeOutputLog("Input to NDI_Booking:" + url.toString());

            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setUseCaches(false);
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String s;
            String outputLines = "";
            while ((s = br.readLine()) != null) {
                outputLines = outputLines + s;
            }

            writeOutputLog("Ouput from NDI_Booking:" + outputLines);

            br.close();
            outputLines = outputLines + "^ZZ";
            getBookingArray = parseOutput(outputLines);
        }
        return getBookingArray;
    }

    /**
     *
     * @param request
     * @param response
     * @return
     * @throws java.net.UnknownHostException
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     */
    public String getItems(HttpServletRequest request, HttpServletResponse response /*, boolean isParcelType */) throws UnknownHostException, IOException, ServletException {

        String sql = "Select pro_noitems,pro_height,pro_length,pro_width " //, "//pro_ptype, "
//                + "pro_iweight "
                + " from pro_product where pro_sessionid='" + (request.getSession(false).getAttribute("sessionid").toString()) + "'";
        String sItemTags = "";

        Connection conDB = null;
        Statement stmRead;
        ResultSet rstRead;
        try {

            conDB = cOpenConnection();
            stmRead = conDB.createStatement();
            rstRead = stmRead.executeQuery(sql);

            while (rstRead.next()) {
                sItemTags = sItemTags + "&NI=" + rstRead.getInt("pro_noitems");

//                sItemTags = sItemTags + "&WI=" + rstRead.getFloat("pro_iweight");

//                if (!isParcelType) {
                /**
                 * Following line is commented by Adnan on 1-DEC-2104 against CR-Remove Item weight
                 */   
//                    sItemTags = sItemTags + "&WI=" + rstRead.getFloat("pro_iweight");
//                } else {
//                    sItemTags = sItemTags + "&WI=0";
//                }
//                sItemTags=sItemTags+"&PT="+rstRead.getString("pro_ptype");
//                String parcelType = rstRead.getString("pro_ptype");
//                System.out.println("PARCEL TYPE in ThankYouPage b4 validation == " + parcelType);
//                if (parcelType != null && !parcelType.trim().equalsIgnoreCase("0") && !parcelType.trim().equalsIgnoreCase("SELECT")){
//                    System.out.println("PARCEL TYPE in ThankYouPage b4 appending to API call == " + parcelType);
//                    sItemTags = sItemTags + "&PT=" + parcelType;
//                }
                sItemTags = sItemTags + "&HE=" + rstRead.getInt("pro_height");
                sItemTags = sItemTags + "&PL=" + rstRead.getInt("pro_length");
                sItemTags = sItemTags + "&PD=" + rstRead.getInt("pro_width");
            }
            vCloseConnection(conDB);
        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }

        if (sItemTags.length() == 0) {
//            sItemTags = "&NI=&WI=&PT=&HE=&PL=&PD=";          
             sItemTags = "&NI=&HE=&PL=&PD=";
        }

        return sItemTags;
    }

    /**
     *
     * @param request
     * @param response
     * @return
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    public String totalCalculate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sTotalPieces = "";

        try {

            String sSQL = "";

            sSQL = "select SUM(pro_NoItems) as totPieces from pro_product where pro_sessionid='" + (request.getSession(false).getAttribute("sessionid").toString()) + "'";

            Connection conDB = null;
            java.sql.Statement stmRead;
            ResultSet rstRead;

            conDB = cOpenConnection();
            stmRead = conDB.createStatement();
            rstRead = stmRead.executeQuery(sSQL);

            while (rstRead.next()) {
                sTotalPieces = rstRead.getString("totPieces") == null ? "0" : rstRead.getString("totPieces");
            }

            vCloseConnection(conDB);
        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }

        return sTotalPieces;
    }

    /**
     *
     * @param request
     * @param response
     * @return
     * @throws java.net.UnknownHostException
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     */
    public String getCommodityValues(HttpServletRequest request, HttpServletResponse response) throws UnknownHostException, IOException, ServletException {

        String sql = "Select com_code, com_description, com_quantity, com_value, com_indemnity  from com_commodity where com_sessionid='" + (request.getSession(false).getAttribute("sessionid").toString()) + "'";
        String sCommodityTags = "";

        Connection conDB = null;
        Statement stmRead;
        ResultSet rstRead;
        try {

            conDB = cOpenConnection();
            stmRead = conDB.createStatement();
            rstRead = stmRead.executeQuery(sql);

            while (rstRead.next()) {
                sCommodityTags = sCommodityTags + "&CC=" + encStr(rstRead.getString("com_code"));
                sCommodityTags = sCommodityTags + "&CD=" + encStr(rstRead.getString("com_description"));
                sCommodityTags = sCommodityTags + "&CQ=" + rstRead.getDouble("com_quantity");
                sCommodityTags = sCommodityTags + "&CV=" + rstRead.getInt("com_value");

                if (rstRead.getInt("com_Indemnity") == 0) {
                    sCommodityTags = sCommodityTags + "&CI=N";
                } else {
                    sCommodityTags = sCommodityTags + "&CI=Y";
                }
            }

            vCloseConnection(conDB);
        } catch (Exception ex) {
            vShowErrorPage(response, ex);
        }

        if (sCommodityTags.length() == 0) {
            sCommodityTags = "&CC=&CD=&CQ=&CV=";
        }

        return sCommodityTags;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
