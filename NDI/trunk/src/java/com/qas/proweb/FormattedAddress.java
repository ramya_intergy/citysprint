/* ----------------------------------------------------------------------------
 * QuickAddress Pro Web > (c) QAS Ltd > www.qas.com
 * 
 * Common Classes > FormattedAddress.java
 * Formatted address details
 * ----------------------------------------------------------------------------
 */
package com.qas.proweb;

import java.io.Serializable;
import com.qas.proweb.soap.QAAddressType;
import com.qas.proweb.soap.AddressLineType;

/**
 *  Wrapper class to encapsulate data associated with a final formatted address, namely
 * an array of AddressLines plus some flags.
 */
public class FormattedAddress implements Serializable
{
	// ------------------------------------------------------------------------
	// private data
	// ------------------------------------------------------------------------
	private AddressLine[]   m_AddressLines;
	private boolean         m_IsOverflow;
	private boolean         m_IsTruncated;

	// ------------------------------------------------------------------------
	// public methods
	// ------------------------------------------------------------------------
	/** construct instance from SOAP layer object */
	public FormattedAddress(QAAddressType t)
	{
		m_IsOverflow = t.isOverflow();
		m_IsTruncated = t.isTruncated();

		AddressLineType[] aLines = t.getAddressLine();
		// we must have lines in an address so aLines should never be null
		int iSize = aLines.length;
		if (iSize > 0)
		{
			m_AddressLines = new AddressLine[iSize];
			for (int i=0; i < iSize; i++)
			{
				m_AddressLines[i] = new AddressLine(aLines[i]);
			}
		}
	}

	/** Returns the array of individual formatted address line objects */
	public AddressLine[] getAddressLines()
	{
		return m_AddressLines;
	}

	/** Flag that indicates that there were not enough layout lines to contain
	 * all of the address line, and that some elements had to overflow onto other
	 * lines.  If this is <code>true</code>, then the integrator should either
	 * add more output address lines in the specified layout,
	 * or specify larger widths (server configuration). */
	public boolean isOverflow()
	{
		return m_IsOverflow;
	}

	/** Flag that indicates that some of the address lines were too short to accommodate
	 * all of the formatted address, and so truncation has occurred. If this is <code>true</code>,
	 * then you should either add more output address lines in the specified layout,
	 * or specify larger widths (server configuration). */
	public boolean isTruncated()
	{
		return m_IsTruncated;
	}
}
