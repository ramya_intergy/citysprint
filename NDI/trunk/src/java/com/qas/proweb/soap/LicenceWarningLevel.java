/**
 * LicenceWarningLevel.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class LicenceWarningLevel implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected LicenceWarningLevel(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _None = "None";
    public static final java.lang.String _DataExpiring = "DataExpiring";
    public static final java.lang.String _LicenceExpiring = "LicenceExpiring";
    public static final java.lang.String _ClicksLow = "ClicksLow";
    public static final java.lang.String _Evaluation = "Evaluation";
    public static final java.lang.String _NoClicks = "NoClicks";
    public static final java.lang.String _DataExpired = "DataExpired";
    public static final java.lang.String _EvalLicenceExpired = "EvalLicenceExpired";
    public static final java.lang.String _FullLicenceExpired = "FullLicenceExpired";
    public static final java.lang.String _LicenceNotFound = "LicenceNotFound";
    public static final java.lang.String _DataUnreadable = "DataUnreadable";
    public static final LicenceWarningLevel None = new LicenceWarningLevel(_None);
    public static final LicenceWarningLevel DataExpiring = new LicenceWarningLevel(_DataExpiring);
    public static final LicenceWarningLevel LicenceExpiring = new LicenceWarningLevel(_LicenceExpiring);
    public static final LicenceWarningLevel ClicksLow = new LicenceWarningLevel(_ClicksLow);
    public static final LicenceWarningLevel Evaluation = new LicenceWarningLevel(_Evaluation);
    public static final LicenceWarningLevel NoClicks = new LicenceWarningLevel(_NoClicks);
    public static final LicenceWarningLevel DataExpired = new LicenceWarningLevel(_DataExpired);
    public static final LicenceWarningLevel EvalLicenceExpired = new LicenceWarningLevel(_EvalLicenceExpired);
    public static final LicenceWarningLevel FullLicenceExpired = new LicenceWarningLevel(_FullLicenceExpired);
    public static final LicenceWarningLevel LicenceNotFound = new LicenceWarningLevel(_LicenceNotFound);
    public static final LicenceWarningLevel DataUnreadable = new LicenceWarningLevel(_DataUnreadable);
    public java.lang.String getValue() { return _value_;}
    public static LicenceWarningLevel fromValue(java.lang.String value)
          throws java.lang.IllegalStateException {
        LicenceWarningLevel enum1 = (LicenceWarningLevel)
            _table_.get(value);
        if (enum1==null) throw new java.lang.IllegalStateException();
        return enum1;
    }
    public static LicenceWarningLevel fromString(java.lang.String value)
          throws java.lang.IllegalStateException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
}
