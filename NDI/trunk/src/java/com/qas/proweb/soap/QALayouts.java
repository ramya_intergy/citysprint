/**
 * QALayouts.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class QALayouts  implements java.io.Serializable {
    private com.qas.proweb.soap.QALayout[] layout;

    public QALayouts() {
    }

    public com.qas.proweb.soap.QALayout[] getLayout() {
        return layout;
    }

    public void setLayout(com.qas.proweb.soap.QALayout[] layout) {
        this.layout = layout;
    }

    public com.qas.proweb.soap.QALayout getLayout(int i) {
        return layout[i];
    }

    public void setLayout(int i, com.qas.proweb.soap.QALayout value) {
        this.layout[i] = value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QALayouts)) return false;
        QALayouts other = (QALayouts) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((layout==null && other.getLayout()==null) || 
             (layout!=null &&
              java.util.Arrays.equals(layout, other.getLayout())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLayout() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLayout());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLayout(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QALayouts.class);

    static {
        org.apache.axis.description.FieldDesc field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("layout");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Layout"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
