/**
 * QALicensedSet.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class QALicensedSet  implements java.io.Serializable {
    private java.lang.String ID;
    private java.lang.String description;
    private java.lang.String copyright;
    private java.lang.String version;
    private java.lang.String baseCountry;
    private java.lang.String status;
    private java.lang.String server;
    private com.qas.proweb.soap.LicenceWarningLevel warningLevel;
    private org.apache.axis.types.NonNegativeInteger daysLeft;
    private org.apache.axis.types.NonNegativeInteger dataDaysLeft;
    private org.apache.axis.types.NonNegativeInteger licenceDaysLeft;

    public QALicensedSet() {
    }

    public java.lang.String getID() {
        return ID;
    }

    public void setID(java.lang.String ID) {
        this.ID = ID;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public java.lang.String getCopyright() {
        return copyright;
    }

    public void setCopyright(java.lang.String copyright) {
        this.copyright = copyright;
    }

    public java.lang.String getVersion() {
        return version;
    }

    public void setVersion(java.lang.String version) {
        this.version = version;
    }

    public java.lang.String getBaseCountry() {
        return baseCountry;
    }

    public void setBaseCountry(java.lang.String baseCountry) {
        this.baseCountry = baseCountry;
    }

    public java.lang.String getStatus() {
        return status;
    }

    public void setStatus(java.lang.String status) {
        this.status = status;
    }

    public java.lang.String getServer() {
        return server;
    }

    public void setServer(java.lang.String server) {
        this.server = server;
    }

    public com.qas.proweb.soap.LicenceWarningLevel getWarningLevel() {
        return warningLevel;
    }

    public void setWarningLevel(com.qas.proweb.soap.LicenceWarningLevel warningLevel) {
        this.warningLevel = warningLevel;
    }

    public org.apache.axis.types.NonNegativeInteger getDaysLeft() {
        return daysLeft;
    }

    public void setDaysLeft(org.apache.axis.types.NonNegativeInteger daysLeft) {
        this.daysLeft = daysLeft;
    }

    public org.apache.axis.types.NonNegativeInteger getDataDaysLeft() {
        return dataDaysLeft;
    }

    public void setDataDaysLeft(org.apache.axis.types.NonNegativeInteger dataDaysLeft) {
        this.dataDaysLeft = dataDaysLeft;
    }

    public org.apache.axis.types.NonNegativeInteger getLicenceDaysLeft() {
        return licenceDaysLeft;
    }

    public void setLicenceDaysLeft(org.apache.axis.types.NonNegativeInteger licenceDaysLeft) {
        this.licenceDaysLeft = licenceDaysLeft;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QALicensedSet)) return false;
        QALicensedSet other = (QALicensedSet) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((ID==null && other.getID()==null) || 
             (ID!=null &&
              ID.equals(other.getID()))) &&
            ((description==null && other.getDescription()==null) || 
             (description!=null &&
              description.equals(other.getDescription()))) &&
            ((copyright==null && other.getCopyright()==null) || 
             (copyright!=null &&
              copyright.equals(other.getCopyright()))) &&
            ((version==null && other.getVersion()==null) || 
             (version!=null &&
              version.equals(other.getVersion()))) &&
            ((baseCountry==null && other.getBaseCountry()==null) || 
             (baseCountry!=null &&
              baseCountry.equals(other.getBaseCountry()))) &&
            ((status==null && other.getStatus()==null) || 
             (status!=null &&
              status.equals(other.getStatus()))) &&
            ((server==null && other.getServer()==null) || 
             (server!=null &&
              server.equals(other.getServer()))) &&
            ((warningLevel==null && other.getWarningLevel()==null) || 
             (warningLevel!=null &&
              warningLevel.equals(other.getWarningLevel()))) &&
            ((daysLeft==null && other.getDaysLeft()==null) || 
             (daysLeft!=null &&
              daysLeft.equals(other.getDaysLeft()))) &&
            ((dataDaysLeft==null && other.getDataDaysLeft()==null) || 
             (dataDaysLeft!=null &&
              dataDaysLeft.equals(other.getDataDaysLeft()))) &&
            ((licenceDaysLeft==null && other.getLicenceDaysLeft()==null) || 
             (licenceDaysLeft!=null &&
              licenceDaysLeft.equals(other.getLicenceDaysLeft())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getID() != null) {
            _hashCode += getID().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getCopyright() != null) {
            _hashCode += getCopyright().hashCode();
        }
        if (getVersion() != null) {
            _hashCode += getVersion().hashCode();
        }
        if (getBaseCountry() != null) {
            _hashCode += getBaseCountry().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getServer() != null) {
            _hashCode += getServer().hashCode();
        }
        if (getWarningLevel() != null) {
            _hashCode += getWarningLevel().hashCode();
        }
        if (getDaysLeft() != null) {
            _hashCode += getDaysLeft().hashCode();
        }
        if (getDataDaysLeft() != null) {
            _hashCode += getDataDaysLeft().hashCode();
        }
        if (getLicenceDaysLeft() != null) {
            _hashCode += getLicenceDaysLeft().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QALicensedSet.class);

    static {
        org.apache.axis.description.FieldDesc field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("ID");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "ID"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("description");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Description"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("copyright");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Copyright"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("version");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Version"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("baseCountry");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "BaseCountry"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("status");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Status"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("server");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Server"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("warningLevel");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "WarningLevel"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "LicenceWarningLevel"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("daysLeft");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "DaysLeft"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "nonNegativeInteger"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("dataDaysLeft");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "DataDaysLeft"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "nonNegativeInteger"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("licenceDaysLeft");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "LicenceDaysLeft"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "nonNegativeInteger"));
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
