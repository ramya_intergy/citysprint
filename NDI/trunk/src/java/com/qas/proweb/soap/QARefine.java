/**
 * QARefine.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.qas.proweb.soap;

public class QARefine  implements java.io.Serializable {
    private java.lang.String moniker;
    private java.lang.String refinement;
    private com.qas.proweb.soap.QAConfigType QAConfig;
    private com.qas.proweb.soap.ThresholdType threshold;  // attribute
    private com.qas.proweb.soap.TimeoutType timeout;  // attribute

    public QARefine() {
    }

    public java.lang.String getMoniker() {
        return moniker;
    }

    public void setMoniker(java.lang.String moniker) {
        this.moniker = moniker;
    }

    public java.lang.String getRefinement() {
        return refinement;
    }

    public void setRefinement(java.lang.String refinement) {
        this.refinement = refinement;
    }

    public com.qas.proweb.soap.QAConfigType getQAConfig() {
        return QAConfig;
    }

    public void setQAConfig(com.qas.proweb.soap.QAConfigType QAConfig) {
        this.QAConfig = QAConfig;
    }

    public com.qas.proweb.soap.ThresholdType getThreshold() {
        return threshold;
    }

    public void setThreshold(com.qas.proweb.soap.ThresholdType threshold) {
        this.threshold = threshold;
    }

    public com.qas.proweb.soap.TimeoutType getTimeout() {
        return timeout;
    }

    public void setTimeout(com.qas.proweb.soap.TimeoutType timeout) {
        this.timeout = timeout;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QARefine)) return false;
        QARefine other = (QARefine) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((moniker==null && other.getMoniker()==null) || 
             (moniker!=null &&
              moniker.equals(other.getMoniker()))) &&
            ((refinement==null && other.getRefinement()==null) || 
             (refinement!=null &&
              refinement.equals(other.getRefinement()))) &&
            ((QAConfig==null && other.getQAConfig()==null) || 
             (QAConfig!=null &&
              QAConfig.equals(other.getQAConfig()))) &&
            ((threshold==null && other.getThreshold()==null) || 
             (threshold!=null &&
              threshold.equals(other.getThreshold()))) &&
            ((timeout==null && other.getTimeout()==null) || 
             (timeout!=null &&
              timeout.equals(other.getTimeout())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMoniker() != null) {
            _hashCode += getMoniker().hashCode();
        }
        if (getRefinement() != null) {
            _hashCode += getRefinement().hashCode();
        }
        if (getQAConfig() != null) {
            _hashCode += getQAConfig().hashCode();
        }
        if (getThreshold() != null) {
            _hashCode += getThreshold().hashCode();
        }
        if (getTimeout() != null) {
            _hashCode += getTimeout().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QARefine.class);

    static {
        org.apache.axis.description.FieldDesc field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("threshold");
        field.setXmlName(new javax.xml.namespace.QName("", "Threshold"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "ThresholdType"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.AttributeDesc();
        field.setFieldName("timeout");
        field.setXmlName(new javax.xml.namespace.QName("", "Timeout"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "TimeoutType"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("moniker");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Moniker"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("refinement");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "Refinement"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new org.apache.axis.description.ElementDesc();
        field.setFieldName("QAConfig");
        field.setXmlName(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAConfig"));
        field.setXmlType(new javax.xml.namespace.QName("http://www.qas.com/web-2005-02", "QAConfigType"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
