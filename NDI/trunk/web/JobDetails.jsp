<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import ="java.util.*" %>
<%@ page import ="java.net.URL"%>
<%@ page import="java.net.URLDecoder"%>
<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="citysprint.co.uk.NDI.LoginInfo"%>
<%@ page import="citysprint.co.uk.NDI.ReportingError"%>


<%
        ReportingError err = new ReportingError();
        HttpSession validSession = request.getSession(false);

        //boolean isLoggedIn=false;
        try {
            if (request.isRequestedSessionIdValid()) {
                String session_ID = (String) validSession.getAttribute("sessionid");
                System.out.println(session_ID);
                if (session_ID == null) {%>
<h2><a href="index.jsp">Login</a></h2>  
<%} else {

             LoginInfo login = new LoginInfo();
             login.parseLogin((String[][]) validSession.getAttribute("loginArray"), request);

             //For Common Connection
             Connection conJobDetails = null;
             Statement stmtJobDetails = null;
             Statement stmtReady = null;
//For Booking Details
             Statement stmtBooking = null;
             ResultSet rstBooking = null;

//For Collection 
//Statement stmtCollect=null;
             ResultSet rstCollect = null;

//For Delivery 

             ResultSet rstDelivery = null;

//For Consignment Details 
             ResultSet rstConsignment = null;


//For Product Details
             ResultSet rstProduct = null;

//For Total Volume
             ResultSet rstVolume = null;
//For Content Details
             ResultSet rstContent = null;

             ResultSet rstReady = null;

             ResultSet rstAuditTrial = null;

             String chk_job_no = "";
             String sqlJobDetails = request.getSession(false).getAttribute("sqlShowAudit") == null ? "" : request.getSession(false).getAttribute("sqlShowAudit").toString();

             System.out.println("Sql=" + sqlJobDetails);
// String sDetailSess=request.getParameter("s")==null?"":request.getParameter("s").toString();
//System.out.println(sDetailSess);
             String chk_Hawbno = request.getParameter("chk_HawbNo") == null ? "" : request.getParameter("chk_HawbNo").toString();
             chk_job_no = chk_Hawbno.substring(0, chk_Hawbno.length() - 1);
//String session_ID=request.getParameter("sessionID")==null?"":request.getParameter("session_ID").toString();
//String chkDetail_jobno=request.getParameter("jobno")==null?"":request.getParameter("jobno").toString();request.getParameter("chk_HawbNo")==null?"":request.getParameter("chk_HawbNo").toString();
             System.out.println("HawbNo=" + chk_Hawbno);
             int adbooking = 1;
             String abooking = "N";
             String sAction = request.getParameter("sAction") == null ? "" : request.getParameter("sAction").toString();
//String sqlJobDetails="";


             try {

                 String localDirName;

                 localDirName = getServletContext().getRealPath("") + "/WEB-INF/classes/citysprint/co/uk/NDI";

                 ResourceBundle bdl = new PropertyResourceBundle(new FileInputStream(localDirName + "/Config.properties"));

                 Class.forName(bdl.getString("sJDBCDriver")).newInstance();
                 conJobDetails = DriverManager.getConnection(bdl.getString("sJDBCurl") + "user=" + bdl.getString("sMySqlUser") + "&password=" + bdl.getString("sMySqlPassword"));

                 stmtJobDetails = conJobDetails.createStatement();
             //sqlBooking="select ndi_depart,ndi_caller,ndi_ref,ndi_phone,ndi_consignno from ndi_Audit";
             //sqlJobDetails="select * from ndi_audit";


             } catch (Exception e) {
                 err.displayError(response, e);
             }

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>

        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="expires" content="0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CitySprint</title>
        <link media="screen" href="CSS/NextDayCss.css" type="text/css" rel="stylesheet">
        <style>
            html { overflow-x: hidden;overflow-y: auto;  }

        </style>

    </head>
    <body background="#BCBCBC" style="width:895px;">

        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-left:4px;padding-right:4px;">


            <tr id="trBooking">
                <td width="100%" valign="top" colspan="2">
                    <table border="0" cellpadding="0" cellspacing="0" id="box" width="100%" style='table-layout:fixed'>


                        <tr style="padding-top:4px;padding-bottom:4px;"><td id="boxtitle" colspan="4">BOOKING DETAILS</td></tr>


                        <%

             String depart = "", caller = "", ref = "", phone = "", hawbno = "", servoption = "";
             String sqlNdiAudit = "";
             String sqlBooking = "";

             try {
                 //stmtBooking = conJobDetails.createStatement();
                 //ndi_sessionid='"+session_ID+"' and
                 //sqlBooking="select ndi_depart,ndi_caller,ndi_ref,ndi_phone,ndi_consignno from ndi_Audit";
                 sqlBooking = "select * from ndi_audit where  ndi_sessionid='" + session_ID + "' and ndi_jobno='" + chk_job_no + "' ";
                 // sqlNdiAudit="select * from ndi_audit where ndi_sessionid='"+session_ID+"' and ndi_jobno='"+chk_job_no+"'";
                 rstBooking = stmtJobDetails.executeQuery(sqlBooking);

                 while (rstBooking.next()) {

                     depart = rstBooking.getString("ndi_depart");
                     ref = rstBooking.getString("ndi_ref");
                     caller = rstBooking.getString("ndi_caller");
                     phone = rstBooking.getString("ndi_phone");
                     hawbno = rstBooking.getString("ndi_consignno");
                     servoption = rstBooking.getString("ndi_service");
                     abooking = rstBooking.getString("ndi_booking");
                 }
                 System.out.println("SqlAudit=" + sqlBooking);
             } catch (Exception e) {
                 err.displayError(response, e);
             }

                        %>

                        <tr>
                            <td id="label" width="25%">
                                <%if (login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("V")) {if (login.isEYFlag()) {%>
                                <%=login.getEy_Department()%>
                                <%} else {%>
                                Department <%}}%>
                            </td>
                            <td width="25%"><%if (login.getDepartmentFlag().equals("R") || login.getDepartmentFlag().equals("Y") || login.getDepartmentFlag().equals("V")) { %><%=depart%><%}%></td>
                            <td id="label" width="25%">
                                <% if(login.getRefFlag().equals("R") || login.getRefFlag().equals("Y") || login.getRefFlag().equals("V")){if (login.isEYFlag()) {%>
                                <%=login.getEy_Reference()%>
                                <%} else {%>
                                Reference<%}}%>
                            </td>
                            <td width="25%"><% if(login.getRefFlag().equals("R") || login.getRefFlag().equals("Y") || login.getRefFlag().equals("V")){%><%=ref%><%}%></td>
                        </tr>

                        <tr>

                            <td id="label" width="25%">
                                <%if (login.isEYFlag()) {%>
                                <%=login.getEy_YourName()%>
                                <%} else {%>
                                Caller<%}%>
                            </td>
                            <td width="25%"><%=caller%></td>
                            <td id="label" width="25%">
                                <%if (login.isEYFlag()) {%>
                                <%=login.getEy_Phone()%>
                                <%} else {%>
                                Reference 2<%}%>
                            </td>
                            <td width="25%"><%=phone%></td>
                        </tr>
                        <tr>
                            <td id="label" width="25%">
                                Consignment <br>Number
                            </td>
                            <td width="25%"><%=hawbno%></td>
                            <td id="label" width="25%">
                                Service Option
                            </td>
                            <td width="25%"><%=servoption%></td>

                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td width="50%">
                    <br>
                    <table border="0" cellpadding="2" cellspacing="0" id="box" width="99%" style='table-layout:fixed'>
                        <tr><td id="boxtitle" colspan="2">COLLECTION ADDRESS</td></tr>

                        <%

             String col_comp = "", col_address = "", col_address2 = "", col_address3 = "", col_phone = "", col_town = "", col_county = "", col_postcode = "", col_country = "";
             String col_contact = "", col_instruct = "", col_residential="";
             String sqlAudit = "";
             try {
                 //stmtJobDetails = conJobDetails.createStatement();
                 // sqlAudit="select ndi_pcomp,ndi_paddress,ndi_paddress2,ndi_paddress3,ndi_ptown,ndi_pcounty,ndi_ppostcode,ndi_pcountry,ndi_pphone,ndi_pcontact,ndi_pinstruct from ndi_audit where ndi_sessionid='"+session_ID+"' and ndi_jobno='"+chk_job_no+"'";
                 sqlAudit = "select ndi_pcomp,ndi_paddress,ndi_paddress2,ndi_paddress3,ndi_ptown,ndi_pcounty,ndi_ppostcode,"
                         + "ndi_pcountry,ndi_pphone,ndi_pcontact,ndi_pinstruct, ndi_presidentialflag "
                         + "from ndi_audit where ndi_sessionid='" + session_ID + "' and ndi_jobno='" + chk_job_no + "'";
                 rstCollect = stmtJobDetails.executeQuery(sqlAudit);
                 //out.println("Sql="+sqlJobDetails);
                 while (rstCollect.next()) {

                     col_comp = rstCollect.getString("ndi_pcomp");
                     col_address = rstCollect.getString("ndi_paddress");
                     col_address2 = rstCollect.getString("ndi_paddress2");
                     col_address3 = rstCollect.getString("ndi_paddress3");
                     col_town = rstCollect.getString("ndi_ptown");
                     col_county = rstCollect.getString("ndi_pcounty");
                     col_postcode = rstCollect.getString("ndi_ppostcode");
                     col_country = rstCollect.getString("ndi_pcountry");
                     col_phone = rstCollect.getString("ndi_pphone");
                     col_contact = rstCollect.getString("ndi_pcontact");
                     col_instruct = rstCollect.getString("ndi_pinstruct");
                     col_residential = rstCollect.getString("ndi_presidentialflag");
                 }

             } catch (Exception e) {
                 err.displayError(response, e);
             }
             
             if (col_residential.trim().equals("") ){
                 col_residential = "N";
             } else if (!col_residential.equalsIgnoreCase("N") &&  !col_residential.equalsIgnoreCase("Y")){
                 col_residential = "N";
             }

                        %>
                        <tr>
                            <td id="label" width="25%">Name</td>
                            <td width="25%" ><%=col_comp%></td>
                        </tr>
                        <tr>
                            <td id="label" width="25%">Address 1</td>
                            <td width="25%"><%=col_address%></td>
                        </tr>
                        <tr>
                            <td id="label" width="25%">Address 2</td>
                            <td width="25%"><%=col_address2%></td>
                        </tr>
                        <tr>
                            <td id="label" width="25%">Address 3</td>
                            <td width="25%"><%=col_address3%></td>
                        </tr>
                        <tr>
                            <td width="25%" id="label">Town</td>
                            <td width="25%"><%=col_town%></td>
                        </tr>
                        <tr>
                            <td width="25%" id="label">County</td>
                            <td width="25%"><%=col_county%></td>
                        </tr>
                        <tr>

                            <td width="25%" id="label">Postcode</td>
                            <td width="25%"><%=col_postcode%></td>
                        </tr>
                        <tr>
                            <td id="label" width="25%">Country</td>
                            <td width="25%"><%=col_country%></td>
                        </tr>
                        <tr>
                            <td id="label" width="25%">Contact</td>
                            <td width="25%"><%=col_contact%></td>
                        </tr>
                        <tr>
                            <td id="label" width="25%">Phone</td>
                            <td width="25%"><%=col_phone%></td>
                        </tr>

                        <tr>
                            <td id="label" width="25%">Instructions</td>
                            <td width="25%"><%=col_instruct%></td>
                        </tr>
                        
                        
                        <tr>
                            <td id="label" width="25%">Residential</td>
                            <td width="25%"><%=col_residential%></td>
                        </tr>

                    </table>
                </td>

                <td width="50%">
                    <br>

                    <table border="0" cellpadding="2" cellspacing="0" id="box" width="99%" style='table-layout:fixed'>
                        <tr><td id="boxtitle" colspan="2">DELIVERY ADDRESS</td></tr>

                        <%

             String del_comp = "", del_address = "", del_address2 = "", del_address3 = "", del_phone = "", del_town = "", del_county = "", del_postcode = "", del_country = "";
             String del_contact = "", del_instruct = "", del_residential ="";
             String sqlDelivery = "";
             try {
                 //stmtjobDetails = conJobDetails.createStatement();
                 //sqlBooking="select ndi_depart,ndi_caller,ndi_ref,ndi_phone,ndi_consignno from ndi_Audit";
                 // sqlDelivery="select ndi_dcomp,ndi_daddress,ndi_daddress2,ndi_daddress3,ndi_dtown,ndi_dcounty,ndi_dpostcode,ndi_dcountry,ndi_dcontact,ndi_dphone,ndi_dinstruct from ndi_audit where ndi_sessionid='"+session_ID+"' and ndi_jobno='"+chk_job_no+"'";
                 sqlDelivery = "select * from ndi_audit where ndi_sessionid='" + session_ID + "' and ndi_jobno='" + chk_job_no + "' ";
                 rstDelivery = stmtJobDetails.executeQuery(sqlDelivery);
                 //  rstDelivery  = stmtJobDetails.executeQuery(sqlDelivery);
                 //out.println("Sql="+sqlJobDetails);
                 while (rstDelivery.next()) {

                     del_comp = rstDelivery.getString("ndi_dcomp");
                     del_address = rstDelivery.getString("ndi_daddress");
                     del_address2 = rstDelivery.getString("ndi_daddress2");
                     del_address3 = rstDelivery.getString("ndi_daddress3");
                     del_town = rstDelivery.getString("ndi_dtown");
                     del_county = rstDelivery.getString("ndi_dcounty");
                     del_postcode = rstDelivery.getString("ndi_dpostcode");
                     del_country = rstDelivery.getString("ndi_dcountry");
                     del_contact = rstDelivery.getString("ndi_dcontact");
                     del_phone = rstDelivery.getString("ndi_dphone");
                     del_instruct = rstDelivery.getString("ndi_dinstruct");
                     del_residential = rstDelivery.getString("ndi_dresidentialflag");
                 }

             } catch (Exception e) {
                 err.displayError(response, e);
             }

             if (del_residential.trim().equals("") ){
                 del_residential = "N";
             } else if (!del_residential.equalsIgnoreCase("N") &&  !del_residential.equalsIgnoreCase("Y")){
                 del_residential = "N";
             }
                 
                        %>


                        <tr>
                            <td id="label" width="25%">Name</td>
                            <td width="25%" ><%=del_comp%></td>
                        </tr>
                        <tr>
                            <td id="label" width="25%">Address 1</td>
                            <td width="25%"><%=del_address%></td>
                        </tr>
                        <tr>
                            <td id="label" width="25%">Address 2</td>
                            <td width="25%"><%=del_address2%></td>
                        </tr>
                        <tr>
                            <td id="label" width="25%">Address 3</td>
                            <td width="25%"><%=del_address3%></td>
                        </tr>
                        <tr>
                            <td width="25%" id="label">Town</td>
                            <td width="25%"><%=del_town%></td>
                        </tr>
                        <tr>
                            <td width="25%" id="label">County</td>
                            <td width="25%"><%=del_county%></td>
                        </tr>
                        <tr>

                            <td width="25%" id="label">Postcode</td>
                            <td width="25%"><%=del_postcode%></td>
                        </tr>
                        <tr>
                            <td id="label" width="25%">Country</td>
                            <td width="25%"><%=del_country%></td>
                        </tr>
                        <tr>
                            <td id="label" width="25%">Contact</td>
                            <td width="25%"><%=del_contact%></td>
                        </tr>
                        <tr>
                            <td id="label" width="25%">Phone</td>
                            <td width="25%"><%=del_phone%></td>
                        </tr>

                        <tr>
                            <td id="label" width="25%">Instructions</td>
                            <td width="25%"><%=del_instruct%></td>
                        </tr>
                        
                        <tr>
                            <td id="label" width="25%">Residential</td>
                            <td width="25%"><%=del_residential%></td>
                        </tr>
                    </table>
                </td>
            </tr>



            <tr id="trCollectTime">
                <td width="50%">
                    <br>
                    <table id="box" border="0" cellpadding="0" cellspacing="0" width="99%" style='table-layout:fixed'>
                        <tr>
                            <td  id="boxtitle" colspan="2">
                                CONSIGNMENT DETAILS
                            </td>
                        </tr>
                        <%
             String sqlProductWeight = "";
             String consign_ptype = "", consign_totpieces = "", consign_totweight = "", consign_totvolume = "", consign_parcel_type = "";
             //sqlProductWeight="select *,(SUM((prod_length*prod_height*prod_depth))) as volWeight from audit_prod,ndi_audit where prod_consignno=ndi_consignno and  prod_consignno = '"+hawbno+"'GROUP BY ndi_id ";
             //sqlProductWeight="select *,(SUM((prod_length*prod_height*prod_depth))) as volWeight from audit_prod,ndi_audit where prod_jobno=ndi_jobno and prod_sessionid=ndi_sessionid and  prod_jobno = '"+chk_job_no+"' and prod_sessionid='"+session_ID+"' GROUP BY ndi_id ";
             //sqlProductWeight="select *,(SUM((prod_length*prod_height*prod_depth))) as volWeight from audit_prod,ndi_audit where prod_jobno=ndi_jobno  and prod_sessionid=ndi_sessionid  and prod_sessionid='"+session_ID+"' and prod_jobno = '"+chk_job_no+"'GROUP BY ndi_id ";

             try {
                 // rstConsignment  = stmtJobDetails.executeQuery(sqlJobDetails);
                 // sqlProductWeight="select ndi_prodtype,ndi_totpieces,ndi_totweight,(SUM((prod_length*prod_height*prod_depth))) as volWeight from audit_prod,ndi_audit where prod_jobno=ndi_jobno  and prod_sessionid=ndi_sessionid  and prod_sessionid='"+session_ID+"' and prod_jobno = '"+chk_job_no+"' GROUP BY ndi_id ";
                 sqlProductWeight = "select ndi_prodtype,ndi_totpieces,ndi_totweight "
                        // + "/*, ndi_parcelName*/ "
                         + " from ndi_audit where ndi_sessionid='" + session_ID + "' and ndi_jobno = '" + chk_job_no + "' ";
                 rstConsignment = stmtJobDetails.executeQuery(sqlProductWeight);
                 while (rstConsignment.next()) {

                     consign_ptype = rstConsignment.getString("ndi_prodtype");
                     consign_totpieces = rstConsignment.getString("ndi_totpieces");
                     consign_totweight = rstConsignment.getString("ndi_totweight");
                 //consign_totvolume   = rstConsignment.getString("volWeight");
                     //consign_parcel_type   = rstConsignment.getString("ndi_parcelName");
                     

                 }
                 System.out.println("SqlConsignment=" + sqlProductWeight);

             } catch (Exception e) {
                 err.displayError(response, e);
             }

                        %>


                        <tr>
                            <td id="label" width="25%">Product Type</td>
                            <td width="25%"><%=consign_ptype%></td>
                        </tr>
                        
<!--                         <tr>
                            <td id="label" width="25%">Parcel Type</td>
                            <td width="25%"><%=consign_parcel_type%></td>
                        </tr>-->
                        
                        
                        <tr>
                            <td id="label" width="25%">Total No of Pieces</td>
                            <td width="25%"><%=consign_totpieces%></td>
                        </tr>
                        <tr>
                            <td id="label" width="25%">Total Weight</td>
                            <td width="25%"><%=consign_totweight%></td>
                        </tr>
                        <tr>
                            <%


             String sqlVolumeWeight = "";
             String sCubingFactor = "6000";

             if (login.getSCubingFactor().length() > 0 && !login.getSCubingFactor().equals("0")) {
                 sCubingFactor = login.getSCubingFactor();
             }


             try {
                 // dc - 24/11/2009 - changed according to David Morgan's email
                 //sqlVolumeWeight = "select *,CAST(SUM(CEILING((prod_length*prod_height*prod_depth*prod_noitems*10)/" + sCubingFactor + ")/10) AS DECIMAL(16,1)) as volWeight from audit_prod,ndi_audit where prod_jobno=ndi_jobno and prod_sessionid=ndi_sessionid  and prod_sessionid='" + session_ID + "' and prod_jobno = '" + chk_job_no + "' GROUP BY ndi_id ";
                 sqlVolumeWeight = "select *,CAST(CEILING(SUM((prod_length*prod_height*prod_depth*prod_noitems)/" + sCubingFactor + ")*10)/10 as DECIMAL(16,1)) as volWeight from audit_prod,ndi_audit where prod_jobno=ndi_jobno and prod_sessionid=ndi_sessionid  and prod_sessionid='" + session_ID + "' and prod_jobno = '" + chk_job_no + "' GROUP BY ndi_id ";
                 rstVolume = stmtJobDetails.executeQuery(sqlVolumeWeight);
                 while (rstVolume.next()) {


                     consign_totvolume = rstVolume.getString("volWeight");

                 }
                 System.out.println("SqlConsignment=" + sqlVolumeWeight);

             } catch (Exception e) {
                 err.displayError(response, e);
             }

                            %>
                            <td id="label" width="25%">Total Volumetric Weight</td>
                            <td width="25%"><%=consign_totvolume%></td>
                        </tr>

                        <tr>
                            <td width="100%" colspan="2">
                                <table border="0" cellpadding="2" cellspacing="0" id="box" width="99%" align="center" style='table-layout:fixed'>
                                    <tr>
                                        <td id="boxtitle" width="12.5%">No Of Items</td>
                                        <!--
                                        <td id="boxtitle" width="12.5%">Parcel Type</td>
                                        -->
<!--                                        <td id="boxtitle" width="12.5%">Item Weight</td>-->
                                        <td id="boxtitle" width="12.5%">Length(cm)</td>
                                        <td id="boxtitle" width="12.5%">Width(cm)</td>
                                        <td id="boxtitle" width="12.5%">Height(cm)</td>
                                        
                                        
                                        
                                        
                                        
                                    </tr>

                                    <%
             int prod_items, prod_length, prod_depth, prod_height;
             String pro_ptype;
             Float    pro_iweight;
             //String prod_items="",prod_length="",prod_depth="",prod_height="";
             String sqlProductDetails = "";

             try {
                 //sqlProductDetails="select prod_noitems,prod_length,prod_height,prod_depth from audit_prod,ndi_audit where prod_jobno=ndi_jobno and prod_sessionid=ndi_sessionid and  prod_sessionid='"+session_ID+"' and prod_jobno = '"+chk_job_no+"'";
                 //sqlProductDetails="select *,prod_noitems,prod_length,prod_height,prod_depth from audit_prod where prod_jobno=ndi_jobno and prod_sessionid=ndi_sessionid and prod_sessionid='"+session_ID+"' and prod_jobno = '"+chk_job_no+"'";
                 // sqlProductDetails="select prod_noitems,prod_length,prod_height,prod_depth from audit_prod,ndi_audit where prod_jobno=ndi_jobno  and prod_jobno = '"+chk_job_no+"'";
                 sqlProductDetails = "select prod_noitems,prod_length,prod_height,prod_depth "//, "//pro_ptype, "
                      //   + "pro_iweight"
                         + " from audit_prod,ndi_audit where prod_sessionid=ndi_sessionid and prod_jobno=ndi_jobno and prod_sessionid='" + session_ID + "' and prod_jobno = '" + chk_job_no + "'";
                 rstProduct = stmtJobDetails.executeQuery(sqlProductDetails);
                 //rstProductWeight=stmtJobDetails.executeQuery(sqlProductWeight);
                 while (rstProduct.next()) {

                     prod_items = rstProduct.getInt(1);//"prod_noitems");
                     prod_length = rstProduct.getInt(2);//"prod_length");
                     prod_depth = rstProduct.getInt(3);//"prod_depth");
                     prod_height = rstProduct.getInt(4);//"prod_height");
                     
                    //pro_ptype =  rstProduct.getString(5);
//                    pro_iweight =  rstProduct.getFloat(6);
//                     pro_iweight =  rstProduct.getFloat(5);

                                    %>

                                    <tr id="odd" style="text-align:right">
                                        <td style="text-align:center"><%=prod_items%></td>
                                        
                                     <!--   <td style="text-align:center"><%//=pro_ptype%></td>
                                     -->
<!--                                        <td style="text-align:center"><//%=pro_iweight%></td>-->
                                        
                                        <td style="text-align:center"><%=prod_length%></td>
                                        <td style="text-align:center"><%=prod_depth%></td>
                                        <td style="text-align:center"><%=prod_height%></td>
                                    </tr>


                                    <%}

                 System.out.println("SqlProduct=" + sqlProductDetails);

             } catch (Exception e) {
                 err.displayError(response, e);
             }
                                    %>


                                </table>
                            </td>
                        </tr>
                    </table>

                </td>

                <td width="50%">
                    <br>
                    <table id="box" height="50%" border="0" cellpadding="0" cellspacing="0" width="99%">
                        <tr>
                            <td id="boxtitle" colspan="2">
                                CONTENTS
                        </td></tr>
                        <tr>
                            <td width="100%" colspan="2">
                                <table border="0" cellpadding="2" cellspacing="0" id="box" width="99%" align="center" style='table-layout:fixed'>
                                    <tr>
                                        <td id="boxtitle" style="padding-left:2px;text-align:left" width="20%">Commodity</td>
                                        <td id="boxtitle" style="padding-left:2px;text-align:right" width="10%">Quantity</td>
                                        <td id="boxtitle" style="padding-left:2px;text-align:right" width="10%">Total Value</td>
                                        <td id="boxtitle" style="padding-left:2px;text-align:right" width="10%">Indemnity</td>
                                    </tr>
                                    <%

             String com_descrip = "", com_quant = "", com_value = "", com_indemnity = "";
             String sqlContentDetails = "";

             try {
                 sqlContentDetails = "select com_description,com_quantity,com_value,com_indemnity from audit_commodity,ndi_audit where com_sessionid=ndi_sessionid and com_jobno=ndi_jobno and com_sessionid='" + session_ID + "' and com_jobno = '" + chk_job_no + "' ";
                 //  sqlContentDetails="select com_description,com_quantity,com_value,com_indemnity from audit_commodity";
                 // sqlContentDetails="select com_description,com_quantity,com_value,com_indemnity from audit_commodity,ndi_audit where com_jobno=ndi_jobno and com_sessionid=ndi_sessionid  and com_jobno = '"+chk_job_no+"' ";
                 rstContent = stmtJobDetails.executeQuery(sqlContentDetails);

                 while (rstContent.next()) {

                     com_descrip = rstContent.getString("com_description");
                     com_quant = rstContent.getString("com_quantity");
                     com_value = rstContent.getString("com_value");
                     com_indemnity = rstContent.getString("com_indemnity");%>

                                    <tr style="text-align:right">
                                        <td style="text-align:left"><%=com_descrip%></td>
                                        <td style="text-align:right"><%=com_quant%></td>
                                        <td style="text-align:right"><%=com_value%></td>
                                        <td style="text-align:right"><%=com_indemnity%></td>
                                    </tr>


                                    <%}
                 System.out.println("sqlContent=" + sqlContentDetails);
             } catch (Exception e) {
                 err.displayError(response, e);
             }
                                    %>

                                </table>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>


            <tr id="trCollectTime">
                <td colspan="2"><br>

                    <table id="box" border="0" cellpadding="0" cellspacing="0" width="100%" style='table-layout:fixed'>
                        <tr>
                            <td id="boxtitle" colspan="4">
                                Job Audit Details
                        </td></tr>


                        <%
             String readyat = "", readyon = "";
             String sqlReady = "";
             try {
                 // sqlReady = "select DATE_FORMAT(ndi_readyon,'%d/%m/%y') as r_on, TIME_FORMAT(ndi_readyat,'%l:%i') as r_at  from ndi_audit where ndi_sessionid='"+session_ID+"' and ndi_jobno = '"+chk_job_no+"' ";

                 stmtReady = conJobDetails.createStatement();
                 sqlReady = "select DATE_FORMAT(ndi_readyon,'%d/%m/%y') as r_on, TIME_FORMAT(ndi_readyat,'%H:%i') as r_at  from ndi_audit where ndi_sessionid='" + session_ID + "' and  ndi_jobno = '" + chk_job_no + "' ";
                 rstReady = stmtReady.executeQuery(sqlReady);
                 while (rstReady.next()) {

                     readyat = rstReady.getString("r_at");
                     readyon = rstReady.getString("r_on");
                 }
                        %>

                        <%
                 System.out.println("SQLReady=" + sqlReady);
             } catch (Exception e) {
                 err.displayError(response, e);
             }
                        %>
                        <tr>
                            <td id="label" width="25%">Advance :
                                <% if (abooking.equalsIgnoreCase("Y")) {%>
                                <input  type="checkbox" name="checkab" id="checkab" checked>
                                <%} else {%>
                                <input  type="checkbox" name="checkab" id="checkab" >
                                <%}%>
                            </td>
                            <td id="label" width="25%">
                                Ready At :<%=readyat%>
                            </td>
                            <td width="50%" colspan="2">
                                Ready On :<%=readyon%>
                            </td>
                            <%-- <td id="label">Ready At : </td>
                <td><%=readyat.trim()%></td>

                <td id="label" >Ready On :</td>
                            <td><%=readyon%></td>--%>
                        </tr>
                        <tr>
                            <td width="100%" colspan="4">
                                <table border="0" cellpadding="2" cellspacing="0" id="box" width="100%" style='table-layout:fixed'>


                                    <tr><td id="boxtitle" style="padding-left:10px" width="25%">Date</td>
                                        <td id="boxtitle" style="padding-left:10px" width="25%">Time </td>
                                        <td id="boxtitle" style="padding-left:10px" width="50%">Description</td>

                                    </tr>

                                    <%
             String auditdate = "", audittime = "", auditdescrip = "";
             String sqlAuditTrialDetails = "";
             try {
                 //sqlAuditTrialDetails="select DATE_FORMAT(audit_date,'%d/%m/%y')as a_date,DATE_FORMAT(audit_time,'%H:%i') ,audit_descrip from audit_trial,ndi_audit where audit_jobno=ndi_jobno and audit_sessionid=ndi_sessionid and audit_sessionid='"+session_ID+"' and audit_jobno='"+chk_job_no+"' order by audit_trial.audit_date, audit_trial.audit_time DESC";
                 sqlAuditTrialDetails = "select DATE_FORMAT(audit_trial.audit_date,'%d/%m/%y')as a_date,DATE_FORMAT(audit_trial.audit_time,'%H:%i') as audit_time," +
                         "audit_trial.audit_descrip from audit_trial JOIN ndi_audit ON audit_trial.audit_jobno=ndi_audit.ndi_jobno where " +
                         "audit_trial.audit_sessionid=ndi_audit.ndi_sessionid and audit_trial.audit_sessionid='" + session_ID + "' " +
                         // following line is commented by Adnan @06MAY2011 against an issue in sorting of job details if
                         // details present of same date and same time as time is stored upto mins in db, and client reported the issue.
                         //"and audit_trial.audit_jobno='" + chk_job_no + "' order by audit_trial.audit_date DESC, audit_trial.audit_time DESC";
                         "and audit_trial.audit_jobno='" + chk_job_no + "' order by audit_trial.audit_id DESC";

                 System.out.println("sqlAuditTrialDetails=" + sqlAuditTrialDetails);
                 //sqlAuditTrialDetails="select DATE_FORMAT(audit_date,'%d/%m/%y')as a_date,audit_time ,audit_descrip from audit_trial,ndi_audit where audit_jobno=ndi_jobno audit_sessionid=ndi_sessionid and audit_jobno='"+chk_job_no+"'";
                 rstAuditTrial = stmtJobDetails.executeQuery(sqlAuditTrialDetails);
                 //System.out.println ("a_date /t audit_time /t audit_descrip");
                 while (rstAuditTrial.next()) {
                     auditdate = rstAuditTrial.getString("a_date");
                     audittime = rstAuditTrial.getString("audit_time");
                     auditdescrip = rstAuditTrial.getString("audit_descrip");
//System.out.println (auditdate + " /t " +audittime +" /t " + auditdescrip);
                                    %>

                                    <tr>
                                        <td id="label" ><%= auditdate%> </td>
                                        <td id="label" ><%= audittime%> </td>
                                        <td id="label" ><%= auditdescrip%> </td>
                                    </tr>


                                    <%    }
                 System.out.println("AuditTrial=" + sqlAuditTrialDetails);
             } catch (Exception e) {
                 err.displayError(response, e);
             }


                                    %>

                                </table>
                            </td>
                        </tr>
                    </table>

                    <form name="formImage" method="post" action="Reporting.jsp">
                    </form>
                </td>
            </tr>
        </table>


        <% }
            }
        } catch (Exception e) {
            err.displayError(response, e);
        }
        %>

    </body>
</html>