<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page contentType="text/html" %>
<%@ page import="com.qas.proweb.servlet.*" %>
<%
	/** QuickAddress Pro Web > (c) QAS Ltd > www.qas.com **/
	/** Web > Verification > verifyInput.jsp **/
	/** Address input page > Enter complete address and selected country **/
	
	/* Input attributes: none
		Output params: DataId, UserInput[], CountryName
		Next Command: Verify */
%>

<html>
	<head>
		<link rel="stylesheet" href="CSS/style.css" type="text/css" />
		<title>QuickAddress Pro Web - Address Verification</title>
		<script type="text/javascript">
			/* Set the focus to the first address line */
			function init()
			{
				document.formAddress.<%= Constants.USER_INPUT %>[0].focus();
			}

			/* Checks the user has entered some address terms and calls setCountryValue */
			function validate(theForm)
			{
				setCountryValue(theForm);
				
				var aUserInput = theForm.<%= Constants.USER_INPUT %>;
				for (var i=0; i < aUserInput.length; i++)
				{	// at least one address term
					if (aUserInput[i].value != "")
					{
						return true;
					}
				}
				alert("Please enter an address.");
				return false;
			}

			/* This function sets the value of the CountryName field to be the text selected in the select control */
			function setCountryValue(theForm)  
			{      
				var iSelected = theForm.<%= Constants.DATA_ID %>.selectedIndex;
				theForm.<%= Constants.COUNTRY_NAME %>.value = theForm.<%= Constants.DATA_ID %>.options[iSelected].text;
			}
		</script>
	</head>
	
	<body onLoad="init();">
		<table width="800" class="nomargin">
			<tr>
				<th class="banner">
					<a href="index.htm"><h1>QuickAddress Pro Web</h1><h2>JSP Samples</h2></a>
				</th>
			</tr>
			<tr>
				<td class="nomargin">
					<h1>Address Verification</h1>
					<h3>Enter &amp; Verify Address</h3>
					
					<form name=formAddress method=post action="QasController" onSubmit="return validate(this);">
						<br />
						<table>
							<tr>
								<td>Address Line 1</td>
								<td width="10"></td>
								<td><input type="text" name="<%= Constants.USER_INPUT %>" size="50" value="" /></td>
							</tr>
							<tr>
								<td>Address Line 2</td>
								<td width="10"></td>
								<td><input type="text" name="<%= Constants.USER_INPUT %>" size="50" value="" /></td>
							</tr>
							<tr>
								<td>Address Line 3</td>
								<td width="10"></td>
								<td><input type="text" name="<%= Constants.USER_INPUT %>" size="50" value="" /></td>
							</tr>
							<tr>
								<td>City</td>
								<td width="10"></td>
								<td><input type="text" name="<%= Constants.USER_INPUT %>" size="50" value="" /></td>
							</tr>
							<tr>
								<td>State or Province</td>
								<td width="10"></td>
								<td><input type="text" name="<%= Constants.USER_INPUT %>" size="50" value="" /></td>
							</tr>
							<tr>
								<td>ZIP or Postal Code</td>
								<td width="10"></td>
								<td><input type="text" name="<%= Constants.USER_INPUT %>" size="50" value="" /></td>
							</tr>
							<tr>
								<td>Country</td>
								<td width="10"></td>
								<td>
									<select name=<%= Constants.DATA_ID %> >
										<%@ include file="countries.all.inc" %>
									</select>
								</td>
							</tr>
						</table>
						
						<input type="hidden" name="<%= Constants.COUNTRY_NAME %>" />
						<input type="hidden" name="<%= Constants.COMMAND %>" value="<%= Verify.NAME %>" />
						
						<input type="submit" value="    Next >   " />
					</form>
					
					<br /><br /><hr /><br />
					<a href="index.htm">Click here to return to the JSP Samples home page.</a>
				</td>
			</tr>
		</table>
	</body>
</html>
