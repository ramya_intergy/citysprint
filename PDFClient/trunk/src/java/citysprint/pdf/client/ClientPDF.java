package citysprint.pdf.client;

import java.nio.CharBuffer;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.soap.*;

import java.io.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;

//FOP
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.DefaultConfigurationBuilder;

import com.jstaten.webService.client.generated.PDF;
import com.jstaten.webService.client.generated.PDFService;


import java.util.*;
import java.text.*;


import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import java.net.URL;


public class ClientPDF {


    private static PDFService service;

    public static void main (String args [])    throws  IOException {
        String message="";

        try {




//           PrintWriter out = response.getWriter();
//            // Call Web Service Operation
//            pdflocal.PDFService service = new pdflocal.PDFService();
//            pdflocal.PDF port = service.getPDFPort();
//            // TODO initialize WS operation arguments here

            String sXSLTName= args[0];// "globalTemplates.xslt" ; //request.getParameter("StyleSheet").toString();
            String sPDFName= args[0] + System.currentTimeMillis(); // request.getParameter("PDFPath").toString();
            String srdXML="" ; //request.getParameter("rdXML")==null?"":request.getParameter("rdXML").toString();


//           // String sRdDefaultXML=request.getParameter("rddefaultXML")==null?"":request.getParameter("rddefaultXML").toString();
//           // String sRdXMLString=request.getParameter("rdXMLString")==null?"":request.getParameter("rdXMLString").toString();

            String sXML= getXMLInString (args[1]);
            	
            	//request.getParameter("XMLFile").toString();

            java.lang.String styleSheet=sXSLTName;
            java.lang.String method = "";
            java.lang.String transportAddress = "";
            java.lang.String transportUsername = "";
            java.lang.String pdfFilename = sPDFName;
//            java.lang.String xml="";

//            if(srdXML.equals("1"))
//                xml=readXML();
//            else
//                xml=sXML;
            try { // Call Web Service Operation

            	System.out.println("Before service created == args[2]" +args[2]);
//            	URL url = new URL("http://localhost:8100/PDFService/PDF" + "?WSDL");
//            	QName qname = new QName("http://pdf.com", "PDFService");

//            	Service service = Service.create(url, qname);


//            	System.out.println("service created ==" +service.getPort(serviceEndpointInterface) );
//            	Iterator<QName> iterator = service.getPorts();

//            	PDFService endPoint = service.getPort(PDFService.class);

                //QName qname = new QName("http://172.16.9.57/PDFService/PDF", "PDFService");
                //String wsdl = "http://172.16.9.57/PDFService/PDF?WSDL";

                service = new PDFService();
                System.out.println("service created ==" + service.toString());
                PDF port = service.getPDFPort();
                // TODO initialize WS operation arguments here
                BindingProvider bindingProvider = (BindingProvider) port;
                bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, args[2]);
                // TODO process result here
                java.lang.String result = port.pdfCreation(styleSheet, method, transportAddress, transportUsername, pdfFilename, sXML);
                System.out.println("Result = "+result);
            } catch (Exception ex) {
                // TODO handle custom exceptions here
            	ex.printStackTrace();
                message=ex.getMessage();
                System.out.println("message = "+message);
            }

//                PDFWSNew180908.PDFService service = new PDFWSNew180908.PDFService();
//                PDFWSNew180908.PDF port = service.getPDFPort();
//
//                java.lang.String result = port.pdfCreation(styleSheet, method, transportAddress, transportUsername, pdfFilename, xml);
//                System.out.println("Result = "+result);



        } catch (Exception ex) {
            // TODO handle custom exceptions here

            message=ex.getMessage();
            System.out.println("message = "+message);
            ex.printStackTrace();
        }

    }

    public String getLocalDirName()
   {
      String localDirName;

      //Use that name to get a URL to the directory we are executing in
      java.net.URL myURL = this.getClass().getResource(getClassName());  //Open a URL to the our .class file

      //Clean up the URL and make a String with absolute path name
      localDirName = myURL.getPath();  //Strip path to URL object out

      localDirName = myURL.getPath().replaceAll("%20", " ");  //change %20 chars to spaces

      //Get the current execution directory
      localDirName = localDirName.substring(0,localDirName.lastIndexOf("/"));  //clean off the file name

      return localDirName;
   }

    public String getClassName()
   {
      String thisClassName;

      //Build a string with executing class's name
      thisClassName = this.getClass().getName();
      thisClassName = thisClassName.substring(thisClassName.lastIndexOf(".") + 1,thisClassName.length());
      thisClassName += ".class";  //this is the name of the bytecode file that is executing

      return thisClassName;
   }

    public String readXML()
    {

        String sXML="";

        try
        {
        ResourceBundle bdl = new PropertyResourceBundle(new FileInputStream(getLocalDirName() + "/Config.properties"));

       // File resultsFile = new File("C:\\Projects\\Verbacorp\\CitySprint-PDFApplication\\CitySprint - PDFApplication\\xml\\xml\\VC01w 788861.xml");

        String sFilename=bdl.getString("XMLFile");
        sFilename=sFilename.replace("\"","");
        File resultsFile = new File(sFilename);
        BufferedReader reader = null;
        String line="";

        StringBuffer sb=new StringBuffer();

        try
        {
            reader = new BufferedReader(new FileReader(resultsFile));



            while(line != null)
            {

                line = reader.readLine();
                sb.append(line);

            }

            sXML=sb.substring(0,sb.length());
            sXML=sXML.replace("null","");

        }
       /* catch(FileNotFoundException e)
        {
        System.out.println("Error Encountered reading..." + resultsFile);
        }*/
        catch(IOException e)
        {
            System.out.println("Error on reading:" + resultsFile);
        }
        finally
        {
            if (reader != null)
            {
                try
                {
                reader.close();
                }
                catch(IOException e)
                {
                System.out.println("Error on closing:" + resultsFile);
                }
            }
        }
        }
        catch (Exception e)
        {

        }

       // createPDF(sXML);

        return sXML;
    }

    public void createPDF(String sXML)
    {
        try
        {

//            Properties props = new Properties();
//            ClassLoader loader = this.getClass().getClassLoader();
//            InputStream istream = loader.getResourceAsStream("Config.properties");
//
//            props.load(istream);
//            XSLTPath = props.getProperty("XSLTPath");


            File xsltfile = new File("C:\\Projects\\Verbacorp\\CitySprint-PDFApplication\\CitySprint - PDFApplication\\xml\\xslt\\citysprint.xslt");
            File pdffile = new File("C:\\Projects\\Verbacorp\\CitySprint-PDFApplication\\CitySprint - PDFApplication\\out\\pdfnew290808.pdf");
            //File xmlFile= new File(xml);
           // xml=xml.replace("\t","");
          //  StringReader rd = new StringReader(sXML);

            ByteArrayInputStream bais = new ByteArrayInputStream(sXML.getBytes());

              // configure fopFactory as desired
            FopFactory fopFactory = FopFactory.newInstance();

            FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
//            // configure foUserAgent as desired
//
//            // Setup output
            OutputStream out = new java.io.FileOutputStream(pdffile);
            out = new java.io.BufferedOutputStream(out);

            try {
//                // Construct fop with desired output format
                Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);
//
//                // Setup XSLT
                TransformerFactory factory = TransformerFactory.newInstance();
                Transformer transformer = factory.newTransformer(new StreamSource(xsltfile));

                // Set the value of a <param> in the stylesheet
                transformer.setParameter("versionParam", "2.0");

                // Setup input for XSLT transformation

                Source src = new StreamSource(bais);

                // Resulting SAX events (the generated FO) must be piped through to FOP
                Result res = new SAXResult(fop.getDefaultHandler());

                // Start XSLT transformation and FOP processing
                transformer.transform(src, res);
                //transformer.transform((Source)xml,res);

            } finally
            {
                out.close();

            }
    }
        catch(Exception ex)
        {
            String message=ex.getMessage();

        }
    }

     public void writeOutputLog(String aContents)
                                 throws java.io.FileNotFoundException, java.io.IOException
     {

    File file;
    FileWriter fw;

     File baseDir = new File(".");
     File logDir = new File(baseDir,"PDFlogs");
     boolean bDirExists=logDir.mkdirs();

    try {
            file = new File(logDir,"error_log.txt");

            if (!file.exists())
            {
                // a new file was created
                boolean fCreated=file.createNewFile();
                Writer output = new BufferedWriter(new FileWriter(file));

                //FileWriter always assumes default encoding is OK!
                output.write("");
                output.write("\\n");
                output.write( aContents );
                output.close();
            }
            else {
                Writer output = new BufferedWriter(new FileWriter(file));

                //FileWriter always assumes default encoding is OK!
                output.write("");
                output.write("\\n");
                output.write( aContents );
                output.close();
            }
        }
        catch (Exception e)
        {
            throw new Error("unable to create file "  +
                            "\n" + e.toString());
        }
  }

     public static String getXMLInString (String xmlFileName)
     {
    	 StringBuffer resultBuffer = new StringBuffer();
     try{
     // Open the file that is the first
     // command line parameter

     FileInputStream fstream = new FileInputStream(xmlFileName);
     // Get the object of DataInputStream
     DataInputStream in = new DataInputStream(fstream);
     BufferedReader br = new BufferedReader(new InputStreamReader(in));
     String strLine;
     //Read File Line By Line
     while ((strLine = br.readLine()) != null)   {
     // Print the content on the console
//     System.out.println (strLine);
    	 resultBuffer.append(strLine);
     }
     //Close the input stream
     in.close();
       }catch (Exception e){//Catch exception if any
     System.err.println("Error: " + e.getMessage());
     }

       return resultBuffer.toString();
     }
}
