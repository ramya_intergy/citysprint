<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PDF Webservice client</title>
        <link media="screen" href="NextDayCss.css" type="text/css" rel="stylesheet">
    </head>
    <body>

        <form action="ClientPDF" method="post">
            <table><tr><td><img src="citysprint_logo.gif"></td></tr></table>
        <table cellpadding="5" cellspacing="0" border="0" align="center" class="" id="box">
            
	<tr>
		<td id="label">StyleSheet</td>
		<td><input name="StyleSheet" type="text" size="20" style="" /></td>
	</tr>
	<tr>
		<td id="label">PDFPath</td>
		<td><input name="PDFPath" type="text" size="20"/></td>
	</tr> 
        <tr>
                    <td id="label">XMLFile</td>
                    <td><input type="radio" value="1" id="rddefaultXML" name="rdXML" checked> Use default XML on the server (or) </td>
        </tr>
        <tr>
                <td></td>
                <td> <input type="radio" value="2" id="rdXMLString" valign=top name="rdXML"> Paste XML string &nbsp;&nbsp;
                <textarea name="XMLFile" cols="40" rows="5" align="bottom" ></textarea></td>
	</tr>
       	<tr>
		<td></td>
		<td><input type="SUBMIT" Value="Generate PDF" id="btn" /></td>
	</tr>
        </table>
        </form>
    </body>
</html>
