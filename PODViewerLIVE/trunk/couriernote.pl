#!/usr/local/server/perl/bin/perl 
# Include various Perl modules
use WDDX;     # Wddx support for Perl, asee www.Wddx.org
use Time::Local;
use Data::Dumper;
use pdflib_pl;
use Text::Wrap;

# Establish some constants to control behavior
$packetFilename   = "/tmp/rr";   # File to retrieve stuff from
$page_x = 595;
$page_y = 842;
$margin = 40;
$page_col2_1 = 120;
$page_col2_2 = 360;
$page_col3_1 = 90;
$page_col3_2 = 300;
$page_col3_3 = 490;
$page_col4_1 = 80;
$page_col4_2 = 200;
$page_col4_3 = 370;
$page_col4_4 = 480;
$tracking_col_1 = 40;
$tracking_col_2 = 120;
$tracking_col_3 = 210;
$y_big_spacing = 30;
$y_small_spacing = 15;
$field_font = "Helvetica";
$field_font_size = 11.0;
$title_font = "Times-Roman";
$title_font_size = 13.0;
# $field_font = "Times-Roman";
$field_title_font = "Times-Roman";
$field_title_font_size = 10.0;
$bottom_margin = 70;
$default_encoding = "host";

if (!$ARGV[0]) {
	print "Usage: couriernote.pl xml_packet_filename\n\n";
	exit 1;
}
$data_file=$ARGV[0];

# let's open the data_file
open (MESSAGE_FILE, $data_file);
read (MESSAGE_FILE, $the_data, 100000000);
close (MESSAGE_FILE);
#unlink $data_file;

# Create new Wddx "worker object" to do our work for us
$wddx = new WDDX();

        $rs_data = $wddx->deserialize( $the_data );

$p = PDF_new();
PDF_set_parameter($p, "compatibility", "1.4");

die "Couldn't open PDF file" if (PDF_open_file($p, "-") == -1);

PDF_set_info($p, "Creator", "BRTC");
PDF_set_info($p, "Author", "BRTC");
PDF_set_info($p, "Title", "Couriernote");


$page_number = 0;

	eval {$JN = $rs_data->get("JN")->as_scalar};
        eval {$YZ = $rs_data->get("YZ")->as_scalar}; 
	$page_left = &print_header($JN,$YZ);

	$page_left = $page_left - $y_big_spacing;
	$true_page_left = $page_left;

	eval {$CA = $rs_data->get("CA")->as_scalar};
	print_bfield_title ("Caller: ", $page_col3_1, $page_left, "Department: ");
	$new_page_left = print_field ("$CA", $page_col3_1, $page_left, "Service Group: ", $page_col3_2);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}

	eval {$JD = $rs_data->get("JD")->as_scalar};
	eval {$JT = $rs_data->get("JT")->as_scalar};
	print_bfield_title ("Date: ", $page_col3_2, $page_left, "Service Group: ");
	$new_page_left = print_field ("$JD $JT", $page_col3_2, $page_left, "Your Reference: ", $page_col3_3);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}

	eval {$RE = $rs_data->get("RE")->as_scalar};
	print_bfield_title ("Your Reference: ", $page_col3_3, $page_left, "Your Reference: ");
	$new_page_left = print_field ("$RE", $page_col3_3, $page_left, "", $page_x - $margin);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}

	$page_left = $true_page_left - $y_small_spacing;

	eval {$SV = $rs_data->get("SV")->as_scalar};
	print_bfield_title ("Service: ", $page_col3_1, $page_left, "Department: ");
	$new_page_left = print_field ("$SV", $page_col3_1, $page_left, "Service Group: ", $page_col3_2);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}

#	eval {$YZ = $rs_data->get("YZ")->as_scalar};
	print_bfield_title ("Service Group: ", $page_col3_2, $page_left, "Service Group: ");
	$new_page_left = print_field ("$YZ", $page_col3_2, $page_left, "Your Reference: ", $page_col3_3);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}
	if ($YZ == 2) { 
		$service = "cab";
	} else {
		$service = "courier";
	}

	eval {$VT = $rs_data->get("VT")->as_scalar};
	print_bfield_title ("Vehicle: ", $page_col3_3, $page_left, "Your Reference: ");
	$new_page_left = print_field ("$VT", $page_col3_3, $page_left, "", $page_x - $margin);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}

	$page_left = $true_page_left - $y_small_spacing;

	eval {$DT = $rs_data->get("DT")->as_scalar};
	print_bfield_title ("Department: ", $page_col3_1, $page_left, "Department: ");
        $new_page_left = print_field ("$DT", $page_col3_1, $page_left, "Service Group: ", $page_col3_2);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}

	eval {$PH = $rs_data->get("PH")->as_scalar};
	print_bfield_title ("Phone: ", $page_col3_2, $page_left, "Service Group: ");
	$new_page_left = print_field ("$PH", $page_col3_2, $page_left, "Your Reference: ", $page_col3_3);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}

	eval {$RJ = $rs_data->get("RJ")->as_scalar};
	print_bfield_title ("Return Job: ", $page_col3_3, $page_left, "Your Reference: ");
	$new_page_left = print_field ("$RJ", $page_col3_3, $page_left, "", $page_x - $margin);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}

	$page_left = $true_page_left - 30;

	$top_of_addresses = $page_left;

	if ($service eq "cab") {
		print_title ("Pickup details", 40, $page_left);
	} else {
		print_title ("Sender", 40, $page_left);
	}

	$page_left = $page_left - $y_small_spacing;

	eval {$PC = $rs_data->get("PC")->as_scalar};
	print_afield_title ("Company: ", $page_col2_1, $page_left);
	$new_page_left = print_field ("$PC", $page_col2_1, $page_left, "Town/Postcode: ", $page_col2_2);
	if ($new_page_left < $page_left) {
		$page_left = $new_page_left;
	}

	$page_left = $page_left - $y_small_spacing;

	eval {$PA = $rs_data->get("PA")->as_scalar};
	print_afield_title ("Address: ", $page_col2_1, $page_left);
	$new_page_left = print_field ("$PA", $page_col2_1, $page_left, "Town/Postcode: ", $page_col2_2);
	if ($new_page_left < $page_left) {
		$page_left = $new_page_left;
	}

	$page_left = $page_left - $y_small_spacing;
	eval {$PS = $rs_data->get("PS")->as_scalar};
	print_afield_title ("Town/Postcode: ", $page_col2_1, $page_left);
	$new_page_left = print_field ("$PS", $page_col2_1, $page_left, "Town/Postcode: ", $page_col2_2);
	if ($new_page_left < $page_left) {
		$page_left = $new_page_left;
	}

	$page_left = $page_left - $y_small_spacing;
	eval {$PX = $rs_data->get("PX")->as_scalar};
	if ($service eq "cab") {
		print_afield_title ("Passenger Name: ", $page_col2_1, $page_left);
	} else {
		print_afield_title ("Contact: ", $page_col2_1, $page_left);
	}
	$new_page_left = print_field ("$PX", $page_col2_1, $page_left, "Town/Postcode: ", $page_col2_2);
	if ($new_page_left < $page_left) {
		$page_left = $new_page_left;
	}

	$page_left = $page_left - $y_small_spacing;
	eval {$PI = $rs_data->get("PI")->as_scalar};
	if ($service eq "cab") {
		print_afield_title ("Phone Number: ", $page_col2_1, $page_left);
	} else {
		print_afield_title ("Instructions: ", $page_col2_1, $page_left);
	}
	$new_page_left = print_field ("$PI", $page_col2_1, $page_left, "Town/Postcode: ", $page_col2_2);
	if ($new_page_left < $page_left) {
		$page_left = $new_page_left;
	}

	#save the old page_left, just case it is lower than the previous one.
	$left_side_page_left = $page_left;
	
	eval {$delivery_address_array = $rs_data->get("delivery_address_array")};
	if (!$@ && $delivery_address_array) {
		$number_of_dropoffs = $delivery_address_array->length;
		$dropoff_count=0;
		while (($array_row = $delivery_address_array->shift())) {
			if ( $array_row->type eq 'hash' ) {
				$dropoff_count++;

				$DC = "";
				$DA = "";
				$DS = "";
				$DI = "";
				$DX = "";

				#Get the data
				#Delivery Company = DC
				eval {$DC = $array_row->get("DC")->as_scalar};
				#Delivery Address = DA
				eval {$DA = $array_row->get("DA")->as_scalar};
				#Delivery Suburb = DS
				eval {$DS = $array_row->get("DS")->as_scalar};
				#Delivery Instructions  = DI
				eval {$DI = $array_row->get("DI")->as_scalar};
				#Delivery Contact = DX
				eval {$DX = $array_row->get("DX")->as_scalar};
				
				#Decide if we have enough space to print on this page
				if (&is_even($dropoff_count)) {
					if ($page_left < $bottom_margin + 120) {
						&print_footer($YZ);
						$page_left = &print_header($JN,$YZ);
					}
					#print on the left hand side of the page
					$page_left = $page_left - 30;

					$top_of_addresses = $page_left;

					if ($service eq "cab") {
						if ($number_of_dropoffs > 1) {
							print_title ("Destination $dropoff_count", 40, $page_left);
						} else {
							print_title ("Destination", 40, $page_left);
						}
					} else {
						if ($number_of_dropoffs > 1) {
							print_title ("Receiver $dropoff_count", 40, $page_left);
						} else {
							print_title ("Receiver", 40, $page_left);
						}
					}

					$page_left = $page_left - $y_small_spacing;

					if ($service eq "courier") {
						print_afield_title ("Company: ", $page_col2_1, $page_left);
						$new_page_left = print_field ("$DC", $page_col2_1, $page_left, "Town/Postcode: ", $page_col2_2);
						if ($new_page_left < $page_left) {
							$page_left = $new_page_left;
						}
				
						$page_left = $page_left - $y_small_spacing;
				
					}
					print_afield_title ("Address: ", $page_col2_1, $page_left);
					$new_page_left = print_field ("$DA", $page_col2_1, $page_left, "Town/Postcode: ", $page_col2_2);
					if ($new_page_left < $page_left) {
						$page_left = $new_page_left;
					}
				
					$page_left = $page_left - $y_small_spacing;
					print_afield_title ("Town/Postcode: ", $page_col2_1, $page_left);
					$new_page_left = print_field ("$DS", $page_col2_1, $page_left, "Town/Postcode: ", $page_col2_2);
					if ($new_page_left < $page_left) {
						$page_left = $new_page_left;
					}
				
					
					if ($service eq "courier") {
						$page_left = $page_left - $y_small_spacing;
						print_afield_title ("Contact: ", $page_col2_1, $page_left);
						$new_page_left = print_field ("$DX", $page_col2_1, $page_left, "Town/Postcode: ", $page_col2_2);
						if ($new_page_left < $page_left) {
							$page_left = $new_page_left;
						}
				
						$page_left = $page_left - $y_small_spacing;
						print_afield_title ("Instructions: ", $page_col2_1, $page_left);
						$new_page_left = print_field ("$DI", $page_col2_1, $page_left, "Town/Postcode: ", $page_col2_2);
						if ($new_page_left < $page_left) {
							$page_left = $new_page_left;
						}
					}
					#save the old page_left, just case it is lower than the previous one.
					$left_side_page_left = $page_left;
				} else {
					#print on the right hand side
					$page_left = $top_of_addresses;
					

					if ($service eq "cab") {
						$field_width = PDF_stringwidth($p, "Town/Postcode: ", $font, $field_font_size);
						if ($number_of_dropoffs > 1) {
							print_title ("Destination $dropoff_count", $page_col2_2 - $field_width, $page_left);
						} else {
							print_title ("Destination", $page_col2_2 - $field_width, $page_left);
						}
					} else {
						$field_width = PDF_stringwidth($p, "Town/Postcode: ", $font, $field_font_size);
						if ($number_of_dropoffs > 1) {
							print_title ("Receiver $dropoff_count", $page_col2_2 - $field_width, $page_left);
						} else {
							print_title ("Receiver", $page_col2_2 - $field_width, $page_left);
						}
					}
					if ($service eq "courier") {
						$page_left = $page_left - $y_small_spacing;

						print_afield_title ("Company: ", $page_col2_2, $page_left);
						$new_page_left = print_field ("$DC", $page_col2_2, $page_left, "", $page_x - $margin);
						if ($new_page_left < $page_left) {
							$page_left = $new_page_left;
						}
				
					}
					$page_left = $page_left - $y_small_spacing;
				
					print_afield_title ("Address: ", $page_col2_2, $page_left);
					$new_page_left = print_field ("$DA", $page_col2_2, $page_left, "", $page_x - $margin);
					if ($new_page_left < $page_left) {
						$page_left = $new_page_left;
					}
				
					$page_left = $page_left - $y_small_spacing;
					print_afield_title ("Town/Postcode: ", $page_col2_2, $page_left);
					$new_page_left = print_field ("$DS", $page_col2_2, $page_left, "", $page_x - $margin);
					if ($new_page_left < $page_left) {
						$page_left = $new_page_left;
					}
				
					if ($service eq "courier") {
						$page_left = $page_left - $y_small_spacing;
						print_afield_title ("Contact: ", $page_col2_2, $page_left);
						$new_page_left = print_field ("$DX", $page_col2_2, $page_left, "", $page_x - $margin);
						if ($new_page_left < $page_left) {
							$page_left = $new_page_left;
						}
					
						$page_left = $page_left - $y_small_spacing;
						print_afield_title ("Instructions: ", $page_col2_2, $page_left);
						$new_page_left = print_field ("$DI", $page_col2_2, $page_left, "", $page_x - $margin);
						if ($new_page_left < $page_left) {
							$page_left = $new_page_left;
						}
					}
				
					#make sure we return the correct pageleft so that the next row doesn't
					# write over this data
					if ($left_side_page_left < $page_left) {
						$page_left = $left_side_page_left;
					}
				}
					
			}
		}
	}

###################### 
	if ($page_left < $bottom_margin + 60) {
		&print_footer($YZ);
		$page_left = &print_header($JN,$YZ);
	} 

	$page_left = $page_left - 30;
	$font = PDF_findfont($p, $field_title_font, "host", 0);
	$field_width = PDF_stringwidth($p, "Date: ", $font, $field_font_size);
	print_title ("Advance Booking", 40, $page_left);
	$font = PDF_findfont($p, $field_title_font, "host", 0);
	if ($service eq "cab") {
		$field_width = PDF_stringwidth($p, "Number of Passengers: ", $font, $field_font_size);
		print_title ("Passenger Information", $page_col4_3 - $field_width, $page_left);
	} else {
		$field_width = PDF_stringwidth($p, "Number of Items: ", $font, $field_font_size);
		print_title ("Goods", $page_col4_3 - $field_width, $page_left);
	}

	$page_left = $page_left - $y_small_spacing;

	$true_page_left = $page_left;
	eval {$RA = $rs_data->get("RA")->as_scalar};
	print_bfield_title ("Date: ", $page_col4_1, $page_left, "Service: ");
	$new_page_left = &print_field ("$RA", $page_col4_1, $page_left, "Length: ", $page_col4_2);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}
				
	eval {$RO = $rs_data->get("RO")->as_scalar};
	print_bfield_title ("Time: ", $page_col4_2, $page_left, "Length: ");
	$new_page_left = &print_field ("$RO", $page_col4_2, $page_left, "Number of Items: ", $page_col4_3);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}

	eval {$PN = $rs_data->get("PN")->as_scalar};
	if ($service eq "cab") {
		print_bfield_title ("Number of Passengers: ", $page_col4_3, $page_left, "Number of Passengers: ");
	} else {
		print_bfield_title ("Number of Items: ", $page_col4_3, $page_left, "Number of Items: ");
	}
	$new_page_left = &print_field ("$PN", $page_col4_3, $page_left, "Cubic Metres: ", $page_col4_4);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}

	if ($service eq "courier") {
		eval {$WE = $rs_data->get("WE")->as_scalar};
		print_bfield_title ("Weight: ", $page_col4_4, $page_left, "Cubic Metres: ");
		$new_page_left = &print_field ("$WE kg", $page_col4_4, $page_left, "", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
	}

	if ($true_page_left < $page_left) {
		$page_left = $true_page_left;
	}

###################### 
	if ($service eq "courier") {
		if ($page_left < $bottom_margin + 60) {
			&print_footer($YZ);
			$page_left = &print_header($JN,$YZ);
		} 
		$page_left = $page_left - 30;
		$font = PDF_findfont($p, $field_title_font, "host", 0);
		$field_width = PDF_stringwidth($p, "Height: ", $font, $field_font_size);
		print_title ("Cubic Dimensions", 40, $page_left);
		$page_left = $page_left - $y_small_spacing;
	
		$true_page_left = $page_left;
	
		eval {$HE = $rs_data->get("HE")->as_scalar};
		print_bfield_title ("Height: ", $page_col4_1, $page_left, "Service: ");
		$new_page_left = print_field ("$HE cm", $page_col4_1, $page_left, "Length: ", $page_col4_2);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
	
		eval {$PL = $rs_data->get("PL")->as_scalar};
		print_bfield_title ("Length: ", $page_col4_2, $page_left, "Length: ");
		$new_page_left = print_field ("$PL cm", $page_col4_2, $page_left, "Number of Items: ", $page_col4_3);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
	
		eval {$PD = $rs_data->get("PD")->as_scalar};
		print_bfield_title ("Depth: ", $page_col4_3 , $page_left, "Number of Items: ");
		$new_page_left = print_field ("$PD cm", $page_col4_3 , $page_left, "Cubic Metres: ", $page_col4_4);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
	
	
		print_bfield_title ("Cubic Metres: ", $page_col4_4 , $page_left, "Cubic Metres: ");
		eval {$CU = $rs_data->get("CU")->as_scalar};

		$new_page_left = print_field ("$CU m" . pack("c", 179) , $page_col4_4 , $page_left, "", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
		if ($true_page_left < $page_left) {
			$page_left = $true_page_left;
		}
	}

###################### 
	if ($page_left < $bottom_margin + 60) {
		&print_footer($YZ);
		$page_left = &print_header($JN,$YZ);
	} 

	$page_left = $page_left - 30;
	$font = PDF_findfont($p, $field_title_font, "host", 0);
	$field_width = PDF_stringwidth($p, "Service: ", $font, $field_font_size);
	print_title ("Costs", 40, $page_left);

	$page_left = $page_left - $y_small_spacing;

	$true_page_left = $page_left;

	eval {$BP = $rs_data->get("BP")->as_scalar};
	print_bfield_title ("Service: ", $page_col4_1, $page_left, "Service: ");
	$ukpound = pack("U", 0xc2a3);	
	$default_encoding = "builtin";
	$new_page_left = print_field ("". $ukpound . "$BP", $page_col4_1, $page_left, "Length: ", $page_col4_2);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}

	eval {$GS = $rs_data->get("GS")->as_scalar};
	print_bfield_title ("VAT: ", $page_col4_2, $page_left, "Length: ");
	$new_page_left = print_field ($ukpound . "$GS", $page_col4_2, $page_left, "Number of Items: ", $page_col4_3);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}

	eval {$TT = $rs_data->get("TT")->as_scalar};
	print_bfield_title ("Total Charges: ", $page_col4_3 , $page_left, "Number of Items: ");
	$new_page_left = print_field ($ukpound . "$TT", $page_col4_3 , $page_left, "", $page_col4_4);
	if ($new_page_left < $true_page_left) {
		$true_page_left = $new_page_left;
	}
	if ($true_page_left < $page_left) {
		$page_left = $true_page_left;
	}
        $default_encoding = "host";

###################### 

	if ($page_left < $bottom_margin + 60) {
		&print_footer($YZ);
		$page_left = &print_header($JN,$YZ);
	} 

	$page_left = $page_left - 30;
	$font = PDF_findfont($p, $field_title_font, "host", 0);
	print_title ("Tracking Details", 40, $page_left);

	$page_left = $page_left - $y_small_spacing;
	# Print column titles
	&print_tracking_column_heading ("Date", $tracking_col_1, $page_left);
	&print_tracking_column_heading ("Time", $tracking_col_2, $page_left);
	&print_tracking_column_heading ("Event", $tracking_col_3, $page_left);
	

	eval {$tracking_details_array = $rs_data->get("tracking_details_array")};
	if (!$@ && $tracking_details_array) {
		while (($array_row = $tracking_details_array->shift())) {
			$page_left = $page_left - $y_small_spacing;
			if ($page_left < $bottom_margin + 60) {
				#Print continued next page
				&print_field ("Continued on the next page ...", $tracking_col_1, $page_left, "", $page_x - $margin);
				&print_footer($YZ);
				$page_left = &print_header($JN,$YZ);
				$page_left = $page_left - 30;
			}

			if ( $array_row->type eq 'hash' ) {
				#Track Date = AD
				eval {$AD = $array_row->get("AD")->as_scalar};
				#Track Time = AT
				eval {$AT = $array_row->get("AT")->as_scalar};
				#Track Event = AE
				eval {$AE = $array_row->get("AE")->as_scalar};
			}
			$true_page_left = $page_left;
		
			$new_page_left = print_field ("$AD", $tracking_col_1, $page_left, "", $tracking_col_2);
			if ($new_page_left < $true_page_left) {
				$true_page_left = $new_page_left;
			}
			$new_page_left = print_field ("$AT", $tracking_col_2, $page_left, "", $tracking_col_3);
			if ($new_page_left < $true_page_left) {
				$true_page_left = $new_page_left;
			}
		
			$new_page_left = print_field ("$AE", $tracking_col_3, $page_left, "", $page_x - $margin);
			if ($new_page_left < $true_page_left) {
				$true_page_left = $new_page_left;
			}
			if ($true_page_left < $page_left) {
				$page_left = $true_page_left;
			}
		
		}
	}


###################### Signatures

	eval {$signatures_array = $rs_data->get("signatures_array")};
	if (!$@ && $signatures_array) {
		if ($page_left < $bottom_margin + 100) {
			&print_footer($YZ);
			$page_left = &print_header($JN,$YZ);
		} 

		$page_left = $page_left - 30;
		$font = PDF_findfont($p, $field_title_font, "host", 0);
		print_title ("POD & Waiting Time (WT) Signatures", 40, $page_left);

#		$page_left = $page_left - $y_small_spacing;
	# Print column titles

		$number_of_sigs = $signatures_array->length;
		while (($array_row = $signatures_array->shift())) {
		
			$page_left = $page_left - $y_small_spacing;
			if ($page_left < $bottom_margin + 60) {
				#Print continued next page
				&print_field ("Continued on the next page ...", $tracking_col_1, $page_left, "", $page_x - $margin);
				&print_footer($YZ);
				$page_left = &print_header($JN,$YZ);
				$page_left = $page_left - 30;
			}

			if ( $array_row->type eq 'hash' ) {
				#Tagcount stop = TC
				eval {$TC = $array_row->get("TC")->as_scalar};
				#Stop Type = ST
				eval {$ST = $array_row->get("ST")->as_scalar};
				#JPEG = JM
				eval {$JM = $array_row->get("JM")->as_scalar};
				# Name = NM
				eval {$NM = $array_row->get("NM")->as_scalar};
				# Address = AY
				eval {$AY = $array_row->get("AY")->as_scalar};
			}
			
			$true_page_left = $page_left;
			if ($JM) {
				if (-s $JM)
				{
				} else {
					$JM = "/usr/local/server/brtc/csfo_pod/missingsig.jpg";
				}
				$image = PDF_open_image_file($p, "jpeg", $JM, "", 0);
				die "Couldn't open image '$JM'" if ($image == -1);
				$width = PDF_get_value($p, "imagewidth", $image);
				$height = PDF_get_value($p, "imageheight", $image);
				PDF_place_image($p, $image, 40, $page_left - $height, 1);
				PDF_close_image($p, $image);
				#unlink $JM;
				$page_left = $page_left - $height;
				$temp = $TC." ".$ST."  Name: ".$NM."   Address: ".$AY;
#				print_field_title ($temp, $page_col4_1, $page_left);
				$new_page_left = print_field ("$temp", $tracking_col_1, $page_left, "", $page_x - $margin);
				if ($new_page_left < $true_page_left) {
					$true_page_left = $new_page_left;
				}
				if ($true_page_left < $page_left) {
					$page_left = $true_page_left;
				}
			}	
		
		}
	}



&print_footer($YZ);

PDF_close($p);

PDF_delete($p);

sub is_even {
	my ($number) = @_;
	if ($number % 2 == 0) {
		return 1;
	} else {
		return 0;
	}
}

sub print_field {
	my ($field, $left_x, $left_y, $next_field, $next_col) = @_;
	$font = PDF_findfont($p, $field_title_font, "host", 0);
	$max_width = $next_col - $left_x - PDF_stringwidth($p, $next_field, $font, $field_title_font_size);
	#print "$next_col $left_x " . PDF_stringwidth($p, $next_field, $font, $field_title_font_size) . "\n";
	$height_increment = $field_font_size;
	$my_height = $height_increment;

	$font = PDF_findfont($p, $field_font, $default_encoding, 0);
	PDF_setfont($p, $font, $field_font_size);

	# Let's find out if the line will fit in
	if ($max_width > PDF_stringwidth($p, $field, $font, $field_font_size)) {
		# just print it
		PDF_show_xy($p, $field, $left_x, $left_y);
	} else {
		$* = 1;
		$i = 0;
		@field_array = split(' ', $field);
		for ($i=0;$i < $#field_array + 1;$i++) {
			if ($the_line) {
				$the_line = $the_line . " " . @field_array[$i];
			} else {
				$the_line = @field_array[$i];
			}
			if ($max_width > PDF_stringwidth($p, $the_line, $font, $field_font_size)) {
				if ($i < $#field_array) {
					$temp_line = $the_line . " " . " " . @field_array[$i + 1];
					if ($max_width < PDF_stringwidth($p, $temp_line, $font, $field_font_size)) {
						# we know we cannot add anymore words so let's print the line.
						#print "1 $#field_array, $i, $the_line, $temp_line \n";
						PDF_show_xy($p, $the_line, $left_x, $left_y);
						$left_y = $left_y - ($height_increment + 2);
						$the_line="";
					}
#print "here 1\n";
				} else {
						#print "2 $#field_array, $i, $the_line, $temp_line \n";
					PDF_show_xy($p, $the_line, $left_x, $left_y);
					$the_line="";
#print "here 2\n";
				}
			}
		}
	}
						#print "$#field_array, $i, $the_line, $temp_line \n";
	return $left_y;
}

sub print_title {
	my ($title, $left_x, $left_y) = @_;
	
	$font = PDF_findfont($p, $title_font, "host", 0);
	PDF_setfont($p, $font, $title_font_size);
	PDF_set_text_pos($p, $left_x, $left_y);
	PDF_show($p, $title);
}

sub print_tracking_column_heading {
	my ($title, $left_x, $left_y) = @_;
	
	$font = PDF_findfont($p, $field_title_font, "host", 0);
	PDF_setfont($p, $font, $field_title_font_size);
	PDF_set_text_pos($p, $left_x, $left_y);
	PDF_show($p, $title);
}

sub print_field_title {
	my ($title, $right_x, $left_y) = @_;
	
	$font = PDF_findfont($p, $field_title_font, "host", 0);
	PDF_setfont($p, $font, $field_title_font_size);
	PDF_set_text_pos($p, $right_x - PDF_stringwidth($p, $title, $font, $field_title_font_size), $left_y);
	PDF_show($p, $title);
}

sub print_afield_title {
        my ($title, $right_x, $left_y) = @_;

        $font = PDF_findfont($p, $field_title_font, "host", 0);
        PDF_setfont($p, $font, $field_title_font_size);
        PDF_set_text_pos($p, $right_x - PDF_stringwidth($p, "Town/Postcode: ", $font, $field_title_font_size), $left_y);
        PDF_show($p, $title);
}

sub print_bfield_title {
        my ($title, $right_x, $left_y, $longest) = @_;

        $font = PDF_findfont($p, $field_title_font, "host", 0);
        PDF_setfont($p, $font, $field_title_font_size);
        PDF_set_text_pos($p, $right_x - PDF_stringwidth($p, $longest, $font, $field_title_font_size), $left_y);
        PDF_show($p, $title);
}

sub print_header {
	my($title,$serv_logo) = @_;
	PDF_begin_page($p, $page_x, $page_y);
        if ($serv_logo == 2) {
          $imagefile = "./westonelogo.jpg";
        } else {
	  $imagefile = "./cslogo.jpg";
        }
	$image = PDF_open_image_file($p, "jpeg", $imagefile, "", 0);
	die "Couldn't open image '$imagefile'" if ($image == -1);
	$width = PDF_get_value($p, "imagewidth", $image);
	$height = PDF_get_value($p, "imageheight", $image);
	PDF_place_image($p, $image, 40, $page_y - 40 - $height, 1);
	PDF_close_image($p, $image);
	$font = PDF_findfont($p, "Helvetica", "host", 0);
	PDF_setfont($p, $font, 10.0);
	PDF_set_text_pos($p, 510, $page_y - 40);
	$page_number++;
	PDF_show($p, "Page $page_number");
	$font = PDF_findfont($p, "Helvetica", "host", 0);
	PDF_setfont($p, $font, 15.0);
	PDF_set_text_pos($p, 365, $page_y - 40 - $height);
	PDF_show($p, "Job Number: $title");
	return $page_y - 40 - $height;
}

sub print_footer {
	my ($serv_grp) = @_;
	$font = PDF_findfont($p, "Helvetica", "host", 0);
	$italicFont = PDF_findfont($p, "Helvetica-Oblique", "host", 0);
	PDF_setfont($p, $font, 8.0);
	if ($serv_grp == 2) {
        PDF_set_text_pos($p, 155, 59);
        PDF_show($p, "Contact Us:");
        PDF_setfont($p, $font, 7.0);
	PDF_set_text_pos($p, 155, 50);
	#if ($serv_grp == 2) {
	    PDF_show($p, "Website: www.westonecars.co.uk");
	    PDF_set_text_pos($p, 155,41);
            PDF_show($p, "Email: customerservices\@westonecars.co.uk");
	    PDF_set_text_pos($p, 155, 32);
	    PDF_show($p, "Telephone: Customer Services");
            PDF_set_text_pos($p, 155, 23);
            PDF_show($p, "07004 WESTONE (07004 937 8663)");
            PDF_set_text_pos($p, 155, 14);
            } else {
	    #PDF_show($p, "Website: www.citysprint.co.uk");
	    #PDF_set_text_pos($p, 155,41);
        #    PDF_show($p, "Email: customerservices\@citysprint.co.uk");
	    #PDF_set_text_pos($p, 155, 32);
	    #PDF_show($p, "Telephone: Customer Services");
         #   PDF_set_text_pos($p, 155, 23);
          #  PDF_show($p, "London on 0207 203 7590");
           # PDF_set_text_pos($p, 155, 14);
            #PDF_show($p, "Outside London on 08707 510076");
	    }	
# Print Office details
        PDF_setfont($p, $font, 8.0);
        PDF_set_text_pos($p, 20, 59);
	if ($serv_grp == 2) {
	   PDF_show($p, "WestOne Cars (UK) Ltd");
	} else {
	   PDF_show($p, "CitySprint (UK) Ltd");
	}	
        PDF_setfont($p, $italicFont, 7.0);
        PDF_set_text_pos($p, 20, 50);
        PDF_show($p, "Registered office");
		
		PDF_setfont($p, $font, 7.0);
		 PDF_set_text_pos($p, 76, 50);
        PDF_show($p, "Ground Floor,");
        PDF_set_text_pos($p, 20, 41);
        PDF_show($p, "RedCentral, 60 High Street,");
        PDF_set_text_pos($p, 20, 32);
        PDF_show($p, "Redhill, Surrey RH1 1SH ");
        PDF_set_text_pos($p, 20, 23);
		
		
			
			
        	
			PDF_setfont($p, $italicFont, 7.0);
		#PDF_show($p, "Registered No. 4327611 (England)");
		PDF_show($p, "Registered No.");

		if($serv_grp == 2 ) {
	
	  #PDF_show($p, "Registered No. 4496350 (England)");
          PDF_set_text_pos($p, 20, 14);
          PDF_show($p, "VAT No. 799 1128 87");
	} else {
        #PDF_show($p, "Registered No. 4327611 (England)");
		
        #PDF_set_text_pos($p, 20, 14);
        #PDF_show($p, "VAT No. 787 9900 56");
		
		PDF_setfont($p, $font, 7.0);
		PDF_set_text_pos($p, 68, 23);
		PDF_show($p, "4327611 (England)");
        PDF_set_text_pos($p, 20, 14);
        PDF_show($p, "VAT No. 997 3273 64");
	}


# Print the regional centres
	if($serv_grp == 1) {
        PDF_setfont($p, $font, 7.0);
        PDF_set_text_pos($p, 230, 59);
        PDF_show($p, "Aberdeen");
        PDF_set_text_pos($p, 230, 50);
        PDF_show($p, "Basingstoke");
        PDF_set_text_pos($p, 230, 41);
        PDF_show($p, "Birmingham");
        PDF_set_text_pos($p, 230, 32);
		PDF_show($p, "Bracknell");
		
        
        PDF_set_text_pos($p, 230, 23);
		PDF_show($p, "Brentwood");
		
		PDF_set_text_pos($p, 230, 14);
		PDF_show($p, "Brighton");
		
		PDF_set_text_pos($p, 285, 59);
        PDF_show($p, "Bristol");
        PDF_set_text_pos($p, 285, 50);
        PDF_show($p, "Cambridge");

        PDF_set_text_pos($p, 285, 41);
        PDF_show($p, "Cardiff");
        PDF_set_text_pos($p, 285, 32);
        #PDF_show($p, "Cheltenham");
        #PDF_set_text_pos($p, 285, 41);
        #PDF_show($p, "Coventry");
        #PDF_set_text_pos($p, 285, 32);
        PDF_show($p, "Croydon");
        #PDF_set_text_pos($p, 285, 41);
        #PDF_show($p, "Docklands");
        PDF_set_text_pos($p, 285, 23);
        PDF_show($p, "Edinburgh");
		PDF_set_text_pos($p, 285, 14);
        PDF_show($p, "Enfield");
		
        PDF_set_text_pos($p, 340, 59);
        PDF_show($p, "Gatwick");
        PDF_set_text_pos($p, 340, 50);
        PDF_show($p, "Glasgow");

        PDF_set_text_pos($p, 340, 41);
        PDF_show($p, "Guildford");

        PDF_set_text_pos($p, 340, 32);
        PDF_show($p, "Heathrow");
        PDF_set_text_pos($p, 340, 23);
        PDF_show($p, "Hemel Hempstead");
        PDF_set_text_pos($p, 340, 14);
        PDF_show($p, "High Wycombe");
		
		
        #PDF_set_text_pos($p, 340, 23);
        #PDF_show($p, "Ipswich");
        PDF_set_text_pos($p, 410, 59);
        PDF_show($p, "Leeds");

        PDF_set_text_pos($p, 410, 50);
        PDF_show($p, "London");

		PDF_set_text_pos($p, 410, 41);
        PDF_show($p, "London Logistics");
        #PDF_set_text_pos($p, 410, 59);
        #PDF_show($p, "Maidstone");
        PDF_set_text_pos($p, 410, 32);
        PDF_show($p, "Manchester");
        PDF_set_text_pos($p, 410, 23);
        PDF_show($p, "Medway");
        #PDF_set_text_pos($p, 410, 32);
        #PDF_show($p, "MidlandCentre");
        PDF_set_text_pos($p, 410, 14);
        PDF_show($p, "Milton Keynes");
        #PDF_set_text_pos($p, 410, 23);
        #PDF_show($p, "Newbury");
		
		PDF_set_text_pos($p, 473, 59);
        PDF_show($p, "Newcastle");
		
        PDF_set_text_pos($p, 473, 50);
        PDF_show($p, "Norwich");

        #PDF_set_text_pos($p, 473, 59);
        #PDF_show($p, "Northampton");
        PDF_set_text_pos($p, 473, 41);
        PDF_show($p, "Nottingham");
        PDF_set_text_pos($p, 473, 32);
        PDF_show($p, "Oxford");
        PDF_set_text_pos($p, 473, 23);
        PDF_show($p, "Plymouth");
		
		PDF_set_text_pos($p, 473, 14);
        PDF_show($p, "Reading");
		
		PDF_set_text_pos($p, 520, 59);
        PDF_show($p, "Slough");
		
        PDF_set_text_pos($p, 520, 50);
        PDF_show($p, "Southampton");
				
        PDF_set_text_pos($p, 520, 41);
        PDF_show($p, "Swindon");
        #PDF_set_text_pos($p, 473, 14);
        #PDF_show($p, "Thames Valley");

        PDF_set_text_pos($p, 520, 32);
        PDF_show($p, "Telford");
		
		PDF_set_text_pos($p, 520, 23);
        PDF_show($p, "Warwick");
	}

	PDF_end_page($p);
}
#Job Number = JN
#Job Date = JD
#Job Time = JT
#Department = DT
#Reference = RE
#Caller = CA
#Service = SV
#Service Group = YZ
#Vehicle = VT
#Phone = PH
#Return Job = RJ
#Advance Booking = AB
#Ready At = RA
#Ready On = RO
#Price = BP
#GST/VAT = GS
#Pickup Company = PC
#Pickup Address = PA
#Pickup Suburb = PS
#Pickup Instructions = PI
#Pickup Contact = PX
#Weight =WE 
#No of Items = PN
#Height = PH
#Length = PL
#Depth = PD
#Delivery Company = DC
#Delivery Address = DA
#Delivery Suburb = DS
#Delivery Instructions  = DI
#Delivery Contact = DX
#Track Date = AD
#Track Time = AT
#Track Event = AE
# test url
# http://10.1.1.1/cnet/src/couriernote.php?JN=12323&JD=10/1/01&JT=10:10:11&DT=Dept. one&RE=My ref&CA=robh&SV=110306+SERV+(P/U+BY+3pm)&SG=1&VT=Motor Bike&PH=(02)93653520&RJ=Yes&AB=Yes&RA=10:10:12&RO=10/2/02&BP=10000&GS=1000&PC=TEST+COMPANY+2&PA=79+JONES+STREET&PS=NORWICH&PI=&PX=robh&WE=100&PN=5&PH=100&PL=10&DC1=TEST+3+COMPANY&DA1=12+HEWITT+ST&DS1=LONDON,+GREATER+LONDON&DX1=QWERTY&DI1=+&AD1=10/10/00&AT1=10:10:10&AE1=Pickup
