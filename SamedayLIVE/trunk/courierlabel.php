<?php
// Customer Key = CK
// Password = PW
//Transaction Stamp = TS
//Job Number = JN
//Job Date = JD
//Job Time = JT
//Department = DT
//Reference = RE
//Caller = CA
//Service = SV
//Service Group = YZ
//Vehicle = VT
//Phone = PH
//Return Job = RJ
//Advance Booking = AB
//Ready At = RA
//Ready On = RO


//Pickup Company = PC
//Pickup Address = PA
//Pickup Address2 = P2
//Pickup Suburb = PS
//Pickup County	= P5
//Pickup Postcode = PP
//Pickup Instructions = PI
//Pickup Contact = PX
//Weight = WE 
//No of Items = PN
//Height = PH
//Length = PL
//Depth = PD
//Delivery Company = DC
//Delivery Address = DA
//Delivery Address2 = D2
//Delivery Suburb = DS
//Delivery County = DO
//Delivery Postcode = DP
//Delivery Instructions  = DI
//Delivery Contact = DX

$packet_id = wddx_packet_start();

$xml_packet_start="<wddxPacket version='1.0'><header></header><data><struct>";
$xml_packet_end="</struct></data></wddxPacket>";
//Customer key= CK
if (isset ($CK)) {
	wddx_add_vars($packet_id, "CK");
}
//Job Number = JN
if (isset ($JN)) {
	wddx_add_vars($packet_id, "JN");
}
//Job Date = JD
if (isset ($JD)) {
	wddx_add_vars($packet_id, "JD");
}
//Job Time = JT
if (isset ($JT)) {
	wddx_add_vars($packet_id, "JT");
}
//Department = DT
if (isset ($DT)) {
	wddx_add_vars($packet_id, "DT");
}
//Reference = RE
if (isset ($RE)) {
	wddx_add_vars($packet_id, "RE");
}
//Caller = CA
if (isset ($CA)) {
	wddx_add_vars($packet_id, "CA");
}
//Service = SV
if (isset ($SV)) {
	wddx_add_vars($packet_id, "SV");
}
//Service Group = YZ
if (isset ($YZ)) {
	wddx_add_vars($packet_id, "YZ");
}
//Vehicle = VT
if (isset ($VT)) {
	wddx_add_vars($packet_id, "VT");
}
//Phone = PH
if (isset ($PH)) {
	wddx_add_vars($packet_id, "PH");
}
//Return Job = RJ
if (isset ($RJ)) {
	wddx_add_vars($packet_id, "RJ");
}
//Advance Booking = AB
if (isset ($AB)) {
	wddx_add_vars($packet_id, "AB");
}
//Ready At = RA
if (isset ($RA)) {
	wddx_add_vars($packet_id, "RA");
}
//Ready On = RO
if (isset ($RO)) {
	wddx_add_vars($packet_id, "RO");
}
//Pickup Company = PC
if (isset ($PC)) {
	wddx_add_vars($packet_id, "PC");
}
//Pickup Address = PA
if (isset ($PA)) {
	wddx_add_vars($packet_id, "PA");
}
//Pickup Address2 = P2
if (isset ($P2)) {
	wddx_add_vars($packet_id, "P2");
}
//Pickup Suburb = PS
if (isset ($PS)) {
	wddx_add_vars($packet_id, "PS");
}
//Pickup County = P5
if (isset ($P5)) {
	wddx_add_vars($packet_id, "P5");
}
//Pickup Postcode = PP
if (isset ($PP)) {
	wddx_add_vars($packet_id, "PP");
}
//Pickup Instructions = PI
if (isset ($PI)) {
	wddx_add_vars($packet_id, "PI");
}
//Pickup Contact = PX
if (isset ($PX)) {
	wddx_add_vars($packet_id, "PX");
}
//Weight = WE 
if (isset ($WE)) {
	wddx_add_vars($packet_id, "WE");
}
//No of Items = PN
if (isset ($PN)) {
	wddx_add_vars($packet_id, "PN");
}
//Height = HE
if (isset ($HE)) {
	wddx_add_vars($packet_id, "HE");
}
//Length = PL
if (isset ($PL)) {
	wddx_add_vars($packet_id, "PL");
}
//Depth = PD
if (isset ($PD)) {
	wddx_add_vars($packet_id, "PD");
}
//Cubice m = CU
if (isset ($CU)) {
	wddx_add_vars($packet_id, "CU");
}

// Do delivery address
$tagcount = 0;
$finished=FALSE;
$index=0;

while (!$finished) {
    	$tagcount += 1;
	//Delivery Company = DCn
	$gen_var = "DC" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DC'] = $$gen_var;
	}
	//Delivery Address = DAn
	$gen_var = "DA" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DA'] = $$gen_var;
	} else {
		$finished = TRUE;
	}
	//Delivery Address2 = D2n
	$gen_var = "D2" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['D2'] = $$gen_var;
	} else {
		$finished = TRUE;
	}
	//Delivery Suburb = DSn
	$gen_var = "DS" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DS'] = $$gen_var;
	}
	//Delivery County = DOn
	$gen_var = "DO" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DO'] = $$gen_var;
	} else {
		$finished = TRUE;
	}
	//Delivery Postcode = DPn
	$gen_var = "DP" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DP'] = $$gen_var;
	} else {
		$finished = TRUE;
	}
	//Delivery Instructions  = DIn
	$gen_var = "DI" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DI'] = $$gen_var;
	}
	//Delivery Contact = DXn
	$gen_var = "DX" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DX'] = $$gen_var;
	}
	$index++;
}
	
if ($delivery_address_array) {
	wddx_add_vars($packet_id, "delivery_address_array");
}

$xml_packet = wddx_packet_end($packet_id); 
//print (htmlentities($xml_packet));

        $filename = "/tmp/pdf_xml_data." . $JN . date("U");
        // let's create a temporary file
        $fp=fopen($filename,"w");
        fwrite ($fp, $xml_packet);
        fclose($fp);
        $pdf_filename = "/tmp/pdf_file_" . $JN . date("U") . ".pdf";
        $full_pdf_filename = $DOCUMENT_ROOT . $pdf_filename;
		$com = 'perl /usr/local/server/brtc/'.dirname($_SERVER['PHP_SELF']).'/courierlabel.pl ' . $filename . " > " . $full_pdf_filename;
        $exec_return = exec ($com);
        print "<meta http-equiv=\"Refresh\" content=\"0; URL=$pdf_filename\">";

//        unlink ($filename);


?>
