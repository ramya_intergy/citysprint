<?php
/*
AD =  Tracking Date		AE = Tracking Event				AT = Tracking Time
BP = Price				AB = Advance Booking			CA = Caller
CK = Customer Key		DA = Delivery Address			DY = Signature
DC = Delivery Company	DI = Delivery Instructions		DS = Delivery Suburb
DT = Department 		DX = Delivery Contact 			DM = Delivery / Pickup time
JN = Job Number 		JD = Job Date					JT = Job Time					
GS = VAT/GST			PW = Password					PC = Pickup Company
PA = Pickup Address		PD = Depth						PH = Height
PS = Pickup Suburb		PI = Pickup Instructions		PN = Number of items
PL = Length				PX = Pickup contact				
RA = Ready At			RE = Reference					RO = Ready On
SV = Service			TS = Transaction Stamp			YZ = Service Group
VT = Vehicle			RJ = Return Job					WE = Weight

// test url
// http://10.1.1.1/cnet/src/couriernote.php?JN=12323&JD=10/1/01&JT=10:10:11&DT=Dept.%20one&RE=My%20ref&CA=robh&SV=110306+SERV+(P/U+BY+3pm)&YZ=1&VT=Motor%20Bike&PH=(02)93653520&RJ=Yes&AB=Yes&RA=10:10:12&RO=10/2/02&BP=10000&GS=1000&PC=TEST+COMPANY+2&PA=79+JONES+STREET&PS=NORWICH&PI=&PX=robh&WE=100&PN=5&PH=100&PL=10&PD=1&DC1=TEST+3+COMPANY&DA1=12+HEWITT+ST&DS1=LONDON,+GREATER+LONDON&DX1=QWERTY&DI1=+&AD1=10/10/00&AT1=10:10:10&AE1=Pickup&AD2=10/10/00&AT2=10:20:10&AE2=Deliver/couriernote.pdf
*/

$t2b = '/usr/local/server/brtc/graphics/bin/t2b';
$btop = '/usr/local/server/brtc/graphics/bin/bmptopnm';
$ptoj = '/usr/local/server/brtc/graphics/bin/pnmtojpeg';

$packet_id = wddx_packet_start();

$xml_packet_start="<wddxPacket version='1.0'><header></header><data><struct>";
$xml_packet_end="</struct></data></wddxPacket>";
//Job Number = JN
if (isset ($JN)) {
	wddx_add_vars($packet_id, "JN");
}
//Job Date = JD
if (isset ($JD)) {
	wddx_add_vars($packet_id, "JD");
}
//Job Time = JT
if (isset ($JT)) {
	wddx_add_vars($packet_id, "JT");
}
//Department = DT
if (isset ($DT)) {
	wddx_add_vars($packet_id, "DT");
}
//Reference = RE
if (isset ($RE)) {
	wddx_add_vars($packet_id, "RE");
}
//Caller = CA
if (isset ($CA)) {
	wddx_add_vars($packet_id, "CA");
}
//Service = SV
if (isset ($SV)) {
	wddx_add_vars($packet_id, "SV");
}
//Service Group = YZ
if (isset ($YZ)) {
	wddx_add_vars($packet_id, "YZ");
}
//Vehicle = VT
if (isset ($VT)) {
	wddx_add_vars($packet_id, "VT");
}
//Phone = PH
if (isset ($PH)) {
	wddx_add_vars($packet_id, "PH");
}
//Return Job = RJ
if (isset ($RJ)) {
	wddx_add_vars($packet_id, "RJ");
}
//Advance Booking = AB
if (isset ($AB)) {
	wddx_add_vars($packet_id, "AB");
}
//Ready At = RA
if (isset ($RA)) {
	wddx_add_vars($packet_id, "RA");
}
//Ready On = RO
if (isset ($RO)) {
	wddx_add_vars($packet_id, "RO");
}
//Price = BP
if (isset ($BP)) {
	wddx_add_vars($packet_id, "BP");
}
//GST/VAT = GS
if (isset ($GS)) {
	wddx_add_vars($packet_id, "GS");
}
//Total Cost = TT
if (isset ($TT)) {
	wddx_add_vars($packet_id, "TT");
}
//Pickup Company = PC
if (isset ($PC)) {
	wddx_add_vars($packet_id, "PC");
}
//Pickup Address = PA
if (isset ($PA)) {
	wddx_add_vars($packet_id, "PA");
}
//Pickup Address2 = P2
if (isset ($P2)) {
	wddx_add_vars($packet_id, "P2");
}
//Pickup Suburb = PS
if (isset ($PS)) {
	wddx_add_vars($packet_id, "PS");
}
//Pickup County = P5
if (isset ($P5)) {
	wddx_add_vars($packet_id, "P5");
}
//Pickup Postcode = PP
if (isset ($PP)) {
	wddx_add_vars($packet_id, "PP");
}
//Pickup Instructions = PI
if (isset ($PI)) {
	wddx_add_vars($packet_id, "PI");
}
//Pickup Contact = PX
if (isset ($PX)) {
	wddx_add_vars($packet_id, "PX");
}
//Weight = WE 
if (isset ($WE)) {
	wddx_add_vars($packet_id, "WE");
}
//No of Items = PN
if (isset ($PN)) {
	wddx_add_vars($packet_id, "PN");
}
//Height = HE
if (isset ($HE)) {
	wddx_add_vars($packet_id, "HE");
}
//Length = PL
if (isset ($PL)) {
	wddx_add_vars($packet_id, "PL");
}
//Depth = PD
if (isset ($PD)) {
	wddx_add_vars($packet_id, "PD");
}
//Cubice m = CU
if (isset ($CU)) {
	wddx_add_vars($packet_id, "CU");
}
//Total waiting time = WT
if (isset ($WT)) {
	wddx_add_vars($packet_id, "WT");
}

// Do delivery address
$tagcount = 0;
$finished=FALSE;
$index=0;

while (!$finished) {
    	$tagcount += 1;
	//Delivery Company = DCn
	$gen_var = "DC" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DC'] = $$gen_var;
	}
	//Delivery Address = DAn
	$gen_var = "DA" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DA'] = $$gen_var;
	} else {
		$finished = TRUE;
	}
	//Delivery Address2 = D2n
	$gen_var = "D2" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['D2'] = $$gen_var;
	} else {
		$finished = TRUE;
	}
	//Delivery Suburb = DSn
	$gen_var = "DS" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DS'] = $$gen_var;
	}
	//Delivery County = DOn
	$gen_var = "DO" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DO'] = $$gen_var;
	} else {
		$finished = TRUE;
	}
	//Delivery Postcode = DPn
	$gen_var = "DP" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DP'] = $$gen_var;
	} else {
		$finished = TRUE;
	}
	//Delivery Instructions  = DIn
	$gen_var = "DI" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DI'] = $$gen_var;
	}
	//Delivery Contact = DXn
	$gen_var = "DX" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DX'] = $$gen_var;
	}
	$index++;
}
if ($delivery_address_array) {
	wddx_add_vars($packet_id, "delivery_address_array");
}

// Do tracking details
$tagcount = 0;
$finished=FALSE;
$index=0;
while (!$finished) {
    	$tagcount += 1;
	//Track Date = AD
	$gen_var = "AD" . $tagcount;
	$gen_val = $$gen_var;
	if (isset ($$gen_var)) {
		$tracking_details_array[$index]['AD'] = $$gen_var;
	} else {
		$finished = TRUE;
	}
	//Track Time = AT
	$gen_var = "AT" . $tagcount;
	if (isset ($$gen_var)) {
		$tracking_details_array[$index]['AT'] = $$gen_var;
	}
	//Track Event = AE
	$gen_var = "AE" . $tagcount;
	if (isset ($$gen_var)) {
		$tracking_details_array[$index]['AE'] = $$gen_var;
	}
	$index++;
}

if ($tracking_details_array) {
	wddx_add_vars($packet_id, "tracking_details_array");
}

// Do signatures
$tagcount = 0;
$finished=FALSE;
$index=0;
$anysigs = "";
	
while (!$finished) {
    	$tagcount += 1;
	// Signatures = DYn
//	$ST = "";
	$full_jpeg_filename = "";	
	$name = "";
	$address = "";
	$temp = "";
	$temp2 = "";
	$gen_var = "DY" . $tagcount;
	if (isset ($$gen_var)) {
	    if ($$gen_var == "0") {      //nothing
			$sigtypearr = array("");
		}
	    if ($$gen_var == "1") {      // POD Only
			$sigtypearr = array("POD");
		}
	    if ($$gen_var == "2") {      //Wait only
			$sigtypearr = array("WT");
		}
	    if ($$gen_var == "3") {      //Both
			$sigtypearr = array("POD","WT");
		}
		reset ($sigtypearr);
		foreach ($sigtypearr as $ST) {
		  if ($ST) {
		    $site = "http://".$_SERVER['SERVER_NAME']."/dms/bin/job_sig?cust_key=$CK&password=$PW&TS=&job_no=$JN&drop_no=$tagcount&sig_type=$ST";
		 	$open = fopen($site, "r"); 
    		$search = fread($open, 50000); 
    		fclose($open); 
            // ^TS12345^SUY^NMTimothy ^AD58/62 SCRUTTON STREET^JSfgdfgidgfiudgfidgfidgf^DD05/09/08^DM15:25^ZZ		
			$len = strlen($search);
			$pos1 = strpos($search, "^NM");
			$pos2 = strpos($search, "^AD");
			$pos3 = strpos($search, "^JS");
			$pos4 = strpos($search, "^DD");
		    $pos5 = strpos($search, "^DM");
			$pos6 = strpos($search, "^ZZ");
			$name = substr($search, $pos1+3,$pos2-$pos1-3);
			$address = substr($search, $pos2+3, $pos3-$pos2-3);
			$sig = substr($search, $pos3+3, $pos4-$pos3-3);

			if ($sig) {
				$anysigs = "yes";
	        		$tmp_file_name = $DOCUMENT_ROOT . '/tmp/sig_file_bmp' . rand() . '.txt';
    				$tmpopen = fopen($tmp_file_name, "ab+"); 	
				fwrite ($tmpopen,$sig);
		                fwrite($tmpopen,"\n");	
				fclose($tmpopen);
			
        			$bmp_filename = '/tmp/sig_file_bmp' . $JN.$tagcount.$index.'R'.rand() . '.bmp';		
				$full_bmp_filename = $DOCUMENT_ROOT . $bmp_filename;
        			$jpeg_filename = '/tmp/sig_file_jpg' . $JN.$tagcount.$index.'R'.rand() . '.jpg';		
				$full_jpeg_filename = $DOCUMENT_ROOT . $jpeg_filename;
				$com = $t2b.' '.$tmp_file_name.' '.$full_bmp_filename;
        			$exec_return = exec ($com);
		        	unlink ($tmp_file_name);				
				$com = $btop.' '.$full_bmp_filename. ' | '.$ptoj.' > '.$full_jpeg_filename;
        			$exec_return = exec ($com);
		        	unlink ($full_bmp_filename);	
			}
                        if ($tagcount==1) {
                            $temp2 = "Sender ";
                        } else {
                                        $temp = $tagcount-1;
                                        $temp2 = "Receiver ".$temp." ";
                        }

		  }
		  $dd = substr($search, $pos4 +3, $pos5 - $pos4 -3);              //delivery date
		  $dm = substr($search, $pos5 +3, $pos6 - $pos5 -3);              //delivery time
				
		  $signatures_array[$index]['TC'] = $temp2;
		  $signatures_array[$index]['ST'] = $ST;		
		  $signatures_array[$index]['JM'] = $full_jpeg_filename;		
		  $signatures_array[$index]['NM'] = $name;
		  $signatures_array[$index]['AY'] = $address;		
		  $signatures_array[$index]['DD'] = $dd;
		  $signatures_array[$index]['DM'] = $dm;
		  $index++;
		}
		
	} else {
		$index++;
		$finished = TRUE;
	}
}
if ($anysigs) {
	wddx_add_vars($packet_id, "signatures_array");
}

$xml_packet = wddx_packet_end($packet_id); 

$filename = "/tmp/pdf_xml_data." . $JN . date("U");
// let's create a temporary file
$fp=fopen($filename,"w");
fwrite ($fp, $xml_packet);
fclose($fp);

$pdf_filename = "/tmp/pdf_file_" . $JN . date("U") . ".pdf";
$full_pdf_filename = $DOCUMENT_ROOT . $pdf_filename;
$com = 'perl /usr/local/server/brtc'.dirname($_SERVER['PHP_SELF']).'/couriernote.pl ' . $filename . " > " . $full_pdf_filename;
$exec_return = exec ($com);
print "<meta http-equiv=\"Refresh\" content=\"0; URL=$pdf_filename\">";
?>
