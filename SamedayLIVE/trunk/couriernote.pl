#!/usr/local/server/perl/bin/perl

# Include various Perl modules
use WDDX;    # Wddx support for Perl, asee www.Wddx.org
use Time::Local;
use Data::Dumper;
use pdflib_pl;
use Text::Wrap;

# Establish some constants to control behavior
$packetFilename  = "/tmp/rr";       # File to retrieve stuff from
$page_x          = 595;
$page_y          = 842;
$margin          = 40;
$page_col2_1     = 120;
$page_col2_2     = 380;
$page_col3_1     = 100;
$page_col3_2     = 300;
$page_col3_3     = 490;
$page_col4_1     = 80;
$page_col4_2     = 200;
$page_col4_3     = 370;
$page_col4_4     = 480;
$tracking_col_1  = 40;
$tracking_col_2  = 120;
$tracking_col_3  = 210;
$y_big_spacing   = 30;
$y_small_spacing = 15;
$field_font      = "Helvetica";
$field_font_size = 10.0;
$title_font      = "Helvetica-Bold";
$title_font_size = 10.0;
$field_title_font= "Helvetica";
$field_title_font_size = 10.0;
$bottom_margin         = 70;
$default_encoding      = "host";
# Ramya 26/11/2008 To Adjust reference field wrapping
$refadjust=15;


if ( !$ARGV[0] ) {
	print "Usage: couriernote.pl xml_packet_filename\n\n";
	exit 1;
}
$data_file = $ARGV[0];

# let's open the xml data_file
open( MESSAGE_FILE, $data_file );
read( MESSAGE_FILE, $the_data, 10000000 );
close(MESSAGE_FILE);

#unlink $data_file;

# Create new Wddx "worker object" to do our work for us
$wddx = new WDDX();
$rs_data = $wddx->deserialize($the_data);

$p = PDF_new();
PDF_set_parameter( $p, "licensefile", "/etc/PDFlib/licensekeys.txt");
#PDF_set_parameter( $p, "license", "W800102-019000-127973-LAH392-AL2G82");
PDF_set_parameter( $p, "compatibility", "1.4" );

die "Couldn't open PDF file" if ( PDF_open_file( $p, "-" ) == -1 );

PDF_set_info( $p, "Creator", "BRTC" );
PDF_set_info( $p, "Author",  "BRTC" );
PDF_set_info( $p, "Title",   "Couriernote" );

$page_number = 0;

eval { $JN = $rs_data->get("JN")->as_scalar };
eval { $YZ = $rs_data->get("YZ")->as_scalar };
$page_left = &print_header( $JN, $YZ );

$page_left      = $page_left - $y_big_spacing;
$true_page_left = $page_left;

@signatures     = get_signaturearray();
%signaturemap   = get_signaturemap(@signatures);
%wtsignaturemap = get_waitingtime_signaturemap(@signatures);
%namemap        = get_namemap(@signatures);
%deliverydatemap = get_deliverydatemap(@signatures);
%deliverytimemap = get_deliverytimemap(@signatures);

#Ramya 31/07/2009
eval { $CUK = $rs_data->get("CUK")->as_scalar };
if($CUK eq "C15740" || $CUK eq "C15706")
{
    $labelFlag = 1;
}
else
{
   $labelFlag = 2;
}

#####################
#Print Caller section
#####################

#Moved to print_header function


$page_left   = $page_left - 30;
$font        = PDF_findfont( $p, $field_title_font, "host", 0 );
$field_width = PDF_stringwidth( $p, "Date: ", $font, $field_font_size );
print_title( "Advance Booking", 40, $page_left );
$font = PDF_findfont( $p, $field_title_font, "host", 0 );
if ( $service eq "cab" ) {
    #Ramya 10/03/2009
	#$field_width =
	#  PDF_stringwidth( $p, "Number of Passengers: ", $font, $field_font_size );
	#print_title( "Passenger Information",
	#	$page_col4_3 - $field_width, $page_left );
}
else {
	$field_width =
	  PDF_stringwidth( $p, "Number of Items: ", $font, $field_font_size );
	print_title( "Goods", $page_col4_3 - $field_width, $page_left );
}

$true_page_left = $page_left - 30;

$page_left = $page_left - $y_small_spacing;

$true_page_left = $page_left;
eval { $RA = $rs_data->get("RA")->as_scalar };
print_bfield_title( "Date: ", $page_col4_1, $page_left, "Service: " );
$new_page_left =
  &print_field( "$RA", $page_col4_1, $page_left, "Length: ", $page_col4_2 );
if ( $new_page_left < $true_page_left ) {
	$true_page_left = $new_page_left;
}

eval { $RO = $rs_data->get("RO")->as_scalar };
print_bfield_title( "Time: ", $page_col4_2, $page_left, "Length: " );
$new_page_left =
  &print_field( "$RO", $page_col4_2, $page_left, "Number of Items: ",
	$page_col4_3 );
if ( $new_page_left < $true_page_left ) {
	$true_page_left = $new_page_left;
}

eval { $PN = $rs_data->get("PN")->as_scalar };
if ( $service eq "cab" ) {
    #Ramya 10/03/2009
	#print_bfield_title( "Number of Passengers: ",
	#	$page_col4_3, $page_left, "Number of Passengers: " );
}
else {
	print_bfield_title( "Number of Items: ",
		$page_col4_3, $page_left, "Number of Items: " );
}

if ( $service eq "courier" ) {
$new_page_left =
  &print_field( "$PN", $page_col4_3, $page_left, "Cubic Metres: ",
	$page_col4_4 );
}

if ( $new_page_left < $true_page_left ) {
	$true_page_left = $new_page_left;
}

if ($service eq "courier") {
		eval {$PW = $rs_data->get("PW")->as_scalar};
		print_bfield_title ("Weight: ", $page_col4_4, $page_left, "Cubic Metres: ");
		#$new_page_left = &print_field ("$WE kg", $page_col4_4, $page_left, "", $page_x - $margin);
        $new_page_left = &print_field ("$PW kg", $page_col4_4, $page_left, "", $page_x - $margin - 10);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
	}

if ( $true_page_left < $page_left ) {
	$page_left = $true_page_left;
}
else {
	$true_page_left = $page_left;
}

$page_left   = $page_left - 30;
$font        = PDF_findfont( $p, $field_title_font, "host", 0 );
$field_width = PDF_stringwidth( $p, "Service: ", $font, $field_font_size );
print_title( "Costs", 40, $page_left );

$page_left = $page_left - $y_small_spacing;

$true_page_left = $page_left;

eval { $BP = $rs_data->get("BP")->as_scalar };
print_bfield_title( "Service: ", $page_col4_1, $page_left, "Service: " );
$ukpound          = pack( "U", 0xc2a3 );
$default_encoding = "builtin";
$new_page_left    = print_field( "" . $ukpound . "$BP",
	$page_col4_1, $page_left, "Length: ", $page_col4_2 );
if ( $new_page_left < $true_page_left ) {
	$true_page_left = $new_page_left;
}

eval { $GS = $rs_data->get("GS")->as_scalar };
print_bfield_title( "VAT: ", $page_col4_2, $page_left, "Length: " );
$new_page_left = print_field( $ukpound . "$GS",
	$page_col4_2, $page_left, "Number of Items: ", $page_col4_3 );
if ( $new_page_left < $true_page_left ) {
	$true_page_left = $new_page_left;
}

eval { $TT = $rs_data->get("TT")->as_scalar };
print_bfield_title( "Total Charges: ",
	$page_col4_3, $page_left, "Number of Items: " );
$new_page_left =
  print_field( $ukpound . "$TT", $page_col4_3, $page_left, "", $page_col4_4 );
if ( $new_page_left < $true_page_left ) {
	$true_page_left = $new_page_left;
}
if ( $true_page_left < $page_left ) {
	$page_left = $true_page_left;
}
$default_encoding = "host";

#End Cost

#new line and space

$rect_top_pos     = $true_page_left - 15;
$page_left        = $true_page_left - 30;
$top_of_addresses = $page_left;

if ( $service eq "cab" ) {
	print_title( "Pickup details", 40, $page_left );
}
else {
	print_title( "Collection 1", 45, $page_left );
}

$page_left = $page_left - $y_small_spacing;

eval { $PC = $rs_data->get("PC")->as_scalar };
$PC = trim($PC);

if(length ($PC) > 40){
    $PC = substr $PC, 0, 40;
}
if ( $service eq "cab" ) {

    $new_page_left =
  print_field( "$PC", $page_col2_1 - 80, $page_left, "Town/Postcode: ",
	$page_col2_2 );
}
else {

print_afield_title( "Company: ", $page_col2_1, $page_left );
$new_page_left =
  print_field( "$PC", $page_col2_1, $page_left, "Town/Postcode: ",
	$page_col2_2 );
}
if ( $new_page_left < $page_left ) {
	$page_left = $new_page_left;
}

$page_left = $page_left - $y_small_spacing;

eval { $PA = $rs_data->get("PA")->as_scalar };

$PA = trim($PA);

if(length ($PA) > 40){
    $PA = substr $PA, 0, 40;
}

if ( $service eq "cab" ) {
    $new_page_left =
  print_field( "$PA", $page_col2_1 - 80, $page_left, "Town/Postcode: ",
	$page_col2_2 );
}
else {
print_afield_title( "Bldg Name/No: ", $page_col2_1, $page_left );
$new_page_left =
  print_field( "$PA", $page_col2_1, $page_left, "Bldg Name/No: ",
	$page_col2_2 );
}

if ( $new_page_left < $page_left ) {
	$page_left = $new_page_left;
}

$page_left = $page_left - $y_small_spacing;

eval { $P2 = $rs_data->get("P2")->as_scalar };
$P2 = trim($P2);

if(length ($P2) > 40){
    $P2 = substr $P2, 0, 40;
}
if ( $service eq "cab" ) {
    $new_page_left =
  print_field( "$P2", $page_col2_1 - 80, $page_left, "Town/Postcode: ",
	$page_col2_2 );
}
else {
print_afield_title( "Address: ", $page_col2_1, $page_left );
$new_page_left =
  print_field( "$P2", $page_col2_1, $page_left, "Address: ",
	$page_col2_2 );
}

if ( $new_page_left < $page_left ) {
	$page_left = $new_page_left;
}

$page_left = $page_left - $y_small_spacing;
eval { $PS = $rs_data->get("PS")->as_scalar };
$PS = trim($PS);

if(length ($PS) > 40){
    $PS = substr $PS, 0, 40;
}
if ( $service eq "cab" ) {
    $new_page_left =
  print_field( "$PS", $page_col2_1 - 80, $page_left, "Town/Postcode: ",
	$page_col2_2 );
}
else {
print_afield_title( "Town: ", $page_col2_1, $page_left );
$new_page_left =
  print_field( "$PS", $page_col2_1, $page_left, "Town: ",
	$page_col2_2 );
}

if ( $new_page_left < $page_left ) {
	$page_left = $new_page_left;
}

$page_left = $page_left - $y_small_spacing;

eval { $P5 = $rs_data->get("P5")->as_scalar };
$P5 = trim($P5);

if(length ($P5) > 40){
    $P5 = substr $P5, 0, 40;
}
if ( $service eq "cab" ) {
    $new_page_left =
  print_field( "$P5", $page_col2_1 - 80, $page_left, "Town/Postcode: ",
	$page_col2_2 );
}
else {
print_afield_title( "County: ", $page_col2_1, $page_left );
$new_page_left =
  print_field( "$P5", $page_col2_1, $page_left, "County: ",
	$page_col2_2 );
}

if ( $new_page_left < $page_left ) {
	$page_left = $new_page_left;
}

$page_left = $page_left - $y_small_spacing;

eval { $PP = $rs_data->get("PP")->as_scalar };
if ( $service eq "cab" ) {
    $new_page_left =
  print_field( "$PP", $page_col2_1 - 80, $page_left, "Town/Postcode: ",
	$page_col2_2 );
}
else {
print_afield_title( "Postcode: ", $page_col2_1, $page_left );
$new_page_left =
  print_field( "$PP", $page_col2_1, $page_left, "Postcode: ",
	$page_col2_2 );
}

if ( $new_page_left < $page_left ) {
	$page_left = $new_page_left;
}

$page_left = $page_left - $y_small_spacing;
eval { $PX = $rs_data->get("PX")->as_scalar };
$PX = trim($PX);

if(length ($PX) > 40){
    $PX = substr $PX, 0, 40;
}
if ( $service eq "cab" ) {
	print_afield_title( "Passenger Name: ", $page_col2_1 - 5 , $page_left );
  $new_page_left =
  print_field( "$PX", $page_col2_1 + 5, $page_left, "Instructions: ",
  $page_col2_2 );
}
else {
	 if($labelFlag == 1){
        print_afield_title( "Serial No: ", $page_col2_1, $page_left );
        $new_page_left =
            print_field( "$PX", $page_col2_1, $page_left, "Serial No: ",
            $page_col2_2 );
    }
    else{
        print_afield_title( "Contact: ", $page_col2_1, $page_left );
        $new_page_left =
                print_field( "$PX", $page_col2_1, $page_left, "Contact: ",
                $page_col2_2 );
    }
    
}

if ( $new_page_left < $page_left ) {
	$page_left = $new_page_left;
}

$page_left = $page_left - $y_small_spacing;
eval { $PI = $rs_data->get("PI")->as_scalar };
$PI = trim($PI);

if(length ($PI) > 60){
    $PI = substr $PI, 0, 60;
}

if ( $service eq "cab" ) {
    #print_afield_title( "Phone Number: ", $page_col2_1, $page_left );
    print_afield_title( "Instructions: ", $page_col2_1 - 5, $page_left );
    #$new_page_left = print_field ("test $page_col2_2", $page_col2_1 , $page_left, "tf ", 460);
 # following line was commented by Adnan for 60 characters change   
#$new_page_left = print_field ("$PI", $page_col2_1 - 20, $page_left, "Instructions: ", $page_col2_2);
if(length ($PI) > 30){
my $PicInst1  = substr $PI, 0, 30;  
$new_page_left = print_field ("$PicInst1", $page_col2_1 - 20, $page_left, "Instructions: ", $page_col2_2);
$page_left = $page_left - $y_small_spacing;

my $PicInst2  = substr $PI, 30;  
$new_page_left = print_field ("$PicInst2", $page_col2_1 - 20, $page_left, "Instructions: ", $page_col2_2);
    #PDF_show_xy( $p, $PI, $page_col2_1, $page_left );
} else{
$new_page_left = print_field ("$PI", $page_col2_1 - 20, $page_left, "Instructions: ", $page_col2_2);
}
}
else {
    print_afield_title( "Instructions: ", $page_col2_1, $page_left );
if(length ($PI) > 30){
my $PicInst1  = substr $PI, 0, 30;  
    $new_page_left = print_field ("$PicInst1", $page_col2_1, $page_left, "Instructions: ", $page_col2_2);

$page_left = $page_left - $y_small_spacing;

my $PicInst2  = substr $PI, 30;  
    $new_page_left = print_field ("$PicInst2", $page_col2_1, $page_left, "Instructions: ", $page_col2_2);
}else {
    $new_page_left = print_field ("$PI", $page_col2_1, $page_left, "Instructions: ", $page_col2_2);
}
}

if ( $new_page_left < $page_left ) {
	$page_left = $new_page_left;
}

# Collection details
$left_side_page_left = $page_left;
$page_left           = $top_of_addresses;

$field_width =
  PDF_stringwidth( $p, "Town/Postcode: ", $font, $field_font_size );
print_title( "Collection details", $page_col2_2 - $field_width, $page_left );

eval { $DD = $rs_data->get("DD")->as_scalar };
$page_left = $page_left - $y_small_spacing;
print_afield_title( "Date: ", $page_col2_2, $page_left );
$new_page_left =
  print_field( $deliverydatemap{"Sender"}, $page_col2_2, $page_left, "", $page_x - $margin );
if ( $new_page_left < $page_left ) {
	$page_left = $new_page_left;
}

eval { $DM = $rs_data->get("DM")->as_scalar };
$page_left = $page_left - $y_small_spacing;
print_afield_title( "Time: ", $page_col2_2, $page_left );
$new_page_left =
  print_field( $deliverytimemap{"Sender"}, $page_col2_2, $page_left, "", $page_x - $margin );
if ( $new_page_left < $page_left ) {
	$page_left = $new_page_left;
}

$page_left = $page_left - $y_small_spacing;
print_afield_title( "Auth Name: ", $page_col2_2, $page_left );
$new_page_left = print_field( $namemap{"Sender"},
	$page_col2_2, $page_left, "", $page_x - $margin );
if ( $new_page_left < $page_left ) {
	$page_left = $new_page_left;
}

#    $page_left = $page_left - $y_small_spacing;
#    print_afield_title ("Waiting Time: ", $page_col2_2, $page_left);
#    $new_page_left = print_field (" ", $page_col2_2, $page_left, "", $page_x - $margin);
#    if ($new_page_left < $page_left) {
#       $page_left = $new_page_left;
#    }

# Sender signature image
$wtimagename = $wtsignaturemap{"Sender"};
if ($wtimagename) {
                $image = PDF_open_image_file( $p, "bmp", $wtimagename, "", 0 );
                die "Couldn't open image '$wtimagename'" if ( $image == -1 );
                $width  = PDF_get_value( $p, "imagewidth",  $image );
                $height = PDF_get_value( $p, "imageheight", $image );
                PDF_place_image(
                    $p, $image,
                    $page_col2_2 - 75,
                    $page_left - $height, 1
                );
    PDF_close_image( $p, $image );

    #unlink $wtimagename;
}

$imagename = $signaturemap{"Sender"};

if ($imagename) {
    $image = PDF_open_image_file( $p, "bmp", $imagename, "", 0 );
    die "Couldn't open image '$imagename'" if ( $image == -1 );
    $width  = PDF_get_value( $p, "imagewidth",  $image );
    $height = PDF_get_value( $p, "imageheight", $image );
    PDF_place_image(
       $p, $image,
                    $page_col2_2 + 50,
                    $page_left - $height, 1
                );
    PDF_close_image( $p, $image );

    #unlink $imagename;
}

$page_left = $page_left - $height - 10;

#Print signature caption
if ($wtimagename) {
    print_afield_title( "Wait", $page_col2_2, $page_left );
}
if ($imagename) {
    print_afield_title( "POD", $page_col2_2 + 125, $page_left );
}

$page_left = $page_left - $y_small_spacing;
if ( $left_side_page_left < $page_left ) {
	$page_left = $left_side_page_left;
}

#save the old page_left, just case it is lower than the previous one.
#$page_left           = $page_left - $y_small_spacing;
#$left_side_page_left = $page_left;

#draw table for the collection section
#Ramya table should be drawn only for CitySprint booking
if( $service eq "courier" ){
draw_rect( 38, $page_left, 257, $rect_top_pos - $page_left );
draw_rect( 38 + 257, $page_left, 255, $rect_top_pos - $page_left );
}
eval { $IT_tags_array = $rs_data->get("IT_tags_array") };
###################
# DELIVERY SECTION
###################
eval { $delivery_address_array = $rs_data->get("delivery_address_array") };
if ( !$@ && $delivery_address_array ) {
	$number_of_dropoffs = $delivery_address_array->length;
	$dropoff_count      = 0;
	while ( ( $array_row = $delivery_address_array->shift() ) ) {
		if ( $array_row->type eq 'hash' ) {

			$dropoff_count++;

			$DC = "";
			$DA = "";
			$DS = "";
			$DI = "";
			$DX = "";

			#Get the data
			#Delivery Company = DC
			eval { $DC = $array_row->get("DC")->as_scalar };
                        $DC = trim($DC);

                        if(length ($DC) > 40){
                            $DC = substr $DC, 0, 40;
                        }
			#Delivery Address = DA
			eval { $DA = $array_row->get("DA")->as_scalar };
                        $DA = trim($DA);

                        if(length ($DA) > 40){
                            $DA = substr $DA, 0, 40;
                        }
			#Delivery Address2 = D2
			eval { $D2 = $array_row->get("D2")->as_scalar };
                        $D2 = trim($D2);

                        if(length ($D2) > 40){
                            $D2 = substr $D2, 0, 40;
                        }
			#Delivery Suburb = DS
			eval { $DS = $array_row->get("DS")->as_scalar };
                        $DS = trim($DS);

                        if(length ($DS) > 40){
                            $DS = substr $DS, 0, 40;
                        }
			#Delivery County = DO
			eval { $DO = $array_row->get("DO")->as_scalar };
                        $DO = trim($DO);

                        if(length ($DO) > 40){
                            $DO = substr $DO, 0, 40;
                        }
			#Delivery Postcode = DP
			eval { $DP = $array_row->get("DP")->as_scalar };
                        $DP = trim($DP);

                        if(length ($DP) > 40){
                            $DP = substr $DP, 0, 40;
                        }
			#Delivery Instructions  = DI
			eval { $DI = $array_row->get("DI")->as_scalar };
                        $DI = trim($DI);

                        if(length ($DI) > 60){
                            $DI = substr $DI, 0, 60;
                        }
			#Delivery Contact = DX
			eval { $DX = $array_row->get("DX")->as_scalar };
                        $DX = trim($DX);

                        if(length ($DX) > 40){
                            $DX = substr $DX, 0, 40;
                        }
			#Decide if we have enough space to print on this page
			#CHIformatchange if (&is_even($dropoff_count)) {
			if ( $page_left < $bottom_margin + 120 ) {
				&print_footer($YZ);
				$page_left = &print_header( $JN, $YZ );
			}

			#print on the left hand side of the page
			$page_left        = $page_left - 30;
			$rect_top_pos     = $page_left + 15;
			$top_of_addresses = $page_left;

			if ( $service eq "cab" ) {
                $page_left        = $page_left + 30;
				if ( $number_of_dropoffs > 1 ) {
					print_title( "Destination $dropoff_count", 40, $page_left  );
				}
				else {
					print_title( "Destination", 40, $page_left );
				}
			}
			else {
				if ( $number_of_dropoffs > 1 ) {
					print_title( "Receiver $dropoff_count", 45, $page_left );
				}
				else {
					print_title( "Receiver", 45, $page_left );
				}
			}

			$page_left = $page_left - $y_small_spacing;
            #Ramya 10/-3/2009 Destination Company should be displayed for both citysprint and westone
			#if ( $service eq "courier" ) {
            if ( $service eq "cab" ) {
                		$new_page_left =
				  print_field( "$DC", $page_col2_1 - 80, $page_left,
					"Town/Postcode: ", $page_col2_2 );

            }
            else {

				print_afield_title( "Company: ", $page_col2_1, $page_left );
				$new_page_left =
				  print_field( "$DC", $page_col2_1, $page_left,
					"Company: ", $page_col2_2 );
            }
            if ( $new_page_left < $page_left ) {
                $page_left = $new_page_left;
            }

			#}
			$page_left = $page_left - $y_small_spacing;

            if ( $service eq "cab" ) {
                	$new_page_left =
			  print_field( "$DA", $page_col2_1 - 80, $page_left, "Town/Postcode: ",
				$page_col2_2 );

            }
            else {
			print_afield_title( "Bldg Name/No: ", $page_col2_1, $page_left );
			$new_page_left =
			  print_field( "$DA", $page_col2_1, $page_left, "Bldg Name/No: ",
				$page_col2_2 );
            }

			if ( $new_page_left < $page_left ) {
				$page_left = $new_page_left;
			}

			$page_left = $page_left - $y_small_spacing;
            if ( $service eq "cab" ) {
                $new_page_left =
			  print_field( "$D2", $page_col2_1 - 80, $page_left, "Town/Postcode: ",
				$page_col2_2 );

            }
            else {
			print_afield_title( "Address: ", $page_col2_1, $page_left );
			$new_page_left =
			  print_field( "$D2", $page_col2_1, $page_left, "Address: ",
				$page_col2_2 );
            }

			if ( $new_page_left < $page_left ) {
				$page_left = $new_page_left;
			}

			$page_left = $page_left - $y_small_spacing;
            if ( $service eq "cab" ) {

			$new_page_left =
			  print_field( "$DS", $page_col2_1 - 80, $page_left, "Town/Postcode: ",
				$page_col2_2 );

            }
            else {
			print_afield_title( "Town: ", $page_col2_1, $page_left );
			$new_page_left =
			  print_field( "$DS", $page_col2_1, $page_left, "Town: ",
				$page_col2_2 );
            }

			if ( $new_page_left < $page_left ) {
				$page_left = $new_page_left;
			}

			$page_left = $page_left - $y_small_spacing;
            if ( $service eq "cab" ) {
                $new_page_left =
			  print_field( "$DO", $page_col2_1 - 80, $page_left, "Town/Postcode: ",
				$page_col2_2 );

            }
            else {
			print_afield_title( "County: ", $page_col2_1, $page_left );
			$new_page_left =
			  print_field( "$DO", $page_col2_1, $page_left, "County: ",
				$page_col2_2 );
            }

			if ( $new_page_left < $page_left ) {
				$page_left = $new_page_left;
			}

			$page_left = $page_left - $y_small_spacing;
            if ( $service eq "cab" ) {
                $new_page_left =
			  print_field( "$DP", $page_col2_1 - 80, $page_left, "Town/Postcode: ",
				$page_col2_2 );

            }
            else {
			print_afield_title( "Postcode: ", $page_col2_1, $page_left );
			$new_page_left =
			  print_field( "$DP", $page_col2_1, $page_left, "Postcode: ",
				$page_col2_2 );
            }

			if ( $new_page_left < $page_left ) {
				$page_left = $new_page_left;
			}

			if ( $service eq "courier" ) {
				$page_left = $page_left - $y_small_spacing;
				if($labelFlag == 1){
                    print_afield_title( "Rtn Serial No: ", $page_col2_1, $page_left );
                }
                else{
                    print_afield_title( "Contact: ", $page_col2_1, $page_left );
                }
				$new_page_left =
				  print_field( "$DX", $page_col2_1, $page_left,
					"Town/Postcode: ", $page_col2_2 );
				if ( $new_page_left < $page_left ) {
					$page_left = $new_page_left;
				}
            }


            $page_left = $page_left - $y_small_spacing;
            if ( $service eq "cab" ) {
                print_afield_title( "Instructions: ", $page_col2_1 - 5,
                    $page_left );
if(length ($DI) > 30){
my $DelInst1  = substr $DI, 0, 30;  
                #$new_page_left = print_field ("$DI", $page_col2_1 - 20, $page_left, "", $page_col2_2 + 92);
$new_page_left = print_field ("$DelInst1", $page_col2_1 - 20, $page_left, "", $page_col2_2 + 92);
my $DelInst2  = substr $DI, 30;         
$page_left = $page_left - $y_small_spacing;
$new_page_left = print_field ("$DelInst2", $page_col2_1 - 20, $page_left, "", $page_col2_2 + 92);
}else {
$new_page_left = print_field ("$DI", $page_col2_1 - 20, $page_left, "", $page_col2_2 + 92);
}
            }
            else {

                  if($labelFlag == 1){
                    print_afield_title( "Telephone No: ", $page_col2_1, $page_left );
                }
                else{
                    print_afield_title( "Instructions: ", $page_col2_1, $page_left );
                }
if(length ($DI) > 30){
my $DelInst1  = substr $DI, 0, 30;  
                #$new_page_left = print_field ("$DI", $page_col2_1, $page_left, "", $page_col2_2 + 90);
$new_page_left = print_field ("$DelInst1", $page_col2_1, $page_left, "", $page_col2_2 + 90);
my $DelInst2  = substr $DI, 30;         
$page_left = $page_left - $y_small_spacing;
$new_page_left = print_field ("$DelInst2", $page_col2_1, $page_left, "", $page_col2_2 + 90);
} else {
$new_page_left = print_field ("$DI", $page_col2_1, $page_left, "", $page_col2_2 + 90);
}

            }

            if ( $new_page_left < $page_left ) {
                $page_left = $new_page_left;
            }


		   #save the old page_left, just case it is lower than the previous one.
			$left_side_page_left = $page_left;

			#Right coloum for Pickup(Collection) and Receiver
			$page_left = $top_of_addresses;

			$field_width =
			  PDF_stringwidth( $p, "Town/Postcode: ", $font, $field_font_size );
			if ( $service eq "cab" ) {
				print_title( "Destination details",
					$page_col2_2 - $field_width, $page_left );
			}
			else {
                if($dropoff_count == $number_of_dropoffs && !$@ && $IT_tags_array)
                {
                    print_title( "Delivery details (Continued below)",
                        $page_col2_2 - $field_width, $page_left );
                }
                else
                {
                    print_title( "Delivery details",
                        $page_col2_2 - $field_width, $page_left );
                }
			}

			$page_left = $page_left - $y_small_spacing;
			print_afield_title( "Date: ", $page_col2_2, $page_left );
			$new_page_left =
			  print_field(  $deliverydatemap{"Receiver $dropoff_count"}, $page_col2_2, $page_left, "",
				$page_x - $margin );
			if ( $new_page_left < $page_left ) {
				$page_left = $new_page_left;
			}

			$page_left = $page_left - $y_small_spacing;
			print_afield_title( "Time: ", $page_col2_2, $page_left );
			$new_page_left =
			  print_field(  $deliverytimemap{"Receiver $dropoff_count"}, $page_col2_2, $page_left, "",
				$page_x - $margin );
			if ( $new_page_left < $page_left ) {
				$page_left = $new_page_left;
			}

			$page_left = $page_left - $y_small_spacing;
			print_afield_title( "Name: ", $page_col2_2, $page_left );
			$new_page_left = print_field( $namemap{"Receiver $dropoff_count"},
				$page_col2_2, $page_left, "", $page_x - $margin );
			if ( $new_page_left < $page_left ) {
				$page_left = $new_page_left;
			}

			#Signature image

			$wtimagename = $wtsignaturemap{"Receiver $dropoff_count"};
			#$page_left   = $page_left - $y_small_spacing;

			if ($wtimagename) {
				$image = PDF_open_image_file( $p, "bmp", $wtimagename, "", 0 );
				die "Couldn't open image '$wtimagename'" if ( $image == -1 );
				$width  = PDF_get_value( $p, "imagewidth",  $image );
				$height = PDF_get_value( $p, "imageheight", $image );
				PDF_place_image(
					$p, $image,
					$page_col2_2 - 75,
					$page_left - $height, 1
				);
				PDF_close_image( $p, $image );

                #unlink $wtimagename;
			}

			$imagename = $signaturemap{"Receiver $dropoff_count"};

			if ($imagename) {
				$image = PDF_open_image_file( $p, "bmp", $imagename, "", 0 );
				die "Couldn't open image '$imagename'" if ( $image == -1 );
				$width  = PDF_get_value( $p, "imagewidth",  $image );
				$height = PDF_get_value( $p, "imageheight", $image );
				PDF_place_image(
					$p, $image,
					$page_col2_2 + 50,
					$page_left - $height, 1
				);
				PDF_close_image( $p, $image );

                #unlink $imagename;
				$page_left = $page_left - $height - 10;

			}

			#Print signature caption
			if ($wtimagename) {
				print_afield_title( "Wait", $page_col2_2, $page_left );
			}
			if ($imagename) {
				print_afield_title( "POD", $page_col2_2 + 125, $page_left );
			}

			#$page_left = $page_left - 5;

			#}

			if ( $left_side_page_left < $page_left ) {
				$page_left = $left_side_page_left;
			}

            #Ramya table should be drawn only for CitySprint booking
            if( $service eq "courier" ){
			draw_rect( 38, $page_left - 15,
				257, $rect_top_pos - $page_left + 15 );
			draw_rect( 38 + 257, $page_left - 15,
				255, $rect_top_pos - $page_left + 15 );
            }
		}
	}

}

#End Delivery Section

######################
# Tracking Details
######################

if ( $page_left < $bottom_margin + 60 ) {
	&print_footer($YZ);
	$page_left = &print_header( $JN, $YZ );
}

$page_left = $page_left - 45;
$font = PDF_findfont( $p, $field_title_font, "host", 0 );
print_title( "Tracking Details", 40, $page_left );

$page_left = $page_left - $y_small_spacing;

# Print column titles
&print_tracking_column_heading( "Date",  $tracking_col_1, $page_left );
&print_tracking_column_heading( "Time",  $tracking_col_2, $page_left );
&print_tracking_column_heading( "Event", $tracking_col_3, $page_left );

eval { $tracking_details_array = $rs_data->get("tracking_details_array") };
if ( !$@ && $tracking_details_array ) {
	while ( ( $array_row = $tracking_details_array->shift() ) ) {
		$page_left = $page_left - $y_small_spacing;
		if ( $page_left < $bottom_margin + 60 ) {

			#Print continued next page
			&print_field( "Continued on the next page ...",
				$tracking_col_1, $page_left, "", $page_x - $margin );
			&print_footer($YZ);
			$page_left = &print_header( $JN, $YZ );
			$page_left = $page_left - 30;
		}

		if ( $array_row->type eq 'hash' ) {
			eval { $AD = $array_row->get("AD")->as_scalar };  #Track Date = AD
			eval { $AT = $array_row->get("AT")->as_scalar };  #Track Time = AT
			eval { $AE = $array_row->get("AE")->as_scalar };  #Track Event = AE
		}

		$true_page_left = $page_left;

		$new_page_left =
		  print_field( "$AD", $tracking_col_1, $page_left, "",
			$tracking_col_2 );
		if ( $new_page_left < $true_page_left ) {
			$true_page_left = $new_page_left;
		}
		$new_page_left =
		  print_field( "$AT", $tracking_col_2, $page_left, "",
			$tracking_col_3 );
		if ( $new_page_left < $true_page_left ) {
			$true_page_left = $new_page_left;
		}

# following line is added by Adnan at 20-07-2011
$AE = substr($AE, 0, 55);

		$new_page_left =
		  print_field( "$AE", $tracking_col_3, $page_left, "",
			$page_x - $margin );
		if ( $new_page_left < $true_page_left ) {
			$true_page_left = $new_page_left;
		}
		if ( $true_page_left < $page_left ) {
			$page_left = $true_page_left;
		}

	}    #end while loop

    #show total wait time in monetary value
	$page_left = $page_left - $y_small_spacing;
	eval { $WT = $rs_data->get("WT")->as_scalar };
	$default_encoding = "builtin";
    $page_left = print_field( "TOTAL WAITING TIME ".$ukpound." $WT",
        $tracking_col_3, $page_left, "", $page_x - $margin );
    $default_encoding="host";

    }
	

    #Consignment/Item Numbers Section
    
    $i=0;

    if ( !$@ && $IT_tags_array ) {

        if ( $page_left < $bottom_margin + 80 ) {
            #Print continued next page
            &print_field( "Continued on the next page ...",
            $tracking_col_1, $page_left, "", $page_x - $margin );
            &print_footer($YZ);
            $page_left = &print_header( $JN, $YZ );
        }
        
        $page_left = $page_left - 30;
        $font = PDF_findfont( $p, $field_title_font, "host", 0 );
        print_title( "Consignment/Item Numbers", 40, $page_left );

        $page_left = $page_left - $y_small_spacing;

        $len = $IT_tags_array->length;

        # Print column titles
        &print_tracking_column_heading( "Total: $len",  $tracking_col_1, $page_left );

        $page_left = $page_left - $y_small_spacing;


        while ( ( $array_row = $IT_tags_array->shift() ) ) {
            $i++;

            if($i % 2 > 0){
                $page_left = $page_left - $y_small_spacing;
            }

            if ( $page_left < $bottom_margin + 60 ) {

                #Print continued next page
                &print_field( "Continued on the next page ...",
                    $tracking_col_1, $page_left, "", $page_x - $margin );
                &print_footer($YZ);
                $page_left = &print_header( $JN, $YZ );
                $page_left = $page_left - 30;
            }

            #if ( $array_row->type eq 'hash' ) {
                eval { $IT = $array_row->as_scalar};  #IT tag
            #}

            $true_page_left = $page_left;
            
            if( $i % 2 == 0){
                $new_page_left =
                print_field( "$IT", $tracking_col_3, $page_left, "",
                $page_x - $margin );
                if ( $new_page_left < $true_page_left ) {
                    $true_page_left = $new_page_left;
                }
            }
            else
            {
                $new_page_left =
                print_field( "$IT", $tracking_col_1, $page_left, "",
                $tracking_col_3 );
                if ( $new_page_left < $true_page_left ) {
                    $true_page_left = $new_page_left;
                }
            }

        }    #end while loop

    }

    

&print_footer($YZ);

PDF_close($p);

PDF_delete($p);

sub is_even {
	my ($number) = @_;
	if ( $number % 2 == 0 ) {
		return 1;
	}
	else {
		return 0;
	}
}

sub get_signaturearray() {
	@signatures;
	eval( $signatures_array = $rs_data->get("signatures_array") );
	$i = 0;
	if ($signatures_array) {
	    while ( ( $array_row = $signatures_array->shift() ) ) {
		    $signatures[$i] = $array_row;
		    $i++;
	    }
	}
	return @signatures;
}

sub get_signaturemap() {
	my (@signature) = @_;

	for ( $i = 0 ; $i <= $#signature ; $i++ ) {
		$array_row = $signature[$i];

		if ( $array_row->type eq 'hash' ) {
			eval { $TC = $array_row->get("TC")->as_scalar }; #Tagcount stop e.g. 'Receiver 1'
			eval { $ST = $array_row->get("ST")->as_scalar }; #Stop Type: 'WT' or 'POD'
			eval { $JM = $array_row->get("JM")->as_scalar }; #JPEG
			# eval { $NM = $array_row->get("NM")->as_scalar }; # Name
			# eval { $AY = $array_row->get("AY")->as_scalar }; # Address
		}

		if ($JM) {
			if ( -s $JM ) {
			} else {
				$JM = "/usr/local/server/brtc/cs/missingsig.jpg";
			}
		}
		if ( $ST eq "POD" ) {
			$imagemap{ trim($TC) } = $JM;
		}
	}
	return %imagemap;
}

sub get_waitingtime_signaturemap() {
	my (@signature) = @_;

	for ( $i = 0 ; $i <= $#signature ; $i++ ) {
		$array_row = $signature[$i];

		if ( $array_row->type eq 'hash' ) {
			eval { $TC = $array_row->get("TC")->as_scalar }; #Tagcount stop e.g. 'Receiver 1'
			eval { $ST = $array_row->get("ST")->as_scalar }; #Stop Type: 'WT' or 'POD'
			eval { $JM = $array_row->get("JM")->as_scalar }; #JPEG
			# eval { $NM = $array_row->get("NM")->as_scalar }; # Name
			# eval { $AY = $array_row->get("AY")->as_scalar }; # Address
		}

		if ($JM) {
			if ( -s $JM ) {
			} else {
				$JM = "/usr/local/server/brtc/cs/missingsig.jpg";
			}
		}
		if ( $ST eq "WT" ) {
			$wtimagemap{ trim($TC) } = $JM;
		}
	}
	return %wtimagemap;
}

sub get_deliverydatemap() {
	my (@signature) = @_;

	for ( $i = 0 ; $i <= $#signature ; $i++ ) {
		$array_row = $signature[$i];

		if ( $array_row->type eq 'hash' ) {
		    eval { $TC = $array_row->get("TC")->as_scalar }; #Tagcount stop e.g. 'Receiver 1'
			eval { $DD = $array_row->get("DD")->as_scalar }; #Delivery date or Pickup date
		}

		$deliverydatemap{ trim($TC) } = $DD;
	}
	return %deliverydatemap;
}

sub get_deliverytimemap() {
	my (@signature) = @_;

	for ( $i = 0 ; $i <= $#signature ; $i++ ) {
		$array_row = $signature[$i];

		if ( $array_row->type eq 'hash' ) {
		    eval { $TC = $array_row->get("TC")->as_scalar }; #Tagcount stop e.g. 'Receiver 1'
			eval { $DM = $array_row->get("DM")->as_scalar }; #Delivery date or Pickup time
		}

		$deliverytimemap{ trim($TC) } = $DM;
	}
	return %deliverytimemap;
}

#Returns a map with TC as key and Name as value
sub get_namemap() {
	my (@signatures_array) = @_;

	for ( $i = 0 ; $i <= $#signatures_array ; $i++ ) {
		$array_row = $signatures_array[$i];

		if ( $array_row->type eq 'hash' ) {
			eval { $TC = $array_row->get("TC")->as_scalar }; #Tagcount stop e.g. 'Receiver 1'
			eval { $NM = $array_row->get("NM")->as_scalar }; # Name
		}

		#add name to the map if it does not exist
		if ( !$namemap{ trim($TC) } ) {
			$namemap{ trim($TC) } = $NM;
		}

	}
	return %namemap;
}

sub trim() {
	my ($string) = @_;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

#left_x: lower left corner h coordinate, left_y: lower left corner v coordinate
sub draw_rect() {
	my ( $left_x, $left_y, $width, $height ) = @_;

	draw_h_line( $left_x, $left_y, $width );
	draw_v_line( $left_x, $left_y, $height );
	draw_h_line( $left_x, $left_y + $height, $width );
	draw_v_line( $left_x + $width, $left_y, $height );
}

#draw a horizontal line at point (x, y) to (x+ width, y)
sub draw_h_line() {
	my ( $left_x, $left_y, $width ) = @_;

	PDF_moveto( $p, $left_x, $left_y );
	PDF_lineto( $p, $left_x + $width, $left_y );
	PDF_fill($p);
}

sub draw_v_line() {
	my ( $left_x, $left_y, $height ) = @_;

	PDF_moveto( $p, $left_x, $left_y );
	PDF_lineto( $p, $left_x, $left_y + $height );
	PDF_fill($p);
}

sub print_field {
	my ( $field, $left_x, $left_y, $next_field, $next_col ) = @_;
	$font = PDF_findfont( $p, $field_title_font, "host", 0 );
	$max_width = $next_col - $left_x -
	  PDF_stringwidth( $p, $next_field, $font, $field_title_font_size );

	$height_increment = $field_font_size;
	$my_height        = $height_increment;

	$font = PDF_findfont( $p, $field_font, $default_encoding, 0 );
	PDF_setfont( $p, $font, $field_font_size );

	# Let's find out if the line will fit in - if it fits, just print it
	if ( $max_width > PDF_stringwidth( $p, $field, $font, $field_font_size ) ) {
		PDF_show_xy( $p, $field, $left_x, $left_y );
	}
	else {
		# $*           = 1; #deprecated operation
		$i           = 0;
		# //@field_array = split( ' ', $field );
		@field_array = split( '/\s+/m', $field );
		for ( $i = 0 ; $i < $#field_array + 1 ; $i++ ) {
			if ($the_line) {
				$the_line = $the_line . " " . @field_array[$i];
			}
			else {
				$the_line = @field_array[$i];
			}
			if ( $max_width >
				PDF_stringwidth( $p, $the_line, $font, $field_font_size ) )
			{
				if ( $i < $#field_array ) {
					$temp_line = $the_line . " " . " " . @field_array[ $i + 1 ];
					if (
						$max_width < PDF_stringwidth(
							$p, $temp_line, $font, $field_font_size
						)
					  )
					{

				  # we know we cannot add anymore words so let's print the line.
						PDF_show_xy( $p, $the_line, $left_x, $left_y );
						$left_y = $left_y - ( $height_increment + 2 );
						$the_line = "";
					}
				} else {
					PDF_show_xy( $p, $the_line, $left_x, $left_y );
					$the_line = "";
				}
			}
		}
	}
	return $left_y;
}

sub print_title {
	my ( $title, $left_x, $left_y ) = @_;

	$font = PDF_findfont( $p, $title_font, "host", 0 );
	PDF_setfont( $p, $font, $title_font_size );
	PDF_set_text_pos( $p, $left_x, $left_y );
	PDF_show( $p, $title );
}

sub print_tracking_column_heading {
	my ( $title, $left_x, $left_y ) = @_;

	$font = PDF_findfont( $p, $field_title_font, "host", 0 );
	PDF_setfont( $p, $font, $field_title_font_size );
	PDF_set_text_pos( $p, $left_x, $left_y );
	PDF_show( $p, $title );
}

sub print_field_title {
	my ( $title, $right_x, $left_y ) = @_;

	$font = PDF_findfont( $p, $field_title_font, "host", 0 );
	PDF_setfont( $p, $font, $field_title_font_size );
	PDF_set_text_pos(
		$p,
		$right_x - PDF_stringwidth( $p, $title, $font, $field_title_font_size ),
		$left_y
	);
	PDF_show( $p, $title );
}

sub print_afield_title {
	my ( $title, $right_x, $left_y ) = @_;

	$font = PDF_findfont( $p, $field_title_font, "host", 0 );
	PDF_setfont( $p, $font, $field_title_font_size );
	PDF_set_text_pos(
		$p,
		$right_x - PDF_stringwidth(
			$p, "Town/Postcode: ",
			$font, $field_title_font_size
		),
		$left_y
	);
	PDF_show( $p, $title );
}

sub print_bfield_title {
	my ( $title, $right_x, $left_y, $longest ) = @_;

	$font = PDF_findfont( $p, $field_title_font, "host", 0 );
	PDF_setfont( $p, $font, $field_title_font_size );
	PDF_set_text_pos(
		$p,
		$right_x -
		  PDF_stringwidth( $p, $longest, $font, $field_title_font_size ),
		$left_y
	);
	PDF_show( $p, $title );
}

#Print out the logo, page number and job number
sub print_header {
my($title,$serv_logo) = @_;
	PDF_begin_page($p, $page_x, $page_y);
      if ($serv_logo == 2)
	{
	      #Ramya 25/02/2009
            $imagefile = "./westonelogo_new.jpg";

		$image = PDF_open_image_file($p, "jpeg", $imagefile, "", 0);
		die "Couldn't open image '$imagefile'" if ($image == -1);
		$width = PDF_get_value($p, "imagewidth", $image);
		$height = PDF_get_value($p, "imageheight", $image);
		PDF_place_image($p, $image, 220, $page_y - 20 - $height, 1);
		PDF_close_image($p, $image);

		$font = PDF_findfont($p, "Helvetica", "host", 0);
		PDF_setfont($p, $font, 10.0);
		PDF_set_text_pos($p, 500, $page_y - 40- $height);
		$page_number++;
		PDF_show($p, "Page $page_number");

		$font = PDF_findfont($p, "Helvetica", "host", 0);
		PDF_setfont($p, $font, 15.0);
		PDF_set_text_pos($p, 40, $page_y - 40 - $height);
		PDF_show($p, "Job Number: $title");
		

      }
	else
	{
	  	$imagefile = "./cslogo.jpg";

		$image = PDF_open_image_file($p, "jpeg", $imagefile, "", 0);
		die "Couldn't open image '$imagefile'" if ($image == -1);
		$width = PDF_get_value($p, "imagewidth", $image);
		$height = PDF_get_value($p, "imageheight", $image);
		PDF_place_image($p, $image, 40, $page_y - 40 - $height, 1);
		PDF_close_image($p, $image);
		$font = PDF_findfont($p, "Helvetica", "host", 0);
		PDF_setfont($p, $font, 10.0);
		PDF_set_text_pos($p, 510, $page_y - 40);
		$page_number++;
		PDF_show($p, "Page $page_number");
		$font = PDF_findfont($p, "Helvetica", "host", 0);
		PDF_setfont($p, $font, 15.0);
		PDF_set_text_pos($p, 365, $page_y - 40 - $height);
		PDF_show($p, "Job Number: $title");
		#return $page_y - 40 - $height;
	}

    $data_fileH = $ARGV[0];
    $margin          = 40;
    $page_col2_1     = 120;
    $page_col2_2     = 380;
    $page_col3_1     = 100;
    $page_col3_2     = 300;
    $page_col3_3     = 490;
    $page_col4_1     = 80;
    $page_col4_2     = 200;
    $page_col4_3     = 370;
    $page_col4_4     = 480;
    $tracking_col_1  = 40;
    $tracking_col_2  = 120;
    $tracking_col_3  = 210;
    $y_big_spacing   = 30;
    $y_small_spacing = 15;
    $field_font      = "Helvetica";
    $field_font_size = 10.0;
    $title_font      = "Helvetica-Bold";
    $title_font_size = 10.0;
    $field_title_font= "Helvetica";
    $field_title_font_size = 10.0;
    $bottom_margin         = 70;
    $default_encoding      = "host";
    # Ramya 26/11/2008 To Adjust reference field wrapping
    $refadjust=15;

    if ( $serv_logo == 2 ) {
        $page_left = $page_y - 180;
    }
    else{
        $page_left = $page_y - 140;
    }

    $true_page_left = $page_left;
    # let's open the xml data_file
    open( MESSAGE_FILE, $data_fileH );
    read( MESSAGE_FILE, $the_dataH, 10000000 );
    close(MESSAGE_FILE);

    $wddxH = new WDDX();
    $rs_dataH = $wddxH->deserialize($the_dataH);
    
    if ( $serv_logo == 2 ) {
    eval { $CN = $rs_dataH->get("CN")->as_scalar };
    if(length ($CN) >18){
    $CN=substr $CN, 0, 18;
    }
    print_bfield_title( "Account Name: ", $page_col3_1 - 25, $page_left + 15, "Caller: " );
    $new_page_left =  print_field( "$CN", $page_col3_1 + 15, $page_left + 15, "Caller: ",	$page_col3_2 );
    #$new_page_left =   print_field( "$CN", 300, 15, "Service Group: ", 	$page_col3_2 );
    if ( $new_page_left < $true_page_left ) {
        $true_page_left = $new_page_left;
    }
    }
    eval { $CA = $rs_dataH->get("CA")->as_scalar };
    if(length ($CA) >18){
    $CA=substr $CA, 0, 18;
    }
    print_bfield_title( "Caller: ", $page_col3_1, $page_left, "Department: " );
    $new_page_left =
      print_field( "$CA", $page_col3_1, $page_left, "Service Group: ",
        $page_col3_2 );
    if ( $new_page_left < $true_page_left ) {
        $true_page_left = $new_page_left;
    }

    eval { $JD = $rs_dataH->get("JD")->as_scalar };
    eval { $JT = $rs_dataH->get("JT")->as_scalar };
    print_bfield_title( "Date: ", $page_col3_2, $page_left, "Service Group: " );
    $new_page_left =
      print_field( "$JD $JT", $page_col3_2, $page_left, "Your Reference: ",
        $page_col3_3 );
    if ( $new_page_left < $true_page_left ) {
        $true_page_left = $new_page_left;
    }

    eval {$RE = $rs_dataH->get("RE")->as_scalar};
    #$RE = trim($RE);
    # Ramya 26/11/2008 To Adjust reference field wrapping
    if(length ($RE) > 10)
    {
        print_bfield_title ("Your Reference: ", ($page_col3_3-$refadjust), $page_left, "Your Reference: ");
        $new_page_left = print_field ("$RE", ($page_col3_3-$refadjust), $page_left, "", $page_x - $margin);
        if ($new_page_left < $true_page_left) {
            $true_page_left = $new_page_left;
        }

    }
    else
    {
        print_bfield_title ("Your Reference: ", $page_col3_3, $page_left, "Your Reference: ");
        $new_page_left = print_field ("$RE", $page_col3_3, $page_left, "", $page_x - $margin);
        if ($new_page_left < $true_page_left) {
            $true_page_left = $new_page_left;
        }
    }


    $page_left = $true_page_left - $y_small_spacing;

    eval { $SV = $rs_dataH->get("SV")->as_scalar };
    if(length ($SV) >18){
    $SV=substr $SV, 0, 18;
    }

    print_bfield_title( "Service: ", $page_col3_1, $page_left, "Department: " );
    $new_page_left =
      print_field( "$SV", $page_col3_1, $page_left, "Service Group: ",
        $page_col3_2 );
    if ( $new_page_left < $true_page_left ) {
        $true_page_left = $new_page_left;
    }

    print_bfield_title( "Service Group: ",
        $page_col3_2, $page_left, "Service Group: " );
    if ( $YZ == 1 ) {
        $new_page_left =
          print_field( "SAMEDAY", $page_col3_2, $page_left, "Your Reference: ",
            $page_col3_3 );
        $service = "courier";
    } elsif ( $YZ == 2 ) {
        $new_page_left =
          print_field( "WESTONE", $page_col3_2, $page_left, "Your Reference: ",
            $page_col3_3 );
        $service = "cab";
    }
    else {
        $new_page_left =
          print_field( "$YZ", $page_col3_2, $page_left, "Your Reference: ",
            $page_col3_3 );
    }

    if ( $new_page_left < $true_page_left ) {
        $true_page_left = $new_page_left;
    }

    eval { $VT = $rs_dataH->get("VT")->as_scalar };
    print_bfield_title( "Vehicle: ", $page_col3_3, $page_left, "Your Reference: " );
    $new_page_left =
      print_field( "$VT", $page_col3_3, $page_left, "", $page_x - $margin );
    if ( $new_page_left < $true_page_left ) {
        $true_page_left = $new_page_left;
    }

    $page_left = $true_page_left - $y_small_spacing;

    

    eval { $DT = $rs_data->get("DT")->as_scalar };
    if(length ($DT) >18){
    $DT=substr $DT, 0, 18;
    }
    print_bfield_title( "Department: ", $page_col3_1, $page_left, "Department: " );
    $new_page_left =
      print_field( "$DT", $page_col3_1, $page_left, "Service Group: ",
        $page_col3_2 );
    if ( $new_page_left < $true_page_left ) {
        $true_page_left = $new_page_left;
    }

    eval { $PH = $rs_data->get("PH")->as_scalar };
    print_bfield_title( "Phone: ", $page_col3_2, $page_left, "Service Group: " );
    $new_page_left =
      print_field( "$PH", $page_col3_2, $page_left, "Your Reference: ",
        $page_col3_3 );
    if ( $new_page_left < $true_page_left ) {
        $true_page_left = $new_page_left;
    }

    eval { $RJ = $rs_data->get("RJ")->as_scalar };
    print_bfield_title( "Return Job: ", $page_col3_3, $page_left,
        "Your Reference: " );
    $new_page_left =
      print_field( "$RJ", $page_col3_3, $page_left, "", $page_x - $margin );
    if ( $new_page_left < $true_page_left ) {
        $true_page_left = $new_page_left;
    }

    if ( $serv_logo == 2 ) {
        return $page_y - 40 - $height - 60;
    }
    else{
        return $page_y - 40 - $height - 60;
    }
    #return $page_y - 40 - $height - 30;
}

sub print_footer {
	my ($serv_grp) = @_;
	$font = PDF_findfont($p, "Helvetica", "host", 0);	
	$italicFont = PDF_findfont($p, "Helvetica-Oblique", "host", 0);
	PDF_setfont($p, $font, 8.0);
    if ($serv_grp == 2) {
        PDF_set_text_pos($p, 155, 59);
        PDF_show($p, "Contact Us:");
    }
        PDF_setfont($p, $font, 7.0);
	PDF_set_text_pos($p, 155, 50);
	if ($serv_grp == 2) {
	    PDF_show($p, "Website: www.westonecars.co.uk");
	    PDF_set_text_pos($p, 155,41);
            PDF_show($p, "Email: customerservices\@westonecars.co.uk");
	    PDF_set_text_pos($p, 155, 32);
	    PDF_show($p, "Customer Services");
            PDF_set_text_pos($p, 155, 23);
            PDF_show($p, "Phone: 0870 873 2152");
            PDF_set_text_pos($p, 155, 14);
		PDF_show($p, "Fax: 0870 873 2232");
            } else {
	    #PDF_show($p, "Website: www.citysprint.co.uk");
	    #PDF_set_text_pos($p, 155,41);
        #    PDF_show($p, "Email: customerservices\@citysprint.co.uk");
	    #PDF_set_text_pos($p, 155, 32);
	    #PDF_show($p, "Telephone: Customer Services");
        #    PDF_set_text_pos($p, 155, 23);
        #    PDF_show($p, "London on 0207 203 7590");
        #    PDF_set_text_pos($p, 155, 14);
        #    PDF_show($p, "Outside London on 08707 510076");
	    }
# Print Office details
        PDF_setfont($p, $font, 8.0);
        PDF_set_text_pos($p, 20, 59);
	if ($serv_grp == 2) {
	   PDF_show($p, "WestOne Cars Ltd");
	    PDF_setfont($p, $font, 7.0);
          PDF_set_text_pos($p, 20, 50);
	    PDF_show($p, "Registered office");
          PDF_set_text_pos($p, 20, 41);
          PDF_show($p, "58-62 Scrutton Street");
          PDF_set_text_pos($p, 20, 32);
          PDF_show($p, "London, EC2A 4PH");
          PDF_set_text_pos($p, 20, 23);
	    PDF_show($p, "Registered No. 6229424 (England)");
          PDF_set_text_pos($p, 20, 14);
          PDF_show($p, "VAT No. 916 3811 27");

	} else {
	   PDF_show($p, "CitySprint (UK) Ltd");
	    	#PDF_setfont($p, $font, 7.0);
			PDF_setfont($p, $italicFont, 7.0);
			
        	PDF_set_text_pos($p, 20, 50);
	      PDF_show($p, "Registered office:");
		  PDF_setfont($p, $font, 7.0);
		  PDF_set_text_pos($p, 76, 50);
        	PDF_show($p, "Ground Floor,");
        	PDF_set_text_pos($p, 20, 41);
        	PDF_show($p, "RedCentral, 60 High Street,");
        	PDF_set_text_pos($p, 20, 32);
        	PDF_show($p, "Redhill, Surrey RH1 1SH ");
        	PDF_set_text_pos($p, 20, 23);
			PDF_setfont($p, $italicFont, 7.0);
		#PDF_show($p, "Registered No. 4327611 (England)");
		PDF_show($p, "Registered No.");
		
		PDF_setfont($p, $font, 7.0);
		PDF_set_text_pos($p, 68, 23);
		PDF_show($p, "4327611 (England)");
        	PDF_set_text_pos($p, 20, 14);
        	PDF_show($p, "VAT No. 997 3273 64");
	}

# Print the regional centres
	if($serv_grp == 1) {
        PDF_setfont($p, $font, 7.0);
        PDF_set_text_pos($p, 230, 59);
        PDF_show($p, "Aberdeen");
        PDF_set_text_pos($p, 230, 50);
        PDF_show($p, "Basingstoke");
        PDF_set_text_pos($p, 230, 41);
        PDF_show($p, "Birmingham");
        PDF_set_text_pos($p, 230, 32);
		PDF_show($p, "Bracknell");
		
        
        PDF_set_text_pos($p, 230, 23);
		PDF_show($p, "Brentwood");
		
		PDF_set_text_pos($p, 230, 14);
		PDF_show($p, "Brighton");
		
		PDF_set_text_pos($p, 285, 59);
        PDF_show($p, "Bristol");
        PDF_set_text_pos($p, 285, 50);
        PDF_show($p, "Cambridge");

        PDF_set_text_pos($p, 285, 41);
        PDF_show($p, "Cardiff");
        PDF_set_text_pos($p, 285, 32);
        #PDF_show($p, "Cheltenham");
        #PDF_set_text_pos($p, 285, 41);
        #PDF_show($p, "Coventry");
        #PDF_set_text_pos($p, 285, 32);
        PDF_show($p, "Croydon");
        #PDF_set_text_pos($p, 285, 41);
        #PDF_show($p, "Docklands");
        PDF_set_text_pos($p, 285, 23);
        PDF_show($p, "Edinburgh");
		PDF_set_text_pos($p, 285, 14);
        PDF_show($p, "Enfield");
		
        PDF_set_text_pos($p, 340, 59);
        PDF_show($p, "Gatwick");
        PDF_set_text_pos($p, 340, 50);
        PDF_show($p, "Glasgow");

        PDF_set_text_pos($p, 340, 41);
        PDF_show($p, "Guildford");

        PDF_set_text_pos($p, 340, 32);
        PDF_show($p, "Heathrow");
        PDF_set_text_pos($p, 340, 23);
        PDF_show($p, "Hemel Hempstead");
        PDF_set_text_pos($p, 340, 14);
        #PDF_show($p, "High Wycombe");
		PDF_show($p, "Leeds");
		
        #PDF_set_text_pos($p, 340, 23);
        #PDF_show($p, "Ipswich");
        PDF_set_text_pos($p, 410, 59);
        PDF_show($p, "London");

        PDF_set_text_pos($p, 410, 50);
        PDF_show($p, "London UK & Intl");

		PDF_set_text_pos($p, 410, 41);
        PDF_show($p, "London Logistics");
        #PDF_set_text_pos($p, 410, 59);
        #PDF_show($p, "Maidstone");
        PDF_set_text_pos($p, 410, 32);
        PDF_show($p, "Manchester");
        PDF_set_text_pos($p, 410, 23);
        PDF_show($p, "Medway");
        #PDF_set_text_pos($p, 410, 32);
        #PDF_show($p, "MidlandCentre");
        PDF_set_text_pos($p, 410, 14);
        PDF_show($p, "Milton Keynes");
        #PDF_set_text_pos($p, 410, 23);
        #PDF_show($p, "Newbury");
		
		PDF_set_text_pos($p, 473, 59);
        PDF_show($p, "Newcastle");
		
        PDF_set_text_pos($p, 473, 50);
        PDF_show($p, "Norwich");

        #PDF_set_text_pos($p, 473, 59);
        #PDF_show($p, "Northampton");
        PDF_set_text_pos($p, 473, 41);
        PDF_show($p, "Nottingham");
        PDF_set_text_pos($p, 473, 32);
        PDF_show($p, "Oxford");
        PDF_set_text_pos($p, 473, 23);
        PDF_show($p, "Plymouth");
		
		PDF_set_text_pos($p, 473, 14);
        PDF_show($p, "Reading");
		
		PDF_set_text_pos($p, 520, 59);
        PDF_show($p, "Redhill");
		
		PDF_set_text_pos($p, 520, 50);
        PDF_show($p, "Slough");
		
        PDF_set_text_pos($p, 520, 41);
        PDF_show($p, "Southampton");
				
        PDF_set_text_pos($p, 520, 32);
        PDF_show($p, "Swindon");
        #PDF_set_text_pos($p, 473, 14);
        #PDF_show($p, "Thames Valley");

        PDF_set_text_pos($p, 520, 23);
        PDF_show($p, "Telford");
		
		PDF_set_text_pos($p, 520, 14);
        PDF_show($p, "Warwick");

	}

	PDF_end_page($p);
}

#Job Number = JN
#Job Date = JD
#Job Time = JT
#Department = DT
#Reference = RE
#Caller = CA
#Service = SV
#Service Group = YZ
#Vehicle = VT
#Phone = PH
#Return Job = RJ
#Advance Booking = AB
#Ready At = RA
#Ready On = RO
#Price = BP
#GST/VAT = GS
#Pickup Company = PC
#Pickup Address = PA
#Pickup Suburb = PS
#Pickup Instructions = PI
#Pickup Contact = PX
#Weight =WE
#No of Items = PN
#Height = PH
#Length = PL
#Depth = PD
#Delivery Company = DC
#Delivery Address = DA
#Delivery Suburb = DS
#Delivery Instructions  = DI
#Delivery Contact = DX
#Track Date = AD
#Track Time = AT
#Track Event = AE
# test url
# http://10.1.1.1/cnet/src/couriernote.php?JN=12323&JD=10/1/01&JT=10:10:11&DM=Dept. one&RE=My ref&CA=robh&SV=110306+SERV+(P/U+BY+3pm)&SG=1&VT=Motor Bike&PH=(02)93653520&RJ=Yes&AB=Yes&RA=10:10:12&RO=10/2/02&BP=10000&GS=1000&PC=TEST+COMPANY+2&PA=79+JONES+STREET&PS=NORWICH&PI=&PX=robh&WE=100&PN=5&PH=100&PL=10&DC1=TEST+3+COMPANY&DA1=12+HEWITT+ST&DS1=LONDON,+GREATER+LONDON&DX1=QWERTY&DI1=+&AD1=10/10/00&AT1=10:10:10&AE1=Pickup
