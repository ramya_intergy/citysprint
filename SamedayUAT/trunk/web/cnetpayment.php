<?php 
// to add a customer for credit card usage only use the following array
$cc = array ("TEST2", "TEST3", "VC01W", "ABAM02", "AMEX00", "AMEX01", "AMEX05", "AMEX067", "AMEX068", "AMEX07", "AMEX071", "AMEX072", "AMEX073", "AMEX074", "AMEX076", "AMEX077", "AMEX078", "AMEX079", "AMEX083", "AMEX084", "AMEX085", "AMEX087", "AMEX089", "AMEX091", "AMEX093", "AMEX096", "AMEX097", "AMEX099", "AMEX10", "AMEX106", "AMEX11", "AMEX110", "AMEX119", "AMEX120", "AMEX122", "AMEX123", "AMEX124", "AMEX125", "AMEX126", "AMEX127", "AMEX128", "AMEX129", "AMEX13", "AMEX130", "AMEX131", "AMEX132", "AMEX133", "AMEX134", "AMEX137", "AMEX139", "AMEX14", "AMEX142", "AMEX15", "AMEX151", "AMEX152", "AMEX16", "AMEX19", "AMEX20", "AMEX200", "AMEX201", "AMEX202", "AMEX21", "AMEX23", "AMEX24", "AMEX25", "AMEX26", "AMEX31", "AMEX32", "AMEX33", "AMEX34", "AMEX39", "AMEX42", "AMEX43", "AMEX47", "AMEX49", "AMEX50", "AMEX51", "AMEX52", "AMEX62", "AMEX64", "AMEX65", "AMEX66", "AMEX92", "AMX203", "AMXPCO06");
// to add a customer for credit card usage and invoice use the following array
$ccboth = array ("TEST4", "TEST5", "TEST6");
// mail header setting from field
$headers = "From: Internet Booking <booking@ijb.citysprint.co.uk>\r\n";
if ($AC==1) {
	if (in_array ($CK, $cc)) {
		print "OKCC";
	}
	else {
		if (in_array ($CK, $ccboth)) {
			print "OKBOTH";
		}
		else {
			print "ERR";
		}
	}
}
else {
	if ($AC==2) { 
		$tomail = "vmcgarvey@citysprint.co.uk";
		$subject = "$JN $CK"; 
		$message = "Customer Key: $CK\nCaller: $CA\nPhone: $PH\nJob No: $JN\n\nType: $L2\nHolders Name: $L3\nCredit Card No: $L4\nExpiry Date (MMYY): $L5\nStart Date: $L6\nIssue No: $L7\n"; 
		mail($tomail,$subject,$message,$headers); 
		print "OK";
	}
	else {
		print "ERR";
	}
}

?> 
