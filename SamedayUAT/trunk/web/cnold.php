<?php
// Customer Key = CK
// Password = PW
//Transaction Stamp = TS
//Job Number = JN
//Job Date = JD
//Job Time = JT
//Department = DT
//Reference = RE
//Caller = CA
//Service = SV
//Service Group = YZ
//Vehicle = VT
//Phone = PH
//Return Job = RJ
//Advance Booking = AB
//Ready At = RA
//Ready On = RO
//Price = BP
//GST/VAT = GS
//Pickup Company = PC
//Pickup Address = PA
//Pickup Suburb = PS
//Pickup Instructions = PI
//Pickup Contact = PX
//Weight = WE 
//No of Items = PN
//Height = PH
//Length = PL
//Depth = PD
//Delivery Company = DC
//Delivery Address = DA
//Delivery Suburb = DS
//Delivery Instructions  = DI
//Delivery Contact = DX
//Track Date = AD
//Track Time = AT
//Track Event = AE
//Signature = DY
// test url
// http://10.1.1.1/cnet/src/couriernote.php?JN=12323&JD=10/1/01&JT=10:10:11&DT=Dept.%20one&RE=My%20ref&CA=robh&SV=110306+SERV+(P/U+BY+3pm)&YZ=1&VT=Motor%20Bike&PH=(02)93653520&RJ=Yes&AB=Yes&RA=10:10:12&RO=10/2/02&BP=10000&GS=1000&PC=TEST+COMPANY+2&PA=79+JONES+STREET&PS=NORWICH&PI=&PX=robh&WE=100&PN=5&PH=100&PL=10&PD=1&DC1=TEST+3+COMPANY&DA1=12+HEWITT+ST&DS1=LONDON,+GREATER+LONDON&DX1=QWERTY&DI1=+&AD1=10/10/00&AT1=10:10:10&AE1=Pickup&AD2=10/10/00&AT2=10:20:10&AE2=Deliver/couriernote.pdf

$t2b = '/usr/local/server/brtc/graphics/bin/t2b';
$btop = '/usr/local/server/brtc/graphics/bin/bmptopnm';
$ptoj = '/usr/local/server/brtc/graphics/bin/pnmtojpeg';


$packet_id = wddx_packet_start();

$xml_packet_start="<wddxPacket version='1.0'><header></header><data><struct>";
$xml_packet_end="</struct></data></wddxPacket>";
//Job Number = JN
if (isset ($JN)) {
	wddx_add_vars($packet_id, "JN");
}
//Job Date = JD
if (isset ($JD)) {
	wddx_add_vars($packet_id, "JD");
}
//Job Time = JT
if (isset ($JT)) {
	wddx_add_vars($packet_id, "JT");
}
//Department = DT
if (isset ($DT)) {
	wddx_add_vars($packet_id, "DT");
}
//Reference = RE
if (isset ($RE)) {
	wddx_add_vars($packet_id, "RE");
}
//Caller = CA
if (isset ($CA)) {
	wddx_add_vars($packet_id, "CA");
}
//Service = SV
if (isset ($SV)) {
	wddx_add_vars($packet_id, "SV");
}
//Service Group = YZ
if (isset ($YZ)) {
	wddx_add_vars($packet_id, "YZ");
}
//Vehicle = VT
if (isset ($VT)) {
	wddx_add_vars($packet_id, "VT");
}
//Phone = PH
if (isset ($PH)) {
	wddx_add_vars($packet_id, "PH");
}
//Return Job = RJ
if (isset ($RJ)) {
	wddx_add_vars($packet_id, "RJ");
}
//Advance Booking = AB
if (isset ($AB)) {
	wddx_add_vars($packet_id, "AB");
}
//Ready At = RA
if (isset ($RA)) {
	wddx_add_vars($packet_id, "RA");
}
//Ready On = RO
if (isset ($RO)) {
	wddx_add_vars($packet_id, "RO");
}
//Price = BP
if (isset ($BP)) {
	wddx_add_vars($packet_id, "BP");
}
//GST/VAT = GS
if (isset ($GS)) {
	wddx_add_vars($packet_id, "GS");
}
//Total Cost = TT
if (isset ($TT)) {
	wddx_add_vars($packet_id, "TT");
}
//Pickup Company = PC
if (isset ($PC)) {
	wddx_add_vars($packet_id, "PC");
}
//Pickup Address = PA
if (isset ($PA)) {
	wddx_add_vars($packet_id, "PA");
}
//Pickup Suburb = PS
if (isset ($PS)) {
	wddx_add_vars($packet_id, "PS");
}
//Pickup Instructions = PI
if (isset ($PI)) {
	wddx_add_vars($packet_id, "PI");
}
//Pickup Contact = PX
if (isset ($PX)) {
	wddx_add_vars($packet_id, "PX");
}
//Weight = WE 
if (isset ($WE)) {
	wddx_add_vars($packet_id, "WE");
}
//No of Items = PN
if (isset ($PN)) {
	wddx_add_vars($packet_id, "PN");
}
//Height = HE
if (isset ($HE)) {
	wddx_add_vars($packet_id, "HE");
}
//Length = PL
if (isset ($PL)) {
	wddx_add_vars($packet_id, "PL");
}
//Depth = PD
if (isset ($PD)) {
	wddx_add_vars($packet_id, "PD");
}
//Cubice m = CU
if (isset ($CU)) {
	wddx_add_vars($packet_id, "CU");
}

// Do delivery address
$tagcount = 0;
$finished=FALSE;
$index=0;

while (!$finished) {
    	$tagcount += 1;
	//Delivery Company = DCn
	$gen_var = "DC" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DC'] = $$gen_var;
	}
	//Delivery Address = DAn
	$gen_var = "DA" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DA'] = $$gen_var;
	} else {
		$finished = TRUE;
	}
	//Delivery Suburb = DSn
	$gen_var = "DS" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DS'] = $$gen_var;
	}
	//Delivery Instructions  = DIn
	$gen_var = "DI" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DI'] = $$gen_var;
	}
	//Delivery Contact = DXn
	$gen_var = "DX" . $tagcount;
	if (isset ($$gen_var)) {
		$delivery_address_array[$index]['DX'] = $$gen_var;
	}
	$index++;
}
if ($delivery_address_array) {
	wddx_add_vars($packet_id, "delivery_address_array");
}

// Do tracking details
$tagcount = 0;
$finished=FALSE;
$index=0;
while (!$finished) {
    	$tagcount += 1;
	//Track Date = AD
	$gen_var = "AD" . $tagcount;
	$gen_val = $$gen_var;
	if (isset ($$gen_var)) {
		$tracking_details_array[$index]['AD'] = $$gen_var;
	} else {
		$finished = TRUE;
	}
	//Track Time = AT
	$gen_var = "AT" . $tagcount;
	if (isset ($$gen_var)) {
		$tracking_details_array[$index]['AT'] = $$gen_var;
	}
	//Track Event = AE
	$gen_var = "AE" . $tagcount;
	if (isset ($$gen_var)) {
		$tracking_details_array[$index]['AE'] = $$gen_var;
	}
	$index++;
}

if ($tracking_details_array) {
	wddx_add_vars($packet_id, "tracking_details_array");
}

// Do signatures
$tagcount = 0;
$finished=FALSE;
$index=0;
$anysigs = "";
	
while (!$finished) {
    	$tagcount += 1;
	// Signatures = DYn
//	$ST = "";
	$full_jpeg_filename = "";	
	$name = "";
	$address = "";
	$temp = "";
	$temp2 = "";
	$gen_var = "DY" . $tagcount;
	if (isset ($$gen_var)) {
	    if ($$gen_var == "0") {
// Nothing
			$sigtypearr = array("");
//			$ST = "POD";
		}
	    if ($$gen_var == "1") {
// POD Only
			$sigtypearr = array("POD");
//			$ST = "POD";
		}
	    if ($$gen_var == "2") {
// Wait Only
			$sigtypearr = array("WT");
//			$ST = "WT";
		}
	    if ($$gen_var == "3") {
// Both
			$sigtypearr = array("POD","WT");
//			$ST = "POD";
//			$ST = "WT";
		}
		reset ($sigtypearr);
		foreach ($sigtypearr as $ST) {
		  if ($ST) {
		    $site = "http://ijb.citysprint.co.uk/dms/bin/job_sig?cust_key=$CK&password=$PW&TS=&job_no=$JN&drop_no=$tagcount&sig_type=$ST";
		 	$open = fopen($site, "r"); 
    		$search = fread($open, 50000); 
    		fclose($open); 
// ^TS12345^SUY^NMTimothy ^AD58/62 SCRUTTON STREET^JSfgdfgidgfiudgfidgfidgf^ZZ		
			$len = strlen($search);
			$pos1 = strpos($search, "^NM");
			$pos2 = strpos($search, "^AD");
			$pos3 = strpos($search, "^JS");
			$pos4 = strpos($search, "^ZZ");
			$name = substr($search, $pos1+3,$pos2-$pos1-3);
			$address = substr($search, $pos2+3, $pos3-$pos2-3);
			$sig = substr($search, $pos3+3, $pos4-$pos3-3);
//		$sig = substr($search, $pos3+3, $len-$pos3-3-3);

			if ($sig) {
				$anysigs = "yes";
	        	$tmp_file_name = '/tmp/sig_file_bmp' . rand() . '.txt';
    			$tmpopen = fopen($tmp_file_name, "ab+"); 	
				fwrite ($tmpopen,$sig);
		    	fwrite($tmpopen,"\n");	
				fclose($tmpopen);
			
        		$bmp_filename = '/tmp/sig_file_bmp' . $JN.$tagcount.$index.'R'.rand() . '.bmp';		
				$full_bmp_filename = $DOCUMENT_ROOT . $bmp_filename;
        		$jpeg_filename = '/tmp/sig_file_jpg' . $JN.$tagcount.$index.'R'.rand() . '.jpg';		
				$full_jpeg_filename = $DOCUMENT_ROOT . $jpeg_filename;
				$com = $t2b.' '.$tmp_file_name.' '.$full_bmp_filename;
        		$exec_return = exec ($com);
		        unlink ($tmp_file_name);				
				$com = $btop.' '.$full_bmp_filename. ' | '.$ptoj.' > '.$full_jpeg_filename;
        		$exec_return = exec ($com);
		        unlink ($full_bmp_filename);
		  		if ($tagcount==1) {
			  		$temp2 = "Sender ";
		  		}
		  		else {
			  		$temp = $tagcount-1;
					$temp2 = "Receiver ".$temp." ";
		  		}
			}
		  }
		  $signatures_array[$index]['TC'] = $temp2;
		  $signatures_array[$index]['ST'] = $ST;		
		  $signatures_array[$index]['JM'] = $full_jpeg_filename;		
		  $signatures_array[$index]['NM'] = $name;
		  $signatures_array[$index]['AY'] = $address;		
		  $index++;
		}
		
	} else {
		$index++;
		$finished = TRUE;
	}
}
if ($anysigs) {
	wddx_add_vars($packet_id, "signatures_array");
}


$xml_packet = wddx_packet_end($packet_id); 
//print (htmlentities($xml_packet));

        $filename = "/tmp/pdf_xml_data." . $JN . date("U");
        // let's create a temporary file
        $fp=fopen($filename,"w");
        fwrite ($fp, $xml_packet);
        fclose($fp);
        $pdf_filename = "/tmp/pdf_file_" . $JN . date("U") . ".pdf";
        $full_pdf_filename = $DOCUMENT_ROOT . $pdf_filename;
		$com = '/usr/local/server/perl/bin/perl /usr/local/server/brtc/csfo/couriernote.pl ' . $filename . " > " . $full_pdf_filename;
        $exec_return = exec ($com);
        print "<meta http-equiv=\"Refresh\" content=\"0; URL=$pdf_filename\">";

//        unlink ($filename);


?>
