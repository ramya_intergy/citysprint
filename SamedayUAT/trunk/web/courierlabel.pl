#!/usr/local/server/perl/bin/perl 
# Include various Perl modules
use WDDX;     # Wddx support for Perl, asee www.Wddx.org
use Time::Local;
use Data::Dumper;
# use pdflib_pl 3.03;
use pdflib_pl;
use Text::Wrap;

# Establish some constants to control behavior
$packetFilename   = "/tmp/rr";   # File to retrieve stuff from
$page_x = 595;
$page_y = 842;
$margin = 40;

$page_col1_1 = 200;
$page_col1_2 = 420;

$page_col2_1 = 150;

$page_col3_1 = 100;
$page_col3_2 = 300;
$page_col3_3 = 490;
$y_big_spacing = 23;
$y_small_spacing = 15;
$field_font = "Helvetica";
$field_font_size = 18.0;
$title_font = "Times-Roman";
$title_font_size = 13.0;
$field_title_font = "Times-Roman";
$field_title_font_size = 10.0;
$bottom_margin = 70;



if (!$ARGV[0]) {
	print "Usage: courierlabel.pl xml_packet_filename\n\n";
	exit 1;
}
$data_file=$ARGV[0];

# let's open the data_file
open (MESSAGE_FILE, $data_file);
read (MESSAGE_FILE, $the_data, 10000000);
close (MESSAGE_FILE);
#unlink $data_file;

# Create new Wddx "worker object" to do our work for us
$wddx = new WDDX();

        $rs_data = $wddx->deserialize( $the_data );

$p = PDF_new();
PDF_set_parameter($p, "compatibility", "1.4");

die "Couldn't open PDF file" if (PDF_open_file($p, "-") == -1);

PDF_set_info($p, "Creator", "BRTC");
PDF_set_info($p, "Author", "BRTC");
PDF_set_info($p, "Title", "Courierlabel");

$lineimagefile = "C:/xampp/htdocs/CS_Sameday/line1.jpg";
$lineimage = PDF_open_image_file($p, "jpeg", $lineimagefile, "", 0);
die "Couldn't open image '$lineimagefile" if ($lineimage == -1);
$lineheight = PDF_get_value($p, "imageheight", $lineimage);

# job number
	eval {$JN = $rs_data->get("JN")->as_scalar};
# Service Group
        eval {$YZ = $rs_data->get("YZ")->as_scalar};
# date
	eval {$JD = $rs_data->get("JD")->as_scalar};
# Company
	eval {$PC = $rs_data->get("PC")->as_scalar};
# Address
	eval {$PA = $rs_data->get("PA")->as_scalar};
# Address2
	eval {$P2 = $rs_data->get("P2")->as_scalar};
# Suburb
	eval {$PS = $rs_data->get("PS")->as_scalar};
# County
	eval {$P5 = $rs_data->get("P5")->as_scalar};
# Postcode
	eval {$PP = $rs_data->get("PP")->as_scalar};
# Reference
	eval {$RE = $rs_data->get("RE")->as_scalar};
# Contact
	eval {$PX = $rs_data->get("PX")->as_scalar};	
# Customer Key
	eval {$CK = $rs_data->get("CK")->as_scalar};	
# No of items / labels
	eval {$PN = $rs_data->get("PN")->as_scalar};
# Delivery address array	
	eval {$delivery_address_array = $rs_data->get("delivery_address_array")};
	
	if ( !$@ && $delivery_address_array ) {
	$number_of_dropoffs = $delivery_address_array->length;
	} else {
	$number_of_dropoffs = 0;
	}
	
	if ($number_of_dropoffs > 1) {
		$maxlabels = $number_of_dropoffs;
		$maxcount = 1;
		$drops = 1;
	}
	else {
		if ($PN eq "") {
			$PN = 1;
		}
		if ($PN eq "0") {
			$PN = "1";
		}
		$drops = 0;
		$maxlabels = $PN;
		$maxcount = $PN;
	}
	
	$top = 1;
	$firsttime = 1;
    for ($i=0;$i < $maxlabels ;$i++) {
$field_font_size = 14.0;					
		if ($top) {
			$logo_pos = 802;
 			$foot_pos = 498; 
			$yoffset = 0;
			$top = 0;
		}
		else {
			$logo_pos = 381; 		
 			$foot_pos = 77; 
			$yoffset = 421;
			$top = 1;
		}
		print_logo($logo_pos,$YZ);
		
		
		
		if ($maxcount eq 1) {
			$count = 1;
		}
		else {
			$count = $i+1;
		}
		$page_left = 782-$yoffset;
		$true_page_left = $page_left;

		print_company_title($PC, $page_col1_1, $page_left);
		$field_font_size = 12.0;		
		print_field_title ("Ref: ", $page_col1_2, $page_left);
		$new_page_left = print_field ("$RE", $page_col1_2, $page_left, " ", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
		$page_left = $true_page_left - $y_small_spacing;
		print_company_title($PA, $page_col1_1, $page_left);
		print_field_title ("Job No: ", $page_col1_2, $page_left);
		$new_page_left = print_field ("$JN", $page_col1_2, $page_left, " ", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
		$page_left = $true_page_left - $y_small_spacing;
		
		print_company_title($P2, $page_col1_1, $page_left);
		print_field_title ("Date: ", $page_col1_2, $page_left);
		$new_page_left = print_field ("$JD", $page_col1_2, $page_left, " ", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
		
		$page_left = $true_page_left - $y_small_spacing;
		
		print_company_title($PS, $page_col1_1, $page_left);
		print_field_title ("Contact: ", $page_col1_2, $page_left);
		$new_page_left = print_field ("$PX", $page_col1_2, $page_left, " ", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}

		$page_left = $true_page_left - $y_small_spacing;

		print_company_title($P5, $page_col1_1, $page_left);
		$page_left = $true_page_left - $y_small_spacing;

		print_company_title($PP, $page_col1_1, $page_left);

		$page_left = $true_page_left - $y_small_spacing;
		
		PDF_place_image($p, $lineimage, 30, $page_left - $lineheight, 1);
# set a colour for the line 
#		pdf_setcolor($p, "stroke", "rgb", 0, 0, 0); 
# draw a line  
#		pdf_moveto($p, 40, $page_left); 
#		pdf_lineto($p, 555, $page_left); 
#		pdf_stroke($p); 	
		$page_left = $page_left - $y_big_spacing;
if ( !$@ && $delivery_address_array ) {
		if ($drops || $firsttime) {
			$firsttime = 0;
			$DC = "";
			$DA = "";
			$DS = "";
			$DI = "";
			$DX = "";

			#Get the data
			$array_row = $delivery_address_array->shift();
			#Delivery Company = DC
			eval {$DC = $array_row->get("DC")->as_scalar};
			#Delivery Address = DA
			eval {$DA = $array_row->get("DA")->as_scalar};
			#Delivery Address = D2
			eval {$D2 = $array_row->get("D2")->as_scalar};
			#Delivery Suburb = DS
			eval {$DS = $array_row->get("DS")->as_scalar};
			#Delivery County = DO
			eval {$DO = $array_row->get("DO")->as_scalar};
			#Delivery Postcode = DP
			eval {$DP = $array_row->get("DP")->as_scalar};
			#Delivery Instructions  = DI
			eval {$DI = $array_row->get("DI")->as_scalar};
			#Delivery Contact = DX
			eval {$DX = $array_row->get("DX")->as_scalar};
		}
		}
		$field_font_size = 12.0;					
 		print_field_title ("Receiver: ", $page_col2_1, $page_left);
		$new_page_left = print_field ("$DC", $page_col2_1, $page_left, " ", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
		$page_left = $true_page_left - $y_small_spacing;

		print_field_title ("Bldg Name/No: ", $page_col2_1, $page_left);
		$new_page_left = print_field ("$DA", $page_col2_1, $page_left, " ", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}

		$page_left = $true_page_left - $y_small_spacing;

		print_field_title ("Address: ", $page_col2_1, $page_left);
		$new_page_left = print_field ("$D2", $page_col2_1, $page_left, " ", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}

		$page_left = $true_page_left - $y_small_spacing;
		
		print_field_title ("Town: ", $page_col2_1, $page_left);
		$new_page_left = print_field ("$DS", $page_col2_1, $page_left, " ", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
		
		$page_left = $true_page_left - $y_small_spacing;

		print_field_title ("County: ", $page_col2_1, $page_left);
		$new_page_left = print_field ("$DO", $page_col2_1, $page_left, " ", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
		
		$page_left = $true_page_left - $y_small_spacing;

		print_field_title ("Postcode: ", $page_col2_1, $page_left);
		$new_page_left = print_field ("$DP", $page_col2_1, $page_left, " ", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}

		$page_left = $true_page_left - $y_small_spacing;
		
		print_field_title ("Contact: ", $page_col2_1, $page_left);
		$new_page_left = print_field ("$DX", $page_col2_1, $page_left, " ", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
		$page_left = $true_page_left - $y_small_spacing;
		$field_font_size = 12.0;							
		print_field_title ("Instructions: ", $page_col2_1, $page_left);
if(length ($DI) > 30){
my $DelInst1  = substr $DI, 0, 30;  
		$page_left = print_field ("$DelInst1", $page_col2_1, $page_left, " ", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
my $DelInst2  = substr $DI, 30;                  
                $page_left = print_field ("$DelInst2", $page_col2_1, $page_left, " ", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
}else {
$page_left = print_field ("$DI", $page_col2_1, $page_left, " ", $page_x - $margin);
if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
}
		$page_left = $true_page_left - $y_big_spacing ;
		
		PDF_place_image($p, $lineimage, 30, $page_left - $lineheight, 1);		
# set a colour for the line 
#		pdf_setcolor($p, "stroke", "rgb", 0, 0, 0); 
# draw a line  
#		pdf_moveto($p, 40, $page_left); 
#		pdf_lineto($p, 555, $page_left); 
#		pdf_stroke($p); 	
		$page_left = $page_left - $y_big_spacing;
$field_font_size = 12.0;							
		print_field_title ("Quantity: ", $page_col3_1, $page_left);
		$new_page_left = print_field ("$PN", $page_col3_1, $page_left, "Weight ", $page_col3_2);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
		
		print_field_title ("Weight: ", $page_col3_2, $page_left);
		$new_page_left = print_field ("$WE Kg", $page_col3_2, $page_left, "Volume: ", $page_col3_3);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}
		
		print_field_title ("Volume: ", $page_col3_3, $page_left);
		$page_left = print_field ("$CU m3", $page_col3_3, $page_left, "", $page_x - $margin);
		if ($new_page_left < $true_page_left) {
			$true_page_left = $new_page_left;
		}

		$page_left = $true_page_left - $y_small_spacing;
		PDF_place_image($p, $lineimage, 30, $page_left - $lineheight + 2 , 1);		
		print_label_footer($foot_pos ,$count,$maxcount);		
		
	}
	
	if ($page_left > 380) {
 		PDF_end_page($p);
	}
	PDF_close_image($p, $lineimage);
PDF_close($p);

PDF_delete($p);

sub is_even {
	my ($number) = @_;
	if ($number % 2 == 0) {
		return 1;
	} else {
		return 0;
	}
}

sub print_field {
	my ($field, $left_x, $left_y, $next_field, $next_col) = @_;
	$font = PDF_findfont($p, $field_title_font, "host", 0);
	$max_width = $next_col - $left_x - PDF_stringwidth($p, $next_field, $font, $field_title_font_size);
	#print "$next_col $left_x " . PDF_stringwidth($p, $next_field, $font, $field_title_font_size) . "\n";
	$height_increment = $field_font_size;
	$my_height = $height_increment;

	$font = PDF_findfont($p, $field_font, "host", 0);
	PDF_setfont($p, $font, $field_font_size);

	# Let's find out if the line will fit in
	if ($max_width > PDF_stringwidth($p, $field, $font, $field_font_size)) {
		# just print it
		PDF_show_xy($p, $field, $left_x, $left_y);
	} else {
		$* = 1;
		$i = 0;
		@field_array = split(' ', $field);
		for ($i=0;$i < $#field_array + 1;$i++) {
			if ($the_line) {
				$the_line = $the_line . " " . @field_array[$i];
			} else {
				$the_line = @field_array[$i];
			}
			if ($max_width > PDF_stringwidth($p, $the_line, $font, $field_font_size)) {
				if ($i < $#field_array) {
					$temp_line = $the_line . " " . " " . @field_array[$i + 1];
					if ($max_width < PDF_stringwidth($p, $temp_line, $font, $field_font_size)) {
						# we know we cannot add anymore words so let's print the line.
						#print "1 $#field_array, $i, $the_line, $temp_line \n";
						PDF_show_xy($p, $the_line, $left_x, $left_y);
						$left_y = $left_y - ($height_increment + 2);
						$the_line="";
					}
#print "here 1\n";
				} else {
						#print "2 $#field_array, $i, $the_line, $temp_line \n";
					PDF_show_xy($p, $the_line, $left_x, $left_y);
					$the_line="";
#print "here 2\n";
				}
			}
		}
	}
						#print "$#field_array, $i, $the_line, $temp_line \n";
	return $left_y;
}

sub print_title {
	my ($title, $left_x, $left_y) = @_;
	
	$font = PDF_findfont($p, $field_title_font, "host", 0);
	PDF_setfont($p, $font, $title_font_size);
	PDF_set_text_pos($p, $left_x, $left_y);
	PDF_show($p, $title);
}

sub print_company_title {
	my ($title, $left_x, $left_y) = @_;	
	$font = PDF_findfont($p, $field_font, "host", 0);
	PDF_setfont($p, $font, $title_font_size);
	PDF_set_text_pos($p, $left_x, $left_y);
	PDF_show($p, $title);
}

sub print_field_title {
	my ($title, $right_x, $left_y) = @_;
	
	$font = PDF_findfont($p, $field_title_font, "host", 0);
	PDF_setfont($p, $font, $field_title_font_size);
	PDF_set_text_pos($p, $right_x - PDF_stringwidth($p, $title, $font, $field_title_font_size), $left_y);
	PDF_show($p, $title);
}

sub print_logo {
# pos_y for a4 & two labels: 802, 381
	my($pos_y,$custkey) = @_;
	if ($pos_y > 400) {	
      PDF_begin_page($p, $page_x, $page_y);
	}

        if ($custkey == 2) {
          $imagefile = "C:/xampp/htdocs/CS_Sameday/cslabel.jpg";
        } else {
          $imagefile = "C:/xampp/htdocs/CS_Sameday/cslabel.jpg";
        }
#	$page_col1_1 = 160;
	$image = PDF_open_image_file($p, "jpeg", $imagefile, "", 0);
	die "Couldn't open image '$imagefile'" if ($image == -1);
	$width = PDF_get_value($p, "imagewidth", $image);
	$height = PDF_get_value($p, "imageheight", $image);
	PDF_place_image($p, $image, 40, $pos_y - $height, 1);
	PDF_close_image($p, $image);
}

sub print_label_footer {
	my($pos_y,$count,$total) = @_;
# pos_y for a4 & two labels: 410?, 60?
# set a colour for the line 
#	pdf_setcolor($p, "stroke", "rgb", 0, 0, 0); 
# draw a line  
#	pdf_moveto($p, 40, $pos_y); 
#	pdf_lineto($p, 555, $pos_y); 
#	pdf_stroke($p); 	
        $z = $pos_y+15;
        $font = PDF_findfont($p, "Helvetica", "host", 0);		
		$italicFont = PDF_findfont($p, "Helvetica-Oblique", "host", 0);
#        PDF_setfont($p, $font, 8.0);
#        PDF_set_text_pos($p, 160, $z);
#        PDF_show($p, "Contact Us:");
#        PDF_setfont($p, $font, 7.0);
#        PDF_set_text_pos($p, 160, $z-9);
#        PDF_show($p, "Website: www.citysprint.co.uk");
#        PDF_set_text_pos($p, 160, $z-18);
#        PDF_show($p, "Email:  customerservices\@citysprint.co.uk");
#        PDF_set_text_pos($p, 160, $z-27);
#        PDF_show($p, "Telephone: Customer Services");
#        PDF_set_text_pos($p, 160, $z-36);
#        PDF_show($p, "London on 0207 203 7590");
#        PDF_set_text_pos($p, 160, $z-45);
#        PDF_show($p, "Outside London on 0208 636 1200");
# Print Office details
$z = $z-45;
        PDF_setfont($p, $font, 8.0);
        PDF_set_text_pos($p, 20, $z);
        PDF_show($p, "CitySprint (UK) Ltd");
		
       PDF_setfont($p, $italicFont, 7.0);
        PDF_set_text_pos($p, 20, $z-9);
        PDF_show($p, "Registered office:");
		
		PDF_setfont($p, $font, 7.0);
        PDF_set_text_pos($p, 76, $z-9);
        PDF_show($p, "Ground Floor,");

		PDF_set_text_pos($p, 20, $z-18);
		PDF_show($p, "RedCentral, 60 High Street,");
        PDF_set_text_pos($p, 20, $z-27);
        PDF_show($p,  "Redhill, Surrey RH1 1SH ");        
		
		PDF_setfont($p, $italicFont, 7.0);
		PDF_set_text_pos($p, 20, $z-36);
        PDF_show($p, "Registered No.");

		PDF_setfont($p, $font, 7.0);
		PDF_set_text_pos($p, 68, $z-36);
		PDF_show($p, "4327611 (England)");
        PDF_set_text_pos($p, 20, $z-45);
        PDF_show($p, "VAT No. 997 3273 64");


# Print the regional centres
        PDF_setfont($p, $font, 7.0);
        PDF_set_text_pos($p, 290, $z);
        PDF_show($p, "Aberdeen");
        PDF_set_text_pos($p, 290, $z-9);
        PDF_show($p, "Basingstoke");
        PDF_set_text_pos($p, 290, $z-18);
        PDF_show($p, "Birmingham");
        PDF_set_text_pos($p, 290, $z-27);
		PDF_show($p, "Bracknell");
		
		PDF_set_text_pos($p, 290, $z-36);
        PDF_show($p, "Brentwood");
        PDF_set_text_pos($p, 290, $z-45);
		PDF_show($p, "Brighton");
		
		
		PDF_set_text_pos($p, 335, $z);
        PDF_show($p, "Bristol");
        PDF_set_text_pos($p, 335, $z-9);
        PDF_show($p, "Cambridge");

        PDF_set_text_pos($p, 335, $z-18);
        PDF_show($p, "Cardiff");
        #PDF_set_text_pos($p, 335, $z-9);
        #PDF_show($p, "Cheltenham");
        #PDF_set_text_pos($p, 335, $z-18);
        #PDF_show($p, "Coventry");
        PDF_set_text_pos($p, 335, $z-27);
        PDF_show($p, "Croydon");
        PDF_set_text_pos($p, 335, $z-36);
        PDF_show($p, "Edinburgh");
		
		PDF_set_text_pos($p, 335, $z-45);
        PDF_show($p, "Enfield");
		
        PDF_set_text_pos($p, 382, $z);
        PDF_show($p, "Gatwick");

        PDF_set_text_pos($p, 382, $z-9);
        PDF_show($p, "Glasgow");
        PDF_set_text_pos($p, 382, $z-18);
        PDF_show($p, "Guildford");
        PDF_set_text_pos($p, 382, $z-27);
        PDF_show($p, "Heathrow");
        PDF_set_text_pos($p, 382, $z-36);
        PDF_show($p, "Hemel Hempstead");
        PDF_set_text_pos($p, 382, $z-45);
        PDF_show($p, "High Wycombe");
				
        #PDF_set_text_pos($p, 445, $z);
        #PDF_show($p, "Ipswich");

        PDF_set_text_pos($p, 445, $z);
        PDF_show($p, "Leeds");
        PDF_set_text_pos($p, 445, $z-9);
        PDF_show($p, "London");
		
		 PDF_set_text_pos($p, 445, $z-18);
        PDF_show($p, "London Logistics");
		
        #PDF_set_text_pos($p, 445, $z-18);
        #PDF_show($p, "Maidstone");
        PDF_set_text_pos($p, 445, $z-27);
        PDF_show($p, "Manchester");
        PDF_set_text_pos($p, 445, $z-36);
        #PDF_show($p, "Midlands");
		PDF_show($p, "Medway");
        PDF_set_text_pos($p, 445, $z-45);
        PDF_show($p, "Milton Keynes");

        #PDF_set_text_pos($p, 493, $z);
        #PDF_show($p, "Newbury");
        PDF_set_text_pos($p, 493, $z);
        PDF_show($p, "Newcastle");
		
		PDF_set_text_pos($p, 493, $z-9);
        PDF_show($p, "Norwich");
		
        #PDF_set_text_pos($p, 493, $z-18);
        #PDF_show($p, "Northampton");
        PDF_set_text_pos($p, 493, $z-18);
        PDF_show($p, "Nottingham");
        PDF_set_text_pos($p, 493, $z-27);
        PDF_show($p, "Oxford");
		
		PDF_set_text_pos($p, 493, $z-36);
        PDF_show($p, "Plymouth");
		
        PDF_set_text_pos($p, 493, $z-45);
        PDF_show($p, "Reading");

		PDF_set_text_pos($p, 540, $z);
        PDF_show($p, "Slough");
        PDF_set_text_pos($p, 540, $z-9);
        PDF_show($p, "Southampton");
        PDF_set_text_pos($p, 540, $z-18);
        PDF_show($p, "Swindon");
		
		PDF_set_text_pos($p, 540, $z-27);
        PDF_show($p, "Telford");
        PDF_set_text_pos($p, 540, $z-18);
        PDF_show($p, "Warwick");

	$pos_y = $pos_y - $y_small_spacing;
	
	$font = PDF_findfont($p, "Helvetica", "host", 0);
	PDF_setfont($p, $font, 9.0);

	$font = PDF_findfont($p, "Helvetica", "host", 0);
	PDF_setfont($p, $font, 15.0);
	PDF_set_text_pos($p, 510, $pos_y-35);
	PDF_show($p, "$count of $total");
	if ($pos_y < 380) {
 		PDF_end_page($p);
	}
}
